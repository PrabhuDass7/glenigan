use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use LWP::Simple;
use URI::Escape;

use HTML::Entities;

my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 }, );
$ua->cookie_jar($cookie_jar);
$ua->agent("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36");

open FC,">output.txt";
print FC "S.No\tSession Type\tSession Title\tPresentation Date and Time\tPresentation Title\tAuthor Block\tDisclosure Block\tFull Abstract\tMain URL\tSession URL\tPresentation URL\n";
close FC;

my $c=0;
my $url="https://arvo2021.arvo.org/agenda#/?limit=20&sortByFields[0]=startsAt&sortByOrders[0]=1&uid=wNXPiJ9LTGxc5KczW";
open my $fh,"<ARVO 2021 Annual Meeting_ Agenda.html";

my $content=do { local $/; <$fh> };
my $i=0;

while($content=~m/<li\s*class\=\"flex\-item\"\s*data\-cy\=\"flex\-item\">([\w\W]*?)<\/li>/igs){
	my $block=$1;
	open CC,">Block.txt";
	print CC "$block\n";
	close CC;
	
	while($block=~m/<a[\w\W]*?href\=\"([\w\W]*?)\">[\w\W]*?<span\sclass\=\"m\-filled\"[^>]*?>([\w\W]*?)<\/span><\/div><h4\sclass\=\"flexcard\_title\"\s*?data\-cy\=\"flexcard\-name\"><span>([\w\W]*?)<\/span>/igs){
		my $link=$1;
		my $type=$2;
		my $title=$3;
		if($type eq "Poster Session"||$type eq "Paper Session"){
			print "$title\n";
			print "$type\n";
			print "$link\n";
			$i++;
			my $incontent=getcont1("$link","","","GET");
			
			open FC,">incontent.txt";
			print FC "$incontent\n";
			close FC;

			if($incontent=~m/<li\s*class\=\"flex\-item\"\s*data\-cy\=\"flex\-item\">([\w\W]*?)<\/li>/is){
				my $next=if_sub('<button\s*?class\=\"paginate\-button\s*?next\">(Next)<\/button>',$incontent);
				while($incontent=~m/<li\s*class\=\"flex\-item\"\s*data\-cy\=\"flex\-item\">([\w\W]*?)<\/li>/igs){
					my $block1=$1;
					open CC,">Block.txt";
					print CC "$block\n";
					close CC;
					# print "Stop";<>;
					while($block1=~m/<a[\w\W]*?href\=\"([\w\W]*?)\">[\w\W]*?<span\sclass\=\"m\-filled\"[^>]*?>([\w\W]*?)<\/span><\/div><h4\sclass\=\"flexcard\_title\"\s*?data\-cy\=\"flexcard\-name\"><span>([\w\W]*?)<\/span>/igs){
						my $inlink="https://arvo2021.arvo.org".$1;
						my $typea=$2;
						my $intitle=$3;
						print "$typea\n";
						print "$intitle\n";
						# $inlink="https://arvo2021.arvo.org/meetings/virtual/oxuJWk6G8zhPhBqHM";
						my $inlinkurl="https://api.scraperapi.com/?api_key=4720ce5db2d6c75d7582d2fc440cc5f0&url=".$inlink."&render=true";
						print "$inlink\n";<>;
						my $finalcontent=getcont1("$inlinkurl","","","GET");
						open FC,">finalcontent.txt";
						print FC "$finalcontent\n";
						close FC;
						# print "finalcontent";<>;
						my $presentationdateandtime=if_sub('<p\s*data\-cy\=\"meeting\-date\"><i\sclass\=\"fal\s*?fa\-clock\smeta\_icon\"><\/i>([\w\W]*?)<\/span>',$finalcontent);
						$presentationdateandtime=~s/<[^>]*?>//igs;
						my $author=if_sub('<u>(Author\sBlock\:\s*?<\/u>[\w\W]*?)<\/p>',$finalcontent);
						$author=~s/<[^>]*?>//igs;
						$author=~s/<\/[^>]*?>//igs;
						my $disclosure=if_sub('<u>(Disclosure\sBlock\:\s*?<\/u>[\w\W]*?)<\/p>',$finalcontent);
						$disclosure=~s/<[^>]*?>//igs;
						$disclosure=~s/<\/[^>]*?>//igs;
						my $abstract=if_sub('<p><b>(Purpose\:<\/b>[\w\W]*?)<\/div><\/div><\/div>',$finalcontent);
						$abstract=~s/<[^>]*?>//igs;
						$abstract=~s/<\/[^>]*?>//igs;
						decode_entities($title);
						decode_entities($intitle);
						decode_entities($author);
						decode_entities($disclosure);
						decode_entities($abstract);
						$title=trim($title);
						$intitle=trim($intitle);
						$author=trim($author);
						$disclosure=trim($disclosure);
						$abstract=trim($abstract);
						
						open CC,">>Output.txt";
						print CC "$i\t$type\t$title\t$presentationdateandtime\t$intitle\t$author\t$disclosure\t$abstract\t$url\t$link\t$inlink\t$next\n";
						close CC;
						$i++;
						
					}
				}
			}else{
				my $presentationdateandtime=if_sub('<p\s*data\-cy\=\"meeting\-date\"><i\sclass\=\"fal\s*?fa\-clock\smeta\_icon\"><\/i>([\w\W]*?)<\/span>',$incontent);
				$presentationdateandtime=~s/<[^>]*?>//igs;

				open CC,">>Output.txt";
				print CC "$i\t$type\t$title\t$presentationdateandtime\t\t\t\t\t$url\t$link\t\n";
				close CC;
			}
		}
	}
}

sub if_sub(){
	my ($regex,$cont)=@_;
	my $data="";
	if($cont=~m/$regex/is){
		$data=$1;
	}
	return $data;
}

sub anyevent()
{
	my $url = shift;
	my $cv = AnyEvent->condvar;
	my $c=0;
	my $html;my $status;my $st;
	# print "Start $url";
	$cv->begin;
	http_get $url, sub {
		($html,$status) = @_;
		$st = $status->{Status};
		# print $c;
		# print $st;
		# print "$url received, Size: ",length $html;
		$cv->end;
	};	
	$cv->recv;
	return($html);
}
sub trim()
{
	my $v=shift;
	$v=~s/<[^>]*?>/ /igs;
	$v=~s/\s+/ /igs;
	$v=~s/^\s//igs;
	$v=~s/\s$//igs;
	$v=~s/\&#amp\;/&/igs;
	$v=~s/&amp;/&/igs;
	$v=~s/\&amp\;/&/igs;
	$v=~s/\&\#39\;/'/igs;
	$v=~s/\&\#8203\;//igs;
	$v=~s/\&nbsp\;/ /igs;
	$v=~s/&quot;/"/gs;
	$v=~s/\?//igs;
	$v=~s/\?//igs;
	return $v;
}
sub getcont1()
{
    my($ur,$cont,$ref,$method)=@_;
	my $cc=0;
    netfail:
    my $request=HTTP::Request->new("$method"=>$ur);
    $request->header("accept"=>"*/*");
    $request->header(":path"=>"/sockjs/028/gzh0f5c3/xhr");
    $request->header("origin"=>"https://arvo2021.arvo.org");
    $request->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8");
    # $request->header("Referer"=>"");
    $request->header("Cookie"=>"galaxy-sticky=!jabLX86tEw7TmAD3L-fjbmh; scope={%22accountId%22:%2274ztYugWhSH9G92pA%22%2C%22communityId%22:%22tCfLLJvXnziB8LuGt%22%2C%22siteId%22:%22DSFGtwRyZs5ZHRRXm%22}; _ga=GA1.2.1765926.1618815118; _gid=GA1.2.98982912.1618815118; crisp-client%2Fsession%2Fb680b8ba-f9ae-4a3c-9ee8-aa67d603a57e=session_e4b82d69-b3b8-44bf-8aa7-234bbd32337e");

    if($ref ne '')
    {
   
        $request->header("Referer"=>"$ref");
    }
    if(lc $method eq 'post')
    {
   
        $request->content($cont);
    }
    my $res=$ua->request($request);
    $cookie_jar->extract_cookies($res);
    $cookie_jar->save;
    $cookie_jar->add_cookie_header($request);
	# my $req_head1=$res->headers_as_string();
	# print "req_head1 :$req_head1\n";
	# my $ckk;
	# if($req_head1=~m/Cookie\s*\:\s+([^>]*?)Cookie2/is)
	# {
		# $ckk=$1;
		# chomp($ckk);
	# }

    my $code=$res->code;
	
	if($code==200)
    {
           
            my $content=$res->content();
            my $content=$res->decoded_content();
            # $content=decode_entities($content);
            return $content;
    }elsif($code=~m/50/is)
    {
		$cc++;
		if($cc<=5)
		{

			sleep(30);
			goto netfail;
		}
    }
    elsif($code=~m/30/is)
    {
       
		my $loc=$res->header("Location");
		$loc="https://www.mbp.state.md.us$loc";
		
		print "\nLocation: $loc";
		my $request1=HTTP::Request->new(GET=>$loc);
        $request1->header("Content-Type"=>"text/html;charset=ISO-8859-1");
        my $res1=$ua->request($request1);
        $cookie_jar->extract_cookies($res1);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request1);
		my $ckk;
        my $content1=$res1->content();
        return ($content1);

    }
    elsif($code=~m/40/is)
    {
		 my $content=$res->content();
        return $content;
        print "\n URL Not found";
    }
}