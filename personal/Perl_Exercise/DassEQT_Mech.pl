use strict;
use WWW::Mechanize;
use URI::Escape;
use IO::Socket::SSL qw();
use HTTP::CookieMonster;
use JSON;


my $mech = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);
				
my $Response = $mech->get("https://lp.eqtgroup.com/login");

# pgadmin@partnersgroup.com

my $Content = $mech->content;
my $currentPageURL = $mech->uri();
my $scriptStatus = $mech->status;

my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );

my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();
print "session_id:: $session_id\n";

print "currentPageURL:: $currentPageURL\n";
print "scriptStatus:: $scriptStatus\n";

open GH,">Content.html";
print GH "$Content";
close GH;

#-------------- Content1 ---------------------

my $post_url="https://lp.eqtgroup.com/api/public/start-login";
my $postcontent='{"email":"pgadmin@partnersgroup.com"}';

$mech->add_header( "authority" => "lp.eqtgroup.com" );
$mech->add_header( "method" => "POST" );
$mech->add_header( "path" => "/api/public/start-login" );
$mech->add_header( "accept" => "application/json, text/plain, */*" );
$mech->add_header( "content-length" => 37 );
$mech->add_header( "Content-Type" => "application/json;charset=UTF-8");
$mech->add_header( "origin" => "https://lp.eqtgroup.com" );
$mech->add_header( "referer" => "https://lp.eqtgroup.com/login" );
$mech->add_header( "cookie" => "__cfduid=".$session_id );
$mech->add_header( "user-agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36" );


$mech->post( $post_url, Content => "$postcontent");
	  
my $con = $mech->content;
my $code = $mech->status;
my $currentPageURL = $mech->uri();

print "currentPageURL:: $currentPageURL\n";
print "code:: $code\n";
	
open GH,">Content2.html";
print GH "$con";
close GH;

#-------------- Content2 ---------------------

# my $url3='https://investor-portal-prod.eu.auth0.com/authorize?client_id=QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX&redirect_uri=https%3A%2F%2Flp.eqtgroup.com&max_age=900&email=pgadmin%40partnersgroup.com&scope=openid%20profile%20email%20offline_access&response_type=code&response_mode=query&state=bkQ3OWFUaThHUS5kaE5JODRPVWVNdjVRVm41TklSYXI2c0tFRk5rMk5FNw%3D%3D&nonce=VkJqUzM5YmZnVENVbHFuZ21TNVA5NGEzN0JFM0xSdWg2QWE3SWQ1VUFaRw%3D%3D&code_challenge=okr_GDeKiGusWAof-g6iLEwVK8xvJRRLAhJg_XYkZ98&code_challenge_method=S256&auth0Client=eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCJ9';

my $url3='https://investor-portal-prod.eu.auth0.com/authorize?client_id=QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX&redirect_uri=https%3A%2F%2Flp.eqtgroup.com&max_age=900&email=pgadmin%40partnersgroup.com&scope=openid%20profile%20email%20offline_access&response_type=code&response_mode=query&state=VlRkT3VabHlFOHNSQX45RTJMcl9JVlVmUENKOTJ1RTNVR1ZDUTFEQXBwTg%3D%3D&nonce=T1J3V2c4NGx1Z2pvUWVlWDhxWX5WQVRnaXFsNEZSZ3pKajBEWVdVeXBiRA%3D%3D&code_challenge=zjPPSqHf57OEXOlzQP9q2Dvw8nsO_rKOh87zGj7eJvI&code_challenge_method=S256&auth0Client=eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCJ9';

$mech->add_header( "authority" => "investor-portal-prod.eu.auth0.com");
$mech->add_header( "Content-Type" => "text/html; charset=utf-8");
$mech->add_header( "referer" => "https://lp.eqtgroup.com/");
$mech->add_header( "upgrade-insecure-requests" => 1);
$mech->add_header( "cookie" => "__cfduid=".$session_id);
$mech->add_header( "user-agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36" );

my $Response = $mech->get($url3);
my $Content = $mech->content;
my $currentPageURL = $mech->uri();
my $scriptStatus = $mech->status;

my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );

my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();
print "session_id:: $session_id\n";

print "currentPageURL:: $currentPageURL\n";
print "scriptStatus:: $scriptStatus\n";

open GH,">Content3.html";
print GH "$Content";
close GH;



#-------------- Content3 ---------------------

my $stateID=$1 if($currentPageURL=~m/state\=([^>]*?)\&/is);
my $nonceID=$1 if($currentPageURL=~m/nonce\=([^>]*?)\&/is);
my $code_challenge=$1 if($currentPageURL=~m/code\_challenge\=([^>]*?)\&/is);

print "stateID:: $stateID\n";
print "nonceID:: $nonceID\n";
print "code_challenge:: $code_challenge\n";

my $nonceID_unescaped = uri_unescape( $nonceID );

my $csrf_Lk="https://investor-portal-prod.eu.auth0.com/login?state=$stateID&client=QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX&protocol=oauth2&redirect_uri=https%3A%2F%2Flp.eqtgroup.com&max_age=900&email=pgadmin%40partnersgroup.com&scope=openid%20profile%20email%20offline_access&response_type=code&response_mode=query&nonce=$nonceID&code_challenge=$code_challenge&code_challenge_method=S256&auth0Client=eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCJ9";

my $Response = $mech->get($csrf_Lk);
my $Content = $mech->content;
my $currentPageURL = $mech->uri();
my $scriptStatus = $mech->status;

my $csrf_cook=$mech->response->header('Set-Cookie');

print "csrf_cook:: $csrf_cook\n";
my $CRSF_TOKEN=$1 if($csrf_cook=~m/\_csrf\=([^>]*?)\;/is);

open GH,">CSRFContent.html";
print GH "$Content";
close GH;

# -----------------------

my $post_url="https://investor-portal-prod.eu.auth0.com/passwordless/start";
my $postcontent='{"client_id":"QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX","connection":"email","email":"pgadmin@partnersgroup.com","send":"code","authParams":{"response_type":"code","redirect_uri":"https://lp.eqtgroup.com","scope":"openid profile email offline_access","state":"'.$stateID.'","nonce":"'.$nonceID_unescaped.'"}}';


$mech->add_header( "authority" => "investor-portal-prod.eu.auth0.com");
$mech->add_header( "method" => "POST");
$mech->add_header( "path" => "/passwordless/verify");
$mech->add_header( "Content-Type" => "application/json");
$mech->add_header( "content-length" => length($postcontent));
$mech->add_header( "origin" => 'https://investor-portal-prod.eu.auth0.com');
$mech->add_header( "referer" => $currentPageURL);
$mech->add_header( "cookie" => "__cfduid=".$session_id);
$mech->add_header( "auth0-client" => "eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCIsImVudiI6eyJsb2NrLmpzLXVscCI6IjExLjI0LjUiLCJhdXRoMC5qcy11bHAiOiI5LjEzLjQiLCJhdXRoMC5qcyI6IjkuMTMuNCJ9fQ==");

$mech->post( $post_url, Content => "$postcontent");
my $Content = $mech->content;
my $currentPageURL = $mech->uri();
my $scriptStatus = $mech->status;

my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );

my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();
print "session_id4:: $session_id\n";
print "scriptStatus:: $scriptStatus\n";
print "Content4 currentPageURL:: $currentPageURL\n";

open GH,">Content4.html";
print GH "$Content";
close GH;


#------------------------- VERIFICATION CODE --------------------------------------------


print "Please Enter Code Your Verification Code (OTP) :: ";
my $verificationCode=<STDIN>;

$verificationCode=trim($verificationCode);
print "Your Verification Code is $verificationCode\n";
my $post_url="https://investor-portal-prod.eu.auth0.com/passwordless/verify";
my $postcontent='{"connection":"email","verification_code":"'.$verificationCode.'","email":"pgadmin@partnersgroup.com","auth0Client":"eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCJ9"}';

$postcontent=trim($postcontent);

print "*******pooo********$postcontent*********\n";<>;
print "*******post_url********$post_url*********\n";<>;

$mech->add_header( "authority" => "investor-portal-prod.eu.auth0.com");
$mech->add_header( "method" => "POST");
$mech->add_header( "path" => "/passwordless/verify");
$mech->add_header( "Content-Type" => "application/json");
$mech->add_header( "content-length" => length($postcontent));
$mech->add_header( "origin" => 'https://investor-portal-prod.eu.auth0.com');
$mech->add_header( "referer" => $currentPageURL);
$mech->add_header( "cookie" => "__cfduid=".$session_id);
# $mech->add_header( "auth0-client" => "eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCIsImVudiI6eyJsb2NrLmpzLXVscCI6IjExLjI0LjUiLCJhdXRoMC5qcy11bHAiOiI5LjEzLjQiLCJhdXRoMC5qcyI6IjkuMTMuNCJ9fQ==");

$mech->post( $post_url, Content => "$postcontent");
my $Content5 = $mech->content;
my $post_url = $mech->uri();
my $scriptStatus = $mech->status;

print "post_url After CODE:: $post_url\n";
print "scriptStatus:: $scriptStatus\n";

my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );

my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();

print "*******session_id555********$session_id*********\n";<>;

open GH,">Content5.html";
print GH "$Content5";
close GH;

print "*** Content5 Completed ***\n";<STDIN>;

# my $red_URL="https://investor-portal-prod.eu.auth0.com/passwordless/verify_redirect?client_id=QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX&response_type=code&redirect_uri=https%3A%2F%2Flp.eqtgroup.com&scope=openid%20profile%20email%20offline_access&state=$stateID&nonce=$nonceID%3D%3D&verification_code=$verificationCode&response_mode=query&_intstate=deprecated&_csrf=4qtzxL5v-Ab47apjzP8Hm9H0t9_OxZCF0JkU&max_age=900&code_challenge_method=S256&code_challenge=$code_challenge&auth0Client=eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCIsImVudiI6eyJsb2NrLmpzLXVscCI6IjExLjI0LjUiLCJhdXRoMC5qcy11bHAiOiI5LjEzLjQiLCJhdXRoMC5qcyI6IjkuMTMuNCJ9fQ%3D%3D&protocol=oauth2&email=pgadmin%40partnersgroup.com&connection=email&popup=false";

my $red_URL="https://investor-portal-prod.eu.auth0.com/passwordless/verify_redirect?client_id=QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX&response_type=code&redirect_uri=https%3A%2F%2Flp.eqtgroup.com&scope=openid%20profile%20email%20offline_access&state=$stateID&nonce=$nonceID%3D%3D&verification_code=$verificationCode&response_mode=query&_intstate=deprecated&_csrf=$CRSF_TOKEN&max_age=900&code_challenge_method=S256&code_challenge=$code_challenge&auth0Client=eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCIsImVudiI6eyJsb2NrLmpzLXVscCI6IjExLjI0LjUiLCJhdXRoMC5qcy11bHAiOiI5LjEzLjQiLCJhdXRoMC5qcyI6IjkuMTMuNCJ9fQ%3D%3D&protocol=oauth2&email=pgadmin%40partnersgroup.com&connection=email&popup=false";

$mech->add_header( "authority" => "investor-portal-prod.eu.auth0.com");
$mech->add_header( "method" => "GET");
$mech->add_header( "referer" => $currentPageURL);
$mech->add_header( "cookie" => "__cfduid=".$session_id);

my $Response = $mech->get($red_URL);

my $Content = $mech->content;
my $currentPageURL = $mech->uri();
my $scriptStatus = $mech->status;

print "currentPageURL:: $currentPageURL\n";
print "scriptStatus:: $scriptStatus\n";

open GH,">Content6.html";
print GH "$Content";
close GH;

my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );
my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();
print "session_id:: $session_id\n";


=e

#-------------- Content5qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq ---------------------
my $token_url="https://investor-portal-prod.eu.auth0.com/oauth/token";
my $token_content='{"redirect_uri":"https://lp.eqtgroup.com","client_id":"QxBcqZqwr4MyM8MSapvFh4OdLwjEeBsX","code_verifier":"PpFjBlafEvbXCz2R3SZ9YKRjcQ3OiDrRX-4AE7Nm7N3","grant_type":"authorization_code","code":"skIRCiJXgpWl8qPL"}';

$token_content=trim($token_content);
my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );

my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();
print "session_id6666:: $session_id\n";
print "*******pooo********$token_content*********\n";<>;
# print "*******session_id********$session_id*********\n";<>;

$mech->add_header( "authority" => "investor-portal-prod.eu.auth0.com");
$mech->add_header( "method" => "POST");
$mech->add_header( "path" => "/oauth/token");
$mech->add_header( "Content-Type" => "application/json");
$mech->add_header( "content-length" => length($token_content));
$mech->add_header( "origin" => 'https://lp.eqtgroup.com');
$mech->add_header( "referer" => 'https://lp.eqtgroup.com/');
$mech->add_header( "cookie" => "__cfduid=".$session_id);
# $mech->add_header( "auth0-client" => "eyJuYW1lIjoiYXV0aDAtc3BhLWpzIiwidmVyc2lvbiI6IjEuMTIuMCIsImVudiI6eyJsb2NrLmpzLXVscCI6IjExLjI0LjUiLCJhdXRoMC5qcy11bHAiOiI5LjEzLjQiLCJhdXRoMC5qcyI6IjkuMTMuNCJ9fQ==");


$mech->post( $token_url, Content => "$token_content");
my $Content6 = $mech->content;
my $token_url = $mech->uri();
my $scriptStatus = $mech->status;

# print "currentPageURL:: $currentPageURL\n";

# print "currentPageURL:: $currentPageURL\n";
print "scriptStatus:: $scriptStatus\n";

open GH,">Content6.html";
print GH "$Content6";
close GH;


print "*** Content 6 Completed ***\n";<>;

#-----------------------------------------------------------------

my $inbox_url="https://lp.eqtgroup.com/api/private/inbox";

my $monst=HTTP::CookieMonster->new( $mech->cookie_jar );

my $cook=$monst->get_cookie('__cfduid');
my $session_id=$cook->val();
print "session_id6666:: $session_id\n";

$mech->add_header( "authority" => "lp.eqtgroup.com");
$mech->add_header( "method" => "GET");
$mech->add_header( "path" => "/api/private/inbox");
$mech->add_header( "Content-Type" => "application/json; charset=utf-8");
# $mech->add_header( "content-length" => length($token_content));
$mech->add_header( "origin" => 'https://lp.eqtgroup.com');
$mech->add_header( "referer" => 'https://lp.eqtgroup.com/');
$mech->add_header( "cookie" => "__cfduid=".$session_id);
$mech->add_header( "auth0-client" => "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkltS0pkLUx2UjVBU2dDU05HcWlhaCJ9.eyJuaWNrbmFtZSI6InBnYWRtaW4iLCJuYW1lIjoicGdhZG1pbkBwYXJ0bmVyc2dyb3VwLmNvbSIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci9lMGIxMmViMzBlM2E1ZDM1MzViNjZkYzkwZDBhNmUxZj9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnBnLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDIxLTAzLTA5VDA2OjEwOjIwLjQ2N1oiLCJlbWFpbCI6InBnYWRtaW5AcGFydG5lcnNncm91cC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6Ly9pbnZlc3Rvci1wb3J0YWwtcHJvZC5ldS5hdXRoMC5jb20vIiwic3ViIjoiZW1haWx8NWVjY2I3N2Y3NmM4NTM2NzI4MjI1MjM0IiwiYXVkIjoiUXhCY3FacXdyNE15TThNU2FwdkZoNE9kTHdqRWVCc1giLCJpYXQiOjE2MTUyNzAyMjQsImV4cCI6MTYxNTI3MTQyMywiYXV0aF90aW1lIjoxNjE1MjcwMjIwLCJub25jZSI6IkxXUTFlV0UxTVVWc2RsUklTRVZyVG1oU00wVjVMamQwTVdWelJGaGpjR0puTVRsdlRtcEZOak56UWc9PSJ9.qWuXLcBuGMQifSkbxoBOnRVIz0sWbuL-jvs_krryXHrVz55EqO9dZSGzg42VumGQtWovpMuSq95HNLd5M0GB6dYuRX1jg0JeZPgbCYHr2TgZJBgFmeguUcsDSvHjEtYIgqcmiFqB6Pt0YZ-xPMHbRP0TaA39PCSs2GKsrhzesi0-X6RBcPUWZPHZXEBE3VCTrPJXI99QuDVBqpFsdZbapbL8cC4fCPR80yVxF9USS7-_yUM8WUBWxdv54xLcHOoB3VmXlqT0W5p8DtfeeLDSQhmu4JDpwSFzMengbORv-oE0qKaNKzllZ67xKd058e1R-Q78LkrFRGTP0ejk-HP4EQ");

my $Response11 = $mech->get($inbox_url);

# pgadmin@partnersgroup.com

my $Content6 = $mech->content;
my $inbox_url = $mech->uri();
my $scriptStatus = $mech->status;

# print "currentPageURL:: $currentPageURL\n";

print "inbox_url:: $inbox_url\n";
print "scriptStatus:: $scriptStatus\n";

open GH,">Content7.html";
print GH "$Content6";
close GH;

print "*** Content 6 Completed ***\n";<>;

=cut

sub trim
{
	my $v=shift;
	$v=~s/\&nbsp\;//igs;
	$v=~s/\&amp\;/\&/igs;
	$v=~s/\s+/ /igs;
	$v=~s/^\s//igs;
	$v=~s/\s$//igs;
	return $v;
}