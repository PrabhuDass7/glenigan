<%
	set PdfCreator = Server.CreateObject("PdfOut.PdfCreator")

	ConvertHTMLToPDF1 PdfCreator,"http://www.google.com","google001.pdf"
	
	ConvertHTMLToPDF2 PdfCreator,"http://www.google.com","google002.pdf"

	'Convert MS Word document to PDF file
	ConvertWordToPDF pdfCreator,"test1.doc","test1.pdf"
	ConvertWordToPDF pdfCreator,"test2.doc","test2.pdf"
	ConvertWordToPDF pdfCreator,"test3.doc","test3.pdf"
	ConvertWordToPDF pdfCreator,"test4.doc","test4.pdf"
	
	set PdfCreator = nothing


	function ConvertHTMLToPDF1(byval PdfCreator, strHTMLFile, strPDFFile)
		'The first method to create a PDF file from HTML file
		strPDFFile  = server.mappath(".") & "\" & strPDFFile
		dim strCommandLine
		strCommandLine = """" + strHTMLFile + """"
		strCommandLine = strCommandLine + " "
		strCommandLine = strCommandLine + """" + strPDFFile + """"
		strCommandLine = strCommandLine + " "
		strCommandLine = strCommandLine + """" + "paperType=6" + """"
		Response.write "HTML To PDF Command Line1 is: " & strCommandLine & "<br>"	
		PdfCreator.Doc2PDFCommandLine(strCommandLine)
	end function 

	function ConvertHTMLToPDF2(byval PdfCreator, strHTMLFile, strPDFFile)
		'The second method to create a PDF file from HTML file
		strPDFFile  = server.mappath(".") & "\" & strPDFFile
		PdfCreator.html2PDF= strHTMLFile
		PdfCreator.fileName = strPDFFile
		PdfCreator.paperType = 6
		pdfCreator.headersFooters = "on"
		pdfCreator.htmlHeaders="&w&bPage&p/&P"
		pdfCreator.htmlFooters="&u&b&d"
		pdfCreator.margin_left = 100.0
		Response.write "HTML To PDF Command Line2 is: " & strHTMLFile & " ==> " & strPDFFile & "<br>"
		result = PdfCreator.Doc2PDFViaSocket()
	end function 
	
	function ConvertWordToPDF(byval PdfCreator, strWordFile, strPDFFile)
		strPDFFile1 = server.mappath(".") & "\" & strWordFile
		strPDFFile2 = server.mappath(".") & "\" & strPDFFile
		strCommand = """" & strPDFFile1 & """"
		strCommand = strCommand & " " & """" & strPDFFile2 & """"
		Response.write "WordToPDF Command Line is: " & strCommand & "<br>"
		PdfCreator.Doc2PDFCommandLine(strCommand)
	end function 
%>
