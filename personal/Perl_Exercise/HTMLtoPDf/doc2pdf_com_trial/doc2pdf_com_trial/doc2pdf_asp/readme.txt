
>How to Quick Start the ASP example?

A: Please by following steps to Quick Start the ASP example,

1. Download and install PDFcamp software from our website,
http://www.toppdf.com/pdfcamp/pdfcamp_setup.exe

2. Please run the "../install.bat" to install DocConverter COM to your system,

3. Run MS Internet Explorer software, and open 
"http://127.0.0.1/doc2pdf_via_socket.asp" file to test,

5. The trial version of DocConverter COM will popup a messagebox 
on your server, but the registered version hasn't this messagebox, 
you may manual close it, then the conversion will continue.

6. You'll find a printer icon appear in the taskbar on your server, 
after a few seconds, the printer icon disappear, the pdf file is created.

7. Then it's OK.


Please notice, you need VB6.0 DLL library installed on your server, if 
you haven't VB6.0 library, please contact us.

If you encounter any problem, please contact us at support@verypdf.com.
Thank you.