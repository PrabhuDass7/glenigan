// execmd.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string>
#include <vector>
using namespace std;

typedef struct FILELISTtag{
	string m_strInFilename;
	string m_strOutFilename;
}FILELIST;
int NTSystemCommand(const char *command)
{
	char
		local_command[2048];
	
	DWORD   child_status = 0;
	
	int
		status;
	
	PROCESS_INFORMATION
		process_info;
	
	STARTUPINFO
		startup_info;
	
	unsigned int
		background_process;
	
	if (command == (char *) NULL)
		return(-1);
	OutputDebugString(command);
	OutputDebugString("\n");
	GetStartupInfo(&startup_info);
	startup_info.dwFlags=STARTF_USESHOWWINDOW;
	startup_info.wShowWindow=SW_HIDE;
	(void) strcpy(local_command,command);
	background_process=command[strlen(command)-1] == '&';
	if (background_process)
		local_command[strlen(command)-1]='\0';
	if (command[strlen(command)-1] == '|')
		local_command[strlen(command)-1]='\0';
	else
		startup_info.wShowWindow=SW_HIDE;

	status=CreateProcess((LPCTSTR) NULL,local_command,
		(LPSECURITY_ATTRIBUTES) NULL,(LPSECURITY_ATTRIBUTES) NULL,(BOOL) FALSE,
		(DWORD) NORMAL_PRIORITY_CLASS,(LPVOID)NULL,(LPCSTR) NULL,&startup_info,
		&process_info);

	if (status == 0)
		return(-1);
	if (background_process)
		return(status == 0);
	status=WaitForSingleObject(process_info.hProcess,INFINITE);
	if (status != WAIT_OBJECT_0)
		return(status);
	status=GetExitCodeProcess(process_info.hProcess,&child_status);
	if (status == 0)
		return(-1);
	CloseHandle(process_info.hProcess);
	CloseHandle(process_info.hThread);
	return((int) child_status);
}
int FindAllFiles(LPCTSTR szInputFilename, vector<FILELIST>& m_aryFiles)
{
	char szOutExt[_MAX_EXT];
	char szOutDrive[_MAX_DRIVE];
	char szOutDir[_MAX_DIR];
	char szOutName[_MAX_PATH];
	
	char szTempExt[_MAX_EXT];
	char szTempDrive[_MAX_DRIVE];
	char szTempDir[_MAX_DIR];
	char szTempName[_MAX_PATH];

	_splitpath(szInputFilename, szOutDrive, szOutDir, szOutName, szOutExt);	
	WIN32_FIND_DATA stFindData;
	memset(&stFindData,0,sizeof(WIN32_FIND_DATA));
	HANDLE hFind = FindFirstFile(szInputFilename,&stFindData);
	BOOL bFind = TRUE;
	while((hFind != INVALID_HANDLE_VALUE)&&(bFind))
	{
		char szInFileName[MAX_PATH];
		_splitpath(stFindData.cFileName, szTempDrive, szTempDir, szTempName, szTempExt);
		_makepath(szInFileName,szOutDrive, szOutDir, szTempName, szTempExt);

   		DWORD m_dwFileAttrib = GetFileAttributes(szInFileName);
		if(m_dwFileAttrib != 0xFFFFFFFF && !(m_dwFileAttrib&FILE_ATTRIBUTE_DIRECTORY))
		{
			FILELIST m_itemFile;
			m_itemFile.m_strInFilename = szInFileName;
			m_aryFiles.push_back(m_itemFile);
		}
		
		bFind = FindNextFile(hFind,&stFindData);
	}
	FindClose(hFind);
	return m_aryFiles.size();
}
int ConverterFiles(vector<FILELIST>& m_aryFiles,string strOptions)
{
	char szExeName[MAX_PATH];
	GetModuleFileName(NULL,szExeName,MAX_PATH);
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	_splitpath(szExeName, drive, dir, fname, ext );
	_makepath(szExeName,drive,dir,"html2pdf",".exe");

	int m_iCount = m_aryFiles.size();
	for(int i = 0; i < m_iCount; i++)
	{
		char *ptrInFilename = (char*)m_aryFiles[i].m_strInFilename.c_str();
		char *ptrOutFilename = (char*)m_aryFiles[i].m_strOutFilename.c_str();
		_splitpath(ptrOutFilename,drive,dir,fname,ext );

		string strCommandLine;
		strCommandLine = "\"";
		strCommandLine += szExeName;
		strCommandLine += "\" \"";
		strCommandLine += ptrInFilename;
		strCommandLine += "\" \"";
		strCommandLine += ptrOutFilename;
		if(strOptions.size() > 0)
		{
			strCommandLine += "\" \"";
			strCommandLine += strOptions;
		}
		strCommandLine += "\"";
		NTSystemCommand(strCommandLine.c_str());
		printf("%s ==> %s\n",ptrInFilename,ptrOutFilename);
	}
	return 0;
}

void GenerateOutputFilenames(LPCTSTR szOutputFilename, vector<FILELIST>& m_aryFiles)
{
	char drive1[_MAX_DRIVE],dir1[_MAX_DIR],fname1[_MAX_FNAME],ext1[_MAX_EXT];

	int m_iCount = m_aryFiles.size();
	if(m_iCount == 0)
		return;
	{
		_splitpath(szOutputFilename,drive1,dir1,fname1,ext1);
		char szOutFolder[MAX_PATH];
		DWORD m_dwFileAttrib = GetFileAttributes(szOutputFilename);
		if(m_dwFileAttrib != 0xFFFFFFFF && (m_dwFileAttrib&FILE_ATTRIBUTE_DIRECTORY))
			strcpy(szOutFolder,szOutputFilename);
		else
			_makepath(szOutFolder,drive1,dir1,NULL,NULL);
		if(szOutFolder[strlen(szOutFolder)-1] != '\\')
			strcat(szOutFolder,"\\");

		char defaultext[_MAX_EXT];
		if(ext1[0])
			strcpy(defaultext,ext1);
		else
			strcpy(defaultext,".pdf");

		for(int i = 0; i < m_iCount; i++)
		{
			char szOutputTmpFilename[MAX_PATH];
			char *ptrInFilename = (char*)m_aryFiles[i].m_strInFilename.c_str();
			_splitpath(ptrInFilename,drive1,dir1,fname1,ext1);
			strcpy(szOutputTmpFilename,szOutFolder);
			strcat(szOutputTmpFilename,fname1);
			strcat(szOutputTmpFilename,defaultext);
			if(m_aryFiles[i].m_strOutFilename.size() == 0)
				m_aryFiles[i].m_strOutFilename = szOutputTmpFilename;
		}
	}
}
int main(int argc, char* argv[])
{
	if(argc != 3 && argc != 4)
	{
		printf("usage: batch.exe <in file> <out file> [options]\n");
		return 0;
	}
	char szInputFilename[MAX_PATH];
	strcpy(szInputFilename,argv[1]);

	DWORD m_dwFileAttrib = GetFileAttributes(szInputFilename);
	if(m_dwFileAttrib != 0xFFFFFFFF && (m_dwFileAttrib&FILE_ATTRIBUTE_DIRECTORY))
	{
		if(szInputFilename[strlen(szInputFilename)-1] != '\\')
			strcat(szInputFilename,"\\");
		strcat(szInputFilename,"*.*");
	}
	vector<FILELIST> m_aryFiles;
	char drive[_MAX_DRIVE],dir[_MAX_DIR],fname[_MAX_PATH],ext[_MAX_EXT];
	_splitpath(szInputFilename, drive, dir, fname, ext);
	if(strstr(fname,"*"))
		FindAllFiles(szInputFilename,m_aryFiles);
	else
	{
		FILELIST m_itemFile;
		m_itemFile.m_strInFilename = szInputFilename;
		m_aryFiles.push_back(m_itemFile);
	}
	GenerateOutputFilenames(argv[2],m_aryFiles);
	if(argc == 4)
		ConverterFiles(m_aryFiles,argv[3]);
	else if(argc == 3)
		ConverterFiles(m_aryFiles,"");
	return 0;
}
