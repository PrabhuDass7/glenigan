use strict;

my @arr = (1..9);
# expected output array - (9,2,7,4,5,6,3,8,1);
# print $arr[-2];


my $len=$#arr+1;
# print $len;

my @op;
my $cc=0;
foreach my $array(@arr)
{
	$cc++;
	if ($array%2==0)
	{
		push(@op,$array);
	}else{
		push(@op,$arr[-$cc]);
	}
}

print "OP:: @op\n";
