# Write a function rotate(ar[], d, n) that rotates arr[] of size n by d elements.
# Array

# Input @arr = (1,2,3,4,5,6,7,8)

# Rotation of the above array by 2 will make array
# output = (3,4,5,6,7,8,1,2)

#--------------------------------------------------------------------------------

use strict;

my @arr = (1,2,3,4,5,6,7,8);

# my $s=slice(0,1,@arr);

my $output=&rotate(\@arr,2);

my @op=@$output;

print "output:: @op";

sub rotate()
{
	my ($array,$count)=@_;
	my @arr=@$array;
	for(my $i=1;$i<=$count;$i++)
	{
		my $pop = shift(@arr);
		push(@arr,$pop);
	}
	return (\@arr);
}