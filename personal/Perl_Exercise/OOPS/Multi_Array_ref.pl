@IP = ('192.168.1.10','192.168.1.15'); 

#Array contains the source port and destination port numbers
@PORT = ("5000","5002"); 

#Sublayers of TCP layer
@TCP = ("Q931","H225","H245"); 

#Common layers are available in a TCP packet.
@LAYER = ("ETHERNET","IP",\@TCP); 

@PKT = (
         \@IP,
         \@PORT,
         \@LAYER
        );
#Storing the reference of @PKT array into the scalar variable.
$array_ref = \@PKT;


print  "LAYER ARR1:: ". ${ $array_ref } [2] ."\n";
print "LAYER ARR2:: ".$$array_ref[2] ."\n";
print "LAYER ARR3:: ".$array_ref->[2] ."\n";



print "ARR4:: ".$ { $ { $array_ref } [2] } [2] ."\n";
print "ARR5:: ".$array_ref->[2]->[2]."\n";;

print "ARR6:: ".$ { $ { $ { $array_ref } [2] } [2] } [1] ."\n";
print "ARR7:: ".$array_ref->[2]->[2]->[1] ."\n";

print "ARR8:: ".$array_ref->[2][2][1] ."\n";