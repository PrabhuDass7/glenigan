
#----- Array Reference -------------------

my @array = ("one","two","three","four","five");
 
my $array_ref1 = \@array;
my $array_ref2 = @array;
my $array_ref3 = "@array";
 
print "Scalar :: ". scalar @array ."\n";
print "ARRAY1 :: $array_ref1\n";
print "ARRAY2 :: $array_ref2\n";
print "ARRAY3 :: $array_ref3\n";

#---------- Array Passed to subroutine -----------

&arryRef(\@array);

sub arryRef()
{
	my $arr=shift;
	print "SUbARR:: $arr\n";
}

#----------- reference of an Anonymous Array into the scalar variable ------------

my $AnanymousArray=['1','2','3',4,"dass"];

print "AnanymousArray :: $AnanymousArray\n";

print "Dereference Length::". @{$AnanymousArray} ."\n";
print "Dereference Data::". @{$AnanymousArray}[4] ."\n";

# print "Get all Elements:: " . @{ $_[0] } ."\n";
# print "Get First Elements:: " . $ { $_[0] } [0] ."\n";

print "Omited brace ::". @$AnanymousArray[2] ."\n";


#-------------------------------------

