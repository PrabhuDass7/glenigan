package Rectangle;

use strict;

sub new{

	my $class=shift;
	
	my $self={
		'Length'=>shift,
		'Breadth'=>shift
	};
	
	bless $self, $class;
	return $self;
}


sub getPerimeter{
	my $self =shift;
	print "getPerimeter of Rectangle is ::",2*($self->{'Length'}+$self->{'Breadth'}),"\n";
}

sub getArea{
	my $self=shift;
	return $self->{'Length'} * $self->{'Breadth'};
}

1;