use Text::CSV;

#---------- Writing CSV --------

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();

my $CSV_Output_File_Name = "$Pdf_store_path\\Appeals_CaseWork_$Date_Tmp.csv";

open my $fh, ">", "$CSV_Output_File_Name" or die "Failed to open file: $!";
$csv->print($fh,["ID","Date","Search_Date_Type","Search_Status_Type","Application","Applicant_Name","Agent_Name","Address","Application_Type","Local_Planning_Authority","Proposal","Appeal_Status","Appeal_Decision_Status","Appeal_Lodged_Date","Appeal_Decision_Date","Document_Url","Council_Code","PDF_Appeal_application","PDF_Application_Refno","PDF_Refused_date","PDF_Appeal_Decision_status"]);
close $fh;

#--------- Read csv ------------

my $csv = Text::CSV->new({ sep_char => ', ' });
   
my $file_to_be_read = $ARGV[0] or die;
    
# Reading the file
open(my $data_file, '<', $file_to_be_read) or die;
while (my $line = <$data_file>) 
{
  chomp $line;
   
  # Parsing the line
  if ($csv->parse($line)) 
  {
        
      # Extracting elements
      my @words = $csv->fields();
      for (my $i = 0; $i <= 2; $i++) 
      {
          print "$words[$i] ";
      }
  
      print "\n";
  } 
  else 
  {
      # Warning to be displayed
      warn "Line could not be parsed: $line\n";
  }
}

#-------------------

use strict;

# Using Text::CSV file to allow
# full CSV Reader and Writer
use Text::CSV;

my $file = $ARGV[0] or die;

my $csv = Text::CSV->new (
{
	binary => 1,
	auto_diag => 1,
	sep_char => ', '
});

my $sum = 0;

# Reading the file
open(my $data, '<:encoding(utf8)', $file) or die;

while (my $words = $csv->getline($data))
{
	for (my $i = 0; $i < 3; $i++)
	{
		print "$words->[$i]";
	}
	print "\n";
}

# Checking for End-of-file
if (not $csv->eof)
{
	$csv->error_diag();
}
close $data;
