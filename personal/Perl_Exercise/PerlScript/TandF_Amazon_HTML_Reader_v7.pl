#--------- Project_Name     :: T & F (Amazon)--------
#--------- Developer_Name   :: PRABHUDASS.M ---------
#--------- Create_Date      :: 20-Jun-2019 ----------
#--------- Last_Modify_Date :: 26-July-2019 ----------	

use strict;
use LWP::Simple;
use HTML::TagParser;
use LWP::UserAgent;
use HTML::Entities;
use Encode;
use HTTP::Cookies;
use DateTime;
use Cwd;


my $dir = getcwd;
#------------- DIRECTORY CREATION--------------#

our $ss="$dir/Amazon_HTML";
if (-e $ss and -d $ss) 
{
	print "Directory exists\n";
}
else
{
	mkdir( $ss ) or die "Couldn't create $ss directory\n";
}

open KK,">Link_Collection.txt";
print KK "ISBN\tHeader\tRef_Link\tTitle\tSold_Price\tList_Price\tHome_url\tStart_Time\tEnd_Time\n";
close KK;
				
open JJ,">Amazon_Price_Output.txt";
print JJ "S.No\tFile_Name\tISBN\tTitle\tAuthor\tType\tCatagory\tSold_Price\tList_price\tISBN_10\tISBN_13\tSold_By\tRef_Link\tPublisher_Day\tPublisher\tMain_URL\tType1\tType2\tcata1\tcata2\tPubDt2\tBlk_soldby1\tBlk_soldby_2\tProd_soldBy\tProd_pub_dat\tisbn_10_H\tisbn_13_H\tisbn_10_P\tisbn_13_P\tStart_Time\tEnd_Time\n";
close JJ;

# my $proxycount=0;	
# my @inp = file_to_array("proxy.txt");	
			
# my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
# my $ua=LWP::UserAgent->new;
# $ua->agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");

my $count=1;

open FH,"Input.txt";
my @arr=<FH>;

my $id_cc=1;

foreach my $inn(@arr)
{
	# my ($id,$in_tit)=split('\t',$inn);
	my $id;
	my $in_tit;
	
	chomp($id);
	chomp($in_tit);
	chomp($inn);
	
	my $start_time = DateTime->now( time_zone => "Asia/Kolkata" );
	
	# my $Home_url='https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dstripbooks&field-keywords='.$id;
	#my $Home_url='https://whatismyipaddress.com/';
	
	my $Home_url=$inn;
	
	# print "HOME_URL:: $Home_url\n";
	
	# my $Home_cont=getcont($Home_url,"","","GET","$in_tit","$id");
	
	$id=id_fnm($Home_url);
	
	open HH,"$Home_url";
	my @con=<HH>;
	my $Home_cont="@con";
	# $Home_cont=decode_entities($Home_cont);
	
	# sleep(5);

	open DH,">$ss/$id.html"; 
	print DH "$Home_cont\n";
	close DH;
	
	open DH,">>Fetched_Path.txt"; 
	print DH "$Home_url\n";
	close DH;
	
	print "LLL:: ".length($Home_cont);
	
	# print "**** Count :: $count ****\n";
	# print "****  Home_Content   ****\n";
	
	my $html=HTML::TagParser->new($Home_cont);
	
	# print "ID:: $id\n";	
	
	&HomePageCollection($html,$id,$Home_cont,$Home_url,$start_time)	if($id=~m/^[\d]+$/is);
	&InnerPageCollection($html,$id,$Home_cont,$Home_url,$start_time) if($id!~m/^[\d]+$/is);
	
	
	print "*******  ID COUNT :: $id_cc  ************\n";
	$id_cc++;

}

print " ******* Completed ******* \n";


sub HomePageCollection()
{
	my ($html,$id,$Home_cont,$Home_url,$start_time)=@_;
	my $block = $html->getElementsByClassName("s-result-list s-search-results sg-row");
	
	print "BLK::". length($block);
	
	if (length($block) == 0)
	{
		open JJ,">>Error_Files.txt";
		print JJ "$id\t$Home_url\n";
		close JJ;
		
		goto skpp1; 
	}
	
	my $sub = $block->subTree();
	my @blk=$sub->getElementsByClassName("s-include-content-margin s-border-bottom");
	foreach my $blk(@blk)
	{
		$blk=$blk->subTree();
		
		my $hdr=$blk->getElementsByClassName("a-size-mini a-spacing-none a-color-base s-line-clamp-2");
		my $headr=$hdr->innerText();
		$headr=trim($headr);
		print "HEADER::  $headr\n";
		
		# my $href= $hdr->getElementsByTagName("a");
		my $href=$hdr->getAttribute("href");
		print "link::$href";
		# print "a-section a-spacing-none a-spacing-top-micro->;
		
		my @atbk=$blk->getElementsByClassName("a-row a-size-base a-color-secondary");
		# print $#atbk;
		my $atbk=$atbk[0]->innerText()  if(($#atbk) != -1);
		my ($authr,$pub_dat)=split('\|',$atbk);
		
		$authr=replace($authr,'by',"");
		$authr=remove_space($authr);
		$pub_dat=remove_space($pub_dat);
		$authr=trim($authr);
		$pub_dat=trim($pub_dat);
		print "Author:: $authr\n";
		print "pub_dat:: $pub_dat\n";
		
		#----------- Outer Price List ------
		
		# my($top_handover_hdr_price,$top_kindl_hdr_price,$top_Paperback_hdr_price,$top_handover_list_price,$top_kindl_list_price,$top_Paperback_list_price,$top_handover_lk,$top_kindl_lk,$top_Paperback_lk);
		
		my($btm_handover_tit,$btm_kindl_tit,$btm_Paperback_tit,$btm_handover_hdr_price,$btm_kindl_hdr_price,$btm_Paperback_hdr_price,$btm_handover_list_price,$btm_kindl_list_price,$btm_Paperback_list_price,$btm_handover_lk,$btm_kindl_lk,$btm_Paperback_lk);
		
		#------ Top Area --------
		
		# my @topblk=$blk->getElementsByClassName("a-section a-spacing-none a-spacing-top-small");
		my $topbk=$blk->getElementsByClassName("a-section a-spacing-none a-spacing-top-small");
		# foreach	my $topbk(@topblk)
		# {
			my $bbsub=$topbk->subTree();
			my ($top_hdr_prc,$top_lst_prc);
			
			my $top_tit_bk=$bbsub->getElementsByClassName("a-size-base a-link-normal a-text-bold");
			# print length($top_tit_bk);
			
			my $top_tit=$top_tit_bk->innerText();
			
			# my $top_tit;
			# defined($top_tit ? $top_tit=$top_tit_bk->innerText() : $top_tit= "");
			
			# print "top_tit:: $top_tit\n";
			# print "************\n";
			
			my $top_lk=$top_tit_bk->getAttribute("href");
			$top_lk='https://www.amazon.com'.$top_lk;
			my $top_pr_blk=$bbsub->getElementsByClassName("a-size-base a-link-normal s-no-hover a-text-normal");
			# print "top_pr_blk:: $top_pr_blk\n";
			if(length($top_pr_blk) != "")
			{

				my $bnkk=$top_pr_blk->subTree();
				
				# $top_hdr_prc=$bnkk->getElementsByClassName("a-price")->subTree()->getElementsByClassName("a-offscreen");
				# $top_hdr_prc=$top_hdr_prc->innerText() if(length($top_hdr_prc) != "");
				
				# $top_lst_prc=$bnkk->getElementsByClassName("a-price a-text-price");
				# $top_lst_prc=$top_lst_prc->subTree()->getElementsByClassName("a-offscreen") if(length($top_lst_prc) != "");
				# $top_lst_prc=$top_lst_prc->innerText() if(length($top_lst_prc) != "");
				
				
				my @ar_pr=$bnkk->getElementsByTagName("span");
				foreach my $arr(@ar_pr)
				{
					my $ar=$arr->getAttribute("data-a-color");
					$top_hdr_prc=$arr->subTree()->getElementsByClassName("a-offscreen")->innerText() if ($ar eq 'base');
					$top_lst_prc=$arr->subTree()->getElementsByClassName("a-offscreen")->innerText() if ($ar eq 'secondary');
				}
				
				# print "top_hdr_prc:: $top_hdr_prc\n";
				# print "top_lst_prc:: $top_lst_prc\n";
			}
			
			my $btm_hdr_prc=$top_hdr_prc;
			my $btm_lst_prc=$top_lst_prc;
			my $btm_tit=$top_tit;
			
			
			
			$btm_handover_tit='Hardcover' if($btm_tit eq 'Hardcover');
			$btm_kindl_tit='Kindle' if($btm_tit eq 'Kindle');
			$btm_Paperback_tit='Paperback' if($btm_tit eq 'Paperback');
			
			$btm_handover_hdr_price=$btm_hdr_prc if($btm_tit eq 'Hardcover');
			$btm_kindl_hdr_price=$btm_hdr_prc if($btm_tit eq 'Kindle');
			$btm_Paperback_hdr_price=$btm_hdr_prc if($btm_tit eq 'Paperback');

			$btm_handover_list_price=$btm_lst_prc if($btm_tit eq 'Hardcover');
			$btm_kindl_list_price=$btm_lst_prc if($btm_tit eq 'Kindle');
			$btm_Paperback_list_price=$btm_lst_prc if($btm_tit eq 'Paperback');
			
			$btm_handover_lk=$top_lk if($btm_tit eq 'Hardcover');
			$btm_kindl_lk=$top_lk if($btm_tit eq 'Kindle');
			$btm_Paperback_lk=$top_lk if($btm_tit eq 'Paperback');
			
			my $end_time = DateTime->now( time_zone => "Asia/Kolkata" );
			
			open KK,">>Link_Collection.txt";
			print KK "$id\t$headr\t$top_lk\t$btm_tit\t$btm_hdr_prc\t$btm_lst_prc\t$Home_url\t$start_time\t$end_time\n";
			close KK;
			
			print "Print\n";
			
		# }
		
		#------ Bottom Area ----
		
		my @btmblk=$blk->getElementsByClassName("a-section a-spacing-none a-spacing-top-mini");
		# my @btmblk=$blk->getElementsByClassName("a-section a-spacing-none a-spacing-top-small");
		foreach	my $btbk(@btmblk)
		{
			my $bbsub=$btbk->subTree();
			my @rwmn=$bbsub->getElementsByClassName("a-row a-spacing-mini");
			
			foreach my $rwdt(@rwmn)
			{
				my $rwsub=$rwdt->subTree();
				my $btm_tit_bk=$rwsub->getElementsByClassName("a-size-base a-link-normal a-text-bold");
				my $btm_tit=$btm_tit_bk->innerText();
				$btm_tit=trim($btm_tit);
				# print "btm_tit:: $btm_tit\n";
				my $btm_lk=$btm_tit_bk->getAttribute("href");
				$btm_lk='https://www.amazon.com'.$btm_lk;
				
				my ($btm_hdr_prc,$btm_lst_prc);
				my $btm_pr_blk=$rwsub->getElementsByClassName("a-size-base a-link-normal s-no-hover a-text-normal");
				# print "btm_pr_blk:: $btm_pr_blk\n";
				if(length($btm_pr_blk) != "")
				{
					# print "Inside****\n";
					my $bnkk=$btm_pr_blk->subTree();
					
					# $btm_hdr_prc=$bnkk->getElementsByClassName("a-price")->subTree()->getElementsByClassName("a-offscreen");
					# $btm_hdr_prc=$btm_hdr_prc->innerText() if(length($btm_hdr_prc) != "");
					
					# $btm_lst_prc=$bnkk->getElementsByClassName("a-price a-text-price");
					# $btm_lst_prc=$btm_lst_prc->subTree()->getElementsByClassName("a-offscreen") if(length($btm_lst_prc) != "");
					# $btm_lst_prc=$btm_lst_prc->innerText() if(length($btm_lst_prc) != "");
					
					my @ar_pr=$bnkk->getElementsByTagName("span");
					foreach my $arr(@ar_pr)
					{
						my $ar=$arr->getAttribute("data-a-color");
						$btm_hdr_prc=$arr->subTree()->getElementsByClassName("a-offscreen")->innerText() if ($ar eq 'base');
						$btm_lst_prc=$arr->subTree()->getElementsByClassName("a-offscreen")->innerText() if ($ar eq 'secondary');
					}
					
					# print "btm_hdr_prc:: $btm_hdr_prc\n";
					# print "btm_lst_prc:: $btm_lst_prc\n";
					
				}
				
				
				
				$btm_handover_tit='Hardcover' if($btm_tit eq 'Hardcover');
				$btm_kindl_tit='Kindle' if($btm_tit eq 'Kindle');
				$btm_Paperback_tit='Paperback' if($btm_tit eq 'Paperback');
			
				$btm_handover_hdr_price=$btm_hdr_prc if($btm_tit eq 'Hardcover');
				$btm_kindl_hdr_price=$btm_hdr_prc if($btm_tit eq 'Kindle');
				$btm_Paperback_hdr_price=$btm_hdr_prc if($btm_tit eq 'Paperback');

				$btm_handover_list_price=$btm_lst_prc if($btm_tit eq 'Hardcover');
				$btm_kindl_list_price=$btm_lst_prc if($btm_tit eq 'Kindle');
				$btm_Paperback_list_price=$btm_lst_prc if($btm_tit eq 'Paperback');
				
				$btm_handover_lk=$btm_lk if($btm_tit eq 'Hardcover');
				$btm_kindl_lk=$btm_lk if($btm_tit eq 'Kindle');
				$btm_Paperback_lk=$btm_lk if($btm_tit eq 'Paperback');
				
				# print "btm_handover_hdr_price:: $btm_handover_hdr_price\n";
				# print "btm_kindl_hdr_price:: $btm_kindl_hdr_price\n";
				# print "btm_Paperback_hdr_price:: $btm_Paperback_hdr_price\n";
				# print "btm_handover_list_price:: $btm_handover_list_price\n";
				# print "btm_kindl_list_price:: $btm_kindl_list_price\n";
				# print "btm_Paperback_list_price:: $btm_Paperback_list_price\n";
				# print "btm_handover_lk:: $btm_handover_lk\n";
				# print "btm_kindl_lk:: $btm_kindl_lk\n";
				# print "btm_Paperback_lk:: $btm_Paperback_lk\n";
				
				my $end_time = DateTime->now( time_zone => "Asia/Kolkata" );
				
				open KK,">>Link_Collection.txt";
				print KK "$id\t$headr\t$btm_lk\t$btm_tit\t$btm_hdr_prc\t$btm_lst_prc\t$Home_url\t$start_time\t$end_time\n";
				close KK;
			}
			
		}

	}
	
	skpp1:
	
}


sub InnerPageCollection()
{
	my ($html2,$id,$Home_cont,$Home_url,$start_time)=@_;
	
	my($handover_hdr_price,$kindl_hdr_price,$Paperback_hdr_price,$handover_list_price,$kindl_list_price,$Paperback_list_price,$handover_lk,$kindl_lk,$Paperback_lk,$handover_isbn_10,$handover_isbn_13,$kindl_isbn_10,$kindl_isbn_13,$Paperback_isbn_10,$Paperback_isbn_13);


	my $inhrf=Page_Url($Home_cont);
	# $inhrf='https://www.amazon.com'.$inhrf;

	# print length($html2);
	# my $tit_cont=content_check('id|title');
	# my $title=DataPoints($typ_cont,'id|productTitle');

	
	my $title=cond($Home_cont,'<[^>]*?id\=\"(?:productTitle|ebooksProductTitle)\"[^>]*?>([\w\W]*?)<\/[^>]*?>');
	
	print "title:: $title\n";
	
	if ($title eq "")
	{
		open JJ,">>Error_Files.txt";
		print JJ "$id\t$Home_url\n";
		close JJ;
		
		goto skpp; 
	}
	
	
	
	my $typ_date=cond($Home_cont,'<[^>]*?id\=\"title\"[^>]*?>[\w\W]*?class\=\"a\-size\-medium\s*a\-color\-secondary\s*a\-text\-normal\">([\w\W]*?)<\/h1>');
	
	# print "typ_date:: $typ_date\n";
	my ($typ2,$pubDt2)=split('\–',$typ_date);
	
	# print "pubDt2:: $pubDt2\n";
	
	my $typ_cont=content_check($html2,'class|a-tab-heading a-active mediaTab_heading');
	
	# open KK,">>typ_cont.txt";
	# print KK"$typ_cont";
	# close KK; 
	
	my $type1=DataPoints($typ_cont,'class|a-size-large mediaTab_title') if($typ_cont ne "");
	$type1=remove_space($type1);
	# print "**** Type1:: $type1 ****\n";
	# print "typ2:: $typ2\n";
	
	my $type;
	$type=$type1 if($type1 ne "");
	$type=$typ2 if($type1 eq "");
	
	# print "**** type:: $type ****\n";
	
	my $auth_cont=content_check($html2,'id|bylineInfo');
	# print "**** authr::\n". length($auth_cont);
	
	my $authr=Auth_Info($auth_cont,'class|author notFaded');
	
	
	# print "**** authr:: $authr ****\n";
	
	# id|usedOfferAccordionRow BUY_USED
	# id|newOfferAccordionRow  BUY_NEW
	
	# id|buyOfferAccordionRow	BUY
	
	my @arid=("id|buyOfferAccordionRow","id|usedOfferAccordionRow","id|newOfferAccordionRow","id|mediaNoAccordion","class|mediaTab_content_landing","id|buybox","id|usedBuySection","class|no-kindle-offer-message","id|mediaOlp");
	
	# a-accordion-row-a11y
	# ,"id|ebookAccordion"
	
	foreach my $ids(@arid)
	{
		
		my $innerblock=content_check($html2,$ids);
		# print "ids::$ids\n";
		# print "LEN:: ".length($innerblock);<>;
		if(length($innerblock) != 0)
		{
		# print "Inn****\n";
		my $blk_soldby=DataPoints($innerblock,'id|merchant-info');
		# print "blk_soldby:: $blk_soldby\n";
		
		my $blk_soldby_buybox=DataPoints($innerblock,'id|sellerProfileTriggerId');
		
		# print "blk_soldby_buybox:: $blk_soldby_buybox\n";
		
		
		
		my $blk_soldby_2;
		
		if($blk_soldby eq "")
		{
			$blk_soldby_2=sold_by_DataPoints($innerblock,'class|a-row a-spacing-mini,class|a-row a-spacing-top-small');
			$blk_soldby_2=trim($blk_soldby_2);
		}
		# print "blk_soldby_2:: $blk_soldby_2\n";
		
		
		
		
		my $cata1=DataPoints($innerblock,'class|a-size-medium header-text');
		my $cata2=DataPoints($innerblock,'class|a-column a-span4 a-text-left a-nowrap');
		# my $cata_4=DataPoints($innerblock,'a-column a-span8');
		# my $cata_5=DataPoints($innerblock,'class|a-column a-span4');

		
		my $catagory;
		$catagory=$cata1 if($cata1 ne "");
		$catagory=$cata2 if($cata1 eq "");
		
		# print "catagory:: $catagory\n";
		# print "cata_4:: $cata_4\n";
		# print "cata_5:: $cata_5\n";
		
		# my $hedr_prc=header_price($innerblock,id|a-size-medium header-price a-color-price,class|a-size-medium header-price a-color-price');
	
		my $hedr_prc=header_price($innerblock,'class|a-size-medium header-price a-color-price,class|a-size-medium header-price a-color-secondary,class|a-size-medium a-color-price header-price,id|singleLineOlp,class|a-size-medium a-color-price offer-price a-text-normal,class|a-size-medium a-color-secondary header-price,class|a-color-base offer-price a-text-normal,class|a-size-base a-color-base a-text-normal,id|rental-header-price');
		$hedr_prc=remove_space($hedr_prc);
		
		

		# id|buyNewSection,id|unqualifiedBuyBox,id|usedBuySection,id|singleLineOlp,class|kindle-price,class|paperback-price,class|handover-price');
		
		# print "hedr_prc:: $hedr_prc\n";
		
		# my $list_prc_blk=list_price($innerblock,'class|a-color-secondary a-text-strike,class|print-list-price,id|buyBoxInner');
		
		# my $ppp=cond($innerblock,'id\=\"print\-list\-price[^>]*?>([\w\W]*?)<\/div>');
		# print "PPP:: $ppp\n";
		
		my $list_prc_blk=content_check($html2,"id|print-list-price");
		$list_prc_blk=$innerblock if($list_prc_blk eq "");
		my $lit_prc=list_price($list_prc_blk,'class|a-color-secondary a-text-strike,class|a-text-strike');
		
		
		
		# print-list-price
		$lit_prc=remove_space($lit_prc);
		# print "lit_prc:: $lit_prc\n";
		
		$handover_hdr_price=$hedr_prc if($type eq 'Hardcover');
		$kindl_hdr_price=$hedr_prc if($type eq 'Kindle');
		$Paperback_hdr_price=$hedr_prc if($type eq 'Paperback');
		
		$handover_list_price=$lit_prc if($type eq 'Hardcover');
		$kindl_list_price=$lit_prc if($type eq 'Kindle');
		$Paperback_list_price=$lit_prc if($type eq 'Paperback');
		
		$handover_lk=$inhrf if($type eq 'Hardcover');
		$kindl_lk=$inhrf if($type eq 'Kindle');
		$Paperback_lk=$inhrf if($type eq 'Paperback');
		
		#-------- ISBN COLLECTION -- TOP CORNER ----
		
		my $isbn_top_crn_blk=content_check($html2,'id|isbn_feature_div,id|printEditionIsbn_feature_div,id|productDetailsTable,id|prodDetails');
		
		my ($isbn_10_H,$isbn_13_H)=ISBN_top_crn($isbn_top_crn_blk,'class|a-row');
		
		$isbn_10_H=remove_space($isbn_10_H);
		$isbn_13_H=remove_space($isbn_13_H);
		
		# print "ISBN_10_TOP:: $isbn_10_H\n";
		# print "ISBN_13_TOP:: $isbn_13_H\n";
		
		# ------- ISBN num from Product Details ----
		
		my $isbn_10;
		my $isbn_13;
		
		my $isbn_prod_blk=content_check($html2,'id|productDetailsTable,id|prodDetails');
		
		my ($isbn_10_P,$isbn_13_P)=ISBN_Product_Details($isbn_prod_blk,'li|tr');
		
		# print "isbn_10_P:: $isbn_10_P\n";
		# print "isbn_13_P:: $isbn_13_P\n";
		

		if(($isbn_10_H eq "") && ($isbn_13_H eq ""))
		{
			$isbn_10=$isbn_10_P;
			$isbn_13=$isbn_13_P;
		}else{
			$isbn_10=$isbn_10_H;
			$isbn_13=$isbn_13_H;
		
		}
		
		# print "isbn_10:: $isbn_10\n";
		# print "isbn_13:: $isbn_13\n";f
		
		my $prodDetailsblk=content_check($html2,'id|productDetailsTable,id|prodDetails');
		
		my ($pubser,$prod_pub_dat,$prod_soldBy)=Pub_Dat_Product_Details($prodDetailsblk,'li|tr');
		
		# print "pubser:: $pubser\n";
		# print "prod_pub_dat:: $prod_pub_dat\n";
		# print "prod_soldBy:: $prod_soldBy\n";
		
		
		my $pub_dat;
		my $pubDt3=cond($pubser,'(([\d]+)([\-\./])([\d]+)([\-\./])([\d]+)|((Jan(|uary)|Feb(|ruary)|Mar(|ch)|Apr(|il)|May|Jun(|e)|Jul(|y)|Aug(|ust)|Sept(|ember)|Oct(|ober)|(Nov|Dec)(|ember))([\s\-])(|([\d]+){1,2}([\s\-]|\, ))([\d]+){4}))');
		
		$pub_dat=$pubDt2 if($pubDt2 ne "");
		$pub_dat=$pubDt3 if($pubDt2 eq "");
		$pub_dat=~s/\(//igs;
		$pub_dat=~s/\)//igs;
		
		# print "pub_dat:: $pub_dat\n";
		
		my $soldBy;
		if($blk_soldby ne "")
		{
			$soldBy=$blk_soldby;
		}elsif($blk_soldby_2 ne ""){
			$soldBy=$blk_soldby_2;
		}
		else
		{
			$soldBy=$prod_soldBy;
		}
		
		# print "soldBy:: $soldBy\n";
		
		# open KK,">SOLD.txt";
		# print KK "$soldBy";
		# close KK;
		
		if($soldBy=~m/(?:s|S)old\s*by/is)
		{
			$soldBy=cond($soldBy,'(?:s|S)old\s*by([\w\W]*?)\s*$');
		}
		
		if(($soldBy eq "") && ($blk_soldby_buybox ne ""))
		{
			$soldBy=$blk_soldby_buybox;
		}
		
		
		# print "soldBy:: $soldBy\n";
		

		my $end_time = DateTime->now( time_zone => "Asia/Kolkata" );

		my $ISBN_ID=cond($id,'([\d]+).*');
		
		
		open JJ,">>Amazon_Price_Output.txt";
		print JJ "$count\t$id\t$ISBN_ID\t$title\t$authr\t$type\t$catagory\t$hedr_prc\t$lit_prc\t$isbn_10\t$isbn_13\t$soldBy\t$inhrf\t$pub_dat\t$pubser\t$Home_url\t$type1\t$typ2\t$cata1\t$cata2\t$pubDt2\t$blk_soldby\t$blk_soldby_2\t$prod_soldBy\t$prod_pub_dat\t$isbn_10_H\t$isbn_13_H\t$isbn_10_P\t$isbn_13_P\t$start_time\t$end_time\n";
		close JJ;
		
		
		
		$count++;
			
		print "Printed\n";
		}
	# print "Printed********* \n";
	}
	
	# print "Printed###### \n";
	
	skpp:
	
	# print "PrintedOOOOO\n";
}



sub cond()
{
	my $value=shift;
	my $regex=shift;
	if($value=~m/$regex/is)
	{
		my $data=$1;
		$data=&trim($data);
		return $data;
	}
}
		
sub Page_Url()
{	
	my ($cont)=@_;
	if($cont=~m/<link\s*rel\=\"canonical\"\s*href\=\"([^>]*?)\"/is)
	{
		my $in_ref=$1;
		return $in_ref;
	}
}
		
sub id_fnm()
{
	my $con=shift;
	if($con=~m/.*\\([\w\W]*?)\.html/igs)
	{
		my $dt=$1;
		return $dt;
	}
	
}

sub Auth_Info()
{
	my ($cont,$tag)=@_;
	my $autx;
	my @ar=split('\,',$tag);
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;

		if(length($cont) != 0)
		{
		my $sub_cont=$cont->subTree();
		if(length($sub_cont) != "")
		{
			my @html=$sub_cont->getElementsByClassName($tnm) if($tg eq 'class');
			foreach my $aut(@html)
			{
				my $ac;
				
				$ac=$aut->subTree()->getElementsByClassName("a-link-normal contributorNameID");

				if (length($ac) eq "")
				{

					$ac=$aut->subTree()->getElementsByTagName("a");	
					
				}
				
				$autx=$autx.",".$ac->innerText() if(length($ac) !="");
				
				# defined($ac ? return $ob->innerText() : return "");
			}
		}
		$autx=&trim($autx);
		}
		return $autx;
	}

}

sub trim()
{
	my $v=shift;
	$v=~s/\n//igs;
	$v=~s/<[^>]*?>//igs;
	$v=~s/\s+/ /igs;
	$v=~s/^\s//igs;
	$v=~s/^\://igs;
	$v=~s/\:$//igs;
	$v=~s/^\,//igs;
	$v=~s/\,$//igs;
	$v=~s/\s$//igs;
	$v=~s/^\|//igs;
	$v=~s/\|$//igs;
	$v=~s/\&ndash\;/\-/igs;
	
	return $v;
}

sub file_to_array
{
	my $fn = shift;
	open FH,"<".$fn;
	my @wa = <FH>;
	close FH;
	return @wa;
}

sub ISBN_Product_Details()
{
	my($cont,$tag)=@_;
	my ($isbn_10,$isbn_13);
	# print "ISBN_Product_Details*** \n";
	
	chomp($tag);
	
	# print "tag:: $tag\n";
	# print "ISBN_Product_Details*** \n";
	
	my @ar=split ('\|',$tag);
	foreach my $ltg(@ar)
	{
		chomp($ltg);
		# print "ltg:: \n"; $ltg->innerText(); 
		goto kkl if(length($cont) == '0');
		my $sub_cont=$cont->subTree();
		my @tags=$sub_cont->getElementsByTagName($ltg);
		# my @tags=$sub_cont->getElementsByTagName("li");
		# print "ltg2:: $ltg2\n";
		foreach my $html(@tags)
		{
			# print "header_price LEN:: ". length($html) ."\n";
			
			my $intx=$html->innerText() if(length($html) ne "");
			$intx=&trim($intx);
			# print "ISBN_Product_Details intx:: $intx\n";
			$isbn_10=$intx if(index($intx,'ISBN-10') != -1);
			$isbn_13=$intx if(index($intx,'ISBN-13') != -1);
		
		}
		
		$isbn_10=replace($isbn_10,'ISBN-10',"");
		$isbn_10=replace($isbn_10,':',"");
		$isbn_13=replace($isbn_13,'ISBN-13',"");
		$isbn_13=replace($isbn_13,':',"");
		
		$isbn_10=remove_space($isbn_10);
		$isbn_13=remove_space($isbn_13);
		
		# print "isbn_10:: $isbn_10\n";
		# print "isbn_13***:: $isbn_13\n";
		
		last if(($isbn_10 ne "") || ($isbn_13 ne ""));
		
	}
	kkl:
	return ($isbn_10,$isbn_13);

}

sub Pub_Dat_Product_Details()
{
	my($cont,$tag)=@_;
	my ($isbn_10,$isbn_13,$soldBy);
	# print "ISBN_Product_Details*** \n";
	
	chomp($tag);
	# print "tag:: $tag\n";
	# print "ISBN_Product_Details*** \n";
	my @ar=split ('\|',$tag);
	foreach my $ltg(@ar)
	{
		chomp($ltg);
		# print "ltg:: $ltg\n";
		goto kkl if(length($cont) == '0');
		my $sub_cont=$cont->subTree();
		
		# print $cont->innerText();
		
		# print "ltg2:: ".length($sub_cont);
		
		# my @tags=$sub_cont->getElementsByTagName($ltg);
		my @tags=$sub_cont->getElementsByTagName("li");
		# print "ltg2:: \n" . length(@tags);
		foreach my $html(@tags)
		{
			# print "header_price LEN:: ". length($html) ."\n";
			
			my $intx=$html->innerText() if(length($html) ne "");
			$intx=&trim($intx);
			# print "ISBN_Product_Details intx:: $intx\n";
			$isbn_10=$intx if(index($intx,'Publisher:') != -1);
			$isbn_13=$intx if(index($intx,'Publication Date:') != -1);
			$soldBy=$intx if(index($intx,'Sold by:') != -1);
		}
		
		$isbn_10=replace($isbn_10,'Publisher:',"");
		$isbn_13=replace($isbn_13,'Publication Date:',"");
		$soldBy=replace($soldBy,'Sold by:',"");

		$isbn_10=remove_space($isbn_10);
		$isbn_13=remove_space($isbn_13);
		$soldBy=remove_space($soldBy);
		
		# last if(($isbn_10 ne "") || ($isbn_13 ne ""));
	}
	kkl:
	return ($isbn_10,$isbn_13,$soldBy);

}



sub ISBN_top_crn()
{

	my($cont,$tag)=@_;
	my ($isbn_10,$isbn_13);
	# print "ISBN_top_crn****\n";
	my @ar=split('\,',$tag);
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my @html;
		
		# print "ISBN_top_crn TG:: $tg\n";
		# print "ISBN_top_crn tnm:: $tnm\n";
		
		
		# print length($cont);
		goto kkl if(length($cont) == '0');
		
		my $sub_cont=$cont->subTree();
		@html=$sub_cont->getElementById($tnm) if($tg eq 'id');
		@html=$sub_cont->getElementsByClassName($tnm) if($tg eq 'class');
		
		# print "ISBN_top_crn LEN:: ". length(@html) ."\n";
		if(length(@html) != "")
		{
			foreach my $html(@html) 
			{
				my $intx=$html->innerText() if(length($html) ne "");
				$intx=&trim($intx);
				# print "ISBN_top_crn intx:: $intx\n";
				$isbn_10=$intx if(index($intx,'ISBN-10') != -1);
				$isbn_13=$intx if(index($intx,'ISBN-13') != -1);
			}
		}
		$isbn_10=replace($isbn_10,'ISBN-10:',"");
		$isbn_13=replace($isbn_13,'ISBN-13:',"");
		
		$isbn_10=remove_space($isbn_10);
		$isbn_13=remove_space($isbn_13);
	}
	kkl:
	return ($isbn_10,$isbn_13);
}



sub header_price()
{
	my($cont,$tag)=@_;
	my @ar=split('\,',$tag);
	
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;
		
		# print "header_price TG:: $tg\n";
		# print "length tnm:: $tnm\n";
		# print "length tnm::\n".length($cont);
		if(length($cont) != 0)
		{
			my $sub_cont=$cont->subTree();
			
			$html=$sub_cont->getElementById($tnm) if($tg eq 'id');
			$html=$sub_cont->getElementsByClassName($tnm) if($tg eq 'class');
			return &trim($html->innerText()) if(length($html) ne "");
		# print "header_price LEN:: ". length($html) ."\n";
		}
		
		
	}
	
	
}


sub DataPoints()
{
	my($cont,$tag)=@_;
	
	# open JJ,">CC.txt";
	# print JJ $cont->innerText();
	# close JJ;
	# defined($cont ? $cont->innerText() : "");
	
	# print $cont;
	# print "tag:: $tag\n";<>;
	# print "Content\n";
	
	my @ar=split('\,',$tag);
	# print $#ar;<>;
	foreach my $ss(@ar)
	{
	
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;
		
		# print "tg:: $tg\n";
		# print "tnm:: $tnm\n";
		
		# $cont=defined($cont ? $cont->innerText() : "");
		# print "DATYA" . length($cont);<>;
		
		if(length($cont) != '0')
		{	
			# print "qqqqqqqqqqqqq\n";
			my $sub_cont=$cont->subTree();
			
			
			if(length($sub_cont) != ""){
			# print "_________pppp_____\n";
				$html=$sub_cont->getElementById($tnm) if($tg eq 'id');
				$html=$sub_cont->getElementsByClassName($tnm) if($tg eq 'class');
			}else{
				
				# print "______________\n";
				$html=$cont->getElementById($tnm) if($tg eq 'id');
				
				$html=$cont->getElementsByClassName($tnm) if($tg eq 'class');
			}
		
		}
		return &trim($html->innerText()) if(length($html) ne "");
	}
}



sub sold_by_DataPoints()
{
	my($cont,$tag)=@_;
	my @ar=split('\,',$tag);
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;
		# print "header_price TG:: $tg\n";
		# print "length tnm:: $tnm\n";
		
		
		# print "DATA" . length($cont);
		if(length($cont) != "")
		{
			my $sub_cont=$cont->subTree();
			if(length($sub_cont) != 0)
			{
				my @htmlar;
				@htmlar=$sub_cont->getElementById($tnm) if($tg eq 'id');
				@htmlar=$sub_cont->getElementsByClassName($tnm) if($tg eq 'class');
				# print "htmlar:: @htmlar\n";
				# print $#htmlar;
				foreach my $hh(@htmlar)
				{
					# my $hx=$hh->innerText();
					
					# my $hx=defined($hh ? return $hh->innerText() : return "");
					my $hx=defined($hh ? $hh->innerText() : "");
					
					# print "HXX:: $hx\n";
					my $hx=$hh->innerText();
					$hx=lc($hx);
					my $hx1=$hx if(index($hx,'sold by:') != -1);
					my $hx2=$hx if(index($hx,'sold by') != -1);
					$html=$hx1 if($hx1 ne "");
					return &trim($html) if(length($html) ne "");
					# goto nnk if($hx1 ne "");
					$html=$hx2 if($hx2 ne "");
					return &trim($html) if(length($html) ne "");
					# goto nnk if($hx2 ne "");
				}
			}else{
			
				$html=$cont->getElementById($tnm) if($tg eq 'id');
				$html=$cont->getElementsByClassName($tnm) if($tg eq 'class');
			}
		
		}
	
		return &trim($html->innerText()) if(length($html) ne "");
	}
}





sub list_price()
{
	my($cont,$tag)=@_;
	my @ar=split('\,',$tag);
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;
		
		# print length($cont);
		# print "list_price TG:: $tg\n";
		# print " list_price tnm:: $tnm\n";
		
		if(length($cont) != 0)
		{
			my $sub_cont=$cont->subTree();
			# my $sub_cont=$cont;

			$html=$sub_cont->getElementById($tnm) if($tg eq 'id');
			$html=$sub_cont->getElementsByClassName($tnm) if($tg eq 'class');
			
			# print "list_price LEN:: ". length($html) ."\n";
		}
		return $html->innerText() if(length($html) ne "");
	}
}

sub content_check()
{
	my ($cont,$tag)=@_;
	
	# print "CONT:: $cont\n";
	
	my @ar=split('\,',$tag);
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;
		
		# print "TG:: $tg\n";
		# print "tnm:: $tnm\n";
		
		# print "LEN:: ". length($cont) ."\n";

		$html=$cont->getElementById($tnm) if($tg eq 'id');
		$html=$cont->getElementsByClassName($tnm) if($tg eq 'class');
		
		return $html if(length($html) != "");
	}
}


sub remove_space
{
	my $str = shift;
	my $fs = " ";
	my $ls = " ";
	my $c = -1;
	my $l = length($str);
	while ($ls eq " ")
	{
		$c++;
		$ls = substr($str,$l-($c+1),1);
	}
	my $strn = substr($str,0,($l-$c));	
	$l = length($strn);
	$c = -1;
	while ($fs eq " ")
	{
		$c++;
		$fs = substr($strn,$c,1);
	}
	$strn = substr($strn,$c,($l-$c));
	return $strn;	
}

sub replace
{
	my $str=shift;
	my $find=shift;
	my $replace=shift;
	my $pos=index($str,$find);
	while($pos != -1)
	{
	  substr($str,$pos,length($find),$replace);
	  $pos=index($str,$find,$pos+length($replace));
	}
	return $str;
}

=e
sub getcont()
{
    my($ur,$cont,$ref,$method,$pagename,$id)=@_;
    netfail:
	sleep(10);
	
	#---- Proxy Setting ------------
	
	# print("**$proxy*");
	# print("**welcome to new  $proxy*\n");
	my $proxy;
	$proxy=proxy();
	
	$proxy=~s/@/\@/igs;
	print("**welcome to new  $proxy*\n");
	$ua->proxy(['https','http'] => $proxy);
	
	#-----------------------------------
	
    my $request=HTTP::Request->new("$method"=>$ur);
    #$request->header(":authority"=>"mipel.com");
    #$request->header("User-Agent"=>"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
    #$request->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
   
    
    if($ref ne '')
    {
   
        $request->header("Referer"=>"$ref");
    }
    if(lc $method eq 'post')
    {
   
        $request->content($cont);
    }
    my $res=$ua->request($request);
    $cookie_jar->extract_cookies($res);
    $cookie_jar->save;
    $cookie_jar->add_cookie_header($request);
    my $code=$res->code;
    print"\n $code";
    if($code==200)
    {
       
		my $content=$res->content();
		if($content=~m/1996\s*\-\s*2019\s*\,\s*Amazon\s*\.\s*com/is)
		{
			write_file($content,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t200\n";
			my $pagename1="FileCRT".".txt";
			write_file($b,$pagename1,">>$ss/");
			return $content;
			# goto netfail;
			
		}
		else
		{
			write_file($content,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t200\n";
			my $pagename1="FileERR".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
		}		
    }
    elsif($code=~m/50/is)
    {
        my $content=$res->content();
		if($content=~m/1996\s*\-\s*2019\s*\,\s*Amazon\s*\.\s*com/is)
		{
			write_file($content,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t500\n";
			my $pagename1="FileCRT".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
			return $content;
		}
		else
		{
			write_file($content,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t500\n";
			my $pagename1="FileERR".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
		}		
    }
    elsif($code=~m/30/is)
    {
       
        my $loc=$res->header("Location");
         #print "\nLocation: $loc";
        my $request1=HTTP::Request->new(GET=>$loc);
        $request1->header("Content-Type"=>"application/x-www-form-urlencoded");
        my $res1=$ua->request($request1);
        $cookie_jar->extract_cookies($res1);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request1);
        my $content1=$res1->content();
        if($content1=~m/1996\s*\-\s*2019\s*\,\s*Amazon\s*\.\s*com/is)
		{
			write_file($content1,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t300\n";
			my $pagename1="FileCRT".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
			return $content1;
		}
		else
		{
			write_file($content1,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t300\n";
			my $pagename1="FileERR".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
		}		

    }
    elsif($code=~m/40/is)
    {
         my $content=$res->content();
		#$content=decode_entities($content);
		if($content=~m/1996\s*\-\s*2019\s*\,\s*Amazon\s*\.\s*com/is)
		{
			write_file($content,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t400\n";
			my $pagename1="FileCRT".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
			return $content;
		}
		else
		{
			write_file($content,"Test.html",">");
			my $b="$id\t$pagename\t$ur\t$proxy\t400\n";
			my $pagename1="FileERR".".txt";
			write_file($b,$pagename1,">>$ss/");
			goto netfail;
		}		
    }
}	

sub write_file
{
    my $con = shift;
    my $fname = shift;
    my $mode = shift;
    open FD,$mode.$fname;
    print FD $con;
    close FD;
}


sub proxy()
{
	if(($proxycount%10 == 0))
	{
		$proxycount=1;
	}
	else
	{
		$proxycount++;
	}
	print("proxy count ::$proxycount\n");
	my $a=$inp[$proxycount-1];
	# print("Proxy $proxycount");
	$a=~s/\s+//igs;
	return $a;
}
