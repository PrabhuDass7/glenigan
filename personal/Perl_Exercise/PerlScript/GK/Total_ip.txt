Brand	Item	Ref_Links
SMARTPHONES	Samsung S20 | S20+ | S20 Ultra	https://uae.sharafdg.com/samsung-galaxy-s20/
SMARTPHONES	iPhone 11 & 11 Pro	https://uae.sharafdg.com/iphone-11/
SMARTPHONES	Samsung Galaxy Note 10	https://uae.sharafdg.com/buy-note-10/
SMARTPHONES	Huawei Mate 30 Pro	https://uae.sharafdg.com/huawei-mate-30-pro/
SMARTPHONES	iPhone XS & iPhone XS Max	https://uae.sharafdg.com/iphone-xs/
SMARTPHONES	Oppo Reno	https://uae.sharafdg.com/oppo-reno/
SMARTPHONES	Huawei P30 Series	https://uae.sharafdg.com/huawei-p30-pro/
SMARTPHONES	iPhone XR	https://uae.sharafdg.com/iphone-xr/
SMARTPHONES	iPhone X	https://uae.sharafdg.com/iphone-x/
SMARTPHONES	Mid-Range Smartphones	https://uae.sharafdg.com/mid-range-smartphones/
TABLETS	The New iPad Pro	https://uae.sharafdg.com/ipad-pro-2018/
TABLETS	iPads	https://uae.sharafdg.com/ipads/
TABLETS	Samsung Tabs	https://uae.sharafdg.com/samsung-tablets/
TABLETS	Lenovo	https://uae.sharafdg.com/lenovo-tablets/
TABLETS	Microsoft Surface Pro	https://uae.sharafdg.com/microsoft-tablet/
TABLETS	Huawei MediaPad	https://uae.sharafdg.com/?q=Huawei%20MediaPad&#038;post_type=product
ACCESSORIES	USB C Connectors and Cables	https://uae.sharafdg.com/c/mobiles_tablets/mobile_accessories/mobile_cables/
ACCESSORIES	Screen Protectors	https://uae.sharafdg.com/c/mobiles_tablets/mobile_screen_guards/
ACCESSORIES	Power Banks	https://uae.sharafdg.com/c/mobiles_tablets/mobile_accessories/mobile_power_banks/
ACCESSORIES	Memory Cards	https://uae.sharafdg.com/c/mobiles_tablets/mobile_memory_cards/
ACCESSORIES	Earphones & Headphones	https://uae.sharafdg.com/c/tv_video_audio/audio/earphones_headphones/
ACCESSORIES	More	https://uae.sharafdg.com/c/mobiles_tablets/mobile_accessories/
BY BRANDS	iPhones	https://uae.sharafdg.com/buy-iphone/
BY BRANDS	Samsung	https://uae.sharafdg.com/samsung-smartphones/
BY BRANDS	Huawei	https://uae.sharafdg.com/huawei-smartphones/
BY BRANDS	Honor	https://uae.sharafdg.com/honor-mobile-phone/
BY BRANDS	Nokia	https://uae.sharafdg.com/c/mobiles_tablets/mobiles/?dFR%5Btaxonomies.attr.Brand%5D%5B0%5D=Nokia
WEARABLES	Apple Watch	https://uae.sharafdg.com/buy-apple-watch/
WEARABLES	Samsung Watch	https://uae.sharafdg.com/c/wearables_smartwatches/smartwatches/
WEARABLES	Huawei Watch	https://uae.sharafdg.com/?q=Huawei%20Watch&#038;dFR%5Btaxonomies.product_cat%5D%5B0%5D=Wearables%20%26%20Smartwatches&#038;post_type=product
WEARABLES	Fitness Trackers	https://uae.sharafdg.com/c/wearables_smartwatches/fitness_trackers/
WEARABLES	Smartwatches	https://uae.sharafdg.com/?q=smartwatch&#038;post_type=product
WEARABLES	Budget Smartwatches	https://uae.sharafdg.com/budget-smartwatches/
TELEVISIONS	8K Televisions	https://uae.sharafdg.com/8k-televisions/
TELEVISIONS	4K UHD	https://uae.sharafdg.com/4k-uhd-tvs/
TELEVISIONS	QLED TVs	https://uae.sharafdg.com/?q=oled&#038;dFR%5Btaxonomies.product_cat%5D%5B0%5D=TVs&#038;post_type=product
TELEVISIONS	OLED TVS	https://uae.sharafdg.com/?dFR%5Btaxonomies.attr.Type%5D%5B0%5D=4K%20OLED&#038;dFR%5Btaxonomies.product_cat%5D%5B0%5D=TVs&#038;post_type=product
TELEVISIONS	65" and above	https://uae.sharafdg.com/xx-large-televisions/
TELEVISIONS	55" to 65"	https://uae.sharafdg.com/xlarge-televisions/
LAPTOPS	Macbook	https://uae.sharafdg.com/macbooks/
LAPTOPS	Microsoft Surface	https://uae.sharafdg.com/microsoft-laptops/
LAPTOPS	Gaming Laptops	https://uae.sharafdg.com/gaming-laptops/
LAPTOPS	Modern Laptops	https://uae.sharafdg.com/inking-hello-and-touch/
LAPTOPS	HP	https://uae.sharafdg.com/hp-laptops/
LAPTOPS	Dell	https://uae.sharafdg.com/dell-laptops/
LAPTOPS	Lenovo	https://uae.sharafdg.com/lenovo-laptops/
LAPTOPS	Asus	https://uae.sharafdg.com/asus-laptops/
LAPTOPS	Acer	https://uae.sharafdg.com/acer-laptops-shop/
CAMERAS	DSLRs	https://uae.sharafdg.com/c/cameras_camcorders/digital_cameras/
CAMERAS	Digital Cameras	https://uae.sharafdg.com/c/cameras_camcorders/digital_cameras/
CAMERAS	Instant Cameras	https://uae.sharafdg.com/c/cameras_camcorders/instant_cameras/
CAMERAS	Security and Surveillance	https://uae.sharafdg.com/c/home_appliances/smart_home_security/security_cameras_surveillance/
CAMERAS	Accessories	https://uae.sharafdg.com/c/cameras_camcorders/camera_accessories/
CAMERAS	Camera Lenses	https://uae.sharafdg.com/c/cameras_camcorders/camera_accessories/digital_camera_lenses/
CAMERAS	Camera Bags	https://uae.sharafdg.com/c/cameras_camcorders/camera_accessories/camera_bags_cases/
VIDEO GAMES	PS4 Games	https://uae.sharafdg.com/c/gaming/games/sony_ps4_games/
VIDEO GAMES	Xbox One Games	https://uae.sharafdg.com/c/gaming/games/xbox_one_games/
VIDEO GAMES	All Games	https://uae.sharafdg.com/c/gaming/games/
VIDEO GAMES	Consoles	https://uae.sharafdg.com/c/gaming/gaming_consoles/
VIDEO GAMES	Game Enhancers	https://uae.sharafdg.com/c/gaming/gaming_accessories/game_enhancers/
HOME AUDIO & VIDEO	Speakers	https://uae.sharafdg.com/?q=speaker&#038;dFR%5Btaxonomies.product_cat%5D%5B0%5D=TV%2C%20Video%20%26%20Audio&#038;post_type=product
HOME AUDIO & VIDEO	Sound bars	https://uae.sharafdg.com/c/tv_video_audio/home_cinema_soundbars/soundbars/
HOME AUDIO & VIDEO	Home Theatres	https://uae.sharafdg.com/?q=Home%20Theatre&#038;dFR%5Btaxonomies.product_cat%5D%5B0%5D=Audio&#038;post_type=product
HOME AUDIO & VIDEO	Projectors	https://uae.sharafdg.com/c/tv_video_audio/projectors/
HOME AUDIO & VIDEO	DVD and Blu-ray players	https://uae.sharafdg.com/c/tv_video_audio/blu_ray_dvd_players/
HOME AUDIO & VIDEO	Noice Cancellation Headphones	https://uae.sharafdg.com/c/tv_video_audio/audio/earphones_headphones/?dFR%5Btaxonomies.attr.Noise%20Cancellation%20Headphone%5D%5B0%5D=Yes
HOME AUDIO & VIDEO	Wireless headphones and earphones	https://uae.sharafdg.com/c/tv_video_audio/audio/earphones_headphones/?q=wireless
HOME AUDIO & VIDEO	TV Accessories	https://uae.sharafdg.com/c/tv_video_audio/tv_accessories/
IT EQUIPMENT	Printers & Inks	https://uae.sharafdg.com/c/computing/printers_ink/
IT EQUIPMENT	Monitors	https://uae.sharafdg.com/c/computing/monitors/
IT EQUIPMENT	Routers	https://uae.sharafdg.com/c/computing/networking_wireless/wireless_routers/
IT EQUIPMENT	Software	https://uae.sharafdg.com/c/computing/storage/software/
IT EQUIPMENT	Warranties	https://uae.sharafdg.com/return-exchange-and-warranty/
IT EQUIPMENT	Hard Drives	https://uae.sharafdg.com/c/computing/storage/
CAMCORDERS	Action Camcorders	https://uae.sharafdg.com/c/cameras_camcorders/camcorders/action_camcorders/
CAMCORDERS	Camcorders	https://uae.sharafdg.com/c/cameras_camcorders/camcorders/traditional_camcorders/
