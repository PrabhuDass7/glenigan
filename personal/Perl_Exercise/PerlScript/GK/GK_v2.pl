use strict;
use LWP::UserAgent;
use URI::URL;
use HTTP::Cookies;
use Cwd qw(abs_path);
use File::Basename;
use LWP::Simple;
use WWW::Mechanize;
use IO::Socket::SSL qw();
use File::stat;
use Mozilla::CA;
use Cwd;
use DateTime;
use File::Path 'rmtree';
use filehandle;
use HTML::Entities;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use File::Basename;
use JSON;


my $dir = getcwd;
my $HTML_Folder="$dir/HTML_Files";
my $Op_Folder="$dir/Output_Files";

&createdirectory($HTML_Folder);
&createdirectory($Op_Folder);

# my $heads="Count\tPost_ID\tPost_Title\tPermaLink\tPrice\tRegular_Price\tModel\tRatingValue\tReviewCount\tSKU\tSold_By\tBrand\tProd_Color\tDual_SIM\tOS\tOS_Version\tStandards\tTechnology\t2G\t3G\t4G\tSpeed\tGPRS\tEDGE\tSIM_Card\tScreen_Size\tResolution\tType\tMultitouch\tChipset\tCPU_Details\tCPU\tGPU\tSensors\tMessaging\tBrowser\tJava\tInternal_Memory\tRAM\tCard_Slot\tRear_Front_Camera\tFront_Camera\tVideo_Resolution\tAlert_Type\tLoudspeaker\tjack\tWiFi\tBluetooth\tNFC\tUSB\tRadio\tGPS\tBattery_Capacity\tUsage_Time\tBattery_Type\tDimensions\tWeight\tRelease_Date\tRecommended_Accessories\n";
		
# open JJ,">Output.txt";
# print JJ "$heads";
# close JJ;

my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);               
$ua->cookie_jar($cookie_jar);
$cookie_jar->save;

my $Count=0;


open KK,"<Input.txt";
my @inputs=<KK>;

foreach my $input(@inputs)
{
	chomp($input);
	
	print "Input:: $input\n";
	my $Item=$input;
	
	$input=uri_escape($input);
	
	my @prod_catgs;
	my $cat_temp="True";
	my $Catagory_c=0;
	my $Catagory="";
	# $prod_catgs[$Catagory_c]="";
	
	# my $Document_Url="https://uae.sharafdg.com/?q=$input&dFR[taxonomies.product_cat][0]=Mobiles&post_type=product";
	my $Document_Url="https://uae.sharafdg.com/?q=$input&post_type=product";
	
	my $Accept_Host="9khjlg93j1-dsn.algolia.net";
	my $Content_Type="text/html; charset=UTF-8";
	# my $Content_Type="application/x-www-form-urlencoded";
	# my $Referer="https://uae.sharafdg.com/c/mobiles_tablets/mobiles/";
	my $Referer="";

	my ($Content,$Code,$Redir_Url) = &Getcontent($Document_Url,"",$Accept_Host,$Content_Type,$Referer,"","");

	open HH,">$HTML_Folder/$Item"."_"."Main_Content.html";
	print HH "$Content";
	close HH;

	print "Content Completed";

	my $api_key=$1 if($Content=~m/\"search\_api\_key\"\:\s*\"([^>]*?)\"/is);
	my $application_id=$1 if($Content=~m/\"application_id\"\:\s*\"([^>]*?)\"/is);
	my $instock=$1 if($Content=~m/\"instock\"\:\s*\"([^>]*?)\"/is);
	
	my $Json_lk="https://$application_id-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%203.24.9%3BJS%20Helper%202.23.2&x-algolia-application-id=$application_id&x-algolia-api-key=$api_key";

	nxt_Catagory:
	
	my $current_Page_no=0;
	my $head_print_temp="True";
	my @head_check_old;
	my $Catagory2;
	
	next_Navigate:
	
	print "prod_catgs:: $Catagory\n";
	print "Document_Url:: $Document_Url\n";
	
	my $Accept_Post_Content_Tmp='{"requests":[{"indexName":"products_index","params":"query='."$input".'&hitsPerPage=24&maxValuesPerFacet=20&page='."$current_Page_no".'&attributesToRetrieve=permalink%2Cpermalink_ar%2Cpost_id%2Cpost_title%2Cpost_title_ar%2Cmain_sku%2Ctaxonomies.taxonomies_hierarchical.product_cat%2Ctaxonomies.product_brand%2Cdiscount%2Cdiscount_val%2Cimages%2Cprice%2Csku%2Cpromotion_offer_json%2Cregular_price%2Csale_price%2Cin_stock%2Ctags%2Crating_reviews%2Ctags_ar&attributesToHighlight=post_title%2Cpost_title_ar&getRankingInfo=1&filters=post_status%3Apublish%20AND%20price%3E0%20AND%20in_stock%3A1%20AND%20ee%3A1%20AND%20archive%3A0&facets=%5B%22tags.title%22%2C%22taxonomies.attr.Brand%22%2C%22price%22%2C%22rating_reviews.rating%22%2C%22taxonomies.attr.Processor%22%2C%22taxonomies.attr.Screen%20Size%22%2C%22taxonomies.attr.Internal%20Memory%22%2C%22taxonomies.attr.Type%22%2C%22taxonomies.attr.Storage%20Size%22%2C%22taxonomies.attr.RAM%22%2C%22taxonomies.attr.Graphics%20Card%22%2C%22taxonomies.attr.Color%22%2C%22taxonomies.attr.OS%22%2C%22taxonomies.attr.Size%22%2C%22taxonomies.attr.Megapixel%22%2C%22taxonomies.attr.Capacity%22%2C%22taxonomies.attr.Loading%20Type%22%2C%22taxonomies.attr.Tonnage%22%2C%22taxonomies.attr.Compressor%20Type%22%2C%22taxonomies.attr.Star%20Rating%22%2C%22taxonomies.attr.Energy%20input%22%2C%22taxonomies.attr.Water%20Consumption%22%2C%22taxonomies.attr.Control%20Type%22%2C%22taxonomies.attr.Number%20of%20Channels%22%2C%22taxonomies.attr.Audio%20Output%22%2C%22taxonomies.attr.No%20of%20Burners%2FHobs%22%2C%22taxonomies.attr.HDMI%22%2C%22taxonomies.attr.Total%20Capacity%22%2C%22taxonomies.attr.Number%20of%20Doors%22%2C%22taxonomies.attr.Watches%20For%22%2C%22taxonomies.attr.Display%20Type%22%2C%22taxonomies.attr.Water%20Resistant%20Depth%20(m)%22%2C%22taxonomies.attr.Power%20Source%22%2C%22taxonomies.attr.Net%20Content%22%2C%22taxonomies.attr.Target%20Group%22%2C%22taxonomies.attr.Fragrance%20Type%22%2C%22taxonomies.attr.Noise%20Cancellation%20Headphone%22%2C%22taxonomies.attr.Built-in%20%2F%20Free-standing%22%2C%22taxonomies.attr.Built%20In%20%2F%20Free%20Standing%22%2C%22taxonomies.attr.Mode%22%2C%22taxonomies.attr.Series%22%2C%22taxonomies.attr.PEGI%2FESRB%22%2C%22taxonomies.attr.Lens%20Color%22%2C%22taxonomies.attr.Lens%20Type%22%2C%22taxonomies.attr.Frame%20Material%22%2C%22taxonomies.attr.Frame%20Shape%22%2C%22taxonomies.attr.Focal%20Length%20Range%22%2C%22taxonomies.attr.Aperture%20Range%22%2C%22taxonomies.attr.Filter%20Thread%22%2C%22taxonomies.attr.Effective%20Megapixel%22%2C%22taxonomies.attr.Resolution%22%2C%22taxonomies.attr.Aspect%20Ratio%22%2C%22taxonomies.attr.Functions%22%2C%22taxonomies.attr.Print%20Technology%22%2C%22taxonomies.attr.Scanning%20speed%20(color)%22%2C%22taxonomies.attr.Supported%20Memory%20Type%22%2C%22taxonomies.attr.Seating%20Capacity%22%2C%22taxonomies.attr.Unit%20Components%20Count%22%2C%22taxonomies.attr.Battery%20Capacity%22%2C%22taxonomies.attr.USB%20Output%22%2C%22taxonomies.attr.Unit%20Component%22%2C%22taxonomies.attr.Toys%20Type%22%2C%22taxonomies.attr.Fitness%20Equipments%20Type%22%2C%22taxonomies.attr.Miscellaneous%20Type%22%2C%22taxonomies.attr.Cookware%20Type%22%2C%22taxonomies.attr.Outdoor%20Products%20Type%22%2C%22taxonomies.attr.Tools%20Type%22%2C%22taxonomies.attr.Ideal%20For%22%2C%22taxonomies.attr.Age%20Group%22%2C%22taxonomies.taxonomies_hierarchical.product_cat.lvl0%22%5D&tagFilters=&facetFilters=%5B%5B%22taxonomies.product_cat%3A'."$Catagory".'%22%5D%5D"},{"indexName":"products_index","params":"query='."$input".'&hitsPerPage=1&maxValuesPerFacet=20&page=0&attributesToRetrieve=%5B%5D&attributesToHighlight=%5B%5D&getRankingInfo=1&filters=post_status%3Apublish%20AND%20price%3E0%20AND%20in_stock%3A1%20AND%20ee%3A1%20AND%20archive%3A0&attributesToSnippet=%5B%5D&tagFilters=&analytics=false&facets=taxonomies.product_cat"}]}';

	my ($Post_Responce,$Post_Res_code)=&Post_Method($Json_lk,$Accept_Host,$Content_Type,$Document_Url,$Accept_Post_Content_Tmp,"");
			
	open(DC,">$HTML_Folder/$Item"."_Post_Responce_$current_Page_no.html");
	print DC $Post_Responce;
	close DC;

	print "*******\n";
	
	if(($Post_Responce=~m/\{\"taxonomies\.product\_cat\"\:\{([^>]*?)\}/is) && ($cat_temp eq "True"))
	{
		my $catg_blk=$1;
		
		print "*** Collecting Product Catagory ***\n";
		
		while($catg_blk=~m/\"([^>]*?)\"\:/igs)
		{
			print "prod_catgs:: $1\n";
			push(@prod_catgs,$1);
			$cat_temp="False";
		}
		
		$Catagory=uri_escape($prod_catgs[$Catagory_c]);
		$Catagory2=$prod_catgs[$Catagory_c];
		
		$Document_Url="https://uae.sharafdg.com/?q=$input&dFR[taxonomies.product_cat][0]=$Catagory&post_type=product";
		
		print "prod_catgs:: $Catagory\n";
		goto nxt_Catagory if($Catagory2 ne "");
	}
	
	my $file_name;
	if($Catagory2 ne "")
	{
		$file_name=$Item."_".$Catagory2;
	}else{
		$file_name=$Item;
	}	
	
	my $File_Location = "$Op_Folder/$file_name"."_Output.txt";
	
	($head_print_temp,my $head_check_old)=&Data_Extractions_New($Post_Responce,$Count,$head_print_temp,\@head_check_old,$File_Location);
	@head_check_old=@$head_check_old;
	
	#----- Next Pagination ------------------------------------------------
	
	if($Post_Responce=~m/\,\"page\"\:\s*([\d]+)\s*\,\"nbPages\"\:([\d]+)\,/is)
	{
		$current_Page_no=$1;
		my $Total_Page=$2;
		print "*** Current Page :: $current_Page_no\n";
		print "*** Total Page :: $Total_Page\n";
		if($current_Page_no == $Total_Page-1)
		{
			print "*** Completed ***";
			$Catagory_c++;
			print "Catagory_c :: $Catagory_c\n";
			$Catagory=uri_escape($prod_catgs[$Catagory_c]);
			
			$Catagory2=$prod_catgs[$Catagory_c];
			
			$Document_Url="https://uae.sharafdg.com/?q=$input&dFR[taxonomies.product_cat][0]=$Catagory&post_type=product";
			
			
			# &Add_Headers($File_Location,\@head_check_old);		
			
			goto nxt_Catagory;
			
		}else{
			
			$current_Page_no++;
			
			print "*** Next Page $current_Page_no ***\n";
			goto next_Navigate;
		}
	}
	
}

sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}

sub Add_Headers()
{
	my $File_Location=shift;
	my $headers=shift;
	
	print "*** Welcome Headers Add Function ***\n";
	
	my @headers=@$headers;
	
	open KK,"<$File_Location";
	my @FILE=<KK>;
	close KK;

	my $FILE="@FILE";

	# print "FILE:: $FILE\n";

	my $join=join("\t",@headers);

	$FILE=~s/<UAE_Headers_UAE>/$join/igs;
	
	# $FILE=~s/(^Count\t.*)/$1$join/igs;

	# print "FILE:: $FILE\n";

	open KK,">$File_Location";
	print KK "$FILE";
	close KK;

	print "*** Headers Addting is Completed ***\n";
}


sub Data_Extractions_New()
{
	my $Post_Responce=shift;
	my $Count=shift;
	my $head_print_temp = shift;
	my $head_check_old= shift;
	my $File_Location= shift;
	
	print "File_Location:: $File_Location\n";
	
	my @head_check_old=@$head_check_old;
	
	while($Post_Responce=~m/(\{\"post\_id\"[\w\W]*?\}\}\,)/igs)
	{
		my $Block=$1;
		my $postID=cond($Block,'\"post\_id\"\:([\w\W]*?)\,\"');
		my $post_title=cond($Block,'\"post_title\"\:\"([\w\W]*?)\"\,');
		my $permalink=cond($Block,'\"permalink\"\:\"([\w\W]*?)\"\,');
		my $price=cond($Block,'\"price\"\:([\w\W]*?)\,');
		my $regular_price=cond($Block,'\"regular_price\"\:([\w\W]*?)\,');
		
		$Count++;
		
		print "Count:: $Count\n";
		print "postID:: $postID\n";
		print "post_title:: $post_title\n";
		print "permalink:: $permalink\n";
		print "price:: $price\n";
		
		my ($Content,$Code,$Redir_Url) = &Getcontent($permalink,"","","","","","");
		
		sleep(5);
		
		open(DC,">Inner_Page.html");
		print DC $Content;
		close DC;

		print "***  Inner_Page  ****\n";
		
		#------------------Main Product Details ------------------------------------	
		
		
		my $inn_Blk=$1 if($Content=~m/<div\s*class\=\"quick\-right\">([\w\W]*?<[^>]*?pdp\-footer\s*clearfix[^>]*?>[\w\W]*?<\/a>)/is);
		
		my $Model=cond($inn_Blk,'<[^>]*?>\s*Model\:\s*<[^>]*?>([\w\W]*?)<\/p>');
		my $ratingValue=cond($inn_Blk,'<[^>]*?\"product\-rating\-count\"[^>]*?\"ratingValue\">([^>]*?)<[^>]*?>');
		my $reviewCount=cond($inn_Blk,'<[^>]*?\"review-details in-block reviewCount\">[\w\W]*?reviewCount[^>]*?>([^>]*?)<[^>]*?>');
		my $SKU=cond($inn_Blk,'<[^>]*?>\s*SKU\:\s*<[^>]*?>([\w\W]*?)<\/p>');
		my $Sold_By=cond($inn_Blk,'<span\s*class\=\"sold\-by\">\s*Sold\s*by\:([\w\W]*?)<\/span>');
		
		my $Title1="Count\tPostID\tPost_Title\tPermaLink\tPrice\tRegular_Price\tModel\tRatingValue\tReviewCount\tSKU\tSold_By";
		
		#------------------Product Details Tab -------------------------------------
		
		my $prod_detail_blk=$1 if($Content=~m/<div\s*class\=\"detailspecbox\">[^>]*?<table[^>]*?>([\w\W]*?)<\/section>/is);
		
		my @trs = $prod_detail_blk=~m/<tr>\s*(<td\s*class\=\"specs\-heading\">[\w\W]*?)<\/tr>/igs;
		
		my (@headers,%datas);
		foreach my $tr(@trs)
		{
			print $tr ."\n";
			
			my @tds = $tr=~m/<td\s*class\=\"specs\-heading\">([\w\W]*?)<\/td>\s*<td[^>]*?>([\w\W]*?)<\/td>/igs;
			
			push(@headers,trim($tds[0]));
			
			print "P0:: $tds[0] \n";
			print "P1:: $tds[1] \n";
			
			$datas{trim($tds[0])}=trim($tds[1]);
		}

		# print "head_print_temp:: $head_print_temp\n";
		
		if($head_print_temp eq "True")
		{
			open JJ,">$File_Location";
			print JJ "$Title1\t<UAE_Headers_UAE>\n";
			close JJ;
			
			$head_print_temp="False";
			# print "head_print_temp:: $head_print_temp\n";
			@head_check_old=@headers;
			push(@head_check_old,"<UAE_Headers_UAE>");
			&Add_Headers($File_Location,\@head_check_old);
		}
		
		my $wantToAddNewHeaders="No";
		my @adding_new_header;
		foreach my $head_check_new(@headers)
		{
			my $title_Check="True";
			
			foreach my $head_check_old(@head_check_old)
			{
				if($head_check_new eq $head_check_old)
				{
					$title_Check="False";
				}
			}
			
			print "title_Check:: $title_Check\n";
			
			if($title_Check eq "True")
			{
				push(@head_check_old,$head_check_new);
				push(@adding_new_header,$head_check_new);
				
				$wantToAddNewHeaders="Yes";
			}
		}
		
		print "New Headers @adding_new_header*****************\n";<>;
		
		if($wantToAddNewHeaders eq "Yes")
		{
			
			push(@adding_new_header,"<UAE_Headers_UAE>");
			&Add_Headers($File_Location,\@adding_new_header);
			
			print "New Headers Added*****************\n";
		}

		my $ops;
		foreach my $op(@head_check_old)
		{
			print "opXXX:: $op ***\n";
			$ops .= $datas{$op}."\t" if($op ne "<UAE_Headers_UAE>");
			# print "OOO:: $ops\n";
		}
		
		$ops=~s/\t$//igs;
		$ops=~s/\s*$//igs;
		
		print "ops::: $ops";<>;
		
		# my $Recommended_Accessories;
		# if($prod_detail_blk=~m/<tr>\s*<td[^>]*?>\s*Recommended\s*Accessories\s*<\/td>([\w\W]*?)<\/tr>/is)
		# {
			# my $det_blk=$1;
			# $det_blk=~s/<\/li>/\|/igs;
			# $Recommended_Accessories=&trim($det_blk);
		# }
		
		my $op="$Count\t$postID\t$post_title\t$permalink\t$price\t$regular_price\t$Model\t$ratingValue\t$reviewCount\t$SKU\t$Sold_By";
		
		print "OPPP::: $op";
		
		open JJ,">>$File_Location";
		print JJ "$op\t$ops\n";
		close JJ;
		
	}
	
}	


sub Data_Extractions()
{
	my $Post_Responce=shift;
	my $Count=shift;
	
	while($Post_Responce=~m/(\{\"post\_id\"[\w\W]*?\}\}\,)/igs)
	{
		my $Block=$1;
		my $postID=cond($Block,'\"post\_id\"\:([\w\W]*?)\,\"');
		my $post_title=cond($Block,'\"post_title\"\:\"([\w\W]*?)\"\,');
		my $permalink=cond($Block,'\"permalink\"\:\"([\w\W]*?)\"\,');
		my $price=cond($Block,'\"price\"\:([\w\W]*?)\,');
		my $regular_price=cond($Block,'\"regular_price\"\:([\w\W]*?)\,');
		
		$Count++;
		
		print "Count:: $Count\n";
		print "postID:: $postID\n";
		print "post_title:: $post_title\n";
		print "permalink:: $permalink\n";
		print "price:: $price\n";
		
		my ($Content,$Code,$Redir_Url) = &Getcontent($permalink,"","","","","","");
		
		sleep(5);
		
		open(DC,">Inner_Page.html");
		print DC $Content;
		close DC;

		print "***  Inner_Page  ****\n";
		
		
		
		
		my $inn_Blk=$1 if($Content=~m/<div\s*class\=\"quick\-right\">([\w\W]*?<[^>]*?pdp\-footer\s*clearfix[^>]*?>[\w\W]*?<\/a>)/is);
		
		my $Model=cond($inn_Blk,'<[^>]*?>\s*Model\:\s*<[^>]*?>([\w\W]*?)<\/p>');
		my $ratingValue=cond($inn_Blk,'<[^>]*?\"product\-rating\-count\"[^>]*?\"ratingValue\">([^>]*?)<[^>]*?>');
		my $reviewCount=cond($inn_Blk,'<[^>]*?\"review-details in-block reviewCount\">[\w\W]*?reviewCount[^>]*?>([^>]*?)<[^>]*?>');
		my $SKU=cond($inn_Blk,'<[^>]*?>\s*SKU\:\s*<[^>]*?>([\w\W]*?)<\/p>');
		my $Sold_By=cond($inn_Blk,'<span\s*class\=\"sold\-by\">\s*Sold\s*by\:([\w\W]*?)<\/span>');
		
		#---------------------------------------------------------------------------------------------------------
		# my $summ_Blk=$1 if($inn_Blk=~m/<[^>]*?product\-summary\s*clearfix\">([\w\W]*?)<\/ul>\s*<\/div>/is);
		
		# my $OS_Version=cond($summ_Blk,'<[^>]*?>\s*OS\s*Version\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $Screen_Size=cond($summ_Blk,'<[^>]*?>\s*Screen\s*Size\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $CPU=cond($summ_Blk,'<[^>]*?>\s*CPU\s*\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $Memory=cond($summ_Blk,'<[^>]*?>\s*Internal\s*Memory\s*\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $RAM=cond($summ_Blk,'<[^>]*?>\s*RAM\s*\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $Camera=cond($summ_Blk,'<[^>]*?>\s*Rear\s*\/\s*Front\s*Camera\s*\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $Battery=cond($summ_Blk,'<[^>]*?>\s*Battery\s*Capacity\s*\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		# my $Color=cond($summ_Blk,'<[^>]*?>\s*Color\s*\:\s*<[^>]*?>([\w\W]*?)<\/li>');		
		#----------------------------------------------------------------------------------------------------------
		
		# my $Dimensions=
		
		my $prod_detail_blk=$1 if($Content=~m/<div\s*class\=\"detailspecbox\">[^>]*?<table[^>]*?>([\w\W]*?)<\/section>/is);
		
		my $New_Product_Details=$prod_detail_blk;
		
		
		my $Brand=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Brand\s*<\/td>([\w\W]*?)<\/tr>');
		my $Prod_Color=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Color\s*<\/td>([\w\W]*?)<\/tr>');
		my $Dual_SIM=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Dual\s*SIM\s*<\/td>([\w\W]*?)<\/tr>');
		my $OS=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*OS\s*<\/td>([\w\W]*?)<\/tr>');
		my $OS_Version=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*OS\s*Version\s*<\/td>([\w\W]*?)<\/tr>');
		my $Standards=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Comm\s*Standards\s*<\/td>([\w\W]*?)<\/tr>');
		my $Technology=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Technology\s*<\/td>([\w\W]*?)<\/tr>');
		my $Two_G=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*2G\s*Network\s*<\/td>([\w\W]*?)<\/tr>');
		my $Three_G=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*3G\s*Network\s*<\/td>([\w\W]*?)<\/tr>');
		my $Four_G=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*4G\s*Network\s*<\/td>([\w\W]*?)<\/tr>');
		my $Speed=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Speed\s*<\/td>([\w\W]*?)<\/tr>');
		my $GPRS=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*GPRS\s*<\/td>([\w\W]*?)<\/tr>');
		my $EDGE=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*EDGE\s*<\/td>([\w\W]*?)<\/tr>');
		my $SIM_Card=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*SIM\s*Card\s*<\/td>([\w\W]*?)<\/tr>');
		my $Screen_Size=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Screen\s*Size\s*<\/td>([\w\W]*?)<\/tr>');
		my $Resolution=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Resolution\s*<\/td>([\w\W]*?)<\/tr>');
		my $Type=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Type\s*<\/td>([\w\W]*?)<\/tr>');
		my $Multitouch=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Multitouch\s*<\/td>([\w\W]*?)<\/tr>');
		my $Chipset=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Chipset\s*<\/td>([\w\W]*?)<\/tr>');
		my $CPU_Details=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*CPU\s*Details\s*<\/td>([\w\W]*?)<\/tr>');
		my $CPU=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*CPU\s*<\/td>([\w\W]*?)<\/tr>');
		my $GPU=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*GPU\s*<\/td>([\w\W]*?)<\/tr>');
		my $Sensors=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Sensors\s*<\/td>([\w\W]*?)<\/tr>');
		my $Messaging=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Messaging\s*<\/td>([\w\W]*?)<\/tr>');
		my $Browser=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Browser\s*<\/td>([\w\W]*?)<\/tr>');
		my $Java=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Java\s*<\/td>([\w\W]*?)<\/tr>');
		my $Internal_Memory=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Internal\s*Memory\s*<\/td>([\w\W]*?)<\/tr>');
		my $RAM=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*RAM\s*<\/td>([\w\W]*?)<\/tr>');
		my $Card_Slot=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Card\s*slot\s*<\/td>([\w\W]*?)<\/tr>');
		my $Rear_Front_Camera=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Rear\s*\/\s*Front\s*Camera\s*<\/td>([\w\W]*?)<\/tr>');
		my $Front_Camera=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Front\s*Camera\s*<\/td>([\w\W]*?)<\/tr>');
		my $Video_Resolution=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Video\s*Resolution\s*<\/td>([\w\W]*?)<\/tr>');
		my $Alert_Type=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Alert\s*types\s*<\/td>([\w\W]*?)<\/tr>');
		my $Loudspeaker=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Loudspeaker\s*<\/td>([\w\W]*?)<\/tr>');
		my $jack=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*3\.5mm\s*jack\s*<\/td>([\w\W]*?)<\/tr>');
		my $WiFi=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*WiFi\s*Type\s*<\/td>([\w\W]*?)<\/tr>');
		my $Bluetooth=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Bluetooth\s*<\/td>([\w\W]*?)<\/tr>');
		my $NFC=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*NFC\s*<\/td>([\w\W]*?)<\/tr>');
		my $USB=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*USB\s*<\/td>([\w\W]*?)<\/tr>');
		my $Radio=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Radio\s*<\/td>([\w\W]*?)<\/tr>');
		my $GPS=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*GPS\s*<\/td>([\w\W]*?)<\/tr>');
		my $Battery_Capacity=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Battery\s*Capacity\s*<\/td>([\w\W]*?)<\/tr>');
		my $Usage_Time=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Usage\s*Time\s*<\/td>([\w\W]*?)<\/tr>');
		my $Battery_Type=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Battery\s*Type\s*<\/td>([\w\W]*?)<\/tr>');
		my $Dimensions=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Dimensions\s*<\/td>([\w\W]*?)<\/tr>');
		my $Weight=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Weight\s*<\/td>([\w\W]*?)<\/tr>');
		my $Release_Date=cond($prod_detail_blk,'<tr>\s*<td[^>]*?>\s*Release\s*Date\s*<\/td>([\w\W]*?)<\/tr>');
		
		my $Recommended_Accessories;
		if($prod_detail_blk=~m/<tr>\s*<td[^>]*?>\s*Recommended\s*Accessories\s*<\/td>([\w\W]*?)<\/tr>/is)
		{
			my $det_blk=$1;
			$det_blk=~s/<\/li>/\|/igs;
			$Recommended_Accessories=&trim($det_blk);
		}
		
		
		my $op="$Count\t$postID\t$post_title\t$permalink\t$price\t$regular_price\t$Model\t$ratingValue\t$reviewCount\t$SKU\t$Sold_By\t$Brand\t$Prod_Color\t$Dual_SIM\t$OS\t$OS_Version\t$Standards\t$Technology\t$Two_G\t$Three_G\t$Four_G\t$Speed\t$GPRS\t$EDGE\t$SIM_Card\t$Screen_Size\t$Resolution\t$Type\t$Multitouch\t$Chipset\t$CPU_Details\t$CPU\t$GPU\t$Sensors\t$Messaging\t$Browser\t$Java\t$Internal_Memory\t$RAM\t$Card_Slot\t$Rear_Front_Camera\t$Front_Camera\t$Video_Resolution\t$Alert_Type\t$Loudspeaker\t$jack\t$WiFi\t$Bluetooth\t$NFC\t$USB\t$Radio\t$GPS\t$Battery_Capacity\t$Usage_Time\t$Battery_Type\t$Dimensions\t$Weight\t$Release_Date\t$Recommended_Accessories\n";
		
		open JJ,">>Output.txt";
		print JJ "$op";
		close JJ;
	}
}


sub cond()
{
	my $value=shift;
	my $regex=shift;
	
	if($value=~m/$regex/is)
	{
		my $data=$1;
		$data=&trim($data);
		return $data;
	}
}

sub trim()
{
	my $v=shift;
	
	my $c=chr(194);
	my $d=chr(160);

	$v=~s/$c//igs;
	$v=~s/$d//igs;

	$v=~s/\n//igs;
	$v=~s/&nbsp;/ /igs;
	$v=~s/<br[^>]*?>/ /igs;
	$v=~s/<\/li>/ \| /igs;
	$v=~s/<[^>]*?>//igs;
	$v=~s/\s+/ /igs;
	$v=~s/^\s*//igs;
	$v=~s/\s*$//igs;
	# $v=~s/\t$//igs;
	
	$v=~s/^\~//igs;
	
	$v=~s/^\|//igs;
	$v=~s/\|$//igs;
	
	$v=~s/&amp;/\&/igs;
	$v=~s/{ord(32)}//igs;
	$v=~s/\•//igs;

	return $v;
}
=e
my $json = JSON->new;
my $data = $json->decode($Post_Responce);

my @KeyObjects = @{$data->{'hits'}};

foreach my $Keys ( @KeyObjects) 
{	
	print "KEY:: $Keys\n";
	
	my $post_id = $Keys->{'post_id'};
	my $post_title = $Keys->{'post_title'};
	my $images = $Keys->{'images'};
	my $sale_price = $Keys->{'DocDate'};
	
	print "post_id:: $post_id \n";
	print "post_title:: $post_title \n";
	print "images:: $images \n";
	print "sale_price:: $sale_price \n";
	
}	
=cut

sub Getcontent()
{
	my $Document_Url=shift;
	my $URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $Accept_Language=shift;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://');
	# }
	
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE 1:: $code :: $Proxy\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		print "CODE 2:: $code :: $Proxy\n";
	}
	elsif($code=~m/30/is)
	{
		print "CODE 3:: $code :: $Proxy\n";
		my $loc=$res->header("location");
		
		print "loc :: $loc\n";
		
		$Document_Url=$loc;
		
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				
				print "U2 :: $u2\n";
				
				if($Document_Url=~m/rugby\.gov\.uk/is)
				{
					$Document_Url=$Document_Url;					
				}
				else
				{
					$Document_Url=$u2;
				}
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	elsif($Proxy eq 'Y')
	{
		print "CODE 4:: $code :: $Proxy\n";
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			$proxyFlag=1;
			# print "via [[192]]...\n";
			# $ua->proxy(['http','https'], 'http://');
			goto Home;
		}
	}
	else
	{
		print "CODE 5:: $code :: $Proxy\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	# print "redir_url==>$redir_url\n";
	return ($content,$code,$Document_Url);
}


sub Post_Method()
{
	my $Doc_Post_URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	my $Proxy=shift;
	my $rerun_count=0;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://');
	# }
	
	$Doc_Post_URL =~ s/^\s+|\s+$//g;
	$Doc_Post_URL =~ s/amp;//igs;
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(POST=>$Doc_Post_URL);
	$req->header("Host"=> "$Host");
	# $req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Referer"=> "$Referer");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=> "en-us,en;q=0.5");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# my %res=%$res;
	# my $headers=$res{'_headers'};
	# my %headers=%$headers;

	# for my $k(keys %headers)
	# {
		# print "[$k	:$headers{$k}]\n";
	# }	
	# my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	# print "set-cookie:: $Cookie\n";	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$Doc_Post_URL);
			my $u2=$u1->abs;
			my $Redir_url;
			($content,$Redir_url)=&Getcontent($u2,$Doc_Post_URL,$Host,$Content_Type,$Referer,$Proxy);
		}
	}
	elsif($Proxy eq 'Y')
	{
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			print "via 192...\n";
			$proxyFlag=1;
			# $ua->proxy(['http','https'], 'http://');
			goto Home;
		}
	}
	else
	{
		if ($rerun_count < 1)
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	return ($content,$code);
}