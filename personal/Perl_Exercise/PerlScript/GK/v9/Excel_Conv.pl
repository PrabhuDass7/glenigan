use strict;
use Time::HiRes qw(time);
use DateTime;
use POSIX qw(strftime);
use Time::Piece;
use Excel::Writer::XLSX;
use Encode;
use File::Path 'rmtree';
use File::Find;
use File::stat;
use Cwd qw(abs_path);
use File::Basename;
use Cwd;

my $dir = getcwd;

my $dt = DateTime->now->set_time_zone( 'Asia/Kolkata' );
my $y=$dt->year;
my $m=$dt->month_abbr;
my $d=$dt->day;
my $Date="$d-$m-$y";

print "Date:: $Date\n";

my $Location=$ARGV[0];

my @files = glob("$Location/*.txt");
my ($workbook,$worksheet);
$workbook = Excel::Writer::XLSX->new("$Location/UAE_Output_$Date.xlsx");	
foreach my $file (@files)
{
	chomp($file);
	print "FILE_NAME:: $file\n";
	my $Sheet_Name=$1 if($file=~m/.*\/([^>]*?)\.txt/is);
	$worksheet = $workbook->add_worksheet("$Sheet_Name");
	
	my $rc=0;
	my $border_c;
	
	open FH,"<$file";
	my @Datas=<FH>;
	
	foreach my $Datas(@Datas)
	{
		my @Data = split("\t",$Datas);
		
		my $cc = 0;
		my $format = $workbook->add_format( border=>1, color=>'black' );
		$format->set_bg_color( 'None' );
			
		foreach my $Data(@Data)
		{
			chomp($Data);
			if($rc == 0){
				$format  = $workbook->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
				$format->set_bold();
				$border_c=$cc;
			}
			$format->set_align('center');
			# $worksheet->write($rc, $cc, $Data, $format);
			$worksheet->write($rc, $cc, decode('utf8',$Data), $format);
			
			$cc++;
		}
		
		while($border_c != $cc-1)
		{
			$worksheet->write($rc, $cc, decode('utf8',""), $format);
			$cc++;
		}
			
		$rc++;
	}
}

$workbook->close;

print "**** Text File to Excel Conversion Completed ****\n";
