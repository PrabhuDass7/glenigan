use strict;
use LWP::UserAgent;
use URI::URL;
use HTTP::Cookies;
use HTML::Entities;
use Cwd qw(abs_path);
use File::Basename;
use LWP::Simple;
use MIME::Base64;
use URI::Escape;
use WWW::Mechanize;
use IO::Socket::SSL qw();
# use Unicode::Escape;
use URI::Encode;
# use Config::Tiny;
use Encode;
use File::stat;
# require Exporter;
use HTML::TableExtract;
use Mozilla::CA;
use File::Path 'rmtree';
use Cwd;
use Time::Piece;
use POSIX qw(strftime);
use DateTime;
use Date::Calc qw(Add_Delta_Days);

# my @ISA = qw(Exporter);
# my @EXPORT = qw(ImageDownload);


open HH,">Status.txt";
print HH "ID\tOriginal_Url\tSuitable_Url\tCompany_Name\tStatus_Code\tImage_Url\tImage_Size\tImage_Status_code\tImage_Err\tStart_Time\tEnd_Time\n";
close HH;

#--------- LWP UserAgent declaration  ----------------#
my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);               
$ua->cookie_jar($cookie_jar);
$cookie_jar->save;

my $mech1 = WWW::Mechanize->new(autocheck => 0 );

my $dir = getcwd;
my $HTML_Folder="$dir/HTML_Folder";
my $Img_Folder="$dir/Image_Folder";

&createdirectory($HTML_Folder);
&createdirectory($Img_Folder);

open FH,"<Input.txt";
my @urls=<FH>;

my $count=0;

foreach my $urls(@urls)
{
	my($id,$url,$name)=split("\t",$urls);
	chomp($url);
	chomp($name);
	$count++;
	
	my $Start_Time = DateTime->now( time_zone => "Asia/Kolkata" );

	print "****Count :: $count ***\n";
	print "****ID :: $id ***\n";
	print "URL:: $url\n";
	
	my $Org_url=$url;
	
	my ($content,$code);
	if($url=~m/http/is)
	{
		($content,$code)=&Getcontent("$url","","","","","","","");
	}
	else
	{
		$url='http://'.$url;
		($content,$code,$url)=&Getcontent("$url","","","","","","","");
		
		print "Test Code:: $code\n";
		
		if($code!~m/20/is)
		{
			$url=~s/http\:/https\:/igs;
			# print "Test url:: $url\n";<STDIN>;
			($content,$code)=&Getcontent("$url","","","","","","","");
		}
		
	}
	
	
	open HH,">$HTML_Folder/$id.html";
	print HH "$content";
	close HH;
	
	print "Content Completed***\n";
	
	
	#---- Collecting Valid img url ----
	
	my ($img_url,$img,$typ);
	my $temp=0;
	while($content=~m/((?:src|href)\=(?:\"|\')[^>]*?logo[^>]*?>)/igs)
	{
		my $img_blk=$1;
		
		$img=$1 if($img_blk=~m/(?:src|href)\s*\=\s*(?:\"|\')([^>]*?)(?:\"|\')/is);
		
		print "img:: $img\n";
		print "IMGLen:: ".length($img)."\n";
		
		if(length($img) != 1)
		{
			$typ=$1 if($img_blk=~m/(?:\.|)(pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png)/is);
			$temp=1;	
			goto nxt;
		}
	}
	
	nxt:
	
	if($temp==0)
	{
		print "IMg Unavailable***\n";
		
		my $End_Time = DateTime->now( time_zone => "Asia/Kolkata" );
		
		open HH,">>Status.txt";
		print HH "$id\t$Org_url\t$url\t$name\t$code\tNo\t\t\t\t$Start_Time\t$End_Time\n";
		close HH;
	}
	else
	{
		if($img!~m/http/is)
		{
			print "http not available**\n";
			print "url:: $url**\n";
			if($url=~m/^([^>]*?\.[^>]*?(?:\/|$))/is)
			{
				my $domain=$1;
				print "Domain concatinated**\n";
				$domain=~s/\/\s*$//igs;
				$img=~s/^\s*\///igs;
				$img_url="$domain/$img";
			}
		
		}else{
			$img_url=$img;
		}
		
		# print "Img_Url:: $img_url\n";<STDIN>;
	
		my($Img_Size,$img_code,$img_Err)=&Download($img_url,"$id.$typ",$Img_Folder) if($img_url=~m/(?:http|www)/is);
		
		my $End_Time = DateTime->now( time_zone => "Asia/Kolkata" );
		
		open HH,">>Status.txt";
		print HH "$id\t$Org_url\t$url\t$name\t$code\t$img_url\t$Img_Size\t$img_code\t$img_Err\t$Start_Time\t$End_Time\n";
		close HH;
	}
	
	print "Completed:: $id\n";
}

print "**** END ****\n";	



sub Getcontent()
{
	my $Document_Url=shift;
	my $URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $Accept_Language=shift;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	# }
	
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	

	my $res = $ua->request($req);
	
	
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE 1:: $code :: $Proxy\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		print "CODE 2:: $code :: $Proxy\n";
	}
	elsif($code=~m/30/is)
	{
		print "CODE 3:: $code :: $Proxy\n";
		my $loc=$res->header("location");
		
		print "loc :: $loc\n";
		
		$Document_Url=$loc;
		
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				
				print "U2 :: $u2\n";
				
				if($Document_Url=~m/rugby\.gov\.uk/is)
				{
					$Document_Url=$Document_Url;					
				}
				else
				{
					$Document_Url=$u2;
				}
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	elsif($Proxy eq 'Y')
	{
		print "CODE 4:: $code :: $Proxy\n";
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			$proxyFlag=1;
			print "via [[192]]...\n";
			$ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		print "CODE 5:: $code :: $Proxy\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# print "redir_url==>$redir_url\n";<STDIN>;
	return ($content,$code,$Document_Url);
}


sub Post_Method()
{
	my $Doc_Post_URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	my $Proxy=shift;
	my $rerun_count=0;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	# }
	
	$Doc_Post_URL =~ s/^\s+|\s+$//g;
	$Doc_Post_URL =~ s/amp;//igs;
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(POST=>$Doc_Post_URL);
	$req->header("Host"=> "$Host");
	# $req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Referer"=> "$Referer");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=> "en-us,en;q=0.5");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# my %res=%$res;
	# my $headers=$res{'_headers'};
	# my %headers=%$headers;

	# for my $k(keys %headers)
	# {
		# print "[$k	:$headers{$k}]\n";
	# }	
	# my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	# print "set-cookie:: $Cookie\n";	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$Doc_Post_URL);
			my $u2=$u1->abs;
			my $Redir_url;
			($content,$Redir_url)=&Getcontent($u2,$Doc_Post_URL,$Host,$Content_Type,$Referer,$Proxy);
		}
	}
	elsif($Proxy eq 'Y')
	{
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			print "via 192...\n";
			$proxyFlag=1;
			# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		if ($rerun_count < 1)
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	return ($content,$code);
}


sub Download()
{
	my $img_url 		= shift;
	my $pdf_name 		= shift;
	my $Temp_Download_Location	= shift;
	
	$Temp_Download_Location=$Temp_Download_Location."/$pdf_name";
	
	print "img:: $img_url\n";
	print "File Name:: $Temp_Download_Location\n";
	
	$mech1->get( $img_url, ':content_file' => $Temp_Download_Location );
		
	my $code;
	if ( $mech1->success() ) {
		print("PDF_Downloaded_Successfully\n");
		$code=$mech1->status;
	}
	else 
	{
		my $status1 = getstore($img_url, $Temp_Download_Location);
		# print "status is: " . $mech1->status;
		
		if($status1!~m/20/is)
		{
			my ($content1,$code1)=getcont_old($img_url,"","","GET");
			
			if($code1=~m/20/is)
			{
				open TU,">$Temp_Download_Location";
				binmode TU;
				print TU "$content1";
				close TU;
				
				$code="200";
			}
	   }else{
			$code="200";
	   }
	}
	
	my $size = stat($Temp_Download_Location)->size;
	my $Img_Size=sprintf("%.2f", $size / 1024);
	
	print "FileSizein_kb  :: $Img_Size\n";
	my $Err="";
	return($Img_Size,$code,$Err);
	
=e
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.[^>]*?$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code);
	my $download_flag=0;
	my $Img_Size;
	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		
		
		if($pdf_url=~m/\.(pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png)/is)
		{
			my $nnnm=$1;
			if($pdf_url!~m/(.$1)$/is)
			{
				my $rnm=$1 if($pdf_name=~m/(.*)\.(?:pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png)[^>]*?$/is);
				$pdf_name=$rnm.".$nnnm";
			}
		}
		
		
		(my $pdf_content,$code,my $rurl) = &Getcontent($pdf_url,"","","","");
		

		$file_name=$Temp_Download_Location.'/'.$pdf_name.".png";
			
		print "pdf_url:: $pdf_url\n";
		print "File Name:: $file_name\n";
		
		eval{
				print "Eval Block***\n";
				
				my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
				binmode($fh);
				$fh->print($pdf_content);
				$fh->close();
				
				my $size = stat($file_name)->size;
                my $return_size_kb=sprintf("%.2f", $size / 1024);
                
				print "FileSizein_kb  :: $return_size_kb\n";
                
				$Img_Size=$return_size_kb;
				
				if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
                {
                    unlink $file_name;
					$code=600;
                    $downCount++;
                    goto Re_Download;
                }				
		
		};
		if($@)
		{
			$Err=$@;
			print "Err:: $Err\n";
			if($download_flag == 0)
			{
				$download_flag = 1;
				goto Re_Download;
			}	
		}
		
	}

	return($Img_Size,$code,$Err);
=cut	
}


=e
sub filefolders()
{
	my $folder_typ=shift;
	my $url=shift;
	
	my $crt_dir="$dir/$Domain_Name/DOWNLOADED";
	
	&createdirectory($crt_dir);
	my $pdf_name;
	
	$pdf_name=$1 if($url=~m/.*\/([^>]*?)$/is);
	
	print "PDF::url:: $url\n";
	# print "pdf_name22:: $pdf_name\n";<>;
	
	
	# if($url=~m/(.pdf)/is)
	# {
		# $pdf_name=$pdf_name.".pdf" if($url!~m/(.pdf)$/is);
	# }
	
	
	if($url=~m/\.(pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png)/is)
	{
		my $nnnm=$1;
		if($url!~m/(.$1)$/is)
		{
			my $rnm=$1 if($pdf_name=~m/(.*)\.(?:pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png)[^>]*?$/is);
			$pdf_name=$rnm.".$nnnm";
		}
	}

	
	$pdf_name  = uri_unescape($pdf_name);
	$pdf_name  = uri_unescape($pdf_name);

	# $pdf_name = URL::Encode::url_encode($pdf_name);
	
	# print "pdf_namexx:: $pdf_name\n";<>;
	
	my $path=$crt_dir."\/$pdf_name";
	
	# print "PDF_Catagory:: $path\n";
	
	my $value=&Duplicate($url);
	
	# print "value:: $value\n";<>;
	
	if ($value == 1)
	{
		$mech1->get( $url, ':content_file' => $path );
		
		# $m = WWW::Mechanize->new();
		# $m->get($url);
		
		if ( $mech1->success() ) {
			print("PDF_Downloaded_Successfully\n");
			return $pdf_name;
		}
		else {
			my $status1 = getstore($url, $path);
			# print "status is: " . $mech1->status;
			
			if($status1!~m/20/is)
			{
			
				# my $content1=getcon->Getcont($url,"","","GET","","");
				
				my ($content1,$code1)=getcont_old($url,"","","GET");
				
				if($code1=~m/20/is)
				{
					open TU,">$path";
					binmode TU;
					print TU "$content1";
					close TU;
					
					return $pdf_name;
				}
		   }else{
			
				return $pdf_name;
		   
		   }
		}
		
		
	}
}
=cut

sub docMechMethod() 
{
    my $URL = shift;
    my $mech = shift;
   

	
	$URL=~s/amp\;//igs;
	print "URL==>$URL\n";

	# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');
	
	
	# BEGIN {
	 # $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
	 # $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
	 # $ENV{HTTPS_DEBUG} = 1;  #Add debug output
	# }

	my $proxyFlag=0;
	rePingwith_199:
	$mech->get($URL);
	my $con = $mech->content;
    my $code = $mech->status;
	print "Code: $code\n";
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
		goto rePingwith_199;
	}
    return($con,$code);
}

sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}


sub getcont_old()
{
    my($ur,$cont,$ref,$method)=@_;
    my $ccc=0;
    if($ur ne "")
    {
        netfail:
        my $request=HTTP::Request->new("$method"=>$ur);
        $request->header("Content-Type"=>"text/html; charset=UTF-8");
        # $request->header("Content-Type"=>"application/json");
        # $request->header("authority"=>"restaurantguru.com");
        # $request->header("path"=>"/La-Cocina-de-mi-Mama-Mexico-City-2/load-meals?review_limit=1");
		
        # $request->header("Cookie"=>'currentCountry=9; currentCity=207652; dc_location=ci109130; PHPSESSID=8fec9a115a35f2a71be1531abfb26627; rg_check=1; _ga=GA1.2.680176691.1587186803; _gid=GA1.2.181468251.1587186806; __gads=ID=9dd6cf0c29f65179:T=1587186804:S=ALNI_MY-9a9i_Rxr3jSOw4B7nHwWMeSHGQ');
        # $ua->ssl_opts('verify_hostname' => 0); #works
		
		
		
		# :authority: restaurantguru.com
# :method: GET
# :path: /La-Cocina-de-mi-Mama-Mexico-City-2/load-meals?review_limit=1
# :scheme: https
# accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
# accept-encoding: gzip, deflate, br
# accept-language: en-US,en;q=0.9
# cookie: currentCountry=9; currentCity=207652; dc_location=ci109130; PHPSESSID=8fec9a115a35f2a71be1531abfb26627; rg_check=1; _ga=GA1.2.680176691.1587186803; _gid=GA1.2.181468251.1587186806; __gads=ID=9dd6cf0c29f65179:T=1587186804:S=ALNI_MY-9a9i_Rxr3jSOw4B7nHwWMeSHGQ
		
		
        
        if($ref ne '')
        {
			$request->header("Referer"=>"$ref");
        }
		
        if(lc $method eq 'post')
        {
			$request->content($cont);
        }
        
		my $res=$ua->request($request);
        $cookie_jar->extract_cookies($res);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request);
        my $code=$res->code;
        
        print"\nRESPONSE CODE:$code\n";
        if($code==200)
        {
           
            my $content=$res->content();
            #$content=decode_entities($content);
            return ($content,$code);
        }
        elsif($code=~m/50/is)
        {
            print"\n Net Failure";
            $ccc++;
            if($ccc<4)
            {
                goto netfail;
            }
        }
        elsif($code=~m/30/is)
        {
           my $loc=$res->header("Location");
		   # my $rurl = $request->uri();
             print "\nLocation: $loc";
            my $request1=HTTP::Request->new(GET=>$loc);
            $request1->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8");
            my $res1=$ua->request($request1);
            $cookie_jar->extract_cookies($res1);
            $cookie_jar->save;
            $cookie_jar->add_cookie_header($request1);
            my $content1=$res1->content();
			
			
			# my $loc=$res->header("Location");
			# $loc=URI::URL->new_abs($loc,$ur);
			# Getcont($loc,'','','GET','','');
			# return ($code)

			
            return ($content1,$code);
        }
        elsif($code=~m/40/is)
        {
            my $content=$res->content();
            print "\n URL Not found";
            return ($content,$code);
        }
    }
}