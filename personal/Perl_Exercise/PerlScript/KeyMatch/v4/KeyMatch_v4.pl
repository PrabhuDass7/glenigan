use strict;
use LWP::UserAgent;
use URI::URL;
use HTTP::Cookies;
use Cwd qw(abs_path);
use File::Basename;
use LWP::Simple;
use WWW::Mechanize;
# use HTML::TagParser;
# use IO::Socket::SSL qw();
use File::stat;
use Mozilla::CA;
use Cwd;
use DateTime;
use File::Path 'rmtree';
use filehandle;
use HTML::Entities;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use File::Basename;
use Digest::MD5;
use List::MoreUtils qw/uniq/;
my $md5 = Digest::MD5->new;

use IO::Socket::SSL qw( SSL_VERIFY_NONE );
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
# use CACertOrg::CA;

open HH, ">Output.txt";
print HH "Count\tArticle_Heading\tURL\tStatus\tAveeno\tNeutrogena\tSupergoop\tSun Bum\tHawaiian Tropic\tBare Republic\tGoddess Garden\tAlba Botanica\tBabyganics\tStartTime\tEndTime\n";
close HH;

#--------- LWP UserAgent declaration  ----------------#
my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(1); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);               
$ua->cookie_jar($cookie_jar);
$cookie_jar->save;

# my $mech1 = WWW::Mechanize->new(autocheck => 0 );
my $mech1 = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);
				
my $dir = getcwd;
my $HTML_Folder="$dir/HTML_Folder";


&createdirectory($HTML_Folder);


#---------- Test ----------------

my $count=0;

open HH,"<Product_List.txt";
my @input_prod_list=<HH>;
	
open FH,"<Input.txt";
my @urls=<FH>;

foreach my $urls(@urls)
{
	my($id,$name,$url)=split("\t",$urls);
	chomp($url);
	chomp($name);
	$count++;
	
	my $Start_Time = DateTime->now( time_zone => "Asia/Kolkata" );

	print "**** Count :: $count ***\n";
	print "**** ID    :: $id    ***\n";
	print "**** URL   :: $url   ***\n";<>;
	
	my ($content,$code,$url)= get_c($url);
	
	($content,$code,$url)=&Content_Collection($url) if($code!~m/20/is);
	
	($content,$code,$url)=&docMechMethod($url,$mech1) if($code!~m/20/is);
	
	#-------- Save Content In HTML Format -----------
	
	open HH,">$HTML_Folder/$id.html";
	print HH "$content";
	close HH;
	
	#-------- Save Content In Text Format -----------
	
	my $content_txt=$content;
	$content_txt=~s/<[^>]*?>//igs;
	open HH,">$HTML_Folder/$id.txt";
	print HH "$content_txt";
	close HH;
	
	print "Content Completed***\n";
	
	#-------------- Read HTML Files ---------------------------------------------------	
	
	# open JJ,"<html.txt";
	# my @arr=<JJ>;
	
	# foreach my $Home_urls(@arr)
	# {
		# my ($Home_url,$content_ID)=split('\t',$Home_urls);
		# chomp($Home_url);
		# chomp($content_ID);
		# print "Home_url::$Home_url\n";<>;
		# open DD,"$Home_url";
		# my @con=<DD>;
		# my $content="@con";
	#-----------------------------------------------------------------------------------
	
	my $prod_list='Aveeno|Neutrogena|Supergoop|Sun Bum|Hawaiian Tropic|Bare Republic|Goddess Garden|Alba Botanica|Babyganics';
	my $prod_list2=lc($prod_list);
	$prod_list2=~s/\s*//igs;
	
	# my @titles=$content=~m/<\s*h[\d]+\s*>([\w\W]*?)<\s*\/h[\d]+\s*>/igs;
	my @titles=$content=~m/(<[^>]*?>[^>]*?(?:$prod_list|$prod_list2)[^>]*?<[^>]*?>)/igs;
	
	print "LEN::$#titles\n";

	my @keys=("Aveeno","Neutrogena","Supergoop","Sun Bum","Hawaiian Tropic","Bare Republic","Goddess Garden","Alba Botanica","Babyganics");
	my %op;
	my $Dublicate;
	foreach my $tit(@titles)
	{
		chomp($tit);
		# print "tit:: $tit\n";<>;
		
		# goto nxt1 if($tit=~m/(?:\"\:\"|\{|\}|\"\,\"|<\s*\/\s*p\s*>|<p>|<em>)/is);
		
		my $tit_temp=$tit;
		$tit=trim($tit);
		# goto nxt1 if($tit=~m/\.com\b/is);
		
		# $tit=decode_entities($tit);
		($Dublicate,my $value)=&Duplicate($Dublicate,$tit);
		goto nxt if($value!=1);
		
		foreach my $key_prod_list(@input_prod_list)
		{
			chomp($key_prod_list);
			
			my $key_prod_list_lng=length($key_prod_list);
			my $tit_lng=length($tit);
			
			my $Score;
			if($key_prod_list_lng > $tit_lng)
			{
				$Score=trigram_score($key_prod_list,$tit);
			}else{
				$Score=trigram_score($tit,$key_prod_list);
			}
			
			# print "Score Is :: $Score\n";<>;
			
			# if($tit=~m/^\s*\b$key_prod_list\b/is)
			# if($tit=~m/\b$key_prod_list\b/is)
			
			if($Score >= 50)
			{
				if($tit_temp=~m/(?:\"\:\"|\{|\}|\"\,\"|<\s*\/\s*p\s*>|<p>|<em>)/is) #**** Fetch From Paragraph ****************
				{
					$tit=$key_prod_list if($tit=~m/\b$key_prod_list\b/is);
					goto nxt1 if($tit eq "");
				}
				
				foreach my $key(@keys)
				{
					chomp($key);
					my $temp_key=lc($key);
					$temp_key=~s/\s*//igs;
					
					# goto nxt if($tit eq $key);
					# goto nxt if($tit eq $temp_key);
					
					
					# if($tit=~m/^(?:\W+|)\s*(?:$temp_key|$key)/is)
					# if($key_prod_list=~m/(?:$temp_key|$key)/is)
					if($tit=~m/(?:$temp_key|$key)/is)
					{
						# $op{$key}.=$key_prod_list." | ";
						# print "Matched*****\n";
						$tit=decode_entities($tit);
						$op{$key}.=$tit." | ";
						print "Score Is :: $Score\n";
						print "Matched*****\n";
						
						goto nxt1;
					}
				}
			}
			# nxt:	
		}
		nxt:
		nxt1:
	}

	my $output=&KeyMatch(\%op,\@keys);

	print "OUTPUT:: $output\n";
	
	my $End_Time = DateTime->now( time_zone => "Asia/Kolkata" );
	
	if($output=~m/^(\t)*(?:\n|)$/igs)
	{
		$code="No Data";
	}
	
	open HH, ">>Output.txt";
	print HH "$id\t$name\t$url\t$code\t$output\t$Start_Time\t$End_Time\n";
	# print HH "$count\t\t$Home_url\t\t$output\n";
	close HH;
}

print "**** END ****\n";	


sub trigram_score()
{
	my $word =shift;
	my $dictword =shift;
	if(($word ne "")&&($dictword ne ""))
	{
		$word=~s/\s+//igs;
		$dictword=~s/\s+//igs;
		$word=~s/\W//igs;
		$dictword=~s/\W//igs;
		$word=lc($word);
		$dictword=lc($dictword);
		my @pairs = $word =~ /(?=(..(?:.)?))/g;
		# print(" \"@pairs\" \n");
		local $" = q{|};
		my $matcher = qr{(?=(@pairs))};
		# print("matcher:: \"$matcher\" \n");
		my %coef;
		my $matches = () = $dictword =~ /$matcher/g;
		my $totalz = "Total Matches:: $matches \t DicWord:: $dictword \n";
		# print "totalz:: $totalz\n";
		my $coef = 2 * $matches / (@pairs + length($dictword)-1);
		$coef=$coef*100;
		# print ("\n----------------------------------\ncoeffesion score is $coef\n---------------------------\n");
		return $coef;
	}
	else
	{
		my $coef=0;
		# print ("\n----------------------------------\ncoeffesion score is $coef\n---------------------------\n");
		return $coef;
	}
}



sub KeyMatch()
{
	my $hash=shift;
	my $keys=shift;
	
	my %hash=%$hash;
	my @keys=@$keys;
	
	my $op;
	my $opp;
	foreach my $key(@keys)
	{
		$op="$hash{$key}";
		$op=~s/\s*\|\s*$//igs;
		$opp=$opp."\t".$op;
		# print "KEY:: $key VALUE::$opp\n";<>;
	}
	$opp=~s/^\t//igs;	
	return $opp;
}


sub content_check()
{
	my ($cont,$tag)=@_;
	
	# print "CONT:: $cont\n";
	
	my @ar=split('\,',$tag);
	foreach my $ss(@ar)
	{
		my ($tg,$tnm)=split('\|',$ss);
		chomp($tg);
		chomp($tnm);
		my $html;
		
		# print "TG:: $tg\n";
		# print "tnm:: $tnm\n";
		
		# print "LEN:: ". length($cont) ."\n";

		$html=$cont->getElementById($tnm) if($tg eq 'id');
		$html=$cont->getElementsByClassName($tnm) if($tg eq 'class');
		
		return $html if(length($html) != "");
	}
}



sub Content_Collection()
{
	my $url=shift; 
	my ($content,$code);
	if($url=~m/http/is)
	{
		($content,$code)=&Getcontent("$url","","","","","","","");
	}
	else
	{
		$url='http://'.$url;
		($content,$code,$url)=&Getcontent("$url","","","","","","","");
		
		print "Test Code:: $code\n";
		
		if($code!~m/20/is)
		{
			$url=~s/http\:/https\:/igs;
			# print "Test url:: $url\n";
			($content,$code)=&Getcontent("$url","","","","","","","");
		}
		
	}
	
	
	
	# $content=decode_entities($content);
	# $content=decode_utf8($content);
	
	return ($content,$code,$url);
}

sub Duplicate()
{
	my $Dublicate=shift;    
	my $string=shift;    
	
	print "*** Dup check ***\n";
	print "*** string:: $string ***\n";
	$md5->add("$string");
	my $digest = $md5->hexdigest;                                                                                                                
	print "$digest\n";
	 
	if($Dublicate!~/<b>$digest<b>/is)
	{
		my $du_con="<b>".$digest."<b>";                                                                                                                                          
		$Dublicate=$Dublicate.$du_con;              
		return($Dublicate,'1');
	}
	else
	{                              
		return($Dublicate,'0');                          
	}
}
	
sub url_collection()
{
	my ($Domain_name,$content,$regex_blk)=@_;
	
	my @Regx=@$regex_blk;
	
	my $temp=0;
	my ($img,$typ);
	
	my $priority=0;
	my $format_types='jpeg|jpg|png|gif|svg';	
	foreach my $regex(@Regx)
	{
		chomp($regex);
		
		$regex=~s/<Domain_Name>/$Domain_name/igs;
			
		$priority++;
		
		
		
		while($content=~m/$regex/igs)
		{
			my $img_blk=$1;
			
			
			print "Matched Priority:: $priority \n";
			# print "img_blk:: $img_blk \n";
			my $logo_key;
			if($img_blk=~m/(?:src\s*\=\s*|logo\"\:)(?:\"|\')([^>]*?)(?:\"|\')/is)
			{
				$img=$1;
				$img=~s/\.\.\///igs;
				$logo_key=$1 if($img=~m/.*\/([^>]*?)$/is);
				$logo_key = "True" if($logo_key=~m/logo/is);
			}
			
			print "img:: $img\n";
			print "logo_key:: $logo_key\n";
			# print "IMGLen:: ".length($img)."\n";<>;
			
			if(($logo_key eq "True") or ($img=~m/$Domain_name/is) or ($priority <= 3))
			{
				$typ=$1 if($img_blk=~m/(?:\.|)($format_types)/is);
				$temp=1;	
				goto nxt;
			}
		}
	}
	
	nxt:
	
	return ($img,$typ,$temp,"Priority_$priority");
}

sub trim()
{
	my $v=shift;
	$v=~s/\n//igs;
	$v=~s/<[^>]*?>//igs;
	$v=~s/((?:\(|\$)[^>]*?$)//igs;
	$v=~s/\s+/ /igs;
	$v=~s/^\s//igs;
	$v=~s/^\://igs;
	$v=~s/\:$//igs;
	$v=~s/^\,//igs;
	$v=~s/\,$//igs;
	$v=~s/\s$//igs;
	$v=~s/^\|//igs;
	$v=~s/\|$//igs;
	$v=~s/\&ndash\;/\-/igs;
	return $v;
}

sub Getcontent()
{
	my $Document_Url=shift;
	my $URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $Accept_Language=shift;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://');
	# }
	
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE 1:: $code :: $Proxy\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		
	}
	elsif($code=~m/30/is)
	{
		print "CODE 3:: $code :: $Proxy\n";
		my $loc=$res->header("location");
		
		print "loc :: $loc\n";
		
		$Document_Url=$loc;
		
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				
				print "U2 :: $u2\n";
				
				if($Document_Url=~m/rugby\.gov\.uk/is)
				{
					$Document_Url=$Document_Url;					
				}
				else
				{
					$Document_Url=$u2;
				}
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	elsif($Proxy eq 'Y')
	{
		print "CODE 4:: $code :: $Proxy\n";
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			$proxyFlag=1;
			# print "via [[192]]...\n";
			# $ua->proxy(['http','https'], 'http://');
			goto Home;
		}
	}
	else
	{
		print "CODE 5:: $code :: $Proxy\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	# print "redir_url==>$redir_url\n";
	return ($content,$code,$Document_Url);
}

sub Download()
{
	my $img_url 		= shift;
	my $pdf_name 		= shift;
	my $Temp_Download_Location	= shift;
	
	$Temp_Download_Location=$Temp_Download_Location."/$pdf_name";
	
	print "img:: $img_url\n";
	print "File Name:: $Temp_Download_Location\n";
	
	$mech1->get( $img_url, ':content_file' => $Temp_Download_Location );
		
	my $code;
	if ( $mech1->success() ) {
		print("PDF_Downloaded_Successfully\n");
		$code=$mech1->status;
	}
	else 
	{
		$code = getstore($img_url, $Temp_Download_Location);
		# print "status is: " . $mech1->status;
		
		if($code!~m/20/is)
		{
			(my $content1,$code)=getcont_old($img_url,"","","GET");
			
			if($code=~m/20/is)
			{
				open TU,">$Temp_Download_Location";
				binmode TU;
				print TU "$content1";
				close TU;
				
				$code="200";
			}
	   }
	}
	
	my ($Img_Size,$Err);
	eval
	{
		my $size = stat($Temp_Download_Location)->size;
		$Img_Size=sprintf("%.2f", $size / 1024);
	};
	if($@)
	{
		$Err=$@;
		print "Err:: $Err\n";
		$Err=~s/\s*\n//igs;
	}		
	print "FileSizein_kb  :: $Img_Size\n";
	# my $Err="";
	return($Img_Size,$code,$Err);
	
	
}


sub docMechMethod() 
{
    my $URL = shift;
    my $mech = shift;
    
	$URL=~s/amp\;//igs;
	print "URL==>$URL\n";

	# $mech->proxy(['http','https'], 'http://');
	
	
	# BEGIN {
	 # $ENV{HTTPS_PROXY} = 'http://';
	 # $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
	 # $ENV{HTTPS_DEBUG} = 1;  #Add debug output
	# }

	my $proxyFlag=0;
	rePingwith_199:
	$mech->get($URL);
	my $con = $mech->content;
    my $code = $mech->status;
	print "Code: $code\n";
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		# $mech->proxy(['http','https'], 'http://');
		goto rePingwith_199;
	}
    return($con,$code,$URL);
}

sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}


sub getcont_old()
{
    my($ur,$cont,$ref,$method)=@_;
    my $ccc=0;
    if($ur ne "")
    {
        netfail:
        my $request=HTTP::Request->new("$method"=>$ur);
        $request->header("Content-Type"=>"text/html; charset=UTF-8");
		
        if($ref ne '')
        {
			$request->header("Referer"=>"$ref");
        }
		
        if(lc $method eq 'post')
        {
			$request->content($cont);
        }
        
		my $res=$ua->request($request);
        $cookie_jar->extract_cookies($res);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request);
        my $code=$res->code;
        
        print"\nRESPONSE CODE:$code\n";
        if($code==200)
        {
           
            my $content=$res->content();
            #$content=decode_entities($content);
            return ($content,$code);
        }
        elsif($code=~m/50/is)
        {
            print"\n Net Failure";
            $ccc++;
            if($ccc<4)
            {
                goto netfail;
            }
        }
        elsif($code=~m/30/is)
        {
           my $loc=$res->header("Location");
		   # my $rurl = $request->uri();
             print "\nLocation: $loc";
            my $request1=HTTP::Request->new(GET=>$loc);
            $request1->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8");
            my $res1=$ua->request($request1);
            $cookie_jar->extract_cookies($res1);
            $cookie_jar->save;
            $cookie_jar->add_cookie_header($request1);
            my $content1=$res1->content();
			return ($content1,$code);
        }
        elsif($code=~m/40/is)
        {
            my $content=$res->content();
            print "\n URL Not found";
            return ($content,$code);
        }
    }
}





sub Download_new()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my @MD5_LIST = "";
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5,$return_size_kb);
	my $download_flag=0;
	
	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		
		(my $pdf_content,$code,my $rurl) = &Getcontent($pdf_url,"","","",$Document_Url);
				
		$file_name=$Temp_Download_Location.'\\'.$pdf_name;
		
		# print "file_name:: $file_name\n";<STDIN>;
		
		eval{
				
			if(grep( /^hfdsfh$/, @MD5_LIST )) 
			{
				print "Doc Exist*****\n";
				$MD5  = '';
				$code = 600;

			}
			else
			{
				print "Doc not Exist!!!!!****\n";
				my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
				binmode($fh);
				$fh->print($pdf_content);
				$fh->close();
				
				my $size = stat($file_name)->size;
                $return_size_kb=sprintf("%.2f", $size / 1024);
                print "FileSizein_kb  :: $return_size_kb\n";
               
				if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
                {
                    unlink $file_name;
					$code=600;
                    $downCount++;
                    goto Re_Download;
                }				
			}
		};
		if($@)
		{
			$Err=$@;
			if($download_flag == 0)
			{
				$download_flag = 1;
				goto Re_Download;
			}	
		}
		
	}
	
	return($return_size_kb,$code,$Err);
}


sub get_c()
{
	my $link = shift;
	my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
	# my $ua = LWP::UserAgent->new( ssl_opts => {		
		# SSL_verify_mode   => 'SSL_VERIFY_NONE',
		# verify_hostnames => 1,
		# SSL_ca_file      => CACertOrg::CA::SSL_ca_file()
	# });
	$ua->agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	my $response = $ua->get($link);

	if($response->is_success) {
		my $s = $response->code;
		# my $ct = $response->decoded_content;  # or whatever
		my $ct = $response->content;  # or whatever
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		# $ct = decode_entities($ct);		
		return($ct,$s,$rurl);
	}
	else{
		my $ct= $response->status_line;
		my $s = $response->code;
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		if ($s == 500){
			my ($cont,$s1,$rur) = get_c1($link);
			return($cont,$s1,$rur);
		}else{
			return($ct,$s,$rurl);
		}		
	}
}

sub get_c1()
{
	my $link = shift;
	# my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
	my $ua = LWP::UserAgent->new( ssl_opts => {		
		SSL_verify_mode   => SSL_VERIFY_NONE,
		verify_hostnames => 1,
		SSL_ca_file      => CACertOrg::CA::SSL_ca_file()
	});
	$ua->agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	my $response = $ua->get($link);

	if($response->is_success) {
		my $s = $response->code;
		# my $ct = $response->decoded_content;  # or whatever
		my $ct = $response->content;  # or whatever
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		# $ct = decode_entities($ct);		
		return($ct,$s,$rurl);
	}
	else{
		# print "Here\n";
		my $ct= $response->status_line;
		my $s = $response->code;
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		return($ct,$s,$rurl);
	}
}