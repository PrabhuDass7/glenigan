use strict;
use warnings;
use CAM::PDF;
use Try::Tiny;
use Cwd;
use CAM::PDF::PageText;
open FH,">Pdf_fileStatus.txt";
close FH;

my $curdir=getcwd();
opendir (DIR, $curdir) or die $!;

while (my $file = readdir(DIR)) 
{
	next if ($file =~ m/^\./);
	# print "$file\n";<>;
	&convert_pdf_to_text($file) if($file=~m/\.pdf/is);
}
closedir(DIR);

sub convert_pdf_to_text 
{
	my $filename=shift;
	my $pdf_filename=$curdir."/".$filename;
	try
	{
		my $pdf = CAM::PDF->new($pdf_filename) or die "foo";
		# try 
		# {
				# my $y = $pdf->getPageContentTree(1);
				# my $result = CAM::PDF::PageText->render($y) or die "foo"; 
				print "$filename :: File Not Corrupted\n";
				open FH,">>Pdf_fileStatus.txt";
				print FH $filename."\t"."File Okay\n";
				close FH;
		# } 
		# catch 
		# {
			# warn "$filename :: caught error: $_\n";
			# print "Corrupted File";
			# open FH,">>Pdf_fileStatus.txt";
			# print FH $filename."\t"."Corrupted File\n";
			# close FH;
		# };
	
	}
	catch
	{
		warn "Couldn't read PDF $pdf_filename\n";
		# print "Corrupted File";
		open FH,">>Pdf_fileStatus.txt";
		print FH $filename."\t"."Corrupted File\n";
		close FH;
	
	};
}

# use PDF::Extract;
# $pdf=new PDF::Extract( PDFDoc=>"10.1073-pnas.1515367112.pdf");
# $pdf=new PDF::Extract( PDFDoc=>"TCS FILE.pdf");
# $pdf->getPDFExtract( PDFPages=>$PDFPages );
# print $pdf->getVars("PDFError");
# print $pdf;<>;
 # $i=1;
 # while ( $pdf[$i++]=$pdf->getPDFExtract( PDFPages=>$i ) );

# use strict;
# use warnings;
# use PDF::OCR::Thorough;

# my $filename = "10.1073-pnas.1515367112.pdf";
# my $filename = "TCS FILE.pdf";

# my $pdf = PDF::OCR::Thorough->new($filename);
# my $text = $pdf->get_text();
# print "$text";