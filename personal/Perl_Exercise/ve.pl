#!/usr/bin/perl -w
#
#                 Copyright TPG Internet Pty Ltd - 2008
#
require "XASConfig.pl";
ConfigSubsys('ias');
use strict;
require 'Security.pl';
require 'Util.pl';
require 'StatusText.pl';
require "Offline.pl";
require "MPhone.pl";
require 'User.pl';
require 'PkgDetails.pl';
require 'GRenewal.pl';
require 'GPackage.pl';
require 'Company.pl';

use vars qw (%in $BG_bad $secure_user $cacheid);

my $datetime = TodayNow1();
my ( $today, $now ) = split( / /, $datetime );

my $logfile_date = time_add2( $datetime, 0, -5, 'D' );

print HTMLTitle( "Report Generation", "" );

getList();

sub getList
{

# remove from list if more than 30 days old
my $too_old_and_must_die = time_add2( $datetime, 0, -30, 'D' );
my $logfile_name = FmtDate( $logfile_date, 'yyyymmdd' );
my %state_file   = ();

open STATEFILEIN, "</data/state/new_adsl_list";

while (<STATEFILEIN>)
{
my ( $conn_date_in, $cust_id ) = split( /,/, $_ );
chomp $cust_id;

if ( $conn_date_in gt $too_old_and_must_die )
{
$state_file{$cust_id} = { conn_date => $conn_date_in };
}

}

close STATEFILEIN;

my @rr =
 `grep -h "^D102.*ADSL has been installed" /data/dblog/sys/$logfile_name`;

foreach my $r (@rr)
{
if ( $r =~ /:ADSL([0-9]+)\./ )
{
my $cust_id = $1;

if ( !$state_file{$cust_id} )
{
$state_file{$cust_id} = { conn_date => $logfile_date };
}
}
}

getData(%state_file);
}

sub getData
{
my %state_file = @_;

my $new_counter   = 0;
my $old_counter   = 0;
my %new_customers = ();
my %old_customers = ();

my $path = "/data/cache/";

my $filename = "recent_adsl_report_$today$now.csv";

my $three_days_ago = time_add2( $datetime, 0, -3, 'D' );

open FILE, ">$path$filename";

foreach my $key ( sort( keys %state_file ) )
{
my $usage_date = 'ZZZ'; # has to be > any date

my $cust_id = $key;

my $cust_details;

if (   ( $cust_details = GetUserDetailsFromUsernum($cust_id) )
&& ( !MPPlanSearch( 20080515, $cust_id, '', '', '', 0 ) ) )
{

my %pkg_details = GetPkgDetails( $$cust_details{pkg_type} );
my $renewal     = GetPkgRenewal($cust_id);

my $pkg_counter = 0;

my @pkg = PackageSearch( 20030612, '', $cust_id, '', '' );

foreach my $pkg (@pkg)
{
$pkg_counter++;
}

my $eligable = 0;

if ( defined $pkg_details{BUNDLE}
and $pkg_details{BUNDLE} =~ /,2,/ )
{
$eligable = 1;
}
else
{
delete $state_file{$key};
}

if ( %pkg_details and $eligable )
{

$$cust_details{name_f}     =~ s/,//go;
$$cust_details{name_s}     =~ s/,//go;
$$cust_details{addr_1}     =~ s/,//go;
$$cust_details{addr_city}  =~ s/,//go;
$$cust_details{addr_state} =~ s/,//go;
$$cust_details{addr_pcode} =~ s/,//go;
$$cust_details{phone_h}    =~ s/,//go;
$$cust_details{phone_m}    =~ s/,//go;
$$cust_details{email}      =~ s/,//go;
$pkg_details{NAME}         =~ s/,//go;

if ( $pkg_counter eq 1 )
{

my $logfilename = sprintf "/data/dblog/%02d/%010d",
 $cust_id % 100, $cust_id;
my @log = `grep "^U007.*Acc1" $logfilename`;

foreach my $log (@log)
{
if ( $log =~ /^U007:([0-9]{8})/ )
{
$usage_date = $1;
last;
}
}

if ( $usage_date le $three_days_ago )
{

$new_customers{$cust_id} = {
cust_id            => $cust_id,
name_f             => $$cust_details{name_f},
name_s             => $$cust_details{name_s},
addr_1             => $$cust_details{addr_1},
addr_city          => $$cust_details{addr_city},
addr_state         => $$cust_details{addr_state},
addr_pcode         => $$cust_details{addr_pcode},
phone_h            => $$cust_details{phone_h},
phone_m            => $$cust_details{phone_m},
email              => $$cust_details{email},
pkg_name           => $pkg_details{NAME},
pkg_type           => $$cust_details{pkg_type},
contract_remaining => $$renewal{contract},
coy => $Company::NameShort[ $$cust_details{coy} ],
usage_date => $usage_date
};

$new_counter++;

delete $state_file{$key};
}

}
else
{

$old_customers{$cust_id} = {

cust_id            => $cust_id,
name_f             => $$cust_details{name_f},
name_s             => $$cust_details{name_s},
addr_1             => $$cust_details{addr_1},
addr_city          => $$cust_details{addr_city},
addr_state         => $$cust_details{addr_state},
addr_pcode         => $$cust_details{addr_pcode},
phone_h            => $$cust_details{phone_h},
phone_m            => $$cust_details{phone_m},
email              => $$cust_details{email},
pkg_name           => $pkg_details{NAME},
pkg_type           => $$cust_details{pkg_type},
contract_remaining => $$renewal{contract},
coy => $Company::NameShort[ $$cust_details{coy} ],
pkg_counter => $pkg_counter
};
$old_counter++;

delete $state_file{$key};
}

}
}
else
{

delete $state_file{$key};
}

}

my $total_records = $new_counter + $old_counter;

print FILE "Customer,";
print FILE "COY,";
print FILE "STATUS,";
print FILE "F_name,";
print FILE "S_name,";
print FILE "Address,";
print FILE "City,";
print FILE "State,";
print FILE "Pcode,";
print FILE "Phone H,";
print FILE "Phone M,";
print FILE "Email,";
print FILE "Plan,";
print FILE "Plan ID,";
print FILE "Contract remaining,";
print FILE "Date of 1st usage,";
print FILE "Past months,\n";

foreach my $key ( sort( keys %new_customers ) )
{
print FILE "$new_customers{$key}{cust_id},";
print FILE "$new_customers{$key}{coy},";
print FILE "new,";
print FILE "$new_customers{$key}{name_f},";
print FILE "$new_customers{$key}{name_s},";
print FILE "$new_customers{$key}{addr_1},";
print FILE "$new_customers{$key}{addr_city},";
print FILE "$new_customers{$key}{addr_state},";
print FILE "$new_customers{$key}{addr_pcode},";
print FILE "$new_customers{$key}{phone_h},";
print FILE "$new_customers{$key}{phone_m},";
print FILE "$new_customers{$key}{email},";
print FILE "$new_customers{$key}{pkg_name},";
print FILE "$new_customers{$key}{pkg_type},";
print FILE "$new_customers{$key}{contract_remaining},";
print FILE "$new_customers{$key}{usage_date},";
print FILE ",\n";

}

foreach my $key ( sort( keys %old_customers ) )
{
print FILE "$old_customers{$key}{cust_id},";
print FILE "$old_customers{$key}{coy},";
print FILE "old,";
print FILE "$old_customers{$key}{name_f},";
print FILE "$old_customers{$key}{name_s},";
print FILE "$old_customers{$key}{addr_1},";
print FILE "$old_customers{$key}{addr_city},";
print FILE "$old_customers{$key}{addr_state},";
print FILE "$old_customers{$key}{addr_pcode},";
print FILE "$old_customers{$key}{phone_h},";
print FILE "$old_customers{$key}{phone_m},";
print FILE "$old_customers{$key}{email},";
print FILE "$old_customers{$key}{pkg_name},";
print FILE "$old_customers{$key}{pkg_type},";
print FILE "$old_customers{$key}{contract_remaining},";
print FILE ",";
print FILE "$old_customers{$key}{pkg_counter},\n";

}

close FILE;

updateList(%state_file);

# scp to dawn

my $dawndpath = 'log';    # where we get things from
my $rc        = 0;

`scp -2 -i /data/config/ssh_keys/dawn.dsa  $path$filename adsl2mobile\@dawn.tpg.com.au:$dawndpath/$filename`;

$rc = $?;
if ( $rc != 0 )
{                         # Copy to dawn failed
$datetime = TodayNow1();
print STDERR "$datetime Problem copying $path$filename to dawn ($rc)\n";
}

`rm $path$filename`;      # Clean up
}

sub updateList
{
my %state_file = @_;

open STATEFILEOUT, ">/data/state/new_adsl_list";

foreach my $key ( sort( keys %state_file ) )
{
print STATEFILEOUT "$state_file{$key}{conn_date},$key\n";

}

close STATEFILEOUT;
}

1;