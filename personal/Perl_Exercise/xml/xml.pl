use strict;
use warnings;

use XML::LibXML;

my $filename = 'playlist.xml';

my $dom = XML::LibXML->load_xml(location => $filename);

foreach my $title ($dom->findnodes('/playlist/movie/title')) {
    print $title->to_literal() ,"\n";
}

foreach my $movie ($dom->findnodes('//movie')) {
    say 'Title:    ', $movie->findvalue('./title');
    say 'Director: ', $movie->findvalue('./director');
    say 'Rating:   ', $movie->findvalue('./mpaa-rating');
    say 'Duration: ', $movie->findvalue('./running-time'), " minutes";
    my $cast = join ', ', map {
        $_->to_literal();
    } $movie->findnodes('./cast/person/@name');
    say 'Starring: ', $cast;
    say "";
}

my $cast = join ', ', map {
        $_->getAttribute('name');
    } $movie->findnodes('./cast/person');
    say 'Starring: ', $cast;

my $cast = join ', ', map {
        $_->{name};
    } $movie->findnodes('./cast/person');
    say 'Starring: ', $cast;
	

my $op="<name>$nmae</name>
			<id><\/id>
";
	
open JJ,">fin.xml";
print JJ "$op";
close JJ;

'2021/92814','2021/92739','2021/92757'