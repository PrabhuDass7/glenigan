APP NO / APPLICANT PROPOSAL LOCATION
1
BOROUGH OF HYNDBURN
TOWN AND COUNTRY PLANNING ACT 1990
APPLICATIONS DEPOSITED
The following is a schedule of applications received during the period from Monday 05 April 2021 to Friday 9 April 2021.
The applications may be inspected at Planning Offices, Scaitcliffe House, Ormerod Street, Accrington, Lancs BB5 0PF
OR this list can be viewed
On Hyndburn Borough Council's Web Site
the link is Search Application page
S Prideaux
Chief Planning and Transportation Officer
Information for the Public
Representations about the above applications must be made within 14 days beginning with the date on which
this list is published.
The applications can be inspected at The Planning Department, Scaitcliffe House, Ormerod Street,
Accrington, BB5 0PF between the hours of 9am and 5pm Monday to Friday and on Hyndburn Borough
Council's Web Site.
Representations can be made by email (planning@hyndburnbc.gov.uk) quoting the relevant application
reference number or can be sent by post to The Planning Department, Scaitcliffe House, Ormerod Street,
Accrington, BB5 0PF. In the case of a householder application, in the event of an appeal that proceeds by
way of the expedited procedure, any representations made about the application will be passed to the
Secretary of State and there will be no opportunity to make further representations.
DATE PUBLISHED 16 04 21
"NB Due to the current coronavirus situation and social distancing the Council are not allowing members of the public to visit the
Council Office � the plans are still accessible on line but the letters received from the public are not. Sorry for this inconvenience."
APP NO / APPLICANT PROPOSAL LOCATION
2
11/21/0115 Householder: Erection of dormer extension 27 Whalley Road Great 09/04/2021
and loft conversion Harwood BB6 7TE
Mrs Catherine Whiteley
11/21/0131 Householder: Erection of two storey side 21 St Albans Road Rishton 06/04/2021
extension relocate entrance to side of BB1 4HA
property, erection of dormer on the rear roof.
Installation of 2 Velux windows to front
elevation roof
Mr M Young
11/21/0156 Householder: Variation of condition No 2 Rothwell Mill Stables 09/04/2021
(increase the roof height) pursuant to Miller Fold Avenue Accrington
11/19/0342 BB5 0NY
Mr Ian Moss
11/21/0166 Householder Prior Approval: Erection of 7 Hazel Grove Oswaldtwistle 06/04/2021
rear extension 4 m beyond the rear wall of BB1 3NB
the original dwelling house 3.20 m maximum
height 2.90 m height to eaves
Mr Hussain
11/21/0168 Householder Prior Approval: Erection of 12 Dorchester Avenue 08/04/2021
rear extension 4 m beyond the original rear Oswaldtwistle BB5 4NR
wall 4.20 m maximum height 2.4 m height to
eaves
Mr Dale Beaghan
11/21/0171 Householder: Erection of two-storey Oakenshaw Farm Bates Street 06/04/2021
extension and re-roofing of store Clayton le Moors Lancs BB5
5SU
Mr Ben Newbould
