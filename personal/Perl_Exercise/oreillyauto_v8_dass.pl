use strict;
use LWP::Simple;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
# use getcon;
use DateTime;
use IO::Socket::SSL qw( SSL_VERIFY_NONE );
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
use CACertOrg::CA;
my $time_now = DateTime->now( time_zone => "Asia/Kolkata" );

# open FJ,">21252860000_O'Reilly_Auto_Parts_Output.txt";
# print FJ "sno\tInputCity\tinputState\tInputZip\tStore_No\tStoreName\tAddress1\tAddress2\tCity\tState\tZip\tPhone\tFax\tHours_of_Operation\tContact_Person\tContact_Title\tList_Page\tDetail_PageURL\tHomepageurl\tGoogle_MapsDirectionLink\tlongitude\tlatitude\tTime\tStatus\tAddress_Block\n";
# close FJ;

# open ML,">Main_Link.txt";
# print ML "";
# close ML;

open(FH,">21252860000_O_Reilly_Auto_Parts_Output.txt");
print FH "Chain_id\tRetailer_name\tStore_No\tStore_Name\tAddress1\tAddress2\tCity\tState\tZip\tPhone\tFax\tLatitude\tLongitude\tHours_of_Operation\tContact_Person\tContact_Title\tList_Page_URL\tDetail_Page_URL\tGoogle_Maps_Direction_Link\tCollection_Time_IST\tCollection_Time_GMT\tStatus\n";
close FH;


my $ua=LWP::UserAgent->new();
$ua->agent("Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0");

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
$ua->cookie_jar($cookie_jar);

my $cook='_ga=GA1.2.171809506.1556006082; atgRecVisitorId=138F7ulqwqzfyswcZzfy8sAtNtPCQ5_M8vyH1hKoGkAisWY621A; JSESSIONID=ost-p-tcapp-www1a-7~9FDD3297A731DCB121D12963E19E55E8-n1.ost-p-tcapp-www1a-7; ga_session=e375b422-0ab7-4242-bce5-b58ad3c984d1; ActiveID=5DLL-F40E-DJHY-ZTX3-6VDO-EZ5N-ZMCG-BP96; cust_id="TvtBhKHZhGSQERQjspCPV4iFs5ZWsasXh/9psks3YAo="; _gid=GA1.2.1949146486.1564057527; ak_bmsc=5B922B27ADDD6812F8A8827BDF43A7CE170F07BADF4A0000B39F395D8A27AF60~plVnFH6pX64lR1KOcWWQHsWeQLF4VNMIgKxsprg/JrHlV1RXxIi9kQmpo8lsDn++g0VLNo1hjqAobENp2Sqs43U9QHCZdE9diGVGUJXmCYG1hLGpAoHnyxCYz9fllIzHeGFcgG//n834hAqPzYSLSySrgl8++c1YWPaDzcbxlUTBwXbznxnYUmv90qnHZiVyiAA5oF3jd9NAIr+hMo7tad6DCxySXVNnpRq2Txr9p14SMNSAnTbPAcL6KCSB9Knfzn; _fbp=fb.1.1564057528713.1738470087; LPVID=UwNjhjM2Y0NWM1ODhkODJk; LPSID-16349016=NmKYX9UmTty12iluIZX4sQ; bm_sv=BD2B41E50422A742068C54EE1B3D2F0B~WyLPBB5Rcw4JxFIZOxv4XBI8WmwthmifhGCPVU+LyJYMU9nky/ITXvrXk2i3i3pv0JkrStfYIfWXKCsZZbUG0pBczuhfwEvEtIsKsrYm+LCNQabTiKdhGQfectOe5guEnbUKHzepPhhp1VE+Ab/VDxRkf87hGygsO3/kpJKuhS4=; _4c_=jVJNj5swEP0rkc8hsQ3%2Byq1qpSqXXlarHhGYcUBLMAInNF3tf%2B%2BYkEZNo6ocLGbmzcd7M%2B9kqqEjOyZkRiWlRmQ8W5M3uIxk905sH99zfE5DS3akDqEfd9vtNE0bP0DTtpfiFPzG%2BuO29bYIje%2FG7dGTNbG%2BAsxgZmM2DO3wEy2KP%2F3gq5MNebj0ETBBuRqrNwxUcG4s5FNThXrO5PTuraE51CG6OZ3d%2FRAN%2FJuarvLTY9rivadRbdB9KPIRxhHHRN83v3q5Gqv9FwyOATntqyUSDYy3YANUGH1pDh1U%2Bb7LX0eIzU9dBa7p5mA5%2BOnq%2FVwP%2FggrJdDrUUXyfZ5kRHMAB8Mwo2K3JkT%2BDzIuAdzAn7EoHC6CxLJR6Tbm4urW5Oun%2FBXHR4qKadwglRsmBC5TUs3nrn1bXPIm8joWhnFlcM1KZYq50mWpESDKUhrpWIQv8lzhXOg0FYaCKjDDaXDCOgk6K8EI58jHmvy4Xg%2FLMsqZUhI3HfBUNB5U%2FBAxxFrzGRGZalo4yZMiK0WSuVQnRlqTFJZzSy1WdnGGuSaywd5cKyOxyLm51XBARUq5SXAcnmRM0qQ0yiaWgZOVK1OjNPk9FxUm44xrtsyFEi1j9e1Skd3BCq9Ha3YjEUnd0OcFLR4op5T%2BTfl6Dv%2FI4U9ksjeGYTjBowqaq4gJC8YV7QiPkJQh5HhX%2Bz%2B2%2FaTAYXC3LlcBlcSJU%2FUE%2FPHxCw%3D%3D';

# my $cook="";

my $url="https://restaurantguru.com/La-Cocina-de-mi-Mama-Mexico-City-2/load-meals?review_limit=1";

print "URL::$url\n";
# my $cont=getcon->Getcont("$url","","","GET","","$cook");
my $ref='https://restaurantguru.com/La-Cocina-de-mi-Mama-Mexico-City-2';	

my ($cont,$cook)=getcont($url,"","$ref","GET");
# print "Cook : $cook\n";

open FH,">Content.html";
print FH "$cont";
close FH;

print "Completed\n";
my $count=1;

if($cont=~m/<div\s*class\=\"states\-cities\-list\s*row\">([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>\s*<\/div>\s*<\/div>/is)
{
	my $main_con=$1;
	
	while($main_con=~m/<a\s*href\=\"([^>]*?)\">/igs)
	{
		my $mn_lk='https://www.oreillyauto.com'.$1;
		
		print "url2:: $mn_lk\n";
		
		open ML,">>Main_Link.txt";
		print ML "$mn_lk\n";
		close ML;
		
		open ML,">>Dump_Main_Link.txt";
		print ML "$mn_lk\n";
		close ML;
	}	
		
	

	open HH,"<Main_Link.txt";
	my @arr1=<HH>;	
	
	foreach my $mn_lk(@arr1)
	{
		chomp($mn_lk);
		# print "MNLK:: $mn_lk\n";
		$mn_lk=~s/\&\#39\;/\'/igs;
		my $cont2=getcon->Getcont("$mn_lk","","","GET","","");
		# my ($cont2,$cook)=getcont($mn_lk,"","","GET");
		
		open FH,">Content2.html";
		print FH "$cont2";
		close FH;
		
		# print "Comp2\n";
		
		
		
		if($cont2=~m/<div\s*class\=\"states\-cities\-list\s*row\">([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>\s*<\/div>\s*<\/div>/is)
		{
			my $main2=$1;
			$main2=~s/\&\#39\;/\'/igs;
			
			open ML,">Main_Link2.txt";
			print ML "";
			close ML;
			
			while($main2=~m/<a\s*href\=\"([^>]*?)\">/igs)
			{
				my $mn2_lk='https://www.oreillyauto.com'.$1;
				print "mn2_lk:: $mn2_lk\n";
				
				open ML,">>Main_Link2.txt";
				print ML "$mn2_lk\n";
				close ML;
				
				open ML,">>Dump_Main_Link2.txt";
				print ML "$mn2_lk\n";
				close ML;
				
			}	
				
			open HH,"<Main_Link2.txt";
			my @arr=<HH>;	
			
			foreach my $mn2_lk(@arr)
			{
				chomp($mn2_lk);
				print "mn2_lk:: $mn2_lk\n";
				$mn2_lk=~s/\&\#39\;/\'/igs;
				# $mn2_lk="https://www.oreillyauto.com/locations/mo/lee's-summit";
				my $cont3=getcon->Getcont("$mn2_lk","","","GET","","");
				# my ($cont3,$cook)=getcont($mn2_lk,"","","GET");
		
				open FH,">Content3.html";
				print FH "$cont3";
				close FH;
				
				# print "Comp3\n";
				my $tem_con=$cont3;
				# sleep(3);
				if($cont3=~m/<ul\s*class\=\"store\-list\">([\w\W]*?)<\/ul>/is)
				{
					my $conblks=$1;
					
					
				
				# }
				# while($conblk=~m/<a\s*class\=\"js\-fas\-details\-link\"[^>]*?href\=\"([^>]*?)\">/igs)
				
				while($conblks=~m/<li[^>]*?>([\w\W]*?)<\/li>/igs)
				{
					
					my $conblk=$1;
					my $map_lk=cond($conblk,'class\=\"js\-fas\-directions\-link\"[^>]*?href\=\"([^>]*?)\"');
					my $det_lk=cond($conblk,'<a\s*class\=\"js\-fas\-details\-link\"[^>]*?href\=\"([^>]*?)\">');
					# $det_lk="https://locations.oreillyauto.com/al/birmingham/autoparts-1132.html";					# my $det_lk=$1;
					
					# print "det_lk:: $det_lk\n";
					# print "MapLk::$map_lk\n";
					$det_lk=~s/\&\#39\;/\'/igs;
					open DD,">>Details_Lks.txt";
					print DD"$det_lk\n";
					close DD;
					
					# sleep(5);
					my $det_con=getcon->Getcont("$det_lk","","","GET","","");
					# my ($det_con,$cook)=getcont($det_lk,"","","GET");
		
					open FH,">Content4.html";
					print FH "$det_con";
					close FH;
					
					# print "Comp4\n";
					# my $store_nm=cond($det_con,'<span\s*class\=\"store\-number\">([\w\W]*?)<\/[^>]*?>');
					my $store_nm=cond($det_con,'<div\s*class\="indy-card-logo[^>]*?>([\w\W]*?)<\/div>');
					$store_nm=~s/<[^>]*?>//igs;
					print "store_nm:: $store_nm\n";
					my $ad1=cond($det_con,'<h2>\s*<span\s*class\=\"address\">([^>]*?)<\/span>');
					my $cty=cond($det_con,'<h2>\s*[\w\W]*?<span\s*class\=\"address\">([^>]*?)\,[^>]*?<\/span>\s*</h2>');
					
					my $stat=cond($det_con,'<h2>\s*[\w\W]*?<span\s*class\=\"address\">[^>]*?\,\s*([^>]*?)\s*[\d]+[^>]*?<\/span>\s*<\/h2>');
					my $zip=cond($det_con,'<h2>\s*[\w\W]*?<span\s*class\=\"address\">[^>]*?\,[^>]*?([\d]+[^>]*?)<\/span>\s*<\/h2>');
					my $phnm=cond($det_con,'<div\s*class\=\"show\-desktop\s*show\-tablet\">([^>]*?)<\/div>');
					
					
					my $hr;
					if($det_con=~m/(<div\s*class\=\"hours\">[\w\W]*?<\/div>\s*<\/div>\s*<\/div>)/is)
					{
						my $tm_bk=$1;
						while($tm_bk=~m/<div\s*class\=\"day\-hour\-row\">([\w\W]*?)<\/div>/igs)
						{
							my $h=$1;
							$hr=$hr." | ".$h;
						}
					}
					
					$hr=trim($hr);
					$hr=~s/^\|//igs;
					$hr=~s/\|$//igs;
					# my $Store_num=cond($det_lk,'([\d]+)\.html');
						# $Store_num=remove_zero($Store_num);
					if($store_nm eq "")
					{
						# my $Store_num=cond($det_lk,'([\d]+)\.html');
						# $Store_num=remove_zero($Store_num);
						if($conblk=~m/<div\s*class\=\"fas\-store\_\_info\"[^>]*?>([\w\W]*?)<\/div>\s*<\/div>/is)
						{
							my $adr=$1;
							$ad1=cond($adr,'<h2\s*class\=\"fas\-store\_\_address\-text\">([\w\W]*?)<br[^>]*?>');
							$store_nm=cond($adr,'<h2\s*class\=\"fas\-store\_\_address\-text\">[\w\W]*?<br[^>]*?>([\w\W]*?)<\/h2>');
							
							$phnm=cond($adr,'<a\s*class\=\"fas\-store\_\_phone\"[^>]*?>([\w\W]*?)<\/a>');
							$cty=cond($store_nm,'^([\w\W]*?)\,');
							$stat=cond($store_nm,'[\w\W]*?\,([\w\W]*?)$');
							
							# my $det_lk2=cond($cont3,'<a\s*class\=\"js\-fas\-details\-link\"[^>]*?href\=\"([^>]*?)\">');
							
							# $det_lk2=~s/\&\#39\;/\'/igs;
					
							
							
							my $Store_num=cond($det_lk,'([\d]+)\.html');
							$Store_num=remove_zero($Store_num);
							print "Store_num:: $Store_num\n";
							my $hm_lk='https://www.oreillyauto.com';
							open FJ,">>21252860000_O'Reilly_Auto_Parts_Output.txt";
							print FJ "$count\t\t\t\t$Store_num\t$store_nm\t$ad1\t\t$cty\t$stat\t$zip\t$phnm\tFax\t$hr\t\t\t\t$det_lk\t$hm_lk\t$map_lk\t\t\t$time_now\t\t\n";
							close FJ;
							
							open FH,">>21252860000_O_Reilly_Auto_Parts_Output.txt";
							print FH "21252860000\tO'Reilly Auto Parts\t$Store_num\t$store_nm\t$ad1\t\t$cty\t$stat\t$zip\t$phnm\t\t\t\t\t\t\t\t$det_lk\t\t\t$time_now\t\n";
							close FH;
							
							print "Detail-$count-$det_lk";
							$count++;
						
						}
					}else{
					
					my $hm_lk='https://www.oreillyauto.com';
					
					my $Store_num=cond($det_lk,'([\d]+)\.html');
					$Store_num=remove_zero($Store_num);
					$store_nm=~s/\#\s*[\d]+\s*$//igs;
					
					open FJ,">>21252860000_O'Reilly_Auto_Parts_Output.txt";
					print FJ "$count\t\t\t\t$Store_num\t$store_nm\t$ad1\t\t$cty\t$stat\t$zip\t$phnm\tFax\t$hr\t\t\t\t$det_lk\t$hm_lk\t$map_lk\t\t\t$time_now\t\t\n";
					close FJ;
					
					open FH,">>21252860000_O_Reilly_Auto_Parts_Output.txt";
					print FH "21252860000\tO'Reilly Auto Parts\t$Store_num\t$store_nm\t$ad1\t\t$cty\t$stat\t$zip\t$phnm\t\t\t\t\t\t\t\t$det_lk\t\t\t$time_now\t\n";
					close FH;
					
					print "Detail-$count-$det_lk";
					$count++;
					}
					

				}
				
				}	

			}

		}

	}
}

print "Completed\n";
sub remove_zero()
{
	my $zz=shift;
	$zz=~s/^0000000//igs;
	$zz=~s/^000000//igs;
	$zz=~s/^00000//igs;
	$zz=~s/^0000//igs;
	$zz=~s/^000//igs;
	$zz=~s/^00//igs;
	$zz=~s/^0//igs;
	return $zz;
}
sub cond()
{
	my $value=shift;
	my $regex=shift;
	if($value=~m/$regex/is)
	{
		my $data=$1;
		$data=decode_entities($data);
		$data=trim_space($data);
		$data=~s/<[^>]*?>//igs;     
		return $data;
	}
}


sub getcont()
{
    my($ur,$cont,$ref,$method)=@_;
	my $cc=0;
    # netfail:
    my $request=HTTP::Request->new("$method"=>$ur);
    $request->header("Content-Type"=>"application/json");
    # $request->header("Cookie"=>'_ga=GA1.2.171809506.1556006082; atgRecVisitorId=138F7ulqwqzfyswcZzfy8sAtNtPCQ5_M8vyH1hKoGkAisWY621A; JSESSIONID=ost-p-tcapp-www1a-7~9FDD3297A731DCB121D12963E19E55E8-n1.ost-p-tcapp-www1a-7; ga_session=e375b422-0ab7-4242-bce5-b58ad3c984d1; ActiveID=5DLL-F40E-DJHY-ZTX3-6VDO-EZ5N-ZMCG-BP96; cust_id="TvtBhKHZhGSQERQjspCPV4iFs5ZWsasXh/9psks3YAo="; _gid=GA1.2.1949146486.1564057527; ak_bmsc=5B922B27ADDD6812F8A8827BDF43A7CE170F07BADF4A0000B39F395D8A27AF60~plVnFH6pX64lR1KOcWWQHsWeQLF4VNMIgKxsprg/JrHlV1RXxIi9kQmpo8lsDn++g0VLNo1hjqAobENp2Sqs43U9QHCZdE9diGVGUJXmCYG1hLGpAoHnyxCYz9fllIzHeGFcgG//n834hAqPzYSLSySrgl8++c1YWPaDzcbxlUTBwXbznxnYUmv90qnHZiVyiAA5oF3jd9NAIr+hMo7tad6DCxySXVNnpRq2Txr9p14SMNSAnTbPAcL6KCSB9Knfzn; _fbp=fb.1.1564057528713.1738470087; LPVID=UwNjhjM2Y0NWM1ODhkODJk; LPSID-16349016=NmKYX9UmTty12iluIZX4sQ; bm_sv=BD2B41E50422A742068C54EE1B3D2F0B~WyLPBB5Rcw4JxFIZOxv4XBI8WmwthmifhGCPVU+LyJYMU9nky/ITXvrXk2i3i3pv0JkrStfYIfWXKCsZZbUG0pBczuhfwEvEtIsKsrYm+LCNQabTiKdhGQfectOe5guEnbUKHzepPhhp1VE+Ab/VDxRkf87hGygsO3/kpJKuhS4=; _4c_=jVJNj5swEP0rkc8hsQ3%2Byq1qpSqXXlarHhGYcUBLMAInNF3tf%2B%2BYkEZNo6ocLGbmzcd7M%2B9kqqEjOyZkRiWlRmQ8W5M3uIxk905sH99zfE5DS3akDqEfd9vtNE0bP0DTtpfiFPzG%2BuO29bYIje%2FG7dGTNbG%2BAsxgZmM2DO3wEy2KP%2F3gq5MNebj0ETBBuRqrNwxUcG4s5FNThXrO5PTuraE51CG6OZ3d%2FRAN%2FJuarvLTY9rivadRbdB9KPIRxhHHRN83v3q5Gqv9FwyOATntqyUSDYy3YANUGH1pDh1U%2Bb7LX0eIzU9dBa7p5mA5%2BOnq%2FVwP%2FggrJdDrUUXyfZ5kRHMAB8Mwo2K3JkT%2BDzIuAdzAn7EoHC6CxLJR6Tbm4urW5Oun%2FBXHR4qKadwglRsmBC5TUs3nrn1bXPIm8joWhnFlcM1KZYq50mWpESDKUhrpWIQv8lzhXOg0FYaCKjDDaXDCOgk6K8EI58jHmvy4Xg%2FLMsqZUhI3HfBUNB5U%2FBAxxFrzGRGZalo4yZMiK0WSuVQnRlqTFJZzSy1WdnGGuSaywd5cKyOxyLm51XBARUq5SXAcnmRM0qQ0yiaWgZOVK1OjNPk9FxUm44xrtsyFEi1j9e1Skd3BCq9Ha3YjEUnd0OcFLR4op5T%2BTfl6Dv%2FI4U9ksjeGYTjBowqaq4gJC8YV7QiPkJQh5HhX%2Bz%2B2%2FaTAYXC3LlcBlcSJU%2FUE%2FPHxCw%3D%3D');
    # $request->header("Host"=>"orangetv.orange.es");
    # $request->header("Origin"=>"http://orangetv.orange.es");
    # $request->header("Accept-Language"=>"en-US,en;q=0.5");

    if($ref ne '')
    {
   
        $request->header("Referer"=>"$ref");
    }
    if(lc $method eq 'post')
    {
   
        $request->content($cont);
    }
    my $res=$ua->request($request);
    $cookie_jar->extract_cookies($res);
    $cookie_jar->save;
    $cookie_jar->add_cookie_header($request);
	my $req_head1=$request->headers_as_string();
	# print "req_head1 :$req_head1\n";
	my $ckk;
	# if($req_head1=~m/Cookie\s*\:\s+([^>]*?)\;/is)
	if($req_head1=~m/Cookie\s*\:\s+([^>]*?)Cookie2/is)
	{
		$ckk=$1;
	}
	# print "ckk :$ckk\n";

    my $code=$res->code;
    print"\n $code";
    if($code==200)
    {
       
        my $content=$res->content();
			# $content=decode_entities($content);
        return ($content,$ckk);
    }
    elsif($code=~m/50/is)
    {
        # print"\n Net Failure";
		# print"\n $code";
        # sleep(30);
        # goto netfail;
		$cc++;
		if($cc<=2)
		{

			sleep(30);
			goto netfail;
		}
    }
    elsif($code=~m/30/is)
    {
       
		my $loc=$res->header("Location");
			# goto first;
		$loc="$ur$loc";
		
		print "\nLocation: $loc";
		my $request1=HTTP::Request->new(GET=>$loc);
        $request1->header("Content-Type"=>"text/html; charset=utf-8");
        my $res1=$ua->request($request1);
        $cookie_jar->extract_cookies($res1);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request1);
		my $req_head=$request->headers_as_string();
		# print "req_head2 :$req_head\n";
		my $ckk;
		if($req_head1=~m/Cookie\s*\:\s+([^>]*?)\;/is)
		# if($req_head1=~m/Cookie\s*\:\s+([^>]*?)\;\s*ASP.NET/is)
		{
			$ckk=$1;
		}
		
        my $content1=$res1->content();
        return ($content1,$ckk);

    }
    elsif($code=~m/40/is)
    {
		 my $content=$res->content();
			# $content=decode_entities($content);
        return $content;
        print "\n URL Not found";
    }
}



sub get_c
{
	my $link = shift;
	my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
	# my $ua = LWP::UserAgent->new( ssl_opts => {		
		# SSL_verify_mode   => 'SSL_VERIFY_NONE',
		# verify_hostnames => 1,
		# SSL_ca_file      => CACertOrg::CA::SSL_ca_file()
	# });
	$ua->agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	my $response = $ua->get($link);

	if($response->is_success) {
		my $s = $response->code;
		# my $ct = $response->decoded_content;  # or whatever
		my $ct = $response->content;  # or whatever
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		# $ct = decode_entities($ct);		
		return($ct,$s,$rurl);
	}
	else{
		my $ct= $response->status_line;
		my $s = $response->code;
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		if ($s == 500){
			my ($cont,$s1,$rur) = get_c1($link);
			return($cont,$s1,$rur);
		}else{
			return($ct,$s,$rurl);
		}		
	}
}

sub get_c1
{
	my $link = shift;
	# my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
	my $ua = LWP::UserAgent->new( ssl_opts => {		
		SSL_verify_mode   => SSL_VERIFY_NONE,
		verify_hostnames => 1,
		SSL_ca_file      => CACertOrg::CA::SSL_ca_file()
	});
	$ua->agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	my $response = $ua->get($link);

	if($response->is_success) {
		my $s = $response->code;
		# my $ct = $response->decoded_content;  # or whatever
		my $ct = $response->content;  # or whatever
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		# $ct = decode_entities($ct);		
		return($ct,$s,$rurl);
	}
	else{
		# print "Here\n";
		my $ct= $response->status_line;
		my $s = $response->code;
		my $request = $response->request();
		# my $rurl = $response->header( 'location' );
		my $rurl = $request->uri();
		return($ct,$s,$rurl);
	}
}


# sub get_c
# {
	# my $url = shift;
	# my $ua = LWP::UserAgent->new;
	# $ua->agent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	# my $response = $ua->get($url);

	# if($response->is_success) {
		# my $ct = $response->decoded_content;  # or whatever
		# return($ct);
	# }
	# else{
		# my $ct= $response->status_line;
		# return($ct);
	# }

# }

sub GetContent()
{
	my($g_url,$method,$parameter,$headers)=@_;
	my ($req,$res,$code); 
	my $debug=0;
	my $loc_count=1;
	start1:
	$req= HTTP::Request->new($method => $g_url);
	if($method=~m/POST/is)
	{
		$req->content($parameter);
	}
	$res = $ua->request($req);
	$cookie_jar->extract_cookies($res);
	$cookie_jar->save;
	$cookie_jar->add_cookie_header($req);
	
	$code=$res->code();
	print "\nCODE  ::  $code\n";
	if($code=~m/40/is)
	{
		print "\nURL missing .Page not found\n";
		if($debug<3)
		{
			sleep(10);
			$debug++;
			goto start1;
		}
		return $res->content;
	}
	elsif($code=~m/50/is)
	{
		$req=HTTP::Request->new(GET=>"http://www.cpan.org");
		$req->header("Content-Type"=> "application/x-www-form-urlencoded");
		$res=$ua->request($req);
		my $code1=$res->code();
	 	print "\nNET_CHECK :$code1\n";
		if($code1=~m/50/is)
		{
			print "\nNet Failure";
			sleep(5);
			goto start1;
		}
		elsif($code1=~m/20/is)
		{
			if($debug==3)
			{
				print "\nURL missing .Page not found\n";
				open(FH,">>URLMissingError.txt");
				print FH "Code  ::  $code\nURL  ::  $g_url\n";
				close FH;
				return;
			}
			else
			{	
				print "\nDEBUG :: $code1 :: $debug\n";
			}
			$debug++;
			print "\nDEBUG :: $debug\n";
			sleep(0);
			goto start1;
		}
		return $res->content;
	}
	elsif($code=~m/30/is)
	{
		if($loc_count>=4)
		{
			open(FH,">>URLMissingError.txt");
			print FH "Code  ::  $code\nURL  ::  $g_url\n";
			close FH;
			return;
		}
		$loc_count++;
		my $location=$res->header('location');
		my $loc_url=URI::URL->new_abs($location,$g_url);
		print "Redirect URL  ::  $loc_url\n";
		($g_url,$method)=($loc_url,'GET');
		goto start1;
	}
	elsif($code=~m/20/is)
	{
		my $g_con=$res->content;
		return $g_con;
	}
	my $g_con=$res->content;
	return $g_con;
}
sub trim_space
{	
	my $value=shift;
	$value=~s/\s+/ /igs;
	$value=~s/^\s//igs;
	$value=~s/\s$//igs;
	return $value;
}
sub trim
{
	my $val=shift;
	
	$val=~s/^\s+//igs;
	$val=~s/\s+$//igs;
	$val=~s/\s+/ /igs;
	$val=~s/\&amp\;/&/igs;
	$val=~s/\&#038\;/&/igs;
	$val=~s/\&#039\;/'/igs;
	$val=~s/\&nbsp\;//igs;
	$val=~s/\&ntilde\;/ñ/igs;
	$val=~s/^\|//igs;
	$val=~s/\|$//igs;
	$val=~s/\|\|/\|/igs;
	$val=~s/<[^>]*?>/ /igs;     
	$val=~s/\s+/ /igs;
	$val=~s/^\s//igs;
	$val=~s/\s$//igs;
	$val=~s/^\s*//igs;
	$val=~s/\s*$//igs;
	 
	return $val;
}	
