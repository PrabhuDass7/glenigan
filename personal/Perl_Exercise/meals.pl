use strict;
use warnings;
use LWP::Simple;
use LWP::UserAgent;
use HTML::TagParser;
use HTTP::Cookies;

# my $url = "https://restaurantguru.com/La-Cocina-de-mi-Mama-Mexico-City-2/load-meals?review_limit=1";
# my $ref = "https://restaurantguru.com/La-Cocina-de-mi-Mama-Mexico-City-2";

# my $url='https://planningapi.agileapplications.co.uk/api/application/21482/document';
# my $url='https://planning.agileapplications.co.uk/exmoor/application-details/21482';


my $url = "https://planningapi.agileapplications.co.uk/api/application/31327/document";
		# my $POST_URL = "https://planning.agileapplications.co.uk/rugby/application-details/31327";
		my $ref = "https://planning.agileapplications.co.uk/rugby/search-applications/results?criteria=%7B%22query%22:%22R20%2F0288%22%7D&page=1";
		
		# ($searchPageCon,$searchCode) = &Download_Utility::docPostMechMethod($POST_URL,"",$pageFetch);
	
		# print "Search_POST=>$Search_POST\n";
		# ($searchPageCon,$searchCode) =&Download_Utility::Getcontent($POST_URL,"",'planningapi.agileapplications.co.uk','application/json; charset=utf-8',$refer,'','');
		



#-------------->COOKIE AND USER AGENT
my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
my $ua=LWP::UserAgent->new;
# $ua->agent("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0");
$ua->agent("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36");
$ua->max_redirect(1);
#---------------------------------------------------

my $c = getcont($url,"",$ref,"GET");
# my $c = get($url);

print $c;

write_file($c,"op.html",">");

sub file_to_array
{
	my $fn = shift;
	open FH,"<".$fn;
	my @wa = <FH>;
	close FH;
	return @wa;
}

sub file_to_string
{
	my $fn = shift;
	open FH,"<".$fn;
	my @wa = <FH>;
	close FH;
	return "@wa";
}

sub write_file
{
    my $con = shift;
    my $fname = shift;
    my $mode = shift;
    open FD,$mode.$fname;
    print FD $con;
    close FD;

}


sub getcont()
{
    my($ur,$cont,$ref,$method)=@_;
    netfail:
    my $request=HTTP::Request->new("$method"=>$ur);
    $request->header("Host"=>"planningapi.agileapplications.co.uk");
    $request->header("Content-Type"=>"application/json; charset=utf-8");
    $request->header("Accept"=>"application/json, text/plain, */*");
    $request->header("Origin"=>"https://planning.agileapplications.co.uk");
    $request->header("x-client"=>"EXMOOR");
    $request->header("x-product"=>"CITIZENPORTAL");
    $request->header("x-service"=>"PA");
	
	# x-client: EXMOOR
# x-product: CITIZENPORTAL
# x-service: PA
	# $Content_Type='application/json; charset=utf-8';
	# $Host='planningapi.agileapplications.co.uk';
	
	# Origin: https://planning.agileapplications.co.uk

	
	
    # $request->header("Content-Type"=>"application/x-www-form-urlencoded");
    # $request->header("Cookie"=>"CFID=35231955; CFTOKEN=a102a537f89e45dc-18CB8AE8-D034-7E24-B9315C57BE0AB8CE; _ceg.s=oc3b79; _ceg.u=oc3b79; _ga=GA1.2.354458192.1471434376; _ceg.s=oc1y8l; _ceg.u=oc1y8l; BIGipServerPOOL-207.97.254.104-80=156084416.20480.0000; _gat=1; _gat_GSA_ENOR0=1");
    if($ref ne '')
    {
   
        $request->header("Referer"=>"$ref");
    }
    if(lc $method eq 'post')
    {
   
        $request->content($cont);
    }
    my $res=$ua->request($request);
    $cookie_jar->extract_cookies($res);
    $cookie_jar->save;
    $cookie_jar->add_cookie_header($request);
    my $code=$res->code;
    print"\nRESPONSE CODE:$code\n";
    if($code==200)
    {
       
        my $content=$res->content();
		#$content=decode_entities($content);
        return $content;
    }
    elsif($code=~m/50/is)
    {
        print"\n Net Failure";
        # sleep(30);
        goto netfail;
    }
    elsif($code=~m/30/is)
    {
       
       my $loc=$res->header("Location");
         print "\nLocation: $loc";
        my $request1=HTTP::Request->new(GET=>$loc);
        $request1->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8");
        my $res1=$ua->request($request1);
        $cookie_jar->extract_cookies($res1);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request1);
        my $content1=$res1->content();
        return $content1;

    }
    elsif($code=~m/40/is)
    {
		 my $content=$res->content();
		#$content=decode_entities($content);
		print "\n URL Not found";
        return $content;
        
    }
}	