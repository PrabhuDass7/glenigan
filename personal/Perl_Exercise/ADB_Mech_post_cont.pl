#----- Developer Name		: Anitha A
#----- Date 				: 22-Jan-2020
#----- Site 				: Coherent - ADB

use strict;
use LWP::Simple;
use LWP::UserAgent;
use HTML::Entities;
use HTTP::Cookies;
use POSIX 'strftime';
use Time::Piece;
use Time::Seconds;
use DateTime;
use Date::Manip;
use URI::Encode qw(uri_encode uri_decode);

use URI::URL;
use HTTP::Cookies;
use WWW::Mechanize;
use IO::Socket::SSL qw();
use Mozilla::CA;
use Cwd;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use JSON;


my $ua=LWP::UserAgent->new();
$ua->agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36");
$ua->max_redirect(0);
# my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,); 
# $ua->cookie_jar($cookie_jar);
# $cookie_jar->save;

use URI::Escape;


#--------- Post Content ----------------------

my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);               
$ua->cookie_jar($cookie_jar);
$cookie_jar->save;

#----------------------------------------------------

my @Input = file_to_array("input2.txt");
my $date=dt_to_epoch();
foreach my $input(@Input)
{
	my ($sel_type,$sel_url)=split('\t',$input);
	chomp($sel_url);
	
	print "sel_type:: $sel_type\n";
	
	my $sel_url1=uri_escape($sel_url);
	my $sel_type1=lc($sel_type);
	$date=Epp();
	my $surl="https://api.olasearch.com/intent?env=production&projectId=5910080696b7644e74cefeac&timestamp=$date";
	
	print "SURL:: $surl\n";
	print "sel_type:: $sel_type\n";
	print "sel_type1:: $sel_type1\n";
	print "sel_url1:: $sel_url1\n";
# 5910080696b7644e74cefeac projectId678cd887
	
	my $source="data=%7B%22q%22%3A%22%22%2C%22enriched_q%22%3A%22%22%2C%22page%22%3A1%2C%22per_page%22%3A10%2C%22facet_query%22%3A%5B%7B%22displayName%22%3A%22Type%22%2C%22name%22%3A%22ola_collection_name%22%2C%22type%22%3A%22checkbox%22%2C%22facetNames%22%3A%7B%7D%2C%22isRoot%22%3Atrue%2C%22multiSelect%22%3Atrue%2C%22_id%22%3A%22591128b0e792d44f39013b2b%22%2C%22isGlobal%22%3Atrue%2C%22order%22%3A0%2C%22showSelectedTag%22%3Afalse%2C%22fixedValues%22%3A%5B%5D%2C%22defaultValue%22%3A%5B%5D%2C%22limit%22%3A%2220%22%2C%22exclusions%22%3A%5B%22feature%22%5D%2C%22isCollapsed%22%3Afalse%2C%22selected%22%3A%5B%22$sel_type1%7C$sel_type%22%5D%2C%22isToken%22%3Afalse%7D%5D%2C%22filters%22%3A%5B%7B%22name%22%3A%22document%22%2C%22type%22%3A%22field%22%2C%22field%22%3A%22ola_collection_name%22%2C%22selected%22%3A%5B%22media-contacts%7CMedia+Contacts%22%2C%22operation%7COperation%22%2C%22inflation%7CInflation%22%2C%22gdp-growth%7CGDP+Growth%22%2C%22total-operations%7CTotal+Operations%22%2C%22population-below-povery-line%7CPopulation+Below+Povery+Line%22%2C%22current-account-balance%7CCurrent+Account+Balance%5Ct%22%5D%2C%22multiSelect%22%3Afalse%2C%22exclude%22%3Atrue%2C%22hidden%22%3Atrue%7D%5D%2C%22searchInput%22%3A%22url%22%2C%22skip_intent%22%3Atrue%2C%22skip_spellcheck%22%3Afalse%2C%22skip_facet_fields%22%3A%5B%5D%2C%22source%22%3Anull%2C%22tokens%22%3A%5B%5D%2C%22projectId%22%3A%225910080696b7644e74cefeac%22%2C%22env%22%3A%22production%22%2C%22view_all%22%3Afalse%2C%22referrer%22%3A%22%22%2C%22api%22%3A%22search%22%2C%22userId%22%3A%22062aa827-f99b-4559-af0a-d996bca7d849%22%2C%22userSession%22%3A%22062aa827-f99b-4559-af0a-d996bca7d849%22%2C%22searchSession%22%3A%226d521159-729f-4bde-9f8f-6107662ba0b0%22%2C%22context%22%3A%7B%22fields%22%3A%5B%5D%2C%22location%22%3Anull%2C%22isRequestingLocation%22%3Afalse%2C%22hasRequestedLocation%22%3Afalse%2C%22userSession%22%3A%22062aa827-f99b-4559-af0a-d996bca7d849%22%2C%22searchSession%22%3A%226d521159-729f-4bde-9f8f-6107662ba0b0%22%2C%22userId%22%3A%22062aa827-f99b-4559-af0a-d996bca7d849%22%2C%22isNewUser%22%3Afalse%2C%22isNewSession%22%3Afalse%2C%22href%22%3A%22$sel_url1%22%2C%22hostname%22%3A%22www.adb.org%22%2C%22pathname%22%3A%22%2Fsearch%22%2C%22referrer%22%3Anull%2C%22hasUsedVoice%22%3Afalse%2C%22filter_term_sequence%22%3A%5B%22ola_collection_name%3A$sel_type1%7C$sel_type%22%5D%7D%7D";
	
	
	#-------------Mech Post_Method ----------------

	my $pageFetch = &mech_UserAgent("Y");
	
	my ($con,$code,$filename)=docPostMechMethod($surl,$source,$pageFetch);
	print "Mech_code:: $code\n";
	print "filename:: $filename\n";
	write_file($con,'Mech_con.html','>');
	# write_file($filename,'Mec_filename.html','>');

	print "content\n";
	
	
}

sub docPostMechMethod() 
{
    my $URL = shift;
    my $postcontent = shift;
    my $mech = shift;
   
	$URL=~s/amp\;//igs;
	my $c=1;
	my $proxyFlag=0;
	rePingwith_192:
	$mech->post( $URL, Content => "$postcontent");
	  
	my $con = $mech->content;
    my $code = $mech->status;
	my $res = $mech->res();
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		$c++;
		
		goto nxt if($c == 3);
		goto rePingwith_192;
	}
	
	nxt:
	
	my $filename;
	if($res->is_success){
		$filename = $res->filename();
	}
	$filename=~s/\s+/_/igs;
    return($con,$code,$filename);
}


sub mech_UserAgent()
{
    my($currentCouncilCode) = @_;
	my $mech;

    if($currentCouncilCode eq 'N')
    {
        $mech = WWW::Mechanize->new(autocheck => 0);
    }
    else
    {	
		print "###############################\n";
        # $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$mech = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);
    }
    return($mech);
}







sub Epp
{
	my $time=time();
	print $time;
	$time=$time+3;
	$time=$time*1000;
	print $time;
	return $time;
}
sub dt_to_epoch
{
	my $string =DateTime->now->set_time_zone( 'Asia/Kolkata' );
	$string=~s/T/ /igs;
	my $unix_time = UnixDate( ParseDate($string), "%s" );
	$unix_time=$unix_time+5;
	$unix_time=$unix_time*1000;
	return $unix_time;

}
sub Add_sub
{
	my $fval=shift;
	my $sym=shift;
	my $as_value=shift;
	my $ab;
	$ab=$fval+$as_value if($sym eq 'add');
	$ab=$fval-$as_value if($sym eq 'sub');
	return $ab;
}
sub write_file
{
    my $con = shift;
    my $fname = shift;
    my $mode = shift;
    open FD,$mode.$fname;
    print FD $con;
    close FD;

}
sub file_to_array()
{
	my $fn = shift;
	open FH,"<".$fn;
	# open FH,'<:encoding(UTF-8)',$fn;
	# open(my $FH, '<:encoding(UTF-8)', $fn);
	my @wa = <FH>;
	close FH;
	return @wa;
}

sub file_to_string
{
	my $fn = shift;
	open FH,"<".$fn;
	my @wa = <FH>;
	close FH;
	return "@wa";
}
#----------- GETCONTENT
