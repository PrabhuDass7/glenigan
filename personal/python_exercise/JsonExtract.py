import requests
import json
import re
from bs4 import BeautifulSoup

url='https://brantu.com/eg-en/premoda-belt-low-waist-810-48-2174-12-beige-p'

headers = {
		"content-type": "text/html; charset=utf-8",
		"content-encoding": "br",
	}
    
s = requests.session()
s.headers.update(headers)

response = s.get(url)
soup = BeautifulSoup(response.text, "html.parser")

eventTarget = soup.find('script', {'id': 'INIT_STATE'})
json_text = re.findall(r'window\.\_\_INITIAL\_STATE\_\_\s*\=[^>]*?(\{[\w\W]*?)\)',str(eventTarget),re.I)

jsondata=str(json_text[0])
cont=json.loads(jsondata)

print ("articleNumber : ",cont['product']['product']['articleNumber'])
