test_list = [{"Akash" : 1}, {"Kil" : 2}, {"Akshat" : 3}, {"Kil" : 2}, {"Akshat" : 3}]
  
# printing original list 
print ("Original list : " + str(test_list))
  
# using naive method to 
# remove duplicates 
res_list = []
for i in range(len(test_list)):
    print (test_list[i])
    print (test_list[i + 1:])
    if test_list[i] not in test_list[i + 1:]:
    # if test_list[i] not in res_list:
        print ("Not in")
        res_list.append(test_list[i])
  
# printing resultant list
print ("Resultant list is : " + str(res_list))

# exit()

print ("*****************")

test_list = [1, 3, 5, 6, 3, 5, 6, 7]
print (set(test_list))

print (list(dict.fromkeys(test_list)))
print (test_list)

res=[]
[res.append(x) for x in test_list if x not in res]

print ("Response:", res)

res.insert(2,4)
print ("Response:", res)

res.pop()
print ("Response:", res)

res.pop(2)
print ("Response:", res)

res.remove(3)
print ("Response:", res)
# exit()

def pypart2(n):
    k = n - 1
    for i in range(0, n):
        print(' '*k,end="")
        print("*"*(2*i+1))
        k = k - 1

n = 5
pypart2(n)

# exit()
def pyfunc(r):
    for x in range(r):
        print ("x:",x)
        if x<5:
            print(' '*(r-x-1)+'*'*(2*x+1))
        # else:
            # print(' '*(r-x+1)+'*'*(3*x-x))
pyfunc(8)


# exit()

import numpy as np
arr = np.array([1, 3, 2, 4, 5])
print ("arr:",arr[:-2])
print ("arr:",arr.argsort()[-3:])
print ("arr:",arr.argsort()[::-1])
print(arr.argsort()[-3:][::-1])

input("STOp")
exit()


def bs(a):
    print ("Length Of a::",len(a))
    b=len(a)-1    # minus 1 because we always compare 2 adjacent values
    print ("Length Of b::",b)           
    for x in range(b):
        print ("a::",a)
        print ("x::",x)
        for y in range(b-x):
            print ("y::",y)
            print ("a[y]::",a[y])
            print ("a[y+1]::",a[y+1])
            if a[y]>a[y+1]:
                a[y],a[y+1]=a[y+1],a[y]
                print ("a[y]::",a[y],"a[y+1]::", a[y+1])
                print ("a::",a)
                input("STOp")
    return a

a=[32,5,3,6,7,54,87]
op=bs(a)

print ("OP::",op)