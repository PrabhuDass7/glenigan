test_dict = {'Nikhil' : { 'roll' : 24, 'marks' : 17},
			'Akshat' : {'roll' : 54, 'marks' : 12},
			'Akash' : { 'roll' : 12, 'marks' : 15}}

# print("The original dictionary : " + str(test_dict))

# res = sorted(test_dict.items(), key = lambda x: x[1]['marks'])

# print("The sorted dictionary by marks is : " + str(res))


# {'KEY1':{'name':'google','date':20100701,'downloads':0},
 # 'KEY2':{'name':'chrome','date':20071010,'downloads':0},
 # 'KEY3':{'name':'python','date':20100710,'downloads':100}}

def keyfunc(tup):
    key, d = tup
    print ("KY:",key)
    print ("VAL:",d)
    return d["marks"]

items = sorted(test_dict.items(), key = keyfunc)

print("The sorted dictionary by marks is : " + str(items))
