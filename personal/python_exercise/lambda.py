#----- Map ------------
'''The map() function executes a specified function for each item in an iterable. The item is sent to the function as a parameter.

Syntax
--------
map(function, iterables)

'''
def myfunc(n):
  return n.upper()

x = map(myfunc, ('apple', 'banana', 'cherry'))

print ("MAP::",x)

#convert the map into a list, for readability:
print(list(x))

# print(tuple(x))

#-------------------------------------------------------------

def myfunc(a, b):
  return a + b

x = map(myfunc, ('apple', 'banana', 'cherry'), ('orange', 'lemon', 'pineapple'))

print(x)

#convert the map into a list, for readability:
print(list(x))

input("**** Map - STOP ****")
# exit()

#----- Decarator -------------

'''A decorator is a design pattern in Python that allows a user to add new functionality to an existing object without modifying its structure. Decorators are usually called before the definition of a function you want to decorate'''

def our_decorator(func):
    def function_wrapper(x):
        print("Before calling " + func.__name__)
        func(x.upper())
        print("After calling " + func.__name__)
    return function_wrapper

@our_decorator 
def food(Y):
    print("Hi, Dass" + str(Y))

def foo(x):
    print("Hi, foo has been called with " + str(x))

food("Good Morning")
foo("Hi")

input("STOP")
#----- Yield -----------------

''''Yield is a keyword in Python that is used to return from a function without destroying the states of its local variable and when the function is called, the execution starts from the last yield statement. Any function that contains a yield keyword is termed a generator. Hence, yield is what makes a generator. The yield keyword in Python is less known off but has a greater utility which one can think of.

Advantages of yield: 

Since it stores the local variable states, hence overhead of memory allocation is controlled.
Since the old state is retained, the flow doesn’t start from the beginning and hence saves time.
Disadvantages of yield: 

Sometimes, the use of yield becomes erroneous if the calling of function is not handled properly.
Time and memory optimization has a cost of complexity of code and hence sometimes hard to understand the logic behind it.

'''

def nextSquare():
    i = 1
    # An Infinite loop to generate squares
    while True:
        yield i*i                
        i += 1 # Next execution resumes
                # from this point    
 
# Driver code
for num in nextSquare():
    if num > 100:
        break   
    print(num)

def print_even(test_list) :
    print ("::print_even::")
    for i in test_list:
        print ("::I::",i)
        if i % 2 == 0:
            print ("I::",i)
            yield i
 
# initializing list
test_list = [1, 4, 5, 6, 7]
 
# printing initial list
print ("The original list is : " +  str(test_list))
 
# printing even numbers
print ("The even numbers in list are : ")
for j in print_even(test_list):
    print ("j:",j)

#------------ LAMBDA -------

'''# A lambda function is a small anonymous function.
# A lambda function can take any number of arguments, but can only have one expression. '''

def myfunc(n):
    print ("a::",n)
    return (lambda a : a * n)

mydoubler = myfunc(2)

print(mydoubler(11))


# ----------------

with open(sdf.json,'w') as f:
	write(sdf.json,f)

with open ('ads.json','r') as f:
	data=json.load(f)
	data=f.read()
	
conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
cursor = conn.cursor()
cursor.execute(finalinsertQuery)
conn.commit()

import request
s=request.session()
s.headers.update(headers)
obj=s.post(URL,headers=headers,data=postcont)

obj.status
obj.content

appno=soup.find('span',{'id':'class name'}).text.strip() or get('href')

#-------------- CLASS --------------
class Car():
    def Benz(self):
        print(" This is a Benz Car ")
class Bike():
    def Bmw(self):
        print(" This is a BMW Bike ")
class Bus():
    def Volvo(self):
        print(" This is a Volvo Bus ")
class Truck():
    def Eicher(self):
        print(" This is a Eicher Truck ")
class Plane():
    def Indigo(self):
        print(" This is a Indigo plane ")
class Transport(Car,Bike,Bus,Truck,Plane):
    def Main(self):
        print("This is the Main Class")
B=Transport()
B.Benz()
B.Bmw()
B.Volvo()
B.Eicher()
B.Indigo()
B.Main()
