import requests
import os
import re
from bs4 import BeautifulSoup

def clean(cleanValue):
    try:      
        clean = ''  
        clean = re.sub(r'\n', "", str(cleanValue))
        clean = re.sub(r'\'', "''", str(clean)) 
        clean = re.sub(r'\t', "", str(clean))
        clean = re.sub(r'\\', "", str(clean))
        clean = re.sub(r'\&nbsp\;', " ", str(clean))
        clean = re.sub(r'\&amp\;', "&", str(clean))
        clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
        clean = re.sub(r'^\s+|\s+$', "", str(clean))
        clean = re.sub(r'\s\s+', " ", str(clean))
        clean = re.sub(r'^\W+$', "", str(clean))
        clean = re.sub(r'\&\#39\;', "'", str(clean))
        clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
        clean = re.sub(r'\'', "''", str(clean))
        clean = re.sub(r'^\s*', "", str(clean))
        clean = re.sub(r'\s*$', "", str(clean))
        return clean    
    except Exception as ex:
        print (ex,sys.exc_traceback.tb_lineno)

url='https://www.northerner.com/us/on-8mg-mint-mini-dry-nicotine-pouches'

res=requests.get(url)
# cont=str(res.content)
cont=res.content
with open('content.html', 'wb') as fd:
    fd.write(res.content)
    
# cont = BeautifulSoup(cont, 'html.parser')
cont = BeautifulSoup(cont, 'html5lib')
# cont=re.sub(r'\\n','',str(cont))

# Mainblks = cont.find('div', attrs = {'id':'select-qty'})
# blks = cont.findAll('div', attrs = {'class':'qty-option'})
# for blk in blks:
    # print ("blk:",blk)
    # CANS = blk.find('b').text.strip()
    # CAN=""
    # try:
        # CAN = blk.find('strong').text.strip()
    # except:
        # pass
    # tprice = blk.find('div', attrs = {'class': 'total-price'}).text.strip()
    # print ("CANS:",CANS)
    # print ("CAN:",CAN)
    
    # print ("tprice:",tprice)  

# exit()

blk=re.findall(r'id\=\"select\-qty\"[^>]*?>([\w\W]*?)<[^>]*?id\=\"product-addtocart-button[^>]*?>',str(cont),re.IGNORECASE)

blks=re.findall(r'<div\s*class\=\"qty-option[^>]*?>([\w\W]*?<[^>]*?total-price\">[\w\W]*?<\/div>\s*<\/div>)',str(blk[0]),re.IGNORECASE)

for b in blks:
    Cans=re.findall(r'<div\s*class\=\"radio\">([\w\W]*?)<\/b>',str(b),re.IGNORECASE)
    Can=re.findall(r'<strong>([^>]*?)<\/strong>',str(b),re.IGNORECASE)
    totalPrice=re.findall(r'<div\s*class\=\"total\-price\">([^>]*?)<[^>]*?>',str(b),re.IGNORECASE)
    
    CanTxt=totalPriceTxt=""
    if len(Can) > 0:
        CanTxt=clean(Can[0])
        
    if len(totalPrice) > 0:
        totalPriceTxt=clean(totalPrice[0])
   
    print ("Cans : ",clean(Cans[0]))
    print ("Can : ",CanTxt)
    print ("totalPrice : ",totalPriceTxt)
    print ("***************************")


