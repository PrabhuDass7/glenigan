#!/usr/bin/env python
# -*- coding: utf-8 -*-
#----------- Developer         :: PrabhuDass.M ----------
#----------- Project_Name      :: j-platpat (IPDL) -------
#----------- Creation_Date     :: 24-May-2019 ----------
#----------- Last_Modified_Date:: 11-Nov-2019 ------

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import time
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import codecs
import re
import urllib2
import requests
import base64
from os import path
import sys
 
reload(sys)
sys.setdefaultencoding('utf-8')

def down_file(src,ext,fname):
##     filedata = urllib2.urlopen(src)
##     headers = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' }
     r = requests.get(src)
     time.sleep(5)
##     req = urllib2.Request(src, None, headers)
##     html = urllib2.urlopen(req).read()
     with open(fname+"."+ext,'wb') as f:
          f.write(r.content)
     
def wf(f_name,r_type,f_encoding,string):    
    var2 = codecs.open(f_name,r_type,encoding=f_encoding)
    var2.write(string)
    var2.close

def r_f_a(location):
    x = codecs.open(location,'r',encoding='utf-8')
    sx = x.readlines()
    return sx

def trim(str1):
     str1 = re.sub(r'\s$', '', str1)
     str1 = re.sub(r'^\s', '', str1)
     return str1

def get_dn(str1,s):
     si = str1.find(s)
     return str1[:si]

def down_img(src,ext,fname):
     urllib.request.urlretrieve(src, fname + "." + ext)


def wait_for_load(fnm,driver,driver1,idv):
     try:
##          print "WAIT_FUN::"
          s = driver1.find_element_by_tag_name(idv)
          s1 = -1
          t = 0
          while s1 != t:
               driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

##               scroll = driver.find_element_by_tag_name('body')  #----scroll_Down--------
##               for i in range(0,2):
##                  time.sleep(2)
##                  scroll.send_keys(Keys.PAGE_DOWN)
                  
               time.sleep(20)
               try:
                    s = driver1.find_element_by_tag_name(idv)
                    j = s.get_attribute('innerText')
                    js = j.split('\n')
                    jl = len(js)
                    print "LENGTH:: TXT::",jl

                    if jl > t:
                         t = jl
                    else:
                         t = -1
               except:
                    try:
                         nx=driver.find_element_by_id('contents')
                         nx=nx.get_attribute('innerText')
                         
                         if (nx.find('The Server is busy now') != -1):
                                 print "*** The Server is busy now ***"
                                 driver.close()
                                 break
                         else:
                              print "Loading......"
                    except:
                         break
          print "Wait Finished"
     except Exception as e:
           print("*****")
		

# --- Single Folder Create ----


def Create_Directory(fnm):
     mdr=str(path.exists(fnm))

     if mdr=='False':
         os.mkdir(fnm)
         print "Folder_Created"
     else:
         print "Already Exits Folder"

     ppa=os.getcwd()
     file_dir = ppa+'/'+fnm+'//'
     return file_dir

#---- Sub_Folder_Creation ----

def Create_Sub_Directory(Main_folder_name,sub_folder_name): 
    ##current_dir = os.getcwd()
    complete_path = os.path.join(Main_folder_name, sub_folder_name)
     
    # Setup Output Folder
    if not os.path.isdir(complete_path):
            print ("Path doesn't exist, creating one")
            try:
                    os.makedirs(complete_path)
                    if not os.path.exists(complete_path):
                            print ("Made a new folder named %s" %complete_path)
                    else:
                            print ("Tried creating the folder but path doesn't exist")
            except OSError as exc: 			# Guard against race condition
                    print ("Didn't create a new folder, error occurred")
    else:
            print ("Folder already exists")

    return complete_path+'/'

def save_img(images):
    for image in images:        
        img_URL = 'https://www.j-platpat.inpit.go.jp'+str(image)
        resource = urllib.urlopen(img_URL)
        output = open("file01.jpg","wb")
        output.write(resource.read())
        output.close()

def getdata(fnm,tan):
     
     Start_Time=time.strftime("%b %d %Y %H:%M:%S")

     url="https://www.j-platpat.inpit.go.jp/"
     driver.get(url)
     time.sleep(7)
     driver.maximize_window()
     time.sleep(5)

     driver.find_element_by_xpath('//*[@id="mat-radio-3"]/label/div[2]').click()
     time.sleep(5)
     
##     driver.find_element_by_id('cfc001_header_lnkLangChange').click()                #Language_Change
     
     langx=driver.find_element_by_id('cfc001_header_lnkLangChange').get_attribute('innerText')
##     print "Language ::",langx

     if (langx.find('English') != -1):
          driver.find_element_by_id('cfc001_header_lnkLangChange').click()
          
     time.sleep(5)

     elem = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.ID, 's01_srchCondtn_txtSimpleSearch'))) #input_Box
     elem.send_keys(fnm)
     time.sleep(3)

     file_name=Create_Directory(tan)
     img_dir=Create_Sub_Directory(tan,"Img_Folder")
     html_files=Create_Directory("HTML_FILES")

     driver.find_element_by_id('s01_srchBtn_btnSearch').click()  #Search Button

     time.sleep(5)
     tb=driver.find_element_by_id('p0107_docDispScreenFormal_radioArea')   #Ratio_block

     td=tb.find_element_by_class_name('mat-radio-container').click()    #txt_formate
     time.sleep(5)

##     print "TXT_CLICKED"     
     tbl=driver.find_element_by_class_name('linetable-format-list')

     tbody=tbl.find_element_by_tag_name('tbody')
     tr=tbody.find_elements_by_tag_name('tr')

     for trw in tr:
          aplnm=trw.find_element_by_id('patentUtltyIntnlNumOnlyLst_tableView_appNumArea')      #Appli_num
          aplnmx=trim(aplnm.get_attribute('innerText'))
          aplnmx=re.sub(r'-','',aplnmx)
          
          pubnm=trw.find_element_by_id('patentUtltyIntnlNumOnlyLst_tableView_publicNumArea')   #publication_num
          pubnmx=trim(pubnm.get_attribute('innerText'))
          pubnmx=re.sub(r'-','',pubnmx)
          
          regnm=trw.find_elements_by_id('patentUtltyIntnlNumOnlyLst_tableView_examPubNumArea')[1]       #table_of_reg_num
          regnmx=regnm.get_attribute('innerText')

          regnmx=trim(regnmx)
          regnmx=re.sub(r'-', '', regnmx)

          if(regnmx.find(fnm)!=-1):
               regnm.click()
               break
          elif(aplnmx.find(fnm)!=-1):
               pubnm.click()
               break
          elif(pubnmx.find(fnm)!=-1):
               pubnm.click()
               break

     driver.close()
     time.sleep(3)
     driver.switch_to_window(driver.window_handles[-1])
     time.sleep(5)

     # ---------- Section click ------
     
     bc=0
     btn=driver.find_elements_by_class_name('l-toggle__header__button--auto')   #section_block
     btn[0].click()
     time.sleep(5)
     for j in range(0,len(btn)-1):
          bc=bc+1
          vb=driver.find_elements_by_class_name('is-active')[bc] #section_Title
          tit=vb.get_attribute('innerText')

          if (tit.find('Detailed Description') != -1) or (tit.find('Scope of Claims') != -1) or (tit.find('Drawings') != -1):
               btn[j].click()
               time.sleep(7)
               wait_for_load(fnm,driver,vb,"sdo")            #Waiting for Loading
##               print "LOOPPPP ****"

     #--- Data_point---
          
     time.sleep(5)

     op_full=str(tan) + '_Full' + '.html'
     op_Claim=str(tan) + '_Claim' + '.html'
     op_Desc=str(tan) + '_Desc' + '.html'
     claim_html=""
     dec_html=""
     
     pc=0
     cnm=driver.find_elements_by_class_name('is-active')
     for ccn in cnm:
          pc=pc+1
          if pc>=2:
               hd=ccn.find_element_by_tag_name('h3').get_attribute('innerText')
               hd=trim(hd)
               
               if hd == 'Scope of Claims':
##                    time.sleep(3)
                    claim_txt=ccn.find_element_by_tag_name('sdo').get_attribute('innerText')
                    claim_html=ccn.find_element_by_tag_name('sdo').get_attribute('innerHTML')
                    claim_html = re.sub('src="/gazette_work','src="https://www.j-platpat.inpit.go.jp/gazette_work',str(claim_html))

                    y = codecs.open(file_name+op_Claim,'w',encoding='utf-8')
                    y.write(claim_html+"\n")
                    y.close()
##                    time.sleep(1)

               if hd =='Detailed Description':
##                    time.sleep(3)
                    dec_txt=ccn.find_element_by_tag_name('sdo').get_attribute('innerText')
                    dec_html=ccn.find_element_by_tag_name('sdo').get_attribute('innerHTML')

                    data = re.findall('<txf\s*fr[^>]*?>\s*([^<]*?)\s*<br>\s*<img\s*style\s*[^>]*?src="([^>]*?)">',str(dec_html))
                    dec_html = re.sub('src="/gazette_work','src="https://www.j-platpat.inpit.go.jp/gazette_work',str(dec_html))

                    for i in data:
                         tnm,imgs = i
                         tnm=tnm.replace(']','')
                         tnm=tnm.replace('[','')
                         tnm=trim(tnm)

                         imgs='https://www.j-platpat.inpit.go.jp/'+imgs
                         
                         r = requests.get(imgs)
                         with open(img_dir+tnm+'.png', "wb") as f:
                              f.write(r.content)

                   
                    a = codecs.open(file_name+op_Desc,'w',encoding='utf-8')
                    a.write(dec_html+"\n")
                    a.close()
##                    time.sleep(1)
    
               lts=''     
               if hd =='Drawings':
                    mx=ccn.find_elements_by_tag_name('txf')
                    for mm in mx:
                         mxx=mm.get_attribute('innerText')
##                         print mxx
                         mxx=mxx.replace(']','')
                         mxx=mxx.replace('[','')
                         mxx=mxx.replace('\n','|')
                         mxx=trim(mxx)
                         mxs=mxx.split("|")
                         img_nm=mxs[0]
                         img_nm=trim(img_nm)
                         mlk=mm.find_element_by_tag_name('img').get_attribute("src")
                         r = requests.get(mlk)

                         with open(img_dir+img_nm+'.png', "wb") as f:
                              f.write(r.content)


     z = codecs.open(file_name+op_full,'w',encoding='utf-8')
     z.write(claim_html+"\n"+dec_html+"\n")
     z.close()
##     time.sleep(1)
               
     src = driver.page_source
     cur_url=driver.current_url

     src = re.sub('src="/gazette_work','src="https://www.j-platpat.inpit.go.jp/gazette_work',str(src))
                    
     wf(html_files+tan+'.html',"w","utf-8",src)

     endtime = time.strftime("%b %d %Y %H:%M:%S")
     
     z=codecs.open('log_file.txt','a',encoding='utf-8')
     z.write(fnm+"\t"+tan+"\t"+cur_url+"\t"+Start_Time+"\t"+endtime+"\n")
     z.close()


##driver=webdriver.Chrome("C:/Python27/Lib/site-packages/selenium/webdriver/chrome/chromedriver_win32/chromedriver.exe")
driver=webdriver.Chrome("chromedriver.exe")

var = codecs.open("input.txt", "r",encoding="utf-8",errors="ignore")
var1 = var.readlines()

count=0
for fnms in var1:
     fnm,tan=fnms.split('\t')
     fnm=trim(fnm)
     tan=trim(tan)
     count=count+1
     print "*** Count :: ",count," *** Input :: ",fnm
     temp=0
     bc=0
     while(1):
          
          try:
               getdata(fnm,tan)
               temp=0
               
          except Exception as e:
               z=codecs.open('Exception.txt','a',encoding='utf-8')
               z.write(fnm + "\t" + tan + "\t" + str(e) + "\n")
               z.close()

               driver.close()

               if (bc>3):
                    z=codecs.open('Skip_input.txt','a',encoding='utf-8')
                    z.write(fnm + "\t" + tan + "\t"+ str(e) + "\n")
                    z.close()
                    print "******** Browser is spleeping Mode in 30 Mins ... Please Wait for Execution **********"

                    time.sleep(600)
                    temp=0
               
               else:
                    temp=1
                    time.sleep(10)

               driver=webdriver.Chrome("chromedriver.exe")
               time.sleep(5)
               
          if(temp == 0):
               break

          bc=bc+1     

    
