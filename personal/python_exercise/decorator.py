def print_even(test_list) :
    print ("::print_even::")
    for i in test_list:
        # print ("::I::",i)
        if i % 2 == 0:
            # print ("I::",i)
            yield i
            # return i
 
# initializing list
test_list = [1, 4, 5, 6, 7]
 
# printing initial list
print ("The original list is : " +  str(test_list))
 
# printing even numbers
print ("The even numbers in list are : ")

for j in print_even(test_list):
    print ("j:",j)

input("STOP")
exit()


def our_decorator(func):
    def function_wrapper(x):
        print("Before calling " + func.__name__)
        func(x)
        print ("X::",x)
        print("After calling " + func.__name__)
    return function_wrapper


@our_decorator 
def food(Y):
    print("Hi, Dass " + str(Y))

 
def foo(x):
    print("Hi, foo has been called with " + str(x))

food("Hid")
foo("Hi")

input("STOP")
exit()



# for i in range(0,5):
    # for j in range(0,i+1):
        # print ('*',end=" ")
    # print ()

# i=0
# while(i<5):
    # for j in range(0,i+1):
        # print ('*',end=" ")
    # print ()
    # i+=1

# i=0
# while(i<5):
    # j=0
    # while(j<i):
        # print ('*',end=" ")
        # j+=1
    # print()
    # i+=1


# emp 1 and emp2 

# emp1 =no, name, des 
# emp2 =no, sala,

 # no, name, des, salary