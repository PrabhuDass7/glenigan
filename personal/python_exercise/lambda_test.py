l=[1,2,3,4]

def func(n):
    y = lambda y : y * n
    return y

for i in l:
    # op=func(i)
    # y=i
    y = lambda y : y * y
    
    print ("OP:",y(i))

#--------------------------------

op=list(map(lambda n:n*n,l))
print (op)