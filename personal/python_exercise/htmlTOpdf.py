# -*- coding: utf-8 -*-
import requests
import pdfkit
import os,sys,re
from bs4 import BeautifulSoup

# config = pdfkit.configuration(wkhtmltopdf='C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe')
# pdfkit.from_file('asp-dot-net-mvc-architecture.html','asp-dot-net-mvc-architecture.pdf',configuration=config)

# exit()
# reload(sys)
# sys.setdefaultencoding("utf-8")

def clean(cleanValue):
	try:
		clean='' 
		clean = re.sub(r'\n', " ", str(cleanValue)) 
		clean = re.sub(r'\r', " ", str(cleanValue)) 
		clean = re.sub(r'\n+', " ", str(cleanValue))
		clean = re.sub(r'\s+', " ", str(cleanValue))
		clean = re.sub(r'\t', "", str(cleanValue))
		clean = re.sub(r'\\', "", str(cleanValue))
		clean = re.sub(r'\&nbsp\;', " ", str(cleanValue))
		clean = re.sub(r'^\s+|\s+$', "", str(cleanValue))
		clean = re.sub(r'\s\s+', " ", str(cleanValue))
		clean = re.sub(r'^\W+$', "", str(cleanValue))
		clean = re.sub(r'\&\#39\;', "\'", str(cleanValue))
		clean = re.sub(r'(?:[\,\s])+$', "", str(cleanValue))
		clean = re.sub(r'\'', "\'\'", str(cleanValue))
		clean = re.sub(r'^\s*', "", str(cleanValue))
		clean = re.sub(r'\s*$', "", str(cleanValue))
		
		return clean	

	except Exception as ex:
		print (ex,sys.exc_traceback.tb_lineno)

config = pdfkit.configuration(wkhtmltopdf='C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe')

url='https://dotnettutorials.net/course/asp-dot-net-mvc-tutorials/'
s=requests.session()

respond=s.get(url)

print ("respond Status : ",respond.status_code)
page_content=respond.content

with open("page_content.html", 'wb') as fd:
	fd.write(page_content)


LinkBlock=re.findall('<[^>]*?class\=\"\s*widget\s*widget\_course\_syllabus\s*\">[^>]*?>([\w\W]*?)<\/aside>',str(page_content),re.IGNORECASE)


lists=re.findall('(<[^>]*?class\=\"\s*lesson\-title\"\s*>[\w\W]*?<\/a>)',str(LinkBlock[0]),re.IGNORECASE)

for count,blks in enumerate(lists):
	blk=re.search('<[^>]*?class\=\"\s*lesson\-title\"\s*>\s*<a\s*href\=\"([^>]*?)\">([\w\W]*?)<\/a>',str(blks),re.IGNORECASE)
	
	sy_url = blk.group(1)
	sy_name = blk.group(2)
	
	sy_url=clean(sy_url)
	sy_name=clean(sy_name)
	print ("Count : ",count ,"Of", len(lists))
	print ("Syllabus URL : ",sy_url)
	print ("Syllabus Name : ",sy_name)
	
	pdfName=re.findall('.*\/([^>]*?)\/',sy_url,re.IGNORECASE)
	pdfName=str(pdfName[0])
	print ("pdfName : ",pdfName)
	
	# input("STOP")
	
	respond=s.get(sy_url)

	print ("respond Status : ",respond.status_code)
	Inner_page_content = BeautifulSoup(respond.content, "html.parser")
	Inner_page_content = Inner_page_content.encode("ascii", "ignore")
	Inner_page_content = Inner_page_content.decode()

	with open(pdfName+str('.html'), 'w') as fd:
		fd.write(Inner_page_content)
	
	MainBlock=re.findall('<[^>]*?id\=\"main\"[^>]*?>[\w\W]*?(<article[^>]*?>[\w\W]*?<\/article>)',str(Inner_page_content),re.IGNORECASE)

	MainBlock=clean(MainBlock[0])

	pdfkit.from_string(MainBlock,pdfName+str('.pdf'),configuration=config)
	# pdfkit.from_file(pdfName+str('.html'),pdfName+str('.pdf'),configuration=config)
	# pdfkit.from_file('asp-dot-net-mvc-architecture.html',pdfName+str('.pdf'),configuration=config)
	# pdfkit.from_url(sy_url,pdfName+str('.pdf'),configuration=config)








