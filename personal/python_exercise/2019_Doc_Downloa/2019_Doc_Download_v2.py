import requests
from datetime import datetime, timedelta
import codecs
import pyodbc
import sys
import re
import json
import os
import shutil
import hashlib 
import traceback
import datetime
import html
import js2py
import logging
from parsel import Selector
from urllib.parse import urljoin

# reload(sys)
# sys.setdefaultencoding("utf-8")

x = datetime.datetime.now()
Downloaded_date=(x.strftime("%m/%d/%Y %H:%M"))
date1=(x.strftime("%d_%m_%Y"))
print (Downloaded_date)

def get_checksum(input_code):
    """
    To get the checksum id using the JS code

    Parameters
    ----------
    input_code : str
       documentData's  UriPattern

    Returns
    -------
    result : int(checksum)
    """

    # JS code to generate the checksum
    js_code = \
        """function(n) {
        var t = 0;
        if (n.length == 0)
            return t;
        for (i = 0; i < n.length; i++)
            char = n.charCodeAt(i),
            t = (t << 5) - t + char,
            t = t & t;
        return t
    }"""
    f = js2py.eval_js(js_code)
    return f(input_code)

# Clean function
def clean(cleanValue):
    try:      
        clean = ''  
        clean = re.sub(r'\n', "", str(cleanValue)) 
        clean = re.sub(r'\'', "''", str(clean)) 
        clean = re.sub(r'\t', "", str(clean))
        clean = re.sub(r'\\', "", str(clean))
        clean = re.sub(r'\&nbsp\;', " ", str(clean))
        clean = re.sub(r'\&amp\;', "&", str(clean))
        clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
        clean = re.sub(r'^\s+|\s+$', "", str(clean))
        clean = re.sub(r'\s\s+', " ", str(clean))
        clean = re.sub(r'^\W+$', "", str(clean))
        clean = re.sub(r'\&\#39\;', "'", str(clean))
        clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
        clean = re.sub(r'\'', "''", str(clean))
        clean = re.sub(r'^\s*', "", str(clean))
        clean = re.sub(r'\s*$', "", str(clean))
        return clean    
    except Exception as ex:
        print (ex,sys.exc_traceback.tb_lineno)
        
def execute_query(dbh,insertquery):
    # print (dbh)
    # print (insertquery)
    dbcount=1
    while True:
        try:        
            cursor = dbh.cursor()
            cursor.execute(insertquery)
            dbh.commit()
            break
        except Exception as e:
            print ("Query execution Failed",traceback.format_exc())
            if dbcount<=3:
                dbcount+=1
                continue
            else:
                with open(Log_directory+'/Failedquery_'+date1+'.txt','a') as FH:
                    FH.write(str(insertquery)+'\n')
                break
                
def dbConnection(): 
    dbcount=1
    while True:
        try:
            dbh = pyodbc.connect('DRIVER={SQL Server};SERVER=10.101.53.25;DATABASE=SCREENSCRAPPER;UID=User2;PWD=Merit456')
            break
        except Exception as e:
            print ("Connection Failed",str(e))
            if dbcount<=3:
                dbcount+=1
                continue
            else:
                break
    return (dbh)

def test_dbConnection():
    try:
        dbh1 = pyodbc.connect('DRIVER={SQL Server};SERVER=172.27.137.184;DATABASE=SCREENSCRAPPER;UID=User2;PWD=Merit456')
    except Exception as e:
        print ("Connection Failed",str(e))
    return (dbh1)
    
def PDF_FileCreation(Doc_url,Doc_Desc,document_type,pdfcount):  
    Filetype_check=re.match('\.([\w]{3,4})\s*$',Doc_url,re.I)
    if Filetype_check:
        Filetype=Filetype.group(1)
    else:
        Filetype='pdf'
    print ("Filetype",Filetype)
    if Doc_Desc !='':
        PDF_name=str(pdfcount)+'_'+str(Doc_Desc)+'.'+str(Filetype)      
    else:
        PDF_name=str(pdfcount)+'_'+str(document_type)+'.'+str(Filetype)
    
    PDF_name=re.sub('\.\.+','.',str(PDF_name))  
    PDF_name=re.sub('\W+','_',str(PDF_name))
    PDF_name=re.sub('\_\_+','_',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)pdf)+','.pdf',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)docx)+','.docx',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)doc)+','.doc',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)xlsx)+','.xlsx',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)xls)+','.xls',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)rtf)+','.rtf',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)jpg)+','.jpg',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)jpeg)+','.jpeg',str(PDF_name))
    PDF_name=re.sub('((?:\.|_)png)+','.png',str(PDF_name))
    
    print ('PDF_name:::',PDF_name)
    return(PDF_name)

def Retrieve_MD5(dbh,council_code,FormatID):
    query='select PDF_MD5 from TBL_PDF_MD5 where council_code='+str(council_code)+' and format_id='+str(FormatID)
    dbcount=1
    while True:
        try:
            cursor = dbh.cursor()
            cursor.execute(query)
            MD5_record = cursor.fetchall()
            MD5List=[]
            if len(MD5_record) !=0:     
                for MD5_rec in MD5_record:
                    MD5List.append(MD5_rec[0])
            break
        except Exception as e:
            print ("Connection Failed",str(e))
            if dbcount<=3:
                dbcount+=1
                continue
            else:
                break
    return (MD5List)
    
def status_code(D_code):
    if D_code == 200:
        download_status='Y'
    else:
        download_status='N'
    return(download_status)
    
def Retrieve_Input(dbh,council_code,Format_ID):     
    try:
        if Format_ID:
            selectquery="select id,SOURCE ,Application_No ,Document_Url ,MARKUP ,URL, No_of_Documents,case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME from (select ID,Source,Application_No,Document_Url,F.MARKUP,URL,No_of_Documents, isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME from FORMAT_PUBLIC_ACCESS f,L_council co where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-350,112) and CONVERT(Varchar(10),GETDATE(),112) And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = "+council_code+" And co.COUNCIL_CD_P!='486' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor') and ID in ("+Format_ID+")) as c ORDER BY ID DESC"          
        else:
            selectquery="select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents, case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME  from (select ID,Source,Application_No,Document_Url,F.MARKUP,URL,No_of_Documents,    isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id, isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id, isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME  from FORMAT_PUBLIC_ACCESS f,L_council co where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)    And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = "+council_code+" And co.COUNCIL_CD_P <> '486'  And co.COUNCIL_CD_P <> '20'  and All_Documents_Downloaded = 'N' and f.MARKUP in ('HOUSE','LARGE','Smalls')  and (CONVERT(varchar,f.oa_scraped_date ,103)<>CONVERT(varchar,getdate(),103) or f.oa_scraped_date is null)) as c where  project_status <> 'Processed Complete' ORDER BY ID DESC"
        cursor = dbh.cursor()
        cursor.execute(selectquery)
        records = cursor.fetchall()
    except Exception as e:
        print("Retrieving input error: ", traceback.format_exc())
    return (records)        

def Download(Document_Url,Doc_url,PDF_name,temp_Download_location,Markup,MD5_lists):    
    
    Files_avil_in_Temp_Loc = [x for x in os.listdir(temp_Download_location) if x.endswith(".pdf")]
    downcount=0
    MD5=code=Err=response1=''
    
    try:
        if PDF_name in Files_avil_in_Temp_Loc:
            print ('File Already downloaded')
            code=600
        else:
            count=1
            res_con=''
            while True:
                try:
                    headers = {'Host': '137.191.225.173',
                              'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                              'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'}
                    
                    response1 = requests.get(Doc_url, headers=headers)
                    
                    # preparing file name
                    file_name = ''
                    try:
                        content_disposition = \
                            response1.headers.get('Content-Disposition')
                        file_name = \
                            re.findall('filename\=(?:\"|\')?([^>]*?)(?:\"|\')?\s*$'
                                       , content_disposition)[0]
                        file_name = '_' + file_name.replace(r'\s+', '_')
                        file_name = os.path.splitext(file_name)
                        file_name=file_name[1]
                    except:
                        file_name = '.pdf'
                    
                    PDF_name = str(PDF_name)+str(file_name)
                    print ("PDF_name::",PDF_name)
                    code=response1.status_code
                    print ('response1',code)
                    if code==200:
                        res_con=response1.text                          
                        break
                    else:
                        if count <= 3:
                            count+=1                                
                            continue
                        else:
                            break
                except Exception as ex:
                    Err='Document Download Ping Error'
                    # print("Document download error: ", traceback.format_exc())
                    break
            if code == 200:
                try:
                    result = hashlib.md5(res_con.encode()) 
                    MD5=result.hexdigest()              
                    print ('MD5:::',MD5)
                except Exception as ex:
                    try:
                        result1 = hashlib.md5(PDF_name.encode()) 
                        MD5=result1.hexdigest()             
                        print ('MD5pdf:::',MD5)
                    except Exception as ex:
                        Err='PDF MD5 conversion Error'
        if code == 200:
            if MD5 in MD5_lists:
                print ('Doc exists\n')
                MD5=''
                code=600
            else:           
                print ('Doc not Exist!!!!!\n')
                if code == 200:                     
                    size = len(response1.content)
                    return_size_kb=size/1024
                    return_size_kb='{:.2f}'.format(return_size_kb)
                    print ('return_size_kb:::::',return_size_kb)
                    Download_Location=temp_Download_location+'/'+str(PDF_name)
                    with open(str(Download_Location), 'wb') as handle:
                        handle.write(response1.content)
                    downcount+=1                    
    except Exception as ex:
        Err='Document Download Ping Error'
        # print("Document download error: ", traceback.format_exc())
    return (downcount,MD5,code,Err) 
    
def Download_Document(Doc_Details,NO_OF_DOCUMENTS,dbh,councilcode,Formatid,Markup,Document_Url):
    Doc_count=len(Doc_Details)
    print ('Doc::::',Doc_count)
    print ('NO_OF_DOCUMENTS::::',NO_OF_DOCUMENTS)
    No_Of_Doc_Downloaded=0
    MD5_InsertQueryList=Insert_Document_List=''
    if NO_OF_DOCUMENTS == None:
        NO_OF_DOCUMENTS=0
    else:
        NO_OF_DOCUMENTS=NO_OF_DOCUMENTS
    temp_Download_location=Data_directory+'/'+str(Formatid)
    if not os.path.isdir(temp_Download_location):
        os.makedirs(temp_Download_location)
    process=Flag=''
    MD5_value=[]
    if int(Doc_count) > int(NO_OF_DOCUMENTS):
        MD5_value=Retrieve_MD5(dbh,councilcode,Formatid)
        process=1
    # else:
        # Flag=1
            
    update_query='' 
    One_App_Name=Comments=One_App_Avail_Status=''
    if process == 1:
        for key in Doc_Details:
            Doc_url=key
            Doc_dets=Doc_Details[key]
            Doc_Type=Doc_dets[0]
            PDF_name=Doc_dets[1]
            DateReceived=Doc_dets[2]
            Doc_Desc=Doc_dets[3]
                    
            print ('Doc_url:::',Doc_url)
            print ('Doc_Type:::',Doc_Type)
            print ('PDF_name:::',PDF_name)
            print ('DateReceived:::',DateReceived)
            print ('Doc_Desc:::',Doc_Desc)
            
            pdfsearch=re.search('^[^>]*?(without_personal_data)[^>]*?$',PDF_name,re.I)
            if pdfsearch:
                pdfsearch=pdfsearch.group(1)
                PDF_name=re.sub(pdfsearch,'Application_Form_without_personal_data',PDF_name)
            
            if Markup.lower() == 'large':
                (downcount,MD5,code,Err)=Download(Document_Url,Doc_url,PDF_name,temp_Download_location,Markup,MD5_value)                
                download_status=status_code(code)
                Comments+=Err
                PDF_AppMatch=re.search('\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:redacted|Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*',PDF_name,re.I)
                PDF_NegMatch=re.search('COVER[ING]+_?LETTER',PDF_name,re.I)
                
                if PDF_AppMatch and not PDF_NegMatch:   
                    One_App_Avail_Status='Y'
                    One_App_Name=PDF_name
                
                if MD5 != '':
                    MD5_InsertQuery='(\''+str(Formatid)+'\',\''+str(councilcode)+'\',\''+str(MD5)+'\',\''+str(PDF_name)+'\'),'      
                    MD5_InsertQueryList+=str(MD5_InsertQuery)
                Doc_Desc=re.sub('\'','\'\'',str(Doc_Desc))  
                Insert_Document='(\''+str(Formatid)+'\',\''+str(PDF_name)+'\',\''+str(Doc_Type)+'\',\''+str(Doc_Desc)+'\',\''+str(Downloaded_date)+'\',\''+str(DateReceived)+'\',\''+str(download_status)+'\'),'
                Insert_Document_List+=str(Insert_Document)                  
                No_Of_Doc_Downloaded+=downcount
            else:
                PDF_AppMatch=re.search('\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:redacted|Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*',PDF_name,re.I)
                PDF_NegMatch=re.search('COVER[ING]+_?LETTER',PDF_name,re.I)
                if PDF_AppMatch and not PDF_NegMatch:                   
                    One_App_Avail_Status='Y'
                    One_App_Name=PDF_name
                    (downcount,MD5,code,Err)=Download(Document_Url,Doc_url,PDF_name,temp_Download_location,Markup,MD5_value)
                    Comments+=Err
                    download_status=status_code(code)
                    
                    if MD5 != '':
                        MD5_InsertQuery='(\''+str(Formatid)+'\',\''+str(councilcode)+'\',\''+str(MD5)+'\',\''+str(PDF_name)+'\'),'                  
                        MD5_InsertQueryList+=str(MD5_InsertQuery)
                    
                    if code == 200:
                        Flag_Update_query='update FORMAT_PUBLIC_ACCESS set Document_Name =\''+str(One_App_Name)+'\',  No_of_Documents = \''+str(Doc_count)+'\',  All_Documents_Downloaded = \'Y\', OA_Scraped_Date = \''+str(Downloaded_date)+'\' where id = \''+str(Formatid)+'\' and COUNCIL_CODE = \''+str(councilcode)+'\';'
                        update_query+=Flag_Update_query
                    Doc_Desc=re.sub('\'','\'\'',str(Doc_Desc))
                    Insert_Document='(\''+str(Formatid)+'\',\''+str(PDF_name)+'\',\''+str(Doc_Type)+'\',\''+str(Doc_Desc)+'\',\''+str(Downloaded_date)+'\',\''+str(DateReceived)+'\',\''+str(download_status)+'\'),'
                    Insert_Document_List+=str(Insert_Document)
                    No_Of_Doc_Downloaded+=downcount
            
    # if Flag == 1:
        # No_Of_Doc_Downloadedtmp=No_Of_Doc_Downloaded+NO_OF_DOCUMENTS
        # No_Of_Doc_Downloaded=No_Of_Doc_Downloadedtmp
                    
    print ('No_Of_Doc_Downloaded:::::',No_Of_Doc_Downloaded)    
    print ('NO_OF_DOCUMENTS::::::',NO_OF_DOCUMENTS) 
    print ('pdfcount::::::',Doc_count)
    
    
    one_App_Location='\\\\172.27.137.202\\One_app_download\\'+str(Formatid)
    # one_App_Location='E:/Sandhiya/Scripts/Individual/Python/Rugby/data/'+str(Formatid)
    Temp_Loca=one_App_Location
    files = os.listdir(temp_Download_location)
    if(len(files) !=0):
        if not os.path.isdir(Temp_Loca):
            os.makedirs(Temp_Loca)
    try:
        for file in files:
            src_tempfile=temp_Download_location+'/'+file
            dst_tempfile=Temp_Loca+'/'+file
            if os.path.exists(dst_tempfile):
                os.remove(dst_tempfile)
                shutil.move(src_tempfile, Temp_Loca)
            else:
                shutil.move(src_tempfile, Temp_Loca)
        shutil.rmtree(temp_Download_location)
    except Exception as ex:
        print("Error in Moving files", traceback.format_exc())

    Insert_OneAppLog='(\''+str(Formatid)+'\',\''+str(councilcode)+'\',\''+str(Markup)+'\',\''+str(Document_Url)+'\',\''+str(Doc_count)+'\',\''+str(One_App_Avail_Status)+'\',\''+str(Downloaded_date)+'\',\''+str(Comments)+'\',\''+str(No_Of_Doc_Downloaded)+'\'),'
    
    if One_App_Name == '':
        if Markup.lower() == 'large':
            Flag_Update_query='update FORMAT_PUBLIC_ACCESS set No_of_Documents =\''+str(Doc_count)+'\' where id =\''+str(Formatid)+'\' and COUNCIL_CODE = \''+str(councilcode)+'\';'
            update_query+=Flag_Update_query
        else:
            Flag_Update_query='update FORMAT_PUBLIC_ACCESS set No_of_Documents =\''+str(Doc_count)+'\' where id =\''+str(Formatid)+'\' and COUNCIL_CODE = \''+str(councilcode)+'\';'
            update_query+=Flag_Update_query
    else:
        if Markup.lower() == 'large':
            Flag_Update_query='update FORMAT_PUBLIC_ACCESS set Document_Name = \''+str(One_App_Name)+'\', No_of_Documents = \''+str(Doc_count)+'\', OA_Scraped_Date = \''+str(Downloaded_date)+'\' where id = \''+str(Formatid)+'\' and COUNCIL_CODE = \''+str(councilcode)+'\';'
            update_query+=Flag_Update_query
            
    
    # input()
    return(Insert_Document_List,MD5_InsertQueryList,Insert_OneAppLog,update_query,No_Of_Doc_Downloaded,Doc_count)
    
def Document_Details(dbh,Record,councilcode):
    for rec1 in record:
        councilname_tmp=rec1[9]
    Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values"
    Insert_OneAppLogs = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values"
    MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values "
    Dashboard_Insert_Query = 'insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\''+str(councilcode)+'\', \''+str(councilname_tmp)+'\', \'Running\', \''+str(Downloaded_date)+'\')'
    print (Dashboard_Insert_Query)
    execute_query(dbh,Dashboard_Insert_Query)   
    update_Query=Status_Update_Query1=''
    
    todaycount=Today_Downloaded_Count=0
    try:        
        for rec in record:
            Formatid=rec[0]
            Source=rec[1]
            Application_No=rec[2]
            Document_Url=rec[3]
            Markup=rec[4]
            URL=rec[5]              
            NO_OF_DOCUMENTS=rec[6]          
            PROJECT_ID=rec[7]
            PROJECT_STATUS=rec[8]
            COUNCIL_NAME=rec[9]
            
            print ("Formatid::",Formatid)
            print ("Application_No::",Application_No)
            print ("Document_Url::",Document_Url)

            Doc_Details = {} 
            if Document_Url:

                # headers
                head = {'Host': 'www.eplanning.ie',
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'}

                # using requests to get the page content
                response = requests.get(Document_Url, headers=head)
                soup = Selector(text=response.text)
                view_scanned_file = \
                    soup.re_first('popupEnquirySubWindow\((?:\'|\&39\;)([^>]*?)\s*(?:\'|\&39\;)\s*\,[^>]*?value\=\"View\s*Scanned\s*Files\"'
                                  )
                view_scanned_file = html.unescape(view_scanned_file)
                id = re.findall(r'id\=([^>]*?)(?:&|$)', view_scanned_file)[0]
                l = re.findall(r'la\=([^>]*?)(?:&|$)', view_scanned_file)[0]

                head_frame = {'Host': '137.191.225.173',
                              'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                              'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'}

                # preparing frame_url
                frame_url = \
                    'http://137.191.225.173/mcc_dwviewer_external/LeftNav.aspx?id={}&la={}'.format(id,
                        l)
                response_scanned = requests.get(frame_url, headers=head_frame)
                HTML_Content=response_scanned.content
                with open("response_scanned.html",'wb')as f:
                    f.write(response_scanned.content)

                # looping to navigate all the link from left bar
                soup_framecontent = Selector(text=response_scanned.text)
                pdfcount=1
                for (i, link) in enumerate(soup_framecontent.css('fieldset ul li a'
                                           )):
                    link = link.css('a::attr(href)').extract_first()
                    print (i, '=>', link)
                    head_document = {  # "Upgrade-Insecure-Requests": "1",
                        'Host': '137.191.225.173',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                        'Accept-Language': 'en-US,en;q=0.5',
                        'Content-Type': 'text/html; charset=utf-8',
                        }
                    response_document = requests.get(link, headers=head_document)

                    # extrating auth, doc_id and fileCabinetId from "response_document"
                    auth = \
                        re.findall(r'\"Name\"\:\"documentData\"\,\"UriPattern\"\:\"[^>]*?_auth\=([^>]*?)(?:\"|\&)'
                                   , response_document.text)[0]
                    doc_id = re.findall(r'\{\"DocID\"\s*\:\s*([^>]*?)\s*\,',
                                        response_document.text)[0]
                    fc_id = re.findall(r'\"FCID\"\s*\:\s*\"([^>]*?)\"\s*\,',
                                       response_document.text)[0]
                    
                    print ("*** doc_id ***::",doc_id)
                    
                    DateReceived=PDFFileName=''
        
                    try:
                        PDFFileName=re.findall(r'<a[^>]*?id\='+str(doc_id)+'[^>]*?>([^>]*?)\([^>]*?[\d]+\/[\d]+\/[\d]+[^>]*?\)<\/a>',str(HTML_Content))
                        PDFFileName=PDFFileName[0]
                    except:
                        pass
                    
                    try:
                        DateReceived=re.findall(r'<a[^>]*?id\='+str(doc_id)+'[^>]*?>[^>]*?\([^>]*?([\d]+\/[\d]+\/[\d]+)[^>]*?\)<\/a>',str(HTML_Content))
                        DateReceived=DateReceived[0]
                    except:
                        pass
                        
                    print ("PDFFileName::",PDFFileName)
                    print ("DateReceived::",DateReceived)
                    print ("Document_Url::",Document_Url)
        
                    head_document = {  # "Upgrade-Insecure-Requests": "1",
                    'Host': '137.191.225.173',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
                    'Accept': 'application/json, text/javascript, */*; q=0.01',
                    'Accept-Language': 'en-US,en;q=0.5',
                    'Content-Type': 'application/json; charset=utf-8',
                    'Referer': response_document.url,
                    'X-Requested-With': 'XMLHttpRequest',
                    'ETag': '_7322cc29-VNgpWA1tMY6XphWGSNyJgr1LdpY=637508914666570000',
                    }

                    # preparing document_meta_url
                    document_meta_url = \
                        'http://137.191.225.173/DocuWare/PlatformRO/WebClient/Viewer/DocumentData?fileCabinetId={fileCabinetId}&docId={docId}&section=0&page=0&includeDocument=true&includeSection=true&checksum=false&latestVersion=true&_auth={auth_token}'.format(fileCabinetId=fc_id,
                            docId=doc_id, auth_token=auth)

                    input_text = document_meta_url.replace('http://137.191.225.173'
                            , '')

                    # generating checksum ID
                    cs_value = get_checksum(input_text)
                    print ("cs_value::",cs_value)
                    document_meta_url = html.unescape(document_meta_url)

                    # adding the checksum id in the document_meta_url
                    document_meta_url = \
                        'http://137.191.225.173/DocuWare/PlatformRO/WebClient/Viewer/DocumentData?fileCabinetId={fileCabinetId}&docId={docId}&section=0&page=0&includeDocument=true&includeSection=true&checksum=false&latestVersion=true&_auth={auth_token}&_cs={cs_value}'.format(fileCabinetId=fc_id,
                            docId=doc_id, auth_token=auth, cs_value=cs_value)

                    document_meta_content = requests.get(document_meta_url,
                            headers=head_document)

                    # Extracting document url from the "document_meta_content"
                    if doc_id !='':
                        Doc_url = \
                            re.findall(r'\{\"rel\"\:\"fileDownload\"\,\"href\"\:\"([^>]*?)\"'
                                       , document_meta_content.text)[0]
                        Doc_url = urljoin('http://137.191.225.173', Doc_url)
                        PDFname=PDFFileName
                    
                        Doc_Desc=''
                        document_type=''
                        PDFname=clean(PDFname)
                        PDFname=str(pdfcount)+'_'+str(PDFname)
                        print ("PDFname::",PDFname)
                        input("STOP")
                        # PDFname=PDF_FileCreation(Doc_url,Doc_Desc,document_type,pdfcount)
                        Doc_Details[Doc_url] = [document_type,PDFname,DateReceived,Doc_Desc]
                        pdfcount+=1
                    else:
                        if ses_count <= 3:
                            ses_count+=1
                            continue
                        else:
                            break
            else:
                print ("No Document Url")       
            
            (Insert_Document_List,MD5_InsertQueryList,Insert_OneAppLog,update_query,Downloaded_count,Doc_count)=Download_Document(Doc_Details,NO_OF_DOCUMENTS,dbh,councilcode,Formatid,Markup,Document_Url)
            
            Today_Downloaded_Count+=Downloaded_count
            todaycount+=Doc_count
            Insert_Document+=Insert_Document_List
            MD5_Insert_Query+=MD5_InsertQueryList
            update_Query+=update_query
            Insert_OneAppLogs+=Insert_OneAppLog
        
        
        Todays_Count=todaycount
        Todays_Downloaded_Count=Today_Downloaded_Count
        Status_Update_Query = 'Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \''+str(Todays_Count)+'\', Downloaded_Count = \''+str(Todays_Downloaded_Count)+'\' where Council_Code = \''+str(councilcode)+'\' and Scraped_Date= \''+str(Downloaded_date)+'\';'
        
        
        with open(Log_directory+'/query_'+date1+'.txt','a') as FH:
            FH.write(str(Downloaded_date)+'\t'+str(Insert_Document)+'\t'+str(MD5_Insert_Query)+'\t'+str(Insert_OneAppLogs)+'\t'+str(update_Query)+'\t'+str(Status_Update_Query)+'\n')
            
        inserdoc=re.search('values\s*$',Insert_Document)
        Md5_doc=re.search('values\s*$',MD5_Insert_Query)
        one_app_doc=re.search('values\s*$',Insert_OneAppLogs)
        update_doc=re.search('^\s*$',update_Query)
        status_doc=re.search('^\s*$',Status_Update_Query)
        
        if not inserdoc:
            Insert_Document=re.sub('\,\s*$','',Insert_Document)         
            print (Insert_Document)
            execute_query(dbh,Insert_Document)
        if not Md5_doc:         
            MD5_Insert_Query=re.sub('\,\s*$','',MD5_Insert_Query)
            print (MD5_Insert_Query)
            execute_query(dbh,MD5_Insert_Query)
        if not one_app_doc:
            Insert_OneAppLogs=re.sub('\,\s*$','',Insert_OneAppLogs)
            print (Insert_OneAppLogs)
            execute_query(dbh,Insert_OneAppLogs)
        if not update_doc:
            print (update_Query)
            execute_query(dbh,update_Query)
        if not status_doc:
            print (Status_Update_Query)
            execute_query(dbh,Status_Update_Query)
    except Exception as ex:
        print("Document response is Error ::",ex)
        
if __name__== "__main__":   
    councilcode = sys.argv[1]   
    # dbh = test_dbConnection()
    dbh = dbConnection()
    print (dbh)
    try:
        FormatID = sys.argv[2]
        if FormatID:
            record=Retrieve_Input(dbh,councilcode,FormatID)
    except Exception as e:
        record=Retrieve_Input(dbh,councilcode,'')
    print ("record : ",record)

    ses = requests.session()
    MD5List=[]
    
    Currentdir=os.getcwd()
    Currentdir=re.sub(r'\\','/',Currentdir) 
    Data_directory=Currentdir+"/data/Documents"
    Log_directory=Currentdir+"/log"

    # if not os.path.isdir(Log_directory):
        # os.makedirs(Log_directory)
    try:
        if len(record) !=0:
            Document_Details(dbh,record,councilcode)            
            print ("Download Completed")
        else:
            print ('No input records')
    except Exception as ex:
        print("Error in fetching details ", traceback.format_exc())