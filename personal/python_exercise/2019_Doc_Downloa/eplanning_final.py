#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import os
import html
import js2py
import hashlib
import logging
import requests
from parsel import Selector
from urllib.parse import urljoin

def get_checksum(input_code):
    """
    To get the checksum id using the JS code

    Parameters
    ----------
    input_code : str
       documentData's  UriPattern

    Returns
    -------
    result : int(checksum)
    """

    # JS code to generate the checksum
    js_code = \
        """function(n) {
        var t = 0;
        if (n.length == 0)
            return t;
        for (i = 0; i < n.length; i++)
            char = n.charCodeAt(i),
            t = (t << 5) - t + char,
            t = t & t;
        return t
    }"""
    f = js2py.eval_js(js_code)
    return f(input_code)


def main():

    # get the current working director
    dir_path = os.path.dirname(os.path.realpath(__file__))

    # startup URL
    # url = 'http://www.eplanning.ie/MayoCC/AppFileRefDetails/21135/0'
    url = 'http://www.eplanning.ie/MayoCC/AppFileRefDetails/20247/0'

    # headers
    head = {'Host': 'www.eplanning.ie',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'}

    # using requests to get the page content
    response = requests.get(url, headers=head)
    soup = Selector(text=response.text)
    view_scanned_file = \
        soup.re_first('popupEnquirySubWindow\((?:\'|\&39\;)([^>]*?)\s*(?:\'|\&39\;)\s*\,[^>]*?value\=\"View\s*Scanned\s*Files\"'
                      )
    view_scanned_file = html.unescape(view_scanned_file)
    id = re.findall(r'id\=([^>]*?)(?:&|$)', view_scanned_file)[0]
    l = re.findall(r'la\=([^>]*?)(?:&|$)', view_scanned_file)[0]

    head_frame = {'Host': '137.191.225.173',
                  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'}

    # preparing frame_url
    frame_url = \
        'http://137.191.225.173/mcc_dwviewer_external/LeftNav.aspx?id={}&la={}'.format(id,
            l)
    response_scanned = requests.get(frame_url, headers=head_frame)
    HTML_Content=response_scanned.content
    with open("response_scanned.html",'wb')as f:
        f.write(response_scanned.content)

    # looping to navigate all the link from left bar
    soup_framecontent = Selector(text=response_scanned.text)
    for (i, link) in enumerate(soup_framecontent.css('fieldset ul li a'
                               )):
        link = link.css('a::attr(href)').extract_first()
        print (i, '=>', link)
        head_document = {  # "Upgrade-Insecure-Requests": "1",
            'Host': '137.191.225.173',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Content-Type': 'text/html; charset=utf-8',
            }
        response_document = requests.get(link, headers=head_document)
        
        with open("response_document.html",'wb')as f:
            f.write(response_document.content)
        # print (response_document.text)
        # with open("response_document.txt",'wb')as f:
            # f.write(str(response_document.text))
            
        input("STOP2")

        # extrating auth, doc_id and fileCabinetId from "response_document"
        auth = \
            re.findall(r'\"Name\"\:\"documentData\"\,\"UriPattern\"\:\"[^>]*?_auth\=([^>]*?)(?:\"|\&)'
                       , response_document.text)[0]
        doc_id = re.findall(r'\{\"DocID\"\s*\:\s*([^>]*?)\s*\,',
                            response_document.text)[0]
        fc_id = re.findall(r'\"FCID\"\s*\:\s*\"([^>]*?)\"\s*\,',
                           response_document.text)[0]
        
        #------------------------------------------------------
        print ("DOC ID::",doc_id)
        Dates=PDFFileName=''
        
        try:
            PDFFileName=re.findall(r'<a[^>]*?id\='+str(doc_id)+'[^>]*?>([^>]*?)\([^>]*?[\d]+\/[\d]+\/[\d]+[^>]*?\)<\/a>',str(HTML_Content))
            PDFFileName=PDFFileName[0]
        except:
            pass
        
        try:
            Dates=re.findall(r'<a[^>]*?id\='+str(doc_id)+'[^>]*?>[^>]*?\([^>]*?([\d]+\/[\d]+\/[\d]+)[^>]*?\)<\/a>',str(HTML_Content))
            Dates=Dates[0]
        except:
            pass
            
        print ("PDFFileName::",PDFFileName)
        print ("Dates::",Dates)
        
        input("ssssss")
        #-----------------------------------------------------
        
        head_document = {  # "Upgrade-Insecure-Requests": "1",
            'Host': '137.191.225.173',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'en-US,en;q=0.5',
            'Content-Type': 'application/json; charset=utf-8',
            'Referer': response_document.url,
            'X-Requested-With': 'XMLHttpRequest',
            'ETag': '_7322cc29-VNgpWA1tMY6XphWGSNyJgr1LdpY=637508914666570000',
            }

        # preparing document_meta_url
        document_meta_url = \
            'http://137.191.225.173/DocuWare/PlatformRO/WebClient/Viewer/DocumentData?fileCabinetId={fileCabinetId}&docId={docId}&section=0&page=0&includeDocument=true&includeSection=true&checksum=false&latestVersion=true&_auth={auth_token}'.format(fileCabinetId=fc_id,
                docId=doc_id, auth_token=auth)

        input_text = document_meta_url.replace('http://137.191.225.173'
                , '')

        # generating checksum ID
        cs_value = get_checksum(input_text)

        document_meta_url = html.unescape(document_meta_url)

        # adding the checksum id in the document_meta_url
        document_meta_url = \
            'http://137.191.225.173/DocuWare/PlatformRO/WebClient/Viewer/DocumentData?fileCabinetId={fileCabinetId}&docId={docId}&section=0&page=0&includeDocument=true&includeSection=true&checksum=false&latestVersion=true&_auth={auth_token}&_cs={cs_value}'.format(fileCabinetId=fc_id,
                docId=doc_id, auth_token=auth, cs_value=cs_value)

        document_meta_content = requests.get(document_meta_url,
                headers=head_document)
        
        with open("document_meta_content.html",'wb')as f:
            f.write(document_meta_content.content)
        input("STOP")
        
        # Extracting document url from the "document_meta_content"
        document_url = \
            re.findall(r'\{\"rel\"\:\"fileDownload\"\,\"href\"\:\"([^>]*?)\"'
                       , document_meta_content.text)[0]
        document_url = urljoin('http://137.191.225.173', document_url)
        doc_content = requests.get(document_url, headers=head_frame)

        # creating file path
        file_path = os.path.join(dir_path, 'downloads')
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        # generating hash key using hashlib - document url as input
        hash = hashlib.md5(document_url.encode()).hexdigest()

        # preparing file name
        file_name = ''
        try:
            content_disposition = \
                doc_content.headers.get('Content-Disposition')
            file_name = \
                re.findall('filename\=(?:\"|\')?([^>]*?)(?:\"|\')?\s*$'
                           , content_disposition)[0]
            file_name = str(hash) + '_' + file_name.replace(r'\s+', '_')
        except:
            file_name = str(hash) + '_' + str(doc_id) + '.pdf'

        document_path = os.path.join(file_path, file_name)
        print ('file_name::', file_name, '\n')

        # storing the files in our desired pat
        with open(document_path, 'wb') as f:
            f.write(doc_content.content)
            
        print ("*** Content Completed ***")
        input ("STOP3")


if __name__ == '__main__':
    main()
