def myFun(arg1, arg2, arg3):
    print("arg1:", arg1)
    print("arg2:", arg2)
    print("arg3:", arg3)


def myFun2(**kwargs):
    for arg in kwargs.items():
    # for arg in kwargs.keys():
    # for arg in kwargs.values():
        print("arg1:", arg)

    
# Now we can use *args or **kwargs to
# pass arguments to this function :
# args = ("Geeks", "for", "Geeks")
# myFun(*args)
 
# kwargs = {"arg1" : "Geeks", "arg2" : "for", "arg3" : "Geeks"}
dass = {"arg1" : "Geeks", "arg2" : "for", "arg3" : "Geeks"}
# myFun(**kwargs)
myFun2(**dass)


'''What does this mean: *args, **kwargs? And why would we use it?
Ans: We use *args when we aren’t sure how many arguments are going to be passed to a function, or if we want to pass a stored list or tuple of arguments to a function. **kwargs is used when we don’t know how many keyword arguments will be passed to a function, or it can be used to pass the values of a dictionary as keyword arguments. The identifiers args and kwargs are a convention, you could also use *bob and **billy but that would not be wise.'''