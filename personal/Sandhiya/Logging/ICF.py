# -*- coding: utf-8 -*-
import warnings
import requests
import re
import sys
import json
import shutil
import os
import time
import logging
import urllib.parse
from random import randint
from datetime import datetime
import traceback
import ftplib
from html.parser import HTMLParser
h = HTMLParser()
warnings.filterwarnings("ignore")

##### Fetches the content for the given URL #####

def getContent(url='', method='GET', parameter={}, header={}, sleep=[0,0], timeline=200):
	global session
	count = 1
	
	while True:
		try:
			time.sleep(randint(sleep[0], sleep[1]))
			
			if method == 'GET':
				reqObj = session.get(url, headers=header, timeout=timeline)
				code = str(reqObj.status_code)
				print('Url :: ' + url)
				print('Code :: ' + code + '\n')
				
				if re.search('^5', code):
					if count <= 5:
						count += 1
						continue
					else:
						return reqObj
				
				return reqObj
			elif method == 'POST':
				reqObj = session.post(url, data=parameter, headers=header, timeout=timeline)
				code = str(reqObj.status_code)
				print('Url :: ' + url)
				print('Code :: ' + code + '\n')
				
				if re.search('^5', code):
					if count <= 5:
						count += 1
						continue
					else:
						return reqObj
				
				return reqObj
			else:
				return False
		except Exception:
			if count <= 5:
				count += 1
			else:
				print(traceback.format_exc())
				return False

####### Content Replacemant #####

def replaceContent(Content):
	Content = str(Content)
	Content = re.sub('\\\\u0026', '&', Content)
	Content = re.sub('\\\\u003c', '<', Content)
	Content = re.sub('\\\\u003e', '>', Content)
	Content = re.sub('\\\<', '<', Content)
	Content = re.sub('\\\>', '>', Content)
	Content = re.sub('\\\\"', '"', Content)
	Content = re.sub("\\\\'", "'", Content)
	Content = re.sub(r'\\r', ' ', Content)
	Content = re.sub(r'\\n', ' ', Content)
	Content = re.sub(r'\\t', ' ', Content)
	Content = re.sub(r'&nbsp;', ' ', Content)
	Content = re.sub(r'&amp;', '&', Content)
	Content = h.unescape(Content)
	return Content

####### Cleaning the Attribute #####

def Clean(Data):
	Data = re.sub(r'<[^>]*?>', ' ', Data)
	Data = re.sub(r'(?:\t|\n)', ' ', Data)
	Data = re.sub(r'(?:\\t|\\n)', ' ', Data)
	Data = re.sub(r'\s+', ' ', Data)
	Data = re.sub(r'(?:^\s+|\s+$)', '', Data)
	return Data

##### Creates output and cache files #####

def fileHandling(fileName, accessMode, write_content):
	file = open(fileName, accessMode)
	file.write(write_content)
	file.close()


####### Main Function ############

if __name__ == "__main__":
	
	try:
		pid = str(os.getpid()) # Process ID
		date = datetime.now()
		startTime = datetime.strftime(date, "%Y-%m-%dT%H-%M")
		today = datetime.strftime(date, "%Y-%m-%d")
		date = datetime.strftime(date, "%Y/%m/%d")
		
		generaloutputFile = "ICF_Unidash_general_plant_information_{}.txt".format(startTime)
		logFilename = "ICF_Unidash_log_{}.txt".format(startTime)
		dcoutputFile = "ICF_Unidash_DC_{}.txt".format(startTime)
		irradianceFile = "ICF_Unidash_irradiance_{}.txt".format(startTime)
		acoutputFile = "ICF_Unidash_AC_{}.txt".format(startTime)
		
		logFilepath = "Log/"+today+"/"
		outputFilepath = "Output/"+today+"/"
		cachepath = "Cache/"+today+"/"
		
		if not os.path.isdir(logFilepath):
			os.makedirs(logFilepath)
		if not os.path.isdir(outputFilepath):
			os.makedirs(outputFilepath)
		if not os.path.isdir(cachepath):
			os.makedirs(cachepath)
			
		log_file_name = logFilepath+logFilename
		logging.basicConfig(
			filename = log_file_name,
			format = "%(asctime)s\t%(levelname)s\t%(funcName)s\t%(lineno)s\t%(message)s",
			level = logging.DEBUG
		)
		logging.info('Script execution initiated : '+str(startTime))
		logging.info('Process ID : ' + pid)
		
		with open(generaloutputFile,'w',encoding="utf-8") as out:
			out.write('PlantId\tPlantName\tScrapeDate\tCommunicationStatus\tDatetime\tTodayEnergyValue\tTodayEnergyValueUnit\tWeeklyEnergyValue\tWeeklyEnergyValueUnit\tMonthlyEnergyValue\tMonthlyEnergyValueUnit\tLifetimeEnergy\tInputPlantName\tPlantProfileUrl\tStatus\n')

		with open(dcoutputFile,'w',encoding="utf-8") as out:	
			out.write('PlantId\tPlantName\tScrapeDate\tInverterName\tDateTime\tChannelName\tEnergyValue\tUnit\tInputPlantName\tStatus\n')

		with open(irradianceFile,'w',encoding="utf-8") as out:
			out.write('PlantId\tPlantName\tScrapeDate\tInverterName\tDatetime\tIrradianceValue\tUnit\tInputPlantName\tStatus\n')

		with open(acoutputFile,'w',encoding="utf-8") as out:
			out.write('PlantId\tPlantName\tScrapeDate\tInverterName\tDatetime\tEnergyValue\tUnit\tInputPlantName\tStatus\n')

		configfile = 'ICF_Unidash_Config.json'
		if not os.path.isfile(configfile):
			logging.info('file missing '+configfile)
			endTime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M")
			logging.info('Script execution completed : '+str(endTime))
			sys.exit
		else:
			try:
				with open(configfile, 'r') as js:
					jsondata = json.load(js)
			except:
				logging.error('Login exception '+str(traceback.format_exc()))
				endTime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M")
				logging.info('Script execution completed : '+str(endTime))
				sys.exit
		
		sleeptime = jsondata['Sleep']
		InputPlantNames = jsondata['PlantNames']
		Logins = jsondata['Logins']
		InputPlantNames = InputPlantNames.split(',')
		
		inputcount = 1
		
		for Login in Logins:
			UserName = jsondata['Logins'][Login]['UserName']
			PassWord = jsondata['Logins'][Login]['PassWord']
			logging.info('UserName: '+str(UserName)+'\tPassWord: '+str(PassWord))
			
			session = requests.Session()
			homeUrl = 'http://icf.unidash.in/'
			obj = getContent(url=homeUrl, method='GET', parameter={}, header={})
			if obj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_home_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(obj.content)
			else:
				logging.error('Error in Fetching homepage content')
				sys.exit
				
			Login_url='http://icf.unidash.in/user/logsin'
			
			postdata ='username='+UserName+'&password='+PassWord
			requestheaders = {"Host": "icf.unidash.in","Content-Type": "application/x-www-form-urlencoded","Referer": "http://icf.unidash.in/user/login"}
			reqObj = getContent(url=Login_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_login_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching loginpage content')
				sys.exit
			
			searchUrl = 'http://icf.unidash.in/user/login'
			reqObj = getContent(url=searchUrl, method='GET', parameter={}, header={})
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_search_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching searchpage content')
				sys.exit
				
			PlantId=PlantName=Todayprod_unit=Todayprod=cumulative_energy=TodayEnergy=rad_unit=rad=CommunicationStatus=Datetime1=''
			
			PlantName=''
			Todayprod_unit=re.search('<h\d+[^>]*?id\s*=\s*"\s*todayprod\s*"[^>]*?>\s*(?:\s*<[^>]*?>\s*)*\s*([\w\W]*?)\s*</p>',reqObj.text, flags=re.I|re.M)
			rad_unit=re.search('\$\(\"\#r1\"\)[^>]*?\+\"\s*([^>]*?)\s*"\s*\)',reqObj.text, flags=re.I|re.M)
		
				
			if Todayprod_unit:
					Todayprod_unit=Clean(Todayprod_unit.group(1))
			if rad_unit:
					rad_unit=Clean(rad_unit.group(1))
				
			Time_url='http://icf.unidash.in/data/getTime'
			postdata =''
			requestheaders = {"Host": "icf.unidash.in","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/"}
			reqObj = getContent(url=Time_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_Time_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching DC Data '+DC_Scrol1+' Page scroll content')
				sys.exit		
				
			Timing=reqObj.text
			Timing1=str(Timing)
			if Timing1 != '':
				dateobj = re.search('([\d]{1,2})\-([\d]{1,2})\-([\d]{1,4})\s+([^>]*?)$', Timing1)
				if dateobj:
					day = dateobj.group(1)
					month1 = dateobj.group(2)
					year1 = dateobj.group(3)
					commtime = dateobj.group(4)
					Datetime1 = year1+'-'+month1+'-'+day+' '+commtime
					Datetime1 = re.sub('^\s*--\s*$','',Datetime1)
			else:
				Datetime1=str(Timing1)
				
			Dashboard_url='http://icf.unidash.in/data/getDashboard'			
			postdata =''
			requestheaders = {"Host": "icf.unidash.in","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/"}
			reqObj = getContent(url=Dashboard_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_Dashboard_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching Dashboard Page content')
				sys.exit	
				
			rad=''			
			try:
				Dashboard_Page=reqObj.text
				Dashlist = str(Dashboard_Page).split(",")
				cumulative_energy=Dashlist[2]
				Todayprod=Dashlist[1]
				rad=Dashlist[7]
			except:
				logging.info('Unable to get datas from dashboard')	
			
			InverterName=''
			EnergyUnit=''
			EnergyValue=''
			IrradianceValue=rad
			IrradianceUnit=rad_unit
			ScrapeDate = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
			Irrad_data = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(PlantId, PlantName, ScrapeDate, InverterName, Datetime1,IrradianceValue, IrradianceUnit, '','Matched')
			with open(irradianceFile,'a',encoding="utf-8") as out:
				out.write(Irrad_data)
				
				
			Week_url='http://icf.unidash.in/data/getWeekProd'			
			postdata =''
			requestheaders = {"Host": "icf.unidash.in","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/"}
			reqObj = getContent(url=Week_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_Weekly_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching Weekly Page content')
				sys.exit
				
			weeklylist=[]
			Averageweek=''
			jsonContent = reqObj.json()		
			try:
				weekunit = jsonContent['label']
				Weekdata = jsonContent['data']
				for week in Weekdata:
					Weekdata1=week[1]
					weeklylist.append(Weekdata1)
			except:
				weekunit = ''
				Weekdata = ''
			try:
				Averageweek=sum(weeklylist)/len(weeklylist)
			except:
				logging.info('Unable to get the average of Weeks')	
				
			Month_url='http://icf.unidash.in/data/getMonthlyProd'			
			postdata =''
			requestheaders = {"Host": "icf.unidash.in","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/"}
			reqObj = getContent(url=Month_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_Monthly_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching Monthly Page content')
				sys.exit

			Monthlylist=[]
			Averagemonth=''
			jsonContent = reqObj.json()		
			try:
				Monthunit = jsonContent['label']
				Monthdata = jsonContent['data']
				for Month in Monthdata:
					Monthdata1=Month[1]
					Monthlylist.append(Monthdata1)
			except:
				Monthunit = ''
				Monthdata = ''
			
			try:
				Averagemonth=sum(Monthlylist)/len(Monthlylist)
			except:
				logging.info('Unable to get the average of Months')
			
			TodayEnergyValue=Todayprod
			TodayEnergyValueUnit=Todayprod_unit
			WeeklyEnergyValue=Averageweek
			WeeklyEnergyValueUnit=weekunit
			MonthlyEnergyValue=Averagemonth
			MonthlyEnergyValueUnit=Monthunit
			LifetimeEnergy=cumulative_energy
			
			ScrapeDate = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
			General_Plant_Info_data ='{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(PlantId,PlantName,ScrapeDate,CommunicationStatus,Datetime1,TodayEnergyValue,TodayEnergyValueUnit,WeeklyEnergyValue,WeeklyEnergyValueUnit,MonthlyEnergyValue,MonthlyEnergyValueUnit,LifetimeEnergy,'','','Matched')
			with open(generaloutputFile,'a',encoding="utf-8") as out:
				out.write(General_Plant_Info_data)
				
			
			Time_url='http://icf.unidash.in/data/getTime'
			postdata =''
			requestheaders = {"Host": "icf.unidash.in","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/"}
			reqObj = getContent(url=Time_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_Time_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching DC Data '+DC_Scrol1+' Page scroll content')
				sys.exit		
				
			Timing=reqObj.text
			Timing1=str(Timing)
			if Timing1 != '':
				dateobj = re.search('([\d]{1,2})\-([\d]{1,2})\-([\d]{1,4})\s+([^>]*?)$', Timing1)
				if dateobj:
					day = dateobj.group(1)
					month1 = dateobj.group(2)
					year1 = dateobj.group(3)
					commtime = dateobj.group(4)
					Datetime1 = year1+'-'+month1+'-'+day+' '+commtime
					Datetime1 = re.sub('^\s*--\s*$','',Datetime1)
			else:
				Datetime1=str(Timing1)
				
			Ac_url='http://icf.unidash.in/dashboard/invview'	
			reqObj = getContent(url=Ac_url, method='GET', parameter={}, header={})
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_AC_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching AC Data content')
				sys.exit
			
			
			AC_energy_value=AC_energy_unit=InverterName=''
			AC_Scrol=re.findall('(<h\d+[^>]*?class="box-title"[^>]*?>\s*[\w\W]*?</div>\s*</div>)',reqObj.text, flags=re.I|re.M)
			for AC_row in AC_Scrol:	
				AC_Scrol1=re.search('<div[^>]*?id\s*=\s*\"\s*scb\s*\-\s*(\d+)\s*\"[^>]*?>',str(AC_row))
				InverterName=re.search('<h\d+[^>]*?class="box-title"[^>]*?>\s*([\w\W]*?)<\/h\d+>',str(AC_row))
				AC_Scrol1=Clean(AC_Scrol1.group(1))
				InverterName=Clean(InverterName.group(1))
				Data_url='http://icf.unidash.in/data/invdata'			
				postdata ='id='+AC_Scrol1
				requestheaders = {"Host": "icf.unidash.in","Content-Type": "application/x-www-form-urlencoded; charset=UTF-8","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/dashboard/invview"}
				reqObj = getContent(url=Data_url, method='POST', parameter=postdata, header=requestheaders)
				
				if reqObj != False:
					cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
					CachePage = cachepath+"ICF_Unidash_AC_DATA_page_{}_{}_{}.html".format(AC_Scrol1,cachetime,inputcount)
					with open(CachePage,'wb') as f:
						f.write(reqObj.content)
				else:
					logging.error('Error in Fetching AC Data '+AC_Scrol1+' Page scroll content')
					sys.exit
					
				AC_energy=re.search('<td[^>]*?>\s*AC\s*Power\s*</td>\s*(?:\s*<[^>]*?>\s*)*\s*([^>]*?)\s*([A-Za-z]*?)\s*</td>',reqObj.text, flags=re.I|re.M)
					
				if AC_energy:
					AC_energy_value=Clean(AC_energy.group(1))
					AC_energy_unit=Clean(AC_energy.group(2))
					
				AC_EnergyValue=AC_energy_value
				AC_EnergyValue_Unit=AC_energy_unit
				IrradianceValue=rad
				Irradiance_Unit=rad_unit
				AC_InverterName=InverterName
				ScrapeDate = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
				AC_Output_data ='{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(PlantId,PlantName,ScrapeDate,AC_InverterName,Datetime1,AC_EnergyValue,AC_EnergyValue_Unit,'','Matched')
				with open(acoutputFile,'a',encoding="utf-8") as out:
					out.write(AC_Output_data)
			
			
			Time_url='http://icf.unidash.in/data/getTime'
			postdata =''
			requestheaders = {"Host": "icf.unidash.in","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/"}
			reqObj = getContent(url=Time_url, method='POST', parameter=postdata, header=requestheaders)
			
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_Time_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching DC Data '+DC_Scrol1+' Page scroll content')
				sys.exit		
				
			Timing=reqObj.text
			Timing1=str(Timing)
			if Timing1 != '':
				dateobj = re.search('([\d]{1,2})\-([\d]{1,2})\-([\d]{1,4})\s+([^>]*?)$', Timing1)
				if dateobj:
					day = dateobj.group(1)
					month = dateobj.group(2)
					year = dateobj.group(3)
					commtime = dateobj.group(4)
					Datetime1 = year+'-'+month+'-'+day+' '+commtime
					Datetime1 = re.sub('^\s*--\s*$','',Datetime1)
			else:
				Datetime1=str(Timing1)
			
			DC_url='http://icf.unidash.in/dashboard/scbview'	
			reqObj = getContent(url=DC_url, method='GET', parameter={}, header={})
			if reqObj != False:
				cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
				CachePage = cachepath+"ICF_Unidash_DC_page_{}_{}.html".format(cachetime,inputcount)
				with open(CachePage,'wb') as f:
					f.write(reqObj.content)
			else:
				logging.error('Error in Fetching DC Data Page content')
				sys.exit
				

			ChannelName=''					
			DC_Scrol=re.findall('(<h\d+[^>]*?class="box-title"[^>]*?>\s*[\w\W]*?</div>\s*</div>)',reqObj.text, flags=re.I|re.M)
			for DC_row in DC_Scrol:	
				DC_Scrol1=re.search('<div[^>]*?id\s*=\s*\"\s*scb\s*\-\s*(\d+)\s*\"[^>]*?>',str(DC_row))
				ChannelName=re.search('<h\d+[^>]*?class="box-title"[^>]*?>\s*([\w\W]*?)<\/h\d+>',str(DC_row))
				ChannelName=Clean(ChannelName.group(1))
				DC_Scrol1=Clean(DC_Scrol1.group(1))
				
				DC_Data_url='http://icf.unidash.in/data/scbdata'
				postdata ='id='+DC_Scrol1
				requestheaders = {"Host": "icf.unidash.in","Content-Type": "application/x-www-form-urlencoded; charset=UTF-8","X-Requested-With": "XMLHttpRequest","Referer": "http://icf.unidash.in/dashboard/invview"}
				reqObj = getContent(url=DC_Data_url, method='POST', parameter=postdata, header=requestheaders)
				
				if reqObj != False:
					cachetime = datetime.strftime(datetime.now(), "%Y-%m-%dT%H-%M-%S")
					CachePage = cachepath+"ICF_Unidash_DC_DATA_page_{}_{}_{}.html".format(DC_Scrol1,cachetime,inputcount)
					with open(CachePage,'wb') as f:
						f.write(reqObj.content)
				else:
					logging.error('Error in Fetching DC Data '+DC_Scrol1+' Page scroll content')
					sys.exit
					
				DC_Energy=re.search('<td[^>]*?>\s*Power\s*</td>\s*(?:\s*<[^>]*?>\s*)*\s*([^>]*?)\s*([A-Za-z]*?)\s*</td>',reqObj.text, flags=re.I|re.M)
					
				if DC_Energy:
					Dc_energy_value= DC_Energy.group(1)
					DC_energy_Unit = DC_Energy.group(2)
					
				DC_InverterName=''
				DC_EnergyValue=Dc_energy_value
				DC_EnergyValue_Unit=DC_energy_Unit
				ScrapeDate = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
				DC_Output_data ='{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(PlantId,PlantName,ScrapeDate,DC_InverterName,Datetime1,ChannelName,DC_EnergyValue,DC_EnergyValue_Unit,'','Matched')
				
				with open(dcoutputFile,'a',encoding="utf-8") as out:
					out.write(DC_Output_data)
					
		inputcount +=1
		# shutil.move(outputFile,outputFilepath+outputFile)
	except:
		logging.error('Exception '+str(traceback.format_exc()))
		# shutil.move(outputFile,outputFilepath+outputFile)
		
date = datetime.now()
endTime = datetime.strftime(date, "%Y-%m-%dT%H-%M")
logging.info('Script execution completed : '+str(endTime))