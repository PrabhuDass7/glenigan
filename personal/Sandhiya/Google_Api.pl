#--------------------------PROJECT NAME :	HANLEYWOOD--------------------------------#
#--------------------------SPEC NAME	:	GOOGLE API--------------------------------#	
#--------------------------DATE 		:	23-MAY-2017-------------------------------#
#--------------------------DEVELOPER	:	KAVI SUDHA J------------------------------#	

#--------------->HEADER FILE
use strict;
use Cwd;
use HTML::Entities;
use List::MoreUtils qw(uniq); #To filter out uniq values in an array
use crawling_modules qw(trim_space pdf_download bytes1 random_sleep if_condition trim trim_tag trim_abstract trim_json ip bytes time_difference datetime);
use JSON qw( decode_json );
use Data::Dumper;
use HTML::TagParser;



#-------------->COOKIE AND USER AGENT
my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
my $ua=LWP::UserAgent->new;
$ua->agent("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0");
$ua->max_redirect(0);
my $count=0;

open FO,">Output.txt";
print FO "count\tsearch_string\tgoogle_url\tData available\tcity\tkeyword\tformatted_address\tgeometry_location_lat\tgeometry_location_lng\tgeometry_viewport_northeast_lat\tgeometry_viewport_northeast_lng\tgeometry_viewport_southwest_lat\tgeometry_viewport_southwest_lng\ticon\tid\tname\tphotos_height\ta_text\ta_href\tphotos_reference\tphotos_width\tplace_id\trating\treference\tt\t\n";
close FO;


open FI,"input.txt";
while(<FI>)
{
    my($keyword,$city)=split('\t',$_);
    chomp($keyword);chomp($city);
    $count++;
    print "< $count > $keyword\n";
    # my $city="Cincinnati";
    my $search_string="$keyword+$city";
    $search_string=~s/\s+/\+/igs;
    my $google_url;
    
    
    my $next_page_token;
    nextpage:
    # print "next_page_tokenL:$next_page_token\n";<>;
    if($next_page_token ne "")
    {
        print "Next page\n";
        $google_url="https://maps.googleapis.com/maps/api/place/textsearch/json?query=$search_string&key=AIzaSyDYxNVsfZbO7VGVBaX3iEh2E098t7IdwJc&pagetoken=$next_page_token";
        # $google_url="https://maps.googleapis.com/maps/api/place/textsearch/json?query=$search_string&key=AIzaSyB9W-IRYiri6AJwMRZJHn794z4LBvLcRh0&pagetoken=$next_page_token";
    }
    else
    {
        $google_url="https://maps.googleapis.com/maps/api/place/textsearch/json?query=$search_string&key=AIzaSyDYxNVsfZbO7VGVBaX3iEh2E098t7IdwJc";
        # $google_url="https://maps.googleapis.com/maps/api/place/textsearch/json?query=$search_string&key=AIzaSyB9W-IRYiri6AJwMRZJHn794z4LBvLcRh0";
    }
    
    # print "Google:$google_url\n";
    
    my ($content1)=getcont($google_url,"","","GET");
    sleep(5);

    open FC,">content1.txt";
    print FC "$content1\n";
    close FC;
    
   
    open FC, "<content1.txt";
    my @arr = <FC>;
    my $json = "@arr";
    my $content1=$json; 
    my $decoded = decode_json($json);
    
    my $next_token=$decoded->{"next_page_token"};
    print "next:$next_token\n";	
    
    my $status=$decoded->{"status"} if($decoded->{"status"} ne undef);
    print "Status:$status\n";

    if($status=~m/ZERO\_RESULTS/is)
    {
        open FO,">>Output.txt";
        print FO "$count\t$search_string\t$google_url\tData not available\n";
        close FO;
    }
    elsif($status=~m/OVER\_QUERY\_LIMIT/is)
    {
        open FO,">>Output.txt";
        print FO "$count\t$search_string\t$google_url\tLimit exceeded\n";
        close FO;
    }
    
    
    
    my @friends = @{ $decoded->{'results'}};
    foreach my $f ( @friends ) 
    {
        
        my $formatted_address=$f->{"formatted_address"} if($f->{'formatted_address'} ne undef);
        print "Address:$formatted_address\n";
        my $geometry_location_lat=$f->{"geometry"}{"location"}{"lat"} if($f->{"geometry"}{"location"}{"lat"} ne undef);
        my $geometry_location_lng=$f->{"geometry"}{"location"}{"lng"} if($f->{"geometry"}{"location"}{"lng"} ne undef);
        my $geometry_viewport_northeast_lat=$f->{"geometry"}{"viewport"}{"northeasteast"}{"lat"} if($f->{"geometry"}{"viewport"}{"norteast"}{"lat"} ne undef);
        my $geometry_viewport_northeast_lng=$f->{"geometry"}{"viewport"}{"northeast"}{"lng"} if($f->{"geometry"}{"viewport"}{"norteast"}{"lng"} ne undef);
        my $geometry_viewport_southwest_lat=$f->{"geometry"}{"viewport"}{"southwest"}{"lat"} if($f->{"geometry"}{"viewport"}{"southwest"}{"lat"} ne undef);
        my $geometry_viewport_southwest_lng=$f->{"geometry"}{"viewport"}{"southwest"}{"lng"} if($f->{"geometry"}{"viewport"}{"southwest"}{"lng"} ne undef);
        
        my $icon=$f->{"icon"} if($f->{'icon'} ne undef);
        my $id=$f->{"id"} if($f->{'id'} ne undef);
        my $name=$f->{"name"} if($f->{'name'} ne undef);
       
        my @friends1 = @{ $f->{'photos'} } if($f->{'photos'} ne undef);
        my($photos_height,$photos_reference,$photos_width,$a_text,$a_href);
        foreach my $f1 ( @friends1 ) 
        {
        
            $photos_height=$f1->{"height"} if($f1->{"height"} ne undef);
            my @photos = @{ $f1->{'html_attributions'} } if($f1->{'html_attributions'} ne undef);
            foreach my $f11 ( @photos ) 
            {
                my $obj=HTML::TagParser->new($f11);
                $a_text=$obj->getElementsByTagName("a")->innerText() if($obj->getElementsByTagName("a")->innerText() ne undef);
                $a_href=$obj->getElementsByTagName("a")->getAttribute("href") if($obj->getElementsByTagName("a")->getAttribute("href"));
                
            }
            
            $photos_reference=$f1->{"photo_reference"} if($f1->{"photo_reference"} ne undef);
            $photos_width=$f1->{"width"} if($f1->{"width"} ne undef);
        }
       
        my $place_id=$f->{"place_id"} if($f->{"place_id"} ne undef);
        my $rating=$f->{"rating"} if($f->{"rating"} ne undef);
        my $reference=$f->{"reference"} if($f->{"reference"} ne undef);
        
        
        my @types = @{ $f->{'types'} } if($f->{"types"} ne undef);
        my $t;
        foreach my $t1 ( @types ) 
        {
            $t="$t|$t1";
        }
        
        
        open FO,">>Output.txt";
        print FO "$count\t$search_string\t$google_url\tData available\t$city\t$keyword\t$formatted_address\t$geometry_location_lat\t$geometry_location_lng\t$geometry_viewport_northeast_lat\t$geometry_viewport_northeast_lng\t$geometry_viewport_southwest_lat\t$geometry_viewport_southwest_lng\t$icon\t$id\t$name\t$photos_height\t$a_text\t$a_href\t$photos_reference\t$photos_width\t$place_id\t$rating\t$reference\t$t\t\n";
        close FO;
        
        # open FO,">>Output.txt";
        # print FO "$count\t@status\t$search_string\t$formatted_address\t$geometry_location_lat\t$geometry_location_lng\t$geometry_viewport_northeast_lat\t$geometry_viewport_northeast_lng\t$geometry_viewport_southwest_lat\t$geometry_viewport_southwest_lng\t$icon\t$id\t$name\t$photos_height\t$a_text\t$a_href\t$photos_reference\t$photos_width\t$place_id\t$rating\t$reference\t$t\t$google_url\t\n";
        # close FO;
          
    }
    if($next_token ne "")
    {
        $next_page_token=$next_token;
        goto nextpage;
    }
   

    
}



#----------------GET CONTENT-------------------#
sub getcont
{
    my($ur,$cont,$ref,$method)=@_;
    netfail:
    my $request=HTTP::Request->new("$method"=>$ur);
    $request->header("Content-Type"=>"application/x-www-form-urlencoded");
    $request->header("Cookie"=>"CFID=35231955; CFTOKEN=a102a537f89e45dc-18CB8AE8-D034-7E24-B9315C57BE0AB8CE; _ceg.s=oc3b79; _ceg.u=oc3b79; _ga=GA1.2.354458192.1471434376; _ceg.s=oc1y8l; _ceg.u=oc1y8l; BIGipServerPOOL-207.97.254.104-80=156084416.20480.0000; _gat=1; _gat_GSA_ENOR0=1");
    if($ref ne '')
    {
   
        $request->header("Referer"=>"$ref");
    }
    if(lc $method eq 'post')
    {
   
        $request->content($cont);
    }
    my $res=$ua->request($request);
    $cookie_jar->extract_cookies($res);
    $cookie_jar->save;
    $cookie_jar->add_cookie_header($request);
    my $code=$res->code;
    print"\nRESPONSE CODE:$code\n";
    if($code==200)
    {
       
        my $content=$res->content();
		#$content=decode_entities($content);
        return $content;
    }
    elsif($code=~m/50/is)
    {
        print"\n Net Failure";
        # sleep(30);
        goto netfail;
    }
    elsif($code=~m/30/is)
    {
       
       my $loc=$res->header("Location");
         print "\nLocation: $loc";
        my $request1=HTTP::Request->new(GET=>$loc);
        $request1->header("Content-Type"=>"application/x-www-form-urlencoded; charset=UTF-8");
        my $res1=$ua->request($request1);
        $cookie_jar->extract_cookies($res1);
        $cookie_jar->save;
        $cookie_jar->add_cookie_header($request1);
        my $content1=$res1->content();
        return $content1;

    }
    elsif($code=~m/40/is)
    {
		 my $content=$res->content();
		#$content=decode_entities($content);
		print "\n URL Not found";
        return $content;
        
    }
}	

