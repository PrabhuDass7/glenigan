package API;
use strict;
use warnings;
use LWP::Simple;
use LWP::UserAgent;
use JSON;
use Encode;

# my $content1=webscrapedata("http://data.com","http://da.co","2014","Data.pdf","20","");


######Webscraping link Collection#################
sub getwebscrape_link()
{
	my $link="http://10.0.73.106/pgsuite/api/getwebscraplinks";
	# my $post_data="{ \"app\" \: \"webscrap\", \"process\" \: \"getlinks\"}";
	my %a=("app"=>"webscrap","process"=>"getlinks");
	my $value=encode_json \%a;
	my ($content,$status_code)=post_c($link,$value);
	print "App is hitted Successsed\n";
	sleep(5);
	return $content;
}

##########Webscriping otp collection#############
sub get_otp()
{
	my $website_link=shift;
	my $mail_id=shift;
	my $link="http://10.0.73.106/pgsuite/api/getotp";
	my %a=("app"=>"webscrap","process"=>"getotp", "email_id"=>$mail_id,"link"=>$website_link);
		
	my $value=encode_json \%a;
	my ($content,$status_code)=post_c($link,$value);
	return $content;
}

##########Webscriping data collection#############
sub webscrapedata()
{
	my $link_data=shift;
	my $domain=shift;
	my $email_id=shift;
	my $file_name=shift;
	my $file_size=shift;
	my $file_content=shift;
	my $link="http://10.0.73.106/pgsuite/api/getwebscrapdata";
	
	my %a=("app"=>"webscrap","process"=>"getscrapdata","data"=>[{"link"=>"$link_data","domain"=>"$domain","email_id"=>"$email_id","file_name"=>"$file_name","file_size"=>"$file_size","file_content"=>""}]);
		
	my $value=encode_json \%a;
	my ($content,$status_code)=post_c($link,$value);
	
	# print " Data: Status Code: $status_code\n";
	print "App is hitted Successsed\n";
	sleep(5);
	open TY,">webscrapedata.html";
	print TY "$content\n";
	close TY;
	
	return $content;
}
	
######webscrapping status#############	
sub webscrapestatus()
{

	my $id=shift;
	my $email_id=shift;
	my $domain=shift;
	my $link=shift;
	my $status=shift;
	my $error=shift;
	my $link1="http://10.0.73.106/pgsuite/api/getwebscrapstatus";
	
	my %a=("app"=>"webscrap","process"=>"getscrapdata","data"=>[{"id"=>"$id","email_id"=>"$email_id","domain"=>"$domain","link"=>"$link","status"=>"$status","error"=>"$error"}]);
		
	my $value=encode_json \%a;
	print "json_value**********$value******************\n";<>;
	my ($content,$status_code)=post_c($link1,$value);
	print " Data Status Status Code: $status_code\n";
	
	print "App is hitted Successsed webstatus\n";
	open TY,">webstatus.html";
	print TY "$content\n";
	close TY;
	sleep(5);
	
	return $content;
}	
	

sub post_c
{
	my $url = shift;
    my $data = shift;
	my $pu='sa_proxy';
	my $pp='Pr0xy.1';
	my $domain= 'domain';
	
	my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 },
    protocols_allowed => ['http','https'],);
	
	$ua->env_proxy;
	$ua->credentials('http://pghnprx001chbaa.partnersgroup.net:8080',"",$pu,$pp);
	$ua->default_header( "Content-Type" => "application/json");
    my $response = $ua->post($url,"content"=>$data);
	if($response->is_success) {
		my $ct = $response->decoded_content;
		my $sc = $response->status_line;
		print "helo\n";
		return($ct,$sc);
	}
	else{
		my $ct = $response->content;  
		my $sc= $response->status_line;
		return($ct,$sc);
	}
}

1;