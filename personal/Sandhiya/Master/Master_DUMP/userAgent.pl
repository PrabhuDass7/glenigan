require API;

my $jso_id = $ARGV[0];
my $jso_email_id = $ARGV[1];
my $jso_domain = $ARGV[2];
my $jso_link = $ARGV[3];
my $status = $ARGV[4];
my $error = $ARGV[5];
my $File_name = $ARGV[6];
my $return_size_kb = $ARGV[7];

my($webscrapeDataContent,$webscrapeDataStatusCode,$webscrapeStatusContent,$webScrapesCode);
if ($status eq "SCRAP_COMPLETE")
{
	($webscrapeDataContent,$webscrapeDataStatusCode) = &API::webscrapedata($jso_link,$jso_domain,$jso_email_id,$File_name,$return_size_kb,"");
	
	($webscrapeStatusContent,$webScrapesCode) = &API::webscrapestatus($jso_id,$jso_email_id,$jso_domain,$jso_link,$status,$error);
	
	print "webscrapeDataContent:: $webscrapeDataContent\n";
	print "webscrapeStatusContent:: $webscrapeStatusContent\n";
}
else{
	($webscrapeStatusContent,$webScrapesCode) = &API::webscrapestatus($jso_id,$jso_email_id,$jso_domain,$jso_link,$status,$error);
}