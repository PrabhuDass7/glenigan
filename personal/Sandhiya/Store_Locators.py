import requests
from bs4 import BeautifulSoup
import xlsxwriter
import json
import re


def galaStore(galaSheet, bold,url):
	# url = 'https://www.gala.ie/about-gala/store-locator/'
	r = requests.get(url)
	soup = BeautifulSoup(r.text, "html.parser")
	tableBody = soup.find('tbody', class_='row-hover')
	cards = tableBody.findAll('tr')
	masterlist = []

	# Write some data headers.
	galaSheet.write('A1', 'Country', bold)
	galaSheet.write('B1', 'Address Line 1', bold)
	galaSheet.write('C1', 'Address Line 2', bold)
	galaSheet.write('D1', 'Address Line 3', bold)
	galaSheet.write('E1', 'Eircode', bold)

	# Start from the first cell below the headers.
	row = 1
	col = 0

	for card in cards:
		sublist = []
		country = card.find('td', class_="column-1")

		addresLine1 = card.find('td', class_="column-2")
		addressLine2 = card.find('td', class_="column-3")
		addressLine3 = card.find('td', class_="column-4")
		eirCode = card.find('td', class_="column-5")
		sublist.append(country.text.strip())
		sublist.append(addresLine1.text.strip())
		sublist.append(addressLine2.text.strip())
		sublist.append(addressLine3.text.strip())
		sublist.append(eirCode.text.strip())
		masterlist.append(sublist)

	# print(masterlist)
	lists = tuple(masterlist)

	# Iterate over the data and write it out row by row.
	for country, addresLine1, addressLine2, addressLine3, eirCode in (lists):
		galaSheet.write(row, col, country)
		galaSheet.write(row, col + 1, addresLine1)
		galaSheet.write(row, col + 2, addressLine2)
		galaSheet.write(row, col + 3, addressLine3)
		galaSheet.write(row, col + 4, eirCode)
		row += 1

def homeBargains(homeBargainSheet, bold,url):
	# url = 'https://storelocator.homebargains.co.uk/all-stores'
	r = requests.get(url)
	soup = BeautifulSoup(r.text, "html.parser")
	tableBody = soup.find('tbody')
	cards = tableBody.findAll('tr')
	homeBarginList = []

	# Write some data headers.
	homeBargainSheet.write('A1', 'Region', bold)
	homeBargainSheet.write('B1', 'Store', bold)
	homeBargainSheet.write('C1', 'Open Today', bold)
	homeBargainSheet.write('D1', 'Tel', bold)
	homeBargainSheet.write('E1', 'Store details', bold)

	# Start from the first cell below the headers.
	row = 1
	col = 0

	for card in cards:
		sublist = []
		if(card.find('td')):
			tableData = card.findAll('td')
			region = tableData[0].find('a')
			store = tableData[1].find('a')
			openToday = tableData[2]
			tel=''
			if(tableData[3].find('a')):
				tel = tableData[3].find('a')
			else:
				tel = tableData[3]
			storeDetails = tableData[4].find('a')
			sublist.append(region.text.strip())
			sublist.append(store.text.strip())
			sublist.append(openToday.text.strip())
			sublist.append(tel.text.strip())
			sublist.append(storeDetails.text.strip())
			homeBarginList.append(sublist)

	lists = tuple(homeBarginList)

	# Iterate over the data and write it out row by row.
	for region, store, openToday, tel, storeDetails in (lists):
		homeBargainSheet.write(row, col, region)
		homeBargainSheet.write(row, col + 1, store)
		homeBargainSheet.write(row, col + 2, openToday)
		homeBargainSheet.write(row, col + 3, tel)
		homeBargainSheet.write(row, col + 4, storeDetails)
		row += 1
		
		
def Aldi(AldiSheet,bold,AldiURL):
	data = []
	# Write some data headers#
	
	AldiSheet.write('A1', 'StoreName', bold)
	AldiSheet.write('B1', 'StoreCode', bold)
	AldiSheet.write('C1', 'Address', bold)
	AldiSheet.write('D1', 'Openhours', bold)

	# Start from the first cell below the headers.
	row = 1
	col = 0
	
	# Scrapping starts
	headers = {
		'Accept':'application/json, text/javascript, */*',
		'Accept-Encoding':'gzip, deflate, ',
		'Connection': 'keep-alive',
		'sec-ch-ua-mobile': '?0',
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
		'Host': 'www.aldi.co.uk',
		'Referer': 'https://www.aldi.co.uk/store-finder?q=london&address=London&latitude=51.5073509&longitude=-0.1277583',
		'Accept-Language': 'en-US,en;q=0.9',
	}
	proxies =  {'http':'http://172.27.140.48:3128','https':'http://172.27.140.48:3128'}
	count = 0
	

	while True:
		url = AldiURL+str(count)
		response = requests.get(url,proxies=proxies,headers=headers)
		jsoncontent= json.loads(response.text)
		pagecount = len(jsoncontent['pagination']['pages'])
		if count < 	pagecount:

			for item in jsoncontent['results']:
				sublist = []
				store_name = item['name']
				store_code = item['code']
				addressList = []
				hourslist = []
				for addressiter in item['address']:
					addressList.append(addressiter)
				for hoursiter in item['openingTimes']:
					day = hoursiter['day']
					hours = hoursiter['hours']
					hours = re.sub('&nbsp;&ndash;&nbsp;','-',hours)
					timings = day+'|'+hours
					hourslist.append(timings)
				sublist.append(str(store_name))
				sublist.append(str(store_code))
				sublist.append(str(addressList))
				sublist.append(str(hourslist))
				data.append(sublist)
			count =count +1
				
		else: 
			break
		
	lists = tuple(data)

	# Iterate over the data and write it out row by row.
	for store_name, store_code, addressList, hourslist in (lists):
		AldiSheet.write(row, col, store_name)
		AldiSheet.write(row, col + 1, store_code)
		AldiSheet.write(row, col + 2, addressList)
		AldiSheet.write(row, col + 3, hourslist)
		row += 1
	
def Amazon(AmazonSheet, bold,AmazonURL):
	data = []
	# Write some data headers#
	
	AmazonSheet.write('A1', 'StoreID', bold)
	AmazonSheet.write('B1', 'StoreName', bold)
	AmazonSheet.write('C1', 'City', bold)
	AmazonSheet.write('D1', 'State', bold)
	AmazonSheet.write('E1', 'Postalcode', bold)
	AmazonSheet.write('F1', 'Country', bold)
	AmazonSheet.write('G1', 'partner', bold)
	AmazonSheet.write('H1', 'storeidentifier', bold)
	AmazonSheet.write('I1', 'Time_zone', bold)
	AmazonSheet.write('J1', 'Country_code', bold)

	# Start from the first cell below the headers.
	row = 1
	col = 0
	headers = {
    'authority': 'd1twkrb4o26gk5.cloudfront.net',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
    'accept': '*/*',
    'sec-fetch-site': 'cross-site',
    'sec-fetch-mode': 'no-cors',
    'sec-fetch-dest': 'script',
    'referer': 'https://code.metalocator.com/index.php?option=com_locator&view=directory&layout=combined&Itemid=10797&tmpl=component&framed=1&source=js&lang=en-GB%20(1)&tags%5B%5D=&user_lat=13.0932246&user_lng=80.16146479999999',
    'accept-language': 'en-US,en;q=0.9',
	}

	params = (
    ('option', 'com_locator'),
    ('view', 'directory'),
    ('force_link', '1'),
    ('tmpl', 'component'),
    ('task', 'search_zip'),
    ('framed', '1'),
    ('format', 'raw'),
    ('no_html', '1'),
    ('templ/[/]', 'address_format'),
    ('layout', '_jsonfast'),
    ('lazy_load_resort', '1'),
    ('Itemid', '10797'),
    ('interface_revision', '496'),
    ('lang', 'en-GB (1)'),
    ('preview', '0'),
    ('parent_table', ''),
    ('parent_id', '0'),
    ('search_type', 'point'),
    ('_opt_out', ''),
    ('ml_location_override', ''),
    ('reset', 'false'),
    ('nearest', 'false'),
    ('national', 'true'),
    ('callback', 'handleJSONPResults'),
	)

	response = requests.get(AmazonURL, headers=headers, params=params)
	
	store_content = response.text
	storecontent1 = re.findall(r'(\{\"reset\"[\w\W]*?)\)\;$',str(store_content),re.I)[0]
	
	jsoncontent =  json.loads(storecontent1)

	for key,iteritems in enumerate(jsoncontent['results']):
	
		sublist = []
		if 'id' in iteritems:
			id = iteritems['id']
		else:
			id= ''
			
		if 'title' in iteritems:
			Title = iteritems['title']
		else:
			Title= ''
			
		if 'address' in iteritems:
			Address = iteritems['address']
		else:
			Address = ''
			
		if 'city' in iteritems:
			City = iteritems['city']
		else:
			City= ''
			
		if 'state' in iteritems:
			State = iteritems['state']
		else:
			State= ''
			
		if 'postalcode' in iteritems:
			Postalcode = iteritems['postalcode']
		else:
			Postalcode= ''
			
		if 'country' in iteritems:
			Country = iteritems['country']
		else:
			Country= ''
			
		if 'partner' in iteritems:
			partner = iteritems['partner']
		else:
			partner = ''
			
		if 'storeidentifier' in iteritems:
			Storeidentifier = iteritems['storeidentifier']
		else:
			Storeidentifier = ''
			
		if 'time_zone' in iteritems:
			time_zone = iteritems['time_zone']
		else:
			time_zone = ''
			
		if 'tld' in iteritems:
			Country_code = iteritems['tld']
		else:
			Country_code = ''
		
		sublist.append(str(id))
		sublist.append(str(Title))
		sublist.append(str(Address))
		sublist.append(str(City))
		sublist.append(str(State))
		sublist.append(str(Postalcode))
		sublist.append(str(Country))
		sublist.append(str(partner))
		sublist.append(str(Storeidentifier))
		sublist.append(str(time_zone))
		sublist.append(str(Country_code))
		data.append(sublist)
		
	lists = tuple(data)

	# Iterate over the data and write it out row by row.
	for id, Title, Address, City,State,Postalcode,Country,partner,Storeidentifier,time_zone,Country_code in (lists):
		AmazonSheet.write(row, col, id)
		AmazonSheet.write(row, col + 1, Title)
		AmazonSheet.write(row, col + 2, Address)
		AmazonSheet.write(row, col + 3, City)
		AmazonSheet.write(row, col + 4, State)
		AmazonSheet.write(row, col + 5, Postalcode)
		AmazonSheet.write(row, col + 6, Country)
		AmazonSheet.write(row, col + 7, partner)
		AmazonSheet.write(row, col + 8, Storeidentifier)
		AmazonSheet.write(row, col + 9, time_zone)
		AmazonSheet.write(row, col + 10, Country_code)
		row += 1
	


def Applegreen(ApplegreenSheet, bold,ApplegreenURL):
	data = []
	# Write some data headers#

	ApplegreenSheet.write('A1', 'StoreId', bold)
	ApplegreenSheet.write('B1', 'StoreName', bold)
	ApplegreenSheet.write('C1', 'Brandname', bold)
	ApplegreenSheet.write('D1', 'Address', bold)
	ApplegreenSheet.write('E1', 'Hours', bold)
	ApplegreenSheet.write('F1', 'Phone', bold)
	ApplegreenSheet.write('G1', 'Email', bold)

	# Start from the first cell below the headers.
	row = 1
	col = 0
	
	# Scrapping starts
	headers = {
		'Connection': 'keep-alive',
		'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
		'Accept': 'application/json, text/javascript, */*; q=0.01',
		'sec-ch-ua-mobile': '?0',
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
		'Origin': 'https://www.applegreenstores.com',
		'Sec-Fetch-Site': 'same-site',
		'Sec-Fetch-Mode': 'cors',
		'Sec-Fetch-Dest': 'empty',
		'Referer': 'https://www.applegreenstores.com/',
		'Accept-Language': 'en-US,en;q=0.9',
	}

	params = (
		('geolat', '52.7384'),
		('geolng', '-4.4647335'),
		('limit', '400'),
		('radius', '600'),
		('country', 'GB'),
	)
	
	response = requests.get(ApplegreenURL, headers=headers, params=params)
	jsoncontent =  json.loads(response.text)	
	for key,iteritems in enumerate(jsoncontent['items']):
		sublist = []
		storeresponse = requests.get(ApplegreenURL+'/'+str(iteritems['id']),headers=headers)
		storedetailcontent =  json.loads(storeresponse.text)
		storeid = storedetailcontent['item']['id']
		storename = storedetailcontent['item']['name']
		brandname = storedetailcontent['item']['brand']
		Address = storedetailcontent['item']['address']
		hours = storedetailcontent['item']['opening_hours']
		Phone = storedetailcontent['item']['phone']
		Email = storedetailcontent['item']['email']
		
		sublist.append(str(storeid))
		sublist.append(str(storename))
		sublist.append(str(brandname))
		sublist.append(str(Address))
		sublist.append(str(hours))
		sublist.append(str(Phone))
		sublist.append(str(Email))
		data.append(sublist)
		
	lists = tuple(data)
	
	# Iterate over the data and write it out row by row.
	for storeid, storename, brandname, Address,hours,Phone,Email, in (lists):
		ApplegreenSheet.write(row, col, storeid)
		ApplegreenSheet.write(row, col + 1, storename)
		ApplegreenSheet.write(row, col + 2, brandname)
		ApplegreenSheet.write(row, col + 3, Address)
		ApplegreenSheet.write(row, col + 4, hours)
		ApplegreenSheet.write(row, col + 5, Phone)
		ApplegreenSheet.write(row, col + 6, Email)
		row += 1
		
# Press the green button in the gutter to run the script.
def storeLocator():
	# Create a workbook and add a worksheet.
	workbook = xlsxwriter.Workbook('store_locator.xlsx')
	galaSheet = workbook.add_worksheet('Gala')
	homeBargainSheet = workbook.add_worksheet('Home Bargains')
	AldiSheet = workbook.add_worksheet('Aldi')
	AmazonSheet = workbook.add_worksheet('Amazon')
	ApplegreenSheet = workbook.add_worksheet('Applegreen')

	# Add a bold format to use to highlight cells.
	bold = workbook.add_format({'bold': True})
	
	'''	Storename: GALA '''	
	GalaUrl='https://www.gala.ie/about-gala/store-locator/'
	galaStore(galaSheet,bold,GalaUrl)
	
	'''
	Storename: Home Bargains '''
	HomeBargainURL='https://storelocator.homebargains.co.uk/all-stores'
	homeBargains(homeBargainSheet, bold,HomeBargainURL)
	
	'''
	storename: Aldi'''
	
	AldiURL='https://www.aldi.co.uk/api/store-finder/search?address=London&latitude=51.5073509&longitude=-0.1277583&page='
	Aldi(AldiSheet, bold,AldiURL)
	
	'''
	storename: Applegreen'''
	
	ApplegreenURL='https://api.applegreenstores.com/v1/locations'
	Applegreen(ApplegreenSheet, bold,ApplegreenURL)
	
	'''
	storename: Amazon'''
	
	AmazonURL='https://d1twkrb4o26gk5.cloudfront.net/index.php'
	Amazon(AmazonSheet, bold,AmazonURL)

	workbook.close()


if __name__ == '__main__':
	storeLocator()

