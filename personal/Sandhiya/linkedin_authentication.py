﻿# -*- coding: utf-8 -*-
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from bs4 import BeautifulSoup
from tabulate import tabulate
import random
import time
import html

from Databot.tools.utiltity import Utilities,Sanitiser
from Databot.preprocessor import login_validation
	
def login(browserObj,databaseObj,ipAddress):
	
	''' Function to do login operations in linkedIn.'''
	
	try:
		sanitiser = Sanitiser(databaseObj)
		utilities = Utilities(databaseObj)
		configData = utilities.get_config_details()
		username = configData.get('Login', 'id')
		password = configData.get('Login', 'password')
		
		if os.path.exists(os.path.join(os.getcwd(), "cookies", 'linkedin_session.pkl')) is True:
			utilities.print_msg("Logging in with previous session\n")
			browserObj.load("https://www.linkedin.com/uas/login?trk=guest_homepage-basic_nav-header-signin")
			browserObj.get_session('LinkedIn')
			loginContent = browserObj.load("https://www.linkedin.com/feed/")
			loginStatus = login_validation.login_status(databaseObj,loginContent)
			if loginStatus == 'pass':
				utilities.print_msg('Login sucessfull.\n')
				browserObj.load("https://www.linkedin.com/sales")
			
			else:
				utilities.print_msg('Login problem...\n')
				utilities.print_msg('Deleting the previous sessions...\n')
				os.remove(os.path.join(os.getcwd(), "cookies", 'linkedin_session.pkl'))
				login(browserObj,databaseObj,ipAddress)
		else:

			utilities.print_msg("Logging in with username '%s'"%username)
			utilities.print_msg("\n")
			browserObj.load("https://www.linkedin.com/uas/login?trk=guest_homepage-basic_nav-header-signin")
			browserObj.input("username",username,False)
			time.sleep(2)
			loginContent = browserObj.input("password",password,True)
			utilities.progress_bar(random.randint(4,7))
			loginStatus = login_validation.login_status(databaseObj,loginContent)
			if loginStatus != 'pass':
				databaseObj.update_instance_status(loginStatus,ipAddress)
				browserObj.close()
				time.sleep(10)
				sys.exit()
			browserObj.save_session('LinkedIn')
			browserObj.load("https://www.linkedin.com/sales")
			
	except Exception as loginIssue:
		databaseObj.update_exception('0000',loginIssue,'Problem with linkedin login')

def Logout(browserObj,databaseObj):
	''' Function to do sign out operation from LinkedIn.'''
	try:
		browserObj.Load("https://www.linkedin.com/sales/logout?trk=d_sales2_nav_account")
		os.remove(os.path.join(os.getcwd(), "cookies", 'linkedin_session.pkl'))
		
	except Exception as logoutIssue:
		databaseObj.update_exception('0000',logoutIssue,'Problem with linkedin login')	