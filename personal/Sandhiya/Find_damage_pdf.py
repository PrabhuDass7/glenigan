from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.converter import HTMLConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
import re
import codecs

def pdf_to_text(pdfname, pageno):

    # PDFMiner boilerplate
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    sio = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
##    device = HTMLConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    # Extract text
    fp = file(pdfname, 'rb')
    for pageNumber, page in enumerate(PDFPage.get_pages(fp)):
        if pageNumber == pageno:
            interpreter.process_page(page)

##    for page in PDFPage.get_pages(fp):
##        interpreter.process_page(page)
    fp.close()

    # Get text from StringIO
    text = retstr.getvalue()

    # Cleanup
    device.close()
    retstr.close()

    return text
count = 0
var = open("inputfile.txt", "r")
var1 = var.readlines()
##print var1
for  v1 in var1:
    v2=v1.replace("\n","")
    for page1 in range(0,1):
        try:
            data=pdf_to_text(v2,page1)
            print v2
            pdftxt = open('Correct.txt','a')        
            pdftxt.write(str(v2)+"\n")
            pdftxt.close()
        except Exception,e:
            print str(e)
            print v2
            pdftxt = open('Missed.txt','a')        
            pdftxt.write(str(v2)+"\n")
            pdftxt.close()
            
            
                
                    
##                if "Purpose for Submission" in data:
##                    pdftxt = open('inputdata.txt','a')        
##                    pdftxt.write(str(v2)+"\n")
##                    pdftxt.close()
##                else:
##                    pdftxt = open('Missing.txt','a')
##                    pdftxt.write(str(v2)+"\n")
##                    pdftxt.close()
                
        
