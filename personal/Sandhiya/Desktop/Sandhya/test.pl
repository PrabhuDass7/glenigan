use strict;
use LWP::Simple;
use LWP::UserAgent;
use HTML::Entities;
use HTTP::Cookies;
use POSIX 'strftime';
use Time::Piece;
use Time::Seconds;
use DateTime;
use Date::Manip;
use URI::Encode qw(uri_encode uri_decode);

use URI::URL;
use HTTP::Cookies;
use WWW::Mechanize;
use IO::Socket::SSL qw();
use Mozilla::CA;
use Cwd;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use JSON;


my $ua=LWP::UserAgent->new();
$ua->agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36");
$ua->max_redirect(0);
# my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,); 
# $ua->cookie_jar($cookie_jar);
# $cookie_jar->save;

use URI::Escape;


#--------- Post Content ----------------------

my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);               
$ua->cookie_jar($cookie_jar);
$cookie_jar->save;


my $Host = 'msp.havering.gov.uk';
my $Content_Type='application/json; charset=UTF-8';
my $Referer='https://msp.havering.gov.uk/planning/search-applications';

my $Proxy="";

my $Doc_Post_URL='https://msp.havering.gov.uk/civica/Resource/Civica/Handler.ashx/doc/list';

my $Post_Content='{"KeyNumb":"0","KeyText":"F0009.20","RefType":"PLANNINGCASE"}';

my ($con,$code)=&Post_Method($Doc_Post_URL,$Host,$Content_Type,$Referer,$Post_Content,$Proxy);
open JJ,">tstet.html";
print JJ "$con";
close JJ;

sub mech_UserAgent()
{
    my($currentCouncilCode) = @_;
	my $mech;

    if($currentCouncilCode eq 'N')
    {
        $mech = WWW::Mechanize->new(autocheck => 0);
    }
    else
    {	
		print "###############################\n";
        # $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$mech = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);
    }
    return($mech);
}

sub docPostMechMethod() 
{
    my $URL = shift;
    my $postcontent = shift;
    my $mech = shift;
   
	$URL=~s/amp\;//igs;
	my $c=1;
	my $proxyFlag=0;
	rePingwith_192:
	$mech->post( $URL, Content => "$postcontent");
	  
	my $con = $mech->content;
    my $code = $mech->status;
	my $res = $mech->res();
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		$c++;
		
		goto nxt if($c == 3);
		goto rePingwith_192;
	}
	
	nxt:
	
	my $filename;
	if($res->is_success){
		$filename = $res->filename();
	}
	$filename=~s/\s+/_/igs;
    return($con,$code,$filename);
}

sub Post_Method()
{
	my $Doc_Post_URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	my $Proxy=shift;
	my $rerun_count=0;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	# }
	
	$Doc_Post_URL =~ s/^\s+|\s+$//g;
	$Doc_Post_URL =~ s/amp;//igs;
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(POST=>$Doc_Post_URL);
	$req->header("Host"=> "$Host");
	# $req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Referer"=> "$Referer");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=> "en-us,en;q=0.5");
	# $req->header("Accept-Language"=> "cookie: ASP.NET_SessionId=tfnjtsmadh3ctefptxeudnoc; CivicaTownW2Auth=2636615309DFD57D63F60354A738250BC81AF857B0BB640BA11D7466CC979AF1DC4D9B50433633BF549486A00B22D155C0EC9B2A6C53A4C601DDE195484E1E2679BD9941B4A6C34CAD214CE07B698D08CD19E5EF00C1B4869192091B88805673BAC5573480FC1D37FFECB7B3F56AD541638CDA2FBF9D18DAEC327EFF2D242A79569BA2DD5A29465D0BAC5B1455D80F35BC1E4FD71BDB9586B2C0ABCAB14DF135F7F5B2A455D6BBB91E24004399F0EF4F3A0065BECDBB1394CCAB6CFFD32EA1F8F1222F52F9067085EBB7C9E56F715B68ADB11152B31A142BFB8EAF8B52F60BF641907446934B49BD8DC6C2AE56DCF913966F2AA4D965FC0BDFCB0CD15735EA91D11F5C22E905F541A0A01CAB6F590FE9");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# my %res=%$res;
	# my $headers=$res{'_headers'};
	# my %headers=%$headers;

	# for my $k(keys %headers)
	# {
		# print "[$k	:$headers{$k}]\n";
	# }	
	# my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	# print "set-cookie:: $Cookie\n";	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$Doc_Post_URL);
			my $u2=$u1->abs;
			my $Redir_url;
			($content,$Redir_url)=&Getcontent($u2,$Doc_Post_URL,$Host,$Content_Type,$Referer,$Proxy);
		}
	}
	elsif($Proxy eq 'Y')
	{
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			print "via 192...\n";
			$proxyFlag=1;
			# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		if ($rerun_count < 1)
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	return ($content,$code);
}
