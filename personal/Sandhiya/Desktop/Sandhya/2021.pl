use strict;
use WWW::Mechanize;
use LWP::Simple;
use HTTP::CookieMonster;
use FileHandle;
use URI::URL;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use DBI;
use DBD::ODBC;

use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename qw(dirname);

use strict;
use LWP::UserAgent;
use URI::URL;
use HTTP::Cookies;
use HTML::Entities;
use Cwd qw(abs_path);
use File::Basename;
use LWP::Simple;
use MIME::Base64;
use URI::Escape;
use WWW::Mechanize;
use IO::Socket::SSL qw();
# use Unicode::Escape;
use URI::Encode;
use Config::Tiny;
use Encode;
use File::stat;
require Exporter;
use HTML::TableExtract;
use Mozilla::CA;
use File::Path 'rmtree';
use File::Find;


#--------- LWP UserAgent declaration  ----------------#
my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n"; <STDIN>;
my $councilCode=$ARGV[0];
my $inputFormatID=$ARGV[1];

my $localDownloadDirectory = ($basePath.'/Documents');

my $driver = 'SQL Server';
# my $server = '172.27.137.184';
my $server = '10.101.53.25';
my $database = 'ScreenScrapper';
my $uid	= 'User2';
my $pwd	= 'Merit456';

my $SSL_Verify='Y';
my $Proxy='proxy1';
my $Host='portal.monaghancoco.ie';
my $Content_Type='N/A';
my $Referer='N/A';
my $Need_Host='Y';
my $Accept_Language='en-US,en;q=0.5';


my %inputHashCode;

my $query ="select id,SOURCE ,Application_No ,Document_Url ,MARKUP ,URL, No_of_Documents,
		case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME 
		from ( 
		select ID,Source,Application_No,Document_Url,F.MARKUP,URL,No_of_Documents, 
		isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
		isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
		isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME
		from FORMAT_PUBLIC_ACCESS f,L_council co
		where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-350,112) and CONVERT(Varchar(10),GETDATE(),112)
		And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = $councilCode And co.COUNCIL_CD_P!='486' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor') 
		and ID in ($inputFormatID)
		) as c ORDER BY ID DESC";

my $dbh = &DbConnection();

my $sth = $dbh->prepare($query);
$sth->execute();
	
while(my @record = $sth->fetchrow)
{
	$inputHashCode{$record[0]} = [$record[0],$record[1],$record[2],$record[3],$record[4],$record[5],$record[6],$record[7],$record[8],$record[9]];
}
$sth->finish();

foreach my $hashValue(sort keys %inputHashCode)
{

	my $formatID 		= $inputHashCode{$hashValue}[0];
	my $Source 			= $inputHashCode{$hashValue}[1];
	my $applicationNo 	= $inputHashCode{$hashValue}[2];
	my $documentURL 	= $inputHashCode{$hashValue}[3];
	my $Markup 			= $inputHashCode{$hashValue}[4];
	my $applicationURL 	= $inputHashCode{$hashValue}[5];
	my $noOfDocumentsfromDB	= $inputHashCode{$hashValue}[6];
	my $projectID 		= $inputHashCode{$hashValue}[7];
	my $projectStatus	= $inputHashCode{$hashValue}[8];
	my $councilName 	= $inputHashCode{$hashValue}[9];

	print "\n##############\n";
	print "formatID==>$formatID\n";
	print "Source==>$Source\n";
	print "applicationNo==>$applicationNo\n";
	print "documentURL==>$documentURL\n";
	print "Markup==>$Markup\n";
	print "applicationURL==>$applicationURL\n";
	print "noOfDocumentsfromDB==>$noOfDocumentsfromDB\n";
	print "projectID==>$projectID\n";
	print "projectStatus==>$projectStatus\n";
	print "councilName==>$councilName\n";
	print "##############\n";<STDIN>;
	
	# next if ($formatID!~m/9363835/is);
	# next if ($formatID!~m/9405843/is);
	
	my $tempDownloadLocation = $localDownloadDirectory.'/'.$formatID;
	print "tempDownloadLocation=>$tempDownloadLocation\n";
	unless ( -d $tempDownloadLocation )
	{
		$tempDownloadLocation=~s/\//\\/igs;
		system("mkdir $tempDownloadLocation");
		# mkdir $tempDownloadLocation;
	}

	print "Folder Created[[MKDIR]]====> $tempDownloadLocation\n";
	
			
	# my $mech = WWW::Mechanize->new();
	
	my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});

	# my $url = 'http://www.sdublincoco.ie/Planning/Details?p=1&regref=SD19A%2F0319';
	my $i=0;
	reping:
	if ($i==1)
	{
		# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# old Proxy	
	}
	else
	{
		# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy	
	}
	my $mechUserAgent=&mechUserAgent($SSL_Verify);
	my $lwpUserAgent=&lwpUserAgent($SSL_Verify);
	# $Host='monaghanpf.secure.data-store.ie';
	$Host='portal.monaghancoco.ie';
	# my $con=&getMethodLWP($documentURL,$applicationURL,$Host,$Content_Type,'',$Proxy,$Need_Host,$lwpUserAgent,'');
	
	
	# $applicationNo=20350;
	
	#------------------------------------------------------------------------------------------
	
			my $Document_Url='https://portal.monaghancoco.ie/WebLinkPlanningPortal/search.aspx?dbid=0&searchcommand={LF:Name~='.$applicationNo.'}';
			
			my $doc_id=$1 if($Document_Url=~m/[^>]*?searchcommand=\s*([^>]*?)\s*$/is);
			my $posturl='https://portal.monaghancoco.ie/WeblinkPlanningPortal/SearchService.aspx/GetSearchListing';
			my $Referer='https://portal.monaghancoco.ie/WebLinkPlanningPortal/search.aspx?dbid=0&searchcommand='.$doc_id.'&cr=1';
			my $post_cnt='{"repoName":"MONLFPLANNING","searchSyn":"'.$doc_id.'","searchUuid":"","sortColumn":"","startIdx":0,"endIdx":20,"getNewListing":true,"sortOrder":2,"displayInGridView":false}';
			$mech->add_header( "Host" => "$Host" );
			$mech->add_header( "Referer" => "$Referer" );
			$mech->post($posturl, Content => "$post_cnt");
			my $pingStatus = $mech->status;	
			print "plan_url3 Page ping Status==>$pingStatus\n";
			my $Content = $mech->content;
			
			open(DC,">2021_Home_Content_2.html");
			print DC $Content;
			close DC;
	
			my $search_Id=$1 if($Content=~m/\"searchUUID\"\:\"([^>]*?)\"/is);
			$search_Id=~s/\s*//igs;
			
			my %Doc_Details;
			while($Content=~m/\"entryId\"\:([\d]+)\,/igs)
			{
				my $doc_id1=$1;
				my $plan_url1='https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/GetBasicDocumentInfo';
				
				print "doc_id1:::$doc_id1\n\n";<STDIN>;
			
				my $post_cnt='{"repoName":"MonLFPlanning","entryId":'.$doc_id1.'}';
								
				$mech->add_header( "Host" => "$Host" );
				$mech->add_header( "Referer" => "$Referer");
				$mech->post($plan_url1, Content => "$post_cnt");
				my $pingStatus = $mech->status;	
				print "plan_url3 Page ping Status==>$pingStatus\n";
				my $Content1 = $mech->content;
				
				open F1, ">ss666.html";
				print F1 $Content1;
				close F1;
				
				my $pg_count=$1 if($Content1=~m/pageCount\"\:([\d]+)\,/is);
				my $document_type=$1 if($Content1=~m/\"Document\s*Type\"\,\"values\"\:\[\"([^>]*?)\"\]\,/is);
				my $pdf_name=$document_type.".pdf";
				
				my $plan_url2='https://portal.monaghancoco.ie/WeblinkPlanningPortal/GeneratePDF10.aspx?key='.$doc_id1.'&PageRange=1%20-%20'.$pg_count.'&Watermark=0';
				
				# $plan_url2='https://portal.monaghancoco.ie/WeblinkPlanningPortal/GeneratePDF10.aspx?key=344097&PageRange=1%20-%2014&Watermark=0';
				
				print "******plan_url2:: $plan_url2*******\n";
				# my ($Content2,$Codes) = &Download_Utility::Getcontent($plan_url2,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
				# https://portal.monaghancoco.ie/WeblinkPlanningPortal/GeneratePDF10.aspx?key=346799&PageRange=1%20-%204&Watermark=0
				
				$mech->get($plan_url2);
				my $code=$mech->status();
				my $Content2=$mech->content();
				
				print "Code:: $code";<STDIN>;
				
				# my $Content2=get($plan_url2);
				
				open F1, ">ss777.html";
				print F1 $Content2;
				close F1;
				
				my $idd=$1 if($Content2=~m/^([^>]*?)\n/is);
				
				$idd=~s/\s*//igs;
				
				print "******idd:: $idd*******\n";
				
				my $Doc_URL="https://portal.monaghancoco.ie/WeblinkPlanningPortal/PDF10/$idd/$doc_id1";
				# my $Doc_URL="https://portal.monaghancoco.ie/WeblinkPlanningPortal/Helper/TileData.aspx?repo=MONLFPLANNING&docID=401533&x=0&y=0&pageNum=1&scale=660&ro=0&time=1611315075440&showAnn=0&pageID=1739294&search=7fb3608c-d937-42cf-b92a-61b5cbee27b8";
				print "Doc_URL**** $Doc_URL ***\n";
				
				# my $Doc_URL2="https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocView.aspx?id=346799&dbid=0&repo=MONLFPLANNING&searchid=$idd&cr=1";
				# https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocView.aspx?id=401518&dbid=0&repo=MONLFPLANNING&searchid=2f47367b-eecd-4a79-bebd-6fcc19a3a2a2
				
				# $mech->add_header( "authority" => "portal.monaghancoco.ie");
				# $mech->add_header( "method" => "GET");
				# $mech->add_header( "path" => '/WeblinkPlanningPortal/Helper/TileData.aspx?repo=MONLFPLANNING&docID=401533&x=0&y=0&pageNum=1&scale=660&ro=0&time=1611315075440&showAnn=0&pageID=1739294&search=7fb3608c-d937-42cf-b92a-61b5cbee27b8');
				# $mech->add_header( "referer" => 'https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocView.aspx?id=401533&dbid=0&repo=MONLFPLANNING&searchid=7fb3608c-d937-42cf-b92a-61b5cbee27b8');
				# $mech->add_header( "scheme" => 'https');
				# $mech->add_header( "cookie" => 'WebLinkSession=mybrpmndjltmdsxby2oz0osk; AcceptsCookies=1; lastSessionAccess=637469118749080824');
				# $mech->add_header( "upgrade-insecure-requests" => '1');
				# $mech->add_header( "accept-encoding" => 'gzip, deflate, br');
				# $mech->add_header( "accept-language" => 'en-US,en;q=0.9');
				# $mech->add_header( "content-type" => 'image/png');
				
				$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,"",""];
				
				# my $pdf_content=&Getcontent($Doc_URL,"","","","","","","");
				
				
				$mech->get($Doc_URL);
				my $code=$mech->status();
				my $pdf_content=$mech->content();
				
				open( FILE, ">$tempDownloadLocation"."/"."$pdf_name");
				binmode FILE;
				print FILE $pdf_content;
				close( FILE );
				
				
				# ---------------------------------------------------------------------------------------
				
				# my $new_URL='https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/GetTextHtmlForPage';
				
				# my $post_content = '{"repoName":"MONLFPLANNING","documentId":401518,"pageNum":4,"showAnn":false,"searchUuid":""}';
				
				# $mech->post( $new_URL, Content => $post_content);
				# my $con = $mech->content;
				# $code = $mech->status;
				# my $res = $mech->res();
				
				# $pdf_name=~s/\.pdf/\.html/igs;
				# my $file_name="$tempDownloadLocation"."/"."$pdf_name";
				
				# my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $tempDownloadLocation/$pdf_name for write :$!";
				# binmode($fh);
				# $fh->print($con);
				# $fh->close();	
				
				# my $size = stat($file_name)->size;
				# my $return_size_kb=sprintf("%.2f", $size / 1024);
				# print "FileSizein_kb  :: $return_size_kb\n";
								
				# my $mechUserAgent=&mechUserAgent($SSL_Verify);
				# my $lwpUserAgent=&lwpUserAgent($SSL_Verify);
				# my $con=&Download($Doc_URL,$plan_url1,$Host,$Content_Type,'',$Proxy,$Need_Host,$lwpUserAgent,'',$tempDownloadLocation);
			}
	
}

sub Download()
{
	my $documentURL=shift;
	my $applicationURL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $lwpAgent=shift;
	my $Accept_Language=shift;
	my $tempDownloadLocation=shift;
	
	my $file_name;
	my ($pdf_content, $code, $file_name)=&getMethodLWP($documentURL,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language);
	# my ($pdf_content)=get($documentURL);
	$file_name=~s/\///igs;
	if($file_name=~m/^\s*$/is)
	{
		$file_name=$documentURL;
		$file_name=~s/^[^>]+\/([^>]*?)$/$1/igs;
		$file_name=~s/\?[^>]*?$//igs;
	}
	# print "code:::$code\n\n";
	print "file_name:::$file_name\n\n";
	# <STDIN>;
	open( FILE, ">$tempDownloadLocation"."/"."$file_name");
	binmode FILE;
	print FILE $pdf_content;
	close( FILE );
	
	# mirror($documentURL,$file_name);
		
	return($pdf_content);
}



sub Getcontent()
{
	my $Document_Url=shift;
	my $URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $Accept_Language=shift;

	if($Proxy eq 'Y')
	{
		print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	}
	elsif($Proxy eq '192')
	{
		print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	}
	
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE 1:: $code :: $Proxy\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		print "CODE 2:: $code :: $Proxy\n";
	}
	elsif($code=~m/30/is)
	{
		print "CODE 3:: $code :: $Proxy\n";
		my $loc=$res->header("location");
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				if($Document_Url=~m/rugby\.gov\.uk/is)
				{
					$Document_Url=$Document_Url;					
				}
				else
				{
					$Document_Url=$u2;
				}
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	elsif($Proxy eq 'Y')
	{
		print "CODE 4:: $code :: $Proxy\n";
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			$proxyFlag=1;
			print "via [[192]]...\n";
			$ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		print "CODE 5:: $code :: $Proxy\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# print "redir_url==>$redir_url\n";<STDIN>;
	return ($content,$code,$redir_url);
}

sub mechUserAgent()
{
	# my $cCode = shift;
	my $SSL_Verify = shift;
	
	my $mech;
    if($SSL_Verify eq 'N')
    {
        $mech = WWW::Mechanize->new(autocheck => 0);
    }
    else
    {	
		$mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
    }
	
    return($mech);
}

sub lwpUserAgent()
{
	# my $cCode = shift;
	my $SSL_Verify = shift;
	
	my ($ua,$cookie);
    if($SSL_Verify eq 'N')
    {
        $ua = LWP::UserAgent->new;
		$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
		$ua->max_redirect(0); 
		$ua->cookie_jar({});
		$ua->ssl_opts( verify_hostname => 0 );
		$cookie = HTTP::Cookies->new(file=>"cookiefile.txt", autosave=>1);
		$ua->cookie_jar($cookie);
    }
    else
    {	
		$ua = LWP::UserAgent->new(ssl_opts => {
				  SSL_verify_mode => SSL_VERIFY_NONE,
				  verify_hostname => 1,
				  keep_alive => 1
			  });
		# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
		$ua->agent("Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Mobile Safari/537.36");
		$ua->max_redirect(0); 
		$ua->cookie_jar({});
		$cookie = HTTP::Cookies->new(file=>"cookiefile.txt", autosave=>1);
		$ua->cookie_jar($cookie);
    }
	
    return($ua);
}

sub getMethodLWP()
{
	my $documentURL=shift;
	my $applicationURL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $lwpAgent=shift;
	my $Accept_Language=shift;

	my $filename;
	my $proxyDetail1 = 'http://172.27.137.199:3128';
	my $proxyDetail2 = 'http://172.27.137.192:3128';
	
	if($Proxy eq 'proxy1')
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail1);
		print "proxyDetail1==>$proxyDetail1\n";
	}
	elsif($Proxy eq 'proxy2')
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail2);
		print "proxyDetail2==>$proxyDetail2\n";
	}
	
	my $rerunCount=0;
	my ($redirectURL,$content, $code);
	if($documentURL=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	
	GetHome:
	if(( $rerunCount >= 2 ) && ($code!~m/^20/is))
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail2);
		print "proxyDetail2==>$proxyDetail2\n";
	}
	
	
	print "getURL==>$documentURL\n";
	
	
	my $req = HTTP::Request->new(GET=>$documentURL);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	
	my $res = $lwpAgent->request($req);
	my $cookie = HTTP::Cookies->new(file=>"cookiefile.txt", autosave=>1);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	$code=$res->code;
	# my $status_line=$res->status_line;
	
	print "LWP GET ResponseCode:: $code\n";
	# print "\n-------req------------\n";
	# print $req->headers_as_string();
	# print "-------------------\n";
	print "--------Res-----------\n";
	# print $res->headers_as_string();
	# print "-------------------\n";
	my $res_cont=$res->headers_as_string();
	print $res_cont;
	$filename=$1 if($res_cont=~m/Content-Disposition\:\s*attachment\;\s*filename=\"([^\"]*?)\"/is);
	# exit;
	if($code=~m/^20/is)
	{
		$content = $res->content;
		$content = $res->decoded_content;
	}
	elsif($code=~m/^30/is)
	{
		my $loc=$res->header("location");
		if( $rerunCount <= 3 )
		{
			$rerunCount++;
			if($loc!~m/http/is)
			{
				# $redirectURL=&formAbsoluteURL($loc,$documentURL);
				my $u1=URI::URL->new($loc,$documentURL);
				my $u2=$u1->abs;
				$redirectURL=$u2;
			}
			else
			{
				$documentURL=$loc;
				$redirectURL=$loc;
			}
			goto GetHome;
		}
	}
	else
	{
		if( $rerunCount <= 1 )
		{
			$rerunCount++;
			
			sleep 1;
			goto GetHome;
		}
	}
	# open(DC,">pdf_content.html");
	# print DC $content;
	# close DC;
	# exit;
	
	return ($content,$code,$filename);
}

# sub Lwp_Get_Proxy()
# {
    # my $url = shift;
    # my $ref = shift;
	# my @param=@$ref if($ref ne '');
	# $url =~ s/amp\;//igs;	
	# my $count=3;
	# HOME:
	# # make request
	# my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	# $req->header("Accept-Encoding"=>"gzip, deflate, br");	
	# $req->header("Connection"=>"keep-alive");	

	# foreach my $header(@param)
	# {
		# if($header=~m/\"([^\"]*?)\"\=\>\"([^\"]*?)\"/is)
		# {
			# my $head=$1;
			# my $tail=$2;

			# $req->header("$head"=>"$tail");
			# print "\nHeaders :: $header\n";
		# }
	# }
	# my $res = $ua->request($req); 
	# my $res_cont=$res->headers_as_string();
	
	# $cookie->extract_cookies($res); 
	# # print "\n-------req------------\n";
	# # print $req->headers_as_string();
	# # print "-------------------\n";
	# # print "--------Res-----------\n";
	# # print $res->headers_as_string();
	# # print "-------------------\n";
	# # print $res_cont;
	# # exit;
	
	# # $res->as_string;
	
	# # my $loc = $res->header( 'Location' );
	 # # print "\nLocation :: $loc\n";
		   
	# # $cookie->clear;
	
	# my $code = $res->code();
	# print "\n----code-----$code----->\n";
	# my $con;
	# if($code=~m/200/is)
	# {
		# $con=$res->decoded_content;
	# }
	# elsif(($code=~m/50|40/is)&&($count >= 1))
	# {
		# $count--;
		# goto HOME;
	# }
	# else
	# {
		# open(FF,">>error_code_url.txt");
		# print FF "$url\n";
		# close FF;
	# }
	
	# return $con,$code;
# }

sub DbConnection()
{
	# Test DB
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.183;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# Live DB
	my $dsn1 ='driver={SQL Server};Server=10.101.53.25;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}

sub DbConnection_old()
{
	my $dsn ='driver={'.$driver.'};Server='.$server.';database='.$database.';uid='.$uid.';pwd='.$pwd.';'; 
	
	my $dbh;
	my $reConFlag=1;
	Reconnect:
	# if($dbh = DBI->connect("DBI:ODBC:$dsn"))
	if($dbh = DBI->connect("dbi:Sybase:server=10.101.53.25;database=SCREENSCRAPPER", 'User2', 'Merit456'))
	{
		print "\nDB SERVER CONNECTED\n";
	}
	else
	{
		print "\nDB CONNENCTION FAILED\n";
		if($reConFlag<=3)
		{
			$reConFlag++;
			goto Reconnect;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}