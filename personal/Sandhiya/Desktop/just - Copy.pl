#!/usr/bin/perl
use strict;
use DateTime;
# use warnings;
use IO::Socket::SSL;
use WWW::Mechanize;
use List::Util qw(first);
# use Devel::Size qw(size total_size);


####
# Declare global variables
####
my $mech;
my $filename = 'C:\\Users\\mgoas\\Desktop\\New folder\\AllLiveURLs.txt';

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date

if (open(my $fh, '<:encoding(UTF-8)', $filename)) 
{
    while (my $url = <$fh>) 
	{
		chomp $url;
		
		my ($CouncilCode,$URL,$SSL_VERIFICATION);
		if($url=~m/(\d+)\s=\s([^<]*?)$/is)
		{
			$CouncilCode=$1;
			$URL=$2;
		}
		print "URL=>$URL\n";
		
		my $rerunCount=0;
		my ($responseCode,$content,$newURL,$searchURL);
		Loop:
		if($URL=~m/^https/is)
		{
			$SSL_VERIFICATION = 'Y';
		}
		
		if($SSL_VERIFICATION=~m/^Y$/is)
		{
			$mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
					);
		}
		else
		{	
			$mech = WWW::Mechanize->new(autocheck => 0);			
		}	
		
		if($responseCode=~m/^(5\d{2}|4\d{2})$/is)
		{
			print "Council code:$CouncilCode === Working via Old proxy..\n";
			$mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
		}
		else
		{
			print "Council code:$CouncilCode === Working via New proxy..\n";
			$mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy	
		}
		
		$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$mech->get($URL);
		
		$content = $mech->content;
		$responseCode = $mech->status;
		$newURL = $mech->uri();
		print "responseCode=>$responseCode\n";
		
		# my $sizes = size($responseCode);
		# print "$sizes\n"; 
		# my $size = &scaleIt($responseCode);
		# print "$size\n"; <STDIN>;
		
		
		$searchURL = $URL;
	
		if(($responseCode!~m/^\s*200\s*$/is) && ($rerunCount<=1))
		{
			$rerunCount++;
			goto Loop;
		}
		
		my $search = "searchCriteriaForm";
		
		open(PP,">>${todaysDate}.txt");
		print PP "$CouncilCode==>$URL==>$responseCode\n";
		close(PP);
			
	}
   close($fh);
}
else
{
  warn "Could not open file '$filename' $!";
}


### Change bytes to KB,MB,GB ###
sub scaleIt {
    my( $size, $n ) =( shift, 0 );
    ++$n and $size /= 1024 until $size < 1024;
    return sprintf "%.2f %s", 
          $size, ( qw[ bytes KB MB GB ] )[ $n ];
}
 