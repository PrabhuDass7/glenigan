import cv2
import sys
import pytesseract
from PIL import Image

pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files (x86)\\Tesseract-OCR'
# Read image path from command line
print(pytesseract.image_to_string(Image.open('Captcha.JPEG')))
# Read image from disk
# im = cv2.imread('Captcha.JPEG', 0)

# Run tesseract OCR on image
# text = py.pytesseract.image_to_string(im)

# Print recognized text
# print(text)