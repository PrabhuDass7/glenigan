def load(self, url):
        
        '''Function to load an URL in the browser.'''

 

        startTime = time.time()

 

        self.utilityObj.print_msg('Requesting URL :' + url + '\n')
        time.sleep(1)
        try:
            self.driver.set_page_load_timeout(200)
            self.driver.get(url)
            scroll = self.driver.find_element_by_tag_name('body')
            try:
                for i in range(0, 15):
                    time.sleep(2)
                    scroll.send_keys(Keys.PAGE_DOWN)
                for i in range(0, 15):
                    time.sleep(2)
                    scroll.send_keys(Keys.PAGE_UP)
            except:
                pass
            time.sleep(random.randint(3, 7))
            pageSource = self.driver.page_source
            pageSource = html.unescape(pageSource)
            return pageSource
        
        except TimeoutException as timeoutException:
            self.databaseObj.update_exception('0000', timeoutException, 'Problem with Browser Timeout')
            time.sleep(10)
            return 'Browser timeout problem'
            
def get_session(self,sessionName):
    
        '''Function to open browser using previous saved session'''
        sessionName = 'linkedin_session.pkl' if sessionName == 'LinkedIn' 
        for cookie in pickle.load(open(os.path.join(os.getcwd(), "cookies", sessionName), 'rb')):
            self.driver.add_cookie(cookie)

 

    def save_session(self,sessionName):
    
        '''Function to capture current session of the browser'''
        sessionName = 'linkedin_session.pkl' if sessionName == 'LinkedIn' else 'insideview_session.pkl'
        os.makedirs(os.path.join(os.getcwd(), "cookies"), exist_ok=True)
        pickle.dump(self.driver.get_cookies(), open(os.path.join(os.getcwd(), "cookies", sessionName), 'wb'))
        
    def get_content(self):
    
        ''' Function to get the page content '''
        time.sleep(random.randint(5, 12))
        pageSource = self.driver.page_source
        pageSource = html.unescape(pageSource)
        return pageSource