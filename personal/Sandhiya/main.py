import requests
from datetime import datetime, timedelta
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import sys, os, re
import time
import pymssql
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
requests.urllib3.disable_warnings()

# daterange search section
class Actions(object):
    pass


# # dbConnection Section
# def dbConnection(database):
#     conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
#     # conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
#     return (conn)

if __name__ == "__main__":
    councilcode = sys.argv[1]
    gcs = sys.argv[2]

    if councilcode is None:
        print('Councilcode arugument is missing')
        sys.exit()

    if gcs is None:
        print('GCS arugument is missing')
        sys.exit()

    # conn = ""
    # cursor = ""
    # conn = dbConnection("SCREENSCRAPPER")
    # cursor = conn.cursor()
    #
    # PROXY = "172.27.137.199:3128"  # IP:PORT or HOST:PORT
    #
    # chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
    # chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

    # -- Setup


    chrome_options = Options()
    # chrome_options.add_argument("--headless")
    # chrome_options.add_argument('--log-level=3').

    browser = webdriver.Chrome(chrome_options=chrome_options,
                               executable_path=r'C:\\Users\\bala\\Desktop\\wriggler images\\chromedriver.exe')
    # browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver.exe')
    browser.maximize_window()


    thisgcs = {
        "GCS001": "0",
        "GCS002": "7",
        "GCS003": "14",
        "GCS004": "21",
        "GCS005": "28",
        "GCS090": "90",
        "GCS180": "180",
    }

    gcsdate = thisgcs[gcs]
    date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))

    format = "%d/%m/%Y"

    todate = date_N_days_ago.strftime(format)

    preday = date_N_days_ago - timedelta(days=6)
    fromdate = preday.strftime(format)
    print('fromdate     :', fromdate)
    print('todate   :', todate)


    browser.get('https://applications.greatercambridgeplanning.org/online-applications//search.do?action=advanced')
    time.sleep(3)

    browser.find_element_by_xpath('// *[ @ id = "search"]').click()

    action = ActionChains(browser)

    firstLevelMenu = browser.find_element_by_xpath('//*[@id="planLinks"]')
    action.move_to_element(firstLevelMenu).perform()

    secondLevelMenu = browser.find_element_by_xpath('// *[ @ id = "planAdvancedSearch"]')
    action.move_to_element(secondLevelMenu).perform()

    secondLevelMenu.click()

    # browser.find_element_by_xpath('// *[ @ id = "planAdvancedSearch"]').click()

    browser.find_element_by_xpath('//*[@id="applicationValidatedStart"]').send_keys(fromdate)
    time.sleep(3)
    browser.find_element_by_xpath('//*[@id="applicationValidatedEnd"]').send_keys(todate)
    browser.find_element_by_xpath('//*[@id="advancedSearchForm"]/div[4]/input[2]').click()
    time.sleep(3)
    browser.find_element_by_xpath('//*[@id="resultsPerPage"]').send_keys(100)
    browser.find_element_by_xpath('// *[ @ id = "searchResults"] / input[4]').click()

    time.sleep(3)

    pageTop = browser.find_element_by_xpath('//*[@id="searchResultsContainer"]/p[1]')
    pageCountText = pageTop.find_elements_by_class_name('page')
    if(pageTop.find_elements_by_class_name('page')):
        pageCount = pageCountText[-1].text
    else:
        pageCount = 1

    contentNamesList = []
    contentlinksList = []
    # countString = browser.find_element_by_xpath('// *[ @ id = "searchResultsContainer"] / p[1] / span[1]')
    # iCount=countString.text

    # Page Looping
    i = 1
    loopCount = int(pageCount)
    while i <= loopCount:
        try:
            time.sleep(10)
            contentList = browser.find_elements_by_class_name('searchresult')
            for item in contentList:
                try:
                    contentLink = item.find_element_by_tag_name('a')
                    contentNamesList.append(contentLink.text)
                    # print(contentLink.text)
                    activeLink = contentLink.get_attribute('href')
                    contentlinksList.append(activeLink)

                except Exception as e:
                    print("ERROR :: ", e)

            if (browser.find_element_by_class_name('next')):
                nextpage = browser.find_element_by_class_name('next').click()
                print("*** Next Page Clicked ***")
                i = i + 1
        except Exception as e:
            print(len(contentNamesList))
            print(len(contentlinksList))
            print("ERROR :: ", e)
            i = i + 1

    # print(len(contentNamesList))
    # print(len(contentlinksList))

    time.sleep(5)

    insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Northing, Easting) values '
    (Application, ProposalCnt, ApplicationType, AppAddress, ApplicationStatus, DateApplicationReceived,
     DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress,
     AgentEmail, AgentTelephone, TargetDecdt, EastingData, NorthingData) = (
    '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '')

    # URL Summary
    ses = requests.session()
    headers = {
        'Host': 'applications.greatercambridgeplanning.org',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'Cookie': 'JSESSIONID=6IexKkqKAsCZ5VUNbBtUZXHde8DhkTGuLaWYwDBA.gcp-palive-dmz1',
    }
    ses.headers.update(headers)

    for AppURL in contentlinksList:

        # url = 'https://applications.greatercambridgeplanning.org/online-applications/applicationDetails.do?keyVal=QR53C2DX0AS00&activeTab=summary'
        resp = requests.get(AppURL , verify=False)
        code = resp.status_code
        print(AppURL)
        soup = BeautifulSoup(resp.text, "html.parser")
        if(soup.find('table',  {"summary":"Case Details"})):
            summaryTableList = soup.find('table',  {"summary":"Case Details"})
            summaryTableRows = summaryTableList.findAll('tr')
            for rows in summaryTableRows:
                tableHead = rows.find('th')
                if(tableHead.text.strip()=='Reference'):
                    Application = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Application Received'):
                    DateApplicationReceived = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Application Validated'):
                    DateApplicationvalidated = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Proposal'):
                    ProposalCnt = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Status'):
                    ApplicationStatus = rows.find('td').text.strip()

        # URL Further Information

        fiUrl = soup.find('a', {"id": "subtab_details"}).get('href')
        fiResp = requests.get('https://applications.greatercambridgeplanning.org/'+fiUrl, verify=False)
        soup = BeautifulSoup(fiResp.text, "html.parser")
        if(soup.find('table', {"summary": "Application details"})):
            fiTableList = soup.find('table', {"summary": "Application details"})
            fiTableRows = fiTableList.findAll('tr')
            for rows in fiTableRows:
                tableHead = rows.find('th')
                if (tableHead.text.strip() == 'Application Type'):
                    ApplicationType = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Applicant Name'):
                    ApplicantName = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Agent Name'):
                    AgentName = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'Agent Address'):
                    AgentAddress = rows.find('td').text.strip()

        # URL Contacts

        iContactsUrl = soup.find('a', {"id": "subtab_contacts"}).get('href')
        iContactsResp = requests.get('https://applications.greatercambridgeplanning.org/' + iContactsUrl, verify=False)
        soup = BeautifulSoup(iContactsResp.text, "html.parser")
        if(soup.find('table', {"summary": "Agent contact details"})):
            iContactsTableList = soup.find('table', {"summary": "Agent contact details"})
            iContactsTableRows = iContactsTableList.findAll('tr')
            for rows in iContactsTableRows:
                tableHead = rows.find('th')
                if (tableHead.text.strip() == 'Phone'):
                    AgentTelephone = rows.find('td').text.strip()
                if (tableHead.text.strip() == 'EMAIL'):
                    AgentEmail = rows.find('td').text.strip()

        format1 = "%Y/%m/%d %H:%M"
        Schedule_Date = datetime.now().strftime(format1)

        Source_With_Time = gcs + "_" + Schedule_Date + "-perl"

        Council_Name = "Cambridge"
        CouncilCode = councilcode
        appCount = 1

        joinValues = "(" + "'" + str(CouncilCode) + "','" + str(Council_Name) + "','" + str(AppAddress) + "','" + str(
            DateApplicationReceived) + "','" + str(Application) + "','" + str(DateApplicationRegistered) + "','" + str(
            DateApplicationvalidated) + "','" + str(ProposalCnt) + "','" + str(ApplicationStatus) + "','','" + str(
            AgentAddress) + "','','" + str(AgentName) + "','','" + str(ApplicantAddress) + "','" + str(
            ApplicantName) + "','" + str(ApplicationType) + "','','','','','','','','','','','" + str(
            TargetDecdt) + "','','" + str(AppURL) + "','" + str(AppURL) + "','','" + Source_With_Time + "','" + str(
            Schedule_Date) + "','" + str(NorthingData) + "','" + str(EastingData) + "')"

        # if appCount == 1:
        #     bulkValuesForQuery = insertQuery + joinValues
        #     appCount += 1
        # elif appCount == 5:
        #     bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
        #     cursor.execute(bulkValuesForQuery)
        #     conn.commit()
        #     appCount = 1
        #     bulkValuesForQuery = ''
        # else:
        #     bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
        #     appCount += 1
    browser.quit()
