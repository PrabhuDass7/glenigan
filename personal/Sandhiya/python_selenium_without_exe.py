from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import PySimpleGUI as sg, chromedriver_autoinstaller

options = Options()
options.add_argument("--headless")
options.add_argument('--log-level=3')
options.add_experimental_option('excludeSwitches', ['enable-logging'])    
driver = webdriver.Chrome(options=options, executable_path=chromedriver_autoinstaller.install(cwd = True))
driver.get(company_url)
detailpage=driver.page_source