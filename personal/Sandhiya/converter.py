from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter#process_pdf
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.converter import HTMLConverter
from pdfminer.layout import LAParams
import codecs
from cStringIO import StringIO
import re

def pdf_to_text(pdfname):
##def pdf_to_html(pdfname):
    # PDFMiner boilerplate
    rsrcmgr = PDFResourceManager()
    sio = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, sio, codec=codec, laparams=laparams)
##    device = HTMLConverter(rsrcmgr, sio, codec=codec, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    # Extract text
    fp = file(pdfname, 'rb')

    for page in PDFPage.get_pages(fp):
        interpreter.process_page(page)
    fp.close()

    # Get text from StringIO
    text = sio.getvalue()

    # Cleanup
    device.close()
    sio.close()

    return text

count = 0
var = open("Input.txt", "r")
var1 = var.readlines()
##print var1
for  v1 in var1:    
    v2=v1.replace("\n","")
    data=pdf_to_text(v2)
    text_file = open(v2 + ".txt", "w")
    text_file.write(data)
    text_file.close()
