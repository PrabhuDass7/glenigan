# import pymssql
import datetime
import pyodbc

def dbConnection(Core,logFilePath):
    serverName=Core.get('dbDetailes', 'server')
    userName=Core.get('dbDetailes', 'user')
    passWord=Core.get('dbDetailes', 'password')
    dataBase=Core.get('dbDetailes', 'database')
    dbh=''
    try:
        # dbh = pymssql.connect(server=serverName, user=userName, password=passWord , database=dataBase)
        dbh = pyodbc.connect('DRIVER={SQL Server};SERVER='+serverName+';DATABASE='+dataBase+';UID='+userName+';PWD='+ passWord)
    except Exception as e:
        print ("Connection Failed",str(e))
        with open(logFilePath+"/dbConnectionFailedLog.txt","w") as f:
            f.write(str(e)+"\t"+str(datetime.datetime.now())+"\n")
        pass
    return dbh

def retriveInput(dbh,selecQuery,logFilePath):
    records=''
    try:
        cursor = dbh.cursor()
        cursor.execute(selecQuery)
        records = cursor.fetchall()
    except Exception as e:
        print ("Fetch Failed",e)
        with open(logFilePath+"/fetchQueryFailedLog.txt","a") as f:
            f.write(str(selecQuery)+"\t"+str(e)+"\t"+str(datetime.datetime.now())+"\n")
        pass
    return records

def queryExecution(dbh,Query,logFilePath):
    try:
        cursor = dbh.cursor()
        cursor.execute(Query)
        dbh.commit()
        print ("*** Query Executed Success ***")
    except Exception as e:
        print ("Execute Failed:",e)
        with open(logFilePath+"/queryFailedLog.txt","a") as f:
            f.write(str(Query)+"\t"+str(e)+"\t"+str(datetime.datetime.now())+"\n")
        pass

def updateFormat(dbh, FormatIDList, status, logFilePath):
    oneAppFormatProjects = ",".join(FormatIDList)
    oneAppFormatQuery = "update FORMAT_PUBLIC_ACCESS set One_App = '"+status+"', Last_Visited_Date = getdate() where ID in ("+oneAppFormatProjects+")"
    queryExecution(dbh, oneAppFormatQuery, logFilePath)
