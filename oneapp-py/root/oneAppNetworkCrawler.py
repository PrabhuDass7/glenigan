import sys
import re
import os
import imp,math
import codecs
from datetime import datetime, timedelta
import xlrd

basePath = os.getcwd()
rootPath=os.path.basename(basePath)
basePath=basePath.replace(rootPath,'')
iniFilePath=basePath+'ctrl'
dataFilePath=basePath+'data'
libraryFilePath=basePath+'lib'
logFilePath=basePath+'logs'

oneAppDBTransactions = imp.load_source('oneAppDBTransactions', libraryFilePath+'\\oneAppDBTransactions.py')
oneAppUtility = imp.load_source('OneappUtility', libraryFilePath+'\\OneappUtility.py')
oneAppScraper = imp.load_source('oneAppScraper', libraryFilePath+'\\oneAppScraper.py')

Core=oneAppUtility.readINI(iniFilePath,"core.ini")
regexFile=oneAppUtility.readINI(iniFilePath,"regex.ini")
pdfLocation=Core.get('pdfLocation', 'location') # read pdf stored path
selectQuery=Core.get('selectInput', 'selectQuery') # read select query
Headers=Core.get('Headers', 'Headers') # read Headers template

dbh=oneAppDBTransactions.dbConnection(Core,logFilePath)
inputList=oneAppDBTransactions.retriveInput(dbh,selectQuery,logFilePath)

imageFormatList=[]
otherFormatList=[]
oneAppFormatList=[]

count=0
for i in range(len(inputList)):
    Format_ID   = str(inputList[i][0])
    Council_Code    = str(inputList[i][1])
    Document_Name   = str(inputList[i][2])
    Application_No  = str(inputList[i][3])
    Source      = str(inputList[i][4])
    Markup      = str(inputList[i][5])
    
    Format_ID=oneAppUtility.commonClean(Format_ID)
    Council_Code=oneAppUtility.commonClean(Council_Code)
    Document_Name=oneAppUtility.commonClean(Document_Name)
    Application_No=oneAppUtility.commonClean(Application_No)
    Source=oneAppUtility.commonClean(Source)
    Markup=oneAppUtility.commonClean(Markup)
    
    count+=1
    print ("count::",count,"=>","Format_ID:",Format_ID)
    
    pdfFileNameWithPath = pdfLocation+'\\'+Format_ID+'\\'+Document_Name
    print ("pdfFileNameWithPath:",pdfFileNameWithPath)
    textFileNameWithPath = pdfFileNameWithPath;
    textFileNameWithPath = textFileNameWithPath.replace('.pdf','.txt')

    pdfFilecontent=''
    try:
        pdfFilecontent = oneAppUtility.convertPDF(libraryFilePath,pdfFileNameWithPath,textFileNameWithPath)
        with open(textFileNameWithPath,'r') as FH:
            pdfFilecontent=FH.read()
    except Exception as e:  
        print ("Error:",e)

    convertedTxtLength = len(pdfFilecontent)
    
    insertQueryTmp=''
    insertQuery=''
    insertQuery=Core.get('insertQuery', 'insertQuery') # read insert query template
    
    File_size_kb = convertedTxtLength/1024
    # if convertedTxtLength != 0:
    if File_size_kb > 1:
        (pdfFormat, pdfFilecontent, pdfFormatTmp) = oneAppUtility.formatIdentify(pdfFilecontent)
        
        # with open("Reconstructed_File.txt","w") as f:
                # f.write(str(pdfFilecontent))
                
        if pdfFormat != 'Others':
            insertQueryTmp = oneAppScraper.scrapePDF(pdfFileNameWithPath,textFileNameWithPath,pdfFormat,pdfFilecontent,regexFile,oneAppUtility)
            
            format1 = "%Y-%m-%d %H:%M:%S"   
            create_date = datetime.now().strftime(format1)
            
            insertQuery = insertQuery+"("+"'"+str(Source)+"','"+str(Format_ID)+"','"+str(Application_No)+"','"+str(Council_Code)+"','"+str(Markup)+"','"+str(insertQueryTmp)+"','"+str(pdfFormatTmp)+"','"+str(create_date)+"');"

            if re.search('values\s*$',insertQuery) is None:
                oneAppDBTransactions.queryExecution(dbh, insertQuery, logFilePath)
                oneAppFormatList.append(Format_ID)
                print ("** Data Inserted ***")
                
        else:
            otherFormatList.append(Format_ID)
    else:
        imageFormatList.append(Format_ID)
    
    if count==995:
        if oneAppFormatList:
            oneAppDBTransactions.updateFormat(dbh, oneAppFormatList,"Y",logFilePath)

        if imageFormatList:
            oneAppDBTransactions.updateFormat(dbh, imageFormatList,"I",logFilePath)

        if otherFormatList:
            oneAppDBTransactions.updateFormat(dbh, otherFormatList,"O",logFilePath)
            
        imageFormatList=[]
        otherFormatList=[]
        oneAppFormatList=[]
        
        count=0

if oneAppFormatList:
    oneAppDBTransactions.updateFormat(dbh, oneAppFormatList,"Y",logFilePath)

if imageFormatList:
    oneAppDBTransactions.updateFormat(dbh, imageFormatList,"I",logFilePath)

if otherFormatList:
    oneAppDBTransactions.updateFormat(dbh, otherFormatList,"O",logFilePath)