use strict;
use Time::Piece;
# use File::stat;
use File::Path;
use Cwd qw(cwd);
use MIME::Lite;

my $scriptPath = 'C:/Glenigan/newDocumentDownload/logs';
open(FN,">>$scriptPath/Remove_Old_Folders_Log.txt");
print FN "Start : ".localtime()."\t";
close FN;

open(FN,">$scriptPath/OneApp_Deleted_Report.txt");
print FN "Folder_name\tDiff_Days\tModifiedTime\tDeleted_On\tFolder_size(MB)\n";
close FN;

open(FD,">$scriptPath/OneApp_Delete_Failed_Report.txt");
print FD "Folder_name\tDiff_Days\tModifiedTime\tDeleted_On\tFolder_size\tFile\tMessage\n";
close FD;					

my $path='//172.27.137.202/One_app_download/';

opendir(my $dh, $path) || die "can't opendir $path: $!";

my $i=1;
my $Err_Flag = 0;
my $Total_Removed_Size = 0;
while (my $folder_name=readdir $dh) 
{
	next if($folder_name=~m/^\s*(\.|\.\.|Thumbs\.db)\s*$/is);
	print "Folder: $folder_name\n";
	next if($folder_name!~m/^[\d]+$/is);
	my $Temp_pdf_store_path="$path$folder_name";
	open(FN,">>$scriptPath/OneApp_Deleted_Report.txt");
	if (-d $Temp_pdf_store_path)
	{
		my $Folder_size_tmp = `dir /s \"$Temp_pdf_store_path\"`;
		my $Folder_size= $1 if($Folder_size_tmp=~m/[\d]+\s*File\s*\(s\)\s*([^>]*?)\s*bytes/is);
		$Folder_size=~s/\,//igs;
		$Folder_size=($Folder_size/1024)/1024;
		# print "size: $Folder_size\n";
		my @stats = stat $Temp_pdf_store_path;
		my $modifiedTime = localtime $stats[9];
		print "modtime: $modifiedTime\n";
		my $now=localtime();
		# print "modtime: $now\n";

		my $format = '%a %b %d %H:%M:%S %Y';

		my $diff = Time::Piece->strptime($now, $format)
				 - Time::Piece->strptime($modifiedTime, $format);		
		my $Diff_Days=int($diff->days);				 
		print "Diff: $Diff_Days\n";

		if($Diff_Days>30)
		{
			# system("rmdir -rf $Temp_pdf_store_path");
			# my $s = rmtree $Temp_pdf_store_path;
			my $s = rmtree ($Temp_pdf_store_path,{ error =>  \my $err});			
			print "$s: Removed : $Temp_pdf_store_path\n";
			if (@$err) 
			{
				for my $Err_Msg (@$err) 
				{
					my ($file, $message) = %$Err_Msg;
					$Err_Flag = 1;
					open(FD,">>$scriptPath/OneApp_Delete_Failed_Report.txt");
					print FD "$folder_name\t$Diff_Days\t$modifiedTime\t$now\t$Folder_size\t$file\t$message\n";
					close FD;						
				}
			}
			else
			{			
				$Total_Removed_Size = $Total_Removed_Size + $Folder_size;
				print FN "$folder_name\t$Diff_Days\t$modifiedTime\t$now\t$Folder_size\n";
				$i++;
			}
		}	
	}	
	close FN;
}
closedir $dh;
my $Time = localtime();
my $subject="Glenigan - OneApp - Deleted Report - $Time";
my $body = "Hi Team, \n\n\t\tHere, We have attached the Removed older files report.\n\t\tTotal Removed Size: $Total_Removed_Size MB\n\t\tPlease find the same.\n\nRegards\nGlenigan Team IT";
my $to ='it.support@meritgroup.co.uk';
my $cc ='mervin.samuel@meritgroup.co.uk;parielavarasan.surulinathan@meritgroup.co.uk';
my $File_name = 'OneApp_Deleted_Report.txt';

&Send_Report($subject,$body,$to,$cc,$File_name);

if($Err_Flag == 1)
{
	my $subject="Glenigan - OneApp - Failure Report - $Time";
	my $body = "Hi Team, \n\n\t\tHere, We have attached the Failure report when removing old folders in OneApp.\nPlease find the same.\n\nRegards\nGlenigan Team IT";
    my $to ='parielavarasan.surulinathan@meritgroup.co.uk';
	my $File_name = 'OneApp_Delete_Failed_Report.txt';
	&Send_Report($subject,$body,$to,$cc,$File_name);
}

open(FN,">>$scriptPath/Remove_Old_Folders_Log.txt");
print FN "End : ".localtime()."\t$Total_Removed_Size\t$i\n";
close FN;


sub Send_Report()
{
    my $Subject = shift;
    my $Body = shift;
    my $to = shift;
    my $cc = shift;
	my $File_name = shift;

	my $Location = cwd;
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    # my $pass='11meritgroup11';
    my $pass='sXNdrc6JU';
    my $cc ='parielavarasan.surulinathan@meritgroup.co.uk';

	
	# my $file_path="C:\\One_App\\$storepath";
	my $file_path="$Location\\$File_name";
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Cc => $cc,
	  Subject => $subject,
	  Data => $body
	  # Type =>'multipart/mixed'
	) or die "Error creating multipart container: $!\n";
    ### Add parts (each "attach" has same arguments as "new"):
	$msg->attach(
        Type        =>'txt',
        Path        =>"$file_path",
        Filename    =>"$File_name",
        Disposition => 'attachment'
    );
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}