use strict;
use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename;
use Time::Piece;
use URI::Escape;
use HTML::Entities;

####
# Set root path for accessing files
####
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n"; 

####
# Declare and load local directories and required private module
####
my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $localDownloadDirectory = ($dataDirectory.'/Documents');

# Private Module
require ($libDirectory.'/webSearchDownloadUtility.pm'); 
require ($libDirectory.'/docDatabase.pm'); 
# require ($libDirectory.'/onlinegetUtility500.pm'); 

my $councilCode=$ARGV[0];
my $inputFormatID=$ARGV[1];

if($councilCode!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

if($councilCode=~m/^(11)$/is)
{
	require ($libDirectory.'/onlinegetUtility500.pm'); 
}

####
# Get mechanize userAgent global variable
####
my $mechAgent = &webSearchDownloadUtility::mechUserAgent($councilCode);
my $lwpAgent = &webSearchDownloadUtility::mechUserAgent($councilCode);
my $MaxCount=0;

####
# Create new object of class Config::Tiny
####
my ($councilDetailsFile,$docDownloadConfig) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile"
####
$councilDetailsFile = Config::Tiny->read($iniDirectory.'/webSearchFormat.ini');
$docDownloadConfig = Config::Tiny->read($iniDirectory.'/docDownloadConfig.ini');


####
# Get values in INI files
####
my ($method,$format,$homeURL,$Search_Post_URL,$Filter_URL,$Doc_URL_Regex,$Accept_Cookie,$Accept_Language,$Accept_Cookie_URL,$Accept_Post_Content,$Need_Host,$Accept_Host,$Host,$Content_Type,$Referer,$Post_Content,$Landingpage_Regex,$Table_Block_Regex,$Tr_Block_Regex,$Td_Block_Regex,$Doc_Publisheddate_Index,$Doc_Type_Index,$Doc_Description_Index,$Doc_DownloadURL_Index,$Doc_DownloadURL_Regex,$Doc_Filter_URL,$Doc_URL_Method,$Pagination_Type,$TotalNoOfPages_Regex,$Nextpage_Regex,$Nextpage_Regex_Replace,$Proxy,$Eventvalidation_Regex,$CSRFToken_Regex,$Viewstategenerator_Regex,$Viewstate_Regex,$Application_URL_Regex,$Token,$AFHToken,$Html_Parser,$Next_Page_Replace_Key,$Current_Page_Number_Regex,$Next_Referer,$Next_Page_Post_Content) = &webSearchDownloadUtility::readINI($councilDetailsFile,$councilCode);

####
# Establish DB Connection
####
my $dbh = &docDatabase::DbConnection();

####
# Get input from format table
####
my %inputHashCode;
my $inputHash = &docDatabase::retrieveInput($dbh,$councilCode,$inputFormatID);
%inputHashCode=%$inputHash;

my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status,$status);
$No_Of_Doc_Downloaded_onDemand = 0;


my $Insert_Document = $docDownloadConfig->{'Queries'}->{'Insert_Format_Document'};
my $MD5_Insert_Query = $docDownloadConfig->{'Queries'}->{'Insert_TBL_PDF_MD5'};
my $Insert_OneAppLog = $docDownloadConfig->{'Queries'}->{'Insert_OneAppLog'};
my $Dashboard_Insert_Query = $docDownloadConfig->{'Queries'}->{'Insert_TBL_ONEAPP_STATUS'};

my $pdfcount_inc=1;
my $todayDocCountInSite=0;
my $totalDownloadedCountToday=0;
my $todayCount = 0;

my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	



foreach my $hashValue(sort keys %inputHashCode)
{	
	my $formatID 		= $inputHashCode{$hashValue}[0];
	my $Source 			= $inputHashCode{$hashValue}[1];
	my $applicationNo 	= $inputHashCode{$hashValue}[2];
	my $documentURL 	= $inputHashCode{$hashValue}[3];
	my $Markup 			= $inputHashCode{$hashValue}[4];
	my $applicationURL 	= $inputHashCode{$hashValue}[5];
	my $noOfDocumentsfromDB	= $inputHashCode{$hashValue}[6];
	my $projectID 		= $inputHashCode{$hashValue}[7];
	my $projectStatus	= $inputHashCode{$hashValue}[8];
	my $councilName 	= $inputHashCode{$hashValue}[9];
	
	
	$noOfDocumentsfromDB=0 if($noOfDocumentsfromDB eq "");
	
	print "\n##############\n";
	print "formatID==>$formatID\n";
	print "Source==>$Source\n";
	print "applicationNo==>$applicationNo\n";
	print "documentURL==>$documentURL\n";
	print "Markup==>$Markup\n";
	print "applicationURL==>$applicationURL\n";
	print "noOfDocumentsfromDB==>$noOfDocumentsfromDB\n";
	print "projectID==>$projectID\n";
	print "projectStatus==>$projectStatus\n";
	print "councilName==>$councilName\n";
	print "##############\n";
	
	
	
	if($councilCode=~m/^149$/is)
	{
		next if($applicationNo=~m/^Chelmsford/is);
	}
	if($councilCode eq '135')
	{
		$documentURL=~s/activeTab=documents/activeTab=externalDocuments/is;
		print "documentURL_171==>$documentURL\n";
	}
	# $documentURL='';
			
	&docDatabase::insertOneAppStatus($dbh,$councilCode,$councilName,$Downloaded_date,'Running');
	
	my ($docDetailsHashRef, $Code, $pdfURLCountInSite);
	my %docDetailsFinal;
	
	($docDetailsHashRef, $documentURL, $Code) = &extractLink($method, $homeURL, $documentURL, $applicationURL, $applicationNo,\%Nextpage_Replace_Hash);
	
	if(!$docDetailsHashRef)
	{
		print "No Docs Found\n";
		next;
	}
	else
	{
		%docDetailsFinal=%$docDetailsHashRef;
		$pdfURLCountInSite=keys %docDetailsFinal;		
	}
	
	print "pdfURLCountInSite==>$pdfURLCountInSite\n";
	$todayDocCountInSite = $todayDocCountInSite + $pdfURLCountInSite;
		
	my $RetrieveMD5=[];
	my $noOfPDFDownloadedCount=0;
	# if(lc($Markup) eq 'large')
	# {
		if($pdfURLCountInSite > $noOfDocumentsfromDB)
		{
			$RetrieveMD5 =&docDatabase::RetrieveMD5($dbh, $councilCode, $formatID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	
	
	my $tempDownloadLocation = $localDownloadDirectory.'/'.$formatID;
	print "tempDownloadLocation=>$tempDownloadLocation\n";
	unless ( -d $tempDownloadLocation )
	{
		$tempDownloadLocation=~s/\//\\/igs;
		system("mkdir $tempDownloadLocation");
	}

	print "Folder Created[[MKDIR]]====> $tempDownloadLocation\n";
	
	my ($oneAppAvailStatus,$oneAppName,$Comments);
	
	foreach my $pdfURL(keys %docDetailsFinal)
	{
		my ($docType,$pdfName,$docPublishedDate);

		print "pdfURL: $pdfURL\n";
		$docType = $docDetailsFinal{$pdfURL}->[0];
		$pdfName = $docDetailsFinal{$pdfURL}->[1];
		$docPublishedDate=$docDetailsFinal{$pdfURL}->[2];
		
		$pdfName=~s/WITHOUT_PRESONAL_DATA/Application_Form_without_personal_data/igs;
		$pdfName=~s/WITHOUT_PERSONAL_DATA/Application_Form_without_personal_data/igs;
		
		if(($docType=~m/Application(?:\s+|_|-)?Form/is) && ($councilCode =~m/(?:54|312)/is))
		{
			$pdfName=~s/Application/Application_Form/igs;
		}
		
		
		my ($downloadStatus);
		if(lc($Markup) eq 'large')
		{
			if($pdfURL!~m/^\s*$/is)
			{				
				my ($downloadResCode, $downloadErrMsg, $MD5)=&webSearchDownloadUtility::Download($pdfURL,$pdfName,$documentURL,$tempDownloadLocation,$RetrieveMD5,$Markup,$lwpAgent);
				# my ($downloadResCode, $downloadErrMsg, $MD5)=&webSearchDownloadUtility::Download($pdfURL,$pdfName,$documentURL,$tempDownloadLocation,$RetrieveMD5,$Markup,$lwpAgent,$councilCode);
				
				($downloadStatus)=&webSearchDownloadUtility::UrlStatus($downloadResCode);
				
				$noOfPDFDownloadedCount++ if($downloadResCode=~m/200/is);
					
				$Comments=$Comments.'|'.$downloadErrMsg if($downloadErrMsg!~m/^\s*$/is);
				
				if(($pdfName=~m/$docDownloadConfig->{'formKeywordsMatch'}->{'keywords'}/is) && ($pdfName !~m/$docDownloadConfig->{'formNegativeKeywordsMatch'}->{'keywords'}/is))
				{
					$oneAppAvailStatus = 'Y';
					$oneAppName = $pdfName;
				}	
				
				$pdfName=~s/\'/\'\'/igs;
				$oneAppName=~s/\'/\'\'/igs;
				$docType=~s/\'/\'\'/igs;
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$formatID\', \'$councilCode\', \'$MD5\', \'$pdfName\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
				}	
				my $Insert_Document_List = "(\'$formatID\', \'$pdfName\', \'$docType\', \'$pdfName\', \'$Downloaded_date\', \'$docPublishedDate\', \'$downloadStatus\'),";	
				$Insert_Document.=$Insert_Document_List;
			}
		}	
		else
		{
			if(($pdfName=~m/$docDownloadConfig->{'formKeywordsMatch'}->{'keywords'}/is) && ($pdfName !~m/$docDownloadConfig->{'formNegativeKeywordsMatch'}->{'keywords'}/is))
			{
				$oneAppAvailStatus = 'Y';
				$oneAppName = $pdfName;
				
				if($pdfURL!~m/^\s*$/is)
				{
					my ($downloadResCode, $downloadErrMsg, $MD5)=&webSearchDownloadUtility::Download($pdfURL,$pdfName,$documentURL,$tempDownloadLocation,$RetrieveMD5,$Markup,$lwpAgent);
					# my ($downloadResCode, $downloadErrMsg, $MD5)=&webSearchDownloadUtility::Download($pdfURL,$pdfName,$documentURL,$tempDownloadLocation,$RetrieveMD5,$Markup,$lwpAgent,$councilCode);
					
					($downloadStatus)=&webSearchDownloadUtility::UrlStatus($downloadResCode);
					
					$noOfPDFDownloadedCount++ if($downloadResCode=~m/200/is);	
					
					$Comments=$Comments.'|'.$downloadErrMsg if($downloadErrMsg!~m/^\s*$/is);
					
					$pdfName=~s/\'/\'\'/igs;
					$oneAppName=~s/\'/\'\'/igs;
					$docType=~s/\'/\'\'/igs;
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$formatID\', \'$councilCode\', \'$MD5\', \'$pdfName\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;						
					}
					my $Insert_Document_List = "(\'$formatID\', \'$pdfName\', \'$docType\', \'$pdfName\', \'$Downloaded_date\', \'$docPublishedDate\', \'$downloadStatus\'),";	
					$Insert_Document.=$Insert_Document_List;
					
						
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$oneAppName\', No_of_Documents = \'$pdfURLCountInSite\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
					$Update_Query.= $Flag_Update_query if($downloadResCode=~m/200/is);	
				}
			}
		}

		#--------- DB INSERTION above 1000 record ---------------
		$MaxCount++;
		if($MaxCount == 995)
		{
			($MaxCount, $Insert_Document, $MD5_Insert_Query, $Update_Query) = QueryExecuteMax($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
		}
	}

	&webSearchDownloadUtility::Move_Files($tempDownloadLocation, $formatID);
	
	DB_Flag:
	# print "noOfPDFDownloadedCount****:: $noOfPDFDownloadedCount\n";<>;
	
	
	if($pdfURLCountInSite > $noOfDocumentsfromDB) 
	{
		$noOfPDFDownloadedCount = $noOfPDFDownloadedCount + $noOfDocumentsfromDB;
		# $totalDownloadedCountToday = $todayCount;
	}	
	# else
	# {
		# $totalDownloadedCountToday = $noOfDocumentsfromDB;
	# }	
	
	
	$totalDownloadedCountToday = $totalDownloadedCountToday + $noOfPDFDownloadedCount;
	
	print "#### Download Report Start ####\n";
	print "todayCountInSite: $todayDocCountInSite\n";
	print "pdfURLCountInSite: $pdfURLCountInSite\n";
	print "noOfDocumentsfromDB: $noOfDocumentsfromDB\n";
	
	print "Todays_Downloaded_Count: $totalDownloadedCountToday\n";
	
	# if($pdfURLCountInSite == $totalDownloadedCountToday)
	# {
		# print "All documents downloaded correctly!!\n";	
		# $status	= "Completed";
	# }
	# else
	# {
		# print "Something missing need to check\n";
		# $status	= "Completed. But have Issue!" if(lc($Markup) eq 'large');
		# $status	= "Completed" if(lc($Markup) ne 'large');
	# }
	
	$status="Completed";
	
	print "#### Download Report End ####\n";
		
	$documentURL=~s/\'/\'\'/igs;
	
	my $insert_query = "(\'$formatID\', \'$councilCode\', \'$Markup\', \'$documentURL\', \'$pdfURLCountInSite\', \'$oneAppAvailStatus\', \'$Downloaded_date\', \'$Comments\', \'$totalDownloadedCountToday\'),";	
	$Insert_OneAppLog.= $insert_query;

	if($oneAppName=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfURLCountInSite\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfURLCountInSite\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$oneAppName\', No_of_Documents = \'$pdfURLCountInSite\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
			$Update_Query.= $Flag_Update_query;
		}
	}
}

&docDatabase::updateOneAppStatus($dbh,$councilCode,$Downloaded_date,$todayDocCountInSite,$totalDownloadedCountToday, $status);

$Insert_OneAppLog=~s/\,$//igs;
# print "\nInsert_OneAppLog:: $Insert_OneAppLog\n";
&docDatabase::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\nInsert_Document:: $Insert_Document\n";
&docDatabase::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\nUpdate_Query:: $Update_Query\n";
&docDatabase::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\nMD5_Insert_Query:: $MD5_Insert_Query\n";
&docDatabase::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

sub extractLink()
{
	my $method = shift;
	my $homeURL = shift;
	my $documentURL = shift;
	my $applicationURL = shift;
	my $applicationNo = shift;
	my $Nextpage_Replace_Hash = shift;
	
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %docDetailsHash;
	
	my ($Content,$responseCode,$RedirectURL);
	
	my $landingPageFlag = 0;
	
	Re_Ping:
	if($method=~m/GET/is)
	{
		if($documentURL eq "")
		{
			if($applicationURL ne "")
			{
				($Content,$responseCode,$RedirectURL) =&webSearchDownloadUtility::getMethodLWP($applicationURL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
			}
			elsif($applicationNo ne "")
			{
				($Content,$responseCode)=&webSearchDownloadUtility::SearchViaApplicationNo($applicationNo,$homeURL,$Search_Post_URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language,$Viewstate_Regex,$Viewstategenerator_Regex,$CSRFToken_Regex,$Eventvalidation_Regex,$Post_Content,$Landingpage_Regex,$Filter_URL,$Application_URL_Regex,$Token,$AFHToken);
			}
		}
		else
		{	
			# if($councilCode=~m/^372$/is)
			# {
				# print "councilCode::$councilCode\n";
				# ($Content,$responseCode,$RedirectURL) =&onlineDownloadUtility::getMethodLWP($documentURL,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
				# ($Content,$responseCode) =&onlinegetUtility500::getMethodLWP($documentURL);
				
				# print "Content::$Content\n";
				# open F1, ">32content.html";
				# print F1 $Content;
				# close F1;
				# print "Content::$Content\n";
				# exit;
			# }
			if($councilCode=~m/^12$/is)
			{
				$documentURL="http://planningdocs.redbridge.gov.uk/AniteIM.WebSearch/ExternalEntryPoint.aspx?SEARCH_TYPE=1&DOC_CLASS_CODE=PL&folder1_ref=$applicationNo";
			}
			elsif($councilCode=~m/^2006$/is){
				$documentURL="https://webapps.dublincity.ie/AniteIM.WebSearch/ExternalEntryPoint.aspx?SEARCH_TYPE=1&DOC_CLASS_CODE=PL&folder1_ref=$applicationNo";
			}
			# else
			# {
				($Content,$responseCode,$RedirectURL) =&webSearchDownloadUtility::getMethodLWP($documentURL,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
				print "responseCode::$responseCode\n";			
			# }
		}
		
	}
	elsif($method eq 'Search')
	{
		($Content,$responseCode)=&webSearchDownloadUtility::SearchViaApplicationNo($applicationNo,$homeURL,$Search_Post_URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language,$Viewstate_Regex,$Viewstategenerator_Regex,$CSRFToken_Regex,$Eventvalidation_Regex,$Post_Content,$Landingpage_Regex,$Filter_URL,$Application_URL_Regex,$Token,$AFHToken);
	}	
			
	if($councilCode == 486)
	{
		my $doc_frameurl=$1	if($Content=~m/id=\"fraPlanningDocs\"[^>]*?src=\"([^>]*?)\"/is);
		# print "doc_frameurl::$doc_frameurl\n\n";
		($Content,$responseCode) =&webSearchDownloadUtility::getMethodLWP($doc_frameurl,$applicationURL,'','',$documentURL,$Proxy,$Need_Host,$lwpAgent);
		# open(DC,">486-frameContent.html");
		# print DC "$Content";
		# close DC;
		# exit;			
	}
	if($Content=~m/$Doc_URL_Regex/is)
	{
		my $relatedDocURL= $1;
		my $DocUrl;
		if($relatedDocURL!~m/http/is)
		{
			$DocUrl=&webSearchDownloadUtility::formAbsoluteURL($relatedDocURL,$applicationURL);
		}
		$DocUrl = &webSearchDownloadUtility::urlClean($DocUrl);
		
		print "formAbsoluteURL==>$DocUrl\n";
		
		($Content,$responseCode,$RedirectURL) =&webSearchDownloadUtility::getMethodLWP($DocUrl,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);		
	}

	if($landingPageFlag == 0)
	{
		if(($Landingpage_Regex ne 'N/A') and ($Landingpage_Regex ne ''))
		{
			if($Content=~m/$Landingpage_Regex/is)
			{
				my $DocUrl=&webSearchDownloadUtility::urlTrim($1);
				
				$documentURL=&webSearchDownloadUtility::formAbsoluteURL($DocUrl,$documentURL);
			
				print "documentURL: $documentURL\n";
				
				$landingPageFlag = 1;
				goto Re_Ping;
			}
		}	
	}
	# if($councilCode == 372)
	# {
		# ($Content,$responseCode) =&onlinegetUtility500::getMethodLWP($documentURL);
		# # open(DC,">3723Content.html");
		# # print DC "$Content";
		# # close DC;
		# # exit;			
	# }
	
	my $page_inc=1;
	my $Previous_page=0;
	Next_Page:
	if($format eq 'Table')
	{
		my $appDocDetails=&collectDocsDetails($Content,$homeURL,$RedirectURL,$documentURL,$applicationURL,$Doc_URL_Method,$lwpAgent);
		my %appDocDetails=%$appDocDetails;
		%docDetailsHash = (%docDetailsHash,%appDocDetails);
	}
	
	# open(DC,">3723Content.html");
	# print DC "$Content";
	# close DC;
	# exit;	
	my ($totalPages);
	if($TotalNoOfPages_Regex ne 'N/A')
	{
		if($Content=~m/$TotalNoOfPages_Regex/is)
		{
			$totalPages=$1;
		}
	}
	
	if($Pagination_Type eq 'N/A')
	{
		return (\%docDetailsHash,$documentURL,$responseCode);
	}
	elsif($Pagination_Type eq 'By Number')
	{
		print "page_inc	: $page_inc\n";
		print "Total_no_Pages	: $totalPages\n";
		
		if($page_inc < $totalPages)
		{
			my ($Next_page_link,$Next_Page_Values);
			my @Next_Page_Values = $Content=~m/$Nextpage_Regex/g;
			if($Nextpage_Regex_Replace ne 'N/A')
			{
				my @Replace_array_Raw=split('\|',$Nextpage_Regex_Replace);
				
				$Next_page_link = $RedirectURL if($Replace_array_Raw[0]=~m/Redir_Url/is);
				
				if($Replace_array_Raw[2]=~m/AllValues/is)
				{
					$Next_Page_Values = join("",@Next_Page_Values);
				}	

				if($Replace_array_Raw[1]=~m/Replace/is)
				{
					for my $Replace_Key(keys %Nextpage_Replace_Hash)
					{
						if($Nextpage_Replace_Hash{$Replace_Key} eq '')
						{
							$Next_page_link=~s/$Replace_Key/$Next_Page_Values/igs;
						}	
						else
						{
							$Next_page_link=~s/$Replace_Key/$Nextpage_Replace_Hash{$Replace_Key}/igs;
						}
					}
				}
				print "Next_page_link: $Next_page_link\n";
			
			}
			$page_inc++;
			if($councilCode==2006){
				$Next_page_link = "https://webapps.dublincity.ie/AniteIM.WebSearch/Results.aspx?grdResultsP=$page_inc";
			}
			($Content,my $code,my $reurl)=&webSearchDownloadUtility::getMethodLWP($Next_page_link,"","","","",$Proxy,$Need_Host,$lwpAgent);
			# ($Content,my $responseCode,my $RedirectURL)=&webSearchDownloadUtility::getMethodLWP($Next_page_link,"","","","",$Proxy,$Need_Host,$lwpAgent);
			
			goto Next_Page;
		}	
	}
	elsif($Pagination_Type eq 'By Format')
	{
		if($Content=~m/$Current_Page_Number_Regex/is)
		{
			my $Current_Page=$1;
			my ($Doc_viewstate,$Doc_viewstategenerator,$CSRFToken,$Doc_Eventvalidation);
			print "Current_Page: $Current_Page\n";
			if($Viewstate_Regex ne 'N/A')
			{
				$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
			}	
			# if($Viewstategenerator_Regex ne 'N/A')
			# {
				# $Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
			# }	
			if($CSRFToken_Regex ne 'N/A')
			{
				$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
			}	
			# if($Eventvalidation_Regex ne 'N/A')
			# {
				# $Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			# }	
			if($Previous_page ne $Current_Page)
			{
				$Previous_page=$Current_Page;
				

				my $Next_Page_Post_Content_Tmp = $Next_Page_Post_Content;
				$Next_Page_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
				$Next_Page_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
				$Next_Page_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
				$Next_Page_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
				
								
				($Content,my $code,my $reurl)=&webSearchDownloadUtility::postMethodLWP($Next_Referer,$Host,$Content_Type,'',$Next_Page_Post_Content_Tmp,$Proxy,$Need_Host,$lwpAgent);
				
				goto Next_Page;
			}
		}
	}
	
	return (\%docDetailsHash,$documentURL,$responseCode);
	
}


sub collectDocsDetails()
{
	my $Content=shift;
	my $homeURL=shift;
	my $RedirectURL=shift;
	my $documentURL=shift;
	my $applicationURL=shift;
	my $Doc_URL_Method=shift;
	my $lwpAgent=shift;
	
	my %docDetails;
	
	print "Collect_Docs_Details\n";
	
	if($councilCode==181)
	{
		$Content=~s/<table\s*id=\"grdResults_tblSortButton\"[\w\W]*?<\/table>//igs;
	}
	
	if(($Html_Parser ne 'N/A') && ($Html_Parser eq 'Y'))
	{
		
		my ($appDocDetails)=&webSearchDownloadUtility::Doc_Download_Html_Header($Content,$homeURL,$RedirectURL,$documentURL,$applicationURL,$Doc_URL_Method,$councilCode,$lwpAgent);
		my %appDocDetails=%$appDocDetails;
		%docDetails = (%docDetails,%appDocDetails);
		
	}
	else
	{
		print "ëlse\n";
		$Content=~s/<td\/>/<td><\/td>/igs;
		# print "content:::$Content\n";
		# open F1, ">181contetnt.html";
		# print F1 $Content;
		# close F1;
		
		if($Table_Block_Regex ne 'N/A')
		{
			my @Block1 = $Content =~m/$Table_Block_Regex/igs;
			# print "Block1:::@Block1\n";
			if($Tr_Block_Regex ne 'N/A')
			{
				for my $Block1(@Block1)
				{	
					my @Block2 = $Block1=~m/$Tr_Block_Regex/igs;
					# print "Block2:::@Block2\n";
					if($Td_Block_Regex ne 'N/A')
					{
						for my $Block2(@Block2)
						{	
							my @Block3 = $Block2=~m/$Td_Block_Regex/igs;
							# print "Block3:::@Block3\n";
							my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$RedirectURL,$documentURL,$applicationURL,$Content);
							my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
							%docDetails = (%docDetails,%Doc_Details_Hash_Ref_tmp);					
						}	
					}
					else
					{
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$RedirectURL,$documentURL,$applicationURL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%docDetails = (%docDetails,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
			}	
			else
			{
				my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$RedirectURL,$documentURL,$applicationURL,$Content);
				my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				%docDetails = (%docDetails,%Doc_Details_Hash_Ref_tmp);					
			}	
		}
	}
	
	return \%docDetails;
	
}

sub Field_Mapping()
{
	my $Data=shift;
	my $RedirectURL=shift;
	my $documentURL=shift;
	my $applicationURL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name);
	my $baseURL;
	my %Doc_Details;
	
	if($RedirectURL ne '')
	{
		$baseURL=$RedirectURL;
	}
	elsif($documentURL ne '')
	{
		$baseURL=$documentURL;
	}
	elsif($applicationURL ne '')
	{
		$baseURL=$applicationURL;
	}
	
	
	$PDF_URL_Cont = &webSearchDownloadUtility::Trim($Data[$Doc_DownloadURL_Index]) if($Doc_DownloadURL_Index ne 'N/A');
	$document_type = &webSearchDownloadUtility::Trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &webSearchDownloadUtility::Trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &webSearchDownloadUtility::Trim($Data[$Doc_Publisheddate_Index]) if($Doc_Publisheddate_Index ne 'N/A');
	
	print "PDF_URL_Cont==>$PDF_URL_Cont\n";
	print "document_type==>$document_type\n";
	print "Doc_Desc==>$Doc_Desc\n";
	print "Published_Date==>$Published_Date\n";
	
	if($Doc_DownloadURL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_DownloadURL_Regex/is)
		{
			my $Durl=$1;
			# print "Durl: $Durl\n";
			
			if($Durl!~m/http/is)
			{
				my $u1=URI::URL->new($Durl,$baseURL);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
		
				# print "Doc_URL2: $Doc_URL\n";
			}	
			else
			{
				$Doc_URL=$Durl;
				# print "Doc_URL2: $Doc_URL\n";
			}	
			
			if($councilCode=~m/^338$/is)
			{
				$Doc_URL="http://planningregisterdocs.ryedale.gov.uk/Planning/StreamDocPage/obj.pdf?DocNo=$Durl&PDF=true&content=obj.pdf";
			}

			if($councilCode=~m/^334$/is)
			{
				$Doc_URL="https://docs.runnymede.gov.uk/PublicAccess_Live/Document/ViewDocument?id=$Durl";
			}

			if($councilCode=~m/^224$/is)
			{
				$Doc_URL="https://edrms2.hartlepool.gov.uk/PublicAccess_Live/Document/ViewDocument?id=$Durl";
			}

			if($councilCode=~m/^268$/is)
			{
				$Doc_URL="https://enterprise.merthyr.gov.uk/PublicAccess_LIVE/Document/ViewDocument?id=$Durl";
			}
			# print "Doc_Download_URL_ABS: $Doc_Download_URL_ABS\n";
			# if($Doc_Download_URL_ABS ne 'N/A') 
			# {
				# $Doc_URL=$Doc_Download_URL_ABS.$Durl;
				# print "Doc_URL1: $Doc_URL\n";
			# }
			# else
			# {
				# if($Durl!~m/http/is)
				# {
					# if($Council_Code=~m/^23$/is)
					# {
						# $Doc_URL="http://camdocs.camden.gov.uk".$Durl;
					# }
					# else
					# {
						# my $u1=URI::URL->new($Durl,$baseURL);
						# my $u2=$u1->abs;
						# $Doc_URL=$u2;
					# }
					# print "Doc_URL2: $Doc_URL\n";
				# }	
				# else
				# {
					# $Doc_URL=$Durl;
					# print "Doc_URL2: $Doc_URL\n";
				# }	
			# }	
		}
		
	}
	if($councilCode=~m/^274$/is)
	{
		# $Doc_URL="https://edrms.milton-keynes.gov.uk/NorthgateIM.WebSearch/(S(j00kebjrp031ht45h3hnht55))/Download.aspx?Download=00".$PDF_URL_Cont.".pdf";
		$Doc_URL="https://edrms.milton-keynes.gov.uk/NorthgateIM.WebSearch/Download.aspx?Download=00".$PDF_URL_Cont.".pdf";
		# $Doc_URL="https://edrms.milton-keynes.gov.uk/NorthgatePublicDocs/00".$PDF_URL_Cont.".pdf";
		# 'https://edrms.milton-keynes.gov.uk/NorthgatePublicDocs/00469841.pdf';
	}
	
	$Doc_Desc = &webSearchDownloadUtility::Clean($Doc_Desc);
	$document_type = &webSearchDownloadUtility::Clean($document_type);
	$Published_Date = &webSearchDownloadUtility::Clean($Published_Date);
	
	my ($file_type);
	if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	
	if($councilCode=~m/^486$/is)
	{
		$Doc_URL=~s/http:\/\/planning.cheshireeast.gov.uk/https:\/\/doc.cheshireeast.gov.uk\/aniteim.websearch\/\(S\(j00kebjrp031ht45h3hnht55\)\)/igs;
	}
	
	$pdf_name = &webSearchDownloadUtility::PDF_Name($pdf_name);
	$Doc_URL=decode_entities($Doc_URL);
	$pdfcount_inc++;
	print "Doc_URL	: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date	: $Published_Date\n";
	

	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	
	return (\%Doc_Details);
}	

sub QueryExecuteMax()
{	
	my ($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query)=@_;
	
	if($Insert_Document!~m/values\s*$/is)
	{
		$Insert_Document=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

		$Update_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

		$MD5_Insert_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);
		
		$Insert_Document="";
		$Update_Query="";
		$MD5_Insert_Query="";
		
		$Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
		$MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

		$MaxCount=0;
	}
	return ($MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
}