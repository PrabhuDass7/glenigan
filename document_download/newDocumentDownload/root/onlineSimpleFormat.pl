use strict;
use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename;
use Time::Piece;

####
# Set root path for accessing files
####
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####
my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $localDownloadDirectory = ($dataDirectory.'/Documents');

# Private Module
require ($libDirectory.'/onlineDownloadUtility.pm');
require ($libDirectory.'/docDatabase.pm'); 
# require ($libDirectory.'/onlinegetUtility500.pm'); 

my $councilCode=$ARGV[0];
my $inputFormatID=$ARGV[1];

if($councilCode!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

if($councilCode=~m/^(?:13|59|70|153|171|191|248|422|488|117)$/is)
{
	require ($libDirectory.'/onlinegetUtility500.pm'); 
}

####
# Get mechanize userAgent global variable
####

my $mechAgent = &onlineDownloadUtility::mechUserAgent($councilCode);
my $lwpAgent = &onlineDownloadUtility::mechUserAgent($councilCode);
my $lwpAgent1 = &onlineDownloadUtility::lwpUserAgent($councilCode);
my $MaxCount=0;

####
# Create new object of class Config::Tiny
####
my ($councilDetailsFile,$docDownloadConfig) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile"
####
$councilDetailsFile = Config::Tiny->read($iniDirectory.'/onlineSimpleFormat.ini');
$docDownloadConfig = Config::Tiny->read($iniDirectory.'/docDownloadConfig.ini');


####
# Get values in INI files
####
my ($method,$format,$homeURL,$Search_Post_URL,$Filter_URL,$Doc_URL_Regex,$Accept_Cookie,$Accept_Language,$Accept_Cookie_URL,$Accept_Post_Content,$Need_Host,$Accept_Host,$Host,$Content_Type,$Referer,$Post_Content,$Landingpage_Regex,$Table_Block_Regex,$Tr_Block_Regex,$Td_Block_Regex,$Doc_Publisheddate_Index,$Doc_Type_Index,$Doc_Description_Index,$Doc_DownloadURL_Index,$Doc_DownloadURL_Regex,$Doc_Filter_URL,$Doc_URL_Method,$Pagination_Type,$TotalNoOfPages_Regex,$Nextpage_Regex,$Nextpage_Regex_Replace,$Proxy,$Eventvalidation_Regex,$CSRFToken_Regex,$Viewstategenerator_Regex,$Viewstate_Regex,$Application_URL_Regex,$Token,$AFHToken,$Html_Parser) = &onlineDownloadUtility::readINI($councilDetailsFile,$councilCode);

####
# Establish DB Connection
####
my $dbh = &docDatabase::DbConnection();

####
# Get input from format table
####
my %inputHashCode;
my $inputHash = &docDatabase::retrieveInput($dbh,$councilCode,$inputFormatID);
%inputHashCode=%$inputHash;

my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status,$status);
$No_Of_Doc_Downloaded_onDemand = 0;
my $todayCount = 0;


my $Insert_Document = $docDownloadConfig->{'Queries'}->{'Insert_Format_Document'};
my $MD5_Insert_Query = $docDownloadConfig->{'Queries'}->{'Insert_TBL_PDF_MD5'};
my $Insert_OneAppLog = $docDownloadConfig->{'Queries'}->{'Insert_OneAppLog'};
my $Dashboard_Insert_Query = $docDownloadConfig->{'Queries'}->{'Insert_TBL_ONEAPP_STATUS'};

my $todayDocCountInSite=0;
my $totalDownloadedCountToday=0;
foreach my $hashValue(sort keys %inputHashCode)
{	
	my $formatID 		= $inputHashCode{$hashValue}[0];
	my $Source 			= $inputHashCode{$hashValue}[1];
	my $applicationNo 	= $inputHashCode{$hashValue}[2];
	my $documentURL 	= $inputHashCode{$hashValue}[3];
	my $Markup 			= $inputHashCode{$hashValue}[4];
	my $applicationURL 	= $inputHashCode{$hashValue}[5];
	my $noOfDocumentsfromDB	= $inputHashCode{$hashValue}[6];
	my $projectID 		= $inputHashCode{$hashValue}[7];
	my $projectStatus	= $inputHashCode{$hashValue}[8];
	my $councilName 	= $inputHashCode{$hashValue}[9];
	
	$noOfDocumentsfromDB=0 if($noOfDocumentsfromDB eq "");
	
	print "\n##############\n";
	print "formatID==>$formatID\n";
	print "Source==>$Source\n";
	print "applicationNo==>$applicationNo\n";
	print "documentURL==>$documentURL\n";
	print "Markup==>$Markup\n";
	print "applicationURL==>$applicationURL\n";
	print "noOfDocumentsfromDB==>$noOfDocumentsfromDB\n";
	print "projectID==>$projectID\n";
	print "projectStatus==>$projectStatus\n";
	print "councilName==>$councilName\n";
	print "##############\n";
	
	
	# $documentURL='';
	# if($councilCode eq '171')
	if($councilCode=~m/^15$|^171$/is)
	{
		$documentURL=~s/activeTab=externalDocuments/activeTab=documents/is;
		print "documentURL_171==>$documentURL\n";
	}
	elsif($councilCode=~m/^217$|^400$|^422$/is)
	{
		$documentURL=~s/^http\:/https\:/i;
		print "documentURL_217==>$documentURL\n";
	}
	# next if($formatID!~m/^(8974911|8974906|8975244|8975320)$/is);
			
	&docDatabase::insertOneAppStatus($dbh,$councilCode,$councilName,$Downloaded_date,'Running');
	
	my ($docDetailsHashRef, $Code, $pdfURLCountInSite);
	my %docDetailsFinal;
	
	($docDetailsHashRef, $documentURL, $Code) = &extractLink($method, $homeURL, $documentURL, $applicationURL, $applicationNo);
	
	if(!$docDetailsHashRef)
	{
		print "No Docs Found\n";
		next;
	}
	else
	{
		%docDetailsFinal=%$docDetailsHashRef;
		$pdfURLCountInSite=keys %docDetailsFinal;		
	}
	
	print "pdfURLCountInSite==>$pdfURLCountInSite\n";
	$todayDocCountInSite = $todayDocCountInSite + $pdfURLCountInSite;
		
	my $RetrieveMD5=[];
	my $noOfPDFDownloadedCount=0;
	
	# if(lc($Markup) eq 'large')
	# {
		if($pdfURLCountInSite > $noOfDocumentsfromDB)
		{
			$RetrieveMD5 =&docDatabase::RetrieveMD5($dbh, $councilCode, $formatID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	
	
	my $tempDownloadLocation = $localDownloadDirectory.'/'.$formatID;
	print "tempDownloadLocation=>$tempDownloadLocation\n";
	unless ( -d $tempDownloadLocation )
	{
		$tempDownloadLocation=~s/\//\\/igs;
		system("mkdir $tempDownloadLocation");
	}

	print "Folder Created[[MKDIR]]====> $tempDownloadLocation\n";
	
	my ($oneAppAvailStatus,$oneAppName,$Comments);
	
	foreach my $pdfURL(keys %docDetailsFinal)
	{
		my ($docType,$pdfName,$docPublishedDate);

		print "pdfURL: $pdfURL\n";
		$docType = $docDetailsFinal{$pdfURL}->[0];
		$pdfName = $docDetailsFinal{$pdfURL}->[1];
		$docPublishedDate=$docDetailsFinal{$pdfURL}->[2];
		
		$pdfName=~s/WITHOUT_PRESONAL_DATA/Application_Form_without_personal_data/igs;
		$pdfName=~s/WITHOUT_PERSONAL_DATA/Application_Form_without_personal_data/igs;
		
		if(($docType=~m/Application(?:\s+|_|-)?Form/is) && ($councilCode =~m/(?:54|312)/is))
		{
			$pdfName=~s/Application/Application_Form/igs;
		}
		
		
		my ($downloadStatus);
		if(lc($Markup) eq 'large')
		{
			if($pdfURL!~m/^\s*$/is)
			{
				my ($downloadResCode, $downloadErrMsg, $MD5)=&onlineDownloadUtility::Download($pdfURL,$pdfName,$documentURL,$tempDownloadLocation,$RetrieveMD5,$Markup,$lwpAgent);
				
				($downloadStatus)=&onlineDownloadUtility::UrlStatus($downloadResCode);
				
				$noOfPDFDownloadedCount++ if($downloadResCode=~m/200/is);
					
				$Comments=$Comments.'|'.$downloadErrMsg if($downloadErrMsg!~m/^\s*$/is);
				
				if(($pdfName=~m/$docDownloadConfig->{'formKeywordsMatch'}->{'keywords'}/is) && ($pdfName !~m/$docDownloadConfig->{'formNegativeKeywordsMatch'}->{'keywords'}/is))
				{
					$oneAppAvailStatus = 'Y';
					$oneAppName = $pdfName;
				}	
				
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$formatID\', \'$councilCode\', \'$MD5\', \'$pdfName\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				$docType=~s/\'/\'\'/igs;
				my $Insert_Document_List = "(\'$formatID\', \'$pdfName\', \'$docType\', \'$pdfName\', \'$Downloaded_date\', \'$docPublishedDate\', \'$downloadStatus\'),";	
				$Insert_Document.=$Insert_Document_List;
				
			}
		}	
		else
		{
			if(($pdfName=~m/$docDownloadConfig->{'formKeywordsMatch'}->{'keywords'}/is) && ($pdfName !~m/$docDownloadConfig->{'formNegativeKeywordsMatch'}->{'keywords'}/is))
			{
				$oneAppAvailStatus = 'Y';
				$oneAppName = $pdfName;
				
				if($pdfURL!~m/^\s*$/is)
				{
					my ($downloadResCode, $downloadErrMsg, $MD5)=&onlineDownloadUtility::Download($pdfURL,$pdfName,$documentURL,$tempDownloadLocation,$RetrieveMD5,$Markup,$lwpAgent);
					
					($downloadStatus)=&onlineDownloadUtility::UrlStatus($downloadResCode);
					
					$noOfPDFDownloadedCount++ if($downloadResCode=~m/200/is);	
					
					$Comments=$Comments.'|'.$downloadErrMsg if($downloadErrMsg!~m/^\s*$/is);
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$formatID\', \'$councilCode\', \'$MD5\', \'$pdfName\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
					
					}	
					
					$docType=~s/\'/\'\'/igs;

					my $Insert_Document_List = "(\'$formatID\', \'$pdfName\', \'$docType\', \'$pdfName\', \'$Downloaded_date\', \'$docPublishedDate\', \'$downloadStatus\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$oneAppName\', No_of_Documents = \'$pdfURLCountInSite\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
					$Update_Query.= $Flag_Update_query if($downloadResCode=~m/200/is);	
				}
			}
		}

		#--------- DB INSERTION above 1000 record ---------------
		$MaxCount++;
		if($MaxCount == 995)
		{
			($MaxCount, $Insert_Document, $MD5_Insert_Query, $Update_Query) = QueryExecuteMax($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
		}
	}

	&onlineDownloadUtility::Move_Files($tempDownloadLocation, $formatID);
	
	DB_Flag:
	
	if($pdfURLCountInSite > $noOfDocumentsfromDB)
	{
		$noOfPDFDownloadedCount = $noOfPDFDownloadedCount + $noOfDocumentsfromDB;
		# $totalDownloadedCountToday = $todayCount;
	}	
	# else
	# {
		# $totalDownloadedCountToday = $noOfDocumentsfromDB;
	# }	
	
	print "#### Download Report Start ####\n";
	print "todayCountInSite: $todayDocCountInSite\n";
	print "pdfURLCountInSite: $pdfURLCountInSite\n";
	print "noOfDocumentsfromDB: $noOfDocumentsfromDB\n";
	
	$totalDownloadedCountToday = $totalDownloadedCountToday + $noOfPDFDownloadedCount;
	print "Todays_Downloaded_Count: $totalDownloadedCountToday\n";
	# if($pdfURLCountInSite eq $totalDownloadedCountToday)
	# {
		# print "All documents downloaded correctly!!\n";	
		# $status	= "Completed";
	# }
	# else
	# {
		# print "Something missing need to check\n";
		# $status	= "Completed. But have Issue!";
	# }
	
	$status	= "Completed";
	
	print "#### Download Report End ####\n";
		
	$documentURL=~s/\'/\'\'/igs;
	
	my $insert_query = "(\'$formatID\', \'$councilCode\', \'$Markup\', \'$documentURL\', \'$pdfURLCountInSite\', \'$oneAppAvailStatus\', \'$Downloaded_date\', \'$Comments\', \'$noOfPDFDownloadedCount\'),";	
	$Insert_OneAppLog.= $insert_query;

	if($oneAppName=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfURLCountInSite\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfURLCountInSite\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$oneAppName\', No_of_Documents = \'$pdfURLCountInSite\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$formatID\' and Council_Code = \'$councilCode\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
}

&docDatabase::updateOneAppStatus($dbh,$councilCode,$Downloaded_date,$todayDocCountInSite,$totalDownloadedCountToday, $status);

$Insert_OneAppLog=~s/\,$//igs;
# print "\n$Insert_OneAppLog\n";
&docDatabase::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\n$Insert_Document\n";
&docDatabase::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
print "Update_Query:: $Update_Query\n";
&docDatabase::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
&docDatabase::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

sub extractLink()
{
	my $method = shift;
	my $homeURL = shift;
	my $documentURL = shift;
	my $applicationURL = shift;
	my $applicationNo = shift;
	
	my %docDetailsHash;
	
	my ($Content,$responseCode,$RedirectURL,$Content1 );
	
	my $landingPageFlag = 0;
	
	
	Re_Ping:
	if($method=~m/GET/is)
	{
		if($documentURL eq "")
		{
			if($applicationURL ne "")
			{
				($Content,$responseCode,$RedirectURL) =&onlineDownloadUtility::getMethodLWP($applicationURL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
			}
			elsif($applicationNo ne "")
			{
				($Content,$responseCode)=&onlineDownloadUtility::SearchViaApplicationNo($applicationNo,$homeURL,$Search_Post_URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language,$Viewstate_Regex,$Viewstategenerator_Regex,$CSRFToken_Regex,$Eventvalidation_Regex,$Post_Content,$Landingpage_Regex,$Filter_URL,$Application_URL_Regex,$Token,$AFHToken);
			}
		}
		else
		{			
			# if($councilCode=~m/^3|^13|^15|^171|^191|^32$|^131$/is)
			# if($councilCode=~m/^67$/is)
			# {
				# print "councilCode::$councilCode\n";
				# ($Content,$responseCode,$RedirectURL) =&onlineDownloadUtility::getMethodLWP($documentURL,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
				# ($Content,$responseCode) =&onlinegetUtility500::getMethodLWP($documentURL);
				
				# # print "Content::$Content\n";
				# # open F1, ">32content.html";
				# # print F1 $Content;
				# # close F1;
				# print "responseCode::$responseCode\n";			
				# # print "Content::$Content\n";
				# exit;
			# }
			# else
			# {
				($Content,$responseCode,$RedirectURL) =&onlineDownloadUtility::getMethodLWP($documentURL,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
			# }
			print "responseCode::$responseCode\n";
		}
		
	}
	elsif($method eq 'Search')
	{
		($Content,$responseCode)=&onlineDownloadUtility::SearchViaApplicationNo($applicationNo,$homeURL,$Search_Post_URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language,$Viewstate_Regex,$Viewstategenerator_Regex,$CSRFToken_Regex,$Eventvalidation_Regex,$Post_Content,$Landingpage_Regex,$Filter_URL,$Application_URL_Regex,$Token,$AFHToken);
	}	
			
	
	if(($Content=~m/$Doc_URL_Regex/is)&&($councilCode!~m/^3|^13|^15|^171/is))
	{
		my $relatedDocURL= $1;
		
		my $DocUrl=&onlineDownloadUtility::formAbsoluteURL($relatedDocURL,$applicationURL);
		if($councilCode eq 422)
		{
			$DocUrl=~s/http\:/https\:/igs;
		}
		$DocUrl = &onlineDownloadUtility::urlClean($DocUrl);
		
		print "formAbsoluteURL==>$DocUrl\n";
		
		($Content,$responseCode,$RedirectURL) =&onlineDownloadUtility::getMethodLWP($DocUrl,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);		
	}

	if(($landingPageFlag == 0)&&($councilCode!~m/^3|^13|^15|^171/is))
	{
		print "landing\n\n";
		if(($Landingpage_Regex ne 'N/A') and ($Landingpage_Regex ne ''))
		{
			if($Content=~m/$Landingpage_Regex/is)
			{
				my $DocUrl;
				if($councilCode eq 119)
				{
					$DocUrl= "https://services.boston.gov.uk/agile/planApp.aspx?ref=".$1;
				}
				else
				{
					$DocUrl=&onlineDownloadUtility::urlTrim($1);
				}
				
				
				$documentURL=&onlineDownloadUtility::formAbsoluteURL($DocUrl,$documentURL);
			
				print "Landing page then documentURL: $documentURL\n"; 
				
				$landingPageFlag = 1;
				goto Re_Ping;
			}
		}	
	}
	
	# open(DC,">248Content.html");
	# print DC "$Content";
	# close DC;
	
	if($format eq 'Table')
	{
		my $appDocDetails=&collectDocsDetails($Content,$homeURL,$RedirectURL,$documentURL,$applicationURL,$Doc_URL_Method,$lwpAgent);
		my %appDocDetails=%$appDocDetails;
		%docDetailsHash = (%docDetailsHash,%appDocDetails);
	}
	
	# open(DC,">17111Content.html");
	# print DC "$Content";
	# close DC;
	# exit;	
	
	if($Pagination_Type eq 'N/A')
	{
		return (\%docDetailsHash,$documentURL,$responseCode);
	}
	
}


sub collectDocsDetails()
{
	my $Content=shift;
	my $homeURL=shift;
	my $RedirectURL=shift;
	my $documentURL=shift;
	my $applicationURL=shift;
	my $Doc_URL_Method=shift;
	my $lwpAgent=shift;
	
	my %docDetails;
	
	print "Collect_Docs_Details\n";
	
	if(($Html_Parser ne 'N/A') && ($Html_Parser eq 'Y'))
	{
		
		my ($appDocDetails)=&onlineDownloadUtility::Doc_Download_Html_Header($Content,$homeURL,$RedirectURL,$documentURL,$applicationURL,$Doc_URL_Method,$councilCode,$lwpAgent);
		my %appDocDetails=%$appDocDetails;
		%docDetails = (%docDetails,%appDocDetails);
		
	}
	else
	{
		print "Pari need to check this";
	}
	
	return \%docDetails;
	
}

sub QueryExecuteMax()
{	
	my ($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query)=@_;
	
	if($Insert_Document!~m/values\s*$/is)
	{
		$Insert_Document=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

		$Update_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

		$MD5_Insert_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);
		
		$Insert_Document="";
		$Update_Query="";
		$MD5_Insert_Query="";
		
		$Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
		$MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

		$MaxCount=0;
	}
	return ($MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
}