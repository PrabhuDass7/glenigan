use strict;
use Time::Piece;
# use File::stat;
use File::Path;
use Cwd qw(cwd);
use MIME::Lite;
use File::Basename;
use Cwd qw(abs_path);
use Time::Piece;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $logDirectory = ($basePath.'/logs');

require ($libDirectory.'/docDatabase.pm'); 

my $dbh = docDatabase::DbConnection();

my $insertQuery = "insert into tbl_large_to_others(Format_ID, Checked_date) values";
my $deleteFailedInsertQuery = "insert into tbl_large_to_others(Format_ID) values";

my $query = "select x.PROJECT_ID_P,x.Format_ID from (
select project_id_p,format_id from dr_project where  format_id in (
select  format_id from project_tracking where markup='L'
and Format_ID in (select  a.Format_ID from project_tracking as a,dr_project as b where isnull(b.PROJECT_SIZE,'')<>'L' and a.format_id=b.format_id   
and b.record_status_flag='Y'))) as x left  join tbl_large_to_others as y
on x.format_id=y.format_id where y.format_id is null";

my $sth = $dbh->prepare($query);
$sth->execute();
my (@Council_Code, @Format_ID,@Application_No,@Markup,@No_of_Documents,@PROJECT_ID,@PROJECT_STATUS);
my $datetime = localtime();
while(my @record = $sth->fetchrow)
{
	push(@PROJECT_ID,&Trim($record[0]));
	push(@Format_ID,&Trim($record[1]));
}
$sth->finish();
open(FN,">>$logDirectory/Removed_Docs_Large_to_Others_Log.txt");
print FN "Start : ".localtime()."\t";
close FN;

open(FN,">>$logDirectory/Large_to_Others_Deleted_Report.txt");
print FN "Format_ID\tFile_name\tDeleted_On\tRemoved_File_Size(MB)\n";
close FN;

open(FD,">>$logDirectory/Large_to_Others_Delete_Failed_Report.txt");
print FN "Format_ID\tFile_name\tDeleted_On\tRemoved_File_Size(MB)\n";
close FD;					

open(FD,">>$logDirectory/L_to_Others_Deleted_Report_Projectwise.txt");
print FN "Format_ID\tDeleted_On\tRemoved_File_Size(MB)\n";
close FD;					

my $path='//172.27.137.202/One_app_download/';

my $i=1;
my $Err_Flag = 0;
my $totalRemoveSize = 0;

my $reccount = @Format_ID;
print "Projects Count :: $reccount\n";
for(my $reccnt = 0; $reccnt < $reccount; $reccnt++)
{
	my $PROJECT_ID 		= $PROJECT_ID[$reccnt];
	my $Format_ID 		= $Format_ID[$reccnt];
	
	my $Temp_pdf_store_path="$path$Format_ID";
	my $dh;
	eval{
	opendir($dh, $Temp_pdf_store_path) || warn "can't opendir $Temp_pdf_store_path: $!";
	};
	if($!)
	{
		$deleteFailedInsertQuery.=	"(\'$Format_ID\'),";
	}
	print "Folder: $Format_ID\n";
	next if($Format_ID!~m/^[\d]+$/is);
	my $docsCount=0;
	my $totalRemoveSizeProjectwise=0;
	# open(FN,">>$logDirectory/OneApp_Deleted_Report.txt");
	if (-d $Temp_pdf_store_path)
	{
		my $now = localtime();
		my $time = Time::Piece->new;
		my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
		while (my $file_name=readdir $dh) 
		{
			next if($file_name=~m/^\s*(\.|\.\.|Thumbs\.db)\s*$/is);
			$docsCount++;
			if($file_name!~m/\s*(Application|App|Planning)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*/is)
			{
				my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat "$Temp_pdf_store_path/$file_name";
				$size=($size/1024)/1024;
				print "size:$size MB\n";
				
				eval{
					unlink("$Temp_pdf_store_path/$file_name");
				};
				if ($@) 
				{
					open(FD,">>$logDirectory/Large_to_Others_Delete_Failed_Report.txt");
					print FD "$Format_ID\t$file_name\t$now\t$size\t@$\n";
					close FD;						
				}
				else
				{			
					# $i++;
					$totalRemoveSize=$totalRemoveSize+$size;
					$totalRemoveSizeProjectwise=$totalRemoveSizeProjectwise+$size;
					open(FN,">>$logDirectory/Large_to_Others_Deleted_Report.txt");
					print FN "$Format_ID\t$file_name\t$now\t$size\n";
					close FN;
				}
			}
		}
		$insertQuery.=	"(\'$Format_ID\',\'$Schedule_Date\'),";
		open(FN,">>$logDirectory/L_to_Others_Deleted_Report_Projectwise.txt");
		print FN "$Format_ID\t$now\t$totalRemoveSizeProjectwise\n";
		close FN;
	}	
	closedir $dh;
}

open(FN,">>$logDirectory/Removed_Old_Folders_Log.txt");
print FN "End : ".localtime()."\t$totalRemoveSize\t$i\n";
close FN;

$insertQuery=~s/,$//igs;
$deleteFailedInsertQuery=~s/,$//igs;
if($insertQuery!~m/values\s*$/is)
{
	docDatabase::Execute($dbh,$insertQuery);
}
	
if($insertQuery!~m/values\s*$/is)
{
	docDatabase::Execute($dbh,$deleteFailedInsertQuery);
}	

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

# select * from tbl_large_to_others where format_ID > 8133841