############################################################
# This webSearchDownloadUtility module 
# 
# 
############################################################
package webSearchDownloadUtility;

use strict;
use Time::Piece;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use HTML::TableExtract;
use HTTP::Request;
use WWW::Mechanize;
use URI::Escape;
use Digest::MD5;
use URI::URL;
use Config::Tiny;
use IO::Handle;
use IO::File;
use Net::SSL;
use FileHandle;
use File::Basename qw(dirname);
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use Cwd qw(abs_path);
use LWP::Simple;
use MIME::Base64;
use URI::Escape;
use URI::Encode;
use Encode;
use File::Path 'rmtree';
use File::stat;
require Exporter;

local $ENV{PERL_NET_HTTPS_SSL_SOCKET_CLASS} = 'Net::SSL';


my @ISA = qw(Exporter);
my @EXPORT = ();

####
# Set root path for accessing files
####
my $basePath = dirname (dirname abs_path $0); 

####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/ctrl');
my $logDirectory = ($basePath.'/logs');

####
# Create new object of class Config::Tiny
####
my ($councilDetailsFile,$configFile) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile"
####
$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/webSearchFormat.ini');
$configFile = Config::Tiny->read($ConfigDirectory.'/docDownloadConfig.ini');


# proxy support
$ENV{HTTPS_PROXY} = $configFile->{'proxyDetail'}->{'proxy1'};


###########################################################
#Function : getDateTime()
#Desc  	  : Return current system date and time
#Date	  : 18-09-2018
###########################################################
sub getDateTime()
{
	my $time = Time::Piece->new;
	my $dateTime = $time->strftime('%m/%d/%Y %H:%M');
	
	return($dateTime);
}

###########################################################
#Function : mechUserAgent()
#Desc  	  : Return mech UserAgent class variable
#Date	  : 18-09-2018
###########################################################
sub mechUserAgent()
{
	my $cCode = shift;
	
	my $mech;
    if($councilDetailsFile->{$cCode}->{'SSL_VERIFICATION'} eq 'N')
    {
        $mech = WWW::Mechanize->new(autocheck => 0);
    }
    else
    {	
		$mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
    }
	
    return($mech);
}

###########################################################
#Function : lwpUserAgent()
#Desc  	  : Return LWP UserAgent class variable
#Date	  : 18-09-2018
###########################################################
sub lwpUserAgent()
{
	my $cCode = shift;
	
	my ($ua,$cookie);
    if($councilDetailsFile->{$cCode}->{'SSL_VERIFICATION'} eq 'N')
    {
        $ua = LWP::UserAgent->new;
		$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
		$ua->max_redirect(0); 
		$ua->cookie_jar({});
		$ua->ssl_opts( verify_hostname => 0 );
		$cookie = HTTP::Cookies->new(file=>"$logDirectory/cookiefile.txt", autosave=>1);
		$ua->cookie_jar($cookie);
    }
    else
    {	
		$ua = LWP::UserAgent->new(ssl_opts => {
				  SSL_verify_mode => SSL_VERIFY_NONE,
				  verify_hostname => 1,
				  keep_alive => 1
			  });
		# $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
		$ua->agent("Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Mobile Safari/537.36");
		$ua->max_redirect(0); 
		$ua->cookie_jar({});
		$cookie = HTTP::Cookies->new(file=>"$logDirectory/cookiefile.txt", autosave=>1);
		$ua->cookie_jar($cookie);
    }
	
    return($ua);
}

###########################################################
#Function : readINI()
#Desc  	  : Read INI inputs and return value in scalar variable
#Date	  : 05-03-2019
###########################################################
sub readINI()
{
	my $iniClassVariable = shift;
	my $councilCode = shift;
	
	my ($Method,$Format,$Home_URL,$Filter_URL,$Doc_URL_Regex,$Accept_Cookie,$Accept_Cookie_URL,$Accept_Post_Content,$Need_Host,$Accept_Host,$Host,$Content_Type,$Referer,$Post_Content,$Landingpage_Regex,$Table_Block_Regex,$Tr_Block_Regex,$Td_Block_Regex,$Doc_Publisheddate_Index,$Doc_Type_Index,$Doc_Description_Index,$Doc_DownloadURL_Index,$Doc_DownloadURL_Regex,$Doc_Filter_URL,$Doc_URL_Method,$Pagination_Type,$TotalNoOfPages_Regex,$Nextpage_Regex,$Nextpage_Regex_Replace,$Proxy,$Accept_Language,$Eventvalidation_Regex,$CSRFToken_Regex,$Viewstategenerator_Regex,$Viewstate_Regex,$Application_URL_Regex,$Search_Post_URL,$Token,$AFHToken,$Html_Parser,$Next_Page_Replace_Key,$Current_Page_Number_Regex,$Next_Referer,$Next_Page_Post_Content);
	

	$Method = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Method'});
	print "Method==>$Method\n";
	$Format = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Format'});
	$Proxy = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Proxy'});
	$Home_URL = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Home_URL'});
	$Search_Post_URL = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Search_Post_URL'});
	$Filter_URL = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Filter_URL'});
	$Doc_URL_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_URL_Regex'});
	$Accept_Cookie = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Accept_Cookie'});
	$Accept_Cookie_URL = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Accept_Cookie_URL'});
	$Accept_Post_Content = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Accept_Post_Content'});
	$Need_Host = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Need_Host'});
	$Accept_Host = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Accept_Host'});
	$Accept_Language = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Accept_Language'});
	$Host = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Host'});
	$Content_Type = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Content_Type'});
	$Referer = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Referer'});
	$Post_Content = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Post_Content'});
	$Landingpage_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Landingpage_Regex'});
	$Html_Parser = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Html_Parser'});
	$Table_Block_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Table_Block_Regex'});
	$Tr_Block_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Tr_Block_Regex'});
	$Td_Block_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Td_Block_Regex'});
	$Doc_Publisheddate_Index = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_Publisheddate_Index'});
	$Doc_Type_Index = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_Type_Index'});
	$Doc_Description_Index = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_Description_Index'});
	$Doc_DownloadURL_Index = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Index'});
	$Doc_DownloadURL_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Regex'});
	$Doc_Filter_URL = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_Filter_URL'});
	$Doc_URL_Method = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Doc_URL_Method'});
	$Pagination_Type = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Pagination_Type'});
	$TotalNoOfPages_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'TotalNoOfPages_Regex'});
	$Nextpage_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Nextpage_Regex'});
	$Nextpage_Regex_Replace = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Nextpage_Regex_Replace'});
	$Next_Page_Replace_Key = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Next_Page_Replace_Key'});
	$Eventvalidation_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Eventvalidation_Regex'});
	$CSRFToken_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'CSRFToken_Regex'});
	$Viewstategenerator_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Viewstategenerator_Regex'});
	$Viewstate_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Viewstate_Regex'});
	$Application_URL_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Application_URL_Regex'});
	$Token = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Token'});
	$AFHToken = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'AFHToken'});
	$Current_Page_Number_Regex = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Current_Page_Number_Regex'});
	$Next_Referer = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Next_Referer'});
	$Next_Page_Post_Content = &iniVariableEmptyCheck($councilDetailsFile->{$councilCode}->{'Next_Page_Post_Content'});
	
    return($Method,$Format,$Home_URL,$Search_Post_URL,$Filter_URL,$Doc_URL_Regex,$Accept_Cookie,$Accept_Language,$Accept_Cookie_URL,$Accept_Post_Content,$Need_Host,$Accept_Host,$Host,$Content_Type,$Referer,$Post_Content,$Landingpage_Regex,$Table_Block_Regex,$Tr_Block_Regex,$Td_Block_Regex,$Doc_Publisheddate_Index,$Doc_Type_Index,$Doc_Description_Index,$Doc_DownloadURL_Index,$Doc_DownloadURL_Regex,$Doc_Filter_URL,$Doc_URL_Method,$Pagination_Type,$TotalNoOfPages_Regex,$Nextpage_Regex,$Nextpage_Regex_Replace,$Proxy,$Eventvalidation_Regex,$CSRFToken_Regex,$Viewstategenerator_Regex,$Viewstate_Regex,$Application_URL_Regex,$Token,$AFHToken,$Html_Parser,$Next_Page_Replace_Key,$Current_Page_Number_Regex,$Next_Referer,$Next_Page_Post_Content);
}


###########################################################
#Function : iniVariableEmptyCheck()
#Desc  	  : To Check INI variable must have value or else assign N/A to empty variables
#Date	  : 05-03-2019
###########################################################
sub iniVariableEmptyCheck()
{
	my $iniFileValue = shift;
	
	$iniFileValue = 'N/A' if($iniFileValue eq '');
	
	# print "iniFileValue==>$iniFileValue\n"; <STDIN>;
	
	return($iniFileValue);
}


###########################################################
#Function : urlTrim()
#Desc  	  : To remove unwanted space and amp symbols in URLs
#Date	  : 05-03-2019
###########################################################
sub urlTrim()
{
	my $urlTrim = shift;
	
	$urlTrim =~ s/^\s+//g;
	$urlTrim =~ s/\s+$//g;
	$urlTrim =~ s/^\n+//g;
	$urlTrim =~ s/\n+$//g;

	$urlTrim =~ s/\&amp\;/\&/g;
	
	return($urlTrim);
}

###########################################################
#Function : Trim()
#Desc  	  : To remove unwanted space and amp symbols in URLs
#Date	  : 15-08-2019
###########################################################
sub Trim()
{
	my $Trim = shift;
	
	$Trim =~ s/^\s+//g;
	$Trim =~ s/\s+$//g;
	$Trim =~ s/^\n+$/\n/g;
	$Trim =~ s/\s\s+/ /g;
	if($Trim!~m/<[^>]*?>/is)
	{
		$Trim =~ s/\'//g;
	}	

	$Trim =~ s/\&amp\;/\&/g;
	
	return($Trim);
}

###########################################################
#Function : formAbsoluteURL()
#Desc  	  : URI::URL module exports the url() function as an alternate constructor interface
#Date	  : 05-03-2019
###########################################################
sub formAbsoluteURL()
{
	my $actualUrl = shift;
	my $sourceURL = shift;
	
	my $fullURL;
	if($actualUrl!~m/http/is)
	{
		my $u1=URI::URL->new($actualUrl,$sourceURL);
		my $u2=$u1->abs;
		$fullURL=$u2;
	}
	else
	{
		# print "Remind Me...."; <STDIN>;
		my $u1=URI::URL->new($actualUrl,$sourceURL);
		my $u2=$u1->abs;
		$fullURL=$u2;		
		
		print "FullURL==>$fullURL\n";
	}
	$fullURL=~s/\&amp\;/\&/gsi;
	return($fullURL);
}

###########################################################
#Function : getMethodLWP()
#Desc  	  : LWP::UserAgent objects can be used to dispatch web requests and returns a HTTP::Response object and content etc.
#Date	  : 05-03-2019
###########################################################
sub getMethodLWP()
{
	my $documentURL=shift;
	my $applicationURL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $lwpAgent=shift;
	my $Accept_Language=shift;

	my $proxyDetail1 = $configFile->{'proxyDetail'}->{'proxy1'};
	my $proxyDetail2 = $configFile->{'proxyDetail'}->{'proxy2'};
	
	if($Proxy eq 'proxy1')
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail1);
		print "proxyDetail1==>$proxyDetail1\n";
	}
	elsif($Proxy eq 'proxy2')
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail2);
		print "proxyDetail2==>$proxyDetail2\n";
	}
	
	my $rerunCount=0;
	my ($redirectURL,$content, $code);
	if($documentURL=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	
	GetHome:
	if(( $rerunCount >= 2 ) && ($code!~m/^20/is))
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail2);
		print "proxyDetail2==>$proxyDetail2\n";
	}
	
	$documentURL=&urlTrim($documentURL);
	
	print "getURL==>$documentURL\n";
	
	
	my $req = HTTP::Request->new(GET=>$documentURL);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	my $res;
	eval {
		$res = $lwpAgent->request($req);
	
		my $cookie = HTTP::Cookies->new(file=>"$logDirectory/cookiefile.txt", autosave=>1);
		$cookie->extract_cookies($res);
		$cookie->save;
		$cookie->add_cookie_header($req);
		
		$code=$res->code;
		my $status_line=$res->status_line;
		
		print "LWP GET ResponseCode:: $code\n";
		
		if($code=~m/^20/is)
		{
			$content = $res->content;
			$content = $res->decoded_content;
			
			$redirectURL = $res->request()->uri();
		}
		elsif($code=~m/^30/is)
		{
			my $loc=$res->header("location");
			if( $rerunCount <= 3 )
			{
				$rerunCount++;
				if($loc!~m/http/is)
				{
					my $absURL=&formAbsoluteURL($loc,$documentURL);
					
					$documentURL=$absURL;
					$redirectURL=$absURL;
				}
				else
				{
					$documentURL=$loc;
					$redirectURL=$loc;
				}
				goto GetHome;
			}
		}
		else
		{
			if( $rerunCount <= 1 )
			{
				$rerunCount++;
				
				sleep 1;
				goto GetHome;
			}
		}
	};
	# open(DC,">pdf_content.html");
	# print DC $content;
	# close DC;
	# exit;
	
	return ($content,$code,$redirectURL);
}


sub postMethodLWP()
{
	my $Doc_Post_URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $lwpAgent=shift;
	
	my $rerun_count=0;
	
	my $proxyDetail = $configFile->{'proxyDetail'}->{'proxy1'};
	
	if($Proxy eq 'proxy1')
	{
		$lwpAgent->proxy(['http','https'], $proxyDetail);
	}
	
	$Doc_Post_URL =~ s/^\s+|\s+$//g;
	$Doc_Post_URL =~ s/amp;//igs;
	
	# print "Doc_Post_URL==>$Doc_Post_URL\n";
	# print "Referer==>$Referer\n";
	
	PostHome:
	my $req = HTTP::Request->new(POST=>$Doc_Post_URL);
	
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}		
	
	
	print "Post_Content_Tmp==>$Post_Content\n";
	
	$req->content($Post_Content);
	my $res = $lwpAgent->request($req);
	
	my $cookie = HTTP::Cookies->new(file=>"$logDirectory/cookiefile.txt", autosave=>1);	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	print "LWP POST Response CODE :: $code\n";
	print "status_line :: $status_line\n";
	
	my ($content,$redirectURL);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
	
		if($rerun_count <= 3)
		{
			$redirectURL=&formAbsoluteURL($loc,$Doc_Post_URL);
				
			($content,$code)=&getMethodLWP($redirectURL,$Doc_Post_URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent);
			
			$rerun_count++;
			
			goto PostHome;
		}
	}
	else
	{
		if ($rerun_count <= 1)
		{
			$rerun_count++;
			
			goto PostHome;
		}
	}
	
	return ($content,$code,$redirectURL);
}

sub Download()
{
	my $pdfURL 		= shift;
	my $pdfName 	= shift;
	my $documentURL	= shift;
	my $tempDownloadLocation= shift;
	my $MD5_LIST	= shift;
	my $Markup		= shift;
	my $lwpAgent	= shift;
	# my $councilCode	= shift;
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$tempDownloadLocation" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;
	
	if( grep (/$pdfName/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		
		# my ($pdf_content, $code, $reurl);
		# if($councilCode==274)
		# {		
			# # my ($pdf_content1, $code1, $reurl1)=&getMethodLWP($documentURL,"","","",$documentURL,"","",$lwpAgent,"");
			# my ($pdf_content1, $code1, $reurl1)=&getMethodLWP($pdfURL,"","","","https://edrms.milton-keynes.gov.uk/NorthgateIM.WebSearch/Results.aspx","",$lwpAgent,"");
			# $pdf_content=$pdf_content1;
			# $code=$code1;
			# $reurl=$reurl1;
			# print "code1 $code1\n\n";
			# print "274pdfURL $pdfURL\n\n";
			# print "274code $code\n\n";
		# }
		# else
		# {
			# my ($pdf_content1, $code1, $reurl1)=&getMethodLWP($pdfURL,"","","",$documentURL,"","",$lwpAgent,"");
			# $pdf_content=$pdf_content1;
			# $code=$code1;
			# $reurl=$reurl1;
		# }
		(my $pdf_content, $code, my $reurl)=&getMethodLWP($pdfURL,"","","",$documentURL,"","",$lwpAgent,"");
		$file_name=$tempDownloadLocation.'\\'.$pdfName;
		# open(DC,">pdf_content.html");
		# print DC $pdf_content;
		# close DC;
		# exit;
		if($code ==200)
		{
			eval
			{
				# if(lc($Markup) eq 'large')
				# {
					$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				# }	
				
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "Doc Exist\n";
					$MD5  = '';
					$code = 600;
				}
				else
				{
					print "Doc not Exist\n";
					print "file_name==>$file_name\n";
					
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $file_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();
					
					my $size = stat($file_name)->size;
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";
					print "downCount  :: $downCount\n";
				   
					# if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					if((($return_size_kb=~m/^0$|^0\.00$/is) or ($return_size_kb <= "1.00"))&& ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}				
				} 
			};
			
			if($@)
			{
				$Err=$@;
				print "DownloadError==>$Err\n";
				# <STDIN>;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download;
				}	
			}
		}
	}
	
	return($code,$Err,$MD5);
}

sub UrlStatus()
{
	my $reponseCode=shift;
	my $reponseStatus;
	if($reponseCode == 200)
	{
		$reponseStatus = 'Y';
	}
	else
	{
		$reponseStatus = 'N';
	}
	return $reponseStatus;
}


sub Doc_Download_Html_Header()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $RedirectURL=shift;
	my $documentURL=shift;
	my $applicationURL=shift;
	my $Doc_URL_Method=shift;
	my $councilCode=shift;
	my $lwpAgent=shift;
	
	my %Doc_Details;
	

	if($councilDetailsFile->{$councilCode}->{'Table_Block_Regex'} ne 'N/A')
	{
		
		my $data = $1 if($Content=~m/($councilDetailsFile->{$councilCode}->{'Table_Block_Regex'})/is);
				
		chomp $data;
		
		$data=~s/<th\s*\/\s*>/<th><\/th>/igs;
		$data=~s/<td\s*\/\s*>/<td><\/td>/igs;
					
		if ($councilDetailsFile->{$councilCode}->{'Doc_Type_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$councilCode}->{'Doc_Type_Index'}='';
		}
		if ($councilDetailsFile->{$councilCode}->{'Doc_Publisheddate_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$councilCode}->{'Doc_Publisheddate_Index'}='';
		}
		
		if ($councilDetailsFile->{$councilCode}->{'Doc_Description_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$councilCode}->{'Doc_Description_Index'}='';
		}
		if ($councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Index'}='';
		}
		
		
		my $te = HTML::TableExtract->new(keep_html => 1,headers =>[$councilDetailsFile->{$councilCode}->{'Doc_Publisheddate_Index'},$councilDetailsFile->{$councilCode}->{'Doc_Type_Index'},$councilDetailsFile->{$councilCode}->{'Doc_Description_Index'},$councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Index'}]);
		
		$te->parse( $data );		
		
		my ($docURL,$docDesc,$docType,$publishedDate,$pdfName,$rootUrl);
				
		if($RedirectURL ne '')
		{
			$rootUrl=$RedirectURL;
		}
		elsif($documentURL ne '')
		{
			$rootUrl=$documentURL;
		}
		elsif($applicationURL ne '')
		{
			$rootUrl=$applicationURL;
		}
		
		my $pdfcountInc = 1;
		foreach my $ts ( $te->tables() )
		{
			foreach my $row ( $ts->rows() )
			{
				
				my $link="@$row[3]"; 
								
				if($councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Regex'} ne 'N/A')
				{
					if($link=~m/$councilDetailsFile->{$councilCode}->{'Doc_DownloadURL_Regex'}/is)
					{	
						my $Durl=$1;
						
						$docURL=&formAbsoluteURL($Durl,$rootUrl);
							
					}					
				}
				
				$docDesc = &Clean(@$row[2]);
				$docType = &Clean(@$row[1]);
				$publishedDate = &Clean(@$row[0]);
				
				print "\ndocDesc::$docDesc";
				print "\ndocType::$docType";
				print "\npublishedDate::$publishedDate";
				print "\ndocURL::$docURL";
				print "\nLink::$link\n";
				# <STDIN>;
			
				my ($fileType);
				if($docURL=~m/\.([\w]{3,4})\s*$/is)
				{
					$fileType=$1;
				}
				else
				{
					$fileType='pdf';
				}	

				if($docDesc!~m/^\s*$/is)
				{
					$pdfName = $pdfcountInc."_".$docDesc.".".$fileType;
				}	
				else
				{
					$pdfName = $pdfcountInc."_".$docType.".".$fileType;
				}
				
				$pdfcountInc++;
				
				$pdfName = &PDF_Name($pdfName);
				$docURL = decode_entities($docURL);
			
				
				if(($docURL!~m/^\s*$/is) && ($pdfName!~m/^\s*$/is))
				{
					$Doc_Details{$docURL}=[$docType,$pdfName,$publishedDate];
				}
				
			}
		}

	}
	
	return \%Doc_Details;

}

sub PDF_Name()
{
	my $name=shift;
	$name=decode_entities($name);
	$name=~s/\s+\-\s+/\-/igs;
	$name=~s/\"//igs;
	$name=~s/\'//igs;
	$name=~s/\/|\\/_/igs;
	$name=~s/^\W+|\W+$//igs;
	$name=~s/\://igs;
	$name=~s/\s+/_/igs;
	$name=~s/\-\s*$//igs;
	$name=~s/\./DotChar/igs;
	$name=~s/[\W]/_/igs;
	$name=~s/\_\_+/_/igs;
	$name=~s/DotChar/\./igs;
	return $name;
}

sub Clean()
{
	my $name=shift;
	$name=~s/<[^>]*?>/ /igs;
	$name=~s/\s\s+/ /igs;
	$name=~s/\&amp\;/\&/igs;
	$name=~s/&nbsp;/ /igs;	
	return $name;
}

sub urlClean()
{
	my $url=shift;
	
	$url=~s/\&amp\;/\&/igs;
	
	return $url;
}


sub SearchViaApplicationNo()
{
	my ($applicationNo,$Home_URL,$Search_Post_URL,$applicationURL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language,$Viewstate_Regex,$Viewstategenerator_Regex,$CSRFToken_Regex,$Eventvalidation_Regex,$Post_Content,$Landing_Page_Regex,$Filter_URL,$Application_URL_Regex,$token_Regex,$afhtoken_Regex) = @_;
	
	print "Home_URL==>$Home_URL\n";
	print "applicationNo==>$applicationNo\n";
	
	my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$CSRFToken,$Content,$Code,$Redir_Url,$afhtoken,$token);
	
	if($Post_Content=~m/<[A-Z]+>/is)
	{
		($Content,$Code,$Redir_Url) =&getMethodLWP($Home_URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$lwpAgent,$Accept_Language);
		
		# open(DC,">Home_Content.html");
		# print DC $Content;
		# close DC;
		# exit;
		
		if($Viewstate_Regex ne 'N/A')
		{
			$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		}	
		if($Viewstategenerator_Regex ne 'N/A')
		{
			$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		}	
		if($CSRFToken_Regex ne 'N/A')
		{
			$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
		}	
		if($Eventvalidation_Regex ne 'N/A')
		{
			$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		}		
		if($afhtoken_Regex ne 'N/A')
		{
			$afhtoken=uri_escape($1) if($Content=~m/$afhtoken_Regex/is);
		}	
		if($token_Regex ne 'N/A')
		{
			$token=uri_escape($1) if($Content=~m/$token_Regex/is);
		}	
	}
		
	$applicationNo=uri_escape($applicationNo);
	my $Post_Content_Tmp=$Post_Content;
	$Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
	$Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
	$Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
	$Post_Content_Tmp=~s/<APP_NUMBER>/$applicationNo/igs;
	$Post_Content_Tmp=~s/<TOKEN>/$token/igs;
	$Post_Content_Tmp=~s/<AFHTOKEN>/$afhtoken/igs;
	

	my($Post_Responce,$res_code,$redirectURL)=&postMethodLWP($Search_Post_URL,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy,$Need_Host,$lwpAgent);
	$Content = $Post_Responce;
	
	# open(DC,">Post_Responce.html");
	# print DC "$Post_Responce";
	# close DC;
	# exit;

	
	return($Content,$res_code);
}

sub Move_Files()
{
	my $Temp_pdf_store_path = shift;
	my $formatID = shift;
	
	my $Current_Location = $basePath;
	
	# print "Temp_pdf_store_path:	$Temp_pdf_store_path\n";
	# print "Current_Location:	$Current_Location\n";

	opendir(my $dh, "$Temp_pdf_store_path") || warn "can't opendir $Temp_pdf_store_path: $!";
	# my @File_Names = readdir($dh);
	my @File_Names = grep {$_ ne '.' and $_ ne '..'} readdir $dh;
	closedir $dh;
	my $cnt=@File_Names;
	my $Comments;
	my $Spd_date=localtime();
	if($cnt==0)
	{
		print "Folder Empty\n";
		# system("rmdir $Temp_pdf_store_path");
		rmtree($Temp_pdf_store_path) or die "Cannot rmtree '$_' : $!";
		$Comments='No Documents found. Kindly check the script';
	}
	else
	{
		my $Download_Location_2 = $configFile->{'downloadLocation'}->{'sharePath'}."\\$formatID";
		my $Temp_pdf_store_path = "$Temp_pdf_store_path";
		eval{
			$Download_Location_2=~s/\//\\/igs;
			$Temp_pdf_store_path=~s/\//\\/igs;
			
			opendir(my $dhf, $Temp_pdf_store_path) || die "can't opendir $Temp_pdf_store_path: $!";
			my @File_Count = grep {$_ ne '.' and $_ ne '..'} readdir $dhf;
			closedir $dhf;
			my $File_Count=@File_Count;
			print "$File_Count\n";			
			if($File_Count > 0)
			{
				system("mkdir $Download_Location_2");
				print "File Moving to $Download_Location_2\n";
				my $Copy_Status = system("copy $Temp_pdf_store_path $Download_Location_2");
				if($Copy_Status == 0)
				{
					print "Moved\n";
					if ($Temp_pdf_store_path ne $Current_Location)
					{
						opendir(my $dhf, $Temp_pdf_store_path) || die "can't opendir $Temp_pdf_store_path: $!";
						my @File_Names=readdir($dhf);
						closedir $dhf;
						for(@File_Names)
						{
							if($_!~m/\.pl|\.pm|\.py|\.ini/is)
							{
								my $f="$Temp_pdf_store_path/$_";
								print "File: $f\n";
								unlink($f);
							}
						}	
						# system("rmdir $Temp_pdf_store_path");
						rmtree($Temp_pdf_store_path);
					}	
				}
				else
				{
					print "Failed To Copy\n";
				}			
			}	
		};
	}
}

sub onDemandStatus()
{
	my $No_Of_Doc_Downloaded_onDemand=shift;
	my $Site_Status=shift;
	my $onDemandStatus;
	
	print "No_Of_Doc_Downloaded_onDemand: $No_Of_Doc_Downloaded_onDemand\n";
	print "Site_Status: $Site_Status\n";
	
	if($No_Of_Doc_Downloaded_onDemand == 0)
	{
		if($Site_Status != 200)
		{
			$onDemandStatus = 'Site issue';
		}
		else
		{
			$onDemandStatus = 'No new docs downloaded';
		}	
	}
	elsif($No_Of_Doc_Downloaded_onDemand > 0)
	{
		$onDemandStatus = $No_Of_Doc_Downloaded_onDemand.' new docs downloaded';
	}

	return $onDemandStatus;
}

1;