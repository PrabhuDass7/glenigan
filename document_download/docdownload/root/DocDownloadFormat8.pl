use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename;
use JSON;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# $basePath="D:\\Glenigan\\Document_download\\";
print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm');
require ($libDirectory.'/Download_DB.pm'); 

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];
print "onDemand: $onDemand\n";
# print "iniDirectory: $iniDirectory\n";
if($Council_Code=~m/^274$/is)
{
	require ($libDirectory.'/onlinegetUtility500.pm'); 
}
if($Council_Code=~m/486/is)
{
	exit;
}

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);


#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";

my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Queries For Bulk Hit -------------------#

my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";
	
&Download_DB::Execute($dbh,$Dashboard_Insert_Query);
if($onDemand eq 'Y')
{
	print "This is an On demand request\n";
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\'";	
	&Download_DB::Execute($dbh,$onDemand_Update_Query);
}

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;
my $MaxCount=0;
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details_Format8.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Proxy = $Config->{$Council_Code}->{'Proxy'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Host1 = $Config->{$Council_Code}->{'Host1'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $FILTER_URL1 = $Config->{$Council_Code}->{'FILTER_URL1'};
my $FILTER_URL2 = $Config->{$Council_Code}->{'FILTER_URL2'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_NUMBER1 = $Config->{$Council_Code}->{'FORM_NUMBER1'};
my $FORM_REFERENECE = $Config->{$Council_Code}->{'FORM_REFERENECE'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Docs_URL_Regex = $Config->{$Council_Code}->{'Docs_URL_Regex'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_Filter_URL = $Config->{$Council_Code}->{'Doc_Download_Filter_URL'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Redir_Url = $Config->{$Council_Code}->{'Redir_Url'};
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $All_Page_Name = $Config->{$Council_Code}->{'All_Page_Name'};
my $All_Page_Name_Value = $Config->{$Council_Code}->{'All_Page_Name_Value'};
my $Doc_URL_BY_Regex = $Config->{$Council_Code}->{'Doc_URL_BY_Regex'};
my $Accept_Name = $Config->{$Council_Code}->{'Accept_Name'};
my $Accept_Value = $Config->{$Council_Code}->{'Accept_Value'};
my $Search_Type = $Config->{$Council_Code}->{'Search_Type'};
my $Search_POST = $Config->{$Council_Code}->{'Search_POST'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};


$Method = "N/A" if($Method eq ""); $Format = "N/A" if($Format eq ""); $Home_URL = "N/A" if($Home_URL eq ""); $Host = "N/A" if($Host eq ""); $Host1 = "N/A" if($Host1 eq ""); $FILTER_URL = "N/A" if($FILTER_URL eq ""); $FILTER_URL1 = "N/A" if($FILTER_URL1 eq ""); $FILTER_URL2 = "N/A" if($FILTER_URL2 eq ""); $Content_Type = "N/A" if($Content_Type eq ""); $FORM_NUMBER = "N/A" if($FORM_NUMBER eq ""); $FORM_NUMBER1 = "N/A" if($FORM_NUMBER1 eq ""); $FORM_REFERENECE = "N/A" if($FORM_REFERENECE eq ""); $Referer = "N/A" if($Referer eq ""); $POST_URL = "N/A" if($POST_URL eq ""); $Doc_URL_Regex = "N/A" if($Doc_URL_Regex eq ""); $Docs_URL_Regex = "N/A" if($Docs_URL_Regex eq "");
$Landing_Page_Regex = "N/A" if($Landing_Page_Regex eq ""); $Viewstate_Regex = "N/A" if($Viewstate_Regex eq ""); $Viewstategenerator_Regex = "N/A" if($Viewstategenerator_Regex eq ""); $Eventvalidation_Regex = "N/A" if($Eventvalidation_Regex eq ""); $Block_Regex1 = "N/A" if($Block_Regex1 eq ""); $Block_Regex2 = "N/A" if($Block_Regex2 eq ""); $Block_Regex3 = "N/A" if($Block_Regex3 eq ""); $Doc_Published_Date_Index = "N/A" if($Doc_Published_Date_Index eq ""); $Doc_Download_URL_Replace = "N/A" if($Doc_Download_URL_Replace eq ""); $Doc_Type_Index = "N/A" if($Doc_Type_Index eq ""); $Doc_Description_Index = "N/A" if($Doc_Description_Index eq "");
$Doc_Download_URL_Index = "N/A" if($Doc_Download_URL_Index eq ""); $Doc_Download_URL_Regex = "N/A" if($Doc_Download_URL_Regex eq ""); $Doc_Download_Filter_URL = "N/A" if($Doc_Download_Filter_URL eq ""); $Total_No_Of_Pages_Regex = "N/A" if($Total_No_Of_Pages_Regex eq ""); $Available_Doc_Count_Regex = "N/A" if($Available_Doc_Count_Regex eq ""); $Doc_URL_BY = "N/A" if($Doc_URL_BY eq ""); $Pagination_Type = "N/A" if($Pagination_Type eq ""); $Next_Page_Regex = "N/A" if($Next_Page_Regex eq ""); $Next_Page_Replace_Info = "N/A" if($Next_Page_Replace_Info eq ""); $Next_Page_Replace_Key = "N/A" if($Next_Page_Replace_Key eq "");
$Redir_Url = "N/A" if($Redir_Url eq ""); $Doc_Download_URL_ABS = "N/A" if($Doc_Download_URL_ABS eq ""); $SSL_VERIFICATION = "N/A" if($SSL_VERIFICATION eq ""); $All_Page_Name = "N/A" if($All_Page_Name eq ""); $All_Page_Name_Value = "N/A" if($All_Page_Name_Value eq ""); $Doc_URL_BY_Regex = "N/A" if($Doc_URL_BY_Regex eq ""); $Accept_Name = "N/A" if($Accept_Name eq ""); $Accept_Value = "N/A" if($Accept_Value eq ""); $Search_Type = "N/A" if($Search_Type eq ""); $Search_POST = "N/A" if($Search_POST eq ""); $Post_Content = "N/A" if($Post_Content eq "");


my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;


my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	# print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	
	print "Format_ID :: $Format_ID \n";
	
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code,$Available_Doc_Count);
	my $Landing_Page_Flag=0;
		
	# my $Download_Location = $Local_Download_Location."/$Format_ID";

	($Doc_Details_Hash_Ref, $Document_Url, $Code,$Available_Doc_Count)=&Extract_Link($Home_URL,$Document_Url,$URL,\%Nextpage_Replace_Hash,$Council_Code,$Application_No);
	print "Extract_Link Code::$Code\n";
	
	$Site_Status = $Code;	
	
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;
	
	$Todays_Count = $Todays_Count + $pdfcount;
	if($Available_Doc_Count=~m/^\s*$/is)
	{
		$Available_Doc_Count = $pdfcount;
	}
	# print "Markup<==>$Markup\n"; 	
	
	my $RetrieveMD5=[];
	my $No_Of_Doc_Downloaded=0;
	# if(lc($Markup) eq 'large')
	# {
		# print "$pdfcount<==>$NO_OF_DOCUMENTS\n"; 
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;
	# print "Temp_Download_Location=>$Temp_Download_Location\n";
	
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====>$Temp_Download_Location\n";
	
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		
		$Doc_Type = &Download_Utility::Clean($Doc_Type);
		$Pdf_Name = &Download_Utility::Clean($Pdf_Name);
		$Doc_Published_Date = &Download_Utility::Clean($Doc_Published_Date);
		$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
		
		# print "Pdf_Name<;;;;;;;;>$Pdf_Name"; 
		# print "Document_Url<;;;;;;;;>$Document_Url"; 
		# print "pdf_url<;;;;;;;;>$pdf_url"; 
		
		$Pdf_Name = &Download_Utility::PDF_Name($Pdf_Name);
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				my ($D_Code, $Err_Msg, $MD5);
				if($Council_Code=~m/^(112|8|236|308|385|376|148|459|461|316|342|158|439|425|292|284|120|280|247|230|133)$/is)
				{	
					print "Document_Url==$Document_Url\n"; 
					if(($Council_Code=~m/^(342|158|439|425|120)$/is) && ($Document_Url eq ""))
					{
						$Document_Url = $URL;
					}
					if($Council_Code=~m/^284$/is)
					{	
						my ($docPageContent,$Code) = &Download_Utility::docMechMethod($pdf_url,$pageFetch);
						$pdf_url="https://documents.newport.gov.uk/PublicAccess_LIVE/Document/ViewDocument" if($Council_Code=~m/^284$/is);
					}
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
				}
				else
				{
					if($Council_Code=~m/^274$/is)
					{	
						my ($docPageContent,$Code) =&Download_Utility::Getcontent($pdf_url);
						$pdf_url="https://npaedms.milton-keynes.gov.uk/PublicAccess_CorpLive/Document/ViewDocument";
					}
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				}
				
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
				
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5);
					if($Council_Code=~m/^(112|8|308|236|385|376|148|459|461|316|342|158|439|425|292|284|120|280|247|230|133)$/is)
					{						
						if(($Council_Code=~m/^(342|158|439|425|120)$/is) &&($Document_Url eq ""))
						{
							$Document_Url = $URL;
						}
						if($Council_Code=~m/^284$/is)
						{	
							my ($docPageContent,$Code) = &Download_Utility::docMechMethod($pdf_url,$pageFetch);
							$pdf_url="https://documents.newport.gov.uk/PublicAccess_LIVE/Document/ViewDocument" if($Council_Code=~m/^284$/is);
						}
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
					}
					else
					{
						if($Council_Code=~m/^274$/is)
						{	
							my ($docPageContent,$Code) =&Download_Utility::Getcontent($pdf_url);
							$pdf_url="https://npaedms.milton-keynes.gov.uk/PublicAccess_CorpLive/Document/ViewDocument";
						}
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					}
					
					my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";
					$Insert_Document.=$Insert_Document_List;
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
							
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
				}
				
			}
		}	
		
		#--------- DB INSERTION above 1000 record ---------------
		$MaxCount++;
		if($MaxCount == 995)
		{
			($MaxCount, $Insert_Document, $MD5_Insert_Query, $Update_Query) = QueryExecuteMax($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
		}
	}	
	
	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	&Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:	
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
	}	
	# else
	# {
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }
	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if(($One_App_Name=~m/^\s*$/is) && ($pdfcount != 0))
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	elsif($pdfcount != 0)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
			
	}	
	if ($onDemand eq 'ApprovedDecision')
	{
		my $Flag_Update_query = "update GLENIGAN..TBL_APPROVED_LARGE_PROJECT set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\', All_Documents_Downloaded = 'Y' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
		$ApprovedDecisionUpdate_query.= $Flag_Update_query;
	}	
	$pdfcount_inc=1;
	
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

print "\n$Insert_OneAppLog\n";
print "\n$Insert_Document\n";
print "\n$Update_Query\n";
print "\n$MD5_Insert_Query\n";
print "\n$ApprovedDecisionUpdate_query\n";

$Insert_OneAppLog=~s/\,$//igs;
# print "\nInsert_OneAppLog===============>\n$Insert_OneAppLog\n";
&Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\nInsert_Document===============>\n$Insert_Document\n";
&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\nUpdate_Query===============>\n$Update_Query\n";
&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\nMD5_Insert_Query\n";
# print "\n------------------------------------\n$MD5_Insert_Query\n------------------------------------\n";
&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
&Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);

if($onDemand eq 'Y')
{
	print "Update & Delete This Ondemand request\n";
	print "$No_Of_Doc_Downloaded_onDemand, $Site_Status\n";
	my $onDemandStatus = &Download_Utility::onDemandStatus($No_Of_Doc_Downloaded_onDemand,$Site_Status);
	my $onDemandUpdateQuery = "update tbl_on_demand_download_backup set Status = \'$onDemandStatus\' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and download_id = \'$onDemandID\'";
	print "onDemandUpdateQuery: $onDemandUpdateQuery\n";
	&Download_DB::Execute($dbh,$onDemandUpdateQuery);
	
	my $onDemandDeleteQuery = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and status = 'Inprogress'";	
	&Download_DB::Execute($dbh,$onDemandDeleteQuery);
}


sub Extract_Link()
{
	my $homeURL = shift;
	my $documentURL = shift;
	my $ApplicationURL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $councilCode = shift;
	my $Application_No = shift;
		
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my ($docPageContent,$Code,$Available_Doc_Count,%Doc_Details_Hash);
	my $Landing_Page_Flag=0;
	my $redir_url_444;
	
	# if($Council_Code=~m/^22|25|195|289|333|454|240|375$/is)
	if($Council_Code=~m/^(8|22|25|90|103|112|113|193|195X|236|240|247|275|285|424|289X|332|333|334|375X|376|378|388|439X|444|454|470|218|329)$/is)
	{
		if(($Council_Code eq "8") && ($ApplicationURL!~m/^http\:\/\/planningregister\.opdc\.london\.gov\.uk/is))
		{
			next;
		}
		# elsif($Council_Code eq "348")
		# {
			# $ApplicationURL = $Home_URL;
		# }
		elsif($Council_Code eq "247")
		{
			my ($docPageContent,$Code,$redir_url) =&Download_Utility::Getcontent($documentURL,$URL,$Host,$Content_Type,'',$Proxy,'');
			$redir_url=~s/plaRecord[^>]*?$/Plastandard.aspx/igs;
			$POST_URL=$redir_url;
		}
		elsif($Council_Code eq "444") # this is only to get redirected url.
		{
			(my $docPageContent, my $Code,$redir_url_444) =&Download_Utility::Getcontent($documentURL,$URL,$Host,$Content_Type,'','','');
		}
		
		$documentURL="";
		
	}
	if($Council_Code=~m/^120$/is)
	{
		$documentURL=~s/:443//igs;
	}
	print "documentURL<==>$documentURL\n"; 
	print "Method<==>$Method\n"; 
	Re_Ping:	
	if($documentURL ne '')
	{
		if($Method=~m/GET/is)
		{
			if($Council_Code=~m/^(394|615|168|185|274)$/is)
			{
				($docPageContent,$Code) =&Download_Utility::Getcontent($documentURL,$URL,$Host,$Content_Type,'','','');
				
				if($Council_Code=~m/^274$/is)
				{
					$documentURL=$1 if($docPageContent=~m/$Landing_Page_Regex/is);
					($docPageContent,$Code) =&Download_Utility::Getcontent($documentURL,$URL,$Host,$Content_Type,'','','');
									
					if($docPageContent=~m/This\s*application\s*is\s*no\s*longer\s*available\s*for\s*viewing/is)
					{
						next;
					}
					$docPageContent=$1 if($docPageContent=~m/var\s*model\s*=\s*([^>]*?)\s*;/is);
				}
			}
			else
			{ 
				if($Council_Code=~m/^284$/is)
				{
					$documentURL="https://licensing.newport.gov.uk/online-applications/applicationDetails.do?activeTab=externalDocuments&keyVal=$1" if($documentURL=~m/keyVal=\s*([^>]*?)\s*&activeTab=documents/is);
				}
				print "documentURL<==>$documentURL\n"; 
				($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
				
				$docPageContent=~s/&amp;/&/igs;
				if($Council_Code=~m/^284$/is)
				{
					if($docPageContent=~m/This\s*application\s*is\s*no\s*longer\s*available\s*for\s*viewing/is)
					{
						next;
					}
					$docPageContent=$1 if($docPageContent=~m/var\s*model\s*=\s*([^>]*?)\s*;/is);
					
				}
			}
		}
		elsif($Method=~m/POST/is)
		{
			($docPageContent,$Code) = &Download_Utility::docPostMechMethod($documentURL,$Post_Content,$pageFetch);	
		}
		
	}
	else
	{
		print "Search Application method id processing..(Document URL not found)\n";
		($docPageContent,$Code)=&Search_Application($Application_No,$ApplicationURL);
	}	
		
	if($Council_Code=~m/^348$/is)
	{		
		($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
		
		my $Content2;
		while($docPageContent=~m/<div[^>]*?class=\"document__section__header\"[^>]*?>\s*<label>\s*View\s*([^>]*?)\s*\:\s*<\/label>\s*([\d]+)\s*Documents\s*Found\s*<\/div>/igs)
		{
			my $doc_type=uri_escape($1);
			my $doc_count=uri_escape($2);
			my $post_url="https://www.sedgemoor.gov.uk/planning_online?action=LoadDocuments&t=$doc_count&i=$doc_type";
			
			# $Search_POST=~s/<LENGTH>/$doc_count/igs;
			my $searchposttmp=$Search_POST;
			$searchposttmp=~s/<LENGTH>/20/igs;
			# $Post_Content=~s/([^>]*?\=)(gvDocs[^>]*?)(\&[^>]*?)$/$1$Eventtarget$3/igs;
			# $Post_Content=~s/<VIEWSTATE>/$Viewstate/igs;
			# $Post_Content=~s/<VIEWSTATEGENERATOR>/$Viewstategen/igs;
			# $Post_Content=~s/<EVENTVALIDATION>/$Eventvalidation/igs;
			
			print "post_url::$post_url\n";
			# print "Search_POST::$searchposttmp\n";
			(my $tempcon,$Code) = &Download_Utility::docPostMechMethod($post_url,$searchposttmp,$pageFetch);
			
			$Content2=$Content2.$tempcon;
			if($doc_count > 10)
			{
				my $searchposttmp=$Search_POST;
				$searchposttmp=~s/<LENGTH>/10/igs;
				$searchposttmp=~s/draw=1/draw=2/igs;
				$searchposttmp=~s/start=0/start=10/igs;
				(my $tempcon,$Code) = &Download_Utility::docPostMechMethod($post_url,$searchposttmp,$pageFetch); 
				$Content2=$Content2.$tempcon;
			
			}
			
		}
		my $tempcon="{\"Values\":[$Content2]}";
		$tempcon=~s/\}\{"draw"/\},\{"draw"/igs;
		
		$docPageContent=$tempcon;
		
	}
	if($Council_Code=~m/^449$/is)
	{
		my $planURL = $documentURL;
		
		if($planURL ne "")
		{
			$planURL=~s/^([^<]*?)pa-documents-public.aspx([^<]*?)$/${1}pa-documents-plans-public.aspx$2/gsi;
		}
		else
		{
			$planURL="https://secure.telford.gov.uk/planning/pa-documents-plans-public.aspx?Applicationnumber=$Application_No";
		}
		my ($docContent,$Code) = &Download_Utility::docMechMethod($planURL,$pageFetch);
		
		my $table = $1 if($docContent=~m/<table\s*id=\"tblDocuments[^>]*?>\s*([\w\W]*?)\s*<\/table>/si);
		$table=~s/<td>(.*?)<\/td>(<td>(.*?)<\/td>)<td>[^<]*?<\/td>(<td>(.*?)<\/td>)<td>(.*?)<\/td><td>[^<]*?<\/td><td>[^<]*?<\/td>(<td>[^<]*?<\/td>)/$2$7$4/gsi;
		$docPageContent=~s/(<\/tr>\s*<\/table>\s*)/${table}$1/gsi;
	}
	
	
	if($docPageContent=~m/<[^>]*?=\"$Accept_Name\"[^>]*?>I\s*agree\s*to\s*this\s*statement\s*<\//is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
		
	}
	elsif($docPageContent=~m/>\s*I\s*agree\s*<\//is)
	{
		my $appNo = $1 if($docPageContent=~m/<h3>Downloads\s*for\s*this\s*application<\/h3>\s*<form[^>]*?planningApplication\/([^\"]*?)\"[^>]*?>/is);
		if($councilCode == 240)
		{
			$appNo = $1 if($docPageContent=~m/<a\s*class[^>]*?appID=([^>]*?)\"\s*>\s*Track\s*this\s*application\s*<\/a>/is);
		}	
		$POST_URL=~s/<APPNUM>/$appNo/gsi;
		
			
		($docPageContent,$Code) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$POST_URL=~s/$appNo/<APPNUM>/gsi;
		
	}
	elsif($docPageContent=~m/By\s*clicking\s*the\s*button\s*below\s*you\s*agree\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
	}
	elsif(($docPageContent=~m/Please\s*click\s*the\s*button\s*below\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is) && ($Council_Code!~m/^(168)$/is)) # for 158,439,425,428
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
		$Landing_Page_Flag = 1;
		
	}
	elsif($docPageContent=~m/<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*(?:<[^>]*?>)?\s*Related\s*Documents\s*</is) # for 533
	{
		my $DocUrl=$1;
		$DocUrl=~s/\&amp\;/\&/gsi;
		print "DocUrl: $DocUrl\n";
		my $u1=URI::URL->new($DocUrl,$documentURL);
		$documentURL=$u1->abs;
		($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
	}
	elsif($docPageContent=~m/<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*(?:<[^>]*?>)?\s*view[^>]*?Documents\s*</is) # for 444,333
	{
		my $DocUrl=$1;
		$DocUrl=~s/\&amp\;/\&/gsi;
		print "before DocUrl: $DocUrl\n";
		
		$documentURL = URI::URL->new($DocUrl)->abs( $Home_URL, 1 );
		
		print "After DocUrl: $documentURL\n";
		# <STDIN>;
		
		
		
		($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
		if($Council_Code=~m/^333$/i)
		{
			if($docPageContent=~m/<[^>]*?=\"$Accept_Name\"[^>]*?>I\s*agree\s*to\s*this\s*statement\s*<\//is)
			{
				$pageFetch->form_number($FORM_NUMBER);
				$pageFetch->set_fields( $Accept_Name => $Accept_Value );
				$pageFetch->click();
				
				print "Welcome\n";
						
				$docPageContent = $pageFetch->content;
				$Code= $pageFetch->status();
			
				$Landing_Page_Flag = 1;
				
			}
		}
	}
	elsif($Council_Code=~m/^(615)$/is)
	{
		if($docPageContent=~m/<form action=\"(\/Disclaimer\/Accept[^\"]*?)\"\s*method=\"post\">[\w\W]*?<input[^>]*?value=\"Accept\"/is)
		{
			my $acceptURL = $1;
			$acceptURL=~s/\&amp\;/\&/gsi;
			my $u1=URI::URL->new($acceptURL,$Home_URL);
			$documentURL=$u1->abs;
			
			print "Getcontent==>$documentURL\n";
			
			($docPageContent,$Code) =&Download_Utility::Getcontent($documentURL,$URL,$Host,$Content_Type,'','','');
			
		}
	}
	elsif($Council_Code=~m/^(168)$/is)
	{
		if($docPageContent=~m/<form\s*action=\"(\/Disclaimer\/Accept[^\"]*?)\"\s*method=\"post\">[\w\W]*?<button[^>]*?value=\"Agree\"/is)
		{
			my $acceptURL = $1;
			$acceptURL=~s/\&amp\;/\&/gsi;
			my $u1=URI::URL->new($acceptURL,$Home_URL);
			$documentURL=$u1->abs;
			
			print "Getcontent==>$documentURL\n";
			
			($docPageContent,$Code) =&Download_Utility::Getcontent($documentURL,$URL,$Host,$Content_Type,'','','');
			
		}
	}
	
	if($Council_Code == 280)
	{
		if($docPageContent=~m/<frame\s*src\s*=\s*\'([^>]*?)\'\s*name[^>]*?/is)
		{
			my $DocUrl=$1;
			my $u1=URI::URL->new($DocUrl,$documentURL);
			$documentURL=$u1->abs;
			print "documentURL: $documentURL\n";
			($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
			if($docPageContent=~m/<frame\s*src\s*=\s*\'([^>]*?)\'\s*name[^>]*?/is)
			{
				my $DocUrl=$1;
				my $u1=URI::URL->new($DocUrl,$documentURL);
				$documentURL=$u1->abs;
				print "documentURL: $documentURL\n";
				($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
			}
		}
	}
	
	
	my %Doc_Details;
	if($Landing_Page_Flag == 0)
	{
		if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
		{
			print "Application Number==>$Application_No\n";
			
			if($docPageContent=~m/$Landing_Page_Regex/is)
			{
				my $DocUrl=$1;
				$DocUrl=~s/\&amp\;/\&/gsi;
				
				# print "DocUrl: $DocUrl\n";
				my $u1=URI::URL->new($DocUrl,$documentURL);
				$documentURL=$u1->abs;
				
				if($Council_Code=~m/^375|112$/is)
				{
					$documentURL = $FILTER_URL1.$documentURL if($documentURL!~m/^http/is);
				}
				elsif($Council_Code=~m/^280$/is)
				{
					$documentURL = "http://planning.westberks.gov.uk/rpp/searchresult.asp?appNumber=$Application_No";
				}
				elsif($Council_Code=~m/^179$/is)
				{
					$documentURL =~s/\&amp\;/\&/gsi;
					$documentURL =~s/^https/http/gsi;
				}
				else
				{
					$documentURL = $FILTER_URL.$documentURL if($documentURL!~m/^http/is);
				}
				print "Document_Url: $documentURL\n";
				$Landing_Page_Flag = 1;
				goto Re_Ping;
			}	
		}
		elsif($Council_Code=~m/^103$/is)
		{
			my $ScrollPos = $1 if($docPageContent=~m/<input[^>]*?name=\"ScrollPos\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $LinkState = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"LinkState\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $APPNUM = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?txtRefno\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $VIEWSTATEZIP = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"__VIEWSTATEZIP\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $VIEWSTATE = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $Link = $1 if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?Link\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $ApplicationType = $1 if($docPageContent=~m/<textarea[^>]*?name=\"[^\"]*?ApplicationType\"[^>]*?>\s*([^<]*?)\s*<\/textarea>/is);
			my $ReceivedDate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?ReceivedDate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $ValidDate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?ValidDate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $SiteAddress = $1 if($docPageContent=~m/<textarea[^>]*?name=\"[^\"]*?SiteAddress\"[^>]*?>\s*([^<]*?)\s*<\/textarea>/is);
			my $Eastings = $1 if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?Eastings\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $Northings = $1 if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?Northings\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $Ward = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?Ward\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $Parish = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?Parish\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $Decision = $1 if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?Decision\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $DecisionNoticeSentDate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?DecisionNoticeSentDate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $CommitteeDate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?CommitteeDate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $CommitteeType = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?CommitteeType\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $TargetDate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?TargetDate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $PublicConsultStart = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?PublicConsultStart\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $PublicConsultEnd = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?PublicConsultEnd\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $ApplicantName = $1 if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?ApplicantName\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $ApplicantAddress = $1 if($docPageContent=~m/<textarea[^>]*?name=\"[^\"]*?ApplicantAddress\"[^>]*?>\s*([^<]*?)\s*<\/textarea>/is);
			my $AgentName = $1 if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?AgentName\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $AgentAddress = $1 if($docPageContent=~m/<textarea[^>]*?name=\"[^\"]*?AgentAddress\"[^>]*?>\s*([^<]*?)\s*<\/textarea>/is);
			my $Proposal = $1 if($docPageContent=~m/<textarea[^>]*?name=\"[^\"]*?Proposal\"[^>]*?>\s*([^<]*?)\s*<\/textarea>/is);
			my $inspectorateref = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?inspectorateref\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $inspectoratedate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?inspectoratedate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $appealtype = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?appealtype\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $appeallodged = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?appeallodged\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $questionnairesentdate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?questionnairesentdate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $questionnaireduedate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?questionnaireduedate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $appealofficer = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?appealofficer\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $appealdecision = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?appealdecision\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $appealdecisiondate = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?appealdecisiondate\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $VIEWSTATEGENERATOR = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			my $EVENTVALIDATION = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"[^\"]*?EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
			$ApplicationType=~s/\s/\+/gsi;
			$SiteAddress=~s/\s/\+/gsi;
			$Decision=~s/\s/\+/gsi;
			$ApplicantName=~s/\s/\+/gsi;
			$ApplicantAddress=~s/\s/\+/gsi;
			$AgentName=~s/\s/\+/gsi;
			$AgentAddress=~s/\s/\+/gsi;
					
			my $docPostContent = "EasySitePostBack=&ScrollPos=<ScrollPos>&LinkState=<LinkState>&__VIEWSTATEZIP=<VIEWSTATEZIP>&__VIEWSTATE=<VIEWSTATE>&PageNotifications%24hfState=&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtLink=<Link>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtRefno=<APPNUM>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtApplicationType=<ApplicationType>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtReceivedDate=<ReceivedDate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtValidDate=<ValidDate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtSiteAddress=<SiteAddress>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtEastings=<Eastings>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtNorthings=<Northings>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtWard=<Ward>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtParish=<Parish>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtDecision=<Decision>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtDecisionNoticeSentDate=<DecisionNoticeSentDate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtCommitteeDate=<CommitteeDate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtCommitteeType=<CommitteeType>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtTargetDate=<TargetDate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtPublicConsultStart=<PublicConsultStart>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtPublicConsultEnd=<PublicConsultEnd>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtApplicantName=<ApplicantName>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtApplicantAddress=<ApplicantAddress>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtAgentName=<AgentName>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtAgentAddress=<AgentAddress>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtProposal=<Proposal>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtinspectorateref=<inspectorateref>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtinspectoratedate=<inspectoratedate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtappealtype=<appealtype>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtappeallodged=<appeallodged>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtquestionnairesentdate=<questionnairesentdate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtquestionnaireduedate=<questionnaireduedate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtappealofficer=<appealofficer>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtappealdecision=<appealdecision>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24txtappealdecisiondate=<appealdecisiondate>&espr%24renderHost%24PageStructureDisplayRenderer%24esctl_3615ccbc-95af-405a-b551-38876b2d66b5%24ctl00%24btnAttachments=View+Attachments&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>";
			
			$docPostContent=~s/<ScrollPos>/$ScrollPos/gsi;
			$docPostContent=~s/<LinkState>/$LinkState/gsi;
			$docPostContent=~s/<VIEWSTATEZIP>/$VIEWSTATEZIP/gsi;
			$docPostContent=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
			$docPostContent=~s/<Link>/$Link/gsi;
			$docPostContent=~s/<APPNUM>/$APPNUM/gsi;
			$docPostContent=~s/<ApplicationType>/$ApplicationType/gsi;
			$docPostContent=~s/<ReceivedDate>/$ReceivedDate/gsi;
			$docPostContent=~s/<ValidDate>/$ValidDate/gsi;
			$docPostContent=~s/<SiteAddress>/$SiteAddress/gsi;
			$docPostContent=~s/<Eastings>/$Eastings/gsi;
			$docPostContent=~s/<Northings>/$Northings/gsi;
			$docPostContent=~s/<Ward>/$Ward/gsi;
			$docPostContent=~s/<Parish>/$Parish/gsi;
			$docPostContent=~s/<Decision>/$Decision/gsi;
			$docPostContent=~s/<DecisionNoticeSentDate>/$DecisionNoticeSentDate/gsi;
			$docPostContent=~s/<CommitteeDate>/$CommitteeDate/gsi;
			$docPostContent=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/gsi;
			$docPostContent=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
			$docPostContent=~s/<appealdecisiondate>/$appealdecisiondate/gsi;
			$docPostContent=~s/<appealdecision>/$appealdecision/gsi;
			$docPostContent=~s/<appealofficer>/$appealofficer/gsi;
			$docPostContent=~s/<questionnaireduedate>/$questionnaireduedate/gsi;
			$docPostContent=~s/<questionnaireduedate>/$questionnaireduedate/gsi;
			$docPostContent=~s/<questionnairesentdate>/$questionnairesentdate/gsi;
			$docPostContent=~s/<appeallodged>/$appeallodged/gsi;
			$docPostContent=~s/<appealtype>/$appealtype/gsi;
			$docPostContent=~s/<inspectoratedate>/$inspectoratedate/gsi;
			$docPostContent=~s/<inspectorateref>/$inspectorateref/gsi;
			$docPostContent=~s/<Proposal>/$Proposal/gsi;
			$docPostContent=~s/<AgentAddress>/$AgentAddress/gsi;
			$docPostContent=~s/<AgentName>/$AgentName/gsi;
			$docPostContent=~s/<ApplicantAddress>/$ApplicantAddress/gsi;
			$docPostContent=~s/<ApplicantName>/$ApplicantName/gsi;
			$docPostContent=~s/<PublicConsultEnd>/$PublicConsultEnd/gsi;
			$docPostContent=~s/<PublicConsultStart>/$PublicConsultStart/gsi;
			$docPostContent=~s/<TargetDate>/$TargetDate/gsi;
			$docPostContent=~s/<CommitteeType>/$CommitteeType/gsi;
			
			$ApplicationURL=~s/\/weekly-list\/weekly-details\/\?Ref=/\/application-search\/\?ref=/gsi;
			my $PostURL=$ApplicationURL;
			$PostURL=~s/http\:/https\:/igs;
			my ($searchPageCon,$searchCode) = &Download_Utility::docPostMechMethod($PostURL,$docPostContent,$pageFetch);
			$docPageContent=$searchPageCon;
		}	
	}	
	
	
	my ($Total_no_Pages);
	if($Total_No_Of_Pages_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_no_Pages=$1;
		}
		print "Total_no_Pages: $Total_no_Pages\n";
	}

	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
		
		print "Available_Doc_Count<==>$Available_Doc_Count\n"; 
		# 	
	}
	
	if($Council_Code=~m/^342$/is)
	{
		my $NXT_BTN_NME = $1 if($docPageContent=~m/<select[^>]*?PageSize\"\s*name=\"([^\"]*?)\"[^>]*?>/is);
		my $NXT_BTN_VAL = $1 if($docPageContent=~m/<option[^>]*?value=\"([^\"]*?)\">\s*All\s*<\/option>/is);

		$pageFetch-> submit_form(
				form_number => $FORM_NUMBER,
				fields      => {
					$NXT_BTN_NME	=> $NXT_BTN_VAL,
					},
				);
		
		my $Page_url = $pageFetch->uri();
	
		my $searchPageCon = $pageFetch->content;
		my $searchCode= $pageFetch->status();
				
		$docPageContent = $searchPageCon;

	}
	
	my $page_inc=1;	
	
	Next_Page:
	if($Format eq 'Table')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($docPageContent,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$documentURL,$ApplicationURL,$Doc_URL_BY);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	elsif($Format eq 'JSON')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_JsonDocs_Details($docPageContent,$Application_No);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	else
	{
		my $Doc_Details_Hash_Ref_tmp=&Direct_Docs_Collect_Details($docPageContent);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}

	if($Pagination_Type eq 'N/A')
	{
		return (\%Doc_Details_Hash,$documentURL,$Code);
	}
	elsif($Pagination_Type eq 'By Number')
	{		
		
		if($page_inc < $Total_no_Pages)
		{		
			print "page_inc	: $page_inc\n";
			print "Total_no_Pages	: $Total_no_Pages\n";
			# 
			
			my ($Next_page_link,$Next_Page_Values);
			my @Next_Page_Values = $docPageContent=~m/$Next_Page_Regex/g;
			
			if($Next_Page_Replace_Info ne 'N/A')
			{
				my @Replace_array_Raw=split('\|',$Next_Page_Replace_Info);
				
				$Next_page_link = $Redir_Url if($Replace_array_Raw[0]=~m/Redir_Url/is);
								
				if($Replace_array_Raw[2]=~m/AllValues/is)
				{
					$Next_Page_Values = join("",@Next_Page_Values);
				}	

				
				if($Replace_array_Raw[1]=~m/Replace/is)
				{
					for my $Replace_Key(keys %Nextpage_Replace_Hash)
					{
						if($Nextpage_Replace_Hash{$Replace_Key} eq '')
						{
							$Next_page_link=~s/$Replace_Key/$Next_Page_Values/igs;
						}	
						else
						{
							$Next_page_link=~s/$Replace_Key/$Nextpage_Replace_Hash{$Replace_Key}/igs;
						}
					}
				}
				print "Next_page_link: $Next_page_link\n"; 
			}
						
			if($Method=~m/GET/is)
			{
				($docPageContent,$Code) = &Download_Utility::docMechMethod($Next_page_link,$pageFetch);
			}	
			
			
			$page_inc++;
		
			goto Next_Page;
		}	
	}
	elsif($Pagination_Type eq 'Mech Number')
	{
		if($page_inc < $Total_no_Pages)
		{		
			print "page_inc	: $page_inc\n";
			print "Total_no_Pages	: $Total_no_Pages\n";
			# 
			
			$page_inc++;
			
			my $nextPageLink;
			if($Council_Code=~m/^236$/is)
			{
				my $tempURL=$FILTER_URL."xappndocs.asp?pageNumber=".$page_inc."&iappID=".$Application_No."&"; 
				$nextPageLink=$tempURL;
			}
			elsif($Council_Code=~m/^444$/is)
			{
				my $tempURL=$redir_url_444.'?grdResultsP='.$page_inc; 
				$nextPageLink=$tempURL;
			}
			
			print "$nextPageLink\n";	
			
			(my $searchPageCon,$Code) = &Download_Utility::docMechMethod($nextPageLink,$pageFetch);	
			$docPageContent=$searchPageCon;			
						
			
			goto Next_Page;
		}
	}
	elsif($Pagination_Type eq 'Purbeck')
	{		
		if($page_inc < $Total_no_Pages)
		{		
			print "page_inc	: $page_inc\n";
			print "Total_no_Pages	: $Total_no_Pages\n";
			
			my $nextPage = $1 if($docPageContent=~m/<input[^>]*?name=\"([^\"]*?)\"[^>]*?title=\"Next\s*Page\"[^>]*?>/is);
			
			$page_inc++;
			
			$pageFetch->form_number($FORM_NUMBER);
			eval{$pageFetch->click( $nextPage );};
					
			
			my $Page_url = $pageFetch->uri();			
			my $searchPageCon = $pageFetch->content;
			my $searchCode= $pageFetch->status();
			
			$docPageContent = $searchPageCon;
			$Code = $searchCode;
			
			print "Click response: $Code\n";
			
			
			goto Next_Page;
		}
	}
	elsif($Pagination_Type eq 'By Format')
	{
		if($page_inc < $Total_no_Pages)
		{		
			print "page_inc	: $page_inc\n";
			print "Total_no_Pages	: $Total_no_Pages\n";
			# 
			
			$page_inc++;
			
			my $nextPage = $1 if($docPageContent=~m/<input\s*name=\"([^\"]*?)\"[^>]*?onclick=\"Navigate\(\'Results\.aspx\'\s*\+\s*[^>]*?\s*\'\?grdResultsP=\'\s*[^>]*?\s*\+\s*$page_inc\)\"\s*value=\"$page_inc\"[^>]*?>/is);
			
			my $nextPageLink;
			if($Council_Code=~m/^334$/is)
			{
				$nextPageLink="http://documents.runnymede.gov.uk/AniteIM.WebSearch/Results.aspx?grdResultsP=".$page_inc;
			}
			else
			{						
				$nextPageLink = $pageFetch->find_link( text => $nextPage );		
				$nextPageLink = $pageFetch->uri();
				print "nextLink=>$nextPageLink\n";	
			
				$nextPageLink=$nextPageLink."\?grdResultsP=".$page_inc;
			}
			# print "$indi\n";	
			
			(my $searchPageCon,$Code) = &Download_Utility::docMechMethod($nextPageLink,$pageFetch);	
			$docPageContent=$searchPageCon;	
			print "nextPageLink=>$nextPageLink\n";	
			if($Council_Code!~m/^334$/is)
			{
				$nextPageLink=~s/\?grdResultsP=\d+\s*$//gsi;
			}
			
			
			print "$nextPageLink\n";	
			
			goto Next_Page;
		}
	}
	elsif($Pagination_Type eq 'Tandridge')
	{			
		if($docPageContent=~m/<td\s*style\s*=\s*\"\s*font-weight\:bold\;\s*\"\s*>\s*<a\s*href[^>]*?>[\d]+<\/a>\s*<\/td>\s*<td>\s*<a\s*href[^>]*?>([\d]+)<\/a>/is)
		{		
			my $currentPage = $1;
			print "page_inc	: $page_inc\n";
			print "currentPage	: $currentPage\n";
			
			my $ViewState = uri_escape($1) if($docPageContent=~m/<input[^>]*?ViewState\"\s*value=\"([^\"]*?)\"[^>]*?>/is);
			my $CSRFToken = uri_escape($1) if($docPageContent=~m/<input[^>]*?CSRFToken\"\s*value=\"([^\"]*?)\"[^>]*?>/is);
			
			$page_inc++;
			
			# $pageFetch->form_number($FORM_NUMBER);
			
			my $postCont = "javax.faces.ViewState=$ViewState&_id49_SUBMIT=1&Civica.CSRFToken=$CSRFToken&_id49%3A_idcl=_id49%3Ascroll_1next&_id49%3A_link_hidden_=&orderby=&_id49_SUBMIT=&_id49%3Ascroll_1=next";	
			print "postCont::$postCont\n";
			print "documentURL::$documentURL\n";

			$pageFetch->add_header( "Accept-Encoding" => "gzip, deflate" );
			$pageFetch->add_header( "Accept-Language" => "en-US,en;q=0.9" );
			$pageFetch->add_header( "Referer" => "$documentURL" );
			$pageFetch->add_header( "Host" => "tdccomweb.tandridge.gov.uk" );
			$pageFetch->add_header( "Origin" => "http://tdccomweb.tandridge.gov.uk" );

			
			$pageFetch->post('http://tdccomweb.tandridge.gov.uk/Planning/lg/GFPlanningDocuments.page', Content => "$postCont");			
			my $searchPageCon = $pageFetch->content;
			my $searchCode= $pageFetch->status();
			
			$docPageContent = $searchPageCon;
			$Code = $searchCode;
			
			print "Click response: $Code\n";
			
			
			goto Next_Page;
		}
	}
	elsif($Pagination_Type eq 'By Click')
	{		
		$pageFetch-> submit_form(
				form_number => $FORM_NUMBER,
				fields      => {
					$All_Page_Name	=> $All_Page_Name_Value,
					},
				);
		
		
		my $Page_url = $pageFetch->uri();
		# print "$Page_url\n";
		my $searchPageCon = $pageFetch->content;
		my $searchCode= $pageFetch->status();
		
		$docPageContent = $searchPageCon;
		$Code = $searchCode;
		
		print "Click response: $Code\n";
		
	
	}
	
	print "Code::=================>$Code\n";
	return (\%Doc_Details_Hash,$documentURL,$Code,$Available_Doc_Count);
}


sub Collect_JsonDocs_Details()
{
	my $Content=shift;
	my $Application_No=shift;
	my %Doc_Details;
	
		
	if($Content=~m/<\!doctype|<html|var\s*model\s*=\s*(\{\"FlexibleColumns\"[\w\W]*?\})\;/is)
	{
		$Content= $1 if($Content=~m/var\s*model\s*=\s*(\{\"CompleteDocument\"[\w\W]*?\"RowCount\"\:\"[^>]*?\"\})/is);
		$Content= $1 if($Content=~m/var\s*model\s*=\s*(\{\"FlexibleColumns\"[\w\W]*?\})\;/is);
		
	}
	else
	{
		$Content= $Content;
	}
	my ($Doc_URL,$Doc_Desc,$pdf_name,$DateReceived,$document_type);
	my $json = JSON->new;
	
	eval{
		my $data = $json->decode($Content);
		if($Council_Code=~m/^284|274|112|230|268|232$/is)
		{
			my @navigation_menus=@{$data->{'Rows'}};
			foreach my $Keys ( @navigation_menus) 
			{
				# print "Dumper:: $Keys\n";
				$DateReceived = '';
				$document_type = $Keys->{'Doc_Type'};
				$Doc_Desc = $Keys->{'Doc_Ref2'};			
				my $guid = $Keys->{'Guid'};
				$DateReceived = $Keys->{'Date_Received'};				
				
				$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
				$document_type = &Download_Utility::Clean($document_type);
				$DateReceived = &Download_Utility::Clean($DateReceived);
				my $Doc_URL="https://documents.newport.gov.uk/PublicAccess_LIVE/Document/GetDocumentDetails?id=$guid&_=" if($Council_Code=~m/^284$/is);
				
				$Doc_URL="https://npaedms.milton-keynes.gov.uk/PublicAccess_CorpLive/Document/GetDocumentDetails?id=$guid&_=" if($Council_Code=~m/^274$/is);

				$Doc_URL="http://planningdms-live.blackburn.gov.uk/PublicAccess_LIVE/Document/ViewDocument?id=$guid" if($Council_Code=~m/^112$/is);
				
				$Doc_URL="https://publicdocuments.hinckley-bosworth.gov.uk/PublicAccess_LIVE/Document/ViewDocument?id=$guid" if($Council_Code=~m/^230$/is);
				
				$Doc_URL="https://enterprisedocs.publicaccess.merthyr.gov.uk:8443/PublicAccess_LIVE/Document/ViewDocument?id=$guid" if($Council_Code=~m/^268$/is);
				
				$Doc_URL="https://iawpa.horsham.gov.uk/PublicAccess_LIVE/Document/ViewDocument?id=$guid" if($Council_Code=~m/^232$/is);
				print "Doc_URL:: $Doc_URL\n";
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
							
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				$pdfcount_inc++;
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "document_type: $document_type\n";
				print "Doc_Desc: $Doc_Desc\n";
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
			}
		}
		if($Council_Code=~m/^(285|424|185|378|329)$/is)
		{
			my @KeyObjects = @{$data->{'CompleteDocument'}};
			
			foreach my $Keys ( @KeyObjects) 
			{	
				$Doc_Desc = $Keys->{'DocDesc'};
				$document_type = $Keys->{'Title'};
				my $DocNo = $Keys->{'DocNo'};
				$DateReceived = $Keys->{'DocDate'};
				
				
				my $tempURL = $FILTER_URL1;
				
				$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
				$tempURL=~s/<DOCNUM>/$DocNo/gsi;
				$Doc_URL = $tempURL;	
				$document_type = &Download_Utility::trim($document_type);			
				$Doc_Desc=~s/\//\-/gsi;
				$document_type=~s/\//\-/gsi;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				
				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
									
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				# 
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
				
			}
		}
		elsif($Council_Code=~m/^332$/is)
		{
			next if($Content=~m/\{\"data\"\:null\}/is);
			
			my @KeyObjects = $data->{'result'};
			
			foreach my $NewData ( @KeyObjects) 
			{	
				my @NewDataSet = $NewData->{'NewDataSet'};
				
				foreach my $Data ( @NewDataSet) 
				{
					my @NewData = @{$Data->{'data1'}};
				
					foreach my $Keys ( @NewData) 
					{
						$Doc_Desc = $Keys->{'Description'};
						$document_type = $Keys->{'FileName'};
						my $DocNo = $Keys->{'ID_PhysicalDoc'};
						$DateReceived = $Keys->{'DateCreated'};
											
						my $tempURL = $FILTER_URL1;
						
						$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
						$tempURL=~s/<APPNUM>/$Application_No/gsi;
						$tempURL=~s/<DOCNUM>/$DocNo/gsi;
						$Doc_URL = $tempURL;	
						$document_type = &Download_Utility::trim($document_type);
						$Doc_Desc=~s/\//\-/gsi;
						
						my ($file_type);
						if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
						{
							$file_type=$1;
						}
						else
						{
							$file_type='pdf';
						}	
						
						
						if($Doc_Desc!~m/^\s*$/is)
						{
							$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
						}	
						else
						{
							$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
						}
						
						$pdf_name=~s/\x{e2}\x{80}\x{93}/\-/igs;
											
						$pdfcount_inc++;
						
						print "Doc_URL	: $Doc_URL\n";
						print "pdf_name	: $pdf_name\n";
						print "file_type: $file_type\n";
						print "DateReceived: $DateReceived\n";
						# 
						
						if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
						{
							$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
						}
					}				
				}
			}
		}
		elsif($Council_Code=~m/^133$/is)
		{
			my @KeyObjects = @{$data->{'Documents'}};
			foreach my $Keys ( @KeyObjects) 
			{							
				$Doc_Desc = $Keys->{'DocType'};
				$document_type = $Keys->{'DocRef2'};
				$Doc_URL = $Keys->{'FilePath'};
				$DateReceived = $Keys->{'DateReceived'};
				
				$Doc_URL = $FILTER_URL.$Doc_URL if($Doc_URL!~m/^http/is);	
				
				$document_type=~s/\n+//gsi;
				$document_type=~s/\(([^<]*?)\)/$1/gsi;
				$document_type = &Download_Utility::trim($document_type);
				# print "document_type::$document_type\n"; 
				
				if(($Council_Code=~m/^247$/is) && ($document_type=~m/No Attachments found for this Application/is))
				{
					goto Loop_End;
				}
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}
				
				if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDoc=$1;
					if($file_typeDoc eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDoc;
					}
				}
				elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDocType=$1;
					if($file_typeDocType eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDocType;
					}
				}
				
				
				if($document_type!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}
									
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				# 
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
			}
		}
		elsif($Council_Code=~m/^348$/is)
		{
			my @navigation_menus=@{$data->{'Values'}};
			foreach my $array(@navigation_menus)
			{
				my $ContentDocumentId =$array->{'data'};
				my $data =decode_json($ContentDocumentId);
				foreach my $key(@$data)
				{
					my $URI=$key->{'URI'};
					my $RenditionURI=$key->{'RenditionURI'};
					$DateReceived=$key->{'RegisteredDate'};
					$document_type=$key->{'DocumentType'};
					$Doc_Desc=$key->{'Description'};	
					
					# if($Doc_Desc=~m/^\s*$/is)
					# {
						# $Doc_Desc=$key->{'Consultee'};
					# }
					
					$Doc_URL="https://www.sedgemoor.gov.uk/planning_online?action=GetDocument&uri=$URI&ren=$RenditionURI&ext=pdf&desc=$Doc_Desc";
					$document_type = &Download_Utility::trim($document_type);
					# print "Doc_URL::::$Doc_URL\n";<STDIN>;
					
					my ($file_type);
					if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
					{
						$file_type=$1;
					}
					else
					{
						$file_type='pdf';
					}
					
					if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
					{
						my $file_typeDoc=$1;
						if($file_typeDoc eq $file_type)
						{
							$file_type;
						}
						else
						{
							$file_type=$file_typeDoc;
						}
					}
					elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
					{
						my $file_typeDocType=$1;
						if($file_typeDocType eq $file_type)
						{
							$file_type;
						}
						else
						{
							$file_type=$file_typeDocType;
						}
					}
					
					
					if($document_type!~m/^\s*$/is)
					{
						$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
					}	
					else
					{
						$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
					}
										
					$pdfcount_inc++;
					
					print "Doc_URL	: $Doc_URL\n";
					print "pdf_name	: $pdf_name\n";
					print "document_type: $document_type\n";
					print "Doc_Desc: $Doc_Desc\n";
					print "DateReceived: $DateReceived\n";
					print "file_type: $file_type\n";
					
					if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					{
						$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
					}	
				}
			}
		}
	};
	if($@)
	{
		print "ERR::: $@\n";
	}
	Loop_End:
	return (\%Doc_Details);	
}

sub Direct_Docs_Collect_Details()
{
	my $Content=shift;
	
	
	my %Doc_Details;
	my @urlArray;
	my ($Doc_URL,$Doc_Desc,$pdf_name,$document_type);
	if($Block_Regex1 ne 'N/A')
	{
		my @Block1 = $Content =~m/$Block_Regex1/igs;
		if($Block_Regex2 ne 'N/A')
		{
			for my $Block1(@Block1)
			{					
				my @Block2 = $Block1=~m/$Block_Regex2/igs;
				if($Block_Regex3 ne 'N/A')
				{
					for my $Block2(@Block2)
					{							
						if($Council_Code!~m/^112$/is)
						{
							while($Block2=~m/$Block_Regex3/gsi)
							{
								my $docHREF = $1;
								if($Docs_URL_Regex ne 'N/A')
								{
									if($docHREF=~m/$Docs_URL_Regex/is)
									{
										$Doc_URL = $1;
										$Doc_Desc = $2;
										if($Council_Code=~m/^112$/is)
										{
											$Doc_URL = $FILTER_URL2.$Doc_URL if($Doc_URL!~m/^http/is);	
										}
										else
										{
											$Doc_URL = $FILTER_URL1.$Doc_URL if($Doc_URL!~m/^http/is);	
										}
										$Doc_Desc=~s/\//\-/gsi;
									}
								}
								my ($file_type);
								if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
								{
									$file_type=$1;
								}
								else
								{
									$file_type='pdf';
								}	
								
								if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
								{
									my $file_typeDoc=$1;
									if($file_typeDoc eq $file_type)
									{
										$file_type;
									}
									else
									{
										$file_type=$file_typeDoc;
									}
								}

								
								$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
								
								$pdfcount_inc++;
								if($Council_Code=~m/^112$/is)
								{
									$Doc_URL = &clean($Doc_URL);
									# $Doc_URL=~s/(stream\.aspx\?target=)\&\#xD\;\&\#xA\;[^>]*?(http)/$1$2/is;
								}
								$Doc_URL=~s/\&amp\;/\&/gsi;
								print "Doc_URL	: $Doc_URL\n";
								print "pdf_name	: $pdf_name\n";
								print "file_type: $file_type\n";
								# 
								
								if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
								{
									$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,"",$Doc_Desc];
								}	
							}
						}
						else
						{
							my @docdata;
							while($Block2=~m/$Block_Regex3/gsi)
							{
								push(@docdata,$1);
							}	
							my $docHREF = $docdata[0]; 
							$Doc_Desc = $docdata[4]; 
							if($docHREF=~m/$Docs_URL_Regex/is)
							{
								$Doc_URL = $1;
								if($Council_Code=~m/^112$/is)
								{
									$Doc_URL = $FILTER_URL2.$Doc_URL if($Doc_URL!~m/^http/is);	
								}
								else
								{
									$Doc_URL = $FILTER_URL1.$Doc_URL if($Doc_URL!~m/^http/is);	
								}
							}
							my ($file_type);
							if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
							{
								$file_type=$1;
							}
							else
							{
								$file_type='pdf';
							}	
							
							if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
							{
								my $file_typeDoc=$1;
								if($file_typeDoc eq $file_type)
								{
									$file_type;
								}
								else
								{
									$file_type=$file_typeDoc;
								}
							}

							
							$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
							
							$pdfcount_inc++;
							if($Council_Code=~m/^112$/is)
							{
								$Doc_URL = &clean($Doc_URL);
								# $Doc_URL=~s/(stream\.aspx\?target=)\&\#xD\;\&\#xA\;[^>]*?(http)/$1$2/is;
							}
							print "Doc_URL	: $Doc_URL\n";
							print "pdf_name	: $pdf_name\n";
							print "file_type: $file_type\n";
							# 
							
							if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
							{
								$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,"",$Doc_Desc];
							}	
						}	
					}	
				}
			}
		}
	}
	
	return (\%Doc_Details);	
}

sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $ApplicationURL=shift;
	my $Doc_URL_BY=shift;
	
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	if (($Doc_Download_URL_Index =~m/([a-z]+)/is) and ($Doc_Download_URL_Index ne 'N/A'))
	{
		# print "if";
		# 
		my ($Doc_Details_Hash_Ref_tmp,$p)=&Download_Utility::Format8_Html_Header($Config,$Content,$Home_URL,$Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY,$Council_Code,$pdfcount_inc,$pageFetch);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
		
		$pdfcount_inc=$p;
	
	}
	else
	{
		print "else\n";
		# 
		$Content=~s/<td\/>/<td><\/td>/igs;
		if($Block_Regex1 ne 'N/A')
		{
			my @Block1 = $Content =~m/$Block_Regex1/igs;
			if($Block_Regex2 ne 'N/A')
			{
				for my $Block1(@Block1)
				{	
					my @Block2 = $Block1=~m/$Block_Regex2/igs;
					if($Block_Regex3 ne 'N/A')
					{
						for my $Block2(@Block2)
						{		
							if($Council_Code=~m/^280$/is)
							{
								$Block2=~s/<\/a><td/<\/a><\/td><td/gis;
								$Block2=~s/$/<\/td>/is;
							}
							elsif($Council_Code=~m/^388$/is)
							{
								$Block2=~s/^\s*<a/<td><a/is;
								$Block2=~s/(<a[^>]*?>)/$1<\/td><td>/is;
								$Block2=~s/\s*<\/a>/<\/td><\/a>/is;
							}
							my @Block3 = $Block2=~m/$Block_Regex3/igs;
							my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
							my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
							%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
						}	
					}
					else
					{
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
			}	
			else
			{
				my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
				my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
			}	
		}
	}
	
	return \%Doc_Details_Hash;
}



sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}

	if($Council_Code=~m/^333$/is)
	{
		$PDF_URL_Cont = $Data[$Doc_Download_URL_Index] if($Doc_Download_URL_Index ne 'N/A');
	}
	elsif($Council_Code=~m/^342$/is)
	{
		my @loc=split(',',$Doc_Download_URL_Index);
		$PDF_URL_Cont = $Data[$loc[0]].$Data[$loc[1]].$Data[$loc[2]];
	}
	else
	{
		$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	}
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');

	if($Doc_URL_BY eq 'Salford')
	{
		my ($AppDoc,$ViewDoc,$AppDoc1,$ViewDoc1,$AppDoc2,$ViewDoc2);
		# if($PDF_URL_Cont=~m/$Doc_URL_BY_Regex/is)
		if($PDF_URL_Cont=~m/<input[^>]*?name=\"([^\"]*?)\"\s*value=\"([^>]*?)\"\s*[^>]*?>/is)
		{
			$AppDoc=uri_escape($1);
			$ViewDoc=uri_escape($2);
		}	
		if($PDF_URL_Cont=~m/<input[^>]*?name=\"([^\"]*?)\"\s*value=\"(View\s*document)\"\s*[^>]*?>/is)
		{
			$AppDoc1=uri_escape($1);
			$ViewDoc1=$2;
			$ViewDoc1=~s/\s/\+/is;
		}	
		
		my $all;
		while($Content=~m/value=\"View\s*document\"\s*[^>]*?>\s*<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/igs)
		{
			$AppDoc2=uri_escape($1);
			$ViewDoc2=uri_escape($2);
			$all .= $AppDoc2."=".$ViewDoc2."&";
		}
		
		# $ViewDoc2=~s/btnViewDocument$/ctl00/gsi;
					
		my ($hdnWindowWidth,$hdnWindowWidthval);
		my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
		my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
		if($Content=~m/<input[^>]*?name=\"([^\"]*?hdnWindowWidth)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
		{
			$hdnWindowWidth = uri_escape($1);
			$hdnWindowWidthval = $2;			
		}
		
		my $Post_Content = "__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24ContentPlaceHolder%24ddlPageSize=25&$AppDoc=$ViewDoc&$AppDoc1=$ViewDoc1&$AppDoc2=$ViewDoc2&$all&ctl00%24hdnWindowWidth=1488";
		
		$Post_Content=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
		$Post_Content=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
		
		# print "$Document_Url\n";
		# my ($App_Page_Content,$Code) = &Download_Utility::docPostMechMethod($Document_Url, $Post_Content, $pageFetch);	
		
		my $Doc_Post_Host = 'dctmviewer.salford.gov.uk';
		my $Content_Type = 'application/x-www-form-urlencoded';
		my $Proxy = 'Y';
		my ($App_Page_Content,$Code)=&Download_Utility::Post_Method($Document_Url,$Doc_Post_Host,$Content_Type,$Document_Url,$Post_Content,$Proxy);
		
		$Post_Content=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
		$Post_Content=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
		
		my $halfURL = $1 if($App_Page_Content=~m/window\.open\(\'([^\']*?)\'\)\;\/\/\]\]>/is);
		
		$Doc_URL=$Doc_Download_URL_ABS.$halfURL;
		
		# print "Doc_URL==>$Doc_URL\n";
		
 	}
	elsif($Doc_URL_BY eq 'By Click')
	{
		my ($AppDoc,$ViewDoc);
				
		if($PDF_URL_Cont=~m/$Doc_URL_BY_Regex/is)
		{
			$AppDoc=$1;
			$ViewDoc=$2;
			# print "$ViewDoc=$AppDoc\n";
		}	
				
		$pageFetch->form_number($FORM_NUMBER1); 
		$pageFetch->set_fields($AppDoc => $ViewDoc);
		$pageFetch->click();	
				
		my $Page_url = $pageFetch->uri();
		# print "$Page_url\n";
		my $App_Page_Content = $pageFetch->content;
		
		
	}
	elsif($Doc_Download_URL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl=$1;	
			
			# print "$PDF_URL_Cont\n"; 
			# print "$Durl\n"; 
			if($Doc_Download_URL_Replace ne 'N/A') 
			{		
				my $docID=$1 if($Durl=~m/Download\.aspx\?ID=(\d+)$/is);	
				$Doc_URL = $Doc_Download_URL_Replace;
				$Doc_URL=~s/<SelectedID>/$docID/igs;
			}
			elsif($Doc_Download_URL_ABS ne 'N/A') 
			{
				if($Council_Code=~m/^8$/is)
				{
					$Doc_URL=$FILTER_URL.$Durl;
					my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);	
					$Doc_URL = $Doc_Download_URL_ABS.$1 if($searchPageCon=~m/<meta[^>]*?URL=\.\.\/\.\.\/([^\"]*?)\"[^>]*?>/is);
				} 
				elsif($Council_Code=~m/^236$/is)
				{	
					if($Durl=~m/^([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)$/is)
					{
						my $app = uri_escape($1);
						my $folder3Ref = uri_escape($2);
						my $dateReceived = uri_escape($3);
						my $docRef = uri_escape($4);
						my $docRef2 = uri_escape($5);
						my $sDocType = uri_escape($6);
						my $modifyType = uri_escape($7);
						my $url = uri_escape($8);
						my $alt = uri_escape($9);
						
						my $pdfRUL = "https://ppc.ipswich.gov.uk/checkdoc.aspx?appid=$app&isanite=true&folder3Ref=$folder3Ref&dateReceived=$dateReceived&docRef=$docRef&docRef2=$docRef2&sDocType=$sDocType&modifyType=$modifyType&alt=$alt&url=$url";
												
						my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($pdfRUL,$pageFetch);	
						$Doc_URL = $Doc_Download_URL_ABS.$1 if($searchPageCon=~m/<a\s*[^>]*?\s*href\=(?:\'|\")([^<]*?)(?:\'|\")\s*[^>]*?>/is);
					}
				}
				else
				{					
					$Doc_URL=$Doc_Download_URL_ABS.$Durl;
				}
				# print "Doc_URL1: $Doc_URL\n";
			}
			elsif($Durl!~m/^http/is)
			{							
				my $u1=URI::URL->new($1,$Root_Url);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
				# print "Doc_URL2: $Doc_URL\n"; 
			}
			else
			{	
				$Doc_URL=$Durl;		
				if($Council_Code=~m/^333$/is)
				{
					$Doc_URL=~s/\s/\%20/gsi;
				}
			}	
		}
	}
	
	if($Council_Code=~m/^(342|158|439|425|292|120)$/is)
	{
		$Doc_URL=$PDF_URL_Cont;
	}
				
	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);	
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	my ($file_type);
	if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	
	
	
	if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
	{
		my $file_typeDoc=$1;
		if($file_typeDoc eq $file_type)
		{
			$file_type;
		}
		else
		{
			$file_type=$file_typeDoc;
		}
	}
	elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
	{
		my $file_typeDocType=$1;
		if($file_typeDocType eq $file_type)
		{
			$file_type;
		}
		else
		{
			$file_type=$file_typeDocType;
		}
	}

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	
	# print "pdfcount_inc	: $pdfcount_inc\n"; 	
	
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	if($Council_Code!~m/^333$/is)
	{
		$Doc_URL=decode_entities($Doc_URL); 
	}
	$pdfcount_inc++;
	
	if($Council_Code=~m/^428$/is)
	{
		next if($Doc_URL=~m/^\s*$/is)
	}
	print "Doc_URL	: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date	: $Published_Date\n";

	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	
	return (\%Doc_Details);
}

sub Search_Application()
{
	my $Application_No = shift;
	my $ApplicationURL = shift;

	my ($homePageContent,$Code) = &Download_Utility::docMechMethod($Home_URL,$pageFetch);	
	print "Code1: $Code\n";
	if($homePageContent=~m/Please\s*click\s*the\s*button\s*below\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is) # for 158,439,425,428
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
				
		$homePageContent = $pageFetch->content;
		$Code= $pageFetch->status();
		print "Code2: $Code\n";
	}
	
	my ($docPageContent,$docRespCode,$searchPageCon,$searchCode);
	
	if($Search_Type eq "By Click")
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields('ctl00$ContentPlaceHolder1$ddlAuthority' => '2') if($Council_Code=~m/^439$/is);
		$pageFetch->set_fields( $FORM_REFERENECE => $Application_No );
		$pageFetch->click();
			
		$searchPageCon = $pageFetch->content;
		$searchCode= $pageFetch->status();
	}
	# elsif($Council_Code=~m/^348$/is)
	# {
		
		# my ($a,$b,$c);
		# if($Application_No=~m/^(\d+)\/(\d+)\/(\d+)$/is)
		# {
			# $a=$1;
			# $b=$2;
			# $c=$3;
		# }
		# $pageFetch->form_number($FORM_NUMBER); 
		# $pageFetch->set_fields('ctl00$sideBar$sdcAppSearch$ddlCaseType' => $a, 'ctl00$sideBar$sdcAppSearch$ddlCaseYear' => $b, 'ctl00$sideBar$sdcAppSearch$txtCaseNo' => $c );
		# $pageFetch->click( 'ctl00$sideBar$sdcAppSearch$imgBtnAppSearch' );
		
		
		# $searchPageCon = $pageFetch->content;
		# $searchCode= $pageFetch->status();
		# $POST_URL=~s/<APPNUM>/$Application_No/gsi;
		# ($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);	
		# $POST_URL=~s/$Application_No/<APPNUM>/gsi;
		
	# }
	elsif($Council_Code=~m/^388$/is)
	{
		print "$ApplicationURL\n";
		($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($ApplicationURL,$pageFetch);	
	}
	elsif($Council_Code=~m/^133$/is)
	{
		$POST_URL=~s/<APPNUM>/$Application_No/gsi;
		($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($POST_URL,$pageFetch);	
		$POST_URL=~s/$Application_No/<APPNUM>/gsi;
	}
	elsif($Council_Code=~m/^285$/is)
	{
		my $Appnum = $1 if($ApplicationURL=~m/KeyNo=([^>]*?)$/is);
		$Search_POST=~s/<APPNUM>/$Appnum/gsi;
		
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'planning.northamptonboroughcouncil.com','application/json; charset=utf-8',"http://planning.northamptonboroughcouncil.com/planning/planning-application?RefType=PBDC&KeyNo=$Appnum",$Search_POST,'Y');
		$Search_POST=~s/$Appnum/<APPNUM>/gsi;
	}
	elsif($Council_Code=~m/^424$/is)
	{
		my $Appnum = $1 if($ApplicationURL=~m/KeyNo=([^>]*?)$/is);
		$Search_POST=~s/<APPNUM>/$Appnum/gsi;
		
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'planning.northamptonboroughcouncil.com','application/json; charset=utf-8',"http://planning360.waverley.gov.uk/planning/search-applications#VIEW?RefType=GFPlanning&KeyNo=$Appnum&KeyText=Subject",$Search_POST,'Y');
		
		$Search_POST=~s/$Appnum/<APPNUM>/gsi;
	}
	elsif($Council_Code=~m/^185$/is)
	{
		my $Appnum = $1 if($ApplicationURL=~m/KeyNo=([^>]*?)$/is);
		$Search_POST=~s/<APPNUM>/$Appnum/gsi;
		print "Search_POST::$Search_POST\n\n";
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'planning.northamptonboroughcouncil.com','application/json; charset=utf-8',"https://www.lewes-eastbourne.gov.uk/planning/application-summary/?RefType=APPPlanCase&KeyText=$Appnum",$Search_POST,'Y');
		print "searchCode::$searchCode\n\n";
		
		$Search_POST=~s/$Appnum/<APPNUM>/gsi;
	}
	elsif($Council_Code=~m/^378$/is)
	{
		
		my $searchPost = "searchType=Application&searchCriteria.caseStatus=All&searchCriteria.simpleSearchString=<APPNUM>&searchCriteria.simpleSearch=true";
		$searchPost=~s/<APPNUM>/$Application_No/gsi;
		my $refer = "https://www12.staffordbc.gov.uk/online-applications/search.do?action=simple&searchType=Application";
		# my ($searchCon, $Code)=&Download_Utility::Post_Method('http://www3.staffordbc.gov.uk/civica/Resource/Civica/Handler.ashx/keyobject/pagedsearch','www12.staffordbc.gov.uk
# ','application/json; charset=utf-8',$refer,$searchPost,'N');
		# my $Appnum = $1 if($searchCon=~m/\,\"KeyNumber\"\:\"([^\"]*?)\"/is);
		$Search_POST=~s/<APPNUM>/$Application_No/gsi;
		print "POST_URL=>$POST_URL\n";
		print "refer=>$refer\n";
		print "Search_POST=>$Search_POST\n";
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'www3.staffordbc.gov.uk','application/x-www-form-urlencoded',$refer,$Search_POST,'Y');
	
		$Search_POST=~s/$Application_No/<APPNUM>/gsi;
		$searchPost=~s/$Application_No/<APPNUM>/gsi;
	}
	elsif($Council_Code=~m/^332$/is)
	{
		$Search_POST=~s/<APPNUM>/$Application_No/gsi;
		my $refer = "https://rotherham.planportal.co.uk/?id=$Application_No";
				
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'rotherham.planportal.co.uk','application/json',$refer,$Search_POST,'Y');
		
		$Search_POST=~s/$Application_No/<APPNUM>/gsi;
	}
	elsif($Council_Code=~m/^329$/is)
	{
		my $refer ="https://planningdocs.rochford.gov.uk/Planning/planning-documents?SDescription=$Application_No";
		$Search_POST=~s/<APPNUM>/$Application_No/gsi;
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'planningdocs.rochford.gov.uk','application/json; charset=UTF-8',$refer,$Search_POST,'Y');
		my $keynum=$1 if($searchPageCon=~m/\"KeyNumber\"\:\"([^>]*?)\"/is);	
		print "329*******\n";
		$POST_URL='https://planningdocs.rochford.gov.uk/civica/Resource/Civica/Handler.ashx/doc/list';
		$Search_POST='{"KeyNumb":"'."$keynum".'","KeyText":"Subject","RefType":"GFPlanning"}';
		
		($searchPageCon, $searchCode)=&Download_Utility::Post_Method($POST_URL,'planningdocs.rochford.gov.uk','application/json; charset=UTF-8',$refer,$Search_POST,'Y');
	
		$Search_POST=~s/$Application_No/<APPNUM>/gsi;
	
	}
	else
	{
		my $appNum;
		if($Council_Code=~m/^376$/is)
		{
			$appNum = uri_escape($1) if($Application_No=~m/(P\/\d+\/\d{2})/is);
		}
		else
		{
			$appNum = uri_escape($Application_No);
		}
		
		my $viewState = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATE"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateGen = uri_escape($1) if($homePageContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"__VIEWSTATEGENERATOR"\s[^>]*?value="([^>]*?)"\s*\/>/is);
		my $eventVal = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__EVENTVALIDATION"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateZip = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATEZIP"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		
		$Search_POST =~s/<VIEWSTATE>/$viewState/gsi;
		$Search_POST =~s/<VIEWSTATEGENERATOR>/$viewStateGen/gsi;
		$Search_POST =~s/<EVENTVALIDATION>/$eventVal/gsi;
		$Search_POST =~s/<APPNUM>/$appNum/gsi;
		$Search_POST =~s/<VIEWSTATEZIP>/$viewStateZip/gsi;
		# print "$Search_POST\n"; 
		
		($searchPageCon,$searchCode) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$Search_POST =~s/$appNum/<APPNUM>/gsi;
		$Search_POST =~s/$viewState/<VIEWSTATE>/gsi;
		$Search_POST =~s/$viewStateGen/<VIEWSTATEGENERATOR>/gsi;
		$Search_POST =~s/$eventVal/<EVENTVALIDATION>/gsi;
		$Search_POST =~s/$viewStateZip/<VIEWSTATEZIP>/gsi;
		# print "$Search_POST\n"; 
	}
	

	if($Council_Code=~m/^(8|22|103|112|193|236|247|275|334|375|376|394|470|158|439|425)$/is)
	{
		if($searchPageCon=~m/<td\s*title=\"View\s*Application[^>]*?>\s*<a[^>]*?href=\"\s*([^\"]*?)\s*\"[^>]*?>\s*(?:<span>View[^<]*?<\/span>\s*)?([^<]*?)\s*<\/a>/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);	
	
		}
		elsif($searchPageCon=~m/<td\s*class=\"apas_tblContent\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*(?:<span>View[^<]*?<\/span>\s*)?([^<]*?)\s*<\/a>/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*<img[^>]*?title=\'View\s*Application\s*$Application_No\'[^>]*?>[^>]*?<\/a>\s*<\/td>/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/td>/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<tr\s*class=\"dbResults\"[^>]*?>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\">/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<h2>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/h2>/is) # For 158,439,425,428
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
	}		
		
	
	if(($searchPageCon=~m/$Doc_URL_Regex/is) && ($Doc_URL_Regex ne 'N/A'))
	{
		my $docURL = $1;
		$docURL=~s/amp\;//igs;
		print "docURL==========> $docURL\n";
		
		if($docURL!~m/^http\:/is)
		{
			$docURL = $FILTER_URL.$docURL;
		}	

		if($Council_Code=~m/^275$/is)
		{
			$docURL = "http://www.molevalley.gov.uk/CausewayDocList/DocList.jsp?ref=$Application_No";
		}
		
		if($Council_Code=~m/^394$/is)
		{
			($docPageContent,$docRespCode) =&Download_Utility::Getcontent($docURL,$URL,$Host,$Content_Type,'','','');
		}
		elsif($Council_Code=~m/^378$/is)
		{
			($docPageContent,$docRespCode) =&Download_Utility::Getcontent($docURL,$URL,$Host,$Content_Type,'','','');
			my $docURL = $1 if($docPageContent=~m/$Landing_Page_Regex/is);
			my $searchPost = '{"refType":"GFPlanning","searchFields":{"SDescription":"'.$Application_No.'"}}';
			my $refer = $docURL;
			my ($NavigateCon, $Code)=&Download_Utility::Post_Method('http://www3.staffordbc.gov.uk/civica/Resource/Civica/Handler.ashx/keyobject/pagedsearch','www3.staffordbc.gov.uk','application/json; charset=utf-8',$docURL,$searchPost,'N');
			my $KeyNumber = $1 if($NavigateCon=~m/\"KeyNumber\"\s*\:\s*\"([^>]*?)\"/is);
			my $searchPost = '{"KeyNumb":"'.$KeyNumber.'","KeyText":"Subject","RefType":"GFPlanning"}';
			my $refer = "https://www12.staffordbc.gov.uk/online-applications/search.do?action=simple&searchType=Application";
			($docPageContent, $docRespCode)=&Download_Utility::Post_Method('http://www3.staffordbc.gov.uk/civica/Resource/Civica/Handler.ashx/doc/list','www3.staffordbc.gov.uk','application/json; charset=utf-8',$docURL,$searchPost,'N');
		}		
		else
		{
			($docPageContent,$docRespCode) = &Download_Utility::docMechMethod($docURL,$pageFetch);			
		}
		if($Council_Code=~m/^112$/is)
		{
			if($docPageContent!~m/var\s*model\s*=\s*([^>]*?)\s*;/is)
			{
				next;
			}
			$docPageContent=$1 if($docPageContent=~m/var\s*model\s*=\s*([^>]*?)\s*;/is);
			
		}
	}	
	else
	{
		$docPageContent = $searchPageCon;
	}

	if($docRespCode=~m/^\s*$/is)
	{
		$docRespCode = $searchCode;
	}
	if($docRespCode=~m/^\s*$/is)
	{
		$docRespCode = $Code;
	}	
	

	return($docPageContent,$docRespCode);
}


sub clean()
{
	my $Data=shift;
	$Data=~s/\s*<[^>]*?>\s*//igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;//igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&nsbp//igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	return($Data);
}

sub QueryExecuteMax()
{	
	my ($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query)=@_;
	
	if($Insert_Document!~m/values\s*$/is)
	{
		$Insert_Document=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

		$Update_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

		$MD5_Insert_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);
		
		$Insert_Document="";
		$Update_Query="";
		$MD5_Insert_Query="";
		
		$Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
		$MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

		$MaxCount=0;
	}
	return ($MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
}