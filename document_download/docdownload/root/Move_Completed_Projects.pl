use File::Copy;
use strict;
use File::Path;
use Cwd qw(abs_path);
use File::Basename;
use strict;
use Time::Piece;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $logDirectory = ($basePath.'/logs');

require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 

my $dbh = Download_DB::DbConnection();

my $query = "select Council_Code, ID, Application_No, MARKUP, No_of_Documents from Format_public_Access where ID = 8022210";

# my $query = "select top 1 project_id_p,format_id from dr_project where  format_id in (
# select  format_id from project_tracking where markup='L'
# and Format_ID in (select  a.Format_ID from project_tracking as a,dr_project as b where a.markup='S' and a.format_id=b.format_id and b.Processed_Date is not null 
# and convert(varchar(10),update_date,112) between 20180701 and 20180714)
 # group by  format_id having count(format_id)>1) order by PROJECT_ID_P";

my $sth = $dbh->prepare($query);
$sth->execute();
my (@Council_Code, @Format_ID,@Application_No,@Markup,@No_of_Documents,@PROJECT_ID,@PROJECT_STATUS);
my $datetime = localtime();
while(my @record = $sth->fetchrow)
{
	push(@PROJECT_ID,&Trim($record[0]));
	push(@Format_ID,&Trim($record[1]));
}
$sth->finish();
open(FN,">>$logDirectory/Backup_Completed_Docs_Log.txt");
print FN "Start : ".localtime()."\t";
close FN;

# open(FN,">>$logDirectory/Large_to_Smalls_Deleted_Report.txt");
# print FN "Format_ID\tFile_name\tDeleted_On\tRemoved_File_Size(MB)\n";
# close FN;

# open(FD,">>$logDirectory/Large_to_Smalls_Delete_Failed_Report.txt");
# print FN "Format_ID\tFile_name\tDeleted_On\tRemoved_File_Size(MB)\n";
# close FD;					

# open(FD,">>$logDirectory/L_to_S_Deleted_Report_Projectwise.txt");
# print FN "Format_ID\tDeleted_On\tRemoved_File_Size(MB)\n";
# close FD;					

my $source='//172.27.137.202/One_app_download/';
# my $source = 'C:/Users/arulk/Downloads/123456/sample/';
my $destination = '//172.27.137.180/ONE APP Supporting Files 2018';

my $i=1;
my $Err_Flag = 0;
my $totalRemoveSize = 0;
my $reccount = @Format_ID;
print "Projects Count :: $reccount\n";
for(my $reccnt = 0; $reccnt < $reccount; $reccnt++)
{
	my $PROJECT_ID 		= $PROJECT_ID[$reccnt];
	my $Format_ID 		= $Format_ID[$reccnt];
	
	print "destination:$destination\n";
	my $Temp_pdf_store_path="$source$Format_ID";
	my $destination_path="$destination$Format_ID";
	print "Folder: $Format_ID\n";
	next if($Format_ID!~m/^[\d]+$/is);
	my $docsCount=0;
	my $totalRemoveSizeProjectwise=0;
	opendir(my $dh, $Temp_pdf_store_path) || warn "can't opendir $Temp_pdf_store_path: $!";
	unless ( -d $destination_path )
	{
		$destination_path=~s/\//\\/igs;
		system('mkdir', $destination_path, '/b/s', '/o:-d'); 
	}	
	print "Dest: $destination_path\n";

	my $now = localtime();
	while (my $file_name=readdir $dh) 
	{
		next if($file_name=~m/^\s*(\.|\.\.|Thumbs\.db)\s*$/is);
		my $sourceFile="$Temp_pdf_store_path/$file_name";
		my $destinationFile="$destination_path/$file_name";
		print "Source: $sourceFile\n";
		print "Destin: $destinationFile\n";
		$docsCount++;
		move("$sourceFile","$destinationFile");
		<STDIN>;
	}
	open(FN,">>$logDirectory/Backup_Completed_Projectwise.txt");
	print FN "$Format_ID\t$now\t$Format_ID\n";
	close FN;
	closedir $dh;
}

# open(FN,">>$logDirectory/Removed_Old_Folders_Log.txt");
# print FN "End : ".localtime()."\t$totalRemoveSize\t$i\n";
# close FN;

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}


