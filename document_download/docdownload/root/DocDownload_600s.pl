use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename;
use JSON;


# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# $basePath="D:\\Glenigan\\Document_download\\";
print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 


my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];
print "onDemand: $onDemand\n";

if($Council_Code=~m/486/is)
{
	exit;
}

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}
my $MaxCount=0;
my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);


#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";

my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|Form?|pdf|information|Documents?|plans?|Details?|Applications?|Application?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	
my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

print "COUNCIL_NAME_TMP=>$COUNCIL_NAME_TMP\n";

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";

my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	

my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";
	
&Download_DB::Execute($dbh,$Dashboard_Insert_Query);

my %Nextpage_Replace_Hash;

if($onDemand eq 'Y')
{
	print "This is an On demand request\n";
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\'";	
	&Download_DB::Execute($dbh,$onDemand_Update_Query);
}

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;

my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;

my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
# print "RECCNT  :: <$reccount>\n";

my ($Method, $Format, $Home_URL, $Host, $Host1, $FILTER_URL, $FILTER_URL1, $FILTER_URL2, $Content_Type, $FORM_NUMBER, $FORM_NUMBER1, $FORM_REFERENECE, $Referer, $POST_URL, $Doc_URL_Regex, $Docs_URL_Regex, $Landing_Page_Regex, $Viewstate_Regex, $Viewstategenerator_Regex, $Eventvalidation_Regex, $Block_Regex1, $Block_Regex2, $Block_Regex3, $Doc_Published_Date_Index, $Doc_Download_URL_Replace, $Doc_Type_Index, $Doc_Description_Index, $Doc_Download_URL_Index, $Doc_Download_URL_Regex, $Doc_Download_Filter_URL, $Total_No_Of_Pages_Regex, $Available_Doc_Count_Regex, $Doc_URL_BY, $Pagination_Type, $Next_Page_Regex, $Next_Page_Replace_Info, $Next_Page_Replace_Key, $Redir_Url, $Doc_Download_URL_ABS, $SSL_VERIFICATION, $All_Page_Name, $All_Page_Name_Value, $Doc_URL_BY_Regex, $Accept_Name, $Accept_Value, $Search_Type, $Search_POST, $Post_Content, $By_Click_Regex);
		
for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	# print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	
	print "Format_ID :: $Format_ID \n";
	print "URL 	 :: $URL \n";	
	print "Document_Url	 :: $Document_Url \n";	
	print "Council_Code==>$Council_Code\n"; 
	
	
	next if($Council_Code!~m/^(632|602|615|628|618|616|605|620|607|619|617|627|609|613|623|631|611|621|629|625|608|626|604|612|624|630|606|603|8)$/is);
	
	my $Config = Config::Tiny->new();
	$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details_600s.ini');
	$Method = $Config->{$Council_Code}->{'Method'};
	$Format = $Config->{$Council_Code}->{'Format'};
	$Home_URL = $Config->{$Council_Code}->{'Home_URL'};
	$Host = $Config->{$Council_Code}->{'Host'};
	$Host1 = $Config->{$Council_Code}->{'Host1'};
	$FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
	$FILTER_URL1 = $Config->{$Council_Code}->{'FILTER_URL1'};
	$FILTER_URL2 = $Config->{$Council_Code}->{'FILTER_URL2'};
	$Content_Type = $Config->{$Council_Code}->{'Content_Type'};
	$FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
	$FORM_NUMBER1 = $Config->{$Council_Code}->{'FORM_NUMBER1'};
	$FORM_REFERENECE = $Config->{$Council_Code}->{'FORM_REFERENECE'};
	$Referer = $Config->{$Council_Code}->{'Referer'};
	$POST_URL = $Config->{$Council_Code}->{'POST_URL'};
	$Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
	$Docs_URL_Regex = $Config->{$Council_Code}->{'Docs_URL_Regex'};
	$Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
	$Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
	$Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
	$Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
	$Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
	$Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
	$Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
	$Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
	$Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
	$Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
	$Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
	$Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
	$Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
	$Doc_Download_Filter_URL = $Config->{$Council_Code}->{'Doc_Download_Filter_URL'};
	$Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
	$Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};
	$Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
	$Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
	$Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
	$Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
	$Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
	$Redir_Url = $Config->{$Council_Code}->{'Redir_Url'};
	$Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
	$SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
	$All_Page_Name = $Config->{$Council_Code}->{'All_Page_Name'};
	$All_Page_Name_Value = $Config->{$Council_Code}->{'All_Page_Name_Value'};
	$Doc_URL_BY_Regex = $Config->{$Council_Code}->{'Doc_URL_BY_Regex'};
	$Accept_Name = $Config->{$Council_Code}->{'Accept_Name'};
	$Accept_Value = $Config->{$Council_Code}->{'Accept_Value'};
	$Search_Type = $Config->{$Council_Code}->{'Search_Type'};
	$Search_POST = $Config->{$Council_Code}->{'Search_POST'};
	$Post_Content = $Config->{$Council_Code}->{'Post_Content'};
	$By_Click_Regex = $Config->{$Council_Code}->{'By_Click_Regex'};

	$By_Click_Regex = "N/A" if($By_Click_Regex eq ""); $Method = "N/A" if($Method eq ""); $Format = "N/A" if($Format eq ""); $Home_URL = "N/A" if($Home_URL eq ""); $Host = "N/A" if($Host eq ""); $Host1 = "N/A" if($Host1 eq ""); $FILTER_URL = "N/A" if($FILTER_URL eq ""); $FILTER_URL1 = "N/A" if($FILTER_URL1 eq ""); $FILTER_URL2 = "N/A" if($FILTER_URL2 eq ""); $Content_Type = "N/A" if($Content_Type eq ""); $FORM_NUMBER = "N/A" if($FORM_NUMBER eq ""); $FORM_NUMBER1 = "N/A" if($FORM_NUMBER1 eq ""); $FORM_REFERENECE = "N/A" if($FORM_REFERENECE eq ""); $Referer = "N/A" if($Referer eq ""); $POST_URL = "N/A" if($POST_URL eq ""); $Doc_URL_Regex = "N/A" if($Doc_URL_Regex eq ""); $Docs_URL_Regex = "N/A" if($Docs_URL_Regex eq "");
	$Landing_Page_Regex = "N/A" if($Landing_Page_Regex eq ""); $Viewstate_Regex = "N/A" if($Viewstate_Regex eq ""); $Viewstategenerator_Regex = "N/A" if($Viewstategenerator_Regex eq ""); $Eventvalidation_Regex = "N/A" if($Eventvalidation_Regex eq ""); $Block_Regex1 = "N/A" if($Block_Regex1 eq ""); $Block_Regex2 = "N/A" if($Block_Regex2 eq ""); $Block_Regex3 = "N/A" if($Block_Regex3 eq ""); $Doc_Published_Date_Index = "N/A" if($Doc_Published_Date_Index eq ""); $Doc_Download_URL_Replace = "N/A" if($Doc_Download_URL_Replace eq ""); $Doc_Type_Index = "N/A" if($Doc_Type_Index eq ""); $Doc_Description_Index = "N/A" if($Doc_Description_Index eq "");
	$Doc_Download_URL_Index = "N/A" if($Doc_Download_URL_Index eq ""); $Doc_Download_URL_Regex = "N/A" if($Doc_Download_URL_Regex eq ""); $Doc_Download_Filter_URL = "N/A" if($Doc_Download_Filter_URL eq ""); $Total_No_Of_Pages_Regex = "N/A" if($Total_No_Of_Pages_Regex eq ""); $Available_Doc_Count_Regex = "N/A" if($Available_Doc_Count_Regex eq ""); $Doc_URL_BY = "N/A" if($Doc_URL_BY eq ""); $Pagination_Type = "N/A" if($Pagination_Type eq ""); $Next_Page_Regex = "N/A" if($Next_Page_Regex eq ""); $Next_Page_Replace_Info = "N/A" if($Next_Page_Replace_Info eq ""); $Next_Page_Replace_Key = "N/A" if($Next_Page_Replace_Key eq "");
	$Redir_Url = "N/A" if($Redir_Url eq ""); $Doc_Download_URL_ABS = "N/A" if($Doc_Download_URL_ABS eq ""); $SSL_VERIFICATION = "N/A" if($SSL_VERIFICATION eq ""); $All_Page_Name = "N/A" if($All_Page_Name eq ""); $All_Page_Name_Value = "N/A" if($All_Page_Name_Value eq ""); $Doc_URL_BY_Regex = "N/A" if($Doc_URL_BY_Regex eq ""); $Accept_Name = "N/A" if($Accept_Name eq ""); $Accept_Value = "N/A" if($Accept_Value eq ""); $Search_Type = "N/A" if($Search_Type eq ""); $Search_POST = "N/A" if($Search_POST eq ""); $Post_Content = "N/A" if($Post_Content eq "");
	
	
	# if($Council_Code=~m/^(619|625|606|630|612|603)$/is)
	if($Council_Code=~m/^(619|606X|612|603)$/is)
	{
		$Document_Url="";
	}
	elsif($Council_Code=~m/^(615|618|616|627|609X)$/is)
	{
		$Document_Url=$URL;
	}
	
	
	if($Council_Code=~m/^(609)$/is)
	{
		$Document_Url="https://plan.dorsetcc.gov.uk/Disclaimer/Accept?returnUrl=%2FPlanning%2FDisplay%3FapplicationNumber%3D$1" if($Document_Url=~m/applicationNumber\=([^>]*?)$/is);
		
	}
	if($Council_Code=~m/^(618)$/is)
	{
		$Document_Url="http://leicestershire.planning-register.co.uk/Disclaimer?returnUrl=%2FPlanning%2FDisplay%3FapplicationNumber%3D$1&accepted=True" if($Document_Url=~m/applicationNumber\=([^>]*?)$/is);
	}
	if($Council_Code=~m/^(625)$/is)
	{
		$Document_Url="https://planning.somerset.gov.uk/Disclaimer/Accept?returnUrl=%2FPlanning%2FDisplay%3FapplicationNumber%3D$1" if($Document_Url=~m/applicationNumber\=([^>]*?)$/is);
	}
	if($Council_Code=~m/^(616)$/is)
	{
		my $app_no=$1 if($Document_Url=~m/\/Display\/([^>]*?)$/is);
		$app_no=~s/\//\%2F/igs;
		$Document_Url="https://www.kentplanningapplications.co.uk/Disclaimer/Accept?returnUrl=%2FPlanning%2FDisplay%2F$app_no";
	}
	if($Council_Code=~m/^(606)$/is)
	{
		#my $app_no=$1 if($Document_Url=~m/\/Display\/([^>]*?)$/is);
		#$app_no=~s/\//\%2F/igs;
		#$Document_Url="https://planning.cumbria.gov.uk/Disclaimer/Accept?returnUrl=%2FPlanning%2FDisplay%2F$app_no";
		$Document_Url="https://planning.cumbria.gov.uk/Disclaimer/Accept?returnUrl=%2FPlanning%2FDisplay%2F$Application_No";
	}
	if($Council_Code=~m/^(607)$/is)
	{
		my $app_no=$1 if($Document_Url=~m/\/Display\/([^>]*?)$/is);
		$app_no=~s/\//\%2F/igs;
		$Document_Url="https://planning.derbyshire.gov.uk/Disclaimer/Accept?returnUrl=%2FPlanning%2FDisplay%2F$app_no";
	}
	
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code,$Available_Doc_Count);
	my $Landing_Page_Flag=0;
		
	# my $Download_Location = $Local_Download_Location."/$Format_ID";
	
	print "Document_Url_rddd:: $Document_Url\n";
	
	($Doc_Details_Hash_Ref, $Document_Url, $Code)=&Extract_Link($Home_URL,$Document_Url,$URL,\%Nextpage_Replace_Hash,$Council_Code,$Application_No,$Config);
	$Site_Status = $Code;	
	
	print "Document_Url_rrr:: $Document_Url\n";
	
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	
	
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;
	
	$Todays_Count = $Todays_Count + $pdfcount;
	if($Available_Doc_Count=~m/^\s*$/is)
	{
		$Available_Doc_Count = $pdfcount;
	}	
	
	my $RetrieveMD5=[];
	my $No_Of_Doc_Downloaded=0;
	
	# if(lc($Markup) eq 'large')
	# {
		# print "$pdfcount<==>$NO_OF_DOCUMENTS\n"; <STDIN>;
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;
	# print "Temp_Download_Location=>$Temp_Download_Location\n";
	
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====>$Temp_Download_Location\n";
	
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		print "pdf_url::$pdf_url\n\n";
		
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				my ($D_Code, $Err_Msg, $MD5);
				if($Council_Code=~m/^(602|603|604|605|607|608|610|611|612|613|615|617|618|619|620|621|623|624|626|627|629|631|632|8|628)$/is)
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
				}
				else
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				}
				
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
				
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5);
					if($Council_Code=~m/^(602|603|604|605|607|608|610|611|612|613|615|617|618|619|620|621|623|624|626|627|629|631|632|8|628)$/is)
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
					}
					else
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					}
					
					my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}	
					
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";
					$Insert_Document.=$Insert_Document_List;
					
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
								
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
				}
			}
		}	

		#--------- DB INSERTION above 1000 record ---------------
		$MaxCount++;
		if($MaxCount == 995)
		{
			($MaxCount, $Insert_Document, $MD5_Insert_Query, $Update_Query) = QueryExecuteMax($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
		}
	}	

	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	&Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:	
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
	}	
	# else
	# {
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }
	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if(($One_App_Name=~m/^\s*$/is) && ($pdfcount != 0))
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	elsif($pdfcount != 0)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}
	}	
	if ($onDemand eq 'ApprovedDecision')
	{
		my $Flag_Update_query = "update GLENIGAN..TBL_APPROVED_LARGE_PROJECT set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\', All_Documents_Downloaded = 'Y' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
		$ApprovedDecisionUpdate_query.= $Flag_Update_query;
	}	
	$pdfcount_inc=1;
	
	
	# undef $Council_Code;	
	
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

$Insert_OneAppLog=~s/\,$//igs;
print "\n$Insert_OneAppLog\n";
&Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
print "\n$Insert_Document\n";
&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
print "\n$Update_Query\n";
&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
print "\n$MD5_Insert_Query\n";
&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);



$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
&Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);


if($onDemand eq 'Y')
{
	print "Update & Delete This Ondemand request\n";
	my $onDemandStatus = &Download_Utility::onDemandStatus($No_Of_Doc_Downloaded_onDemand,$Site_Status);
	my $onDemandUpdateQuery = "update tbl_on_demand_download_backup set Status = \'$onDemandStatus\' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and download_id = \'$onDemandID\'";
	print "onDemandUpdateQuery: $onDemandUpdateQuery\n";
	&Download_DB::Execute($dbh,$onDemandUpdateQuery);
	my $onDemandDeleteQuery = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and status = 'Inprogress'";	
	&Download_DB::Execute($dbh,$onDemandDeleteQuery);
}



sub Extract_Link()
{
	my $homeURL = shift;
	my $documentURL = shift;
	my $ApplicationURL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $councilCode = shift;
	my $Application_No = shift;
	my $Config=shift;
		
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my ($docPageContent,$Code,$Available_Doc_Count,%Doc_Details_Hash);
	my $Landing_Page_Flag=0;
	
	print "Extractlink\n\n\n";
	
	Re_Ping:	
	if($documentURL ne '')
	{
		if($Method=~m/GET/is)
		{
			if($councilCode=~m/^(?:616|625|606|630)$/is)
			{
				($docPageContent,$Code) = &Download_Utility::Getcontent($documentURL,$pageFetch);
			}			
			else
			{
				($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
			}
		}
		elsif($Method=~m/POST/is)
		{
			($docPageContent,$Code) = &Download_Utility::docPostMechMethod($documentURL,$Post_Content,$pageFetch);	
		}
		
	}
	else
	{
		print "Search Application method id processing..(Document URL not found)\n";
		($docPageContent,$Code)=&Search_Application($Application_No,$ApplicationURL);
	}	
	
	# open(DC,">603_Home_Content.html");
	# print DC $docPageContent;
	# close DC;
	# exit;
	
	
	if($docPageContent=~m/<[^>]*?=\"$Accept_Name\"[^>]*?>I\s*agree\s*to\s*this\s*statement\s*<\//is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# <STDIN>;		
	}
	elsif($docPageContent=~m/>\s*I\s*agree\s*<\//is)
	{
		my $appNo = $1 if($docPageContent=~m/<h3>Downloads\s*for\s*this\s*application<\/h3>\s*<form[^>]*?planningApplication\/([^\"]*?)\"[^>]*?>/is);
		$POST_URL=~s/<APPNUM>/$appNo/gsi;
		
		# print "$POST_URL\n";
		# print "$Search_POST\n"; <STDIN>;
		
		($docPageContent,$Code) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$POST_URL=~s/$appNo/<APPNUM>/gsi;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# exit;
	}
	elsif($docPageContent=~m/Please\s*click\s*the\s*button\s*below\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is) # For N608
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# <STDIN>;		
		
	}
	elsif($docPageContent=~m/(?:<input[^>]*?name=\"([^\"]*?)\"\s*value=\"Accept\"\s*[^>]*?>|<input[^>]*?value=\"(?:Continue|Accept)\"\s*[^>]*?>)/is) # For N623
	
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# <STDIN>;		
		
	}
	
	my %Doc_Details;
	if($Landing_Page_Flag == 0)
	{
		if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
		{
			print "Application Number==>$Application_No\n";
			
			if($docPageContent=~m/$Landing_Page_Regex/is)
			{
				my $DocUrl=$1;
				$DocUrl=~s/\&amp\;/\&/gsi;
				
				# print "DocUrl: $DocUrl\n";
				my $u1=URI::URL->new($DocUrl,$documentURL);
				$documentURL=$u1->abs;
				
				
				$documentURL = $FILTER_URL.$documentURL if($documentURL!~m/^http/is);
				
				print "Document_Url: $documentURL\n";
	
				
				$Landing_Page_Flag = 1;
				goto Re_Ping;
			}	
		}
		else
		{
			if($docPageContent=~m/<title>\s*Somerset\s*planning\s*portal\s*<\/title>/is) # for N625
			{
				if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				{
					my $Page = $1;
					my $aplId = $2;
					
					$documentURL = "http://schpwebapppln.somerset.gov.uk/ePlanning/tabPage$Page.jsp?aplId=$aplId";
					
					$Landing_Page_Flag = 1;
					goto Re_Ping;
				}
			}
			elsif($docPageContent=~m/<title>\s*West\s*Sussex\s*County\s*Council/is) # for N630
			{
				if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				{
					my $Page = $1;
					my $aplId = $2;
					
					$documentURL = "http://buildings.westsussex.gov.uk/ePlanningOPS/tabPage$Page.jsp?aplId=$aplId";
										
					$Landing_Page_Flag = 1;
					goto Re_Ping;
				}
			}
			elsif($docPageContent=~m/Suffolk\s*County\s*Council\s*<\/title>/is) # for N627
			{
				if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				{
					my $Page = $1;
					my $aplId = $2;
					
					$documentURL = "https://secure.suffolkcc.gov.uk/ePlanning/tabPage$Page.jsp?aplId=$aplId";
					
					$Landing_Page_Flag = 1;
					goto Re_Ping;
				}
			}
			elsif($docPageContent=~m/Dorset\s*County\s*Council\s*/is) # for N609
			{
				if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				{
					my $Page = $1;
					my $aplId = $2;
					
					$documentURL = "http://countyplanning.dorsetforyou.com/ePlanningOPS/tabPage$Page.jsp?aplId=$aplId";
					
					$Landing_Page_Flag = 1;
					goto Re_Ping;
				}
			}
			elsif($docPageContent=~m/Hertfordshire\s*County\s*Council\s*/is) # for N615
			{
				if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				{
					my $Page = $1;
					my $aplId = $2;
					
					$documentURL = "https://cloud1.atriumsoft.com/HCCePlanningOPS/tabPage$Page.jsp?aplId=$aplId";
					
					$Landing_Page_Flag = 1;
					goto Re_Ping;
				}
			}
			elsif($docPageContent=~m/Kent\s*County\s*Council\s*/is) # for N616
			{
				if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				{
					my $Page = $1;
					my $aplId = $2;
					
					$documentURL = "https://cloud2.atriumsoft.com/KCCePlanningOPS/tabPage$Page.jsp?aplId=$aplId";
					
					$Landing_Page_Flag = 1;
					goto Re_Ping;
				}
			}
			# elsif($docPageContent=~m/Leicestershire\s*County\s*Council\s*/is) # for N618
			# {
				# if($docPageContent=~m/<td[^>]*?onclick=\"openTab(\d+)\(([^\)]*?)\)\"[^>]*?class=\"menuItem\"[^>]*?>\s*(?:<u>)?Documents(?:<\/u>)?/is)
				# {
					# my $Page = $1;
					# my $aplId = $2;
					
					# $documentURL = "https://cloud2.atriumsoft.com/LCCePlanning/tabPage$Page.jsp?aplId=$aplId";
					
					# $Landing_Page_Flag = 1;
					# goto Re_Ping;
				# }
			# }
		}
	}
	
	if($docPageContent=~m/Cumbria\s*County\s*Council/is) #for N606
	{
		$docPageContent=~s/<\/table>\s*[\w\W]*?\s*<table\s*class=\"table\">\s*<thead>[\w\W]*?<\/thead>//gis;
	}
	if($Council_Code=~m/^607$/is) #for N606
	{
		$docPageContent=~s/<thead>[\w\W]*?<\/thead>//gis;
		$docPageContent=~s/<tr[^>]*?class="header[\w\W]*?<\/tr>//gis;
	}
	
	# open(DC,">620ome_Content.html");
	# print DC $docPageContent;
	# close DC;
	# exit;
	
	my ($Total_no_Pages);
	if($Total_No_Of_Pages_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_no_Pages=$1;
		}
		print "Total_no_Pages: $Total_no_Pages\n";
	}

	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
		
		print "Available_Doc_Count<==>$Available_Doc_Count\n"; 
		# <STDIN>;	
	}	
	
	
	
	# print "documentURL:::$documentURL";<STDIN>;
	my $page_inc=1;	
	
	Next_Page:
	if($Format eq 'Table')
	{
		
		my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($docPageContent,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$documentURL,$ApplicationURL,$Doc_URL_BY,$Config);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	elsif($Format eq 'JSON')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_JsonDocs_Details($docPageContent,$Application_No);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	else
	{
		my $Doc_Details_Hash_Ref_tmp=&Direct_Docs_Collect_Details($docPageContent);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	
	if($Pagination_Type eq 'N/A')
	{
		return (\%Doc_Details_Hash,$documentURL,$Code);
	}
	elsif($Pagination_Type eq 'By Format')
	{
		if($page_inc < $Total_no_Pages)
		{		
			print "page_inc	: $page_inc\n";
			print "Total_no_Pages	: $Total_no_Pages\n";
			# <STDIN>;
			
			$page_inc++;
			
			my $nextPage = $1 if($docPageContent=~m/<input\s*name=\"([^\"]*?)\"[^>]*?onclick=\"Navigate\(\'Results\.aspx\'\s*\+\s*[^>]*?\s*\'\?grdResultsP=\'\s*[^>]*?\s*\+\s*$page_inc\)\"\s*value=\"$page_inc\"[^>]*?>/is);
									
			my $nextPageLink = $pageFetch->find_link( text => $nextPage );		
			my $nextPageLink    = $pageFetch->uri();
			
			$nextPageLink=$nextPageLink."\?grdResultsP=".$page_inc;
			
			
			my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($nextPageLink,$pageFetch);	
			$docPageContent=$searchPageCon;			
			$nextPageLink=~s/\?grdResultsP=\d+$//gsi;
			
			# print "$nextPageLink\n";	
			
			# open(DC,">Search_Content$page_inc.html");
			# print DC $docPageContent;
			# close DC;
			# exit;
					
			goto Next_Page;
		}
	}
	elsif($Pagination_Type eq 'By Click') # For N628,N620,N621
	{		
		my $nxtPageCnt = $1 if($docPageContent=~m/$By_Click_Regex/is);
		
		$page_inc++;
		
		if($nxtPageCnt=~m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\'|\&\#39\;)([^\']*?)(\'|\&\#39\;)\,(\'|\&\#39\;)(\'|\&\#39\;)\)\"[^>]*?>\s*<span>\s*$page_inc\s*<\/span>\s*<\/a>/is)
		{		
			
			$pageFetch->form_number($FORM_NUMBER); 
			# my $next_page = $1 if($nxtPageCnt=~m/<a\s*href\s*=\s*\"javascript\:__doPostBack\(\'([^\']*?)\'\,\'\'\)\"[^>]*?>\s*<span>\s*$page_inc\s*<\/span>\s*<\/a>/is);
			my $next_page = $1 if($nxtPageCnt=~m/<input\s*type=\"submit\"\s*name=\"([^\"]*?)\"\s*value=\"\s*\"\s*title=\"Next\s*Page\"\s*class=\"rgPageNext\"\s*\/>/is);
			
			
			eval{$pageFetch->click( $next_page );};
							
			
			my $Page_url = $pageFetch->uri();
			my $searchPageCon = $pageFetch->content;
			my $searchCode= $pageFetch->status();
			
			$docPageContent = $searchPageCon;
			$Code = $searchCode;
			print "Click response: $Code\n";
			
			# open(DC,">Search_Content.html");
			# print DC $docPageContent;
			# close DC;
			# exit;
			goto Next_Page;
			
		}
	}
	
	return (\%Doc_Details_Hash,$documentURL,$Code,$Available_Doc_Count);
}

sub Collect_JsonDocs_Details()
{
	my $Content=shift;
	my $Application_No=shift;
	my %Doc_Details;
	
	my ($Doc_URL,$Doc_Desc,$pdf_name,$DateReceived,$document_type);
	
	my $json = JSON->new;
	my $data = $json->decode($Content);
	eval{
		if($Council_Code=~m/^(285|378)$/is)
		{
			my @KeyObjects = @{$data->{'CompleteDocument'}};
			
			foreach my $Keys ( @KeyObjects) 
			{	
				$Doc_Desc = $Keys->{'DocDesc'};
				$document_type = $Keys->{'Title'};
				my $DocNo = $Keys->{'DocNo'};
				$DateReceived = $Keys->{'DocDate'};
				
				
				my $tempURL = $FILTER_URL1;
				
				$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
				$tempURL=~s/<DOCNUM>/$DocNo/gsi;
				$Doc_URL = $tempURL;	
				$document_type = &Download_Utility::trim($document_type);
				$Doc_Desc=~s/\//\-/gsi;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				
				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
									
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				# <STDIN>;
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
				
			}
		}
		elsif($Council_Code=~m/^332$/is)
		{
			next if($Content=~m/\{\"data\"\:null\}/is);
			
			my @KeyObjects = $data->{'result'};
			
			foreach my $NewData ( @KeyObjects) 
			{	
				my @NewDataSet = $NewData->{'NewDataSet'};
				
				foreach my $Data ( @NewDataSet) 
				{
					my @NewData = @{$Data->{'data'}};
				
					foreach my $Keys ( @NewData) 
					{
						$Doc_Desc = $Keys->{'Description'};
						$document_type = $Keys->{'FileName'};
						my $DocNo = $Keys->{'ID_PhysicalDoc'};
						$DateReceived = $Keys->{'DateCreated'};
											
						my $tempURL = $FILTER_URL1;
						
						$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
						$tempURL=~s/<APPNUM>/$Application_No/gsi;
						$tempURL=~s/<DOCNUM>/$DocNo/gsi;
						$Doc_URL = $tempURL;	
						$document_type = &Download_Utility::trim($document_type);
						$Doc_Desc=~s/\//\-/gsi;
						
						my ($file_type);
						if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
						{
							$file_type=$1;
						}
						else
						{
							$file_type='pdf';
						}	
						
						
						if($Doc_Desc!~m/^\s*$/is)
						{
							$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
						}	
						else
						{
							$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
						}
						
											
						$pdfcount_inc++;
						
						print "Doc_URL	: $Doc_URL\n";
						print "pdf_name	: $pdf_name\n";
						print "file_type: $file_type\n";
						print "DateReceived: $DateReceived\n";
						# <STDIN>;
						
						if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
						{
							$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
						}
					}				
				}
			}
		}
		elsif($Council_Code=~m/^133$/is)
		{
			my @KeyObjects = @{$data->{'Documents'}};
			foreach my $Keys ( @KeyObjects) 
			{							
				$Doc_Desc = $Keys->{'DocType'};
				$document_type = $Keys->{'DocRef2'};
				$Doc_URL = $Keys->{'FilePath'};
				$DateReceived = $Keys->{'DateReceived'};
				
				$Doc_URL = $FILTER_URL.$Doc_URL if($Doc_URL!~m/^http/is);	
				
				$document_type=~s/\n+//gsi;
				$document_type=~s/\(([^<]*?)\)/$1/gsi;
				$document_type = &Download_Utility::trim($document_type);
				# print "document_type::$document_type\n"; <STDIN>;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
		
				if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDoc=$1;
					if($file_typeDoc eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDoc;
					}
				}
				elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDocType=$1;
					if($file_typeDocType eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDocType;
					}
				}
				
				
				if($document_type!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}
				$Doc_URL=~s/\&amp\;/\&/gsi;
									
				$pdfcount_inc++;
				
			
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
				
			}
		}
	};
	if($@)
	{
		print "ERR::: $@\n";
	}
	return (\%Doc_Details);	
}

sub Direct_Docs_Collect_Details()
{
	my $Content=shift;
	
	my %Doc_Details;
	my @urlArray;
	my ($Doc_URL,$Doc_Desc,$pdf_name,$document_type);
	if($Block_Regex1 ne 'N/A')
	{
		my @Block1 = $Content =~m/$Block_Regex1/igs;
		if($Block_Regex2 ne 'N/A')
		{
			for my $Block1(@Block1)
			{			
				my @Block2 = $Block1=~m/$Block_Regex2/igs;
				if($Block_Regex3 ne 'N/A')
				{
					for my $Block2(@Block2)
					{			
						
						while($Block2=~m/$Block_Regex3/gsi)
						{
							my $docHREF = $1;
							
							if($Docs_URL_Regex ne 'N/A')
							{
								if($docHREF=~m/$Docs_URL_Regex/is)
								{
									$Doc_URL = $1;
									$Doc_Desc = $2;									
									$Doc_URL = $Doc_Download_URL_ABS.$Doc_URL if($Doc_URL!~m/^http/is);	
									$Doc_Desc=~s/\//\-/gsi;
								}
							}
							
							my ($file_type);
							if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
							{
								$file_type=$1;
							}
							else
							{
								$file_type='pdf';
							}	
	
							if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
							{
								my $file_typeDoc=$1;
								if($file_typeDoc eq $file_type)
								{
									$file_type;
								}
								else
								{
									$file_type=$file_typeDoc;
								}
							}
							$Doc_URL=~s/\&amp\;/\&/gsi;
							
							$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
							
							$pdfcount_inc++;
							
							
							print "Doc_URL	: $Doc_URL\n";
							print "pdf_name	: $pdf_name\n";
							print "file_type: $file_type\n";
							# <STDIN>;
							
							if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
							{
								$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Doc_Desc];
							}	
						}				
					}	
				}
			}
		}
	}
	
	return (\%Doc_Details);	
}

sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $ApplicationURL=shift;
	my $Doc_URL_BY=shift;
	my $Config=shift;

	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	
	if ($Doc_Download_URL_Index =~m/([a-z]+)/is)
	{
		
		my ($Doc_Details_Hash_Ref_tmp,$p)=&Download_Utility::Doc_download_600_Html_Header($Config,$Content,$Home_URL,$Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY,$Council_Code,$pdfcount_inc);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
		
		$pdfcount_inc=$p;
	
	}
	else
	{
		
		$Content=~s/<td\/>/<td><\/td>/igs;
		if($Block_Regex1 ne 'N/A')
		{
			my @Block1 = $Content =~m/$Block_Regex1/igs;
			if($Block_Regex2 ne 'N/A')
			{
				for my $Block1(@Block1)
				{	
					my @Block2 = $Block1=~m/$Block_Regex2/igs;
					if($Block_Regex3 ne 'N/A')
					{
						for my $Block2(@Block2)
						{	
							
							if($Council_Code!~m/625/is)
							{
								$Block2=~s/(<a\s*href=[^<]*?blank\")(<\/a>)/$1\>$2/gsi;
								$Block2=~s/onclick=\"openDocman\(([^\)]*?)\)\">\s*(<td[^>]*?>)/<td><a href=$1><\/td>$2/gsi; #for N625,N615,N616,N627,N609,N630 council
								next if($Block2=~m/\%\%filename\.panot\%\%\%/is);
							}		
							
							my @Block3 = $Block2=~m/$Block_Regex3/igs;
							
							my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
							my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
							%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);

							
						}	
					}
					else
					{
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
			}	
			else
			{
				my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
				my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
			}	
		}
	}
	
	return \%Doc_Details_Hash;
}



sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	
	# open(DC,">Data.html");
		# print DC $Data;
		# close DC;
	# <STDIN>;
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}

	$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
		# open(DC,">PDF_URL_Cont.html");
		# print DC $PDF_URL_Cont;
		# close DC;	
		
	if($Council_Code=~m/609/is)
	{
		my $modules=$1 if($PDF_URL_Cont=~m/data\-module\=\"([^>]*?)\"/is);
		my $recordNumbers=$1 if($PDF_URL_Cont=~m/data\-record\=\"([^>]*?)\"/is);
		my $planIds=$1 if($PDF_URL_Cont=~m/data\-planid\=\"([^>]*?)\"/is);
		my $imageIds=$1 if($PDF_URL_Cont=~m/data\-imageid\=\"([^>]*?)\"/is);
		my $isPlans=$1 if($PDF_URL_Cont=~m/data\-isplans\=\"([^>]*?)\"/is);
		my $filenames=uri_escape($1) if($PDF_URL_Cont=~m/data\-filename\=\"([^>]*?)\"/is);
		
		
		# $Doc_URL="https://plan.dorsetcc.gov.uk/Document/DownloadSelected?modules=$modules&recordNumbers=$recordNumbers&planIds=$planIds&imageIds=$imageIds&isPlans=$isPlans&filenames=$filenames";
		$Doc_URL="https://plan.dorsetcc.gov.uk/Document/Download?module=$modules&recordNumber=$recordNumbers&planId=$planIds&imageId=$imageIds&isPlan=$isPlans&filename=$filenames";
		
	}
	
	if($Doc_URL_BY eq 'By Click')
	{
		my ($AppDoc,$ViewDoc);
				
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			$AppDoc=$1;
			$ViewDoc=$2;
			print "$AppDoc=$ViewDoc\n"; 
			# <STDIN>;
		}	
				
		$pageFetch->form_number($FORM_NUMBER1); 
		$pageFetch->set_fields($AppDoc => $ViewDoc);
		$pageFetch->click();	
				
		my $Page_url = $pageFetch->uri();
		# print "$Page_url\n";
		my $App_Page_Content = $pageFetch->content;
		
		if($Docs_URL_Regex ne 'N/A')
		{
			if($App_Page_Content=~m/$Docs_URL_Regex/is)
			{
				$Doc_URL = $1;
				
				$Doc_URL = $Doc_Download_URL_ABS.$Doc_URL if($Doc_URL!~m/^http/is);	
			}
		}
		# open(DC,">App_Page_Content.html");
		# print DC $App_Page_Content;
		# close DC;	
		# <STDIN>;
	}
	elsif($Doc_Download_URL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl=$1;				
			# print "Durl: $Durl\n"; <STDIN>;
			if($Doc_Download_URL_Replace ne 'N/A') 
			{		
				my $docID=$1 if($Durl=~m/Download\.aspx\?ID=(\d+)$/is);	
				$Doc_URL = $Doc_Download_URL_Replace;
				$docID=sprintf("%08d",$docID);
				$Doc_URL=~s/<SelectedID>/$docID/igs;
				# print "$Doc_URL\n"; <STDIN>;
			}
			elsif($Doc_Download_URL_ABS ne 'N/A') 
			{	
				if($Council_Code=~m/^8$/is)
				{
					$Doc_URL=$FILTER_URL.$Durl;
					if($Doc_Download_URL_ABS=~m/pa\.brent\.gov\.uk/is)
					{
						$Doc_URL;
					}
					else
					{
						my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);	
						$Doc_URL = $Doc_Download_URL_ABS.$1 if($searchPageCon=~m/<meta[^>]*?URL=\.\.\/\.\.\/([^\"]*?)\"[^>]*?>/is);
					}
				}	
				# elsif($Council_Code=~m/^603$/is)
				# {
					# $Doc_URL=$FILTER_URL.$Durl;
					# if($Durl=~m/planningregister\.opdc\.london\.gov\.uk/is)
					# {
						# $Doc_URL;
					# }
					# else
					# {
						# my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);	
						# $Doc_URL = $Doc_Download_URL_ABS.$1 if($searchPageCon=~m/<meta[^>]*?URL=\.\.\/\.\.\/([^\"]*?)\"[^>]*?>/is);
					# }
				# } 
				elsif($Durl!~m/^http/is)
				{
					$Doc_URL=$Doc_Download_URL_ABS.$Durl;
				}
				else
				{
					$Doc_URL=$Durl;
				}
				# print "Doc_URL1: $Doc_URL\n"; <STDIN>;
			}
			elsif($Durl!~m/^http/is)
			{							
				my $u1=URI::URL->new($1,$Root_Url);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
				# print "Root_Url: $Root_Url\n";
				# print "Doc_URL2: $Doc_URL\n"; <STDIN>;
			}
			else
			{	
				$Doc_URL=$Durl;		
				# print "Doc_URL2: $Doc_URL\n"; <STDIN>;
				
			}	
		}
	}
	
	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);	
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	
	if($Doc_URL=~m/^http\:\/\/planning\.cambridgeshire\.gov\.uk\/swift/is) # for N605 Council
	{
		my ($docPageContent,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
		my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
		$Doc_URL = "http://planning.cambridgeshire.gov.uk/swift".$URL if($URL!~m/^http/is);	
	}
	elsif($Doc_URL=~m/^http\:\/\/myeplanning2\.oxfordshire\.gov\.uk\/swiftlg/is) # for N624 Council
	{
		my ($docPageContent,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
		my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
		$Doc_URL = "http://myeplanning2.oxfordshire.gov.uk/swiftlg".$URL if($URL!~m/^http/is);	
	}
	elsif($Doc_URL=~m/^https?\:\/\/planning\.warwickshire\.gov\.uk\/swiftlg/is) # for N629 Council
	{
		my ($docPageContent,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
		my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
		$Doc_URL = "https://planning.warwickshire.gov.uk/swiftlg".$URL if($URL!~m/^http/is);	
	}
	elsif($Doc_URL=~m/^http\:\/\/e-planning\.worcestershire\.gov\.uk\/swift/is) # for N631 Council
	{
		my ($docPageContent,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
		my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
		$Doc_URL = "http://e-planning.worcestershire.gov.uk/swift".$URL if($URL!~m/^http/is);	
	}
	elsif($Council_Code=~m/^603$/is) # for N631 Council
	{
		my $seqno = $1 if($Doc_URL=~m/SeqNo=\s*([^>]*?)\s*&/is);
		my $appno = $1 if($Doc_URL=~m/theApnkey=\s*([^>]*?)\s*&/is);
		$Doc_URL = "http://planningregister.opdc.london.gov.uk/oak/MediaTemp/$appno-$seqno.pdf";
	}
				
	my ($file_type);
	if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	
	
	if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
	{
		my $file_typeDoc=$1;
		if($file_typeDoc eq $file_type)
		{
			$file_type;
		}
		else
		{
			$file_type=$file_typeDoc;
		}
	}
	elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
	{
		my $file_typeDocType=$1;
		if($file_typeDocType eq $file_type)
		{
			$file_type;
		}
		else
		{
			$file_type=$file_typeDocType;
		}
	}

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	$Doc_URL=~s/\&amp\;/\&/gsi;
	# print "Doc_URL	: $Doc_URL\n"; <STDIN>;	
	
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	
	$Doc_URL=decode_entities($Doc_URL);
	
	$pdfcount_inc++;
	
	if($Council_Code=~m/^(?:616|625|609|606)$/is)
	{
		# print "match\n";
		my $Doc_URL1=uri_escape($1) if($Doc_URL=~m/(\/Document\/Download[^>]*?)$/is);
		# # print "$Doc_URL1\n";
		# # $Doc_URL=~s/https:\/\/planning.somerset.gov.uk\//https:\/\/planning.somerset.gov.uk\/Disclaimer\/Accept\?returnUrl=/igs;
		$Doc_URL=~s/([^>]*?)(\/Document\/Download[^>]*?)$/$1\/Disclaimer\/Accept\?returnUrl=$Doc_URL1/igs;
		
	}
	print "Doc_URL	: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date	: $Published_Date\n"; 
	# <STDIN>;

	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	
	Loop_End:
	return (\%Doc_Details);
}

sub Search_Application()
{
	my $Application_No = shift;
	my $ApplicationURL = shift;

	my ($homePageContent,$Code) = &Download_Utility::docMechMethod($Home_URL,$pageFetch);		
	# open(DC,">homePageContent.html");
	# print DC $homePageContent;
	# close DC;	
	# exit;
	
	if($homePageContent=~m/Please\s*click\s*the\s*button\s*below\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is) # for N619
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$homePageContent = $pageFetch->content;
		$Code= $pageFetch->status();
		
	}
	elsif($homePageContent=~m/By\s*clicking\s*\'Accept\'\s*you\s*agree/is) # for N606
	{
		$pageFetch->form_number(3);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$homePageContent = $pageFetch->content;
		$Code= $pageFetch->status();		
	}
	
	my ($docPageContent,$docRespCode,$searchPageCon,$searchCode);
	
	if($Search_Type eq "By Click")
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $FORM_REFERENECE => $Application_No );
		$pageFetch->click();
			
		$searchPageCon = $pageFetch->content;
		$searchCode= $pageFetch->status();
	}
	else
	{
		my $appNum;
		$Application_No=~s/^Case\s*file\s*for\s*//gsi;
		$appNum = uri_escape($Application_No);
				
		my $viewState = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATE"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateZip = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATEZIP"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateGen = uri_escape($1) if($homePageContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"__VIEWSTATEGENERATOR"\s[^>]*?value="([^>]*?)"\s*\/>/is);
		my $eventVal = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__EVENTVALIDATION"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $Token = uri_escape($1) if($homePageContent=~m/<input\s*\s*name=\"__RequestVerificationToken[^>]*?value="([^>]*?)"[^>]*?>/is);
		
		$Search_POST =~s/<VIEWSTATE>/$viewState/gsi;
		$Search_POST =~s/<VIEWSTATEGENERATOR>/$viewStateGen/gsi;
		$Search_POST =~s/<EVENTVALIDATION>/$eventVal/gsi;
		$Search_POST =~s/<APPNUM>/$appNum/gsi;
		$Search_POST =~s/<Token>/$Token/gsi;
		$Search_POST =~s/<VIEWSTATEZIP>/$viewStateZip/gsi;
		# print "$Search_POST\n"; <STDIN>;
		
		($searchPageCon,$searchCode) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$Search_POST =~s/$appNum/<APPNUM>/gsi;
		$Search_POST =~s/$Token/<Token>/gsi;
		$Search_POST =~s/$viewState/<VIEWSTATE>/gsi;
		$Search_POST =~s/$viewStateGen/<VIEWSTATEGENERATOR>/gsi;
		$Search_POST =~s/$eventVal/<EVENTVALIDATION>/gsi;
		$Search_POST =~s/$viewStateZip/<VIEWSTATEZIP>/gsi;
		# print "$Search_POST\n"; <STDIN>;
	}

	if($Council_Code=~m/^(602|603|604|605|606|607|607|608|609|610|611|612|613|615|616|617|618|619|620|621|623|624|625|626|627|628|629|630|631|632)$/is)
	{
		if($searchPageCon=~m/<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/td>/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<dt>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/dt>/is) # for N619
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<dd><a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/dd>/is) # for N606
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<div[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/div>/is) # for N612
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<h4>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*$Application_No\s*<\/a>\s*<\/h4>/is) # for N608
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<td>\s*<a\s*href=(?:\'|\")([^\"]*?)(?:\'|\")[^>]*?>\s*<b>\s*<u>\s*<font[^>]*?>\s*$Application_No\s*<\/font>\s*<\/u>\s*<\/b>\s*<\/a>\s*<\/td>/is) # for N617
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
		elsif($searchPageCon=~m/<dt>Application\s*Number\s*<\/dt>\s*<dd>\s*<a\s*href=\"([^\"]*?)\">\s*$Application_No\s*<\/a>\s*<\/dd>/is) # for 618
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
	}
	elsif($Council_Code=~m/^8$/is)
	{
		if($searchPageCon=~m/<td\s*class=\"apas_tblContent\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*(?:<span>View[^<]*?<\/span>\s*)?([^<]*?)\s*<\/a>/is)
		{
			my $appURL=&clean($1);
			$appURL=~s/amp\;//igs;
			$appURL=$FILTER_URL.$appURL if($appURL!~m/^http/is);
			($searchPageCon,$searchCode) = &Download_Utility::docMechMethod($appURL,$pageFetch);				
		}
	}
			
		
	# open(DC,">docPageContent.html");
	# print DC $searchPageCon;
	# close DC;	
	# <STDIN>;
	# exit;	
	
	
	if(($searchPageCon=~m/$Doc_URL_Regex/is) && ($Doc_URL_Regex ne 'N/A'))
	{
		my $docURL = $1;
		$docURL=~s/amp\;//igs;
		
		if($docURL!~m/^http\:/is)
		{
			$docURL = $FILTER_URL.$docURL;
		}	
		
		($docPageContent,$docRespCode) = &Download_Utility::docMechMethod($docURL,$pageFetch);	
	}
	else
	{
		$docPageContent = $searchPageCon;
	}
	
	return($docPageContent,$docRespCode);
}


sub clean()
{
	my $Data=shift;
	$Data=~s/\s*<[^>]*?>\s*//igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;//igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&nsbp//igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	return($Data);
}

sub QueryExecuteMax()
{	
	my ($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query)=@_;
	
	if($Insert_Document!~m/values\s*$/is)
	{
		$Insert_Document=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

		$Update_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

		$MD5_Insert_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);
		
		$Insert_Document="";
		$Update_Query="";
		$MD5_Insert_Query="";
		
		$Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
		$MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

		$MaxCount=0;
	}
	return ($MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
}