use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use JSON::Parse 'parse_json';
use Cwd qw(abs_path);
use File::Basename;
use JSON;
use IO::Socket::SSL qw();
# use IO::Socket::SSL qw(SSL_VERIFY_NONE);
# use Net::SSL;
use LWP::Simple;

use WWW::Mechanize;
my $mechz=WWW::Mechanize->new(autocheck => 0);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# $basePath="D:\\Glenigan\\Document_download\\";
# print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 
# require ($libDirectory.'/onlinegetUtility500.pm'); 

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];

print "Council_Code: $Council_Code\n";
print "inputFormatID: $inputFormatID\n";
print "onDemand: $onDemand\n";
print "onDemandID: $onDemandID\n";

if($Council_Code=~m/^(?:274|174)$/is)
{
	require ($libDirectory.'/onlinegetUtility500.pm'); 
}

# my $Council_Code=166,15,455,8,218;

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

# &Download_Utility::User_Agent_Define();

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);


#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";
# my $Input_Table = 'FORMAT_PUBLIC_ACCESS';
# my $Output_Table = 'format_pdf';
# my $Log_Table = 'OneAppLog';
# my $MD5_Table = 'TBL_PDF_MD5';
# my $DashBoard_Table = 'TBL_ONEAPP_STATUS';
my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|Form?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	
&Download_DB::Execute($dbh,$Dashboard_Insert_Query);

if($onDemand eq 'Y')
{
	print "This is an On demand request\n";
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\'";	
	print "onDemand_Update_Query: $onDemand_Update_Query\n";
	&Download_DB::Execute($dbh,$onDemand_Update_Query);
}

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;

my $MaxCount=0;
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $Proxy = $Config->{$Council_Code}->{'Proxy'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Accept_Cookie = $Config->{$Council_Code}->{'Accept_Cookie'};
my $Accept_Cookie_URL = $Config->{$Council_Code}->{'Accept_Cookie_URL'};
my $Accept_Post_Content = $Config->{$Council_Code}->{'Accept_Post_Content'};
my $Accept_Host = $Config->{$Council_Code}->{'Accept_Host'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Need_Host = $Config->{$Council_Code}->{'Need_Host'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $Accept_Language = $Config->{$Council_Code}->{'Accept_Language'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Landing_Page_Regex2 = $Config->{$Council_Code}->{'Landing_Page_Regex2'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $CSRFToken_Regex = $Config->{$Council_Code}->{'CSRFToken_Regex'};
my $Doc_Page_Num_Regex = $Config->{$Council_Code}->{'Doc_Page_Num_Regex'};
my $Doc_NumberOfRows_Regex = $Config->{$Council_Code}->{'Doc_NumberOfRows_Regex'};
my $Doc_POST_ID1_Regex = $Config->{$Council_Code}->{'Doc_POST_ID1_Regex'};
my $Doc_Post_Content = $Config->{$Council_Code}->{'Doc_Post_Content'};
my $Doc_Post_URL = $Config->{$Council_Code}->{'Doc_Post_URL'};
my $Doc_Post_Host = $Config->{$Council_Code}->{'Doc_Post_Host'};
my $DOC_PDF_ID_URL = $Config->{$Council_Code}->{'DOC_PDF_ID_URL'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Block_Regex4 = $Config->{$Council_Code}->{'Block_Regex4'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Download_URL_redirct = $Config->{$Council_Code}->{'Doc_Download_URL_redirct'}; #New
my $Doc_Download_URL_redirct_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_Regex'};#New
my $Doc_Download_URL_redirct_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_ABS'};#New
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Current_Page_Number_Regex = $Config->{$Council_Code}->{'Current_Page_Number_Regex'};
my $Next_Page_Post_Content = $Config->{$Council_Code}->{'Next_Page_Post_Content'};
my $Next_Referer = $Config->{$Council_Code}->{'Next_Referer'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Application_Regex = $Config->{$Council_Code}->{'Application_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};

$Method = 'N/A' if($Method eq '');	$Proxy = 'N/A' if($Proxy eq '');	$Format = 'N/A' if($Format eq '');	$Home_URL = 'N/A' if($Home_URL eq '');	
$Accept_Cookie = 'N/A' if($Accept_Cookie eq '');	$Accept_Cookie_URL = 'N/A' if($Accept_Cookie_URL eq '');	$Accept_Post_Content = 'N/A' if($Accept_Post_Content eq '');	
$Accept_Host = 'N/A' if($Accept_Host eq '');	$Host = 'N/A' if($Host eq '');	$Need_Host = 'N/A' if($Need_Host eq '');	$Content_Type = 'N/A' if($Content_Type eq '');	
$Abs_url = 'N/A' if($Abs_url eq '');	$Doc_URL_Regex = 'N/A' if($Doc_URL_Regex eq '');	$Referer = 'N/A' if($Referer eq '');	$Post_Content = 'N/A' if($Post_Content eq '');	
$Landing_Page_Regex = 'N/A' if($Landing_Page_Regex eq '');	$Landing_Page_Regex2 = 'N/A' if($Landing_Page_Regex2 eq '');	$Doc_URL_BY = 'N/A' if($Doc_URL_BY eq '');	
$Viewstate_Regex = 'N/A' if($Viewstate_Regex eq '');	$Viewstategenerator_Regex = 'N/A' if($Viewstategenerator_Regex eq '');	$Eventvalidation_Regex = 'N/A' if($Eventvalidation_Regex eq '');	
$CSRFToken_Regex = 'N/A' if($CSRFToken_Regex eq '');	$Doc_Page_Num_Regex = 'N/A' if($Doc_Page_Num_Regex eq '');	$Doc_NumberOfRows_Regex = 'N/A' if($Doc_NumberOfRows_Regex eq '');
$Doc_POST_ID1_Regex = 'N/A' if($Doc_POST_ID1_Regex eq '');	$Doc_Post_Content = 'N/A' if($Doc_Post_Content eq '');	$Doc_Post_URL = 'N/A' if($Doc_Post_URL eq '');	
$Doc_Post_Host = 'N/A' if($Doc_Post_Host eq '');	$DOC_PDF_ID_URL = 'N/A' if($DOC_PDF_ID_URL eq '');	$Block_Regex1 = 'N/A' if($Block_Regex1 eq '');	$Block_Regex2 = 'N/A' if($Block_Regex2 eq '');	
$Block_Regex3 = 'N/A' if($Block_Regex3 eq '');	$Block_Regex4 = 'N/A' if($Block_Regex4 eq '');	$Doc_Published_Date_Index = 'N/A' if($Doc_Published_Date_Index eq '');	
$Doc_Type_Index = 'N/A' if($Doc_Type_Index eq '');	$Doc_Description_Index = 'N/A' if($Doc_Description_Index eq '');	$Doc_Download_URL_Index = 'N/A' if($Doc_Download_URL_Index eq '');	
$Doc_Download_URL_Regex = 'N/A' if($Doc_Download_URL_Regex eq '');	$Doc_Download_URL_Replace = 'N/A' if($Doc_Download_URL_Replace eq '');	$Doc_Download_URL_redirct = 'N/A' if($Doc_Download_URL_redirct eq '');	
$Doc_Download_URL_redirct_Regex = 'N/A' if($Doc_Download_URL_redirct_Regex eq '');	$Doc_Download_URL_redirct_ABS = 'N/A' if($Doc_Download_URL_redirct_ABS eq '');	
$Doc_Download_URL_ABS = 'N/A' if($Doc_Download_URL_ABS eq '');	$Pagination_Type = 'N/A' if($Pagination_Type eq '');	$Total_No_Of_Pages_Regex = 'N/A' if($Total_No_Of_Pages_Regex eq '');	
$Next_Page_Regex = 'N/A' if($Next_Page_Regex eq '');	$Next_Page_Replace_Info = 'N/A' if($Next_Page_Replace_Info eq '');	$Next_Page_Replace_Key = 'N/A' if($Next_Page_Replace_Key eq '');	
$Current_Page_Number_Regex = 'N/A' if($Current_Page_Number_Regex eq '');	$Next_Page_Post_Content = 'N/A' if($Next_Page_Post_Content eq '');	$Next_Referer = 'N/A' if($Next_Referer eq '');	
$Abs_url = 'N/A' if($Abs_url eq '');	$Application_Regex = 'N/A' if($Application_Regex eq '');	$Available_Doc_Count_Regex = 'N/A' if($Available_Doc_Count_Regex eq '');	$Accept_Language = 'N/A' if($Accept_Language eq '');	

my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;

my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	print "Format_ID :: $Format_ID \n";
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code, $Available_Doc_Count);
	my $Landing_Page_Flag=0;
	
	($Doc_Details_Hash_Ref, $Document_Url, $Code,$Available_Doc_Count)=&Extract_Link($Method, $Home_URL, $Document_Url, $URL,\%Nextpage_Replace_Hash, $Application_No,$Format_ID);

	$Site_Status = $Code;
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;

	$Todays_Count = $Todays_Count + $pdfcount;
	if($Available_Doc_Count = ~m/^\s*$/is)
	{
		$Available_Doc_Count = $pdfcount;
	}	
	my $RetrieveMD5=[];
	my $No_Of_Doc_Downloaded=0;
	# if(lc($Markup) eq 'large')
	# {
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;
	print "Temp_Download_Location=>$Temp_Download_Location\n";
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====> $Temp_Download_Location\n";
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		print "pdf_url: $pdf_url\n";
		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		
		$Pdf_Name=~s/WITHOUT_PRESONAL_DATA/Application_Form_without_personal_data/igs;
		$Pdf_Name=~s/WITHOUT_PERSONAL_DATA/Application_Form_without_personal_data/igs;
		if(($Doc_Type=~m/Application(?:\s+|_|-)?Form/is) && ($Council_Code =~m/(?:54|312)/is))
		{
			$Pdf_Name=~s/Application/Application_Form/igs;
		}
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my ($Download_Status);
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					
					($Download_Status)=&Download_Utility::Url_Status($D_Code);
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;

					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
				}
				# my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				# $Insert_Document.=$Insert_Document_List;
			}
		}	

		#--------- DB INSERTION above 1000 record ---------------
		$MaxCount++;
		if($MaxCount == 995)
		{
			($MaxCount, $Insert_Document, $MD5_Insert_Query, $Update_Query) = QueryExecuteMax($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
		}
	}	

	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	&Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		# $No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
		my $No_Of_Doc_Downloaded_TMP = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
		# open(NDD,">>No_Of_Doc_Downloaded_Count.xls");
		# print NDD "Differ\t$Council_Code\t$Format_ID\t$Markup\t$No_Of_Doc_Downloaded\t$NO_OF_DOCUMENTS\t$pdfcount\t$Downloaded_date\n";
		# close NDD;
		$No_Of_Doc_Downloaded=$No_Of_Doc_Downloaded_TMP;
	}	
	# else
	# {
		# open(NDD,">>No_Of_Doc_Downloaded_Count.xls");
		# print NDD "Same\t$Council_Code\t$Format_ID\t$Markup\t$No_Of_Doc_Downloaded\t$NO_OF_DOCUMENTS\t$pdfcount\t$Downloaded_date\n";
		# close NDD;
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";
	

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if($One_App_Name=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}	
	if ($onDemand eq 'ApprovedDecision')
	{
		my $Flag_Update_query = "update GLENIGAN..TBL_APPROVED_LARGE_PROJECT set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\', All_Documents_Downloaded = 'Y' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
		$ApprovedDecisionUpdate_query.= $Flag_Update_query;
	}	
	$pdfcount_inc=1;
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

print "\n$Insert_OneAppLog\n";
print "\n$Insert_Document\n";

$Insert_OneAppLog=~s/\,$//igs;
# print "\n$Insert_OneAppLog\n";
&Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\n$Insert_Document\n";
&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
&Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);

if($onDemand eq 'Y')
{
	print "Update & Delete This Ondemand request\n";
	my $onDemandStatus = &Download_Utility::onDemandStatus($No_Of_Doc_Downloaded_onDemand,$Site_Status);
	my $onDemandUpdateQuery = "update tbl_on_demand_download_backup set Status = \'$onDemandStatus\' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and download_id = \'$onDemandID\'";
	print "onDemandUpdateQuery: $onDemandUpdateQuery\n";
	&Download_DB::Execute($dbh,$onDemandUpdateQuery);
	
	my $onDemandDeleteQuery = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and status = 'Inprogress'";	
	&Download_DB::Execute($dbh,$onDemandDeleteQuery);
}
print "Download Completed\n";


sub Extract_Link()
{
	my $Method = shift;
	my $Home_URL = shift;
	my $Document_Url = shift;
	my $URL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $Application_No = shift;
	my $Format_ID = shift;
	print "Extract_Link\n";
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my $Scrp_Flag=0;
	my ($Content,$Code,$Redir_Url,$Doc_Details_Hash_Ref,%Doc_Details_Hash,$Previous_page,$Post_Responce,$Post_Res_code,$Available_Doc_Count);
	my ($Landing_Page_Flag,$Landing_Page_Flag2)=(0,0);
	my $Previous_page=0;
	
	if($Council_Code=~m/^171$/is)
	{
		$Document_Url=~s/activeTab=[^\&]*?\&/activeTab=externalDocuments\&/is;
	}
	if($Council_Code=~m/^23$/is)
	{
		$Document_Url=~s/\&quot\;/\"/isg;
	}
	
	if($Council_Code=~m/^615$|^249$/is)
	{
		$Document_Url=~s/\/Planning\//\/Disclaimer\/Accept\?returnUrl\=\/Planning\//is;
	}
	if($Council_Code=~m/^304$/is)
	{
		$Document_Url="http://eplandocs.bridgend.gov.uk/PublicAccess_Live/SearchResult/RunThirdPartySearch?FileSystemId=PC&FOLDER1_REF=$Application_No";
	}	
	if($Council_Code=~m/^361$/is)
	{
		($Content,$Code,$Redir_Url) = &Download_Utility::docMechMethod($URL,$pageFetch);
		
		$Document_Url='https://southderbyshirepr.force.com/s/sfsites/aura?r=8&arcshared.FilesPublicCont.getFiles=1&arcshared.MetadataServiceCont.getSecureDesignerAttributes=1&ui-communities-components-aura-components-forceCommunity-richText.RichText.getParsedRichTextValue=1';
		
		$Referer=$URL."?tabset-ba98d=2";
	}
	if($Council_Code=~m/^92|489$/is)
	{
		($Content,$Code,$Redir_Url) = &Download_Utility::Getcontent($URL,$pageFetch);
		
		$Document_Url="https://$Host/pr/s/sfsites/aura?r=11&arcshared.FilesPublicCont.getFiles=1&arcshared.MetadataServiceCont.getSecureDesignerAttributes=1";
		
		$Referer="$URL?tabset-e3f5c=2";
	}
	if($Council_Code=~m/^353$/is)
	{	
		($Content,$Code,$Redir_Url) = &Download_Utility::docMechMethod($URL,$pageFetch);
		$Document_Url='https://folkestonehythedc.force.com/pr/s/sfsites/aura?r=11&arcshared.FilesPublicCont.getFiles=1&arcshared.MetadataServiceCont.getSecureDesignerAttributes=1&ui-communities-components-aura-components-forceCommunity-richText.RichText.getParsedRichTextValue=1';

		$Referer=$URL."?tabset-185b1=2";
	}
	if($Accept_Cookie ne 'N/A')
	{
		
		($Content,$Code,$Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host);
		my $Viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		my $Viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		my $Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		my $Accept_Post_Content_Tmp=$Accept_Post_Content;
		$Accept_Post_Content_Tmp=~s/<VIEWSTATE>/$Viewstate/igs;
		$Accept_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Viewstategenerator/igs;
		$Accept_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Eventvalidation/igs;
		print "$Accept_Post_Content_Tmp";
		
		if(($Document_Url=~m/Display\/([^>]*?)$/is) && ($Council_Code == 614))
		{
			$Accept_Cookie_URL="$Accept_Cookie_URL$1";
		}
		
		($Post_Responce,$Post_Res_code)=&Download_Utility::Post_Method($Accept_Cookie_URL,$Accept_Host,$Content_Type,$Accept_Cookie_URL,$Accept_Post_Content_Tmp,$Proxy);
		# open(DC,">Post_Responce.html");
		# print DC $Post_Responce;
		# close DC;
		
		if($Council_Code == 614)
		{
			$Content=$Post_Responce;
		}
		
		
	}
	
	
	
	Re_Ping:
	if($Method=~m/GET/is)
	{
		print "Ping\n";
		if($Council_Code=~m/^(211|171)$/is)
		{
			$Document_Url =~s/activeTab=documents?/activeTab=externalDocuments/is;
		}
		
		if($Council_Code=~m/^(15)$/is)
		{
			$Document_Url =~s/activeTab=externalDocuments/activeTab=documents/is;
		}
		elsif($Council_Code=~m/^(107)$/is)
		{
			$Document_Url='https://www.bathnes.gov.uk/planningdocuments='.$Application_No;
		}
		elsif($Council_Code=~m/^(395)$/is)
		{
			$Document_Url=~s/www2/www3/igs;
		}
		
		if($Council_Code=~m/^386|174$/is)
		{
			($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
		}
		elsif($Council_Code==273)
		{
			goto Next_Page if($Document_Url eq "");
			
			$mechz->get($Document_Url);
			$Code=$mechz->status();
			$Content=$mechz->content();
			
			# ($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
			
			# print "Code1:: $Code\n";<STDIN>;
			if($Content=~m/$Landing_Page_Regex/is)
			{
				my $relatedDocURL= $1;
				$relatedDocURL=~s/&amp;/&/igs;
				
				$mechz->get($relatedDocURL);
				$Code=$mechz->status();
				$Content=$mechz->content();
				
				# print "Code2:: $Code\n";<STDIN>;
			}
		}
		elsif($Council_Code=~m/^361$|^353$|^92|489$/is)
		{		
			my ($key1,$key2);
						
			if($Referer=~m/application\/([^>]*?)\/([^>]*?)\?/is)
			{
				$key1=$1;
				$key2=$2;
			}
			if($Referer!~m/application\/([^>]*?)\/([^>]*?)\?/is)
			{
				next;
			}
			
			my $Next_Page_Post_Content_Tmp=$Post_Content;
		
			$Next_Page_Post_Content_Tmp=~s/<KEY1>/$key1/igs;
			$Next_Page_Post_Content_Tmp=~s/<KEY2>/$key2/igs;
			my($FWUID,$Commapp);
			$FWUID=$1 if($Content=~m/"\s*fwuid\s*"\s*:\s*"\s*([^>]*?)\s*"/is);
			$Commapp=$1 if($Content=~m/"APPLICATION\@markup:\/\/siteforce:communityApp\s*\"\s*:\s*"\s*([^>]*?)\s*"/is);
			
			if(($FWUID=~m/^\s*$/is)||($Commapp=~m/^\s*$/is))
			{
				$FWUID=$1 if($Content=~m/fwuid%22%3A%22\s*([^>]*?)\s*%22/is);
				$Commapp=$1 if($Content=~m/siteforce%3AcommunityApp%22%3A%22\s*([^>]*?)\s*%22/is);
			}
			$Next_Page_Post_Content_Tmp=~s/<FWUID>/$FWUID/igs;
			$Next_Page_Post_Content_Tmp=~s/<COMMAPP>/$Commapp/igs;
			
			($Content,my $res_code)=&Download_Utility::Post_Method($Document_Url,$Host,$Content_Type,$Referer,$Next_Page_Post_Content_Tmp,$Proxy);			
			
			if($Content=~m/\*ERROR\*/is)
			{
				print "\n next format id\n";
				next;
			}		
		}
		else
		{			
			if($Council_Code=~m/^202$/is)
			{			
				$Document_Url=~s/http:/https:/igs;
			}
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
			
			if($Council_Code=~m/^202$/is)
			{
				if($Content=~m/href\s*=\s*"\s*([^>]*?)\s*"[^>]*?>\s*Allow\s*Cookies\s*</is)
				{	
					my $Document_Url1="https://www.fareham.gov.uk/$1";
					
					$Referer=$Document_Url;
					($Content,$Code) =&Download_Utility::Getcontent($Document_Url1,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
					
				}
				my $postcontent_tmp=$Post_Content;				
				
				my $VS=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
				my $VSG=$1 if($Content=~m/$Viewstategenerator_Regex/is);
				my $EV=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
				
				$postcontent_tmp=~s/<VIEWSTATE>/$VS/igs;
				$postcontent_tmp=~s/<VIEWSTATEGENERATOR>/$VSG/igs;
				$postcontent_tmp=~s/<EVENTVALIDATION>/$EV/igs;
				# $Referer=$Document_Url;
				my($Post_Responce,$res_code)=&Download_Utility::Post_Method($Document_Url,$Host,$Content_Type,$Referer,$postcontent_tmp,$Proxy);
				$Content=$Post_Responce;
				$Code=$res_code;
			}
			if($Council_Code==160)
			{
				my $seid=$1 if($Content=~m/jsessionid\=([^>]*?)\"/is);
				
				$Document_Url="https://d0cs.colchester.gov.uk/publisher/mvc/getDocumentList;jsessionid=$seid?_=".time();

				 ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
				 # print "Content--160::$Content\n";
				# open(DC,">Doc_Content-160.html");
				# print DC $Content;
				# close DC;
			}
			if($Council_Code==107)
			{
				my $seid=$1 if($Content=~m/jsessionid\=([^>]*?)\"/is);
				$Referer=$Document_Url;
								
				$Document_Url="https://www.bathnes.gov.uk/publisher/mvc/getDocumentList;jsessionid=$seid?_=".time();
								
				$Content_Type = 'application/json; charset=UTF-8';
							
				 ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
				 # print "Content--107::$Content\n";
				 # open(DC,">Doc_Content-107.html");
				# print DC $Content;
				# close DC;
			}elsif($Council_Code=~m/^274$/is)
			{
				if($Content=~m/$Landing_Page_Regex/is)
				{
					$Document_Url= $1;
					$Document_Url=~s/&amp;/&/igs;
					
					($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
				}
			}
			# else
			# {
				# ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
			# }
		}
		
		if($Document_Url eq "")
		{
			print "Method:\t$Method. But Document URL not found for this App ID\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
			
			
			if($Content=~m/$Doc_URL_Regex/is)
			{
				my $relatedDocURL= $1;
				my $DocUrl=$Abs_url.$relatedDocURL if($relatedDocURL!~m/^https?\:/is);
				($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($DocUrl,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
				# $Content,$Code =&Download_Utility::Getcontent1($Document_Url);
			}
		}
		# open(DC,">Doc_Content.html");
		# print DC $Content;
		# close DC;
		
		if($Code!~m/20/is)
		{
			my $pdfcount=0;
			my $No_Of_Documents_Avail=0;
			my $No_Of_Doc_Downloaded=0;
			my $pdf_name_list;
			my $pdf_Url_list;
			my $One_App_Avail_Status;
			my $Comments=$Code;
			$Document_Url=~s/\'/\'\'/igs;
			my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$No_Of_Documents_Avail\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";
			$Insert_OneAppLog.= $insert_query;
			
			my $Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Terminated\', Responce_Code = \'$Code\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
			$Update_Query.= $Update_Query;
		}
	}
	elsif($Method eq 'Search')
	{
		($Content,$Post_Res_code)=&Search_Application($Application_No, $Council_Code);
		# open(DC,">Post_Responce.html");
		# print DC "$Post_Responce";
		# close DC;		
		if($Content=~m/\*ERROR\*/is  && $Council_Code=~m/^190$/is)
		{
			print "\n next format id\n";
			next;
		}	
	}	

	if($Landing_Page_Flag == 0)
	{
		if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
		{
			if($Content=~m/$Landing_Page_Regex/is)
			{
				my $DocUrl=$1;
				$DocUrl=~s/\&amp\;/\&/gsi;
				my $u1=URI::URL->new($DocUrl,$Document_Url);
				$Document_Url=$u1->abs;
				print "Landing : $Document_Url\n";
				$Landing_Page_Flag = 1;
				goto Re_Ping;
			}
		}	
	}	
	if($Landing_Page_Flag2 == 0)
	{
		if(($Landing_Page_Regex2 ne 'N/A') and ($Landing_Page_Regex2 ne ''))
		{
			if($Content=~m/$Landing_Page_Regex2/is)
			{
				my $DocUrl=$1;
				my $u1=URI::URL->new($DocUrl,$Document_Url);
				$Document_Url=$u1->abs;
				print "Landing URL: $Document_Url\n";
				$Landing_Page_Flag2 = 1;
				goto Re_Ping;
			}
		}	
	}	
	
	my ($Total_no_Pages);
	if($Total_No_Of_Pages_Regex ne 'N/A')
	{
		if($Content=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_no_Pages=$1;
		}
	}
	print "Total_no_Pages: $Total_no_Pages\n";
	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($Content=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
	}	
	my $page_inc=1;

	my $Ltime = localtime();
	$Ltime=~s/\s+/_/igs;
	$Ltime=~s/\://igs;
	print "Time: $time\n";
	
	my $htmlFileLocation = '//172.27.137.202/One_app_download/SourceFiles/'.$Council_Code.'_'.$Format_ID.'_'.$Ltime.'.html';
	# print "htmlFileLocation: $htmlFileLocation\n";
	
	# if($Council_Code=~m/^207$/i)
	# {
		# open(DC,">htmlFileLocation.html");
		# print DC "$Content";
		# close DC;	
		# exit;
	# }
	
	Next_Page:
	if($Council_Code == 487)
	{
		# $Content=~s/<input[^>]*?value\s*=\s*\"([^>]*?)\"\s*onclick[^>]*?>\s*<\/td>/$1<\/td>/igs;
		$Content=~s/(<input[^>]*?value\s*=\s*\")([^>]*?)(\"\s*onclick[^>]*?>)\s*(<\/td>)/$1$2$3$2$4/igs;
	}
	if($Format eq 'Table')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($Content,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	elsif($Format eq 'JSON')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_JsonDocs_Details($Content,$Application_No,$Host);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	
	if($Pagination_Type eq 'N/A')
	{
		return (\%Doc_Details_Hash,$Document_Url,$Code);
	}
	elsif($Pagination_Type eq 'By URL')
	{
		$page_inc++;
		my $Next_Page_Regex_temp=$Next_Page_Regex;
		$Next_Page_Regex_temp=~s/<PageNum>/$page_inc/igs;
		print $Next_Page_Regex_temp;
		if($Content=~m/$Next_Page_Regex_temp/is)
		{
			my $Nextpagelink = $1;
			my $u1=URI::URL->new($Nextpagelink,$Document_Url);
			my $Next_page_link=$u1->abs;
			$Next_page_link=~s/\&amp\;/\&/igs;
			# print "Next_page_link: $Next_page_link\n"; <STDIN>;
			($Content,my $code,my $reurl)=&Download_Utility::Getcontent($Next_page_link,"","","","",$Proxy,$Need_Host);
			goto Next_Page;
		}
	}	
	elsif($Pagination_Type eq 'By Number')
	{
		print "page_inc	: $page_inc\n";
		print "Total_no_Pages	: $Total_no_Pages\n";
		if($page_inc < $Total_no_Pages)
		{
			my ($Next_page_link,$Next_Page_Values);
			my @Next_Page_Values = $Content=~m/$Next_Page_Regex/g;
			
			if($Next_Page_Replace_Info ne 'N/A')
			{
				my @Replace_array_Raw=split('\|',$Next_Page_Replace_Info);
				# print "RedirectURL=>$Redir_Url\n";
				$Next_page_link = $Redir_Url if($Replace_array_Raw[0]=~m/Redir_Url/is);
				
				print "Next_page_link=>$Next_page_link";
				if($Replace_array_Raw[2]=~m/AllValues/is)
				{
					$Next_Page_Values = join("",@Next_Page_Values);
				}	
				# print "Next_Page_Values=>$Next_Page_Values\n";
				# <STDIN>;
				
				if($Replace_array_Raw[1]=~m/Replace/is)
				{
					for my $Replace_Key(keys %Nextpage_Replace_Hash)
					{
						# print "Replace_Key==>$Replace_Key\n";
						if($Nextpage_Replace_Hash{$Replace_Key} eq '')
						{
							$Next_page_link=~s/$Replace_Key/$Next_Page_Values/igs;
							# print "Next_page_link1==>$Next_page_link\n";
						}	
						else
						{
							$Next_page_link=~s/$Replace_Key/$Nextpage_Replace_Hash{$Replace_Key}/igs;
							# print "Next_page_link2==>$Next_page_link\n";
						}
					}
				}
				print "Next_page_link: $Next_page_link\n";
				# elsif($Replace_array_Raw[1]=~m/Append/is)
				# {
					# $Next_Page_Values = join("",@Next_Page_Values);
				# }
			}
			$page_inc++;
			if($Council_Code == 273){
				my $Session_273_Id=$1 if($Content=~m/<link\s*href\=\"\/PublicAccess\.WebSearch\/([^>]*?)\//is);
				
				$Next_page_link="https://padocs.midsussex.gov.uk/PublicAccess.websearch/$Session_273_Id/Results.aspx?grdResultsP=$page_inc";
				$mechz->get($Next_page_link);
				$Code=$mechz->status();
				$Content=$mechz->content();
				# ($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
			}else
			{
				($Content,my $code,my $reurl)=&Download_Utility::Getcontent($Next_page_link,"","","","",$Proxy,$Need_Host);
			}
			
			# print "Next_page_link: $Next_page_link\n";<STDIN>;
			
			goto Next_Page;
		}	
	}
	elsif($Pagination_Type eq 'By Format')
	{
		if($Content=~m/$Current_Page_Number_Regex/is)
		{
			my $Current_Page=$1;
			my ($Doc_viewstate,$Doc_viewstategenerator,$CSRFToken,$Doc_Eventvalidation);
			print "Current_Page: $Current_Page\n";
			if($Viewstate_Regex ne 'N/A')
			{
				$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
				print "Doc_viewstate: $Doc_viewstate\n";
			}	
			if($Viewstategenerator_Regex ne 'N/A')
			{
				$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
			}	
			if($CSRFToken_Regex ne 'N/A')
			{
				$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
			}	
			if($Eventvalidation_Regex ne 'N/A')
			{
				$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			}	
			if($Previous_page ne $Current_Page)
			{
				$Previous_page=$Current_Page;
				

				my $Next_Page_Post_Content_Tmp = $Next_Page_Post_Content;
				$Next_Page_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
				$Next_Page_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
				$Next_Page_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
				$Next_Page_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
				# $Next_Page_Post_Content_Tmp=~s/Next_Page_Post_Content\s*=\s*//igs;
				if($Next_Referer=~m/document_Url/is)
				{
					$Next_Referer=$Document_Url;
				}
				$Doc_Post_Host=$Host if($Doc_Post_Host eq 'N/A');
				# print "Doc_Post_URL: $Doc_Post_URL\nDoc_Post_Host: $Doc_Post_Host\nContent_Type: $Content_Type\nNext_Referer: $Next_Referer\nNext_Page_Post_Content_Tmp: $Next_Page_Post_Content_Tmp\nProxy: $Proxy\n"; <STDIN>;
				($Content,my $res_code)=&Download_Utility::Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Next_Referer,$Next_Page_Post_Content_Tmp,$Proxy);
				goto Next_Page;
			}
		}
	}
	
	# open(DC,">Post_Responce_1.html");
	# print DC $Content;
	# close DC;
		
	return (\%Doc_Details_Hash,$Document_Url,$Code,$Available_Doc_Count);
}
sub Collect_JsonDocs_Details()
{
	my $Content=shift;
	my $Application_No=shift;
	my $Host=shift;
	my %Doc_Details;
	
	my ($Doc_URL,$Doc_Desc,$pdf_name,$DateReceived,$document_type);
	my $json = JSON->new;
	my $data = $json->decode($Content);
	eval{
		if($Council_Code=~m/^377$/is)
		{
			my @navigation_menus=@{$data->{'data'}};
			# print Dumper@navigation_menus;<STDIN>;
			foreach my $Keys ( @navigation_menus) 
			{
				# print $Keys->[0];<STDIN>;
				my $document_type =$Keys->[0];
				my $DateReceived =$Keys->[1];
				my $Doc_Desc =$Keys->[2];
				my $docurl =$Keys->[3];
				
				$Doc_URL="https://docs.spelthorne.gov.uk/publisher$docurl";
				# print "Docurl::$Doc_URL";<STDIN>;
				$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
				$document_type = &Download_Utility::Clean($document_type);
				$DateReceived = &Download_Utility::Clean($DateReceived);
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}
				$pdfcount_inc++;
			}
		}
		if($Council_Code=~m/^92|489$/is)
		{
			my @navigation_menus=@{$data->{'actions'}[1]->{'returnValue'}};
			foreach my $Keys ( @navigation_menus) 
			{
				my $ContentDocumentId = $Keys->{'ContentDocumentId'};
				$DateReceived = $Keys->{'CreatedDate'};
				$document_type = $Keys->{'arcshared__Category__c'};
				$Doc_Desc = $Keys->{'Title'};			
				
				$Doc_URL="https://$Host/pr/sfc/servlet.shepherd/document/download/$ContentDocumentId?operationContext=S1";
				$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
				$document_type = &Download_Utility::Clean($document_type);
				$DateReceived = &Download_Utility::Clean($DateReceived);
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}
				$pdfcount_inc++;
			}
		}
		if($Council_Code=~m/^361$|^353$/is)
		{
			my @navigation_menus=@{$data->{'actions'}[2]->{'returnValue'}};
			foreach my $Keys ( @navigation_menus) 
			{
				my $ContentDocumentId = $Keys->{'ContentDocumentId'};
				$DateReceived = $Keys->{'arcshared__Document_Date__c'};
				$document_type = $Keys->{'arcshared__Category__c'};
				$Doc_Desc = $Keys->{'Description'};			
				
				$Doc_URL="https://southderbyshirepr.force.com/sfc/servlet.shepherd/document/download/$ContentDocumentId?operationContext=S1" if($Council_Code=~m/^361$/is);
				$Doc_URL="https://folkestonehythedc.force.com/pr/sfc/servlet.shepherd/document/download/$ContentDocumentId?operationContext=S1" if($Council_Code=~m/^353$/is);
				
				$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
				$document_type = &Download_Utility::Clean($document_type);
				$DateReceived = &Download_Utility::Clean($DateReceived);
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}
				$pdfcount_inc++;
			}
		}
		if($Council_Code=~m/^190$/is)
		{
			my $search_id=$data->{'actions'}[0]->{'returnValue'}->{'arcusbuilt__PApplication__c'}[0]->{'Id'};
			
			if($search_id ne '')
			{
				my $FWUID=$1 if($Content=~m/"\s*fwuid\s*"\s*:\s*"\s*([^>]*?)\s*"/is);
				my $page_url="https://planning.eastleigh.gov.uk/s/sfsites/aura?r=6&other.LC_ARC_Files_Viewer.findContentVersionsForPlanning=1";
				
				my $postcontent="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22392%3Ba%22%2C%22descriptor%22%3A%22apex%3A%2F%2FLC_ARC_Files_Viewer%2FACTION%24findContentVersionsForPlanning%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2Fc%3AARC_Content_Version_Viewer%22%2C%22params%22%3A%7B%22recordId%22%3A%22$search_id%22%2C%22isLatest%22%3Atrue%2C%22showSensitveFiles%22%3Afalse%2C%22orderBy%22%3A%22Document_Date__c%22%2C%22sortType%22%3A%22DESC%22%7D%2C%22version%22%3Anull%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$FWUID%22%2C%22app%22%3A%22siteforce%3AnapiliApp%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fsiteforce%3AnapiliApp%22%3A%22462p4qNuHyuFiAl_0N5FfQ%22%2C%22COMPONENT%40markup%3A%2F%2FforceContent%3AmodalPreviewPlayer%22%3A%22iNA0-1grkI5HCul1AqtmdA%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fs%2Fpublic-register&aura.token=undefined";
				
				my($Post_Responce,$res_code)=&Download_Utility::Post_Method($page_url,$Host,$Content_Type,$Referer,$postcontent,$Proxy);
				open ss,">documenypage.html";
				print ss $Post_Responce;
				close ss;
				print "Document Page";
				# exit;
				my $data = $json->decode($Post_Responce);
				my @navigation_menus=@{$data->{'actions'}[0]->{'returnValue'}};
				foreach my $array(@navigation_menus)
				{
					my $ContentDocumentId =$array->{'ContentDocumentId'};
					my $DateReceived =$array->{'Document_Date__c'};
					my $document_type =$array->{'Document_Type__c'};
					my $Doc_Desc =$array->{'Title'};
					print "ContentDocumentId::::$ContentDocumentId\n";
					$ContentDocumentId = &Download_Utility::Clean($ContentDocumentId);
					$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
					$document_type = &Download_Utility::Clean($document_type);
					$DateReceived = &Download_Utility::Clean($DateReceived);
					
					$Doc_URL="https://planning.eastleigh.gov.uk/sfc/servlet.shepherd/document/download/$ContentDocumentId?operationContext=S1";
									
					my ($file_type);
					if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
					{
						$file_type=$1;
					}
					else
					{
						$file_type='pdf';
					}	

					if($Doc_Desc!~m/^\s*$/is)
					{
						$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
					}	
					else
					{
						$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
					}
					
					$pdf_name = &Download_Utility::PDF_Name($pdf_name);
					$Doc_URL=decode_entities($Doc_URL);
					
					print "Doc_URL	: $Doc_URL\n";
					print "pdf_name	: $pdf_name\n";
					print "file_type: $file_type\n";
					print "DateReceived: $DateReceived\n";
					if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					{
						$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
					}
					$pdfcount_inc++;
				}
			}	
		}
	};
	if($@)
	{
		print "ERR::: $@\n";
	}
	return (\%Doc_Details);	
}
sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $Doc_URL_BY=shift;
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	print "Collect_Docs_Details\n";
	if (($Doc_Download_URL_Index =~m/([a-z]+)/is) and ($Doc_Download_URL_Index ne 'N/A'))
	{
		my ($Doc_Details_Hash_Ref_tmp,$p)=&Download_Utility::Doc_Download_Html_Header($Config,$Content,$Home_URL,$Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY,$Council_Code,$pdfcount_inc);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
		$pdfcount_inc=$p;
		
	}
	else
	{
		$Content=~s/<td\/>/<td><\/td>/igs;
		if($Block_Regex1 ne 'N/A')
		{
			my @Block1 = $Content =~m/$Block_Regex1/igs;
			if($Block_Regex2 ne 'N/A')
			{
				for my $Block1(@Block1)
				{	
					my @Block2 = $Block1=~m/$Block_Regex2/igs;
					if($Block_Regex3 ne 'N/A')
					{
						for my $Block2(@Block2)
						{	
							my @Block3 = $Block2=~m/$Block_Regex3/igs;
							
							my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$URL,$Content);
							my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
							%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
						}	
					}
					else
					{
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$URL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
			}	
			else
			{
				my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$URL,$Content);
				my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
			}	
		}	
	}
	return \%Doc_Details_Hash;
}

sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}
	
	# my $cnt=0;
	# for my $d(@Data)
	# {
		# print "$cnt: $d\n";
		# $cnt++;
	# }

	my $Session_273_Id=$1 if($Content=~m/<link\s*href\=\"\/PublicAccess\.WebSearch\/([^>]*?)\//is);
	
	$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
	# print "PDF_URL_Cont\n$PDF_URL_Cont\n";
	# open(DC,">PDF_URL_Cont.html");
	# print DC $PDF_URL_Cont;
	# close DC;	
	if($Doc_URL_BY eq 'POST')
	{
		print "Doc_URL_BY eq POST\n";
		my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$Doc_POST_ID2,$CSRFToken);
		my $Doc_Post_Content_Tmp=$Doc_Post_Content;
		if($Viewstate_Regex ne 'N/A')
		{
			$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		}	
		if($Viewstategenerator_Regex ne 'N/A')
		{
			$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		}	
		if($CSRFToken_Regex ne 'N/A')
		{
			$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
		}
		if($Eventvalidation_Regex ne 'N/A')
		{
			$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		}	
		if($Doc_Page_Num_Regex ne 'N/A')
		{
			$Doc_Page_Num=$1 if($Content=~m/$Doc_Page_Num_Regex/is);
		}	
		if($Doc_NumberOfRows_Regex ne 'N/A')
		{
			$Doc_NumberOfRows=$1 if($Content=~m/$Doc_NumberOfRows_Regex/is);
		}	
		if($PDF_URL_Cont=~m/$Doc_POST_ID1_Regex/is)
		{
			$Doc_POST_ID1=uri_escape($1);
			$Doc_POST_ID2=uri_escape($2);
		}
		
		$Doc_Post_URL = $Document_Url if($Doc_Post_URL=~m/Document_Url/is);
		$Referer = $Document_Url if($Referer=~m/Document_Url/is);
		$Doc_Post_URL=decode_entities($Doc_Post_URL);
		$Referer=decode_entities($Referer);
		$Doc_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
		$Doc_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
		$Doc_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
		$Doc_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
		$Doc_Post_Content_Tmp=~s/<PageNum>/$Doc_Page_Num/igs;
		$Doc_Post_Content_Tmp=~s/<PageRows>/$Doc_NumberOfRows/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID1>/$Doc_POST_ID1/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID2>/$Doc_POST_ID2/igs;
		
		
	
		$Doc_Post_Host=$Host if($Doc_Post_Host eq '') or ($Doc_Post_Host eq 'N/A');
		
		($Post_Responce,$res_code)=&Download_Utility::Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Referer,$Doc_Post_Content_Tmp,$Proxy);
		# open(DC,">Post_Responce.html");
		# print DC "$Post_Responce";
		# print "Post_Responce-------->$Post_Responce\n";
		# close DC;
		# <STDIN>;
		
		$Post_Responce=$PDF_URL_Cont if($Council_Code==274);
		
		my $URL_ID;
		if($Post_Responce=~m/$Doc_Download_URL_Regex/is)
		{
			my $val=$1;
			if($DOC_PDF_ID_URL=~m/ID/is)
			{
				$URL_ID=$val;
			}
			elsif($DOC_PDF_ID_URL=~m/URL/is)
			{
				my $u1=URI::URL->new($val,$Doc_Post_URL);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
			}			
		}
		if($Doc_Download_URL_Replace ne 'N/A')
		{
			# next if($PDF_URL_Cont == '');
			$Doc_URL = $Doc_Download_URL_Replace;
			$Doc_URL=~s/<SelectedID>/$URL_ID/igs;
		}
		
	}
	elsif($Doc_Download_URL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl=$1;
			print "Doc_Download_URL_ABS: $Doc_Download_URL_ABS\n";			
			
			if($Doc_Download_URL_ABS ne 'N/A') 
			{
				$Doc_URL=$Doc_Download_URL_ABS.$Durl;
				# print "Doc_URL1: $Doc_URL\n";
			}
			else
			{
				if($Durl!~m/http/is)
				{
					if($Council_Code=~m/^23$/is)
					{
						$Doc_URL="http://camdocs.camden.gov.uk".$Durl;
						$Doc_URL=~s/\&quot\;/\"/igs;
					}
					elsif($Council_Code==273){
						
						$Doc_URL="https://padocs.midsussex.gov.uk/PublicAccess.websearch/".$Session_273_Id."/$Durl";
					}
					elsif($Council_Code=~m/^478$/is)
					{
						my $file_type;
						$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
						$document_type = &Download_Utility::Clean($document_type);
							
						if(($Doc_Desc!~m/\.([\w]{3,4})\s*$/is) or ($document_type!~m/\.([\w]{3,4})\s*$/is))
						{
							$file_type=$1;
						}	
						else
						{
							$file_type='pdf';
						}
						
						if($Durl=~m/theSeqNo\=([\d]+)\&theApnkey\=([\d]+)/is)
						{
							$Doc_URL="http://planning.pembrokeshirecoast.wales/agile_pcnpa/MediaTemp/$2-$1".".$file_type";
						}
					}
					else
					{
						my $u1=URI::URL->new($Durl,$Root_Url);
						my $u2=$u1->abs;
						$Doc_URL=$u2;
					}
					print "Doc_URL2: $Doc_URL\n";
				}	
				else
				{
					$Doc_URL=$Durl;
					print "Doc_URL2: $Doc_URL\n";
				}	
			}	
		}

		if($Doc_Download_URL_redirct eq 'Y')
		{
			my ($Redir_Content, $code, $reurl)=&Download_Utility::Getcontent($Doc_URL,"","","","",$Proxy,$Need_Host);
			if($Redir_Content=~m/$Doc_Download_URL_redirct_Regex/is)
			{
				my $Durl=$1;
				if($Doc_Download_URL_redirct_ABS ne 'N/A')
				{
					$Doc_URL=$Doc_Download_URL_redirct_ABS.$Durl;
				}
				else
				{
					if($Durl!~m/http/is)
					{
						my $u1=URI::URL->new($Durl,$Doc_URL);
						my $u2=$u1->abs;
						$Doc_URL=$u2;
					}	
				}	
			}
		}		
	}
	elsif($Doc_Download_URL_Replace ne 'N/A')
	{
		next if($PDF_URL_Cont == '');
		$Doc_URL = $Doc_Download_URL_Replace;
		$Doc_URL=~s/<SelectedID>/$PDF_URL_Cont/igs;
	}
	
	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	my ($file_type);
	if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	
	if($Council_Code=~m/^202$/is)
	{
		$file_type=$1 if($PDF_URL_Cont=~m/title="\s*[^>]*?\.([^>]*?)\s*"/is);
	}
	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	if($Council_Code=~m/^143$/is)
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	if($Council_Code=~m/^160$/is)
	{
		$Doc_URL =~s/https:\/\/d0cs.colchester.gov.uk\/docs\//https:\/\/d0cs.colchester.gov.uk\/publisher\/docs\//igs;
	}
	$Doc_URL=~s/\&quot\;/\"/igs;
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	$Doc_URL=decode_entities($Doc_URL);
	$pdfcount_inc++;
	print "Doc_URL	: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date	: $Published_Date\n";


	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	return (\%Doc_Details);
}	

sub Search_Application()
{
	my $Application_No = uri_escape(shift);
	my $cCode = shift;
	
	print "$Home_URL";
	
	my ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Home_URL,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
	
	# open(DC,">23Home_Content.html");
	# print DC $Content;
	# close DC;
	
	my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$CSRFToken);
	if($Viewstate_Regex ne 'N/A')
	{
		$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
	}	
	if($Viewstategenerator_Regex ne 'N/A')
	{
		$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
	}	
	if($CSRFToken_Regex ne 'N/A')
	{
		$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
	}	
	if($Eventvalidation_Regex ne 'N/A')
	{
		$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
	}	
	
	my $Post_Content_Tmp=$Post_Content;
	$Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
	$Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
	$Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
	$Post_Content_Tmp=~s/<APP_NUMBER>/$Application_No/igs;
	
	if($Council_Code=~m/^395$/is)
	{
		$Home_URL='https://www3.somersetwestandtaunton.gov.uk/asp/WebPages/Plan/plappdets.asp';
	}
	if($Council_Code=~m/^190$/is)
	{
		my $FWUID=$1 if($Content=~m/"\s*fwuid\s*"\s*:\s*"\s*([^>]*?)\s*"/is);
		if($FWUID=~m/^\s*$/is)
		{
			$FWUID=$1 if($Content=~m/fwuid%22%3A%22\s*([^>]*?)%22/is);
		}
		my $nailapp=$1 if($Content=~m/"APPLICATION\@markup:\/\/siteforce:napiliApp\s*\"\s*:\s*"\s*([^>]*?)\s*"/is);
		$Post_Content_Tmp=~s/<FWUID>/$FWUID/igs;
		$Post_Content_Tmp=~s/<NAILAPP>/$nailapp/igs;
		# print "Post_Content_Tmp::$Post_Content_Tmp\n";<STDIN>;
		$Home_URL='https://planning.eastleigh.gov.uk/s/sfsites/aura?r=4&other.LCPublicRegCont.testDynSearch=1';
	}
	# print "Post_Content_Tmp==>$Post_Content_Tmp\n";

	my($Post_Responce,$res_code)=&Download_Utility::Post_Method($Home_URL,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy);
	
	# open(DC,">23Post_Responce.html");
	# print DC "$Post_Responce";
	# close DC;
	# exit;
	
	if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
	{
		if($Post_Responce=~m/$Landing_Page_Regex/is)
		{
			my $DocUrl=$Abs_url.$1;
			# print "Document_Url:::: $DocUrl\n";
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			$Document_Url=decode_entities($Document_Url);
			# print "Document_Url:::: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
			
			# open(DC,">Landing_Content.html");
			# print DC $Content;
			# close DC;
		}
	}	
	
	if($Council_Code=~m/^23$/is)
	{
		
		if($Post_Responce=~m/<td\s*title=\"View\s*Application\s*Details\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>[\w\W]*?\s*<\/a>\s*<\/td>/is)
		{
			my $DocUrl=$1;
			
			
				
			$DocUrl =~ s/\&amp\;/\&/g;
			$DocUrl =~s/\'/\'\'/igs;
			$DocUrl=~s/\&quot\;/\"/igs;
			
			print "Document_Url:::: $DocUrl\n";
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			$Document_Url=decode_entities($Document_Url);
			
			$Document_Url=~s/PlanningExplorer\/StdDetails/PlanningExplorer\/Generic\/StdDetails/is;
			$Document_Url=~s/\&quot\;/\"/igs;
			# print "Document_Url:::: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
			
			# open(DC,">Landing_Content.html");
			# print DC $Content;
			# close DC;
			# exit;
		}
	}
	
	if($Application_Regex ne 'N/A')
	{
		if($Content=~m/$Application_Regex/is)
		{
			$Document_Url = $1;
			my $u1=URI::URL->new($Document_Url, $Home_URL);
			$Document_Url=$u1->abs;		
			$Document_Url=~s/\&quot\;/\"/igs;
			# print "Document_Url: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
			
			# open(DC,">Doc_Page_Content.html");
			# print DC $Content;
			# close DC;
		}
	}
	if($Council_Code=~m/^190|395$/is)
	{
		$Content=$Post_Responce;
	}
	
	return($Content,$res_code);
}

sub QueryExecuteMax()
{	
	my ($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query)=@_;
	
	if($Insert_Document!~m/values\s*$/is)
	{
		$Insert_Document=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

		$Update_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

		$MD5_Insert_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);
		
		$Insert_Document="";
		$Update_Query="";
		$MD5_Insert_Query="";
		
		$Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
		$MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

		$MaxCount=0;
	}
	return ($MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
}
