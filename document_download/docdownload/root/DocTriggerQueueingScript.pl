use strict;
use Cwd qw(abs_path);
use File::Basename;
use POE qw(Wheel::Run Filter::Reference);
sub MAX_CONCURRENT_TASKS () { 10 }
# use DateTime;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

my $onDemandRequest = $ARGV[0];
print "onDemandRequest: $onDemandRequest\n";
####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'\\lib');
my $scriptDirectory = ($basePath.'\\root');
my $logDirectory = ($basePath.'\\logs');

# Private Module
require ($libDirectory.'\\Download_DB.pm'); 


#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();
#----------------------- Get input -----------------------#
my ($Council_Name,$Council_Code, $formatId, $onDemandID, @formatId, @cCode, @onDemandID);
if($onDemandRequest eq 'onDemand')
{
	($onDemandID,$Council_Code,$formatId) = &Download_DB::retrieveOnDemondInput_CC($dbh);
	@onDemandID = @{$onDemandID};
	@cCode = @{$Council_Code};
	@formatId = @{$formatId};
}
elsif($onDemandRequest eq 'ApprovedDecision')
{
	($Council_Code) = &Download_DB::Retrieve_CC_ApprovedDecision($dbh);
	@cCode = @{$Council_Code};
}
else
{
	($Council_Code) = &Download_DB::Retrieve_Input_CC($dbh);
	@cCode = @{$Council_Code};
}	

# use DateTime;
# my $current_date_time=DateTime->now;
my $current_date_time=localtime();

my $arrSize = @cCode;

if($onDemandRequest ne 'onDemand')
{
	open(my $fh,">>$logDirectory/Retrieve_Input_council_count_log.txt");
	print $fh "$arrSize\t$current_date_time\tTask Started Time\n";
	close ($fh);
}
elsif($onDemandRequest ne 'ApprovedDecision')
{
	open(my $fh,">>$logDirectory/ApprovedDecision_Input_council_count_log.txt");
	print $fh "$arrSize\t$current_date_time\tTask Started Time\n";
	close ($fh);
}

# my $schdule_time=$current_date_time;
# $current_date_time=~s/\-|\:/_/igs;



# my $status_log_file_name="Councile_code_".$current_date_time.".txt";



POE::Session->create(
  inline_states => {
    _start      => \&start_tasks,
    next_task   => \&start_tasks,
    task_result => \&handle_task_result,
    task_done   => \&handle_task_done,
    task_debug  => \&handle_task_debug,
    sig_child   => \&sig_child,
  }
);



sub start_tasks 
{
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	
	while (keys(%{$heap->{task}}) < MAX_CONCURRENT_TASKS) 
	{
		my $next_task = shift @cCode;
		my $next_formatId = shift @formatId;
		my $next_onDemandID = shift @onDemandID;
		last unless defined $next_task;
		
		sleep(5);
		print "Starting GDD task for $next_task $next_formatId...\n";
		my $task = POE::Wheel::Run->new(
		Program      => sub { do_stuff($next_task, $next_formatId, $next_onDemandID) },
		StdoutFilter => POE::Filter::Reference->new(),
		StdoutEvent  => "task_result",
		StderrEvent  => "task_debug",
		CloseEvent   => "task_done",
		);
		
		$heap->{task}->{$task->ID} = $task;
		$kernel->sig_child($task->PID, "sig_child");
	}
}

sub do_stuff 
{
	binmode(STDOUT);    # Required for this to work on MSWin32
	my $councilCode   	= shift;
	my $formatId   		= shift;
	my $next_onDemandID = shift;
	
	my $filter = POE::Filter::Reference->new();

	sleep(rand(15));
	
    my $script_name;	
	if($councilCode=~m/^5\d{2}$/is)
	{
		$script_name="$scriptDirectory/DocDownload_500s.pl";
	}
	elsif($councilCode=~m/^(603|606|607|608|609|610|611|612|616|617|618|619|620|621|623|624|625|626|627|629|630|631)$/is)
	{
		$script_name="$scriptDirectory/DocDownload_600s.pl";
	}
	elsif($councilCode=~m/^(112|133|147|158|168|193|195|201|236|240|247|275|280|285|289|316|332|342|348|375|425|428|439|449|461|470|29|282|284|274|230|268|232)$/is)
	{
		$script_name="$scriptDirectory/DocDownloadFormat8.pl";
	}
	# elsif($councilCode=~m/^(176|420|455|124|94|91|244|367|14|92|26|166|229|297|317|379|394|431|22|46|224|362|132|185|393|9|95|110|116|128|406|424|438|98|16|21|323|370|327|416|216|450|492)$/is)
	elsif($councilCode=~m/^(176|420|124|94|91|244|367|14|26|166|229|297|379|394|362|132|9|95|110|406|98|16|21|323|370|327|216|450|492|438|38|149|185|207|320|326|329|378|455|5|433|339|200|103|260|199|457|261|163|474|456|273|203|31|424|338)$/is)
	{
		$script_name="$scriptDirectory/Doc_Download_Format1_Extented.pl";
		# 339=>stopped due to roxy issue
	}
	elsif($councilCode=~m/^(27|355|218|251|287|97)$/is)
	{
		$script_name="$scriptDirectory/Doc_Download_Others.pl";
	}
	elsif($councilCode=~m/^(451|467|292|290)$/is)
	{
		$script_name="$scriptDirectory/Doc_Download_Direct.pl";
	}
	elsif($councilCode!~m/^(1|2|3|4|6|7|8|10|13|15|17|18|19|25|28|30|32|35|36|40|41|42|44|46|48|49|51|52|54|55|56|58|59|62|63|67|70|72|73|75|77|83|87|100|104|106|111|113|116|117|119|121|122|123|126|128|129|131|134|136|137|139|141|142|146|150|153|155|156|157|159|164|165|167|171|172|173|180|186|188|189|191|192|194|205|206|208|213|217|220|221|223|225|226|228|237|241|242|248|252|253|257|259|262|263|266|267|270|271|272|276|279|283|286|291|293|294|296|298|301|303|305|307|308|311|312|313|317|319|328|330|335|336|337|341|344|345|349|351|352|356|357|368|374|376|380|381|382|383|385|387|388|390|391|398|399|400|402|403|404|405|407|408|409|410|412|416|421|422|427|431|432|434|435|437|440|441|442|443|445|446|452|453|458|459|460|476|483|484|485|487|488|490|491|531|602|604|605|632|633|181|198|224|24|334|371|393|454|392|11|12|78|135|148|179|187|211|214|219|222|234|246|310|322|338|429|444|532|533|2006|2008|482|333|413|302|486|628|20\d{2}|20)$/is)
	{
		$script_name="$scriptDirectory/Doc_Download.pl";
	}
	
	my $current_dateTime=localtime();
	print "current_date_time: $current_dateTime\n";
	open(my $fh,">>$logDirectory/status_log_file_name.txt");
	print $fh "$councilCode\t$current_dateTime\n";
	close ($fh);

	# if($councilCode!~m/^\s*486\s*$/is)
	# {
		if($formatId=~m/^\d+$/is)
		{
			system("start PERL $script_name $councilCode $formatId Y $next_onDemandID");
			open(my $fh,">>$logDirectory/status_log_file_name_onDemand.txt");
			print $fh "$councilCode\t$formatId\t$current_dateTime\tTriggered\n";
			close ($fh);			
		}	
		elsif($onDemandRequest eq 'ApprovedDecision')
		{
			print "onDemandRequest: $onDemandRequest\n";
			system("start","PERL","$script_name","$councilCode","","ApprovedDecision");
			open(my $fh,">>$logDirectory/status_log_ApprovedDecision.txt");
			print $fh "$councilCode\t$current_dateTime\tTriggered\n";
			close ($fh);	
		}
		else
		{
			system("start PERL $script_name $councilCode");
			open(my $fh,">>$logDirectory/status_log_file_name_All.txt");
			print $fh "$councilCode\t$current_dateTime\tTriggered\n";
			close ($fh);			
		}	
	# }	
	# else
	# {
		# open(my $fh,">>$logDirectory/status_log_file_name_486.txt");
		# print $fh "$councilCode\t$current_dateTime\n";
		# close ($fh);		
	# }
		
	
	
	my %result = (
	task   => $councilCode,
	status => "seems ok to me",
	);

	my $output = $filter->put([\%result]);

	print @$output;
}

sub handle_task_result 
{
	my $result = $_[ARG0];
	print "Result for $result->{task}: $result->{status}\n";
}

sub handle_task_debug 
{
	my $result = $_[ARG0];
	print "Debug: $result\n";
}

sub handle_task_done {
	my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
	delete $heap->{task}->{$task_id};
	$kernel->yield("next_task");
}

sub sig_child {
	my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
	my $details = delete $heap->{$pid};

# warn "$$: Child $pid exited";
}

$poe_kernel->run();


if($onDemandRequest ne 'onDemand')
{
	my $current_dateTime=localtime();
	open(my $fh,">>$logDirectory/Retrieve_Input_council_count_log.txt");
	print $fh "$arrSize\t$current_dateTime\tTask End Time\n";
	close ($fh);
}
elsif($onDemandRequest ne 'ApprovedDecision')
{
	my $current_dateTime=localtime();
	open(my $fh,">>$logDirectory/ApprovedDecision_Input_council_count_log.txt");
	print $fh "$arrSize\t$current_dateTime\tTask End Time\n";
	close ($fh);
}
exit 0;