use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use filehandle;
use URI::Escape;
use Digest::MD5;
use CAM::PDF;
use JSON::Parse 'parse_json';
require "C:/Glenigan/Merit/Projects/docdownload_perl/lib/Scrape_Oneapp.pm";
# require "D:/One_App/DB_OneApp.pm";
require "C:/Glenigan/Merit/Projects/docdownload_perl/lib/DB_OneApp_Test.pm";
use Time::Piece;
use Cwd qw(abs_path);
use File::Basename;

my $basePath = dirname (dirname abs_path $0); 

my $scriptDirectory = ($basePath.'/root');

# my $PDF_Path='//172.27.138.188/OneApp/';
# my $PDF_Path='//172.27.138.180/Council_PDFs/';
my $PDF_Path='//172.27.137.202/One_app_download/';

my $dbh = &DB_OneApp_Test::DbConnection();

my ($Format_id,$Council_Code,$DOCUMENT_NAME,$APPLICATION_NO,$SOURCE,$MARKUP) = &DB_OneApp_Test::Retrieve_OneApp_For_Scrape($dbh);

open(FT,">>C:\Glenigan\Merit\Projects\docdownload_perl\logs\PDF_Format_Log.txt");
my (@imageFormatList, @otherFormatList);
for(my $reccnt = 0; $reccnt < @{$Format_id}; $reccnt++ )
{
	# print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_id[$reccnt];
	my $Council_Code 	= @$Council_Code[$reccnt];
	my $Document_Name 	= @$DOCUMENT_NAME[$reccnt];
	my $Application_No 	= @$APPLICATION_NO[$reccnt];
	my $Source 	= @$SOURCE[$reccnt];
	my $Markup 	= @$MARKUP[$reccnt];
	# next if ($Council_Code!~m/^27/is);
	print "Council_Code::$Council_Code\n";
	# next;
	# <STDIN>;
	# my $Document_Name 	= '2018_19664_HH-APPLICATION_FORM-422157.pdf';
	my $InsertFormatPdf = "insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_Country,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area,application_Format,Existing_Chimney_Material, proposed_Chimney_material, Existing_Rainwater_goods_Material, proposed_Rainwater_goods_material, Existing_Ceiling_Material, proposed_Ceiling_material, Existing_Floor_Material, proposed_Floor_material) values ";

	my $now = localtime();
	open(FHM,">>Selected_Project.txt");	
	print FHM "OneApp ~ CC: Format_ID => $Council_Code : $Format_ID\t$now\n";
	print "OneApp ~ CC: Format_ID => $Council_Code : $Format_ID \n";
	close FHM;

	my $pdfFileName = "$PDF_Path/$Format_ID/$Document_Name";
	
	
	
	# eval{
		# system("PERL","$scriptDirectory/deillustrate.pl","$pdfFileName","$pdfFileName");
	# };
	# if($@)
	# {
		# print "$@\n";
		# print "Error While deIllustrate\n";
	# }
	
	my $textFileName = $pdfFileName;
	$textFileName=~s/\.pdf$/\.txt/igs;
	
	# my $pdf = eval{CAM::PDF->new("$PDF_Path/$Format_ID/$Document_Name")};
	# if($@)
	# {
		# print "Error While CAM::PDF Text Convertion:\n$@\n";
	# }	
	# my $pageone_tree;
	# eval {
		# $pageone_tree = eval{$pdf->getPageContentTree(1)};
	# };
	# if($@){
		# print "Convertion Failed : $@\n";
	# }		
	# my $pdfFileContent = eval{CAM::PDF::PageText->render($pageone_tree)};

	# my $convertedLength = length($pdfFileContent);
	# print "Length : $convertedLength\n";
	# if($convertedLength == 0)
	# {
		eval{
			system("$scriptDirectory/pdftotext.exe $pdfFileName $textFileName");
		};
		if($@)
		{
			print "$@\n";
			print "Error While pdftohtml HTML Convertion\n";
		}	
	# }
	open(RF,"<$textFileName");
	my @textdata = <RF>;
	close (RF);
	
	my $pdfFileContent = join("",@textdata);
	my $convertedLength = length($pdfFileContent);
	print "Length : $convertedLength\n";

	my $SaveFormatPdf_Query='';
	if($pdfFileContent=~m/1\.\s*Applicant\s*Name\s*\,\s*Address\s*and\s*Contact\s*Details/is)
	{
		open(FR,">>PDF_Format.txt");
		print FR "$Format_ID\tFormat 1\n";
		close (FR);
		print "Format 1\n";
		print FT "$Format_ID\tFormat 1\n";
		$SaveFormatPdf_Query=&Scrape_Oneapp::Scrape_pdf($dbh,$Document_Name,$Format_ID,$Council_Code,$Application_No,$Source,$Markup,$PDF_Path);
	}	
	elsif($pdfFileContent=~m/1\s*\.\s*Site\s*Address|1\s*\.\s*Site\s*Details/is)
	{
		open(FR,">>PDF_Format.txt");
		print FR "$Format_ID\tFormat 2\n";
		close (FR);
		print "Format 2\n";
		print FT "$Format_ID\tFormat 2\n";
		$SaveFormatPdf_Query=&Scrape_Oneapp::Scrape_pdf_format2($dbh,$Document_Name,$Format_ID,$Council_Code,$Application_No,$Source,$Markup,$PDF_Path);
		print "$SaveFormatPdf_Query\n------------\nFormat2\n";
	}	
	elsif($convertedLength == 0)
	{
		push(@imageFormatList,$Format_ID);
		print "Image format PDF\n";
	}
	else
	{
		push(@otherFormatList,$Format_ID);
		print "Other format PDF\n";
	}

	$InsertFormatPdf.=$SaveFormatPdf_Query;
	$InsertFormatPdf=~s/\,\s*$//igs;

	if($InsertFormatPdf!~m/values\s*$/is)
	{
		print "$InsertFormatPdf\n";
		&DB_OneApp_Test::UpdateDB($dbh,$InsertFormatPdf);
	}
	
}	
close FT;
my $imageFormatList = join(',',@imageFormatList);
print "Image List: $imageFormatList\n";
if(@imageFormatList > 0)
{
	my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'I', Last_Visited_Date = getdate() where ID in ($imageFormatList)";
	&DB_OneApp_Test::UpdateDB($dbh, $query);
}	

my $otherFormatList = join(',',@otherFormatList);
print "Other List: $otherFormatList\n";
if(@otherFormatList > 0)
{
	my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'O', Last_Visited_Date = getdate() where ID in ($otherFormatList)";
	&DB_OneApp_Test::UpdateDB($dbh, $query);
}
