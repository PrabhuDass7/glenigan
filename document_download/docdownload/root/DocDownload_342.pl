use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename;
use JSON;


# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 


my $Council_Code=$ARGV[0];

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);


#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";

my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code);
my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";
	
# &Download_DB::Execute($dbh,$Dashboard_Insert_Query);


my ($Update_Query);

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details_Format8.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Host1 = $Config->{$Council_Code}->{'Host1'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $FILTER_URL1 = $Config->{$Council_Code}->{'FILTER_URL1'};
my $FILTER_URL2 = $Config->{$Council_Code}->{'FILTER_URL2'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_NUMBER1 = $Config->{$Council_Code}->{'FORM_NUMBER1'};
my $FORM_REFERENECE = $Config->{$Council_Code}->{'FORM_REFERENECE'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Docs_URL_Regex = $Config->{$Council_Code}->{'Docs_URL_Regex'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_Filter_URL = $Config->{$Council_Code}->{'Doc_Download_Filter_URL'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Redir_Url = $Config->{$Council_Code}->{'Redir_Url'};
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $All_Page_Name = $Config->{$Council_Code}->{'All_Page_Name'};
my $All_Page_Name_Value = $Config->{$Council_Code}->{'All_Page_Name_Value'};
my $Doc_URL_BY_Regex = $Config->{$Council_Code}->{'Doc_URL_BY_Regex'};
my $Accept_Name = $Config->{$Council_Code}->{'Accept_Name'};
my $Accept_Value = $Config->{$Council_Code}->{'Accept_Value'};
my $Search_Type = $Config->{$Council_Code}->{'Search_Type'};
my $Search_POST = $Config->{$Council_Code}->{'Search_POST'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};


$Method = "N/A" if($Method eq ""); $Format = "N/A" if($Format eq ""); $Home_URL = "N/A" if($Home_URL eq ""); $Host = "N/A" if($Host eq ""); $Host1 = "N/A" if($Host1 eq ""); $FILTER_URL = "N/A" if($FILTER_URL eq ""); $FILTER_URL1 = "N/A" if($FILTER_URL1 eq ""); $FILTER_URL2 = "N/A" if($FILTER_URL2 eq ""); $Content_Type = "N/A" if($Content_Type eq ""); $FORM_NUMBER = "N/A" if($FORM_NUMBER eq ""); $FORM_NUMBER1 = "N/A" if($FORM_NUMBER1 eq ""); $FORM_REFERENECE = "N/A" if($FORM_REFERENECE eq ""); $Referer = "N/A" if($Referer eq ""); $POST_URL = "N/A" if($POST_URL eq ""); $Doc_URL_Regex = "N/A" if($Doc_URL_Regex eq ""); $Docs_URL_Regex = "N/A" if($Docs_URL_Regex eq "");
$Landing_Page_Regex = "N/A" if($Landing_Page_Regex eq ""); $Viewstate_Regex = "N/A" if($Viewstate_Regex eq ""); $Viewstategenerator_Regex = "N/A" if($Viewstategenerator_Regex eq ""); $Eventvalidation_Regex = "N/A" if($Eventvalidation_Regex eq ""); $Block_Regex1 = "N/A" if($Block_Regex1 eq ""); $Block_Regex2 = "N/A" if($Block_Regex2 eq ""); $Block_Regex3 = "N/A" if($Block_Regex3 eq ""); $Doc_Published_Date_Index = "N/A" if($Doc_Published_Date_Index eq ""); $Doc_Download_URL_Replace = "N/A" if($Doc_Download_URL_Replace eq ""); $Doc_Type_Index = "N/A" if($Doc_Type_Index eq ""); $Doc_Description_Index = "N/A" if($Doc_Description_Index eq "");
$Doc_Download_URL_Index = "N/A" if($Doc_Download_URL_Index eq ""); $Doc_Download_URL_Regex = "N/A" if($Doc_Download_URL_Regex eq ""); $Doc_Download_Filter_URL = "N/A" if($Doc_Download_Filter_URL eq ""); $Total_No_Of_Pages_Regex = "N/A" if($Total_No_Of_Pages_Regex eq ""); $Available_Doc_Count_Regex = "N/A" if($Available_Doc_Count_Regex eq ""); $Doc_URL_BY = "N/A" if($Doc_URL_BY eq ""); $Pagination_Type = "N/A" if($Pagination_Type eq ""); $Next_Page_Regex = "N/A" if($Next_Page_Regex eq ""); $Next_Page_Replace_Info = "N/A" if($Next_Page_Replace_Info eq ""); $Next_Page_Replace_Key = "N/A" if($Next_Page_Replace_Key eq "");
$Redir_Url = "N/A" if($Redir_Url eq ""); $Doc_Download_URL_ABS = "N/A" if($Doc_Download_URL_ABS eq ""); $SSL_VERIFICATION = "N/A" if($SSL_VERIFICATION eq ""); $All_Page_Name = "N/A" if($All_Page_Name eq ""); $All_Page_Name_Value = "N/A" if($All_Page_Name_Value eq ""); $Doc_URL_BY_Regex = "N/A" if($Doc_URL_BY_Regex eq ""); $Accept_Name = "N/A" if($Accept_Name eq ""); $Accept_Value = "N/A" if($Accept_Value eq ""); $Search_Type = "N/A" if($Search_Type eq ""); $Search_POST = "N/A" if($Search_POST eq ""); $Post_Content = "N/A" if($Post_Content eq "");


my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;


my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
# print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	# print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	
	print "Format_ID :: $Format_ID \n";
	
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code,$Available_Doc_Count);
	my $Landing_Page_Flag=0;
		
	# my $Download_Location = $Local_Download_Location."/$Format_ID";
	
	($Doc_Details_Hash_Ref, $Document_Url, $Code)=&Extract_Link($Home_URL,$Document_Url,$URL,\%Nextpage_Replace_Hash,$Council_Code,$Application_No);
	
	
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;
	
	$Todays_Count = $Todays_Count + $pdfcount;
	if($Available_Doc_Count=~m/^\s*$/is)
	{
		$Available_Doc_Count = $pdfcount;
	}
	# print "Markup<==>$Markup\n"; <STDIN>;	
	
	my $RetrieveMD5=[];
	if(lc($Markup) eq 'large')
	{
		# print "$pdfcount<==>$NO_OF_DOCUMENTS\n"; <STDIN>;
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	}	

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;
	# print "Temp_Download_Location=>$Temp_Download_Location\n";
	
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====>$Temp_Download_Location\n";
	
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	my $No_Of_Doc_Downloaded=0;
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$Doc_Desc}->[2];
		
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				my ($D_Code, $Err_Msg, $MD5);
				if($Council_Code=~m/^(342)$/is)
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
				}
				else
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				}
				
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\'),";	
					$Insert_Document.=$Insert_Document_List;
				}	
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is && $Pdf_Name!~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5);
					if($Council_Code=~m/^(342)$/is)
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
					}
					else
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					}
					
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
				}
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\'),";	
				$Insert_Document.=$Insert_Document_List;
			}
		}	
	}	

	# &Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:	
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
	}	
	else
	{
		$No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	}
	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if($One_App_Name=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}	
	$pdfcount_inc=1;
	
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

$Insert_OneAppLog=~s/\,$//igs;
# print "\n$Insert_OneAppLog\n";
# &Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\n$Insert_Document\n";
# &Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
# &Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
# &Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

sub Extract_Link()
{
	my $homeURL = shift;
	my $documentURL = shift;
	my $ApplicationURL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $councilCode = shift;
	my $Application_No = shift;
		
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my ($docPageContent,$Code,$Available_Doc_Count,%Doc_Details_Hash);
	my $Landing_Page_Flag=0;

	if($Council_Code=~m/^(342)$/is)
	{
		$documentURL="";
		print "documentURL<==>$documentURL\n"; 
	}
	
	Re_Ping:	
	if($documentURL ne '')
	{
		if($Method=~m/GET/is)
		{
			($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
		}
		elsif($Method=~m/POST/is)
		{
			($docPageContent,$Code) = &Download_Utility::docPostMechMethod($documentURL,$Post_Content,$pageFetch);	
		}
		
	}
	else
	{
		print "Search Application method id processing..(Document URL not found)\n";
		($docPageContent,$Code)=&Search_Application($Application_No,$ApplicationURL);
	}	
	
	# open(DC,">Home_Content.html");
	# print DC $docPageContent;
	# close DC;
	# exit;
	# <STDIN>;
	
		
	if($docPageContent=~m/<[^>]*?=\"$Accept_Name\"[^>]*?>I\s*agree\s*to\s*this\s*statement\s*<\//is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# <STDIN>;		
	}
	elsif($docPageContent=~m/>\s*I\s*agree\s*<\//is)
	{
		my $appNo = $1 if($docPageContent=~m/<h3>Downloads\s*for\s*this\s*application<\/h3>\s*<form[^>]*?planningApplication\/([^\"]*?)\"[^>]*?>/is);
		$POST_URL=~s/<APPNUM>/$appNo/gsi;
		
		($docPageContent,$Code) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$POST_URL=~s/$appNo/<APPNUM>/gsi;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# exit;
	}
	elsif($docPageContent=~m/By\s*clicking\s*the\s*button\s*below\s*you\s*agree\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		# print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# exit;
	}
	
	my %Doc_Details;
	if($Landing_Page_Flag == 0)
	{
		if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
		{
			print "Application Number==>$Application_No\n";
			
			if($docPageContent=~m/$Landing_Page_Regex/is)
			{
				my $DocUrl=$1;
				$DocUrl=~s/\&amp\;/\&/gsi;
				
				my $u1=URI::URL->new($DocUrl,$documentURL);
				$documentURL=$u1->abs;
				
				$documentURL = $FILTER_URL.$documentURL if($documentURL!~m/^http/is);
				print "Document_Url: $documentURL\n";
				# <STDIN>;
				
				$Landing_Page_Flag = 1;
				goto Re_Ping;
			}	
		}
	}
		
	# open(DC,">Home_Content.html");
	# print DC $docPageContent;
	# close DC;
	# exit;	
	
	my ($Total_no_Pages);
	if($Total_No_Of_Pages_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_no_Pages=$1;
		}
		print "Total_no_Pages: $Total_no_Pages\n";
	}

	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
		
		print "Available_Doc_Count<==>$Available_Doc_Count\n"; 
		# <STDIN>;	
	}	
	
	if($Council_Code=~m/^342$/is)
	{
		my $NXT_BTN_NME = $1 if($docPageContent=~m/<select[^>]*?PageSize\"\s*name=\"([^\"]*?)\"[^>]*?>/is);
		my $NXT_BTN_VAL = $1 if($docPageContent=~m/<option[^>]*?value=\"([^\"]*?)\">\s*All\s*<\/option>/is);
		
		$pageFetch-> submit_form(
				form_number => $FORM_NUMBER,
				fields      => {
					$NXT_BTN_NME	=> $NXT_BTN_VAL,
					},
				);
		
		
		my $Page_url = $pageFetch->uri();		
		my $searchPageCon = $pageFetch->content;
		my $searchCode= $pageFetch->status();
		
		# open(DC,">Search_Content.html");
		# print DC $searchPageCon;
		# close DC;
		# exit;
		
		$docPageContent = $searchPageCon;
	}

	
	my $page_inc=1;	
	
	Next_Page:
	if($Format eq 'Table')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($docPageContent,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$documentURL,$ApplicationURL,$Doc_URL_BY);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}
	
	if($Pagination_Type eq 'N/A')
	{
		return (\%Doc_Details_Hash,$documentURL,$Code);
	}
	elsif($Pagination_Type eq 'By Format')
	{
		if($page_inc < $Total_no_Pages)
		{		
			print "page_inc	: $page_inc\n";
			print "Total_no_Pages	: $Total_no_Pages\n";
			# <STDIN>;
			
			$page_inc++;
			
			my $nextPage = $1 if($docPageContent=~m/<input\s*name=\"([^\"]*?)\"[^>]*?onclick=\"Navigate\(\'Results\.aspx\'\s*\+\s*[^>]*?\s*\'\?grdResultsP=\'\s*[^>]*?\s*\+\s*$page_inc\)\"\s*value=\"$page_inc\"[^>]*?>/is);
									
			my $nextPageLink = $pageFetch->find_link( text => $nextPage );		
			my $nextPageLink    = $pageFetch->uri();
			
			$nextPageLink=$nextPageLink."\?grdResultsP=".$page_inc;
	
			
			my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($nextPageLink,$pageFetch);	
			$docPageContent=$searchPageCon;			
			$nextPageLink=~s/\?grdResultsP=\d+$//gsi;
			
			# print "$nextPageLink\n";	
			
			# open(DC,">Search_Content$page_inc.html");
			# print DC $docPageContent;
			# close DC;
			# exit;
					
			goto Next_Page;
		}
	}
	elsif($Pagination_Type eq 'By Click')
	{		
		$pageFetch-> submit_form(
				form_number => $FORM_NUMBER,
				fields      => {
					$All_Page_Name	=> $All_Page_Name_Value,
					},
				);
		
		
		my $Page_url = $pageFetch->uri();
		
		my $searchPageCon = $pageFetch->content;
		my $searchCode= $pageFetch->status();
		
		$docPageContent = $searchPageCon;
		$Code = $searchCode;
		
		print "Click response: $Code\n";
		
		# open(DC,">Search_Content.html");
		# print DC $docPageContent;
		# close DC;
		# exit;
	}
	
	
	return (\%Doc_Details_Hash,$documentURL,$Code,$Available_Doc_Count);
}


sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $ApplicationURL=shift;
	my $Doc_URL_BY=shift;
	
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	# open(DC,">Search_Content.html");
	# print DC $Content;
	# close DC;
	# exit;
	# <STDIN>;
	
	$Content=~s/<td\/>/<td><\/td>/igs;
	if($Block_Regex1 ne 'N/A')
	{
		my @Block1 = $Content =~m/$Block_Regex1/igs;
		if($Block_Regex2 ne 'N/A')
		{
			for my $Block1(@Block1)
			{	
				my @Block2 = $Block1=~m/$Block_Regex2/igs;
				if($Block_Regex3 ne 'N/A')
				{
					for my $Block2(@Block2)
					{	
						my @Block3 = $Block2=~m/$Block_Regex3/igs;
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
				else
				{
					my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
					my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
					%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
				}	
			}
		}	
		else
		{
			my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$ApplicationURL,$Content);
			my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
			%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
		}	
	}
	
	return \%Doc_Details_Hash;
}



sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}

	$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
	
		
	# open(DC,">Content.html");
	# print DC $PDF_URL_Cont;
	# close DC;	
	# <STDIN>;
	# exit;
	

	if($Doc_Download_URL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl=$1;	
			
			# print "$PDF_URL_Cont\n"; <STDIN>;
			if($Doc_Download_URL_Replace ne 'N/A') 
			{		
				my $docID=$1 if($Durl=~m/Download\.aspx\?ID=(\d+)$/is);	
				$Doc_URL = $Doc_Download_URL_Replace;
				$Doc_URL=~s/<SelectedID>/$docID/igs;
			}
			elsif($Doc_Download_URL_ABS ne 'N/A') 
			{
									
				$Doc_URL=$Doc_Download_URL_ABS.$Durl;
				# print "Doc_URL1: $Doc_URL\n";
			}
			elsif($Durl!~m/^http/is)
			{							
				my $u1=URI::URL->new($1,$Root_Url);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
				# print "Doc_URL2: $Doc_URL\n"; <STDIN>;
			}
			else
			{	
				$Doc_URL=$Durl;	
			}	
		}
	}
	if($Council_Code=~m/^342$/is)
	{
		$Doc_URL=$PDF_URL_Cont;
	}			
	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);	
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	my ($file_type);
	if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	
	
	
	if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
	{
		my $file_typeDoc=$1;
		if($file_typeDoc eq $file_type)
		{
			$file_type;
		}
		else
		{
			$file_type=$file_typeDoc;
		}
	}
	elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
	{
		my $file_typeDocType=$1;
		if($file_typeDocType eq $file_type)
		{
			$file_type;
		}
		else
		{
			$file_type=$file_typeDocType;
		}
	}

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	
	# print "pdfcount_inc	: $pdfcount_inc\n"; <STDIN>;	
	
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	
	$pdfcount_inc++;
	
	print "Doc_URL	: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date	: $Published_Date\n";
	
	# $pageFetch->get($Doc_URL);
	# my $pdf_content = $pageFetch->content;
	
	# my $fh = FileHandle->new("$pdf_name",'w') or warn "$!";
	# binmode($fh);
	# $fh->print($pdf_content);
	# $fh->close();	

	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	
	return (\%Doc_Details);
}

sub Search_Application()
{
	my $Application_No = shift;
	my $ApplicationURL = shift;

	my ($homePageContent,$Code) = &Download_Utility::docMechMethod($Home_URL,$pageFetch);	
	
	my ($docPageContent,$docRespCode,$searchPageCon,$searchCode);
	
	if($Search_Type eq "By Click")
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $FORM_REFERENECE => $Application_No );
		$pageFetch->click();
			
		$searchPageCon = $pageFetch->content;
		$searchCode= $pageFetch->status();
	}
	else
	{
		my $appNum;
		
		$appNum = uri_escape($Application_No);	
		
		my $viewState = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATE"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateGen = uri_escape($1) if($homePageContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"__VIEWSTATEGENERATOR"\s[^>]*?value="([^>]*?)"\s*\/>/is);
		my $eventVal = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__EVENTVALIDATION"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateZip = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATEZIP"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		
		$Search_POST =~s/<VIEWSTATE>/$viewState/gsi;
		$Search_POST =~s/<VIEWSTATEGENERATOR>/$viewStateGen/gsi;
		$Search_POST =~s/<EVENTVALIDATION>/$eventVal/gsi;
		$Search_POST =~s/<APPNUM>/$appNum/gsi;
		$Search_POST =~s/<VIEWSTATEZIP>/$viewStateZip/gsi;
		# print "$Search_POST\n"; <STDIN>;
		
		($searchPageCon,$searchCode) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$Search_POST =~s/$appNum/<APPNUM>/gsi;
		$Search_POST =~s/$viewState/<VIEWSTATE>/gsi;
		$Search_POST =~s/$viewStateGen/<VIEWSTATEGENERATOR>/gsi;
		$Search_POST =~s/$eventVal/<EVENTVALIDATION>/gsi;
		$Search_POST =~s/$viewStateZip/<VIEWSTATEZIP>/gsi;
		
	}
	
	# open(DC,">searchPageCon.html");
	# print DC $searchPageCon;
	# close DC;	
	# exit;
	
	if(($searchPageCon=~m/$Doc_URL_Regex/is) && ($Doc_URL_Regex ne 'N/A'))
	{
		my $docURL = $1;
		$docURL=~s/amp\;//igs;
		
		if($docURL!~m/^http\:/is)
		{
			$docURL = $FILTER_URL.$docURL;
		}	

		($docPageContent,$docRespCode) = &Download_Utility::docMechMethod($docURL,$pageFetch);
	}
	else
	{
		$docPageContent = $searchPageCon;
	}
	
	return($docPageContent,$docRespCode);
}


sub clean()
{
	my $Data=shift;
	$Data=~s/\s*<[^>]*?>\s*//igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;//igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&nsbp//igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	return($Data);
}