package Scrape_Oneapp;
use strict;
use DBI;
use DBD::ODBC;
use filehandle;
use CAM::PDF;
use CAM::PDF::PageText;
require Exporter;
use Spreadsheet::XLSX;
use Cwd qw(abs_path);
use File::Basename;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 

# Declare and load local directories and required private module
my $libDirectory = ($basePath.'/lib/OneAppScrapper');
my $pdfConverterDirectory = ($basePath.'/lib/OneAppScrapper/ThirdPartyPrograms');

# Private Module
require ($libDirectory.'/Scrape_Oneapp.pm'); 
require ($libDirectory.'/DB_OneApp.pm'); 
require ($libDirectory.'/Oneapp_utility.pm'); 

my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);


sub Scrape_pdf
{
	my $dbh				= shift;
	my $pdf_name		= shift;
	my $Format_ID  		= shift;
	my $councilcode		= shift;
	my $Application_No	= shift;
	my $Source			= shift;
	my $Markup			= shift;
	my $PDF_Path		= shift;
	
	$PDF_Path=$PDF_Path.$Format_ID;
	print "PDF NAME IN Scraping:: $pdf_name\n";
	print "PDF NAME IN Scraping:: $PDF_Path\n";

	my $applicationFormat='F1';
	my $text_file_name=$pdf_name;
	my $xml_file_name=$pdf_name;
	my $xl_file_name=$pdf_name;
	my $html_file_name="PDF2HTML$councilcode.html";
	$text_file_name=~s/\.pdf\s*$/.txt/igs;
	$xl_file_name=~s/\.pdf\s*$/.xls/igs;
	$xml_file_name=~s/\.pdf\s*$/.xml/igs;

	$text_file_name=$PDF_Path.'/'.$text_file_name;
	$xl_file_name=$PDF_Path.'/'.$xl_file_name;
	$xml_file_name=$PDF_Path.'/'.$xml_file_name;
	$html_file_name=$PDF_Path.'/'.$html_file_name;

	if($pdf_name=~m/\.(?:png|jpg)\s*$/is) 
	{
		goto create_xml_last;
	}
	# 
	my $pdf_name_De=$PDF_Path.'/'.$pdf_name;
	eval{
	system("PERL","deillustrate.pl",$pdf_name_De,$pdf_name_De);
	};
	if($@)
	{
		print "$@\n";
		print "Error While Deillustrate\n";
	}
	if($pdf_name=~m/COVER[ING]+_?LETTER|\.jpg|\.png/is) 
	{
		goto create_xml_last;
	}
	my $pdf = eval{CAM::PDF->new("$PDF_Path/$pdf_name")};
	if($@)
	{
		print "$@\n";
		print "Error While CAM::PDF Text Convertion\n";
	}	
	my $page;
	eval{$page = $pdf->numPages()};
	open my $fh, ">:encoding(utf-8)",  $text_file_name   or warn "could not open $text_file_name : $!\n";
	print $fh "";
	close $fh;
	for(my $i=1;$i<=$page;$i++)
	{
		my $pageone_tree;
		eval {
			local $SIG{ALRM} = sub { die "Alarm Timeout\n" };
			alarm 5;
			$pageone_tree = eval{$pdf->getPageContentTree($i)};
			alarm 0;
		};
		if($@){
			print "Alarm Timeout\n";
		}		
		open my $fh, ">>:encoding(utf-8)",  $text_file_name   or warn "could not open $text_file_name : $!\n";
		print $fh eval {CAM::PDF::PageText->render($pageone_tree)};
		close $fh;
		my $pdf_file_cont= eval{CAM::PDF::PageText->render($pageone_tree)};
	}
	
	my $pdf_name_For_HTML=$PDF_Path.'/'.$pdf_name;
	eval{
		system("pdftohtml.exe -h $pdf_name_For_HTML $html_file_name");
	};
	if($@)
	{
		print "$@\n";
		print "Error While pdftohtml HTML Convertion\n";
	}	
		
	# system("pdftotext $pdf_name $text_file_name");
	my $filesize = -s "$text_file_name";
	my $pdf_name_Size = -s "$PDF_Path/$pdf_name";
	if($filesize == 0 || $filesize eq '' )
	{
		$pdf_name=~s/\\/\//igs;
		my $PDF_Filename=(split('/',$pdf_name))[-1];
		
		print "newName::$PDF_Filename";

		my $PDF_Filename=substr $PDF_Filename, 0;

		rename "$PDF_Path/".$PDF_Filename, "$PDF_Path/".$PDF_Filename;

		my $pdf = eval{CAM::PDF->new("$PDF_Path/".$PDF_Filename)};
		for(my $i=1;$i<=$page;$i++)
		{
			my $pageone_tree;
			eval {
				local $SIG{ALRM} = sub { die "Alarm Timeout\n" };
				alarm 5;
				$pageone_tree = eval{$pdf->getPageContentTree($i)};
				alarm 0;
			};
			if($@){
				print "Alarm Timeout\n";
			}		
			open my $fh, ">>:encoding(utf-8)",  $text_file_name   or warn "could not open $text_file_name : $!\n";
			print $fh eval {CAM::PDF::PageText->render($pageone_tree)};
			close $fh;
			my $pdf_file_cont= eval{CAM::PDF::PageText->render($pageone_tree)};
		}
		rename  "$PDF_Path/".$PDF_Filename, "$PDF_Path/$PDF_Filename";
	}
	my $filesize = -s "$text_file_name";		

	if($filesize == 0 || $filesize eq '' )
	{
		my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'I', Last_Visited_Date = getdate() where id = $Format_ID and council_code = $councilcode";
		&DB_OneApp::UpdateDB($dbh, $query);
		goto create_xml_last;
	}
	else
	{
		my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'Y', Last_Visited_Date = getdate() where id = $Format_ID and council_code = $councilcode";
		&DB_OneApp::UpdateDB($dbh, $query);
	}

	open IN,"<$text_file_name";
	my ($content);
	my $Prev_Title_Number=0;
	while(<IN>)
	{
		# $content.=$_;
		if($_=~m/^(\d+)\.\s*[a-z]+/is)
		{
			my $Next_Title_Number=$1;
			$_=~s/\n/<>\n/igs;
			if($Next_Title_Number==$Prev_Title_Number+1)
			{
				$Prev_Title_Number=$Next_Title_Number;
				$content=$content."|Split|\n".$_;
			}
			else
			{
				$content=$content." ".$_;
			}	
		}
		else
		{
			$_=~s/\n/<>\n/igs;
			$content=$content." ".$_;
		}			
	}
	close IN;
	$content.="|Split|\n";
	$content=~s/(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)//igs; # to remove page continuation mark
	$content=~s/Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>//igs; # to remove page reference
	if($content=~m/A\s+p\s+p\s+l\s+i\s+c\s+a\s+n\s+t\s+N\s+a\s+m\s+e/is)
	{
		$content=~s/\s\s+/DoubleSpace/igs; # to remove page reference
		$content=~s/\s//igs; # to remove page reference
		$content=~s/DoubleSpace/ /igs; # to remove page reference
	}

	$_=~s/<>(?:<>)+/<>/igs;

	my $html_file_path=$PDF_Path;
	opendir(my $dhf, $html_file_path) || warn "can't opendir $html_file_path: $!";
	my @File_Names=readdir($dhf);
	closedir $dhf;
	my ($Html_Content);
	for(@File_Names)
	{
		if(($_=~m/PDF2HTML$councilcode/is) && ($_=~m/\.HTML/is))
		{
			open(re,"<$_");
			my $Prev_Title_Number=0;
			while(<re>)
			{
				# $Html_Content.=$_;
				if($_=~m/>(\d+)\.\s*[a-z]+/is)
				{
					my $Next_Title_Number=$1;
					if($Next_Title_Number==$Prev_Title_Number+1)
					{
						$Prev_Title_Number=$Next_Title_Number;
						$Html_Content=$Html_Content."|Split|\n".$_;
					}
					else
					{
						$Html_Content=$Html_Content." ".$_;
					}	
				}
				else
				{
					$Html_Content=$Html_Content." ".$_;
				}			
			}
			close re;			
		}	
	}	


	$Html_Content.="|Split|\n";
	$Html_Content=~s/(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)//igs; # to remove page continuation mark
	$Html_Content=~s/Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>//igs; # to remove page reference
	open(wr,">Formated_HTML_Content.html");
	print wr $Html_Content;
	close wr;

	my $flag=0;
	my ($site_house,$site_suffix,$site_house_name,$site_street,$site_city,$Site_County,$site_country,$site_postcode,$easting,$northing,$applicant_title,$applicant_first_name,$applicant_surname,$applicant_company_name,$applicant_street,$applicant_town, $applicant_county, $applicant_postcode, $applicant_telephone, $applicant_mobile, $applicant_fax, $applicant_email,$agent, $agent_title, $agent_first_name, $agent_surname,$applicant_country, $agent_company_name, $agent_street, $agent_street, $agent_town, $agent_county, $agent_country, $agent_postcode, $agent_telephone, $agent_mobile, $agent_fax,$agent_email,$oneapp_proposal,$vehicle_car,$vehicle_light_good,$vehicle_motorcycle,$vehicle_disability,$vehicle_cycle,$vehicle_other,$vehicle_other_desc,$res, $existing_wall_material, $proposed_wall_material, $existing_roof_material, $proposed_roof_material, $existing_window_material, $proposed_window_material, $existing_doors_material, $proposed_doors_material, $existing_boundary_treatment_material, $proposed_boundary_treatment_material, $existing_vehicle_access_material, $proposed_vehicle_access_material, $existing_lightning_material, $proposed_lighning_material, $existing_others_materials, $proposed_others_materials, $material_additional_information, $material_additional_info_details, $existing_use, $existing_residential_house, $proposed_houses, $proposed_flats_maisonettes, $proposed_live_work_units, $proposed_cluster_flats, $proposed_sheltered_housing, $proposed_bedsit_studios, $proposed_unknown, $proposed_market_housing_total, $existing_houses, $existing_flats_maisonettes, $existing_live_work_units, $existing_cluster_flats, $existing_sheltered_housing, $existing_bedsit_studios, $existing_unknown, $existing_market_housing_total, $total_proposed_residential_units, $total_existing_residential_units, $non_residential_shops, $non_residential_financial, $non_residential_restaurants, $non_residential_drinking, $non_residential_food, $non_residential_office, $non_residential_research, $non_residential_light, $non_residential_general, $non_residential_storage, $non_residential_hotels, $non_residential_residential, $non_residential_institutions, $non_residential_assembly, $non_residential_other, $non_residential_total, $existing_employee, $proposed_employee, $site_area);
	if($content=~m/\d+\.\s*(?:site\s*address|Trees\s*Location)\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $site=$1;
		$flag=1;
		
		print "------------ Site Details ------------\n";
		
		if($site=~m/House\:([\w\W]*?)\s*Suffix/is)
		{
			$site_house	= Oneapp_utility::trim($1);
			$site_house	=~s/<>/ /igs;
			$site_house	=~s/\s\s+/ /igs;
			print "site_house		:: $site_house\n";
		}
		elsif($site=~m/House\:([^<]*?)\s*</is)
		{
			$site_house	= Oneapp_utility::trim($1);
			$site_house	=~s/<>/ /igs;
			$site_house	=~s/\s\s+/ /igs;
			print "site_house		:: $site_house\n";
		}
		if($site=~m/Suffix\:([\w\W]*?)\s*House\s*name/is)
		{
			$site_suffix=Oneapp_utility::trim($1);
			$site_suffix=~s/<>/ /igs;
			$site_suffix=~s/\s\s+/ /igs;
		}
		elsif($site=~m/Suffix\:([^<]*?)\s*</is)
		{
			$site_suffix=Oneapp_utility::trim($1);
			$site_suffix=~s/<>/ /igs;
			$site_suffix=~s/\s\s+/ /igs;
		}
		if($site=~m/House\s*name\:([\w\W]*?)\s*Street\s*address/is)
		{
			$site_house_name= Oneapp_utility::trim($1);
			$site_house_name=~s/<>/ /igs;
			$site_house_name=~s/\s\s+/ /igs;
		}
		elsif($site=~m/House\s*name\:([^<]*?)\s*</is)
		{
			$site_house_name	= Oneapp_utility::trim($1);
			$site_house_name	=~s/\s+/ /igs;
		}
		if($site=~m/Street\s*address\:([\w\W]*?)\s*Town\/City/is)
		{
			$site_street= Oneapp_utility::trim($1);
			$site_street=~ s/<>/ /igs;
			$site_street=~ s/\s\s+/ /igs;
		}
		if($site=~m/Town\/city\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$site_city	= Oneapp_utility::trim($1);
			$site_city	=~ s/\:\s*//igs;
			$site_city	=~ s/\s+/ /igs;
		}
		if($site=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		{
			$Site_County	= Oneapp_utility::trim($1);
			$Site_County	=~ s/\:\s*//igs;
			$Site_County	=~ s/\s+/ /igs;
		}		
		if($site=~m/Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)</is)
		{
			$site_country=Oneapp_utility::trim($1);
			$site_country=~s/\s+/ /igs;
		}
		if($site=~m/>\s*Postcode\s*(?:\:)?\s*(?:<>)?\s*([^>]*?)\s*(?:<|Are)/is)
		{
			$site_postcode	= $1;
			$site_postcode	=~ s/\W/ /igs;
			$site_postcode	=~s/\s\s+/ /igs;
		}

		if($site=~m/Easting\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Northing)/is)
		{
			$easting	= Oneapp_utility::trim($1);
			$easting	=~ s/\s+/ /igs;
		}
		if($site=~m/Northing\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$northing	= Oneapp_utility::trim($1);
			$northing	=~ s/\s+/ /igs;
		}	
	}
	print "site_suffix		:: $site_suffix\n";
	print "site_house_name		:: $site_house_name\n";
	print "site_street		:: $site_street\n";
	print "site_city		:: $site_city\n";
	print "Site_County		:: $Site_County\n";
	print "site_country:: $site_country\n";
	print "site_postcode		:: $site_postcode\n";
	print "Northing		:: $northing\n";
	print "Easting			:: $easting\n";

	# "------------ Applicant Phone, Mobile And Fax By using PDF to Excel Conversion ------------\n";
	
	my $xl_file_name_read=$xl_file_name.'x';

	eval{
	my $excel = Spreadsheet::XLSX->new("$xl_file_name_read",);

    my $worksheet = $excel->Worksheet("Sheet1");
	my ($row_min,$row_max) = $worksheet->row_range();
	my ($col_min,$col_max) = $worksheet->col_range();

	my (@TPH_Value,@MN_Value,@Fax_Value);
	my $ph_cnt=0;
	my $mo_cnt=0;
	my $fa_cnt=0;
	for(my $r=$row_min;$r<=$row_max;$r++)
	{
		for(my $c=$col_min;$c<=$col_max;$c++)
		{
			my $cell = $worksheet->get_cell($r,$c);
			if($cell)
			{
				my $Value = $cell->value();
				if($Value=~m/Telephone\s*number\s*\:/is)
				{
					my $Telephone_Col_Range=$c+10;
					for(my $TPH=$c+1;$TPH<=$Telephone_Col_Range;$TPH++)
					{
						my $TPH_Cell = $worksheet->get_cell($r,$TPH);
						if($TPH_Cell)
						{
							my $val_temp=$TPH_Cell->value();
							if($val_temp!~m/[a-z]/is)
							{
								$TPH_Value[$ph_cnt].=$val_temp;
							}	
						}	
					}	
					$ph_cnt++;
				}	
				elsif($Value=~m/Mobile\s*number\s*\:/is)
				{
					my $Mobile_Col_Range=$c+10;
					for(my $MN=$c+1;$MN<=$Mobile_Col_Range;$MN++)
					{
						my $MN_Cell = $worksheet->get_cell($r,$MN);
						if($MN_Cell)
						{
							my $Val_Tmp=$MN_Cell->value();
							if($Val_Tmp!~m/[a-z]/is)
							{
								$MN_Value[$mo_cnt].=$Val_Tmp;
							}	
						}	
					}	
					$mo_cnt++;
				}	
				elsif($Value=~m/Fax/is)
				{
					my $Fax_Col_Range=$c+10;
					for(my $Fax=$c+1;$Fax<=$Fax_Col_Range;$Fax++)
					{
						my $Fax_Cell = $worksheet->get_cell($r,$Fax);
						if($Fax_Cell)
						{
							my $val_tmp1=$Fax_Cell->value();
							if($val_tmp1!~m/[a-z]/is)
							{
								$Fax_Value[$fa_cnt].=$val_tmp1;
							}	
						}	
					}	
					$fa_cnt++;
				}
			}	
		}		
	}
	if($TPH_Value[0])
	{
		$applicant_telephone=$TPH_Value[0];
	}	
	if($TPH_Value[1])
	{
		$agent_telephone=$TPH_Value[1]
	}	
	if($MN_Value[0])
	{
		$applicant_mobile=$MN_Value[0] 
	}
	if($MN_Value[1])
	{
		$agent_mobile=$MN_Value[1]
	}
	if($Fax_Value[0])
	{
		$applicant_fax=$Fax_Value[0]
	}
	if($Fax_Value[1])
	{
		$agent_fax=$Fax_Value[1]
	}
	};
	# "------------ Applicant Excel Conversion End ------------\n";
	
	print "------------ Applicant Details ------------\n";
	my $applicant;
	if($content=~m/\d+\.\s*Applicant\s*name([\w\W]*?)\|Split\|\s*/is)
	{
		$applicant=$1;
		if($applicant=~m/Title\:\s*([^<]*?)<>\s*First\s*name\:\s*([^<]*?)<>\s*Surname\:\s*([^<]*?)\s*</is)
		{
			$applicant_title 		= Oneapp_utility::trim($1);
			$applicant_first_name 	= Oneapp_utility::trim($2);
			$applicant_surname 		= Oneapp_utility::trim($3);
			$applicant_title		=~ s/\s+/ /igs;
			$applicant_first_name	=~ s/\s+/ /igs;
			$applicant_surname		=~ s/\s+/ /igs;
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		elsif ($applicant=~m/Title\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*First\s*name\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*Surname\:\s*(?:\s*<>\s*)*([^<]*?)\s*</is)
		{
			$applicant_title 		= Oneapp_utility::trim($1);
			$applicant_first_name 	= Oneapp_utility::trim($2);
			$applicant_surname 		= Oneapp_utility::trim($3);
			$applicant_title		=~ s/\s+/ /igs;
			$applicant_first_name	=~ s/\s+/ /igs;
			$applicant_surname		=~ s/\s+/ /igs;
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		
		if($applicant=~m/Company\s*name\s*\:?\s*([\w\W]*?)\s*Street\s*address/is)
		{
			$applicant_company_name = Oneapp_utility::trim($1);
			$applicant_company_name	=~ s/\s+/ /igs;
			print "applicant_company_name	:: $applicant_company_name\n";
		}
		elsif($applicant=~m/Company\s*name\s*\:?\s*([^<]*?)\s*</is)
		{
			$applicant_company_name = Oneapp_utility::trim($1);
			$applicant_company_name	=~ s/\s+/ /igs;
			print "applicant_company_name	:: $applicant_company_name\n";
		}
		
		if($applicant=~m/Street\s*address\s*\:?\s*([\w\W]*?)(?:Town\/City|Telephone)/is)
		{
			$applicant_street	= Oneapp_utility::trim($1);
			$applicant_street	=~ s/<>/ /igs;
			$applicant_street	=~ s/\s+/ /igs;
		}
		elsif($applicant=~m/Street\s*address\s*\:?\s*([\w\W]*?)Town\/City/is)
		{
			$applicant_street	= Oneapp_utility::trim($1);
			$applicant_street	=~ s/<>/ /igs;
			$applicant_street	=~ s/\s+/ /igs;
			print "applicant_street	:: $applicant_street\n";
		}
		
		if($applicant=~m/Town\/city\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$applicant_town	= $1;
			$applicant_town	=~ s/\s+/ /igs;
			print "applicant_town		:: $applicant_town\n";
		}
		if($applicant=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		{
			$applicant_county	= Oneapp_utility::trim($1);
			$applicant_county	=~ s/\s+/ /igs;
			print "applicant_county	:: $applicant_county\n";
		}
		if($applicant=~m/>\s*Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)/is)
		{
			$applicant_country	=Oneapp_utility::trim($1);
			$applicant_country	=~s/\s+/ /igs;
			print "applicant_country	:: $applicant_country\n";
		}
		if($applicant=~m/>\s*Postcode\s*(?:\:)?\s*(?:<>)?\s*([^>]*?)\s*(?:<|Are)/is)
		{
			$applicant_postcode	= Oneapp_utility::trim($1);
			$applicant_postcode	=~s/\s\s+/ /igs;
			print "applicant_postcode	:: $applicant_postcode\n";
		}
		## Code Number
		my $applicant_telephone_Code='';
		if($applicant_telephone eq '')
		{
			if($applicant =~ m/>\s*Postcode\s*\:?\s*[\w\s\-]*?<>\s*([\d]+)\s*Country/is)
			{
				$applicant_telephone_Code= Oneapp_utility::trim($1);
				$applicant_telephone_Code=~ s/\s+/ /igs;
			}
			elsif($applicant =~ m/>\s*([\d]+)\s*Country\s*<>\s*Code/is)
			{
				$applicant_telephone_Code= Oneapp_utility::trim($1);
				$applicant_telephone_Code=~ s/\s+/ /igs;
			}	
			
			if($applicant =~ m/Telephone\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*<>/is)
			{
				$applicant_telephone	= Oneapp_utility::trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				$applicant_telephone=$applicant_telephone_Code." ".$applicant_telephone;
			}
			elsif($applicant =~ m/\s+([\d\s]+?)National\s*Number/is)
			{
				$applicant_telephone	= Oneapp_utility::trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				$applicant_telephone=$applicant_telephone_Code." ".$applicant_telephone;
			}
			elsif($applicant =~ m/>\s*([\d\s]+?)\s*National\s*<>\s*Number\s*</is)
			{
				$applicant_telephone	= Oneapp_utility::trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				$applicant_telephone=$applicant_telephone_Code." ".$applicant_telephone;
			}		
			elsif($applicant =~ m/Telephone\s*number\s*\:([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s+/is)
			{
				$applicant_telephone	= Oneapp_utility::trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				print "applicant_telephone		:: $applicant_telephone\n";
			}
			else
			{
				$applicant_telephone=$applicant_telephone_Code;
			}
		}	
		
		$applicant_telephone=~s/^\s+//igs;
		if($applicant_mobile eq '')
		{
			if($applicant =~ m/Mobile\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
			{
				$applicant_mobile	= Oneapp_utility::trim($1);
				$applicant_mobile	=~ s/\s+/ /igs;
				print "applicant_mobiles		:: $applicant_mobile\n";
			}
		}
		if($applicant_fax eq '')
		{
			if($applicant =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
			{
				$applicant_fax	= Oneapp_utility::trim($1);
				$applicant_fax	=~ s/\s+/ /igs;
				print "applicant_fax		:: $applicant_fax\n";
			}
		}	

		if($applicant=~m/\s*([^<]*?\@[^<]*?)\s/is)
		{
			$applicant_email	= Oneapp_utility::trim($1);
			$applicant_email	=~ s/[^<]*?\n+([^<]+?\@)/$1/igs;
			$applicant_email	=~ s/\s+//igs;
			print "applicant_email		:: $applicant_email\n";
		}
	}
	# for Phone & fax using HTML content
	if($Html_Content=~m/\d+\.\s*Applicant\s*name([\w\W]*?)\|Split\|\s*/is)
	{
		my $applicant_html=$1;
		if($applicant_telephone eq '')
		{
			if($applicant_html =~ m/Telephone\s*number\s*:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				my $no1= $1;
				my $no2= $2;
				my $no3= $3;
				if($no3!='')
				{
					$applicant_telephone = $1." ".$2." ".$3;
					$applicant_telephone = ~s/\s+/ /igs;
				}	
			}
		}	
		if($applicant_mobile eq '')
		{
			if($applicant_html =~ m/Mobile\s*number\s*:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$applicant_mobile	= $1." ".$2;
				$applicant_mobile	=~ s/\s+/ /igs;
			}
		}
		if($applicant_fax eq '')
		{	
			if($applicant_html =~ m/Fax\s*number\s*:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$applicant_fax	= $1." ".$2;
				$applicant_fax	=~ s/\s+/ /igs;
			}
		}	
	}
	
#### New
	my ($applicant_mobile_t,$applicant_fax_t);
	my $applicant_fax_t_Flag=0;
	if($applicant=~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$applicant_mobile_t	= $1." ".$2;
		$applicant_fax_t	= $3." ".$4;
		$applicant_mobile_t	=~ s/\s+/ /igs;
		$applicant_fax_t	=~ s/\s+/ /igs;
		# print "applicant_mobile 		:: $applicant_mobile_t\n";
		# print "agent_fax 		:: $applicant_fax_t\n";
		if($applicant_fax_t!~m/^\s*$/is)
		{
			$applicant_fax_t_Flag=1;
		}
	}
	elsif($applicant =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		my $no3	= $3;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$applicant_mobile_t	= $no1." ".$no2;
			$applicant_fax_t	= $no3;
		}	
		elsif(length($no1) > 5)
		{
			$applicant_mobile_t	= $no1;
			$applicant_fax_t	= $no2." ".$no3;
		}	
		$applicant_mobile_t	=~ s/\s+/ /igs;
		$applicant_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile 		:: $applicant_mobile_t\n";
		# print "applicant_fax 		:: $applicant_fax_t\n";
		if($applicant_fax_t!~m/^\s*$/is)
		{
			$applicant_fax_t_Flag=1;
		}
	}
	elsif($applicant =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$applicant_mobile_t	= $no1." ".$no2;
		}	
		elsif(length($no1) > 5)
		{
			$applicant_mobile_t	= $no1;
			$applicant_fax_t	= $no2;
		}	
		$applicant_mobile_t	=~ s/\s+/ /igs;
		$applicant_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile 		:: $applicant_mobile_t\n";
		# print "applicant_fax 		:: $applicant_fax_t\n";
		if($applicant_fax_t!~m/^\s*$/is)
		{
			$applicant_fax_t_Flag=1;
		}
	}
	elsif($applicant =~ m/>\s*([\d\s]+?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$applicant_mobile_t	= $1;
		$applicant_mobile_t	=~ s/\s+/ /igs;
		# print "applicant_mobile 		:: $applicant_mobile_t\n";
	}
	
	if($applicant_fax_t_Flag==0)
	{
		if($applicant =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
		{
			$applicant_fax_t	= Oneapp_utility::trim($1);
			$applicant_fax_t	=~ s/\s+/ /igs;
			# $res.="\t<Applicant_Postcode>$value</Applicant_Postcode>\n";
			# print "applicant_fax		:: $applicant_fax_t\n";
		}
	}
	
	if($applicant_mobile eq '')
	{
		$applicant_mobile=$applicant_mobile_t;
	}
	if($applicant_fax eq '')
	{
		$applicant_fax=$applicant_fax_t;
	}
#### New End	
	
	$applicant_telephone=~s/^\s+//igs;
	$applicant_mobile=~s/^\s+//igs;
	$applicant_fax=~s/^\s+//igs;
	print "applicant_mobile 	:: $applicant_mobile\n";
	print "applicant_fax 		:: $applicant_fax\n";
	
	
	print "------------ Agent Details ------------\n";
	my $agent;	
	if($content=~m/\d+\.\s*Agent\s*name\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		$agent=$1;
		if($agent=~m/Title\s*\:?\s*([^<]*?)\s*<>\s*First\s*name\s*\:?\s*([^<]*?)\s*<>\s*Surname\s*\:?\s*([^<]*?)\s*<>/is)
		{
			$agent_title 		= $1;
			$agent_first_name 	= $2;
			$agent_surname 		= $3;
			$agent_title		=~ s/\s+/ /igs;
			$agent_first_name	=~ s/\s+/ /igs;
			$agent_surname		=~ s/\s+/ /igs;
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		elsif($agent=~m/Title\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*First\s*name\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*Surname\:\s*(?:\s*<>\s*)*([^<]*?)\s*</is)
		{
			$agent_title 		= $1;
			$agent_first_name 	= $2;
			$agent_surname 		= $3;
			$agent_title		=~ s/\s+/ /igs;
			$agent_first_name	=~ s/\s+/ /igs;
			$agent_surname		=~ s/\s+/ /igs;
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		if($agent=~m/Company\s*name\s*\:?\s*([\w\W]*?)\s*Street\s*address/is)
		{
			$agent_company_name = $1;
			$agent_company_name	=~ s/\s+/ /igs;
			print "agent_company_name 	:: $agent_company_name\n";
		}
		elsif($agent=~m/Company\s*name\s*\:?\s*([^<]*?)\s*</is)
		{
			$agent_company_name = $1;
			$agent_company_name	=~ s/\s+/ /igs;
			print "agent_company_name 	:: $agent_company_name\n";
		}
		if($agent=~m/Street\s*address\s*\:?\s*([\w\W]*?)(?:Town\/City|Telephone)/is)
		{
			$agent_street 	= $1;
			$agent_street	=~ s/<>/ /igs;
			$agent_street	=~ s/\s+/ /igs;
			print "agent_street 		:: $agent_street\n";
		}
		elsif($agent=~m/Street\s*address\s*\:?\s*([\w\W]*?)Town\/City/is)
		{
			$agent_street 	= $1;
			$agent_street	=~ s/<>/ /igs;
			$agent_street	=~ s/\s+/ /igs;
			print "agent_street 		:: $agent_street\n";
		}
		
		if($agent=~m/Town\/city\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$agent_town	= $1;
			$agent_town	=~ s/\s+/ /igs;
			print "agent_town 		:: $agent_town\n";
		}
		if($agent=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		{
			$agent_county	= $1;
			$agent_county	=~ s/\s+/ /igs;
			print "agent_county 		:: $agent_county\n";
		}
		if($agent=~m/>\s*Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)/is)
		{
			$agent_country	= $1;
			$agent_country	=~ s/\s+/ /igs;
			print "agent_country 		:: $agent_country\n";
		}
		if($agent =~ m/>\s*Postcode\s*(?:\:)?\s*(?:<>)?\s*([^>]*?)\s*(?:<|Are)/is)
		{
			$agent_postcode	= $1;
			$agent_postcode	=~ s/\W/ /igs;
			$agent_postcode	=~ s/\s\s+/ /igs;
			print "agent_postcode 		:: $agent_postcode\n";
		}
		## Code Number
		my $agent_telephone_Code='';
		if($agent_telephone eq '')
		{
			if($agent =~ m/>\s*Postcode\s*\:?\s*[\w\s\-]*?<>\s*([\d]+)\s*Country/is)
			{
				$agent_telephone_Code= Oneapp_utility::trim($1);
				$agent_telephone_Code=~ s/\s+/ /igs;
			}
			elsif($agent =~ m/>\s*([\d\s]+)\s*Country\s*<>\s*Code/is)
			{
				$agent_telephone_Code= Oneapp_utility::trim($1);
				$agent_telephone_Code=~ s/\s+/ /igs;
			}

			if($agent =~ m/Telephone\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*<>/is)
			{
				$agent_telephone	= Oneapp_utility::trim($1);
				$agent_telephone	=~ s/\s+/ /igs;
				$agent_telephone=$agent_telephone_Code." ".$agent_telephone;
			}
			elsif($agent =~ m/\s+([\d\s]+?)National\s*Number/is)
			{
				$agent_telephone	= Oneapp_utility::trim($1);
				$agent_telephone	=~ s/\s+/ /igs;
				$agent_telephone=$agent_telephone_Code." ".$agent_telephone;
			}
			elsif($agent =~ m/>\s*([\d\s]+?)\s*National\s*<>\s*Number\s*</is)
			{
				$agent_telephone	= Oneapp_utility::trim($1);
				$agent_telephone	=~ s/\s+/ /igs;
				$agent_telephone=$agent_telephone_Code." ".$agent_telephone;
			}
		}	
		my $Fax_Flag=0;
		if($agent_mobile eq '')
		{
			if($agent =~ m/Mobile\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*<>/is)
			{
				$agent_mobile	= $1;
				$agent_mobile	=~ s/\s+/ /igs;
			}		
		}	
		if($agent_fax eq '')
		{
			if($agent =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
			{
				$agent_fax	= Oneapp_utility::trim($1);
				$agent_fax	=~ s/\s+/ /igs;
			}
		}	

		if($agent=~m/([\w\.\-]+\@[\w\-]+\.[a-z]{2,3}\.*[a-z]{0,2})/is)
		{
			$agent_email	= $1;
			$agent_email	=~ s/\s+//igs;
		}
		
	}
	# for Phone & fax using HTML content
	if($Html_Content=~m/\d+\.\s*Agent\s*name\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $agent_html=$1;
		if($agent_telephone eq '')
		{
			if($agent_html=~m/Telephone\s*number\s*\:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				my $no1= $1;
				my $no2= $2;
				my $no3= $3;
				$agent_telephone = $1." ".$2." ".$3;
				$agent_telephone =~s/\s+/ /igs;
				$agent_telephone =~s/^\s+|\s+$//igs;
			}
		}	
		if($agent_mobile eq '')
		{
			if($agent_html =~ m/Mobile\s*number\s*\:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$agent_mobile	= $1." ".$2;
				$agent_mobile	=~ s/\s+/ /igs;
			}
		}
		if($agent_fax eq '')
		{	
			if($agent_html =~ m/Fax\s*number\s*\:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$agent_fax	= $1." ".$2;
				$agent_fax	=~ s/\s+/ /igs;
			}
		}	
	}
	
#### NEW	
	my ($agent_mobile_t,$agent_fax_t);
	my $agent_fax_t_Flag=0;
	if($agent=~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$agent_mobile_t	= $1." ".$2;
		$agent_fax_t	= $3." ".$4;
		$agent_mobile_t	=~ s/\s+/ /igs;
		$agent_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile_t 		:: $agent_mobile_t\n";
		# print "agent_fax_t 		:: $agent_fax_t\n";
		if($agent_fax_t!~m/^\s*$/is)
		{
			$agent_fax_t_Flag=1;
		}
	}
	elsif($agent =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		my $no3	= $3;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$agent_mobile_t	= $no1." ".$no2;
			$agent_fax_t	= $no3;
		}	
		elsif(length($no1) > 5)
		{
			$agent_mobile_t	= $no1;
			$agent_fax_t	= $no2." ".$no3;
		}	
		$agent_mobile_t	=~ s/\s+/ /igs;
		$agent_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile_t 		:: $agent_mobile_t\n";
		# print "agent_fax_t 		:: $agent_fax_t\n";
		if($agent_fax_t!~m/^\s*$/is)
		{
			$agent_fax_t_Flag=1;
		}
	}
	elsif($agent =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$agent_mobile_t	= $no1." ".$no2;
		}	
		elsif(length($no1) > 5)
		{
			$agent_mobile_t	= $no1;
			$agent_fax_t	= $no2;
		}	
		$agent_mobile_t	=~ s/\s+/ /igs;
		$agent_fax_t	=~ s/\s+/ /igs;
		if($agent_fax_t!~m/^\s*$/is)
		{
			$agent_fax_t_Flag=1;
		}
	}
	elsif($agent =~ m/>\s*([\d\s]+?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$agent_mobile_t	= $1;
		$agent_mobile_t	=~ s/\s+/ /igs;
	}
	
	if($agent_fax_t_Flag==0)
	{
		if($agent =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
		{
			$agent_fax_t	= Oneapp_utility::trim($1);
			$agent_fax_t	=~ s/\s+/ /igs;
		}
	}	
	if($agent_mobile eq '')
	{
		$agent_mobile=$agent_mobile_t;
	}
	if($agent_fax eq '')
	{
		$agent_fax=$agent_fax_t;
	}	
	
	$agent_telephone=~s/^\s+//igs;
	$agent_mobile=~s/^\s+//igs;
	$agent_fax=~s/^\s+//igs;
	print "agent_telephone		:: $agent_telephone\n";
	print "agent_mobile 		:: $agent_mobile\n";
	print "agent_fax 		:: $agent_fax\n";
	print "agent_email 		:: $agent_email\n";
	
	##### ONEAPP PROPOSAL ##########
	if($content=~m/\d+\.\s*Description\s*of\s*the\s*Proposal[^<]*?change\s*of\s*use\:\s*(?:<>)?\s*([\w\W]*?)\s*Has\s*the/is)
	{
		$oneapp_proposal	= $1;
		$oneapp_proposal	=~ s/\s+/ /igs;
		$oneapp_proposal	=~ s/<>/ /igs;
	}
	elsif($content=~m/on\s*the\s*decision\s*letter\s*\:(?:<>)?\s*([\w\W]*?)Application\s*reference\s*number[\w\W]*?\s*Condition\s*number\(s\)\:\s*[\d]?\s*[a-z]?\s*\)?\s*([\w\W]*?)\s*Has\s*the\s*development\s*already\s*started/is)
	{
		$oneapp_proposal	= $1.$2;
		$oneapp_proposal	=~ s/\s+/ /igs;
		$oneapp_proposal	=~ s/<>/ /igs;
	}
	elsif($content=~m/demolish\s*the\s*listed\s*building\(s\)\s*\:\s*(?:<>)?\s*([\w\W]*?)\s*Has\s*the/is)
	{
		$oneapp_proposal	= $1;
		$oneapp_proposal	=~ s/\s+/ /igs;
	}
	elsif($content=~m/proposed\s*works\s*\:\s*(?:<>)?\s*([\w\W]*?)(?:<>)?\s*Has\s*the/is )
	{
		$oneapp_proposal	= $1;
		$oneapp_proposal	=~ s/\s+/ /igs;
		$oneapp_proposal	=~ s/<>/ /igs;
	}
	elsif($content=~m/\d+\.\s*(?:Description\s*[a-z\s]*?\s*Proposal|Type\s*of\s*Proposed|Description\s*of\s*Proposed\s*Works|[^>]*?Description\s*Of\s*Works)([\w\W]*?)\s*\|Split\|/is)
	{
		my $oneapp_proposal_Block= $1;
		$oneapp_proposal_Block=~s/(?:why|where|how|Are|does|Has)[^>]*?<>\s*No<>\s*Yes(?:<>)?//igs; # to remove yes or no Questions
		$oneapp_proposal_Block=~s/(?:why|where|how|Are|does|Has)[^>]*?<>\s*Yes<>\s*No(?:<>)?//igs; # to remove yes or no Questions
		if($oneapp_proposal_Block=~m/(?:Description|describe|Proposal|proposed)[^>]*?(?:Description|describe|Proposal|proposed)[^>]*?\:\s*(?:<>)?\s*([\w\W]*?)(?:<>)?\s*(?:has\s*the|how)/is)
		{
			$oneapp_proposal= $1;
		}
		elsif($oneapp_proposal_Block=~m/E\.\s*g\.[^>]*?\s*<>\s*([\w\W]*?)\s*$/is)
		{
			$oneapp_proposal= $1;
		}
		elsif($oneapp_proposal_Block=~m/If\s*Yes,\s*please[^>]*?\(include[\w\W]*?\)[^<]*?<>([\w\W]*?)(?:<>)?$/is)
		{
			$oneapp_proposal= $1;
		}
		elsif($oneapp_proposal_Block=~m/please\s*describe[^>]*?[\w\W]*?[^<]*?<>\s*([\w\W]*?)\s*(?:<>)?$/is)
		{
			$oneapp_proposal= $1;
		}
	}
	$oneapp_proposal=&Oneapp_utility::trim($oneapp_proposal);
	$oneapp_proposal=~s/<>/ /igs;
	$oneapp_proposal=~s/\s\s+/ /igs;
	print "oneapp_proposal 	:: $oneapp_proposal\n";
	print "------------ Vehicle Parking ------------\n";
	
	if($content=~m/\d+\.\s*(?:Vehicle)?\s*Parking\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $vehicle = $1;
		if($vehicle=~m/Cars\s*([\-\d\s<>]+)/is)
		{
			$vehicle_car	= $1;
			$vehicle_car	=~ s/<>//igs;
			$vehicle_car	=~ s/\s+/|/igs;
			$vehicle_car	=~ s/\s*\|\s*$//igs;
			$vehicle_car	=~ s/^\||\|$//igs;
			print "vehicle_car 		:: $vehicle_car\n";
		}
		if($vehicle=~m/Light\s*goods\s*vehicles\/public\s*carrier\s*vehicles\s*([\-\d\s<>]+)/is)
		{
			$vehicle_light_good	= $1;
			$vehicle_light_good	=~ s/<>//igs;
			$vehicle_light_good	=~ s/\s+/|/igs;
			$vehicle_light_good	=~ s/\s*\|\s*$//igs;
			$vehicle_light_good	=~ s/^\||\|$//igs;
			print "vehicle_light_good 	:: $vehicle_light_good\n";
		}
		if($vehicle=~m/Motorcycles\s*([\-\d\s<>]+)/is)
		{
			$vehicle_motorcycle	= $1;
			$vehicle_motorcycle	=~ s/<>//igs;
			$vehicle_motorcycle	=~ s/\s+/|/igs;
			$vehicle_motorcycle	=~ s/\s*\|\s*$//igs;
			$vehicle_motorcycle	=~ s/^\||\|$//igs;
			print "vehicle_motorcycle 	:: $vehicle_motorcycle\n";
		}
		if($vehicle=~m/Disability\s*spaces\s*([\-\d\s<>]+)/is)
		{
			$vehicle_disability	= $1;
			$vehicle_disability	=~ s/<>//igs;
			$vehicle_disability	=~ s/\s+/|/igs;
			$vehicle_disability	=~ s/\s*\|\s*$//igs;
			$vehicle_disability	=~ s/^\||\|$//igs;
			print "vehicle_disability 	:: $vehicle_disability\n";
		}
		if($vehicle=~m/Cycle\s*spaces\s*([\-\d\s<>]+)/is)
		{
			$vehicle_cycle	= $1;
			$vehicle_cycle	=~ s/<>//igs;
			$vehicle_cycle	=~ s/\s+/|/igs;
			$vehicle_cycle	=~ s/\s*\|\s*$//igs;
			$vehicle_cycle	=~ s/^\||\|$//igs;
			print "vehicle_cycle 		:: $vehicle_cycle\n";
		}
		# print "applicant :; $applicant \n";
		if($vehicle =~ m/Other\s*\(e\.g\.\s*Bus\)\s*([\-\d\s<>]+)/is)
		{
			$vehicle_other	= $1;
			$vehicle_other	=~ s/<>//igs;
			$vehicle_other	=~ s/\s+/|/igs;
			$vehicle_other	=~ s/\s*\|\s*$//igs;
			$vehicle_other	=~ s/^\||\|$//igs;
			print "vehicle_other 		:: $vehicle_other\n";
		}
		if($vehicle =~ m/description\s*of\s*Other\s*(?:<>)?\s*([^<]+)\s*</is)
		{
			$vehicle_other_desc	= $1;
			$vehicle_other_desc	=~ s/\s+/ /igs;
			print "vehicle_other_desc 	:: $vehicle_other_desc\n";
		}
	}
	if($content=~m/\d+\.\s*Materials\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Material=$1;
		$Material=~s/\-\s*description\:/-description1-description/igs;
		my @material_type;
		while($Material=~m/([a-zA-Z\ ]+?)\s*\-\s*description/igs)
		{
			my $type=$1;
			push(@material_type,$type);
		}
		my $count=0;
		my ($exist_details,$proposed_details);
		while($Material=~m/-\s*description([\w\W]*?)(?:\-description1|Are\s*you\s+supplying\s+additional\s+information)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\:\s*([^<]*?)\s*Description\s*of\s*proposed/is)
			{
				$exist_details.=$material_type[$count]." Existing Details:".$1."|";
			}
			if($material_details=~m/Description\s*of\s*proposed\s+materials\s+and\s+finishes\:\s*([^<]*?)\s*<>/is)
			{
				$proposed_details.=$material_type[$count]." Proposed Details:".$1."|";
			}
			$count++;
		}
		$exist_details=~s/\s*\|\s*$//igs;
		$proposed_details=~s/\s*\|\s*$//igs;
	}
	if($content=~m/\d+\.\s*[^\n]*?Non\-residential\s*Floorspace([\w\W]*?)\|Split\|\s*/is)
	{
		my $applicant=$1;
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Shops\s*Net\s*Tradable\s*Area\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Financial\s*and\s*professional\s*services\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Restaurants\s*and\s*cafes\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\ )]+)\s*<>\s*Drinking\s*establ?ishments\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Hot\s*food\s*takeaways\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Office\s*\(other\s*than\s*A2\)\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Research\s*and\s*development\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Light\s*industrial\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*General\s*industrial\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Storage\s*or\s*distribution\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\ )]+)\s*<>\s*Hotels\s*and\s*halls\s*of\s*residence\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Residential\s*institutions\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Non\-residential\s*institutions\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Assembly\s*and\s*leisure\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Please\s*Specify\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/\d\s*<>\s*Total\s*([\d\.\s]+)/is)
		{
			my $value=$1;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
	}
	if($content=~m/\d+\.\s*Employment([\w\W]*?)\|Split\|\s*/is)
	{
		my $Employment_Details=$1;
		if($Employment_Details=~m/Existing\s*employees\s*([\d\.\s<>]+)/is)
		{
			my $existing_employee=$1;
			$existing_employee=~s/<>//igs;
			$existing_employee=~s/\s+/|/igs;
			$existing_employee=~s/\s*\|\s*$//igs;
			$existing_employee=~s/^\||\|$//igs;
			print "Existing_employee 	:: $existing_employee\n";
		}
		if($Employment_Details=~m/Proposed\s*employees\s*([\d\.\s<>]+)/is)
		{
			my $proposed_employee=$1;
			$proposed_employee=~s/<>//igs;
			$proposed_employee=~s/\s+/|/igs;
			$proposed_employee=~s/\s*\|\s*$//igs;
			$proposed_employee=~s/^\||\|$//igs;
			print "Proposed_employee 	:: $proposed_employee\n";
		}
	}
	if($content=~m/\d+\.\s*Site\s*Area\s*([\w\W]*?)\|Split\|/is)
	{
		my $Site_Area_Content=$1;
		
		if($Site_Area_Content=~m/>\s*([^<]*?)(?:<>)?\s*(hectares|sq\s*\.\s*metres)\s*/is)
		{
			$site_area=$1." ".$2;
			$site_area=~s/What\s*is\s*the\s*site\s*area\?//igs;
			if($site_area=~m/[a-z]+/is)
			{
				$site_area=~s/([\d\,\.]+)/$1/igs;
			}	
			$site_area=~s/\s+/ /igs;
		}	
		elsif($Site_Area_Content=~m/>\s*([^<]*?)<>\s*(hectares|sq\s*\.\s*metres)\s*/is)
		{
			$site_area=$1." ".$2;
			$site_area=~s/\s+/ /igs;
		}	
		elsif($Site_Area_Content=~m/>\s*([^<]*?)<>\s*([^>]*?)\s*(?:What|<>)/is)
		{
			$site_area=$2." ".$1;
			$site_area=~s/\s+/ /igs;
		}	
		print "Site_area 		:: $site_area\n";
	}	
	if($content=~m/\d+\.\s*Existing\s*Use\s*([\w\W]*?)\|Split\|/is)
	{
		my $Site_existing_use=$1;
		if($Site_existing_use=~m/describe[^>]*?current[^>]*?site\s*\:\s*<>\s*([\w\W]*?)<>/is)
		{
			$existing_use=$1;
			$existing_use=~s/<>/ /igs;
			$existing_use=~s/\s+/ /igs;
		}	
		print "Existing_use 		:: $existing_use\n";
	}	
	if($content=~m/\d+\.\s*Industrial\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Industrial=$1;
		my $details;
		if($Industrial=~m/on\s*site\s*\:\s*([^<]*?)\s*Is\s*the\s*proposal/is)
		{
			$details=$1;
			$details=~s/\s+/ /igs;
		}
		# print "Industrial 			:: $Industrial\n";
	}
	if($content=~m/\d+\.(?:[a-z]*?\s*-)?\s*Declaration\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Declaration=$1;
		$Declaration=~s/\s+/ /igs;
		# print "Declaration :: $Declaration\n";
	}
	##################### Material ########################
	print "---------------------- Entering Material Section -----------------------\n";
	my $Materials_Content;
	if($content=~m/([\d]+)\.\s*Materials/is)
	{
		my $Material_Index=$1+1;
		if($content=~m/$1\.\s*Materials\s*([\w\W]*?)\|Split\|/is)
		{
			$Materials_Content=$1;
		}	
	}
	# $Materials_Content=~s/(\n\s*(?:Window|wall|External\s*doors|Internal\s*doors|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls|Other)[a-z\s]*?-\s*[a-z\s]*\s*description(?:\:)?)/|Description Split|$1/igs;	
	$Materials_Content=~s/([a-z\s]*\s*-\s*[a-z\s]*\s*description(?:\:|<>)?)/|Description Split|$1/igs;	
	$Materials_Content.='|Description Split|';
	my $Materials_Content_For_Others=$Materials_Content;
	#### FOR ROOF METERIAL ####
	my $roof_content;
	if($Materials_Content=~m/(?:\s*<>)?\s*(roof(?:s)?)\s*\-\s*description\:(?:\s*<>)?\s*([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$roof_content=$2;
	}
	elsif($Materials_Content=~m/(roof(?:s)?)\s*\-\s*description\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$roof_content=$2;
	}
	elsif($Materials_Content=~m/(roof(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$roof_content=$2;
	}	

	$Materials_Content_For_Others=~s/(?:\s*<>)?\s*(?:Window|wall|External\s*doors|Internal\s*doors|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls|Other)[a-z\s]*?-\s*[a-z\s]*\s*description\:?(?:\s*<>)?\s*([\w\W]*?\|Description\s*Split\|)//igs;
	$Materials_Content_For_Others=~s/\|Description\s*Split\|//igs;
	while($roof_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(<|Description)/igs) ### Need to Change While Loop
	{
		$existing_roof_material=$existing_roof_material.'|'.$1;				
	}
	if($existing_roof_material=~m/^\s*$/is)
	{
		while($roof_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(<|$)/igs)
		{
			$existing_roof_material=$existing_roof_material.'|'.$1;				
		}
	}
	$existing_roof_material=~s/\s+/ /igs;
	$existing_roof_material=~s/^\s+|\s+$//igs;			
	$existing_roof_material=~s/^\||\|$//igs;			
		
	while($roof_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(<>|$)/igs)
	{
		$proposed_roof_material=$proposed_roof_material.'|'.$1;				
	}
	if($proposed_roof_material=~m/^\s*$/is)
	{
		while($roof_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|$)/igs)
		{
			$proposed_roof_material=$proposed_roof_material.'|'.$1;				
		}
	}
	$proposed_roof_material=~s/\s+/ /igs;
	$proposed_roof_material=~s/^\s+|\s+$//igs;			
	$proposed_roof_material=~s/^\||\|$//igs;
	print "Existing Roof Materials		:: $existing_roof_material\n";
	print "Proposed Roof Materials		:: $proposed_roof_material\n";
	
	#### FOR DOOR METERIAL ####

	my $door_content;
	while($Materials_Content=~m/(?:<>)?(door(?:s)?)\s*\-\s*description\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/igs)
	{		
		$door_content.=$2;
	}
	if($door_content=~m/^\s*$/is)
	{
		while($Materials_Content=~m/(?:<>)?\s*(?:Internal|External)\s*(door(?:s)?)\s*\-\s*(?:add\s*)?description\:?\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/igs)
		{		
			$door_content.=$2;
		}
	}	
	if($door_content=~m/^\s*$/is)
	{
		while($Materials_Content=~m/(door(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)<>(?:Window|wall|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls)/igs)
		{
			$door_content.=$2;
		}	
	}	

	### Existing ###
	while($door_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_doors_material=$existing_doors_material.'|'.$1;				
	}
	if($existing_doors_material=~m/^\s*$/is)
	{
		while($door_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_doors_material=$existing_doors_material.'|'.$1;				
		}
	}
	$existing_doors_material=~s/\s+/ /igs;
	$existing_doors_material=~s/^\s+|\s+$//igs;			
	$existing_doors_material=~s/^\||\|$//igs;
	#Proposed
	while($door_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_doors_material=$proposed_doors_material.'|'.$1;				
	}
	if($proposed_doors_material=~m/^\s*$/is)
	{
		while($door_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_doors_material=$proposed_doors_material.'|'.$1;				
		}
	}
	$proposed_doors_material=~s/\s+/ /igs;
	$proposed_doors_material=~s/^\s+|\s+$//igs;			
	$proposed_doors_material=~s/^\||\|$//igs;	

	print "Existing Door Materials		:: $existing_doors_material\n";
	print "Proposed Door Materials		:: $proposed_doors_material\n";
	
	#### FOR Windows METERIAL ####

	my $window_content;
	if($Materials_Content=~m/(?:<>)?(Window(?:s)?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$window_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(Windows?)\s*\-\s*[a-z\s]*?\s*description\s*\:?\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$window_content=$2;
	}
	elsif($Materials_Content=~m/(Window(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)<>(?:Window|wall|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls)/is)
	{
		$window_content=$2;
	}	

	#Existing
	while($window_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_window_material=$existing_window_material.'|'.$1;				
	}
	if($existing_window_material=~m/^\s*$/is)
	{
		while($window_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_window_material=$existing_window_material.'|'.$1;				
		}
	}
	$existing_window_material=~s/\s+/ /igs;
	$existing_window_material=~s/^\s+|\s+$//igs;			
	$existing_window_material=~s/^\||\|$//igs;	

	#Proposed 
	while($window_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_window_material=$proposed_window_material.'|'.$1;				
	}
	if($proposed_window_material=~m/^\s*$/is)
	{
		while($window_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_window_material=$proposed_window_material.'|'.$1;				
		}
	}
	$proposed_window_material=~s/\s+/ /igs;
	$proposed_window_material=~s/^\s+|\s+$//igs;			
	$proposed_window_material=~s/^\||\|$//igs;	

	print "Existing Window Materials	:: $existing_window_material\n";
	print "Proposed Window Materials	:: $proposed_window_material\n";
	
	#### FOR Wall METERIAL ####
	my $wall_content;
	if($Materials_Content=~m/(?:<>)?(wall(?:s)?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$wall_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(wall(?:s)?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$wall_content=$2;
	}
	elsif($Materials_Content=~m/(wall(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$wall_content=$2;
	}

	#Existing 
	while($wall_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_wall_material=$existing_wall_material.'|'.$1;				
	}
	if($existing_wall_material=~m/^\s*$/is)
	{
		while($wall_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_wall_material=$existing_wall_material.'|'.$1;				
		}
	}
	$existing_wall_material=~s/\s+/ /igs;
	$existing_wall_material=~s/^\s+|\s+$//igs;			
	$existing_wall_material=~s/^\||\|$//igs;		
	
	### Proposed ###
	while($wall_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_wall_material=$proposed_wall_material.'|'.$1;				
	}
	if($proposed_wall_material=~m/^\s*$/is)
	{
		while($wall_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_wall_material=$proposed_wall_material.'|'.$1;				
		}
	}
	$proposed_wall_material=~s/\s+/ /igs;
	$proposed_wall_material=~s/^\s+|\s+$//igs;			
	$proposed_wall_material=~s/^\||\|$//igs;		

	print "Existing Wall Materials		:: $existing_wall_material\n";
	print "Proposed Wall Materials		:: $proposed_wall_material\n";
	
	#### FOR Boundary METERIAL ####
	my $boundary_content;
	if($Materials_Content=~m/(?:<>)?(boundary(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?\s*([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$boundary_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(boundary(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$boundary_content=$2;
	}
	elsif($Materials_Content=~m/(boundary(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$boundary_content=$2;
	}	
	
	#Existing ###
	while($boundary_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*</igs)
	{
		$existing_boundary_treatment_material=$existing_boundary_treatment_material.'|'.$1;
	}
	if($existing_boundary_treatment_material=~m/^\s*$/is)
	{
		while($boundary_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|$)/igs)
		{
			$existing_boundary_treatment_material=$existing_boundary_treatment_material.'|'.$1;
		}
	}
	$existing_boundary_treatment_material=~s/\s+/ /igs;	
	$existing_boundary_treatment_material=~s/^\s+|\s+$//igs;			
	$existing_boundary_treatment_material=~s/^\||\|$//igs;

	### Proposed ###
	while($boundary_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_boundary_treatment_material=$proposed_boundary_treatment_material.'|'.$1;
	}
	if($proposed_boundary_treatment_material=~m/^\s*$/is)
	{
		while($boundary_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_boundary_treatment_material=$proposed_boundary_treatment_material.'|'.$1;
		}
	}
	$proposed_boundary_treatment_material=~s/\s+/ /igs;	
	$proposed_boundary_treatment_material=~s/^\s+|\s+$//igs;			
	$proposed_boundary_treatment_material=~s/^\||\|$//igs;			

	print "Existing Boundry Materials	:: $existing_boundary_treatment_material\n";
	print "Proposed Boundry Materials	:: $proposed_boundary_treatment_material\n";
	
	#### FOR VEHICLE METERIAL ####
	my $Vehicle_content;
	if($Materials_Content=~m/(?:<>)?(Vehicle(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Vehicle_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(Vehicle(?:s)?[a-z\s]*?)\s*\-\s*description\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Vehicle_content=$2;
	}
	elsif($Materials_Content=~m/(Vehicle(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$Vehicle_content=$2;
	}

	#Existing
	while($Vehicle_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_vehicle_access_material=$existing_vehicle_access_material.'|'.$1;
	}
	if($existing_vehicle_access_material=~m/^\s*$/is)
	{
		while($Vehicle_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_vehicle_access_material=$existing_vehicle_access_material.'|'.$1;
		}
	}
	$existing_vehicle_access_material=~s/\s+/ /igs;	
	$existing_vehicle_access_material=~s/^\s+|\s+$//igs;			
	$existing_vehicle_access_material=~s/^\||\|$//igs;
	### Proposed ###
	while($Vehicle_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_vehicle_access_material=$proposed_vehicle_access_material.'|'.$1;
	}
	if($proposed_vehicle_access_material=~m/^\s*$/is)
	{
		while($Vehicle_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_vehicle_access_material=$proposed_vehicle_access_material.'|'.$1;
		}
	}
	$proposed_vehicle_access_material=~s/\s+/ /igs;	
	$proposed_vehicle_access_material=~s/^\s+|\s+$//igs;			
	$proposed_vehicle_access_material=~s/^\||\|$//igs;			
	print "Existing Vehicle Materials	:: $existing_vehicle_access_material\n";
	print "Proposed Vehicle Materials	:: $proposed_vehicle_access_material\n";
	
	#### FOR Lighting METERIAL ####
	my $Lighting_content;
	if($Materials_Content=~m/(?:<>)?(Lighting(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Lighting_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(Lighting(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Lighting_content=$2;
	}
	elsif($Materials_Content=~m/(Lighting(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$Lighting_content=$2;
	}	

	### Existing ###
	while($Lighting_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_lightning_material=$existing_lightning_material.'|'.$1;
	}
	if($existing_lightning_material=~m/^\s*$/is)
	{
		while($Lighting_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_lightning_material=$existing_lightning_material.'|'.$1;
		}
	}
	$existing_lightning_material=~s/\s+/ /igs;	
	$existing_lightning_material=~s/^\s+|\s+$//igs;			
	$existing_lightning_material=~s/^\||\|$//igs;
	
	### Proposed ###
	while($Lighting_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\s*\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_lighning_material=$proposed_lighning_material.'|'.$1;
	}
	if($proposed_lighning_material=~m/^\s*$/is)
	{
		while($Lighting_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_lighning_material=$proposed_lighning_material.'|'.$1;
		}
	}
	$proposed_lighning_material=~s/\s+/ /igs;	
	$proposed_lighning_material=~s/^\s+|\s+$//igs;			
	$proposed_lighning_material=~s/^\||\|$//igs;			
	print "Existing lighning Materials	:: $existing_lightning_material\n";
	print "Proposed lighning Materials	:: $proposed_lighning_material\n";
	
	#### FOR OTHER METERIAL ####
	my $Other_content;
	if($Materials_Content=~m/(?:<>)?(Other(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Other_content=$2;
	}
	elsif($Materials_Content=~m/(Other(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$Other_content=$2;
	}	

	### Existing ###
	while($Other_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_others_materials=$existing_others_materials.'|'.$1;
	}
	if($existing_others_materials=~m/^\s*$/is)
	{
		while($Other_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_others_materials=$existing_others_materials.'|'.$1;
		}
	}
	$existing_others_materials=~s/\s+/ /igs;	
	$existing_others_materials=~s/^\s+|\s+$//igs;			
	$existing_others_materials=~s/^\||\|$//igs;
		### Proposed ###
	while($Other_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\s*\:\s*<>\s*([\w\W]*?)\s*(?:Are|$)/igs)
	{
		$proposed_others_materials=$proposed_others_materials.'|'.$1;
	}
	if($proposed_others_materials=~m/^\s*$/is)
	{
		while($Other_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_others_materials=$proposed_others_materials.'|'.$1;
		}
	}

	### Other Materials ###
	while($Materials_Content_For_Others=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_others_materials=$existing_others_materials.'|'.$1;
	}
	if($existing_others_materials=~m/^\s*$/is)
	{
		while($Materials_Content_For_Others=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_others_materials=$existing_others_materials.'|'.$1;
		}
	}
	$existing_others_materials=~s/<>/ /igs;			
	$existing_others_materials=~s/\s+/ /igs;	
	$existing_others_materials=~s/^\s+|\s+$//igs;			
	$existing_others_materials=~s/^\s*\|+|\|+\s*$//igs;
		### Proposed ###
	while($Materials_Content_For_Others=~m/Description\s*of\s*proposed\s*materials[^>]*?\s*\:\s*<>\s*([\w\W]*?)\s*(?:Are|<)/igs)
	{
		$proposed_others_materials=$proposed_others_materials.'|'.$1;
	}
	if($proposed_others_materials=~m/^\s*$/is)
	{
		while($Materials_Content_For_Others=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|<)/igs)
		{
			$proposed_others_materials=$proposed_others_materials.'|'.$1;
		}
	}

	$proposed_others_materials=~s/<>/ /igs;			
	$proposed_others_materials=~s/\s+/ /igs;	
	$proposed_others_materials=~s/^\s+|\s+$//igs;			
	$proposed_others_materials=~s/^\s*\|+|\|+\s*$//igs;		

	print "Existing Other Materials	:: $existing_others_materials\n";
	print "Proposed Other Materials	:: $proposed_others_materials\n";
	
	#### Residential Units
	if($content=~m/\d+\.\s*Residential\s*Units\s*([\w\W]*?)\|Split\|/is)
	{		
		my $Residential_Units=$1;

		while($Residential_Units=~m/Housing\s*-\s*Proposed(?:<>)?([\w\W]*?)\s*Housing\s*-\s*Existing/igs)
		{
			my $proposed_houses_content=$1;
			if($proposed_houses_content=~m/Houses\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_houses=$proposed_houses.'|'.$1;
				$proposed_houses=&Oneapp_utility::Tag_Clean($proposed_houses);
			}
			if($proposed_houses_content=~m/Flats\/Maisonettes\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_flats_maisonettes=$proposed_flats_maisonettes.'|'.$1;
				$proposed_flats_maisonettes=&Oneapp_utility::Tag_Clean($proposed_flats_maisonettes);
			}
			if($proposed_houses_content=~m/Live\s*\-\s*Work\s*units\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_live_work_units=$proposed_live_work_units.'|'.$1;
				$proposed_live_work_units=&Oneapp_utility::Tag_Clean($proposed_live_work_units);
			}
			if($proposed_houses_content=~m/Cluster\s*flats\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_cluster_flats=$proposed_cluster_flats.'|'.$1;
				$proposed_cluster_flats=&Oneapp_utility::Tag_Clean($proposed_cluster_flats);
			}
			if($proposed_houses_content=~m/Sheltered\s*housing\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_sheltered_housing=$proposed_sheltered_housing.'|'.$1;
				$proposed_sheltered_housing=&Oneapp_utility::Tag_Clean($proposed_sheltered_housing);
			}
			if($proposed_houses_content=~m/Bedsit\s*\/\s*Studios\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_bedsit_studios=$proposed_bedsit_studios.'|'.$1;
				$proposed_bedsit_studios=&Oneapp_utility::Tag_Clean($proposed_bedsit_studios);
			}
			if($proposed_houses_content=~m/Unknown\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_unknown=$proposed_unknown.'|'.$1;
				$proposed_unknown=&Oneapp_utility::Tag_Clean($proposed_unknown);
			}
			if($proposed_houses_content=~m/Proposed\s*Market\s*Housing\s*Total\s*(?:<>\s*)?([\d]+)\s*/is)
			{
				$proposed_market_housing_total=$proposed_market_housing_total.'|'.$1;
				$proposed_market_housing_total=&Oneapp_utility::Tag_Clean($proposed_market_housing_total);
			}
		}
		while($Residential_Units=~m/Housing\s*-\s*Existing(?:<>)?([\w\W]*?)(?:Overall\s*Residential|$)/igs)
		{
			my $Existing_houses_content=$1;
			if($Existing_houses_content=~m/Houses\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_houses=$existing_houses.'|'.$1;
				$existing_houses=&Oneapp_utility::Tag_Clean($existing_houses);
			}
			if($Existing_houses_content=~m/Flats\/Maisonettes\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_flats_maisonettes=$existing_flats_maisonettes.'|'.$1;
				$existing_flats_maisonettes=&Oneapp_utility::Tag_Clean($existing_flats_maisonettes);
			}
			if($Existing_houses_content=~m/Live\s*\-\s*Work\s*units\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_live_work_units=$existing_live_work_units.'|'.$1;
				$existing_live_work_units=&Oneapp_utility::Tag_Clean($existing_live_work_units);
			}
			if($Existing_houses_content=~m/Cluster\s*flats\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_cluster_flats=$existing_cluster_flats.'|'.$1;
				$existing_cluster_flats=&Oneapp_utility::Tag_Clean($existing_cluster_flats);
			}
			if($Existing_houses_content=~m/Sheltered\s*housing\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_sheltered_housing=$existing_sheltered_housing.'|'.$1;
				$existing_sheltered_housing=&Oneapp_utility::Tag_Clean($existing_sheltered_housing);
			}
			if($Existing_houses_content=~m/Bedsit\s*\/\s*Studios\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_bedsit_studios=$existing_bedsit_studios.'|'.$1;
				$existing_bedsit_studios=&Oneapp_utility::Tag_Clean($existing_bedsit_studios);
			}
			if($Existing_houses_content=~m/Unknown\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_unknown=$existing_unknown.'|'.$1;
				$existing_unknown=&Oneapp_utility::Tag_Clean($existing_unknown);
			}
			if($Existing_houses_content=~m/Existing\s*Market\s*Housing\s*Total\s*(?:<>\s*)?([\d]+)\s*/is)
			{
				$existing_market_housing_total=$existing_market_housing_total.'|'.$1;
				$existing_market_housing_total=&Oneapp_utility::Tag_Clean($existing_market_housing_total);
			}
		}
		if($Residential_Units=~m/Overall\s*Residential[^<]*?(?:<>)?([\w\W]*?)$/is)
		{	
			my $Overall_Residential=$1;
			if($Overall_Residential=~m/Total\s*proposed\s*residential\s*units\s*(?:<>\s*)?([\d]+)\s*<>/is)
			{
				$total_proposed_residential_units=$1;
			}
			if($Overall_Residential=~m/Total\s*existing\s*residential\s*units\s*(?:<>\s*)?([\d]+)\s*<>/is)
			{
				$total_existing_residential_units=$1;
			}
		}
	}
	if($applicant_telephone=~m/[a-z]/is){$applicant_telephone='';}
	if($applicant_mobile=~m/[a-z]/is){$applicant_mobile='';}
	if($applicant_fax=~m/[a-z]/is){$applicant_fax='';}
	if($agent_telephone=~m/[a-z]/is){$agent_telephone='';}
	if($agent_mobile=~m/[a-z]/is){$agent_mobile='';}
	if($agent_fax=~m/[a-z]/is){$agent_fax='';}
	
	print "------------ Residential Units ------------\n";
	print "proposed_houses			:: $proposed_houses\n";
	print "proposed_flats_maisonettes	:: $proposed_flats_maisonettes\n";
	print "proposed_live_work_units	:: $proposed_live_work_units\n";
	print "proposed_cluster_flats		:: $proposed_cluster_flats\n";
	print "proposed_sheltered_housing	:: $proposed_sheltered_housing\n";
	print "proposed_bedsit_studios		:: $proposed_bedsit_studios\n";
	print "proposed_unknown		:: $proposed_unknown\n";
	print "proposed_market_housing_total	:: $proposed_market_housing_total\n";
	print "existing_houses			:: $existing_houses\n";
	print "existing_flats_maisonettes	:: $existing_flats_maisonettes\n";
	print "existing_live_work_units	:: $existing_live_work_units\n";
	print "existing_cluster_flats		:: $existing_cluster_flats\n";
	print "existing_sheltered_housing	:: $existing_sheltered_housing\n";
	print "existing_bedsit_studios		:: $existing_bedsit_studios\n";
	print "existing_unknown		:: $existing_unknown\n";
	print "existing_market_housing_total	:: $existing_market_housing_total\n";
	print "total_proposed_residential_units:: $total_proposed_residential_units\n";
	print "total_existing_residential_units:: $total_existing_residential_units\n";

	#### Non-residential Floorspace
	if($content=~m/\d+\.\s*[a-z\:\s]*?Non\s*-?\s*residential\s*([\w\W]*?)\|Split\|\s*/is)
	{	
		my $Non_Residential_Units=$1;
		if($councilcode==434)
		{
			open(FL,">>Floor_Data_434.html");
			print FL "\n $Format_ID =================\n";
			print FL $Non_Residential_Units;
			close FL;
		}	

		if($Non_Residential_Units=~m/Shops\s*Net\s*Tradable\s*Area\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_shops=$1;
			$non_residential_shops=&Oneapp_utility::Tag_Clean($non_residential_shops);
		}
		if($Non_Residential_Units=~m/Financial\s*and\s*professional\s*services\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_financial=$1;
			$non_residential_financial=&Oneapp_utility::Tag_Clean($non_residential_financial);
		}
		if($Non_Residential_Units=~m/Restaurants\s*and\s*cafes\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_restaurants=$1;
			$non_residential_restaurants=&Oneapp_utility::Tag_Clean($non_residential_restaurants);
		}
		if($Non_Residential_Units=~m/Drinking\s*establ?ishments\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_drinking=$1;
			$non_residential_drinking=&Oneapp_utility::Tag_Clean($non_residential_drinking);
		}
		if($Non_Residential_Units=~m/Hot\s*food\s*takeaways\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_food=$1;
			$non_residential_food=&Oneapp_utility::Tag_Clean($non_residential_food);
		}
		if($Non_Residential_Units=~m/Office\s*[\w\W]*?<>\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_office=$1;
			$non_residential_office=&Oneapp_utility::Tag_Clean($non_residential_office);
		}
		if($Non_Residential_Units=~m/Research\s*and\s*development\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_research=$1;
			$non_residential_research=&Oneapp_utility::Tag_Clean($non_residential_research);
		}
		if($Non_Residential_Units=~m/Light\s*industrial\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_light=$1;
			$non_residential_light=&Oneapp_utility::Tag_Clean($non_residential_light);
		}
		if($Non_Residential_Units=~m/General\s*industrial\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_general=$1;
			$non_residential_general=&Oneapp_utility::Tag_Clean($non_residential_general);
		}
		if($Non_Residential_Units=~m/Storage\s*or\s*distribution\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_storage=$1;
			$non_residential_storage=&Oneapp_utility::Tag_Clean($non_residential_storage);
		}
		if($Non_Residential_Units=~m/Hotels\s*and\s*halls\s*of\s*residence\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_hotels=$1;
			$non_residential_hotels=&Oneapp_utility::Tag_Clean($non_residential_hotels);
		}
		if($Non_Residential_Units=~m/Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_institutions=$1;
			$non_residential_institutions=&Oneapp_utility::Tag_Clean($non_residential_institutions);
		}
		$Non_Residential_Units=~s/Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)//igs;
		if($Non_Residential_Units=~m/residential\s*institutions\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_residential=$1;
			$non_residential_residential=&Oneapp_utility::Tag_Clean($non_residential_residential);
		}
		if($Non_Residential_Units=~m/Assembly\s*and\s*leisure\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_assembly=$1;
			$non_residential_assembly=&Oneapp_utility::Tag_Clean($non_residential_assembly);
		}
		if($Non_Residential_Units=~m/Please\s*Specify\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_other=$1;
			$non_residential_other=&Oneapp_utility::Tag_Clean($non_residential_other);
		}
		if($Non_Residential_Units=~m/Total\s*<>\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_total=$1;
			$non_residential_total=&Oneapp_utility::Tag_Clean($non_residential_total);
		}
	}	
	print "non_residential_shops		:: $non_residential_shops\n";
	print "non_residential_financial	:: $non_residential_financial\n";
	print "non_residential_restaurants	:: $non_residential_restaurants\n";
	print "non_residential_drinking	:: $non_residential_drinking\n";
	print "non_residential_food		:: $non_residential_food\n";
	print "non_residential_office		:: $non_residential_office\n";
	print "non_residential_research	:: $non_residential_research\n";
	print "non_residential_light		:: $non_residential_light\n";
	print "non_residential_general		:: $non_residential_general\n";
	print "non_residential_storage		:: $non_residential_storage\n";
	print "non_residential_hotels		:: $non_residential_hotels\n";
	print "non_residential_residential	:: $non_residential_residential\n";
	print "non_residential_institutions	:: $non_residential_institutions\n";
	print "non_residential_assembly	:: $non_residential_assembly\n";
	print "non_residential_other		:: $non_residential_other\n";
	print "non_residential_total		:: $non_residential_total\n";
	
	print "DB Insert Happens..\n";
	
	$applicant_telephone='' if($applicant_telephone=~m/[a-z]+/is);
	$applicant_mobile='' if($applicant_mobile=~m/[a-z]+/is);
	$applicant_fax='' if($applicant_fax=~m/[a-z]+/is);
	$agent_telephone='' if($agent_telephone=~m/[a-z]+/is);
	$agent_mobile='' if($agent_mobile=~m/[a-z]+/is);
	$agent_fax='' if($agent_fax=~m/[a-z]+/is);
	
	$Source =&Oneapp_utility::clean($Source);
	$Format_ID =&Oneapp_utility::clean($Format_ID);
	$Application_No =&Oneapp_utility::clean($Application_No);
	$councilcode =&Oneapp_utility::clean($councilcode);
	$Markup =&Oneapp_utility::clean($Markup);
	$site_house =&Oneapp_utility::clean($site_house);
	$site_suffix =&Oneapp_utility::clean($site_suffix);
	$site_house_name =&Oneapp_utility::clean($site_house_name);
	$site_street =&Oneapp_utility::clean($site_street);
	$site_city =&Oneapp_utility::clean($site_city);
	$Site_County =&Oneapp_utility::clean($Site_County);
	$site_country =&Oneapp_utility::clean($site_country);
	$site_postcode =&Oneapp_utility::clean($site_postcode);
	$easting =&Oneapp_utility::clean($easting);
	$northing =&Oneapp_utility::clean($northing);
	$applicant_title =&Oneapp_utility::clean($applicant_title);
	$applicant_first_name =&Oneapp_utility::clean($applicant_first_name);
	$applicant_surname =&Oneapp_utility::clean($applicant_surname);
	$applicant_company_name =&Oneapp_utility::clean($applicant_company_name);
	$applicant_street =&Oneapp_utility::clean($applicant_street);
	$applicant_town =&Oneapp_utility::clean($applicant_town);
	$applicant_county =&Oneapp_utility::clean($applicant_county);
	$applicant_country =&Oneapp_utility::clean($applicant_country);
	$applicant_postcode =&Oneapp_utility::clean($applicant_postcode);
	$applicant_telephone =&Oneapp_utility::clean($applicant_telephone);
	$applicant_mobile =&Oneapp_utility::clean($applicant_mobile);
	$applicant_fax =&Oneapp_utility::clean($applicant_fax);
	$applicant_email =&Oneapp_utility::clean($applicant_email);
	$agent_title =&Oneapp_utility::clean($agent_title);
	$agent_first_name =&Oneapp_utility::clean($agent_first_name);
	$agent_surname =&Oneapp_utility::clean($agent_surname);
	$agent_company_name =&Oneapp_utility::clean($agent_company_name);
	$agent_street =&Oneapp_utility::clean($agent_street);
	$agent_town =&Oneapp_utility::clean($agent_town);
	$agent_county =&Oneapp_utility::clean($agent_county);
	$agent_country =&Oneapp_utility::clean($agent_country);
	$agent_postcode =&Oneapp_utility::clean($agent_postcode);
	$agent_telephone =&Oneapp_utility::clean($agent_telephone);
	$agent_mobile =&Oneapp_utility::clean($agent_mobile);
	$agent_fax =&Oneapp_utility::clean($agent_fax);
	$agent_email =&Oneapp_utility::clean($agent_email);
	$oneapp_proposal =&Oneapp_utility::clean($oneapp_proposal);
	$existing_wall_material =&Oneapp_utility::clean($existing_wall_material);
	$proposed_wall_material =&Oneapp_utility::clean($proposed_wall_material);
	$existing_roof_material =&Oneapp_utility::clean($existing_roof_material);
	$proposed_roof_material =&Oneapp_utility::clean($proposed_roof_material);
	$existing_window_material =&Oneapp_utility::clean($existing_window_material);
	$proposed_window_material =&Oneapp_utility::clean($proposed_window_material);
	$existing_doors_material =&Oneapp_utility::clean($existing_doors_material);
	$proposed_doors_material =&Oneapp_utility::clean($proposed_doors_material);
	$existing_boundary_treatment_material =&Oneapp_utility::clean($existing_boundary_treatment_material);
	$proposed_boundary_treatment_material =&Oneapp_utility::clean($proposed_boundary_treatment_material);
	$existing_vehicle_access_material =&Oneapp_utility::clean($existing_vehicle_access_material);
	$proposed_vehicle_access_material =&Oneapp_utility::clean($proposed_vehicle_access_material);
	$existing_lightning_material =&Oneapp_utility::clean($existing_lightning_material);
	$proposed_lighning_material =&Oneapp_utility::clean($proposed_lighning_material);
	$existing_others_materials =&Oneapp_utility::clean($existing_others_materials);
	$proposed_others_materials =&Oneapp_utility::clean($proposed_others_materials);
	$material_additional_information =&Oneapp_utility::clean($material_additional_information);
	$material_additional_info_details =&Oneapp_utility::clean($material_additional_info_details);
	$vehicle_car =&Oneapp_utility::clean($vehicle_car);
	$vehicle_light_good =&Oneapp_utility::clean($vehicle_light_good);
	$vehicle_motorcycle =&Oneapp_utility::clean($vehicle_motorcycle);
	$vehicle_disability =&Oneapp_utility::clean($vehicle_disability);
	$vehicle_cycle =&Oneapp_utility::clean($vehicle_cycle);
	$vehicle_other =&Oneapp_utility::clean($vehicle_other);
	$vehicle_other_desc =&Oneapp_utility::clean($vehicle_other_desc);
	$existing_use =&Oneapp_utility::clean($existing_use);
	$existing_residential_house =&Oneapp_utility::clean($existing_residential_house);
	$proposed_houses =&Oneapp_utility::clean($proposed_houses);
	$proposed_flats_maisonettes =&Oneapp_utility::clean($proposed_flats_maisonettes);
	$proposed_live_work_units =&Oneapp_utility::clean($proposed_live_work_units);
	$proposed_cluster_flats =&Oneapp_utility::clean($proposed_cluster_flats);
	$proposed_sheltered_housing =&Oneapp_utility::clean($proposed_sheltered_housing);
	$proposed_bedsit_studios =&Oneapp_utility::clean($proposed_bedsit_studios);
	$proposed_unknown =&Oneapp_utility::clean($proposed_unknown);
	$proposed_market_housing_total =&Oneapp_utility::clean($proposed_market_housing_total);
	$existing_houses =&Oneapp_utility::clean($existing_houses);
	$existing_flats_maisonettes =&Oneapp_utility::clean($existing_flats_maisonettes);
	$existing_live_work_units =&Oneapp_utility::clean($existing_live_work_units);
	$existing_cluster_flats =&Oneapp_utility::clean($existing_cluster_flats);
	$existing_sheltered_housing =&Oneapp_utility::clean($existing_sheltered_housing);
	$existing_bedsit_studios =&Oneapp_utility::clean($existing_bedsit_studios);
	$existing_unknown =&Oneapp_utility::clean($existing_unknown);
	$existing_market_housing_total =&Oneapp_utility::clean($existing_market_housing_total);
	$total_proposed_residential_units =&Oneapp_utility::clean($total_proposed_residential_units);
	$total_existing_residential_units =&Oneapp_utility::clean($total_existing_residential_units);
	$non_residential_shops =&Oneapp_utility::clean($non_residential_shops);
	$non_residential_financial =&Oneapp_utility::clean($non_residential_financial);
	$non_residential_restaurants =&Oneapp_utility::clean($non_residential_restaurants);
	$non_residential_drinking =&Oneapp_utility::clean($non_residential_drinking);
	$non_residential_food =&Oneapp_utility::clean($non_residential_food);
	$non_residential_office =&Oneapp_utility::clean($non_residential_office);
	$non_residential_research =&Oneapp_utility::clean($non_residential_research);
	$non_residential_light =&Oneapp_utility::clean($non_residential_light);
	$non_residential_general =&Oneapp_utility::clean($non_residential_general);
	$non_residential_storage =&Oneapp_utility::clean($non_residential_storage);
	$non_residential_hotels =&Oneapp_utility::clean($non_residential_hotels);
	$non_residential_residential =&Oneapp_utility::clean($non_residential_residential);
	$non_residential_institutions =&Oneapp_utility::clean($non_residential_institutions);
	$non_residential_assembly =&Oneapp_utility::clean($non_residential_assembly);
	$non_residential_other =&Oneapp_utility::clean($non_residential_other);
	$non_residential_total =&Oneapp_utility::clean($non_residential_total);
	$existing_employee =&Oneapp_utility::clean($existing_employee);
	$proposed_employee =&Oneapp_utility::clean($proposed_employee);
	$site_area =&Oneapp_utility::clean($site_area);


	#cleaning for un wanted info	
	$existing_wall_material = '' if($existing_wall_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_wall_material = '' if($proposed_wall_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_roof_material = '' if($existing_roof_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_roof_material = '' if($proposed_roof_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_window_material = '' if($existing_window_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_window_material = '' if($proposed_window_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_doors_material = '' if($existing_doors_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_doors_material = '' if($proposed_doors_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_boundary_treatment_material = '' if($existing_boundary_treatment_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_boundary_treatment_material = '' if($proposed_boundary_treatment_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_vehicle_access_material = '' if($existing_vehicle_access_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_vehicle_access_material = '' if($proposed_vehicle_access_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_lightning_material = '' if($existing_lightning_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_lighning_material = '' if($proposed_lighning_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_others_materials = '' if($existing_others_materials=~m/Planning\s*Portal\s*Reference/is);
	$proposed_others_materials = '' if($proposed_others_materials=~m/Planning\s*Portal\s*Reference/is);

	$easting=~s/\s*\(\s*x\s*\)\s*//igs;
	$northing=~s/\s*\(\s*y\s*\)\s*//igs;
	
	$easting = '' if($easting!~m/^\d+$/is);
	$northing = '' if($northing!~m/^\d+$/is);

	$oneapp_proposal =~s/[^[:print:]]+//igs;
	my $SaveFormatPdf;
	if((length($Source) <= 255) && (length($Application_No) <= 100) && (length($Markup) <= 20) && (length($site_house) <= 255) && (length($site_suffix) <= 255) && (length($site_house_name) <= 255) && (length($site_street) <= 255) && (length($site_city) <= 255) && (length($Site_County) <= 50) && (length($site_country) <= 255) && (length($site_postcode) <= 255) && (length($easting) <= 20) && (length($northing) <= 20) && (length($applicant_title) <= 100) && (length($applicant_first_name) <= 255) && (length($applicant_surname) <= 255) && (length($applicant_company_name) <= 255) && (length($applicant_street) <= 255) && (length($applicant_town) <= 255) && (length($applicant_county) <= 255) && (length($applicant_country) <= 255) && (length($applicant_postcode) <= 255) && (length($applicant_telephone) <= 255) && (length($applicant_mobile) <= 255) && (length($applicant_fax) <= 255) && (length($applicant_email) <= 255) && (length($agent_title) <= 20) && (length($agent_first_name) <= 255) && (length($agent_surname) <= 255) && (length($agent_company_name) <= 255) && (length($agent_street) <= 255) && (length($agent_town) <= 255) && (length($agent_county) <= 255) && (length($agent_country) <= 255) && (length($agent_postcode) <= 40) && (length($agent_telephone) <= 255) && (length($agent_mobile) <= 40) && (length($agent_fax) <= 40) && (length($agent_email) <= 255) && (length($existing_wall_material) <= 1500) && (length($proposed_wall_material) <= 1500) && (length($existing_roof_material) <= 1500) && (length($proposed_roof_material) <= 1500) && (length($existing_window_material) <= 1500) && (length($proposed_window_material) <= 1500) && (length($existing_doors_material) <= 1500) && (length($proposed_doors_material) <= 1500) && (length($existing_boundary_treatment_material) <= 1500) && (length($proposed_boundary_treatment_material) <= 1500) && (length($existing_vehicle_access_material) <= 1500) && (length($proposed_vehicle_access_material) <= 1500) && (length($existing_lightning_material) <= 1500) && (length($proposed_lighning_material) <= 1500) && (length($existing_others_materials) <= 1500) && (length($proposed_others_materials) <= 1500) && (length($material_additional_information) <= 10) && (length($vehicle_car) <= 20) && (length($vehicle_light_good) <= 20) && (length($vehicle_motorcycle) <= 20) && (length($vehicle_disability) <= 20) && (length($vehicle_cycle) <= 20) && (length($vehicle_other) <= 20) && (length($vehicle_other_desc) <= 200) && (length($existing_use) <= 1000) && (length($existing_residential_house) <= 20) && (length($proposed_houses) <= 50) && (length($proposed_flats_maisonettes) <= 100) && (length($proposed_live_work_units) <= 20) && (length($proposed_cluster_flats) <= 20) && (length($proposed_sheltered_housing) <= 20) && (length($proposed_bedsit_studios) <= 20) && (length($proposed_unknown) <= 20) && (length($proposed_market_housing_total) <= 20) && (length($existing_houses) <= 20) && (length($existing_flats_maisonettes) <= 20) && (length($existing_live_work_units) <= 20) && (length($existing_cluster_flats) <= 20) && (length($existing_sheltered_housing) <= 20) && (length($existing_bedsit_studios) <= 20) && (length($existing_unknown) <= 20) && (length($existing_market_housing_total) <= 20) && (length($total_proposed_residential_units) <= 20) && (length($total_existing_residential_units) <= 20) && (length($non_residential_shops) <= 30) && (length($non_residential_financial) <= 30) && (length($non_residential_restaurants) <= 30) && (length($non_residential_drinking) <= 30) && (length($non_residential_food) <= 30) && (length($non_residential_office) <= 30) && (length($non_residential_research) <= 30) && (length($non_residential_light) <= 30) && (length($non_residential_general) <= 100) && (length($non_residential_storage) <= 30) && (length($non_residential_hotels) <= 30) && (length($non_residential_residential) <= 30) && (length($non_residential_institutions) <= 30) && (length($non_residential_assembly) <= 30) && (length($non_residential_other) <= 30) && (length($non_residential_total) <= 100) && (length($existing_employee) <= 20) && (length($proposed_employee) <= 20) && (length($site_area) <= 100))
	{
		if((length($applicant_title) != 0) || (length($applicant_first_name) != 0) || (length($applicant_surname) != 0) || (length($applicant_company_name) != 0) || (length($applicant_street) != 0) || (length($applicant_town) != 0) || (length($applicant_county) != 0) || (length($applicant_country) != 0) || (length($applicant_postcode) != 0) || (length($applicant_telephone) != 0) || (length($applicant_mobile) != 0) || (length($applicant_fax) != 0) || (length($applicant_email) != 0) || (length($agent_title) != 0) || (length($agent_first_name) != 0) || (length($agent_surname) != 0) || (length($agent_company_name) != 0) || (length($agent_street) != 0) || (length($agent_town) != 0) || (length($agent_county) != 0) || (length($agent_country) != 0) || (length($agent_postcode) != 0) || (length($agent_telephone) != 0) || (length($agent_mobile) != 0) || (length($agent_fax) != 0) || (length($agent_email) != 0) || (length($oneapp_proposal) != 0))
		{	
			$SaveFormatPdf="(\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\',\'$applicationFormat\'),";
		}	
	}
	else
	{
<<<<<<< HEAD:lib/OneAppScrapper/Scrape_Oneapp.pm
		$SaveFormatPdf="insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_Country,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area) values (\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\');";

=======
		$SaveFormatPdf="insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_Country,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area,application_Format) values (\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\',\'$applicationFormat\');";
		# open(ERR,">>Size_Error_Query.txt");
		# print ERR $SaveFormatPdf."\n";
		# close ERR;
>>>>>>> master:lib/Scrape_Oneapp.pm
		my $query = "insert into TBL_PERL_ERROR_LOG(council_code, format_id, message) values(\'$councilcode\',\'$Format_ID\',\'Truncation Error\')";
		&DB_OneApp::UpdateDB($dbh,$query);
	}
	my $query = "update FORMAT_PUBLIC_ACCESS set Application_Extracted = 'Y' where id = $Format_ID and council_code = $councilcode";
	&DB_OneApp::UpdateDB($dbh,$query);
	return ($SaveFormatPdf);
	create_xml_last:
}
<<<<<<< HEAD:lib/OneAppScrapper/Scrape_Oneapp.pm
=======


sub Scrape_pdf_format2
{
	my $dbh				= shift;
	my $pdf_name		= shift;
	my $Format_ID  		= shift;
	my $councilcode		= shift;
	my $Application_No	= shift;
	my $Source			= shift;
	my $Markup			= shift;
	my $PDF_Path		= shift;
	
	$PDF_Path=$PDF_Path.$Format_ID;
	print "PDF NAME IN Scraping:: $pdf_name\n";
	print "PDF PATH IN Scraping:: $PDF_Path\n";

	my $applicationFormat='F2';
	my $text_file_name=$pdf_name;
	my $xml_file_name=$pdf_name;
	my $xl_file_name=$pdf_name;
	my $html_file_name="PDF2HTML$councilcode.html";
	$text_file_name=~s/\.pdf\s*$/.txt/igs;
	$xl_file_name=~s/\.pdf\s*$/.xls/igs;
	$xml_file_name=~s/\.pdf\s*$/.xml/igs;

	$text_file_name=$PDF_Path.'/'.$text_file_name;
	$xl_file_name=$PDF_Path.'/'.$xl_file_name;
	$xml_file_name=$PDF_Path.'/'.$xml_file_name;
	$html_file_name=$PDF_Path.'/'.$html_file_name;

	if($pdf_name=~m/\.(?:png|jpg)\s*$/is) 
	{
		goto create_xml_last;
	}
	# 
	my $pdf_name_De=$PDF_Path.'/'.$pdf_name;
	eval{
	system("PERL","deillustrate.pl",$pdf_name_De,$pdf_name_De);
	};
	
	if($@)
	{
		print "$@\n";
		print "Error While Deillustrate\n";
	}
	if($pdf_name=~m/COVER[ING]+_?LETTER|\.jpg|\.png/is) 
	{
		goto create_xml_last;
	}
	print "$PDF_Path/$pdf_name\n";
	my $pdf = eval{CAM::PDF->new("$PDF_Path/$pdf_name")};
	if($@)
	{
		print "$@\n";
		print "Error While CAM::PDF Text Convertion\n";
	}	
	my $page;
	eval{$page = $pdf->numPages()};
	open my $fh, ">:encoding(utf-8)",  $text_file_name   or warn "could not open $text_file_name : $!\n";
	print $fh "";
	close $fh;
	for(my $i=1;$i<=$page;$i++)
	{
		my $pageone_tree;
		eval {
			local $SIG{ALRM} = sub { die "Alarm Timeout\n" };
			alarm 5;
			$pageone_tree = eval{$pdf->getPageContentTree($i)};
			alarm 0;
		};
		if($@){
			print "Alarm Timeout\n";
		}		
		open my $fh, ">>:encoding(utf-8)",  $text_file_name   or warn "could not open $text_file_name : $!\n";
		print $fh eval {CAM::PDF::PageText->render($pageone_tree)};
		close $fh;
		my $pdf_file_cont= eval{CAM::PDF::PageText->render($pageone_tree)};
	}
	
	my $pdf_name_For_HTML=$PDF_Path.'/'.$pdf_name;
	eval{
		system("pdftotext.exe -raw $pdf_name_For_HTML $html_file_name");
	};

	if($@)
	{
		print "$@\n";
		print "Error While pdftohtml HTML Convertion\n";
	}	

	# system("pdftotext $pdf_name $text_file_name");
	my $filesize = -s "$text_file_name";
	my $pdf_name_Size = -s "$PDF_Path/$pdf_name";
	if($filesize == 0 || $filesize eq '' )
	{
		$pdf_name=~s/\\/\//igs;
		my $PDF_Filename=(split('/',$pdf_name))[-1];
		
		print "newName::$PDF_Filename";

		my $PDF_Filename=substr $PDF_Filename, 0;

		rename "$PDF_Path/".$PDF_Filename, "$PDF_Path/".$PDF_Filename;

		my $pdf = eval{CAM::PDF->new("$PDF_Path/".$PDF_Filename)};
		for(my $i=1;$i<=$page;$i++)
		{
			my $pageone_tree;
			eval {
				local $SIG{ALRM} = sub { die "Alarm Timeout\n" };
				alarm 5;
				$pageone_tree = eval{$pdf->getPageContentTree($i)};
				alarm 0;
			};
			if($@){
				print "Alarm Timeout\n";
			}		
			open my $fh, ">>:encoding(utf-8)",  $text_file_name   or warn "could not open $text_file_name : $!\n";
			print $fh eval {CAM::PDF::PageText->render($pageone_tree)};
			close $fh;
			my $pdf_file_cont= eval{CAM::PDF::PageText->render($pageone_tree)};
		}
		rename  "$PDF_Path/".$PDF_Filename, "$PDF_Path/$PDF_Filename";
	}
	my $filesize = -s "$html_file_name";		#changes

	if($filesize == 0 || $filesize eq '' )
	{
		my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'I', Last_Visited_Date = getdate() where id = $Format_ID and council_code = $councilcode";
		&DB_OneApp_Test::UpdateDB($dbh, $query);
		goto create_xml_last;
	}
	else
	{
		my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'Y', Last_Visited_Date = getdate() where id = $Format_ID and council_code = $councilcode";
		&DB_OneApp_Test::UpdateDB($dbh, $query);
	}

	open IN,"<$html_file_name";		#changes
	my ($content);
	my $Prev_Title_Number=0;
	while(<IN>)
	{
		if($_=~m/^(?:\W)?(\d+)\.\s*[a-z]+/is)
		{
			my $Next_Title_Number=$1;
			$_=~s/\n/<>\n/igs;
			if($Next_Title_Number==$Prev_Title_Number+1)
			{
				$Prev_Title_Number=$Next_Title_Number;
				$content=$content."|Split|\n".$_;
			}
			else
			{
				$content=$content." ".$_;
			}	
		}
		else
		{
			$_=~s/\n/<>\n/igs;
			$content=$content." ".$_;
		}			
	}
	close IN;

	$content.="|Split|\n";
	$content=~s/(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)//igs; # to remove page continuation mark
	$content=~s/Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>//igs; # to remove page reference

	if($content=~m/A\s+p\s+p\s+l\s+i\s+c\s+a\s+n\s+t\s+N\s+a\s+m\s+e/is)
	{
		$content=~s/\s\s+/DoubleSpace/igs; # to remove page reference
		$content=~s/\s//igs; # to remove page reference
		$content=~s/DoubleSpace/ /igs; # to remove page reference
	}

	$content=~s/<>\s*(?:<>\s*)+/<>/igs;
	$content=~s/Planning Portal Reference\s*\:[^>]*?<>//igs;

	my $html_file_path=$PDF_Path;
	opendir(my $dhf, $html_file_path) || warn "can't opendir $html_file_path: $!";
	my @File_Names=readdir($dhf);
	closedir $dhf;
	my ($Html_Content);
	for(@File_Names)
	{
		if(($_=~m/PDF2HTML$councilcode/is) && ($_=~m/\.HTML/is))
		{
			open(re,"<$_");
			my $Prev_Title_Number=0;

			while(<re>)
			{
				# $Html_Content.=$_;
				if($_=~m/>(\d+)\.\s*[a-z]+/is)
				{
					my $Next_Title_Number=$1;
					if($Next_Title_Number==$Prev_Title_Number+1)
					{
						$Prev_Title_Number=$Next_Title_Number;
						$Html_Content=$Html_Content."|Split|\n".$_;
					}
					else
					{
						$Html_Content=$Html_Content." ".$_;
					}	
				}
				else
				{
					$Html_Content=$Html_Content." ".$_;
				}			
			}
			close re;			
		}	
	}	

	$Html_Content.="|Split|\n";
	$Html_Content=~s/(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)//igs; # to remove page continuation mark
	$Html_Content=~s/Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>//igs; # to remove page reference
	# open(MT,">Final_Html_Content.html");
	# print MT $content;
	# close MT;
	my $flag=0;
	my ($site_house,$site_suffix,$site_house_name,$site_street,$site_city,$Site_County,$site_country,$site_postcode,$easting,$northing,$applicant_title,$applicant_first_name,$applicant_surname,$applicant_company_name,$applicant_street,$applicant_town, $applicant_county, $applicant_postcode, $applicant_telephone, $applicant_mobile, $applicant_fax, $applicant_email,$agent, $agent_title, $agent_first_name, $agent_surname,$applicant_country, $agent_company_name, $agent_street, $agent_street, $agent_town, $agent_county, $agent_country, $agent_postcode, $agent_telephone, $agent_mobile, $agent_fax,$agent_email,$oneapp_proposal,$vehicle_car,$vehicle_light_good,$vehicle_motorcycle,$vehicle_disability,$vehicle_cycle,$vehicle_other,$vehicle_other_desc,$res, $existing_wall_material, $proposed_wall_material, $existing_roof_material, $proposed_roof_material, $existing_window_material, $proposed_window_material, $existing_doors_material, $proposed_doors_material, $existing_boundary_treatment_material, $proposed_boundary_treatment_material, $existing_vehicle_access_material, $proposed_vehicle_access_material, $existing_lightning_material, $proposed_lighning_material, $existing_others_materials, $proposed_others_materials, $material_additional_information, $material_additional_info_details, $existing_use, $existing_residential_house, $proposed_houses, $proposed_flats_maisonettes, $proposed_live_work_units, $proposed_cluster_flats, $proposed_sheltered_housing, $proposed_bedsit_studios, $proposed_unknown, $proposed_market_housing_total, $existing_houses, $existing_flats_maisonettes, $existing_live_work_units, $existing_cluster_flats, $existing_sheltered_housing, $existing_bedsit_studios, $existing_unknown, $existing_market_housing_total, $total_proposed_residential_units, $total_existing_residential_units, $non_residential_shops, $non_residential_financial, $non_residential_restaurants, $non_residential_drinking, $non_residential_food, $non_residential_office, $non_residential_research, $non_residential_light, $non_residential_general, $non_residential_storage, $non_residential_hotels, $non_residential_residential, $non_residential_institutions, $non_residential_assembly, $non_residential_other, $non_residential_total, $existing_employee, $proposed_employee, $site_area);
	if($content=~m/\d+\.\s*(?:site\s*address|Trees\s*Location)\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $site=$1;
		$flag=1;
		# $site=~s/\d+\.\s*(?:site\s*address|Trees\s*Location)//igs;
		# open(MT,">site_Replace.html");
		# print MT $site;
		# close MT;		
		print "------------ Site Details ------------\n";
		
		if($site=~m/Number\s*<>\s*([\w\W]*?)\s*Suffix/is)
		{
			$site_house	= trim($1);
			print "site_house		:: $site_house\n";
		}
		elsif($site=~m/Number\s*([^>]*?)\s*<>/is)
		{
			$site_house	= trim($1);
			print "site_house		:: $site_house\n";
		}

		if($site=~m/Suffix\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Property\s*name/is)
		{
			$site_suffix=trim($1);
		}

		if($site=~m/Property\s*name\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*1/is)
		{
			$site_house_name= trim($1);
		}
		elsif($site=~m/Property\s*name\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*1/is)
		{
			$site_house_name= trim($1);
		}

		if($site=~m/Address\s*line\s*1\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*2/is)
		{
			my $site_street1= trim($1);
			$site_street=$site_street1;
		}
		elsif($site=~m/Address\s*line\s*1\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*2/is)
		{
			my $site_street1= trim($1);
			$site_street=$site_street1;
		}
		
		if($site=~m/Address\s*line\s*2\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*3/is)
		{
			my $site_street2= trim($1);
			$site_street=$site_street.', '.$site_street2;
		}
		elsif($site=~m/Address\s*line\s*2\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*3/is)
		{
			my $site_street2= trim($1);
			$site_street=$site_street.', '.$site_street2;
		}

		if($site=~m/Address\s*line\s*3\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Town/is)
		{
			my $site_street3= trim($1);
			$site_street=$site_street.', '.$site_street3;
			$site_street=~s/(?:,\s*)+$//igs;
		}
		elsif($site=~m/Address\s*line\s*3\s*([^>]*?)(?:\s*<>)?\s*Town/is)
		{
			my $site_street3= trim($1);
			$site_street=$site_street.', '.$site_street3;
			$site_street=~s/(?:,\s*)+$//igs;
		}

		if($site=~m/Town\s*\/\s*city\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Postcode/is)
		{
			$site_city	= trim($1);
		}
		elsif($site=~m/Town\s*\/\s*city\s*([^>]*?)\s*(?:\s*<>)?Postcode/is)
		{
			$site_city	= trim($1);
		}
		elsif($site=~m/Town\s*\/\s*city\s*([^>]*?)\s*<>/is)
		{
			$site_city	= trim($1);
		}
		
		# if($site=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		# {
			# $Site_County	= trim($1);
			# $Site_County	=~ s/\:\s*//igs;
			# $Site_County	=~ s/\s+/ /igs;
		# }
		
		# if($site=~m/Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)</is)
		# {
			# $site_country=trim($1);
			# $site_country=~s/\s+/ /igs;
		# }
		
		if($site=~m/Postcode\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Description\s*of\s*site\s*location/is)
		{
			$site_postcode	= $1;
			$site_postcode	=~s/\W/ /igs;
			$site_postcode	=~s/\s\s+/ /igs;
		}
		elsif($site=~m/Postcode\s*([^>]*?)\s*(?:\s*<>)?Description\s*of\s*site\s*location/is)
		{
			$site_postcode	= $1;
			$site_postcode	=~s/\W/ /igs;
			$site_postcode	=~s/\s\s+/ /igs;
		}
		elsif($site=~m/Postcode\s*([^>]*?)\s*<>/is)
		{
			$site_postcode	= $1;
			$site_postcode	=~s/\W/ /igs;
			$site_postcode	=~s/\s\s+/ /igs;
		}

		if($site=~m/Easting\s*(?:\(x\))?\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Northing/is)
		{
			$easting	= trim($1);
			$easting	=~s/\s+/ /igs;
		}
		elsif($site=~m/Easting\s*\(x\)\s*([^>]*?)\s*(?:\s*<>)?Northing/is)
		{
			$easting	= trim($1);
			$easting	=~s/\s+/ /igs;
		}
		elsif($site=~m/Easting\s*(?:\(x\))?\s*([^>]*?)\s*<>/is)
		{
			$easting	= trim($1);
			$easting	=~s/\s+/ /igs;
		}
		
		if($site=~m/Northing\s*(?:\(y\))?\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Description/is)
		{
			$northing	= trim($1);
			$northing	=~ s/\s+/ /igs;
		}	
		elsif($site=~m/Northing\s*\(y\)\s*([^>]*?)\s*(?:\s*<>)?Description/is)
		{
			$northing	= trim($1);
			$northing	=~ s/\s+/ /igs;
		}	
		elsif($site=~m/Northing\s*(?:\(y\))?\s*([^>]*?)\s*<>/is)
		{
			$northing	= trim($1);
			$northing	=~ s/\s+/ /igs;
		}	
	}
	print "site_suffix		:: $site_suffix\n";
	print "site_house_name		:: $site_house_name\n";
	print "site_street		:: $site_street\n";
	print "site_city		:: $site_city\n";
	print "Site_County		:: $Site_County\n";
	print "site_country:: $site_country\n";
	print "site_postcode		:: $site_postcode\n";
	print "Northing		:: $northing\n";
	print "Easting			:: $easting\n";

	print "------------ Applicant Details ------------\n";
	my $applicant;
	my @Address_list;
	if($content=~m/\d+\.\s*Applicant\s*Details([\w\W]*?)\|Split\|\s*/is)
	{
		$applicant=$1;
		$applicant=~s/\d+\.\s*Applicant\s*Details//igs;
		# open(MT,">Applicant_Replace.html");
		# print MT $applicant;
		# close MT;	
		if($applicant=~m/Title\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*First\s*name\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Surname\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Company\s*name/is)
		{
			$applicant_title 		= trim($1);
			$applicant_first_name 	= trim($2);
			$applicant_surname 		= trim($3);
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		elsif($applicant=~m/Title\s*([^>]*?)(?:\s*<>)?Other(?:\s*<>)?\s*First\s*name\s*([^>]*?)(?:\s*<>)?\s*Surname\s*([^>]*?)(?:\s*<>)?Company\s*name/is)
		{
			$applicant_title 		= trim($1);
			$applicant_first_name 	= trim($2);
			$applicant_surname 		= trim($3);
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		elsif($applicant=~m/Title\s*([^>]*?)\s*<>\s*First\s*name\s*([^>]*?)\s*<>\s*Surname\s*([^>]*?)\s*<>\s*Company\s*name/is)
		{
			$applicant_title 		= trim($1);
			$applicant_first_name 	= trim($2);
			$applicant_surname 		= trim($3);
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		
		if($applicant=~m/Company\s*name\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*1/is)
		{
			$applicant_company_name = trim($1);
			print "applicant_company_name	:: $applicant_company_name\n";
		}
		elsif($applicant=~m/Company\s*name\s*([^>]*?)\s*<>/is)
		{
			$applicant_company_name = trim($1);
			print "applicant_company_name	:: $applicant_company_name\n";
		}
		
		if($applicant=~m/Address\s*line\s*1\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*2/is)
		{
			my $line1=trim($1);
			if($line1 ne '')
			{
				push(@Address_list, $line1);
			}	
		}
		elsif($applicant=~m/Address\s*line\s*1\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*2/is)
		{
			my $line1=trim($1);
			if($line1 ne '')
			{
				push(@Address_list, $line1);
			}	
		}
		
		if($applicant=~m/Address\s*line\s*2\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*3/is)
		{
			my $line2=trim($1);
			if($line2 ne '')
			{
				push(@Address_list, $line2);
			}	
		}
		elsif($applicant=~m/Address\s*line\s*2\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*3/is)
		{
			my $line2=trim($1);
			if($line2 ne '')
			{
				push(@Address_list, $line2);
			}	
		}
		
		if($applicant=~m/Address\s*line\s*3\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Town/is)
		{
			my $line3=trim($1);
			if($line3 ne '')
			{
				push(@Address_list, $line3);
			}	
		}
		elsif($applicant=~m/Address\s*line\s*3\s*([^>]*?)(?:\s*<>)?\s*Town/is)
		{
			my $line3=trim($1);
			if($line3 ne '')
			{
				push(@Address_list, $line3);
			}	
		}
		
		$applicant_street=join(', ',@Address_list);
		print "Applicant_street	:: $applicant_street\n";
		
		if($applicant=~m/Town\/city\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Country/is)
		{
			$applicant_town	= trim($1);
			print "applicant_town		:: $applicant_town\n";
		}
		elsif($applicant=~m/Town\s*\/\s*city\s*([^>]*?)\s*(?:\s*<>)?\s*Country/is)
		{
			$applicant_town	= trim($1);
			print "applicant_town		:: $applicant_town\n";
		}
		elsif($applicant=~m/Town\s*\/\s*city\s*([^>]*?)\s*<>/is)
		{
			$applicant_town	= trim($1);
			print "applicant_town		:: $applicant_town\n";
		}

		if($applicant=~m/Country\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*(?:Planning\s*Portal|2\.\s*Applicant\s*Details|Postcode)/is)
		{
			$applicant_country	=trim($1);
			print "applicant_country	:: $applicant_country\n";
		}
		elsif($applicant=~m/Country\s*([^>]*?)\s*(?:\s*<>)?\s*Postcode/is)
		{
			$applicant_country	=trim($1);
			print "applicant_country	:: $applicant_country\n";
		}
		elsif($applicant=~m/Country\s*([^>]*?)\s*<>/is)
		{
			$applicant_country	=trim($1);
			print "applicant_country	:: $applicant_country\n";
		}
		
		if($applicant=~m/Postcode\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Primary\s*number/is)
		{
			$applicant_postcode	= trim($1);
			print "applicant_postcode	:: $applicant_postcode\n";
		}
		elsif($applicant=~m/Postcode\s*([^>]*?)\s*(?:\s*<>)?\s*Primary\s*number/is)
		{
			$applicant_postcode	= trim($1);
			print "applicant_postcode	:: $applicant_postcode\n";
		}

		if($applicant_telephone eq '')
		{
			if($applicant =~ m/Primary\s*number\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Secondary\s*number/is)
			{
				$applicant_telephone	= trim($1);
			}
			elsif($applicant =~ m/Primary\s*number\s*(?:\s*<>)?\s*([^>]*?)\s*(?:\s*<>)?\s*Secondary\s*number/is)
			{
				$applicant_telephone	= trim($1);
			}
			elsif($applicant =~ m/Secondary\s*number\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Fax\s*number/is)
			{
				$applicant_telephone	= trim($1);
			}
		}
		
		if($applicant_fax eq '')
		{
			if($applicant =~ m/Fax\s*number\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Email\s*address/is)
			{
				$applicant_fax	= trim($1);
				print "applicant_fax		:: $applicant_fax\n";
			}
			elsif($applicant =~ m/Fax\s*number\s*(?:\s*<>)?\s*([^>]*?)\s*(?:\s*<>)?\s*Email\s*address/is)
			{
				$applicant_fax	= trim($1);
				print "applicant_fax		:: $applicant_fax\n";
			}
		}	

		if($applicant=~m/([\w\.\-]+\@[\w\-]+\.[a-z]{2,3}\.*[a-z]{0,2})/is)
		{
			$applicant_email	= trim($1);
			print "applicant_email		:: $applicant_email\n";
		}
	}

	print "applicant_mobile 	:: $applicant_mobile\n";
	print "applicant_fax 		:: $applicant_fax\n";
	

	print "------------ Agent Details ------------\n";
	my $agent;	
	my @Address_list;
	if($content=~m/\d+\.\s*Agent\s*Details\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		$agent=$1;
		# open(MT,">Agent_Replace.html");
		# print MT $agent;
		# close MT;	
		if($agent=~m/Title\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*First\s*name\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Surname\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Company\s*name/is)
		{
			$agent_title 		= trim($1);
			$agent_first_name 	= trim($2);
			$agent_surname 		= trim($3);
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		elsif($agent=~m/Title\s*([^>]*?)(?:\s*<>)?\s*First\s*name\s*([^>]*?)(?:\s*<>)?\s*Surname\s*([^>]*?)(?:\s*<>)?Company\s*name/is)
		{
			$agent_title 		= trim($1);
			$agent_first_name 	= trim($2);
			$agent_surname 		= trim($3);
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		elsif($agent=~m/Title\s*([^>]*?)\s*<>\s*First\s*name\s*([^>]*?)\s*<>\s*Surname\s*([^>]*?)\s*<>\s*Company\s*name/is)
		{
			$agent_title 		= trim($1);
			$agent_first_name 	= trim($2);
			$agent_surname 		= trim($3);
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		
		if($agent=~m/Company\s*name\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*1/is)
		{
			$agent_company_name = trim($1);
			print "agent_company_name 	:: $agent_company_name\n";
		}
		elsif($agent=~m/Company\s*name\s*(?:\s*<>)?\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*1/is)
		{
			$agent_company_name = trim($1);
			print "agent_company_name 	:: $agent_company_name\n";
		}
		elsif($agent=~m/Company\s*name\s*([^>]*?)\s*<>/is)
		{
			$agent_company_name = trim($1);
			print "agent_company_name	:: $agent_company_name\n";
		}

		if($agent=~m/Address\s*line\s*1\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*2/is)
		{
			my $line1=trim($1);
			if($line1 ne '')
			{
				push(@Address_list, $line1);
			}	
		}
		elsif($agent=~m/Address\s*line\s*1\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*2/is)
		{
			my $line1=trim($1);
			if($line1 ne '')
			{
				push(@Address_list, $line1);
			}	
		}
		
		if($agent=~m/Address\s*line\s*2\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Address\s*line\s*3/is)
		{
			my $line2=trim($1);
			if($line2 ne '')
			{
				push(@Address_list, $line2);
			}	
		}
		elsif($agent=~m/Address\s*line\s*2\s*([^>]*?)(?:\s*<>)?\s*Address\s*line\s*3/is)
		{
			my $line2=trim($1);
			if($line2 ne '')
			{
				push(@Address_list, $line2);
			}	
		}

		if($agent=~m/Address\s*line\s*3\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Town/is)
		{
			my $line3=trim($1);
			if($line3 ne '')
			{
				push(@Address_list, $line3);
			}	
		}
		elsif($agent=~m/Address\s*line\s*3\s*([^>]*?)(?:\s*<>)?\s*Town/is)
		{
			my $line3=trim($1);
			if($line3 ne '')
			{
				push(@Address_list, $line3);
			}	
		}
		$agent_street=join(', ',@Address_list);
		
		print "agent_street 		:: $agent_street\n";
		if($agent=~m/Town\/city\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Country/is)
		{
			$agent_town	= trim($1);
			print "agent_town 		:: $agent_town\n";
		}
		elsif($agent=~m/Town\s*\/\s*city\s*([^>]*?)\s*(?:\s*<>)?\s*Country/is)
		{
			$agent_town	= trim($1);
			print "agent_town 		:: $agent_town\n";
		}
		
		if($agent=~m/Country\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*(?:Planning\s*Portal|2\.\s*Agent\s*Details|Postcode)/is)
		{
			$agent_country	= trim($1);
			print "agent_country 		:: $agent_country\n";
		}
		elsif($agent=~m/Country\s*([^>]*?)\s*(?:\s*<>)?\s*Postcode/is)
		{
			$agent_country	= trim($1);
			print "agent_country 		:: $agent_country\n";
		}

		if($agent =~ m/Postcode\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Primary\s*number/is)
		{
			$agent_postcode	= trim($1);
			$agent_postcode	=~ s/\W/ /igs;
			print "agent_postcode 		:: $agent_postcode\n";
		}
		elsif($agent =~ m/Postcode\s*([^>]*?)\s*(?:\s*<>)?\s*Primary\s*number/is)
		{
			$agent_postcode	= trim($1);
			$agent_postcode	=~ s/\W/ /igs;
			print "agent_postcode 		:: $agent_postcode\n";
		}

		my $agent_telephone_Code='';
		if($agent_telephone eq '')
		{
			if($agent =~ m/Primary\s*number\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Secondary\s*number/is)
			{
				$agent_telephone	= trim($1);
			}
			elsif($agent =~ m/Primary\s*number\s*(?:\s*<>)?\s*([^>]*?)\s*(?:\s*<>)?\s*Secondary\s*number/is)
			{
				$agent_telephone	= trim($1);
			}
			elsif($agent =~ m/Secondary\s*number\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Fax\s*number/is)
			{
				$agent_telephone	= trim($1);
			}
		}	
		my $Fax_Flag=0;
		if($agent_fax eq '')
		{
			if($agent =~ m/Fax\s*number\s*<>\s*([\w\W]*?)(?:\s*<>)?\s*Email/is)
			{
				$agent_fax	= trim($1);
				$agent_fax	=~ s/\s+/ /igs;
			}
			elsif($agent =~ m/Fax\s*number\s*(?:\s*<>)?\s*([^>]*?)\s*(?:\s*<>)?\s*Email\s*address/is)
			{
				$agent_fax	= trim($1);
				$agent_fax	=~ s/\s+/ /igs;
			}
		}	

		if($agent=~m/([\w\.\-]+\@[\w\-]+\.[a-z]{2,3}\.*[a-z]{0,2})/is)
		{
			$agent_email	= trim($1);
			$agent_email	=~ s/\s+//igs;
		}
		
	}

	print "agent_telephone		:: $agent_telephone\n";
	print "agent_mobile 		:: $agent_mobile\n";
	print "agent_fax 		:: $agent_fax\n";
	print "agent_email 		:: $agent_email\n";
	
	##### SITE AREA ##########
	if($content=~m/\d+\.\s*Site\s*Area\s*([\w\W]*?)\|Split\|/is)
	{
		my $Site_Area_Content=$1;
		
		if($Site_Area_Content=~m/What\s*is\s*the\s*measurement[^>]*?<>\s*([\w\W]*?)(?:\s*<>)?\s*Unit\s*<>\s*([\w\W]*?)\s*<>/is)
		{
			$site_area=$1." ".$2;
			$site_area=trim($site_area);
		}	
		elsif($Site_Area_Content=~m/What\s*is\s*the\s*measurement[^>]*?<>\s*\(numeric\s*characters\s*only\)\s*\.\s*([\w\W]*?)(?:\s*<>)?\s*Unit\s*([\w\W]*?)\s*<>/is)
		{
			$site_area=$1." ".$2;
			$site_area=trim($site_area);
		}	
		print "Site_area 		:: $site_area\n";
	}	

	##### ONEAPP PROPOSAL ##########
	if($content=~m/\d+\.\s*Description\s*of\s*(?:the\s*)?(?:Proposal|Proposed)\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		$oneapp_proposal	=$1;
		$oneapp_proposal	=~s/Please\s*describe\s*details[^>]*?\:\s*<>//igs;
		$oneapp_proposal	=~s/[\W\w]*?Please\s*describe[^>]*?\:\s*<>//igs;
		$oneapp_proposal	=~s/[\W\w]*?including\s*any\s*change\s*of\s*use\s*(?:<>)?//igs;
		$oneapp_proposal	=~s/Has\s*the[\W\w]*?$//igs;
	}	

	$oneapp_proposal=&trim($oneapp_proposal);
	print "oneapp_proposal 	:: $oneapp_proposal\n";
	
	if($content=~m/\d+\.\s*Existing\s*Use\s*([\w\W]*?)\|Split\|/is)
	{
		my $Site_existing_use=$1;
		if($Site_existing_use=~m/describe[^>]*?current[^>]*?site\s*\<>\s*([\w\W]*?)Is\s*the\s*site\s*currently/is)
		{
			$existing_use=$1;
			$existing_use=~s/Planning\s*Portal\s*Reference[^>]*?<>//igs;
			$existing_use=~s/\d+\.\s*Existing\s*Use\s*<>\s*//igs;
			$existing_use=&trim($existing_use);
		}	
		print "Existing_use 		:: $existing_use\n";
	}	

	if($content=~m/\d+\.\s*Materials\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Material=$1;
		$Material=~s/\-\s*description\:/-description1-description/igs;
		my @material_type;
		my @materialKeywords=('walls','External Walls','Internal Walls','roof','Roof covering','window','doors','External Doors','boundary treatment','vehicle access','lightning','others','Chimney','Ceilings','Floors','Rainwater goods','Vehicle access and hard standing');

		$Material=~s/([a-z]+?\s*<>\s*Description[^>]*?\(\s*optional\s*\)\s*\:)/<M>$1/igs;
		$Material=~s/(>)\s+((?:(?:Internal|External)?\s*doors|Windows|(?:Internal|External)?\s*Walls|boundary\s*treatments(?:\s*\([^>]*?\))?|lightning|Lighting|Chimney|Ceilings|Floors|Rainwater\s*goods|Vehicle\s*access\s*and\s*hard\s*standing|others?[^>]*?|vehicle\s*access|Roof\s*(?:covering)?)\s*<>[^>]*?Description[^>]*?\s*\:\s*<>)/$1\n<M>$2/igs;
		$Material=~s/\(\s*e\.\s*g\s*\.[^>]*?\)//igs;
		$Material=~s/(>)(?:\W*)\s*((?:walls|External\s*Walls|Internal\s*Walls|roof|Roof\s*covering|windows?|doors|Internal\s*Doors|External\s*Doors|boundary\s*treatments?|vehicle\s*access|lightning|Lighting|others|others?\s*type\s*of\s*material[^>]*?|Chimney|Ceilings|Floors|Rainwater\s*goods|Vehicle\s*access\s*and\s*hard\s*standing)\s*<>[^>]*?Description[^>]*?\:)/$1\n<M>$2/igs;
		$Material=~s/[\d]+\s*\.\s*Materials\s*<>//igs;
		foreach my $materialKeyword(@materialKeywords)
		{
			$Material=~s/(?:(<M>$materialKeyword<>[\w\W]*?)<M>$materialKeyword<>)/$1/igs;
		}
		# open(MT,">Material_Replace.html");
		# print MT $Material;
		# close MT;
		
		while($Material=~m/([a-z]*?)\s*<>\s*Description[^>]*?\(\s*optional\s*\)\s*\:/igs)
		{
			my $type=$1;
			push(@material_type,$type);
		}
		my (@existing_wall_material,@proposed_wall_material);
		while($Material=~m/<M>(?:\s*Internal|External)?\s*Walls\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*(?:<>\s*|Description\s*of\s*proposed)/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*(?:<>\s*|Description\s*of\s*proposed)/igs)
				{
					push(@existing_wall_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_wall_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*(?:<>\s*|Description\s*of\s*proposed)/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*(?:<>\s*|Description\s*of\s*proposed)/igs)
				{
					push(@existing_wall_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_wall_material,&trim($1));
				}
			}	
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_wall_material,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_wall_material,&trim($1));
				}
			}	
		}
		$existing_wall_material=join('|',@existing_wall_material);
		$proposed_wall_material=join('|',@proposed_wall_material);
		print "Existing_wall_material 	:: $existing_wall_material\n";
		print "proposed_wall_material 	:: $proposed_wall_material\n";

		my (@existing_roof_material,@proposed_roof_material);
		while($Material=~m/<M>\s*Roof(?:\s*covering)?\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_roof_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_roof_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_roof_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_roof_material,&trim($1));
				}
			}	
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_roof_material,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_roof_material,&trim($1));
				}
			}	
		}
		$existing_roof_material=join('|',@existing_roof_material);
		$proposed_roof_material=join('|',@proposed_roof_material);
		print "existing_roof_material 	:: $existing_roof_material\n";
		print "proposed_roof_material 	:: $proposed_roof_material\n";

		my (@existing_window_material,@proposed_window_material);
		while($Material=~m/<M>\s*windows\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_window_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_window_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_window_material,&trim($1));
				}
			}	
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_window_material,&trim($1));
				}
			}
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_window_material,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_window_material,&trim($1));
				}
			}	
		}
		$existing_window_material=join('|',@existing_window_material);
		$proposed_window_material=join('|',@proposed_window_material);
		print "existing_window_material 	:: $existing_window_material\n";
		print "proposed_window_material 	:: $proposed_window_material\n";

		my (@existing_doors_material,@proposed_doors_material);
		while($Material=~m/<M>(?:\s*Internal|External)?\s*Doors\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_doors_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_doors_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_doors_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_doors_material,&trim($1));
				}
			}
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_doors_material,&trim($1));
				}
			}
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_doors_material,&trim($1));
				}
			}
			
		}
		$existing_doors_material=join('|',@existing_doors_material);
		$proposed_doors_material=join('|',@proposed_doors_material);
		print "existing_doors_material 	:: $existing_doors_material\n";
		print "proposed_doors_material 	:: $proposed_doors_material\n";

		my (@existing_vehicle_access_material,@proposed_vehicle_access_material);
		while($Material=~m/<M>\s*Vehicle\s*access\s*[^>]*?<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_vehicle_access_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_vehicle_access_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_vehicle_access_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_vehicle_access_material,&trim($1));
				}
			}
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_vehicle_access_material,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_vehicle_access_material,&trim($1));
				}
			}	
		}
		$existing_vehicle_access_material=join('|',@existing_vehicle_access_material);
		$proposed_vehicle_access_material=join('|',@proposed_vehicle_access_material);
		print "existing_vehicle_access_material 	:: $existing_vehicle_access_material\n";
		print "proposed_vehicle_access_material 	:: $proposed_vehicle_access_material\n";

		my (@existing_lightning_material,@proposed_lighning_material);
		while($Material=~m/<M>\s*(?:Lighting|lightning)\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_lightning_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_lightning_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_lightning_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_lightning_material,&trim($1));
				}
			}
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_lighning_material,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_lighning_material,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_lighning_material,&trim($1));
				}
			}	
		}
		$existing_lightning_material=join('|',@existing_lightning_material);
		$proposed_lighning_material=join('|',@proposed_lighning_material);
		print "existing_lightning_material 	:: $existing_lightning_material\n";
		print "proposed_lighning_material 	:: $proposed_lighning_material\n";

		my (@existing_boundary_treatment_material,@proposed_boundary_treatment_material);
		while($Material=~m/<M>\s*Boundary\s*treatment[^>]*?\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_boundary_treatment_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_boundary_treatment_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_boundary_treatment_material,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_boundary_treatment_material,&trim($1));
				}
			}
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_boundary_treatment_material,&trim($1));
				}
			}
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_boundary_treatment_material,&trim($1));
				}
			}
		}
		$existing_boundary_treatment_material=join('|',@existing_boundary_treatment_material);
		$proposed_boundary_treatment_material=join('|',@proposed_boundary_treatment_material);
		print "existing_boundary_treatment_material 	:: $existing_boundary_treatment_material\n";
		print "proposed_boundary_treatment_material 	:: $proposed_boundary_treatment_material\n";
		
		my (@existing_others_materials,@proposed_others_materials);
		# while($Material=~m/<M>\s*(?:other|Rainwater\s*good|Floor|Ceiling|Chimney)[^>]*?\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		while($Material=~m/<M>\s*others?[^>]*?\s*<>\s*([\w\W]*?)(?:<M>|Are\s*you\s*supplying)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*<>\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_others_materials,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*<>\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_others_materials,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*\(\s*optional\s*\)\s*\:\s*([^<]*?)\s*<>\s*Description\s*of\s*proposed/igs)
				{
					push(@existing_others_materials,&trim($1));
				}
			}
			elsif($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/is)
			{
				while($material_details=~m/existing\s+materials\s+and\s+finishes\s*(?:\(\s*optional\s*\))?\s*\:\s*([^<]*?)\s*<>\s*Please/igs)
				{
					push(@existing_others_materials,&trim($1));
				}
			}
			
			if($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*<>\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_others_materials,&trim($1));
				}
			}	
			elsif($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/is)
			{
				while($material_details=~m/proposed\s+materials\s+and\s+finishes\s*\:\s*([^<]*?)\s*<>/igs)
				{
					push(@proposed_others_materials,&trim($1));
				}
			}	
		}
		$existing_others_materials=join('|',@existing_others_materials);
		$proposed_others_materials=join('|',@proposed_others_materials);
		print "existing_others_materials 	:: $existing_others_materials\n";
		print "proposed_others_materials 	:: $proposed_others_materials\n";

	}
	print "------------ Vehicle Parking ------------\n";
	
	if($content=~m/\d+\.\s*(?:Vehicle)?\s*Parking\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $vehicle = $1;
		# open(MT,">vehicle_Replace.html");
		# print MT $vehicle;
		# close MT;
		if($vehicle=~m/Cars\s*([\-\d\s<>]+)/is)
		{
			$vehicle_car	= $1;
			$vehicle_car	=~ s/<>//igs;
			$vehicle_car	=~ s/\s+/|/igs;
			$vehicle_car	=~ s/\s*\|\s*$//igs;
			$vehicle_car	=~ s/^\||\|$//igs;
			print "vehicle_car 		:: $vehicle_car\n";
		}
		if($vehicle=~m/Light\s*goods\s*vehicles\/public\s*carrier\s*vehicles\s*([\-\d\s<>]+)/is)
		{
			$vehicle_light_good	= $1;
			$vehicle_light_good	=~ s/<>//igs;
			$vehicle_light_good	=~ s/\s+/|/igs;
			$vehicle_light_good	=~ s/\s*\|\s*$//igs;
			$vehicle_light_good	=~ s/^\||\|$//igs;
			print "vehicle_light_good 	:: $vehicle_light_good\n";
		}
		if($vehicle=~m/Motorcycles\s*([\-\d\s<>]+)/is)
		{
			$vehicle_motorcycle	= $1;
			$vehicle_motorcycle	=~ s/<>//igs;
			$vehicle_motorcycle	=~ s/\s+/|/igs;
			$vehicle_motorcycle	=~ s/\s*\|\s*$//igs;
			$vehicle_motorcycle	=~ s/^\||\|$//igs;
			print "vehicle_motorcycle 	:: $vehicle_motorcycle\n";
		}
		if($vehicle=~m/Disability\s*spaces\s*([\-\d\s<>]+)/is)
		{
			$vehicle_disability	= $1;
			$vehicle_disability	=~ s/<>//igs;
			$vehicle_disability	=~ s/\s+/|/igs;
			$vehicle_disability	=~ s/\s*\|\s*$//igs;
			$vehicle_disability	=~ s/^\||\|$//igs;
			print "vehicle_disability 	:: $vehicle_disability\n";
		}
		if($vehicle=~m/Cycle\s*spaces\s*([\-\d\s<>]+)/is)
		{
			$vehicle_cycle	= $1;
			$vehicle_cycle	=~ s/<>//igs;
			$vehicle_cycle	=~ s/\s+/|/igs;
			$vehicle_cycle	=~ s/\s*\|\s*$//igs;
			$vehicle_cycle	=~ s/^\||\|$//igs;
			print "vehicle_cycle 		:: $vehicle_cycle\n";
		}
		# print "applicant :; $applicant \n";
		if($vehicle =~ m/Other\s*\(e\.g\.\s*Bus\)\s*([\-\d\s<>]+)/is)
		{
			$vehicle_other	= $1;
			$vehicle_other	=~ s/<>//igs;
			$vehicle_other	=~ s/\s+/|/igs;
			$vehicle_other	=~ s/\s*\|\s*$//igs;
			$vehicle_other	=~ s/^\||\|$//igs;
			print "vehicle_other 		:: $vehicle_other\n";
		}
		if($vehicle =~ m/description\s*of\s*Other\s*(?:<>)?\s*([^<]+)\s*</is)
		{
			$vehicle_other_desc	= $1;
			$vehicle_other_desc	=~ s/\s+/ /igs;
			print "vehicle_other_desc 	:: $vehicle_other_desc\n";
		}
	}

	if($content=~m/\d+\.\s*[^\n]*?Non\-residential\s*Floorspace([\w\W]*?)\|Split\|\s*/is)
	{
		my $applicant=$1;
		# open(MT,">Non_Residential_Floorspace.html");
		# print MT $applicant;
		# close MT;		
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Shops\s*Net\s*Tradable\s*Area\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Financial\s*and\s*professional\s*services\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Restaurants\s*and\s*cafes\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\ )]+)\s*<>\s*Drinking\s*establ?ishments\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Hot\s*food\s*takeaways\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Office\s*\(other\s*than\s*A2\)\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Research\s*and\s*development\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Light\s*industrial\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*General\s*industrial\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Storage\s*or\s*distribution\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\ )]+)\s*<>\s*Hotels\s*and\s*halls\s*of\s*residence\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Residential\s*institutions\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Non\-residential\s*institutions\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Assembly\s*and\s*leisure\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Please\s*Specify\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/\d\s*<>\s*Total\s*([\d\.\s]+)/is)
		{
			my $value=$1;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
	}
	
	if($content=~m/\d+\.\s*Employment([\w\W]*?)\|Split\|\s*/is)
	{
		my $Employment_Details=$1;
		if($Employment_Details=~m/Existing\s*employees\s*([\d\.\s<>]+)/is)
		{
			$existing_employee=$1;
			$existing_employee=~s/<>//igs;
			$existing_employee=~s/\s+/|/igs;
			$existing_employee=~s/\s*\|\s*$//igs;
			$existing_employee=~s/^\||\|$//igs;
			print "Existing_employee 	:: $existing_employee\n";
		}
		if($Employment_Details=~m/Proposed\s*employees\s*([\d\.\s<>]+)/is)
		{
			$proposed_employee=$1;
			$proposed_employee=~s/<>//igs;
			$proposed_employee=~s/\s+/|/igs;
			$proposed_employee=~s/\s*\|\s*$//igs;
			$proposed_employee=~s/^\||\|$//igs;
			print "Proposed_employee 	:: $proposed_employee\n";
		}
	}

	#### Residential Units
	if($content=~m/\d+\.\s*Residential\s*\/\s*Dwelling\s*Units\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{		
		my $Residential_Units=$1;
		# open(MT,">Residential_Units.html");
		# print MT $Residential_Units;
		# close MT;
		while($Residential_Units=~m/[a-z]*?\s*:\s*Proposed\s*Housing\s*<>([\w\W]*?)(?:Add\s*\'[a-z\s]*?\'\s*residential\s*units|Total\s*proposed\s*residential\s*units)/igs)
		{
			my $proposed_houses_content=$1;
			if($proposed_houses_content=~m/Houses\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_houses=$proposed_houses.'|'.$1;
				$proposed_houses=&Tag_Clean($proposed_houses);
			}
			if($proposed_houses_content=~m/Flats\/Maisonettes\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_flats_maisonettes=$proposed_flats_maisonettes.'|'.$1;
				$proposed_flats_maisonettes=&Tag_Clean($proposed_flats_maisonettes);
			}
			elsif($proposed_houses_content=~m/Flats\/Maisonettes\s*([\d\.\s<>]+)<>\s*Total/is)
			{
				$proposed_flats_maisonettes=$proposed_flats_maisonettes.'|'.$1;
				$proposed_flats_maisonettes=~s/\s+/\|/igs;
				$proposed_flats_maisonettes=&Tag_Clean($proposed_flats_maisonettes);
			}
			if($proposed_houses_content=~m/Live\s*\-\s*Work\s*units\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_live_work_units=$proposed_live_work_units.'|'.$1;
				$proposed_live_work_units=&Tag_Clean($proposed_live_work_units);
			}
			if($proposed_houses_content=~m/Cluster\s*flats\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_cluster_flats=$proposed_cluster_flats.'|'.$1;
				$proposed_cluster_flats=&Tag_Clean($proposed_cluster_flats);
			}
			if($proposed_houses_content=~m/Sheltered\s*housing\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_sheltered_housing=$proposed_sheltered_housing.'|'.$1;
				$proposed_sheltered_housing=&Tag_Clean($proposed_sheltered_housing);
			}
			if($proposed_houses_content=~m/Bedsit\s*\/\s*Studios\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_bedsit_studios=$proposed_bedsit_studios.'|'.$1;
				$proposed_bedsit_studios=&Tag_Clean($proposed_bedsit_studios);
			}
			elsif($proposed_houses_content=~m/Bedsits?\s*\/\s*Studios\s*([\d\.\s<>]+)\s*<>/is)
			{
				$proposed_bedsit_studios=$proposed_bedsit_studios.'|'.$1;
				$proposed_bedsit_studios=&Tag_Clean($proposed_bedsit_studios);
				$proposed_bedsit_studios=~s/\s+/\|/igs;
			}
			if($proposed_houses_content=~m/Unknown\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$proposed_unknown=$proposed_unknown.'|'.$1;
				$proposed_unknown=&Tag_Clean($proposed_unknown);
			}
			if($proposed_houses_content=~m/Proposed\s*Market\s*Housing\s*Total\s*(?:<>\s*)?([\d]+)\s*/is)
			{
				$proposed_market_housing_total=$proposed_market_housing_total.'|'.$1;
				$proposed_market_housing_total=&Tag_Clean($proposed_market_housing_total);
			}
			elsif($proposed_houses_content=~m/Total\s*([\d\.\s<>]+)\s*<>/is)
			{
				$proposed_market_housing_total=$proposed_market_housing_total.'|'.$1;
				$proposed_market_housing_total=&Tag_Clean($proposed_market_housing_total);
				$proposed_market_housing_total=~s/\s+/\|/igs;
			}
		}
		
		while($Residential_Units=~m/[a-z]*?\s*:\s*existing\s*Housing\s*<>([\w\W]*?)(?:Add\s*\'[a-z\s]*?\'\s*residential\s*units|Total\s*proposed\s*residential\s*units)/igs)
		{
			my $Existing_houses_content=$1;
			if($Existing_houses_content=~m/Houses\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_houses=$existing_houses.'|'.$1;
				$existing_houses=&Tag_Clean($existing_houses);
			}
			if($Existing_houses_content=~m/Flats\/Maisonettes\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_flats_maisonettes=$existing_flats_maisonettes.'|'.$1;
				$existing_flats_maisonettes=&Tag_Clean($existing_flats_maisonettes);
				$existing_flats_maisonettes=~s/\s+/\|/igs;
			}
			elsif($Existing_houses_content=~m/Flats\/Maisonettes\s*([\d\.\s<>]+)\s*<>/is)
			{
				$existing_flats_maisonettes=$existing_flats_maisonettes.'|'.$1;
				$existing_flats_maisonettes=&Tag_Clean($existing_flats_maisonettes);
				$existing_flats_maisonettes=~s/\s+/\|/igs;
			}
			
			if($Existing_houses_content=~m/Live\s*\-\s*Work\s*units\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_live_work_units=$existing_live_work_units.'|'.$1;
				$existing_live_work_units=&Tag_Clean($existing_live_work_units);
			}
			if($Existing_houses_content=~m/Cluster\s*flats\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_cluster_flats=$existing_cluster_flats.'|'.$1;
				$existing_cluster_flats=&Tag_Clean($existing_cluster_flats);
			}
			if($Existing_houses_content=~m/Sheltered\s*housing\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_sheltered_housing=$existing_sheltered_housing.'|'.$1;
				$existing_sheltered_housing=&Tag_Clean($existing_sheltered_housing);
			}
			if($Existing_houses_content=~m/Bedsit\s*\/\s*Studios\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_bedsit_studios=$existing_bedsit_studios.'|'.$1;
				$existing_bedsit_studios=&Tag_Clean($existing_bedsit_studios);
			}
			if($Existing_houses_content=~m/Unknown\s*<>\s*([\d\.\s<>]+)<>\s*[\d]+\s*<>\s*Total\s*<>/is)
			{
				$existing_unknown=$existing_unknown.'|'.$1;
				$existing_unknown=&Tag_Clean($existing_unknown);
			}
			if($Existing_houses_content=~m/Existing\s*Market\s*Housing\s*Total\s*(?:<>\s*)?([\d]+)\s*/is)
			{
				$existing_market_housing_total=$existing_market_housing_total.'|'.$1;
				$existing_market_housing_total=&Tag_Clean($existing_market_housing_total);
			}
		}
		if($Residential_Units=~m/Total\s*proposed\s*residential\s*units\s*<>\s*([\d]+)\s*<>/is)
		{
			$total_proposed_residential_units=$1;
		}
		elsif($Residential_Units=~m/Total\s*proposed\s*residential\s*units\s*([\d]+)\s*<>/is)
		{
			$total_proposed_residential_units=$1;
		}
		
		if($Residential_Units=~m/Total\s*existing\s*residential\s*units\s*<>\s*([\d]+)\s*<>/is)
		{
			$total_existing_residential_units=$1;
		}
		elsif($Residential_Units=~m/Total\s*existing\s*residential\s*units\s*([\d]+)\s*<>/is)
		{
			$total_existing_residential_units=$1;
		}
	}
	if($applicant_telephone=~m/[a-z]/is){$applicant_telephone='';}
	if($applicant_mobile=~m/[a-z]/is){$applicant_mobile='';}
	if($applicant_fax=~m/[a-z]/is){$applicant_fax='';}
	if($agent_telephone=~m/[a-z]/is){$agent_telephone='';}
	if($agent_mobile=~m/[a-z]/is){$agent_mobile='';}
	if($agent_fax=~m/[a-z]/is){$agent_fax='';}
	
	print "------------ Residential Units ------------\n";
	print "proposed_houses			:: $proposed_houses\n";
	print "proposed_flats_maisonettes	:: $proposed_flats_maisonettes\n";
	print "proposed_live_work_units	:: $proposed_live_work_units\n";
	print "proposed_cluster_flats		:: $proposed_cluster_flats\n";
	print "proposed_sheltered_housing	:: $proposed_sheltered_housing\n";
	print "proposed_bedsit_studios		:: $proposed_bedsit_studios\n";
	print "proposed_unknown		:: $proposed_unknown\n";
	print "proposed_market_housing_total	:: $proposed_market_housing_total\n";
	print "existing_houses			:: $existing_houses\n";
	print "existing_flats_maisonettes	:: $existing_flats_maisonettes\n";
	print "existing_live_work_units	:: $existing_live_work_units\n";
	print "existing_cluster_flats		:: $existing_cluster_flats\n";
	print "existing_sheltered_housing	:: $existing_sheltered_housing\n";
	print "existing_bedsit_studios		:: $existing_bedsit_studios\n";
	print "existing_unknown		:: $existing_unknown\n";
	print "existing_market_housing_total	:: $existing_market_housing_total\n";
	print "total_proposed_residential_units:: $total_proposed_residential_units\n";
	print "total_existing_residential_units:: $total_existing_residential_units\n";

	#### Non-residential Floorspace
	if($content=~m/\d+\.\s*[a-z\:\s]*?Non\s*-?\s*residential\s*([\w\W]*?)\|Split\|\s*/is)
	{	
		my $Non_Residential_Units=$1;
		# open(MT,">Non_Residential_Units.html");
		# print MT $Non_Residential_Units;
		# close MT;
		if($Non_Residential_Units=~m/Shops\s*Net\s*Tradable\s*Area\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_shops=$1;
			$non_residential_shops=&Tag_Clean($non_residential_shops);
			$non_residential_shops=~s/\s+/\|/igs;
		}
		if($Non_Residential_Units=~m/Financial\s*and\s*professional\s*services\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_financial=$1;
			$non_residential_financial=&Tag_Clean($non_residential_financial);
		}
		if($Non_Residential_Units=~m/Restaurants\s*and\s*cafes\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_restaurants=$1;
			$non_residential_restaurants=&Tag_Clean($non_residential_restaurants);
			$non_residential_restaurants=~s/\s+/\|/igs;
		}
		if($Non_Residential_Units=~m/Drinking\s*establ?ishments\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_drinking=$1;
			$non_residential_drinking=&Tag_Clean($non_residential_drinking);
			$non_residential_drinking=~s/\s+/\|/igs;
		}
		if($Non_Residential_Units=~m/Hot\s*food\s*takeaways\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_food=$1;
			$non_residential_food=&Tag_Clean($non_residential_food);
			$non_residential_food=~s/\s+/\|/igs;
		}
		if($Non_Residential_Units=~m/Office\s*(?:[\w\s\(\)]*?)?\s*([\-\d\s\.\,<>]+)<>/is)
		{
			$non_residential_office=$1;
			$non_residential_office=&Tag_Clean($non_residential_office);
			$non_residential_office=~s/\s+/\|/igs;
		}
		elsif($Non_Residential_Units=~m/Office\s*[\w\W]*?<>\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_office=$1;
			$non_residential_office=&Tag_Clean($non_residential_office);
		}
		if($Non_Residential_Units=~m/Research\s*and\s*development\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_research=$1;
			$non_residential_research=&Tag_Clean($non_residential_research);
		}
		if($Non_Residential_Units=~m/Light\s*industrial\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_light=$1;
			$non_residential_light=&Tag_Clean($non_residential_light);
		}
		if($Non_Residential_Units=~m/General\s*industrial\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_general=$1;
			$non_residential_general=&Tag_Clean($non_residential_general);
			$non_residential_general=~s/\s+/\|/igs;
		}
		if($Non_Residential_Units=~m/Storage\s*or\s*distribution\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_storage=$1;
			$non_residential_storage=&Tag_Clean($non_residential_storage);
		}
		if($Non_Residential_Units=~m/Hotels\s*and\s*halls\s*of\s*residence\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_hotels=$1;
			$non_residential_hotels=&Tag_Clean($non_residential_hotels);
		}
		if($Non_Residential_Units=~m/Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_institutions=$1;
			$non_residential_institutions=&Tag_Clean($non_residential_institutions);
			$non_residential_institutions=~s/\s+/\|/igs;
		}
		$Non_Residential_Units=~s/Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)//igs;
		if($Non_Residential_Units=~m/residential\s*institutions\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_residential=$1;
			$non_residential_residential=&Tag_Clean($non_residential_residential);
		}
		if($Non_Residential_Units=~m/Assembly\s*and\s*leisure\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_assembly=$1;
			$non_residential_assembly=&Tag_Clean($non_residential_assembly);
			$non_residential_assembly=~s/\s+/\|/igs;
		}
		if($Non_Residential_Units=~m/Please\s*Specify\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_other=$1;
			$non_residential_other=&Tag_Clean($non_residential_other);
		}
		elsif($Non_Residential_Units=~m/other\s*(<>[\-\d\s\.\,<>]+)/is)
		{
			$non_residential_other=$1;
			$non_residential_other=&Tag_Clean($non_residential_other);
		}
		elsif($Non_Residential_Units=~m/other\s*([\-\d\s\.]+)\s*<>/is)
		{
			$non_residential_other=$1;
			$non_residential_other=&Tag_Clean($non_residential_other);
			$non_residential_other=~s/\s+/\|/igs;
		}
		
		if($Non_Residential_Units=~m/Total\s*<>\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_total=$1;
			$non_residential_total=&Tag_Clean($non_residential_total);
		}
		elsif($Non_Residential_Units=~m/Total\s*([\-\d\s\.\,<>]+)\s*<>/is)
		{
			$non_residential_total=$1;
			$non_residential_total=&Tag_Clean($non_residential_total);
			$non_residential_total=~s/\s+/\|/igs;
		}
	}	
	print "non_residential_shops		:: $non_residential_shops\n";
	print "non_residential_financial	:: $non_residential_financial\n";
	print "non_residential_restaurants	:: $non_residential_restaurants\n";
	print "non_residential_drinking	:: $non_residential_drinking\n";
	print "non_residential_food		:: $non_residential_food\n";
	print "non_residential_office		:: $non_residential_office\n";
	print "non_residential_research	:: $non_residential_research\n";
	print "non_residential_light		:: $non_residential_light\n";
	print "non_residential_general		:: $non_residential_general\n";
	print "non_residential_storage		:: $non_residential_storage\n";
	print "non_residential_hotels		:: $non_residential_hotels\n";
	print "non_residential_residential	:: $non_residential_residential\n";
	print "non_residential_institutions	:: $non_residential_institutions\n";
	print "non_residential_assembly	:: $non_residential_assembly\n";
	print "non_residential_other		:: $non_residential_other\n";
	print "non_residential_total		:: $non_residential_total\n";

	print "DB Insert Happens..\n";
	$applicant_telephone='' if($applicant_telephone=~m/[a-z]+/is);
	$applicant_mobile='' if($applicant_mobile=~m/[a-z]+/is);
	$applicant_fax='' if($applicant_fax=~m/[a-z]+/is);
	$agent_telephone='' if($agent_telephone=~m/[a-z]+/is);
	$agent_mobile='' if($agent_mobile=~m/[a-z]+/is);
	$agent_fax='' if($agent_fax=~m/[a-z]+/is);
	
	$Source =&clean11($Source);
	$Format_ID =&clean11($Format_ID);
	$Application_No =&clean11($Application_No);
	$councilcode =&clean11($councilcode);
	$Markup =&clean11($Markup);
	$site_house =&clean11($site_house);
	$site_suffix =&clean11($site_suffix);
	$site_house_name =&clean11($site_house_name);
	$site_street =&clean11($site_street);
	$site_city =&clean11($site_city);
	$Site_County =&clean11($Site_County);
	$site_country =&clean11($site_country);
	$site_postcode =&clean11($site_postcode);
	$easting =&clean11($easting);
	$northing =&clean11($northing);
	$applicant_title =&clean11($applicant_title);
	$applicant_first_name =&clean11($applicant_first_name);
	$applicant_surname =&clean11($applicant_surname);
	$applicant_company_name =&clean11($applicant_company_name);
	$applicant_street =&clean11($applicant_street);
	$applicant_town =&clean11($applicant_town);
	$applicant_county =&clean11($applicant_county);
	$applicant_country =&clean11($applicant_country);
	$applicant_postcode =&clean11($applicant_postcode);
	$applicant_telephone =&clean11($applicant_telephone);
	$applicant_mobile =&clean11($applicant_mobile);
	$applicant_fax =&clean11($applicant_fax);
	$applicant_email =&clean11($applicant_email);
	$agent_title =&clean11($agent_title);
	$agent_first_name =&clean11($agent_first_name);
	$agent_surname =&clean11($agent_surname);
	$agent_company_name =&clean11($agent_company_name);
	$agent_street =&clean11($agent_street);
	$agent_town =&clean11($agent_town);
	$agent_county =&clean11($agent_county);
	$agent_country =&clean11($agent_country);
	$agent_postcode =&clean11($agent_postcode);
	$agent_telephone =&clean11($agent_telephone);
	$agent_mobile =&clean11($agent_mobile);
	$agent_fax =&clean11($agent_fax);
	$agent_email =&clean11($agent_email);
	$oneapp_proposal =&clean11($oneapp_proposal);
	$existing_wall_material =&clean11($existing_wall_material);
	$proposed_wall_material =&clean11($proposed_wall_material);
	$existing_roof_material =&clean11($existing_roof_material);
	$proposed_roof_material =&clean11($proposed_roof_material);
	$existing_window_material =&clean11($existing_window_material);
	$proposed_window_material =&clean11($proposed_window_material);
	$existing_doors_material =&clean11($existing_doors_material);
	$proposed_doors_material =&clean11($proposed_doors_material);
	$existing_boundary_treatment_material =&clean11($existing_boundary_treatment_material);
	$proposed_boundary_treatment_material =&clean11($proposed_boundary_treatment_material);
	$existing_vehicle_access_material =&clean11($existing_vehicle_access_material);
	$proposed_vehicle_access_material =&clean11($proposed_vehicle_access_material);
	$existing_lightning_material =&clean11($existing_lightning_material);
	$proposed_lighning_material =&clean11($proposed_lighning_material);
	$existing_others_materials =&clean11($existing_others_materials);
	$proposed_others_materials =&clean11($proposed_others_materials);
	$material_additional_information =&clean11($material_additional_information);
	$material_additional_info_details =&clean11($material_additional_info_details);
	$vehicle_car =&clean11($vehicle_car);
	$vehicle_light_good =&clean11($vehicle_light_good);
	$vehicle_motorcycle =&clean11($vehicle_motorcycle);
	$vehicle_disability =&clean11($vehicle_disability);
	$vehicle_cycle =&clean11($vehicle_cycle);
	$vehicle_other =&clean11($vehicle_other);
	$vehicle_other_desc =&clean11($vehicle_other_desc);
	$existing_use =&clean11($existing_use);
	$existing_residential_house =&clean11($existing_residential_house);
	$proposed_houses =&clean11($proposed_houses);
	$proposed_flats_maisonettes =&clean11($proposed_flats_maisonettes);
	$proposed_live_work_units =&clean11($proposed_live_work_units);
	$proposed_cluster_flats =&clean11($proposed_cluster_flats);
	$proposed_sheltered_housing =&clean11($proposed_sheltered_housing);
	$proposed_bedsit_studios =&clean11($proposed_bedsit_studios);
	$proposed_unknown =&clean11($proposed_unknown);
	$proposed_market_housing_total =&clean11($proposed_market_housing_total);
	$existing_houses =&clean11($existing_houses);
	$existing_flats_maisonettes =&clean11($existing_flats_maisonettes);
	$existing_live_work_units =&clean11($existing_live_work_units);
	$existing_cluster_flats =&clean11($existing_cluster_flats);
	$existing_sheltered_housing =&clean11($existing_sheltered_housing);
	$existing_bedsit_studios =&clean11($existing_bedsit_studios);
	$existing_unknown =&clean11($existing_unknown);
	$existing_market_housing_total =&clean11($existing_market_housing_total);
	$total_proposed_residential_units =&clean11($total_proposed_residential_units);
	$total_existing_residential_units =&clean11($total_existing_residential_units);
	$non_residential_shops =&clean11($non_residential_shops);
	$non_residential_financial =&clean11($non_residential_financial);
	$non_residential_restaurants =&clean11($non_residential_restaurants);
	$non_residential_drinking =&clean11($non_residential_drinking);
	$non_residential_food =&clean11($non_residential_food);
	$non_residential_office =&clean11($non_residential_office);
	$non_residential_research =&clean11($non_residential_research);
	$non_residential_light =&clean11($non_residential_light);
	$non_residential_general =&clean11($non_residential_general);
	$non_residential_storage =&clean11($non_residential_storage);
	$non_residential_hotels =&clean11($non_residential_hotels);
	$non_residential_residential =&clean11($non_residential_residential);
	$non_residential_institutions =&clean11($non_residential_institutions);
	$non_residential_assembly =&clean11($non_residential_assembly);
	$non_residential_other =&clean11($non_residential_other);
	$non_residential_total =&clean11($non_residential_total);
	$existing_employee =&clean11($existing_employee);
	$proposed_employee =&clean11($proposed_employee);
	$site_area =&clean11($site_area);


	#cleaning for un wanted info	
	$existing_wall_material = '' if($existing_wall_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_wall_material = '' if($proposed_wall_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_roof_material = '' if($existing_roof_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_roof_material = '' if($proposed_roof_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_window_material = '' if($existing_window_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_window_material = '' if($proposed_window_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_doors_material = '' if($existing_doors_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_doors_material = '' if($proposed_doors_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_boundary_treatment_material = '' if($existing_boundary_treatment_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_boundary_treatment_material = '' if($proposed_boundary_treatment_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_vehicle_access_material = '' if($existing_vehicle_access_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_vehicle_access_material = '' if($proposed_vehicle_access_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_lightning_material = '' if($existing_lightning_material=~m/Planning\s*Portal\s*Reference/is);
	$proposed_lighning_material = '' if($proposed_lighning_material=~m/Planning\s*Portal\s*Reference/is);
	$existing_others_materials = '' if($existing_others_materials=~m/Planning\s*Portal\s*Reference/is);
	$proposed_others_materials = '' if($proposed_others_materials=~m/Planning\s*Portal\s*Reference/is);

	$easting=~s/\s*\(\s*x\s*\)\s*//igs;
	$northing=~s/\s*\(\s*y\s*\)\s*//igs;
	
	$easting = '' if($easting!~m/^\d+$/is);
	$northing = '' if($northing!~m/^\d+$/is);

	my $SaveFormatPdf;
	if((length($Source) <= 255) && (length($Application_No) <= 100) && (length($Markup) <= 20) && (length($site_house) <= 255) && (length($site_suffix) <= 255) && (length($site_house_name) <= 255) && (length($site_street) <= 255) && (length($site_city) <= 255) && (length($Site_County) <= 50) && (length($site_country) <= 255) && (length($site_postcode) <= 255) && (length($easting) <= 20) && (length($northing) <= 20) && (length($applicant_title) <= 100) && (length($applicant_first_name) <= 255) && (length($applicant_surname) <= 255) && (length($applicant_company_name) <= 255) && (length($applicant_street) <= 255) && (length($applicant_town) <= 255) && (length($applicant_county) <= 255) && (length($applicant_country) <= 255) && (length($applicant_postcode) <= 255) && (length($applicant_telephone) <= 255) && (length($applicant_mobile) <= 255) && (length($applicant_fax) <= 255) && (length($applicant_email) <= 255) && (length($agent_title) <= 20) && (length($agent_first_name) <= 255) && (length($agent_surname) <= 255) && (length($agent_company_name) <= 255) && (length($agent_street) <= 255) && (length($agent_town) <= 255) && (length($agent_county) <= 255) && (length($agent_country) <= 255) && (length($agent_postcode) <= 40) && (length($agent_telephone) <= 255) && (length($agent_mobile) <= 40) && (length($agent_fax) <= 40) && (length($agent_email) <= 255) && (length($existing_wall_material) <= 1500) && (length($proposed_wall_material) <= 1500) && (length($existing_roof_material) <= 1500) && (length($proposed_roof_material) <= 1500) && (length($existing_window_material) <= 1500) && (length($proposed_window_material) <= 1500) && (length($existing_doors_material) <= 1500) && (length($proposed_doors_material) <= 1500) && (length($existing_boundary_treatment_material) <= 1500) && (length($proposed_boundary_treatment_material) <= 1500) && (length($existing_vehicle_access_material) <= 1500) && (length($proposed_vehicle_access_material) <= 1500) && (length($existing_lightning_material) <= 1500) && (length($proposed_lighning_material) <= 1500) && (length($existing_others_materials) <= 1500) && (length($proposed_others_materials) <= 1500) && (length($material_additional_information) <= 10) && (length($vehicle_car) <= 20) && (length($vehicle_light_good) <= 20) && (length($vehicle_motorcycle) <= 20) && (length($vehicle_disability) <= 20) && (length($vehicle_cycle) <= 20) && (length($vehicle_other) <= 20) && (length($vehicle_other_desc) <= 200) && (length($existing_use) <= 1000) && (length($existing_residential_house) <= 20) && (length($proposed_houses) <= 50) && (length($proposed_flats_maisonettes) <= 100) && (length($proposed_live_work_units) <= 20) && (length($proposed_cluster_flats) <= 20) && (length($proposed_sheltered_housing) <= 20) && (length($proposed_bedsit_studios) <= 20) && (length($proposed_unknown) <= 20) && (length($proposed_market_housing_total) <= 20) && (length($existing_houses) <= 20) && (length($existing_flats_maisonettes) <= 20) && (length($existing_live_work_units) <= 20) && (length($existing_cluster_flats) <= 20) && (length($existing_sheltered_housing) <= 20) && (length($existing_bedsit_studios) <= 20) && (length($existing_unknown) <= 20) && (length($existing_market_housing_total) <= 20) && (length($total_proposed_residential_units) <= 20) && (length($total_existing_residential_units) <= 20) && (length($non_residential_shops) <= 30) && (length($non_residential_financial) <= 30) && (length($non_residential_restaurants) <= 30) && (length($non_residential_drinking) <= 30) && (length($non_residential_food) <= 30) && (length($non_residential_office) <= 30) && (length($non_residential_research) <= 30) && (length($non_residential_light) <= 30) && (length($non_residential_general) <= 100) && (length($non_residential_storage) <= 30) && (length($non_residential_hotels) <= 30) && (length($non_residential_residential) <= 30) && (length($non_residential_institutions) <= 30) && (length($non_residential_assembly) <= 30) && (length($non_residential_other) <= 30) && (length($non_residential_total) <= 100) && (length($existing_employee) <= 20) && (length($proposed_employee) <= 20) && (length($site_area) <= 100))
	{
		if((length($applicant_title) != 0) || (length($applicant_first_name) != 0) || (length($applicant_surname) != 0) || (length($applicant_company_name) != 0) || (length($applicant_street) != 0) || (length($applicant_town) != 0) || (length($applicant_county) != 0) || (length($applicant_country) != 0) || (length($applicant_postcode) != 0) || (length($applicant_telephone) != 0) || (length($applicant_mobile) != 0) || (length($applicant_fax) != 0) || (length($applicant_email) != 0) || (length($agent_title) != 0) || (length($agent_first_name) != 0) || (length($agent_surname) != 0) || (length($agent_company_name) != 0) || (length($agent_street) != 0) || (length($agent_town) != 0) || (length($agent_county) != 0) || (length($agent_country) != 0) || (length($agent_postcode) != 0) || (length($agent_telephone) != 0) || (length($agent_mobile) != 0) || (length($agent_fax) != 0) || (length($agent_email) != 0) || (length($oneapp_proposal) != 0))
		{	
			$SaveFormatPdf="(\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\',\'$applicationFormat\'),";
		}	
	}
	else
	{
		$SaveFormatPdf="insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_Country,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area,application_Format) values (\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\',\'$applicationFormat\');";

		my $query = "insert into TBL_PERL_ERROR_LOG(council_code, format_id, message) values(\'$councilcode\',\'$Format_ID\',\'Truncation Error\')";
		&DB_OneApp_Test::UpdateDB($dbh,$query);
	}
	my $query = "update FORMAT_PUBLIC_ACCESS set Application_Extracted = 'Y' where id = $Format_ID and council_code = $councilcode";
	&DB_OneApp_Test::UpdateDB($dbh,$query);
	return ($SaveFormatPdf);
	create_xml_last:
}

sub trim
{
	my $txt = shift;
	
	$txt =~ s/<>/ /g;
	$txt =~ s/\s\s+/ /g;
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
sub clean11()
{
	my $value1=shift;
	$value1=~s/(\s*<>\s*)+//igs;
	$value1=~s/^\s*|\s*$//igs;
	$value1=~s/\s+/ /igs;
	$value1=~s/\'/''/igs;
	return($value1);
}
sub Tag_Clean
{
	my $value=shift;
	$value=~s/\s*<>\s*/\|/igs;
	$value=~s/\s*\|\s*$//igs;
	$value=~s/^\|+|\|+$//igs;
	return($value);
}
>>>>>>> master:lib/Scrape_Oneapp.pm
