package Oneapp_utility;
use strict;

sub trim
{
	my $txt = shift;
	
	$txt =~ s/<>/ /g;
	$txt =~ s/\s\s+/ /g;
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
sub clean()
{
	my $value1=shift;
	$value1=~s/(\s*<>\s*)+//igs;
	$value1=~s/^\s*|\s*$//igs;
	$value1=~s/\s+/ /igs;
	$value1=~s/\'/''/igs;
	return($value1);
}
sub Tag_Clean
{
	my $value=shift;
	$value=~s/\s*<>\s*/\|/igs;
	$value=~s/\s*\|\s*$//igs;
	$value=~s/^\|+|\|+$//igs;
	return($value);
}
