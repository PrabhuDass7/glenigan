package DB_OneApp_Test;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);

###### DB Connection ####
sub DbConnection()
{
	# Local IP : 172.27.137.181
	# Cloud IP : 10.56.174.29
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.181;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# my $dsn1 ='driver={SQL Server};Server=CH1025BD03;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dsn1 ='driver={SQL Server};Server=10.101.53.25;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nLOCAL SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	
	my $Recon_Flag=1;
	# if($Connection_Flag==0)
	# {
		# Reconnect2:
		# if($dbh  = DBI->connect("DBI:ODBC:$dsn2"))
		# {
			# print "\nCLOUD SERVER CONNECTED\n";
		# }
		# else
		# {
			# print "\nFAILED\n";
			# if($Recon_Flag<=3)
			# {
				# $Recon_Flag++;
				# goto Reconnect2;
			# }	
		# }
	# }
	
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}
sub DbConnection1() # Old connection
{
	# my $dsn ='driver={SQL Server};Server=172.27.137.181;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dsn ='driver={SQL Server};Server=10.101.53.25;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh  = DBI->connect("DBI:ODBC:$dsn") or die "$DBI::errstr\n";
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}

###### Retrieve DocumentURL ####
sub RetrieveUrl()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $startdate 		= shift;
	# my $enddate 		= shift;
	
		# my $query = "select ID,Source,Application_No,Document_Url,F.MARKUP,URL from FORMAT_PUBLIC_ACCESS F,L_council CO
		# where CONVERT(Varchar(10),Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
		# And F.council_Code=Co.COUNCIL_CD_P
		# And COUNCIL_CD_P = $council_code and All_Documents_Downloaded=\'N\' and F.MARKUP in ('HOUSE','LARGE','Smalls','Minor') ORDER BY ID DESC";
		
		# my $query = "select  id,SOURCE ,Application_No ,Document_Url ,MARKUP ,URL,
		# case when project_id <>'' then project_id else house_extn_id end as project_id,project_status 
		# from ( 
		# select ID,Source,Application_No,Document_Url,F.MARKUP,URL, 
		# isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
		# isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
		# isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status
		# from FORMAT_PUBLIC_ACCESS f,L_council co
		# where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-3000,112) and CONVERT(Varchar(10),GETDATE(),112)
		# And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = $council_code and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor') 
		# and f.ID in (7280315,7280457)
		# ) as c ORDER BY ID DESC";
		
		my $query = "select  id,SOURCE ,Application_No ,Document_Url ,MARKUP ,URL,
		case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME 
		from ( 
		select ID,Source,Application_No,Document_Url,F.MARKUP,URL, 
		isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
		isnull((select  top 1 HOUSE_EXTN_ID_P    from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
		isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME
		from FORMAT_PUBLIC_ACCESS f,L_council co
		where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
		And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P =  $council_code and All_Documents_Downloaded = 'N' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor') 
		 ) as c where  project_status <> 'Processed Complete' ORDER BY ID DESC";

	
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_ID,@Source,@Application_No,@Document_Url,@Markup,@URL,@PROJECT_ID,@PROJECT_STATUS,@COUNCIL_NAME);
	my $reccount;
	my $datetime = localtime();
	open(ERR,">>Large_Projects_Occurrence.txt");
	while(my @record = $sth->fetchrow)
	{
		push(@Format_ID,&Trim($record[0]));
		push(@Source,&Trim($record[1]));
		push(@Application_No,&Trim($record[2]));
		push(@Document_Url,&Trim($record[3]));
		push(@Markup,&Trim($record[4]));
		push(@URL,&Trim($record[5]));
		push(@PROJECT_ID,&Trim($record[6]));
		push(@PROJECT_STATUS,&Trim($record[7]));
		push(@COUNCIL_NAME,&Trim($record[8]));
		if($record[4]=~m/large/is)
		{
			print ERR "$record[0]\t$record[4]\t$record[6]\t$record[7]\t$datetime\n";
		}	
	}
	close ERR;		
	$sth->finish();
	return (\@Format_ID,\@Source,\@Application_No,\@Document_Url,\@Markup,\@URL,\@PROJECT_ID,\@PROJECT_STATUS,\@COUNCIL_NAME);
}

sub RetrieveMD5()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $format_id	 	= shift;
	
	my $query = "select PDF_MD5 from TBL_PDF_MD5 where council_code=\'$council_code\' and format_id=\'$format_id\'";
	print "$query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@PDF_MD5);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@PDF_MD5,&Trim($record[0]));
	}
	$sth->finish();
	return (\@PDF_MD5);
}

sub Retrieve_OneApp_For_Scrape()
{
	my $dbh 			= shift;
	my $Date 			= shift;
	
	# my $query = "select ID, COUNCIL_CODE, DOCUMENT_NAME, APPLICATION_NO, SOURCE, MARKUP from format_public_access where Document_Name is not null and ID in (8302720)";

	my $query = "select ID, COUNCIL_CODE, DOCUMENT_NAME, APPLICATION_NO, SOURCE, MARKUP from FORMAT_PUBLIC_ACCESS where 
	One_App = 'N' and 
	(Document_Name is not null and  Document_Name != '') and
	Application_Extracted = 'N' and MARKUP <> 'Minor' and 	ID >= 7214294";

	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_id,@Council_Code,@DOCUMENT_NAME,@APPLICATION_NO,@SOURCE,@MARKUP);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Format_id,&Trim($record[0]));
		push(@Council_Code,&Trim($record[1]));
		push(@DOCUMENT_NAME,&Trim($record[2]));
		push(@APPLICATION_NO,&Trim($record[3]));
		push(@SOURCE,&Trim($record[4]));
		push(@MARKUP,&Trim($record[5]));
	}
	$sth->finish();
	return (\@Format_id,\@Council_Code,\@DOCUMENT_NAME,\@APPLICATION_NO,\@SOURCE,\@MARKUP);
}

sub Retrieve_Error_Report()
{
	my $dbh 			= shift;
	my $Date 			= shift;
	
	my $query = "select  id as format_id,application_no,MARKUP,council_code,
	(select max(cast(ISNULL(No_Of_Documents,0) as int)) from OneAppLog where Format_ID=a.ID ) as no_of_documents_in_website,
	(select sum(cast(ISNULL(No_Of_Doc_Downloaded,0) as int)) from OneAppLog where Format_ID=a.ID ) as No_Of_Doc_Downloaded
	from format_Public_Access as  a 
	where CONVERT(varchar,Schedule_Date,103)=\'$Date\' and MARKUP ='large'
	order by id";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_id, @Application, @Markup, @Council_Code, @No_Of_Documents_in_Website, @No_Of_Doc_Downloaded);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Format_id,&Trim($record[0]));
		push(@Application,&Trim($record[1]));
		push(@Markup,&Trim($record[2]));
		push(@Council_Code,&Trim($record[3]));
		push(@No_Of_Documents_in_Website,&Trim($record[4]));
		push(@No_Of_Doc_Downloaded,&Trim($record[5]));
	}
	$sth->finish();
	return (\@Format_id,\@Application,\@Markup,\@Council_Code,\@No_Of_Documents_in_Website,\@No_Of_Doc_Downloaded);
}

###### Pdf Download Path ####
sub DownloadPath()
{
	my $dbh 		= shift;
	my $downloadpath;
	my $query = "Select Picklistvalue from PickList where PicklistCategory='ONE_APP' and PickListField='PDF_DOWNLOAD_PATH' AND ID=436";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	while(my @record = $sth->fetchrow)
	{
		$downloadpath = &Trim($record[0]);
	}
	$sth->finish();
	return $downloadpath;
}

###### Update DB ####
sub UpdateDB
{
	my $dbh		= shift;
	my $query 	= shift;
	my $sth = $dbh->prepare($query);
	print "\nOK\n";
	eval
	{
		$sth->execute();
		$sth->finish();	
		print "Updated\n";
	};
	if($@)
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

sub DbValidate()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/\'/''/g;
	
	return $txt;
}

sub Check_download
{
	my $Format_ID 		= shift;
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $document_type 	= shift;
	my $councilcode 	= shift;
	my $Application_No 	= shift;
	my $Source			= shift;
	my $Markup			= shift;	
	my $Temp_pdf_store_path	= shift;
	my $Document_Url	= shift;
	my $set_cookie		= shift;
	my $dbh				= shift;
	
	my $flag;
	##opendir ODH , "$Format_ID" or die "Archive directory not found \n";
	opendir ODH , "$Temp_pdf_store_path" or die "Archive directory not found \n";
	my @pdfiles = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	# print "PDF shifted from :: $pdf_name\n";
	# $pdf_name = (split('\/',$pdf_url))[-1];
	# print "PDFNAME :: $pdf_name\n";
	if (grep (/$pdf_name/, @pdfiles) && ($pdf_name!~ m/\s*(Application|App|Planning)(?:\s*|\_)\(?(?:Forms?|information|Documents?|plans?|Details?|Applications?|Public_View|Permission|(?:\s*|\_)for(?:\s*|\_)plan)\)?\s*/is))
	{
		$flag = 1;
		print "$pdf_name file already downloaded \n";
	}
	else
	{
		my $file_name;
		if( grep (/$pdf_name/, @pdfiles))
		{
			print "File Already Downloaded\n";
			$file_name=$Temp_pdf_store_path.'\\'.$pdf_name;
		}
		elsif($councilcode==425)
		{
			my $Host='www.planning.wealden.gov.uk';
			my $pdf_content =&Post_Method($Document_Url,$pdf_url,$Host,$Document_Url);
			$file_name=$Temp_pdf_store_path.'\\'.$pdf_name;
			my $fh = FileHandle->new("$file_name",'w') or die "Cannot open $Temp_pdf_store_path/$pdf_name for write :$!";
			binmode($fh);
			$fh->print($pdf_content);
			$fh->close();
		}	
		else
		{
			my ($pdf_content,$redir) =&Getcontent($pdf_url,$Document_Url,$set_cookie);
			$file_name=$Temp_pdf_store_path.'\\'.$pdf_name;
			my $fh = FileHandle->new("$file_name",'w') or die "Cannot open $Temp_pdf_store_path/$pdf_name for write :$!";
			binmode($fh);
			$fh->print($pdf_content);
			$fh->close();
		}
		if ( $pdf_name !~ m/\.pdf\s*$/is )
		{
			$pdf_name .= ".pdf";
		}
		print "Downloaded\n";
		
		if ($pdf_name=~m/\s*(Application|App|Planning)(?:\s*|\_)\(?(?:Forms?|information|Documents?|plans?|Details?|Applications?|Public_View|Permission|(?:\s*|\_)for(?:\s*|\_)plan)\)?\s*/is )
		{
			&Scrape::Scrape_pdf($dbh,$file_name,$Format_ID,$councilcode,$Application_No,$Source,$Markup);
		}
		$flag = 0;
	}
	# print "PDF RETURN TO :: $pdf_name\n";
	return ($pdf_name,$flag);
}

sub SaveFormatPdf()
{
	my $dbh 									= shift;
	my $Source									= shift;
	my $Format_ID								= shift;
	my $Application_No							= shift;
	my $councilcode								= shift;
	my $Markup									= shift;
	my $site_house								= shift;
	my $site_suffix								= shift;
	my $site_house_name							= shift;
	my $site_street								= shift;
	my $site_city								= shift;
	my $Site_County								= shift;
	my $Site_Country							= shift;
	my $site_postcode							= shift;
	my $easting									= shift;
	my $northing								= shift;
	my $applicant_title							= shift;
	my $applicant_first_name					= shift;
	my $applicant_surname						= shift;
	my $applicant_company_name					= shift;
	my $applicant_street						= shift;
	my $applicant_town							= shift;
	my $applicant_county						= shift;
	my $applicant_country						= shift;
	my $applicant_postcode						= shift;
	my $applicant_telephone						= shift;
	my $applicant_mobile						= shift;
	my $applicant_fax							= shift;
	my $applicant_email							= shift;
	my $agent_title								= shift;
	my $agent_first_name						= shift;
	my $agent_surname							= shift;
	my $agent_company_name						= shift;
	my $agent_street							= shift;
	my $agent_town								= shift;
	my $agent_county							= shift;
	my $agent_country							= shift;
	my $agent_postcode							= shift;
	my $agent_telephone							= shift;
	my $agent_mobile							= shift;
	my $agent_fax								= shift;
	my $agent_email								= shift;
	my $oneapp_proposal							= shift;
	my $existing_wall_material					= shift;
	my $proposed_wall_material					= shift;
	my $existing_roof_material					= shift;
	my $proposed_roof_material					= shift;
	my $existing_window_material				= shift;
	my $proposed_window_material				= shift;
	my $existing_doors_material					= shift;
	my $proposed_doors_material					= shift;
	my $existing_boundary_treatment_material	= shift;
	my $proposed_boundary_treatment_material	= shift;
	my $existing_vehicle_access_material		= shift;
	my $proposed_vehicle_access_material		= shift;
	my $existing_lightning_material				= shift;
	my $proposed_lighning_material				= shift;
	my $existing_others_materials				= shift;
	my $proposed_others_materials				= shift;
	my $material_additional_information			= shift;
	my $material_additional_info_details		= shift;
	my $vehicle_car								= shift;
	my $vehicle_light_good						= shift;
	my $vehicle_motorcycle						= shift;
	my $vehicle_disability						= shift;
	my $vehicle_cycle							= shift;
	my $vehicle_other							= shift;
	my $vehicle_other_desc						= shift;
	my $existing_use							= shift;
	my $existing_residential_house				= shift;
	my $proposed_houses							= shift;
	my $proposed_flats_maisonettes				= shift;
	my $proposed_live_work_units				= shift;
	my $proposed_cluster_flats					= shift;
	my $proposed_sheltered_housing				= shift;
	my $proposed_bedsit_studios					= shift;
	my $proposed_unknown						= shift;
	my $proposed_market_housing_total			= shift;
	my $existing_houses							= shift;
	my $existing_flats_maisonettes				= shift;
	my $existing_live_work_units				= shift;
	my $existing_cluster_flats					= shift;
	my $existing_sheltered_housing				= shift;
	my $existing_bedsit_studios					= shift;
	my $existing_unknown						= shift;
	my $existing_market_housing_total			= shift;
	my $total_proposed_residential_units		= shift;
	my $total_existing_residential_units		= shift;
	my $non_residential_shops					= shift;
	my $non_residential_financial				= shift;
	my $non_residential_restaurants				= shift;
	my $non_residential_drinking				= shift;
	my $non_residential_food					= shift;
	my $non_residential_office					= shift;
	my $non_residential_research				= shift;
	my $non_residential_light					= shift;
	my $non_residential_general					= shift;
	my $non_residential_storage					= shift;
	my $non_residential_hotels					= shift;
	my $non_residential_residential				= shift;
	my $non_residential_institutions			= shift;
	my $non_residential_assembly				= shift;
	my $non_residential_other					= shift;
	my $non_residential_total					= shift;
	my $existing_employee						= shift;
	my $proposed_employee						= shift;
	my $site_area								= shift;
	
	
	# my $insert_query = "insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area) values (\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\') ";

	my $insert_query = "insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_Country,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area) values (\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$Site_Country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\') ";	
	# print "INSERT QUERY :: $insert_query\n";
	my $sth = $dbh->prepare($insert_query);
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $insert_query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $insert_query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub One_App_Log()
{
	my $dbh	= shift;
	my $Format_ID	= shift;
	my $Council_CD	= shift;
	my $Markup	= shift;
	my $Document_URL	= shift;
	my $No_Of_Documents	= shift;
	my $Document_Name	= shift;
	my $Document_URL_List	= shift;
	my $One_App	= shift;
	my $Scraped_Date	= shift;
	my $Comments	= shift;
	my $No_Of_Doc_Downloaded = shift;

	$Document_URL=~s/\'/\'\'/igs;
	$Document_Name=~s/\'/\'\'/igs;
	$Document_URL_List=~s/\'/\'\'/igs;
	$Comments=~s/\'/\'\'/igs;

	$Document_Name  = substr $Document_Name,0,8000; 
	$Document_URL_List  = substr $Document_URL_List,0,8000; 
	
	my $insert_query = "(\'$Format_ID\',\'$Council_CD\',\'$Markup\',\'$Document_URL\',\'$No_Of_Documents\',\'$Document_Name\',\'$Document_URL_List\',\'$One_App\',getdate(),\'$Comments\',\'$No_Of_Doc_Downloaded\'),";	
	
	# my $insert_query = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,Document_Name,Document_URL_List,One_App,Scraped_Date,Comments) values (\'$Format_ID\',\'$Council_CD\',\'$Markup\',\'$Document_URL\',\'$No_Of_Documents\',\'$Document_Name\',\'$Document_URL_List\',\'$One_App\',getdate(),\'$Comments\')";	
	# my $sth = $dbh->prepare($insert_query);
	# if($sth->execute())
	# {
		# print "Executed\n";
	# }
	# else
	# {
		# print "QUERY:: $insert_query\n";
		# open(ERR,">>OneAppLog_Failed_Query.txt");
		# print ERR $insert_query."\n";
		# close ERR;
		# $dbh=&DbConnection();
	# }
	
	return $insert_query;
}