package Download_Schedule_DB;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;

# require Exporter;
# my @ISA = qw(Exporter);
# my @EXPORT = qw(ImageDownload);


###### DB Connection ####
sub DbConnection()
{
	# Test DB
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.183;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# Live DB
	my $dsn1 ='driver={SQL Server};Server=10.101.53.25;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}

sub Retrieve_Input_CC()
{
	my $dbh 			= shift;
=pod		
	my $query ="select distinct COUNCIL_NAME,COUNCIL_CD_P
				from (
				select ID,Source,Application_No,Document_Url,F.MARKUP,URL,
				isnull((select  top 1 PROJECT_ID_P from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
				isnull((select  top 1 HOUSE_EXTN_ID_P    from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
				isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
				from FORMAT_PUBLIC_ACCESS f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P  and All_Documents_Downloaded = 'N' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor')
				) as c where  project_status <> 'Processed Complete'";
=cut

	my $query= "select COUNCIL_NAME,COUNCIL_CD_P
			    from (
			    select ID,Source,Application_No,Document_Url,F.MARKUP,URL,
			    isnull((select  top 1 PROJECT_ID_P from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
			    isnull((select  top 1 HOUSE_EXTN_ID_P    from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
				isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
				from FORMAT_PUBLIC_ACCESS f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P  and All_Documents_Downloaded = 'N' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor')
				) as c where  project_status <> 'Processed Complete'
				order by id desc";
				
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@COUNCIL_NAME,@COUNCIL_CD_P);	
	while(my @record = $sth->fetchrow)
	{
		push(@COUNCIL_NAME,&Trim($record[0]));
		push(@COUNCIL_CD_P,&Trim($record[1]));
	}
	$sth->finish();
	
	my @filteredCC = uniq(@COUNCIL_CD_P);
	my @filteredCN = uniq(@COUNCIL_NAME);
	
	return (\@filteredCN,\@filteredCC);
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

1;