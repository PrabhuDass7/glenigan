package Scrape;
use strict;
use DBI;
use DBD::ODBC;
use filehandle;
# use LWP::Simple qw(getstore);
use CAM::PDF;
use CAM::PDF::PageText;
require Exporter;
use Spreadsheet::XLSX;

# require "D:/Arul/DB_OneApp.pm";
require "C:/Glenigan/Merit/Projects/docdownload_perl/lib/DB_OneApp.pm";
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);


sub Scrape_pdf
{
	my $dbh				= shift;
	my $pdf_name		= shift;
	my $Format_ID  		= shift;
	my $councilcode		= shift;
	my $Application_No	= shift;
	my $Source			= shift;
	my $Markup			= shift;
	
	# print "PDF NAME IN Scraping:: $pdf_name\n";
	my $text_file_name=$pdf_name;
	my $xml_file_name=$pdf_name;
	my $xl_file_name=$pdf_name;
	my $html_file_name="PDF2HTML$councilcode.html";
	$text_file_name=~s/\.pdf\s*$/.txt/igs;
	$xl_file_name=~s/\.pdf\s*$/.xls/igs;
	$xml_file_name=~s/\.pdf\s*$/.xml/igs;

	if($pdf_name=~m/\.(?:png|jpg)\s*$/is) 
	{
		goto create_xml_last;
	}
	eval{
	system("PERL","deillustrate.pl",$pdf_name,$pdf_name);
	};
	
	if($pdf_name=~ m/COVER[ING]+_?LETTER|\.jpg|\.png/is) 
	{
		goto create_xml_last;
	}
	my $pdf = eval{CAM::PDF->new("$pdf_name")};
	my $page;
	eval{$page = $pdf->numPages()};
	open my $fh, ">:encoding(utf-8)",  $text_file_name   or die "could not open $text_file_name : $!\n";
	print $fh "";
	close $fh;
	for(my $i=1;$i<=$page;$i++)
	{
		my $pageone_tree;
		eval {
			local $SIG{ALRM} = sub { die "Alarm Timeout\n" };
			alarm 5;
			$pageone_tree = eval{$pdf->getPageContentTree($i)};
			alarm 0;
		};
		if($@){
			print "Alarm Timeout\n";
		}		
		open my $fh, ">>:encoding(utf-8)",  $text_file_name   or die "could not open $text_file_name : $!\n";
		print $fh eval {CAM::PDF::PageText->render($pageone_tree)};
		close $fh;
		my $pdf_file_cont= eval{CAM::PDF::PageText->render($pageone_tree)};
		# print "PDF CAM PAGES CRAWLED 2:: $text_file_name  :: $pdf_file_cont\n";
	}
	# print "PDF CAM PAGES CRAWLED 1:: $text_file_name\n";
	eval{
		system("pdftohtml.exe -h $pdf_name $html_file_name");
		};
	# system("pdftotext $pdf_name $text_file_name");
	my $filesize = -s "$text_file_name";

	if($filesize == 0 || $filesize eq '' )
	{
		my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'I', Last_Visited_Date = getdate() where id = $Format_ID and council_code = $councilcode";
		print "query: \n$query\n";
		&DB_OneApp::UpdateDB($dbh, $query);
		goto create_xml_last;
	}
	else
	{
		my $query = "update FORMAT_PUBLIC_ACCESS set One_App = 'Y', Last_Visited_Date = getdate() where id = $Format_ID and council_code = $councilcode";
		&DB_OneApp::UpdateDB($dbh, $query);
	}
	
	open IN,"<$text_file_name";
	my ($content);
	my $Prev_Title_Number=0;
	while(<IN>)
	{
		# $content.=$_;
		if($_=~m/^(\d+)\.\s*[a-z]+/is)
		{
			my $Next_Title_Number=$1;
			$_=~s/\n/<>\n/igs;
			if($Next_Title_Number==$Prev_Title_Number+1)
			{
				$Prev_Title_Number=$Next_Title_Number;
				$content=$content."|Split|\n".$_;
			}
			else
			{
				$content=$content." ".$_;
			}	
		}
		else
		{
			$_=~s/\n/<>\n/igs;
			$content=$content." ".$_;
		}			
	}
	close IN;
	$content.="|Split|\n";
	$content=~s/(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)//igs; # to remove page continuation mark
	$content=~s/Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>//igs; # to remove page reference
	if($content=~m/A\s+p\s+p\s+l\s+i\s+c\s+a\s+n\s+t\s+N\s+a\s+m\s+e/is)
	{
		$content=~s/\s\s+/DoubleSpace/igs; # to remove page reference
		$content=~s/\s//igs; # to remove page reference
		$content=~s/DoubleSpace/ /igs; # to remove page reference
	}
	# open(wr,">Formated_Content_122.html");
	# print wr $content;
	# close wr;
	$_=~s/<>(?:<>)+/<>/igs;

	my $html_file_path='D:/One_App/';
	opendir(my $dhf, $html_file_path) || die "can't opendir $html_file_path: $!";
	my @File_Names=readdir($dhf);
	closedir $dhf;
	my ($Html_Content);
	for(@File_Names)
	{
		if(($_=~m/PDF2HTML$councilcode/is) && ($_=~m/\.HTML/is))
		{
			open(re,"<$_");
			my $Prev_Title_Number=0;
			while(<re>)
			{
				# $Html_Content.=$_;
				if($_=~m/>(\d+)\.\s*[a-z]+/is)
				{
					my $Next_Title_Number=$1;
					if($Next_Title_Number==$Prev_Title_Number+1)
					{
						$Prev_Title_Number=$Next_Title_Number;
						$Html_Content=$Html_Content."|Split|\n".$_;
					}
					else
					{
						$Html_Content=$Html_Content." ".$_;
					}	
				}
				else
				{
					$Html_Content=$Html_Content." ".$_;
				}			
			}
			close re;			
		}	
	}	


	# my @Html_Cont_Array=<re>;
	# close(re);
	# my $Html_Content=join("",@Html_Cont_Array);
	$Html_Content.="|Split|\n";
	$Html_Content=~s/(?:\n)?[\d]+\.\s*\(?[a-z\-\:\s]+\s*\(?continued\)//igs; # to remove page continuation mark
	$Html_Content=~s/Ref\s*\:\s*[^>]*?Planning\s*Portal\s*Reference\s*\:[^>]*?<>//igs; # to remove page reference
	open(wr,">Formated_HTML_Content.html");
	print wr $Html_Content;
	close wr;

	my $flag=0;
	my ($site_house,$site_suffix,$site_house_name,$site_street,$site_city,$Site_County,$site_country,$site_postcode,$easting,$northing,$applicant_title,$applicant_first_name,$applicant_surname,$applicant_company_name,$applicant_street,$applicant_town, $applicant_county, $applicant_postcode, $applicant_telephone, $applicant_mobile, $applicant_fax, $applicant_email,$agent, $agent_title, $agent_first_name, $agent_surname,$applicant_country, $agent_company_name, $agent_street, $agent_street, $agent_town, $agent_county, $agent_country, $agent_postcode, $agent_telephone, $agent_mobile, $agent_fax,$agent_email,$oneapp_proposal,$vehicle_car,$vehicle_light_good,$vehicle_motorcycle,$vehicle_disability,$vehicle_cycle,$vehicle_other,$vehicle_other_desc,$res, $existing_wall_material, $proposed_wall_material, $existing_roof_material, $proposed_roof_material, $existing_window_material, $proposed_window_material, $existing_doors_material, $proposed_doors_material, $existing_boundary_treatment_material, $proposed_boundary_treatment_material, $existing_vehicle_access_material, $proposed_vehicle_access_material, $existing_lightning_material, $proposed_lighning_material, $existing_others_materials, $proposed_others_materials, $material_additional_information, $material_additional_info_details, $existing_use, $existing_residential_house, $proposed_houses, $proposed_flats_maisonettes, $proposed_live_work_units, $proposed_cluster_flats, $proposed_sheltered_housing, $proposed_bedsit_studios, $proposed_unknown, $proposed_market_housing_total, $existing_houses, $existing_flats_maisonettes, $existing_live_work_units, $existing_cluster_flats, $existing_sheltered_housing, $existing_bedsit_studios, $existing_unknown, $existing_market_housing_total, $total_proposed_residential_units, $total_existing_residential_units, $non_residential_shops, $non_residential_financial, $non_residential_restaurants, $non_residential_drinking, $non_residential_food, $non_residential_office, $non_residential_research, $non_residential_light, $non_residential_general, $non_residential_storage, $non_residential_hotels, $non_residential_residential, $non_residential_institutions, $non_residential_assembly, $non_residential_other, $non_residential_total, $existing_employee, $proposed_employee, $site_area);
	if($content=~m/\d+\.\s*(?:site\s*address|Trees\s*Location)\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $site=$1;
		$flag=1;
		
		print "------------ Site Details ------------\n";
		
		if($site=~m/House\:([\w\W]*?)\s*Suffix/is)
		{
			$site_house	= trim($1);
			$site_house	=~s/<>/ /igs;
			$site_house	=~s/\s\s+/ /igs;
			print "site_house		:: $site_house\n";
		}
		elsif($site=~m/House\:([^<]*?)\s*</is)
		{
			$site_house	= trim($1);
			$site_house	=~s/<>/ /igs;
			$site_house	=~s/\s\s+/ /igs;
			print "site_house		:: $site_house\n";
		}
		if($site=~m/Suffix\:([\w\W]*?)\s*House\s*name/is)
		{
			$site_suffix=trim($1);
			$site_suffix=~s/<>/ /igs;
			$site_suffix=~s/\s\s+/ /igs;
		}
		elsif($site=~m/Suffix\:([^<]*?)\s*</is)
		{
			$site_suffix=trim($1);
			$site_suffix=~s/<>/ /igs;
			$site_suffix=~s/\s\s+/ /igs;
		}
		if($site=~m/House\s*name\:([\w\W]*?)\s*Street\s*address/is)
		{
			$site_house_name= trim($1);
			$site_house_name=~s/<>/ /igs;
			$site_house_name=~s/\s\s+/ /igs;
		}
		elsif($site=~m/House\s*name\:([^<]*?)\s*</is)
		{
			$site_house_name	= trim($1);
			$site_house_name	=~s/\s+/ /igs;
		}
		if($site=~m/Street\s*address\:([\w\W]*?)\s*Town\/City/is)
		{
			$site_street= trim($1);
			$site_street=~ s/<>/ /igs;
			$site_street=~ s/\s\s+/ /igs;
		}
		if($site=~m/Town\/city\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$site_city	= trim($1);
			$site_city	=~ s/\:\s*//igs;
			$site_city	=~ s/\s+/ /igs;
		}
		if($site=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		{
			$Site_County	= trim($1);
			$Site_County	=~ s/\:\s*//igs;
			$Site_County	=~ s/\s+/ /igs;
		}		
		if($site=~m/Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)</is)
		{
			$site_country=trim($1);
			$site_country=~s/\s+/ /igs;
		}
		if($site=~m/>\s*Postcode\s*(?:\:)?\s*(?:<>)?\s*([^>]*?)\s*(?:<|Are)/is)
		{
			$site_postcode	= $1;
			$site_postcode	=~ s/\W/ /igs;
			$site_postcode	=~s/\s\s+/ /igs;
		}

		if($site=~m/Easting\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Northing)/is)
		{
			$easting	= trim($1);
			$easting	=~ s/\s+/ /igs;
		}
		if($site=~m/Northing\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$northing	= trim($1);
			$northing	=~ s/\s+/ /igs;
		}	
	}
	print "site_suffix		:: $site_suffix\n";
	print "site_house_name		:: $site_house_name\n";
	print "site_street		:: $site_street\n";
	print "site_city		:: $site_city\n";
	print "Site_County		:: $Site_County\n";
	print "site_country:: $site_country\n";
	print "site_postcode		:: $site_postcode\n";
	print "Northing		:: $northing\n";
	print "Easting			:: $easting\n";
	# "------------ Applicant Phone, Mobile And Fax By using PDF to Excel Conversion ------------\n";

	# eval{
	# system("C://Program Files//Total PDF ConverterX//PDFConverterX.exe","$pdf_name","$xl_file_name");
	# };
	my $xl_file_name_read=$xl_file_name.'x';

	eval{
	my $excel = Spreadsheet::XLSX->new("$xl_file_name_read",);

    my $worksheet = $excel->Worksheet("Sheet1");
	my ($row_min,$row_max) = $worksheet->row_range();
	my ($col_min,$col_max) = $worksheet->col_range();

	my (@TPH_Value,@MN_Value,@Fax_Value);
	my $ph_cnt=0;
	my $mo_cnt=0;
	my $fa_cnt=0;
	for(my $r=$row_min;$r<=$row_max;$r++)
	{
		for(my $c=$col_min;$c<=$col_max;$c++)
		{
			my $cell = $worksheet->get_cell($r,$c);
			if($cell)
			{
				my $Value = $cell->value();
				if($Value=~m/Telephone\s*number\s*\:/is)
				{
					my $Telephone_Col_Range=$c+10;
					for(my $TPH=$c+1;$TPH<=$Telephone_Col_Range;$TPH++)
					{
						my $TPH_Cell = $worksheet->get_cell($r,$TPH);
						if($TPH_Cell)
						{
							my $val_temp=$TPH_Cell->value();
							if($val_temp!~m/[a-z]/is)
							{
								$TPH_Value[$ph_cnt].=$val_temp;
							}	
						}	
					}	
					$ph_cnt++;
				}	
				elsif($Value=~m/Mobile\s*number\s*\:/is)
				{
					my $Mobile_Col_Range=$c+10;
					for(my $MN=$c+1;$MN<=$Mobile_Col_Range;$MN++)
					{
						my $MN_Cell = $worksheet->get_cell($r,$MN);
						if($MN_Cell)
						{
							my $Val_Tmp=$MN_Cell->value();
							if($Val_Tmp!~m/[a-z]/is)
							{
								$MN_Value[$mo_cnt].=$Val_Tmp;
							}	
						}	
					}	
					$mo_cnt++;
				}	
				elsif($Value=~m/Fax/is)
				{
					my $Fax_Col_Range=$c+10;
					for(my $Fax=$c+1;$Fax<=$Fax_Col_Range;$Fax++)
					{
						my $Fax_Cell = $worksheet->get_cell($r,$Fax);
						if($Fax_Cell)
						{
							my $val_tmp1=$Fax_Cell->value();
							if($val_tmp1!~m/[a-z]/is)
							{
								$Fax_Value[$fa_cnt].=$val_tmp1;
							}	
						}	
					}	
					$fa_cnt++;
				}
			}	
		}		
	}
	if($TPH_Value[0])
	{
		$applicant_telephone=$TPH_Value[0];
	}	
	if($TPH_Value[1])
	{
		$agent_telephone=$TPH_Value[1]
	}	
	if($MN_Value[0])
	{
		$applicant_mobile=$MN_Value[0] 
	}
	if($MN_Value[1])
	{
		$agent_mobile=$MN_Value[1]
	}
	if($Fax_Value[0])
	{
		$applicant_fax=$Fax_Value[0]
	}
	if($Fax_Value[1])
	{
		$agent_fax=$Fax_Value[1]
	}
	};
	# "------------ Applicant Excel Conversion End ------------\n";
	
	print "------------ Applicant Details ------------\n";
	my $applicant;
	if($content=~m/\d+\.\s*Applicant\s*name([\w\W]*?)\|Split\|\s*/is)
	{
		$applicant=$1;
		if($applicant=~m/Title\:\s*([^<]*?)<>\s*First\s*name\:\s*([^<]*?)<>\s*Surname\:\s*([^<]*?)\s*</is)
		{
			$applicant_title 		= trim($1);
			$applicant_first_name 	= trim($2);
			$applicant_surname 		= trim($3);
			$applicant_title		=~ s/\s+/ /igs;
			$applicant_first_name	=~ s/\s+/ /igs;
			$applicant_surname		=~ s/\s+/ /igs;
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		elsif ($applicant=~m/Title\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*First\s*name\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*Surname\:\s*(?:\s*<>\s*)*([^<]*?)\s*</is)
		{
			$applicant_title 		= trim($1);
			$applicant_first_name 	= trim($2);
			$applicant_surname 		= trim($3);
			$applicant_title		=~ s/\s+/ /igs;
			$applicant_first_name	=~ s/\s+/ /igs;
			$applicant_surname		=~ s/\s+/ /igs;
			print "applicant_title		:: $applicant_title\n";
			print "applicant_first_name	:: $applicant_first_name\n";
			print "applicant_surname	:: $applicant_surname\n";
		}
		
		if($applicant=~m/Company\s*name\s*\:?\s*([\w\W]*?)\s*Street\s*address/is)
		{
			$applicant_company_name = trim($1);
			$applicant_company_name	=~ s/\s+/ /igs;
			print "applicant_company_name	:: $applicant_company_name\n";
		}
		elsif($applicant=~m/Company\s*name\s*\:?\s*([^<]*?)\s*</is)
		{
			$applicant_company_name = trim($1);
			$applicant_company_name	=~ s/\s+/ /igs;
			print "applicant_company_name	:: $applicant_company_name\n";
		}
		
		if($applicant=~m/Street\s*address\s*\:?\s*([\w\W]*?)(?:Town\/City|Telephone)/is)
		{
			$applicant_street	= trim($1);
			$applicant_street	=~ s/<>/ /igs;
			$applicant_street	=~ s/\s+/ /igs;
		}
		elsif($applicant=~m/Street\s*address\s*\:?\s*([\w\W]*?)Town\/City/is)
		{
			$applicant_street	= trim($1);
			$applicant_street	=~ s/<>/ /igs;
			$applicant_street	=~ s/\s+/ /igs;
			print "applicant_street	:: $applicant_street\n";
		}
		
		if($applicant=~m/Town\/city\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$applicant_town	= $1;
			$applicant_town	=~ s/\s+/ /igs;
			print "applicant_town		:: $applicant_town\n";
		}
		if($applicant=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		{
			$applicant_county	= trim($1);
			$applicant_county	=~ s/\s+/ /igs;
			print "applicant_county	:: $applicant_county\n";
		}
		if($applicant=~m/>\s*Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)/is)
		{
			$applicant_country	=trim($1);
			$applicant_country	=~s/\s+/ /igs;
			print "applicant_country	:: $applicant_country\n";
		}
		if($applicant=~m/>\s*Postcode\s*(?:\:)?\s*(?:<>)?\s*([^>]*?)\s*(?:<|Are)/is)
		{
			$applicant_postcode	= trim($1);
			$applicant_postcode	=~s/\s\s+/ /igs;
			print "applicant_postcode	:: $applicant_postcode\n";
		}
		## Code Number
		my $applicant_telephone_Code='';
		if($applicant_telephone eq '')
		{
			if($applicant =~ m/>\s*Postcode\s*\:?\s*[\w\s\-]*?<>\s*([\d]+)\s*Country/is)
			{
				$applicant_telephone_Code= trim($1);
				$applicant_telephone_Code=~ s/\s+/ /igs;
			}
			elsif($applicant =~ m/>\s*([\d]+)\s*Country\s*<>\s*Code/is)
			{
				$applicant_telephone_Code= trim($1);
				$applicant_telephone_Code=~ s/\s+/ /igs;
			}	
			
			if($applicant =~ m/Telephone\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*<>/is)
			{
				$applicant_telephone	= trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				$applicant_telephone=$applicant_telephone_Code." ".$applicant_telephone;
			}
			elsif($applicant =~ m/\s+([\d\s]+?)National\s*Number/is)
			{
				$applicant_telephone	= trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				$applicant_telephone=$applicant_telephone_Code." ".$applicant_telephone;
			}
			elsif($applicant =~ m/>\s*([\d\s]+?)\s*National\s*<>\s*Number\s*</is)
			{
				$applicant_telephone	= trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				$applicant_telephone=$applicant_telephone_Code." ".$applicant_telephone;
			}		
			elsif($applicant =~ m/Telephone\s*number\s*\:([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s+/is)
			{
				$applicant_telephone	= trim($1);
				$applicant_telephone	=~ s/\s+/ /igs;
				print "applicant_telephone		:: $applicant_telephone\n";
			}
			else
			{
				$applicant_telephone=$applicant_telephone_Code;
			}
		}	
		
		$applicant_telephone=~s/^\s+//igs;
		if($applicant_mobile eq '')
		{
			if($applicant =~ m/Mobile\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
			{
				$applicant_mobile	= trim($1);
				$applicant_mobile	=~ s/\s+/ /igs;
				print "applicant_mobiles		:: $applicant_mobile\n";
			}
		}
		if($applicant_fax eq '')
		{
			if($applicant =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
			{
				$applicant_fax	= trim($1);
				$applicant_fax	=~ s/\s+/ /igs;
				print "applicant_fax		:: $applicant_fax\n";
			}
		}	

		if($applicant=~m/\s*([^<]*?\@[^<]*?)\s/is)
		{
			$applicant_email	= trim($1);
			$applicant_email	=~ s/[^<]*?\n+([^<]+?\@)/$1/igs;
			$applicant_email	=~ s/\s+//igs;
			print "applicant_email		:: $applicant_email\n";
		}
	}
	# for Phone & fax using HTML content
	if($Html_Content=~m/\d+\.\s*Applicant\s*name([\w\W]*?)\|Split\|\s*/is)
	{
		my $applicant_html=$1;
		if($applicant_telephone eq '')
		{
			if($applicant_html =~ m/Telephone\s*number\s*:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				my $no1= $1;
				my $no2= $2;
				my $no3= $3;
				if($no3!='')
				{
					$applicant_telephone = $1." ".$2." ".$3;
					$applicant_telephone = ~s/\s+/ /igs;
				}	
			}
		}	
		if($applicant_mobile eq '')
		{
			if($applicant_html =~ m/Mobile\s*number\s*:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$applicant_mobile	= $1." ".$2;
				$applicant_mobile	=~ s/\s+/ /igs;
			}
		}
		if($applicant_fax eq '')
		{	
			if($applicant_html =~ m/Fax\s*number\s*:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$applicant_fax	= $1." ".$2;
				$applicant_fax	=~ s/\s+/ /igs;
			}
		}	
	}
	
#### New
	my ($applicant_mobile_t,$applicant_fax_t);
	my $applicant_fax_t_Flag=0;
	if($applicant=~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$applicant_mobile_t	= $1." ".$2;
		$applicant_fax_t	= $3." ".$4;
		$applicant_mobile_t	=~ s/\s+/ /igs;
		$applicant_fax_t	=~ s/\s+/ /igs;
		# print "applicant_mobile 		:: $applicant_mobile_t\n";
		# print "agent_fax 		:: $applicant_fax_t\n";
		if($applicant_fax_t!~m/^\s*$/is)
		{
			$applicant_fax_t_Flag=1;
		}
	}
	elsif($applicant =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		my $no3	= $3;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$applicant_mobile_t	= $no1." ".$no2;
			$applicant_fax_t	= $no3;
		}	
		elsif(length($no1) > 5)
		{
			$applicant_mobile_t	= $no1;
			$applicant_fax_t	= $no2." ".$no3;
		}	
		$applicant_mobile_t	=~ s/\s+/ /igs;
		$applicant_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile 		:: $applicant_mobile_t\n";
		# print "applicant_fax 		:: $applicant_fax_t\n";
		if($applicant_fax_t!~m/^\s*$/is)
		{
			$applicant_fax_t_Flag=1;
		}
	}
	elsif($applicant =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$applicant_mobile_t	= $no1." ".$no2;
		}	
		elsif(length($no1) > 5)
		{
			$applicant_mobile_t	= $no1;
			$applicant_fax_t	= $no2;
		}	
		$applicant_mobile_t	=~ s/\s+/ /igs;
		$applicant_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile 		:: $applicant_mobile_t\n";
		# print "applicant_fax 		:: $applicant_fax_t\n";
		if($applicant_fax_t!~m/^\s*$/is)
		{
			$applicant_fax_t_Flag=1;
		}
	}
	elsif($applicant =~ m/>\s*([\d\s]+?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$applicant_mobile_t	= $1;
		$applicant_mobile_t	=~ s/\s+/ /igs;
		# print "applicant_mobile 		:: $applicant_mobile_t\n";
	}
	
	if($applicant_fax_t_Flag==0)
	{
		if($applicant =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
		{
			$applicant_fax_t	= trim($1);
			$applicant_fax_t	=~ s/\s+/ /igs;
			# $res.="\t<Applicant_Postcode>$value</Applicant_Postcode>\n";
			# print "applicant_fax		:: $applicant_fax_t\n";
		}
	}
	
	if($applicant_mobile eq '')
	{
		$applicant_mobile=$applicant_mobile_t;
	}
	if($applicant_fax eq '')
	{
		$applicant_fax=$applicant_fax_t;
	}
#### New End	
	
	$applicant_telephone=~s/^\s+//igs;
	$applicant_mobile=~s/^\s+//igs;
	$applicant_fax=~s/^\s+//igs;
	print "applicant_mobile 	:: $applicant_mobile\n";
	print "applicant_fax 		:: $applicant_fax\n";
	
	
	print "------------ Agent Details ------------\n";
	my $agent;	
	if($content=~m/\d+\.\s*Agent\s*name\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		$agent=$1;
		if($agent=~m/Title\s*\:?\s*([^<]*?)\s*<>\s*First\s*name\s*\:?\s*([^<]*?)\s*<>\s*Surname\s*\:?\s*([^<]*?)\s*<>/is)
		{
			$agent_title 		= $1;
			$agent_first_name 	= $2;
			$agent_surname 		= $3;
			$agent_title		=~ s/\s+/ /igs;
			$agent_first_name	=~ s/\s+/ /igs;
			$agent_surname		=~ s/\s+/ /igs;
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		elsif($agent=~m/Title\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*First\s*name\:\s*(?:\s*<>\s*)*([^<]*?)<>\s*Surname\:\s*(?:\s*<>\s*)*([^<]*?)\s*</is)
		{
			$agent_title 		= $1;
			$agent_first_name 	= $2;
			$agent_surname 		= $3;
			$agent_title		=~ s/\s+/ /igs;
			$agent_first_name	=~ s/\s+/ /igs;
			$agent_surname		=~ s/\s+/ /igs;
			print "agent_title		:: $agent_title\n";
			print "agent_first_name	:: $agent_first_name\n";
			print "agent_surname		:: $agent_surname\n";
		}
		if($agent=~m/Company\s*name\s*\:?\s*([\w\W]*?)\s*Street\s*address/is)
		{
			$agent_company_name = $1;
			$agent_company_name	=~ s/\s+/ /igs;
			print "agent_company_name 	:: $agent_company_name\n";
		}
		elsif($agent=~m/Company\s*name\s*\:?\s*([^<]*?)\s*</is)
		{
			$agent_company_name = $1;
			$agent_company_name	=~ s/\s+/ /igs;
			print "agent_company_name 	:: $agent_company_name\n";
		}
		if($agent=~m/Street\s*address\s*\:?\s*([\w\W]*?)(?:Town\/City|Telephone)/is)
		{
			$agent_street 	= $1;
			$agent_street	=~ s/<>/ /igs;
			$agent_street	=~ s/\s+/ /igs;
			print "agent_street 		:: $agent_street\n";
		}
		elsif($agent=~m/Street\s*address\s*\:?\s*([\w\W]*?)Town\/City/is)
		{
			$agent_street 	= $1;
			$agent_street	=~ s/<>/ /igs;
			$agent_street	=~ s/\s+/ /igs;
			print "agent_street 		:: $agent_street\n";
		}
		
		if($agent=~m/Town\/city\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*</is)
		{
			$agent_town	= $1;
			$agent_town	=~ s/\s+/ /igs;
			print "agent_town 		:: $agent_town\n";
		}
		if($agent=~m/>\s*County\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Country)/is)
		{
			$agent_county	= $1;
			$agent_county	=~ s/\s+/ /igs;
			print "agent_county 		:: $agent_county\n";
		}
		if($agent=~m/>\s*Country\s*(?:\:)?\s*(?:<>)?\s*([^<]*?)\s*(?:<|Postcode)/is)
		{
			$agent_country	= $1;
			$agent_country	=~ s/\s+/ /igs;
			print "agent_country 		:: $agent_country\n";
		}
		if($agent =~ m/>\s*Postcode\s*(?:\:)?\s*(?:<>)?\s*([^>]*?)\s*(?:<|Are)/is)
		{
			$agent_postcode	= $1;
			$agent_postcode	=~ s/\W/ /igs;
			$agent_postcode	=~ s/\s\s+/ /igs;
			print "agent_postcode 		:: $agent_postcode\n";
		}
		## Code Number
		my $agent_telephone_Code='';
		if($agent_telephone eq '')
		{
			if($agent =~ m/>\s*Postcode\s*\:?\s*[\w\s\-]*?<>\s*([\d]+)\s*Country/is)
			{
				$agent_telephone_Code= trim($1);
				$agent_telephone_Code=~ s/\s+/ /igs;
			}
			elsif($agent =~ m/>\s*([\d\s]+)\s*Country\s*<>\s*Code/is)
			{
				$agent_telephone_Code= trim($1);
				$agent_telephone_Code=~ s/\s+/ /igs;
			}

			if($agent =~ m/Telephone\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*<>/is)
			{
				$agent_telephone	= trim($1);
				$agent_telephone	=~ s/\s+/ /igs;
				$agent_telephone=$agent_telephone_Code." ".$agent_telephone;
			}
			elsif($agent =~ m/\s+([\d\s]+?)National\s*Number/is)
			{
				$agent_telephone	= trim($1);
				$agent_telephone	=~ s/\s+/ /igs;
				$agent_telephone=$agent_telephone_Code." ".$agent_telephone;
			}
			elsif($agent =~ m/>\s*([\d\s]+?)\s*National\s*<>\s*Number\s*</is)
			{
				$agent_telephone	= trim($1);
				$agent_telephone	=~ s/\s+/ /igs;
				$agent_telephone=$agent_telephone_Code." ".$agent_telephone;
			}
		}	
		my $Fax_Flag=0;
		if($agent_mobile eq '')
		{
			if($agent =~ m/Mobile\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*<>/is)
			{
				$agent_mobile	= $1;
				$agent_mobile	=~ s/\s+/ /igs;
			}		
		}	
		if($agent_fax eq '')
		{
			if($agent =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
			{
				$agent_fax	= trim($1);
				$agent_fax	=~ s/\s+/ /igs;
			}
		}	

		if($agent=~m/([\w\.\-]+\@[\w\-]+\.[a-z]{2,3}\.*[a-z]{0,2})/is)
		{
			$agent_email	= $1;
			$agent_email	=~ s/\s+//igs;
		}
		
	}
	# for Phone & fax using HTML content
	if($Html_Content=~m/\d+\.\s*Agent\s*name\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $agent_html=$1;
		# open(ah,">agent_html.html");
		# print ah $agent_html;
		# print ah;
		if($agent_telephone eq '')
		{
			if($agent_html=~m/Telephone\s*number\s*\:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				my $no1= $1;
				my $no2= $2;
				my $no3= $3;
				$agent_telephone = $1." ".$2." ".$3;
				$agent_telephone =~s/\s+/ /igs;
				$agent_telephone =~s/^\s+|\s+$//igs;
			}
		}	
		if($agent_mobile eq '')
		{
			if($agent_html =~ m/Mobile\s*number\s*\:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$agent_mobile	= $1." ".$2;
				$agent_mobile	=~ s/\s+/ /igs;
			}
		}
		if($agent_fax eq '')
		{	
			if($agent_html =~ m/Fax\s*number\s*\:\s*<br>(?:\s*([\d\W]+)<br>)?(?:\s*([\d\W]+)\s*<br>)?(?:\s*([\d\W]+)\s*<br>)?/is)
			{
				$agent_fax	= $1." ".$2;
				$agent_fax	=~ s/\s+/ /igs;
			}
		}	
	}
	
#### NEW	
	my ($agent_mobile_t,$agent_fax_t);
	my $agent_fax_t_Flag=0;
	if($agent=~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$agent_mobile_t	= $1." ".$2;
		$agent_fax_t	= $3." ".$4;
		$agent_mobile_t	=~ s/\s+/ /igs;
		$agent_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile_t 		:: $agent_mobile_t\n";
		# print "agent_fax_t 		:: $agent_fax_t\n";
		if($agent_fax_t!~m/^\s*$/is)
		{
			$agent_fax_t_Flag=1;
		}
	}
	elsif($agent =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		my $no3	= $3;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$agent_mobile_t	= $no1." ".$no2;
			$agent_fax_t	= $no3;
		}	
		elsif(length($no1) > 5)
		{
			$agent_mobile_t	= $no1;
			$agent_fax_t	= $no2." ".$no3;
		}	
		$agent_mobile_t	=~ s/\s+/ /igs;
		$agent_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile_t 		:: $agent_mobile_t\n";
		# print "agent_fax_t 		:: $agent_fax_t\n";
		if($agent_fax_t!~m/^\s*$/is)
		{
			$agent_fax_t_Flag=1;
		}
	}
	elsif($agent =~ m/>\s*([\d\s]*?)\s*<>\s*([\d\s]*?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		my $no1	= $1;
		my $no2	= $2;
		if((length($no1) <= 5) && (length($no1) > 0))
		{
			$agent_mobile_t	= $no1." ".$no2;
		}	
		elsif(length($no1) > 5)
		{
			$agent_mobile_t	= $no1;
			$agent_fax_t	= $no2;
		}	
		$agent_mobile_t	=~ s/\s+/ /igs;
		$agent_fax_t	=~ s/\s+/ /igs;
		# print "agent_mobile_t 		:: $agent_mobile_t\n";
		# print "agent_fax_t 		:: $agent_fax_t\n";
		if($agent_fax_t!~m/^\s*$/is)
		{
			$agent_fax_t_Flag=1;
		}
	}
	elsif($agent =~ m/>\s*([\d\s]+?)<>[\w\s\.\@\-]*?Mobile\s*Number/is)
	{
		$agent_mobile_t	= $1;
		$agent_mobile_t	=~ s/\s+/ /igs;
		# print "agent_mobile_t 		:: $agent_mobile_t\n";
	}
	
	if($agent_fax_t_Flag==0)
	{
		if($agent =~ m/Fax\s*number\s*\:\s*([\d\W]*?\s+[\d\W]*?\s+[\d\W]*?)\s*</is)
		{
			$agent_fax_t	= trim($1);
			$agent_fax_t	=~ s/\s+/ /igs;
			# $res.="\t<Applicant_Postcode>$value</Applicant_Postcode>\n";
			# print "agent_fax_t		:: $agent_fax_t\n";
		}
	}	
	if($agent_mobile eq '')
	{
		$agent_mobile=$agent_mobile_t;
	}
	if($agent_fax eq '')
	{
		$agent_fax=$agent_fax_t;
	}	
#### End		
	
	$agent_telephone=~s/^\s+//igs;
	$agent_mobile=~s/^\s+//igs;
	$agent_fax=~s/^\s+//igs;
	print "agent_telephone		:: $agent_telephone\n";
	print "agent_mobile 		:: $agent_mobile\n";
	print "agent_fax 		:: $agent_fax\n";
	print "agent_email 		:: $agent_email\n";
	
	##### ONEAPP PROPOSAL ##########
	if($content=~m/\d+\.\s*Description\s*of\s*the\s*Proposal[^<]*?change\s*of\s*use\:\s*(?:<>)?\s*([\w\W]*?)\s*Has\s*the/is)
	{
		$oneapp_proposal	= $1;
		$oneapp_proposal	=~ s/\s+/ /igs;
		$oneapp_proposal	=~ s/<>/ /igs;
	}
	elsif($content=~m/on\s*the\s*decision\s*letter\s*\:(?:<>)?\s*([\w\W]*?)Application\s*reference\s*number[\w\W]*?\s*Condition\s*number\(s\)\:\s*[\d]?\s*[a-z]?\s*\)?\s*([\w\W]*?)\s*Has\s*the\s*development\s*already\s*started/is)
	{
		$oneapp_proposal	= $1.$2;
		$oneapp_proposal	=~ s/\s+/ /igs;
		$oneapp_proposal	=~ s/<>/ /igs;
	}
	elsif($content=~m/demolish\s*the\s*listed\s*building\(s\)\s*\:\s*(?:<>)?\s*([\w\W]*?)\s*Has\s*the/is)
	{
		$oneapp_proposal	= $1;
		$oneapp_proposal	=~ s/\s+/ /igs;
	}
	elsif($content=~m/proposed\s*works\s*\:\s*(?:<>)?\s*([\w\W]*?)(?:<>)?\s*Has\s*the/is )
	{
		$oneapp_proposal	= $1;
		$oneapp_proposal	=~ s/\s+/ /igs;
		$oneapp_proposal	=~ s/<>/ /igs;
	}
	elsif($content=~m/\d+\.\s*(?:Description\s*[a-z\s]*?\s*Proposal|Type\s*of\s*Proposed|Description\s*of\s*Proposed\s*Works|[^>]*?Description\s*Of\s*Works)([\w\W]*?)\s*\|Split\|/is)
	{
		my $oneapp_proposal_Block= $1;
		$oneapp_proposal_Block=~s/(?:why|where|how|Are|does|Has)[^>]*?<>\s*No<>\s*Yes(?:<>)?//igs; # to remove yes or no Questions
		# open(wr,">Proposal_Block.htm");
		# print wr $oneapp_proposal_Block;
		# close wr;		
		if($oneapp_proposal_Block=~m/(?:Description|describe|Proposal|proposed)[^>]*?(?:Description|describe|Proposal|proposed)[^>]*?\:\s*(?:<>)?\s*([\w\W]*?)(?:<>)?\s*(?:has\s*the|how)/is)
		{
			$oneapp_proposal= $1;
		}
		elsif($oneapp_proposal_Block=~m/E\.\s*g\.[^>]*?\s*<>\s*([\w\W]*?)\s*$/is)
		{
			$oneapp_proposal= $1;
		}
		elsif($oneapp_proposal_Block=~m/If\s*Yes,\s*please[^>]*?\(include[\w\W]*?\)[^<]*?<>([\w\W]*?)(?:<>)?$/is)
		{
			$oneapp_proposal= $1;
		}
		elsif($oneapp_proposal_Block=~m/please\s*describe[^>]*?[\w\W]*?[^<]*?<>\s*([\w\W]*?)\s*(?:<>)?$/is)
		{
			$oneapp_proposal= $1;
		}
	}
	$oneapp_proposal=&trim($oneapp_proposal);
	$oneapp_proposal=~s/<>/ /igs;
	$oneapp_proposal=~s/\s\s+/ /igs;
	print "oneapp_proposal 	:: $oneapp_proposal\n";
	print "------------ Vehicle Parking ------------\n";
	
	if($content=~m/\d+\.\s*(?:Vehicle)?\s*Parking\s*([\w\W]*?)\s*\|Split\|\s*/is)
	{
		my $vehicle = $1;
		if($vehicle=~m/Cars\s*([\-\d\s<>]+)/is)
		{
			$vehicle_car	= $1;
			$vehicle_car	=~ s/<>//igs;
			$vehicle_car	=~ s/\s+/|/igs;
			$vehicle_car	=~ s/\s*\|\s*$//igs;
			$vehicle_car	=~ s/^\||\|$//igs;
			print "vehicle_car 		:: $vehicle_car\n";
		}
		if($vehicle=~m/Light\s*goods\s*vehicles\/public\s*carrier\s*vehicles\s*([\-\d\s<>]+)/is)
		{
			$vehicle_light_good	= $1;
			$vehicle_light_good	=~ s/<>//igs;
			$vehicle_light_good	=~ s/\s+/|/igs;
			$vehicle_light_good	=~ s/\s*\|\s*$//igs;
			$vehicle_light_good	=~ s/^\||\|$//igs;
			print "vehicle_light_good 	:: $vehicle_light_good\n";
		}
		if($vehicle=~m/Motorcycles\s*([\-\d\s<>]+)/is)
		{
			$vehicle_motorcycle	= $1;
			$vehicle_motorcycle	=~ s/<>//igs;
			$vehicle_motorcycle	=~ s/\s+/|/igs;
			$vehicle_motorcycle	=~ s/\s*\|\s*$//igs;
			$vehicle_motorcycle	=~ s/^\||\|$//igs;
			print "vehicle_motorcycle 	:: $vehicle_motorcycle\n";
		}
		if($vehicle=~m/Disability\s*spaces\s*([\-\d\s<>]+)/is)
		{
			$vehicle_disability	= $1;
			$vehicle_disability	=~ s/<>//igs;
			$vehicle_disability	=~ s/\s+/|/igs;
			$vehicle_disability	=~ s/\s*\|\s*$//igs;
			$vehicle_disability	=~ s/^\||\|$//igs;
			print "vehicle_disability 	:: $vehicle_disability\n";
		}
		if($vehicle=~m/Cycle\s*spaces\s*([\-\d\s<>]+)/is)
		{
			$vehicle_cycle	= $1;
			$vehicle_cycle	=~ s/<>//igs;
			$vehicle_cycle	=~ s/\s+/|/igs;
			$vehicle_cycle	=~ s/\s*\|\s*$//igs;
			$vehicle_cycle	=~ s/^\||\|$//igs;
			print "vehicle_cycle 		:: $vehicle_cycle\n";
		}
		# print "applicant :; $applicant \n";
		if($vehicle =~ m/Other\s*\(e\.g\.\s*Bus\)\s*([\-\d\s<>]+)/is)
		{
			$vehicle_other	= $1;
			$vehicle_other	=~ s/<>//igs;
			$vehicle_other	=~ s/\s+/|/igs;
			$vehicle_other	=~ s/\s*\|\s*$//igs;
			$vehicle_other	=~ s/^\||\|$//igs;
			print "vehicle_other 		:: $vehicle_other\n";
		}
		if($vehicle =~ m/description\s*of\s*Other\s*(?:<>)?\s*([^<]+)\s*</is)
		{
			$vehicle_other_desc	= $1;
			$vehicle_other_desc	=~ s/\s+/ /igs;
			print "vehicle_other_desc 	:: $vehicle_other_desc\n";
		}
	}
	if($content=~m/\d+\.\s*Materials\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Material=$1;
		$Material=~s/\-\s*description\:/-description1-description/igs;
		my @material_type;
		while($Material=~m/([a-zA-Z\ ]+?)\s*\-\s*description/igs)
		{
			my $type=$1;
			push(@material_type,$type);
		}
		my $count=0;
		my ($exist_details,$proposed_details);
		while($Material=~m/-\s*description([\w\W]*?)(?:\-description1|Are\s*you\s+supplying\s+additional\s+information)/igs)
		{
			my $material_details=$1;
			if($material_details=~m/existing\s+materials\s+and\s+finishes\:\s*([^<]*?)\s*Description\s*of\s*proposed/is)
			{
				$exist_details.=$material_type[$count]." Existing Details:".$1."|";
			}
			if($material_details=~m/Description\s*of\s*proposed\s+materials\s+and\s+finishes\:\s*([^<]*?)\s*<>/is)
			{
				$proposed_details.=$material_type[$count]." Proposed Details:".$1."|";
			}
			# print "exist_details		:: $exist_details\n";
			# print "proposed_details 	:: $proposed_details\n";
			$count++;
		}
		$exist_details=~s/\s*\|\s*$//igs;
		$proposed_details=~s/\s*\|\s*$//igs;
	}
	if($content=~m/\d+\.\s*[^\n]*?Non\-residential\s*Floorspace([\w\W]*?)\|Split\|\s*/is)
	{
		my $applicant=$1;
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Shops\s*Net\s*Tradable\s*Area\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Financial\s*and\s*professional\s*services\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Restaurants\s*and\s*cafes\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\ )]+)\s*<>\s*Drinking\s*establ?ishments\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant=~m/([\w\(\) ]+)\s*<>\s*Hot\s*food\s*takeaways\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Office\s*\(other\s*than\s*A2\)\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Research\s*and\s*development\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Light\s*industrial\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*General\s*industrial\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Storage\s*or\s*distribution\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\ )]+)\s*<>\s*Hotels\s*and\s*halls\s*of\s*residence\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Residential\s*institutions\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Non\-residential\s*institutions\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Assembly\s*and\s*leisure\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/([\w\(\) ]+)\s*<>\s*Please\s*Specify\s*([\d\.\s]+)/is)
		{
			my $value=$1.' '.$2;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
		if($applicant =~ m/\d\s*<>\s*Total\s*([\d\.\s]+)/is)
		{
			my $value=$1;
			$value=~s/\s+/|/igs;
			$value=~s/\s*\|\s*$//igs;
		}
	}
	if($content=~m/\d+\.\s*Employment([\w\W]*?)\|Split\|\s*/is)
	{
		my $Employment_Details=$1;
		if($Employment_Details=~m/Existing\s*employees\s*([\d\.\s<>]+)/is)
		{
			my $existing_employee=$1;
			$existing_employee=~s/<>//igs;
			$existing_employee=~s/\s+/|/igs;
			$existing_employee=~s/\s*\|\s*$//igs;
			$existing_employee=~s/^\||\|$//igs;
			print "Existing_employee 	:: $existing_employee\n";
		}
		if($Employment_Details=~m/Proposed\s*employees\s*([\d\.\s<>]+)/is)
		{
			my $proposed_employee=$1;
			$proposed_employee=~s/<>//igs;
			$proposed_employee=~s/\s+/|/igs;
			$proposed_employee=~s/\s*\|\s*$//igs;
			$proposed_employee=~s/^\||\|$//igs;
			print "Proposed_employee 	:: $proposed_employee\n";
		}
	}
	if($content=~m/\d+\.\s*Site\s*Area\s*([\w\W]*?)\|Split\|/is)
	{
		my $Site_Area_Content=$1;
		open(SA ">Site_Area_Content.html");
		print SA $Site_Area_Content;
		close SA;
		
		if($Site_Area_Content=~m/>\s*([^<]*?)<>\s*(hectares|sq\s*\.\s*metres)\s*/is)
		{
			$site_area=$1." ".$2;
			$site_area=~s/\s+/ /igs;
		}	
		elsif($Site_Area_Content=~m/>\s*([^<]*?)<>\s*([^>]*?)\s*(?:What|<>)/is)
		{
			$site_area=$2." ".$1;
			$site_area=~s/\s+/ /igs;
		}	
		print "Site_area 		:: $site_area\n";
	}	
	if($content=~m/\d+\.\s*Existing\s*Use\s*([\w\W]*?)\|Split\|/is)
	{
		my $Site_existing_use=$1;
		if($Site_existing_use=~m/describe[^>]*?current[^>]*?site\s*\:\s*<>\s*([\w\W]*?)<>/is)
		{
			$existing_use=$1;
			$existing_use=~s/<>/ /igs;
			$existing_use=~s/\s+/ /igs;
		}	
		print "Existing_use 		:: $existing_use\n";
	}	
	if($content=~m/\d+\.\s*Industrial\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Industrial=$1;
		my $details;
		if($Industrial=~m/on\s*site\s*\:\s*([^<]*?)\s*Is\s*the\s*proposal/is)
		{
			$details=$1;
			$details=~s/\s+/ /igs;
		}
		# print "Industrial 			:: $Industrial\n";
	}
	if($content=~m/\d+\.(?:[a-z]*?\s*-)?\s*Declaration\s*([\w\W]*?)\|Split\|\s*/is)
	{
		my $Declaration=$1;
		$Declaration=~s/\s+/ /igs;
		# print "Declaration :: $Declaration\n";
	}
	##################### Material ########################
	print "---------------------- Entering Material Section -----------------------\n";
	my $Materials_Content;
	if($content=~m/([\d]+)\.\s*Materials/is)
	{
		my $Material_Index=$1+1;
		if($content=~m/$1\.\s*Materials\s*([\w\W]*?)\|Split\|/is)
		{
			$Materials_Content=$1;
		}	
	}
	# $Materials_Content=~s/(\n\s*(?:Window|wall|External\s*doors|Internal\s*doors|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls|Other)[a-z\s]*?-\s*[a-z\s]*\s*description(?:\:)?)/|Description Split|$1/igs;	
	$Materials_Content=~s/([a-z\s]*\s*-\s*[a-z\s]*\s*description(?:\:|<>)?)/|Description Split|$1/igs;	
	$Materials_Content.='|Description Split|';
	# open(wr,">Material.html");
	# print wr $Materials_Content;
	# close wr;
	my $Materials_Content_For_Others=$Materials_Content;
	#### FOR ROOF METERIAL ####
	# if($content=~m/\n(roof(?:s)?)\s*\-\s*description\:\s*\n([^>]*?)\n(?:Window|wall|door|roof|Vehicle|Lighting|Boundary)/is)
	my $roof_content;
	if($Materials_Content=~m/(?:\s*<>)?\s*(roof(?:s)?)\s*\-\s*description\:(?:\s*<>)?\s*([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$roof_content=$2;
	}
	elsif($Materials_Content=~m/(roof(?:s)?)\s*\-\s*description\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$roof_content=$2;
	}
	elsif($Materials_Content=~m/(roof(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$roof_content=$2;
	}	
	# print "ROOF CONTENT:: $roof_content\n";
	# open(fh,">roof_content.html");
	# print fh "$roof_content";
	# close fh;
	$Materials_Content_For_Others=~s/(?:\s*<>)?\s*(?:Window|wall|External\s*doors|Internal\s*doors|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls|Other)[a-z\s]*?-\s*[a-z\s]*\s*description\:?(?:\s*<>)?\s*([\w\W]*?\|Description\s*Split\|)//igs;
	$Materials_Content_For_Others=~s/\|Description\s*Split\|//igs;
	while($roof_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(<|Description)/igs) ### Need to Change While Loop
	{
		$existing_roof_material=$existing_roof_material.'|'.$1;				
	}
	if($existing_roof_material=~m/^\s*$/is)
	{
		while($roof_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(<|$)/igs)
		{
			$existing_roof_material=$existing_roof_material.'|'.$1;				
		}
	}
	$existing_roof_material=~s/\s+/ /igs;
	$existing_roof_material=~s/^\s+|\s+$//igs;			
	$existing_roof_material=~s/^\||\|$//igs;			
		
	while($roof_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(<>|$)/igs)
	{
		$proposed_roof_material=$proposed_roof_material.'|'.$1;				
	}
	if($proposed_roof_material=~m/^\s*$/is)
	{
		while($roof_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|$)/igs)
		{
			$proposed_roof_material=$proposed_roof_material.'|'.$1;				
		}
	}
	$proposed_roof_material=~s/\s+/ /igs;
	$proposed_roof_material=~s/^\s+|\s+$//igs;			
	$proposed_roof_material=~s/^\||\|$//igs;
	print "Existing Roof Materials		:: $existing_roof_material\n";
	print "Proposed Roof Materials		:: $proposed_roof_material\n";
	
	#### FOR DOOR METERIAL ####

	my $door_content;
	while($Materials_Content=~m/(?:<>)?(door(?:s)?)\s*\-\s*description\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/igs)
	{		
		$door_content.=$2;
	}
	if($door_content=~m/^\s*$/is)
	{
		while($Materials_Content=~m/(?:<>)?\s*(?:Internal|External)\s*(door(?:s)?)\s*\-\s*(?:add\s*)?description\:?\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/igs)
		{		
			$door_content.=$2;
		}
	}	
	if($door_content=~m/^\s*$/is)
	{
		while($Materials_Content=~m/(door(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)<>(?:Window|wall|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls)/igs)
		{
			$door_content.=$2;
		}	
	}	
	# open(fh,">door_content.html");
	# print fh "$door_content";
	# close fh;
	### Existing ###
	while($door_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_doors_material=$existing_doors_material.'|'.$1;				
	}
	if($existing_doors_material=~m/^\s*$/is)
	{
		while($door_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_doors_material=$existing_doors_material.'|'.$1;				
		}
	}
	$existing_doors_material=~s/\s+/ /igs;
	$existing_doors_material=~s/^\s+|\s+$//igs;			
	$existing_doors_material=~s/^\||\|$//igs;
	#Proposed
	while($door_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_doors_material=$proposed_doors_material.'|'.$1;				
	}
	if($proposed_doors_material=~m/^\s*$/is)
	{
		while($door_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_doors_material=$proposed_doors_material.'|'.$1;				
		}
	}
	$proposed_doors_material=~s/\s+/ /igs;
	$proposed_doors_material=~s/^\s+|\s+$//igs;			
	$proposed_doors_material=~s/^\||\|$//igs;	

	print "Existing Door Materials		:: $existing_doors_material\n";
	print "Proposed Door Materials		:: $proposed_doors_material\n";
	
	#### FOR Windows METERIAL ####

	my $window_content;
	if($Materials_Content=~m/(?:<>)?(Window(?:s)?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$window_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(Windows?)\s*\-\s*[a-z\s]*?\s*description\s*\:?\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$window_content=$2;
	}
	elsif($Materials_Content=~m/(Window(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)<>(?:Window|wall|door|roof|Vehicle|Lighting|Boundary|External\s*Walls|Internal\s*Walls)/is)
	{
		$window_content=$2;
	}	
	# open(fh,">window_content.html");
	# print fh "$window_content";
	# close fh;
	#Existing
	while($window_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_window_material=$existing_window_material.'|'.$1;				
	}
	if($existing_window_material=~m/^\s*$/is)
	{
		while($window_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_window_material=$existing_window_material.'|'.$1;				
		}
	}
	$existing_window_material=~s/\s+/ /igs;
	$existing_window_material=~s/^\s+|\s+$//igs;			
	$existing_window_material=~s/^\||\|$//igs;	
	#Proposed 
	while($window_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_window_material=$proposed_window_material.'|'.$1;				
	}
	if($proposed_window_material=~m/^\s*$/is)
	{
		while($window_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_window_material=$proposed_window_material.'|'.$1;				
		}
	}
	$proposed_window_material=~s/\s+/ /igs;
	$proposed_window_material=~s/^\s+|\s+$//igs;			
	$proposed_window_material=~s/^\||\|$//igs;	

	print "Existing Window Materials	:: $existing_window_material\n";
	print "Proposed Window Materials	:: $proposed_window_material\n";
	
	#### FOR Wall METERIAL ####
	my $wall_content;
	if($Materials_Content=~m/(?:<>)?(wall(?:s)?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$wall_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(wall(?:s)?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$wall_content=$2;
	}
	elsif($Materials_Content=~m/(wall(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$wall_content=$2;
	}
	# open(fh,">wall_content.html");
	# print fh "$wall_content";
	# close fh;
	#Existing 
	while($wall_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_wall_material=$existing_wall_material.'|'.$1;				
	}
	if($existing_wall_material=~m/^\s*$/is)
	{
		while($wall_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_wall_material=$existing_wall_material.'|'.$1;				
		}
	}
	$existing_wall_material=~s/\s+/ /igs;
	$existing_wall_material=~s/^\s+|\s+$//igs;			
	$existing_wall_material=~s/^\||\|$//igs;			
		### Proposed ###
	while($wall_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_wall_material=$proposed_wall_material.'|'.$1;				
	}
	if($proposed_wall_material=~m/^\s*$/is)
	{
		while($wall_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_wall_material=$proposed_wall_material.'|'.$1;				
		}
	}
	$proposed_wall_material=~s/\s+/ /igs;
	$proposed_wall_material=~s/^\s+|\s+$//igs;			
	$proposed_wall_material=~s/^\||\|$//igs;		

	print "Existing Wall Materials		:: $existing_wall_material\n";
	print "Proposed Wall Materials		:: $proposed_wall_material\n";
	
	#### FOR Boundary METERIAL ####
	my $boundary_content;
	if($Materials_Content=~m/(?:<>)?(boundary(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?\s*([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$boundary_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(boundary(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$boundary_content=$2;
	}
	elsif($Materials_Content=~m/(boundary(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$boundary_content=$2;
	}	
	# open(fh,">boundary_content.html");
	# print fh "$boundary_content";
	# close fh;
	
	#Existing ###
	# while($boundary_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*<(Description)/igs)
	while($boundary_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*</igs)
	{
		$existing_boundary_treatment_material=$existing_boundary_treatment_material.'|'.$1;
	}
	if($existing_boundary_treatment_material=~m/^\s*$/is)
	{
		while($boundary_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|$)/igs)
		{
			$existing_boundary_treatment_material=$existing_boundary_treatment_material.'|'.$1;
		}
	}
	$existing_boundary_treatment_material=~s/\s+/ /igs;	
	$existing_boundary_treatment_material=~s/^\s+|\s+$//igs;			
	$existing_boundary_treatment_material=~s/^\||\|$//igs;
		### Proposed ###
	while($boundary_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_boundary_treatment_material=$proposed_boundary_treatment_material.'|'.$1;
	}
	if($proposed_boundary_treatment_material=~m/^\s*$/is)
	{
		while($boundary_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_boundary_treatment_material=$proposed_boundary_treatment_material.'|'.$1;
		}
	}
	$proposed_boundary_treatment_material=~s/\s+/ /igs;	
	$proposed_boundary_treatment_material=~s/^\s+|\s+$//igs;			
	$proposed_boundary_treatment_material=~s/^\||\|$//igs;			

	print "Existing Boundry Materials	:: $existing_boundary_treatment_material\n";
	print "Proposed Boundry Materials	:: $proposed_boundary_treatment_material\n";
	
	#### FOR VEHICLE METERIAL ####
	my $Vehicle_content;
	if($Materials_Content=~m/(?:<>)?(Vehicle(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Vehicle_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(Vehicle(?:s)?[a-z\s]*?)\s*\-\s*description\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Vehicle_content=$2;
	}
	elsif($Materials_Content=~m/(Vehicle(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$Vehicle_content=$2;
	}
	# open(fh,">Vehicle_content.html");
	# print fh "$Vehicle_content";
	# close fh;
	#Existing
	while($Vehicle_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_vehicle_access_material=$existing_vehicle_access_material.'|'.$1;
	}
	if($existing_vehicle_access_material=~m/^\s*$/is)
	{
		while($Vehicle_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_vehicle_access_material=$existing_vehicle_access_material.'|'.$1;
		}
	}
	$existing_vehicle_access_material=~s/\s+/ /igs;	
	$existing_vehicle_access_material=~s/^\s+|\s+$//igs;			
	$existing_vehicle_access_material=~s/^\||\|$//igs;
	### Proposed ###
	while($Vehicle_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_vehicle_access_material=$proposed_vehicle_access_material.'|'.$1;
	}
	if($proposed_vehicle_access_material=~m/^\s*$/is)
	{
		while($Vehicle_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_vehicle_access_material=$proposed_vehicle_access_material.'|'.$1;
		}
	}
	$proposed_vehicle_access_material=~s/\s+/ /igs;	
	$proposed_vehicle_access_material=~s/^\s+|\s+$//igs;			
	$proposed_vehicle_access_material=~s/^\||\|$//igs;			
	print "Existing Vehicle Materials	:: $existing_vehicle_access_material\n";
	print "Proposed Vehicle Materials	:: $proposed_vehicle_access_material\n";
	
	#### FOR Lighting METERIAL ####
	my $Lighting_content;
	if($Materials_Content=~m/(?:<>)?(Lighting(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Lighting_content=$2;
	}
	elsif($Materials_Content=~m/(?:<>)?(Lighting(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Lighting_content=$2;
	}
	elsif($Materials_Content=~m/(Lighting(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$Lighting_content=$2;
	}	
	# open(fh,">Lighting_content.html");
	# print fh "$Lighting_content";
	# close fh;
	# print "Lighting Content:: $Lighting_content\n";

		### Existing ###
	while($Lighting_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_lightning_material=$existing_lightning_material.'|'.$1;
	}
	if($existing_lightning_material=~m/^\s*$/is)
	{
		while($Lighting_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_lightning_material=$existing_lightning_material.'|'.$1;
		}
	}
	$existing_lightning_material=~s/\s+/ /igs;	
	$existing_lightning_material=~s/^\s+|\s+$//igs;			
	$existing_lightning_material=~s/^\||\|$//igs;
		### Proposed ###
	while($Lighting_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\s*\:\s*<>\s*([^>]*?)\s*(?:<>|$)/igs)
	{
		$proposed_lighning_material=$proposed_lighning_material.'|'.$1;
	}
	if($proposed_lighning_material=~m/^\s*$/is)
	{
		while($Lighting_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_lighning_material=$proposed_lighning_material.'|'.$1;
		}
	}
	$proposed_lighning_material=~s/\s+/ /igs;	
	$proposed_lighning_material=~s/^\s+|\s+$//igs;			
	$proposed_lighning_material=~s/^\||\|$//igs;			
	print "Existing lighning Materials	:: $existing_lightning_material\n";
	print "Proposed lighning Materials	:: $proposed_lighning_material\n";
	
	#### FOR OTHER METERIAL ####
	my $Other_content;
	if($Materials_Content=~m/(?:<>)?(Other(?:s)?[a-z\s]*?)\s*\-\s*description\s*\:\s*(?:<>)?([\w\W]*?)\|Description\s*Split\|/is)
	{		
		$Other_content=$2;
	}
	elsif($Materials_Content=~m/(Other(?:s)?)[^>]*?\-\s*add\s*description([\w\W]*?)\|Description\s*Split\|/is)
	{
		$Other_content=$2;
	}	
	# open(fh,">Other_content.html");
	# print fh "$Other_content";
	# close fh;
	# print "Other Content:: $Other_content\n";

		### Existing ###
	while($Other_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_others_materials=$existing_others_materials.'|'.$1;
	}
	if($existing_others_materials=~m/^\s*$/is)
	{
		while($Other_content=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_others_materials=$existing_others_materials.'|'.$1;
		}
	}
	$existing_others_materials=~s/\s+/ /igs;	
	$existing_others_materials=~s/^\s+|\s+$//igs;			
	$existing_others_materials=~s/^\||\|$//igs;
		### Proposed ###
	while($Other_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\s*\:\s*<>\s*([\w\W]*?)\s*(?:Are|$)/igs)
	{
		$proposed_others_materials=$proposed_others_materials.'|'.$1;
	}
	if($proposed_others_materials=~m/^\s*$/is)
	{
		while($Other_content=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$proposed_others_materials=$proposed_others_materials.'|'.$1;
		}
	}

	# open(fh,">Materials_Content_For_Others.html");
	# print fh "$Materials_Content_For_Others";
	# close fh;
	
	### Other Materials ###
	while($Materials_Content_For_Others=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description)/igs)
	{
		$existing_others_materials=$existing_others_materials.'|'.$1;
	}
	if($existing_others_materials=~m/^\s*$/is)
	{
		while($Materials_Content_For_Others=~m/Description\s*of\s*existing\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|$)/igs)
		{
			$existing_others_materials=$existing_others_materials.'|'.$1;
		}
	}
	$existing_others_materials=~s/<>/ /igs;			
	$existing_others_materials=~s/\s+/ /igs;	
	$existing_others_materials=~s/^\s+|\s+$//igs;			
	$existing_others_materials=~s/^\s*\|+|\|+\s*$//igs;
		### Proposed ###
	while($Materials_Content_For_Others=~m/Description\s*of\s*proposed\s*materials[^>]*?\s*\:\s*<>\s*([\w\W]*?)\s*(?:Are|<)/igs)
	{
		$proposed_others_materials=$proposed_others_materials.'|'.$1;
	}
	if($proposed_others_materials=~m/^\s*$/is)
	{
		while($Materials_Content_For_Others=~m/Description\s*of\s*proposed\s*materials[^>]*?\:\s*<>\s*((?!Description[^<]*?)[^<]*?)\s*(?:<|Description|<)/igs)
		{
			$proposed_others_materials=$proposed_others_materials.'|'.$1;
		}
	}

	$proposed_others_materials=~s/<>/ /igs;			
	$proposed_others_materials=~s/\s+/ /igs;	
	$proposed_others_materials=~s/^\s+|\s+$//igs;			
	$proposed_others_materials=~s/^\s*\|+|\|+\s*$//igs;		

	print "Existing Other Materials	:: $existing_others_materials\n";
	print "Proposed Other Materials	:: $proposed_others_materials\n";
	
	#### Residential Units
	if($content=~m/\d+\.\s*Residential\s*Units\s*([\w\W]*?)\|Split\|/is)
	{		
		my $Residential_Units=$1;
		# open(RU,">Residential_Units.html");
		# print RU $Residential_Units;
		# close RU;
		while($Residential_Units=~m/Housing\s*-\s*Proposed(?:<>)?([\w\W]*?)\s*Housing\s*-\s*Existing/igs)
		{
			my $proposed_houses_content=$1;
			if($proposed_houses_content=~m/Houses\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_houses=$proposed_houses.'|'.$1;
				$proposed_houses=&Tag_Clean($proposed_houses);
			}
			if($proposed_houses_content=~m/Flats\/Maisonettes\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_flats_maisonettes=$proposed_flats_maisonettes.'|'.$1;
				$proposed_flats_maisonettes=&Tag_Clean($proposed_flats_maisonettes);
			}
			if($proposed_houses_content=~m/Live\s*\-\s*Work\s*units\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_live_work_units=$proposed_live_work_units.'|'.$1;
				$proposed_live_work_units=&Tag_Clean($proposed_live_work_units);
			}
			if($proposed_houses_content=~m/Cluster\s*flats\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_cluster_flats=$proposed_cluster_flats.'|'.$1;
				$proposed_cluster_flats=&Tag_Clean($proposed_cluster_flats);
			}
			if($proposed_houses_content=~m/Sheltered\s*housing\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_sheltered_housing=$proposed_sheltered_housing.'|'.$1;
				$proposed_sheltered_housing=&Tag_Clean($proposed_sheltered_housing);
			}
			if($proposed_houses_content=~m/Bedsit\s*\/\s*Studios\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_bedsit_studios=$proposed_bedsit_studios.'|'.$1;
				$proposed_bedsit_studios=&Tag_Clean($proposed_bedsit_studios);
			}
			if($proposed_houses_content=~m/Unknown\s*<>\s*([\d\.\s<>]+)/is)
			{
				$proposed_unknown=$proposed_unknown.'|'.$1;
				$proposed_unknown=&Tag_Clean($proposed_unknown);
			}
			if($proposed_houses_content=~m/Proposed\s*Market\s*Housing\s*Total\s*(?:<>\s*)?([\d]+)\s*/is)
			{
				$proposed_market_housing_total=$proposed_market_housing_total.'|'.$1;
				$proposed_market_housing_total=&Tag_Clean($proposed_market_housing_total);
			}
		}
		while($Residential_Units=~m/Housing\s*-\s*Existing(?:<>)?([\w\W]*?)(?:Overall\s*Residential|$)/igs)
		{
			my $Existing_houses_content=$1;
			if($Existing_houses_content=~m/Houses\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_houses=$existing_houses.'|'.$1;
				$existing_houses=&Tag_Clean($existing_houses);
			}
			if($Existing_houses_content=~m/Flats\/Maisonettes\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_flats_maisonettes=$existing_flats_maisonettes.'|'.$1;
				$existing_flats_maisonettes=&Tag_Clean($existing_flats_maisonettes);
			}
			if($Existing_houses_content=~m/Live\s*\-\s*Work\s*units\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_live_work_units=$existing_live_work_units.'|'.$1;
				$existing_live_work_units=&Tag_Clean($existing_live_work_units);
			}
			if($Existing_houses_content=~m/Cluster\s*flats\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_cluster_flats=$existing_cluster_flats.'|'.$1;
				$existing_cluster_flats=&Tag_Clean($existing_cluster_flats);
			}
			if($Existing_houses_content=~m/Sheltered\s*housing\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_sheltered_housing=$existing_sheltered_housing.'|'.$1;
				$existing_sheltered_housing=&Tag_Clean($existing_sheltered_housing);
			}
			if($Existing_houses_content=~m/Bedsit\s*\/\s*Studios\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_bedsit_studios=$existing_bedsit_studios.'|'.$1;
				$existing_bedsit_studios=&Tag_Clean($existing_bedsit_studios);
			}
			if($Existing_houses_content=~m/Unknown\s*<>\s*([\d\.\s<>]+)/is)
			{
				$existing_unknown=$existing_unknown.'|'.$1;
				$existing_unknown=&Tag_Clean($existing_unknown);
			}
			if($Existing_houses_content=~m/Existing\s*Market\s*Housing\s*Total\s*(?:<>\s*)?([\d]+)\s*/is)
			{
				$existing_market_housing_total=$existing_market_housing_total.'|'.$1;
				$existing_market_housing_total=&Tag_Clean($existing_market_housing_total);
			}
		}
		if($Residential_Units=~m/Overall\s*Residential[^<]*?(?:<>)?([\w\W]*?)$/is)
		{	
			my $Overall_Residential=$1;
			if($Overall_Residential=~m/Total\s*proposed\s*residential\s*units\s*(?:<>\s*)?([\d]+)\s*<>/is)
			{
				$total_proposed_residential_units=$1;
			}
			if($Overall_Residential=~m/Total\s*existing\s*residential\s*units\s*(?:<>\s*)?([\d]+)\s*<>/is)
			{
				$total_existing_residential_units=$1;
			}
		}
	}
	if($applicant_telephone=~m/[a-z]/is){$applicant_telephone='';}
	if($applicant_mobile=~m/[a-z]/is){$applicant_mobile='';}
	if($applicant_fax=~m/[a-z]/is){$applicant_fax='';}
	if($agent_telephone=~m/[a-z]/is){$agent_telephone='';}
	if($agent_mobile=~m/[a-z]/is){$agent_mobile='';}
	if($agent_fax=~m/[a-z]/is){$agent_fax='';}
	
	print "------------ Residential Units ------------\n";
	print "proposed_houses			:: $proposed_houses\n";
	print "proposed_flats_maisonettes	:: $proposed_flats_maisonettes\n";
	print "proposed_live_work_units	:: $proposed_live_work_units\n";
	print "proposed_cluster_flats		:: $proposed_cluster_flats\n";
	print "proposed_sheltered_housing	:: $proposed_sheltered_housing\n";
	print "proposed_bedsit_studios		:: $proposed_bedsit_studios\n";
	print "proposed_unknown		:: $proposed_unknown\n";
	print "proposed_market_housing_total	:: $proposed_market_housing_total\n";
	print "existing_houses			:: $existing_houses\n";
	print "existing_flats_maisonettes	:: $existing_flats_maisonettes\n";
	print "existing_live_work_units	:: $existing_live_work_units\n";
	print "existing_cluster_flats		:: $existing_cluster_flats\n";
	print "existing_sheltered_housing	:: $existing_sheltered_housing\n";
	print "existing_bedsit_studios		:: $existing_bedsit_studios\n";
	print "existing_unknown		:: $existing_unknown\n";
	print "existing_market_housing_total	:: $existing_market_housing_total\n";
	print "total_proposed_residential_units:: $total_proposed_residential_units\n";
	print "total_existing_residential_units:: $total_existing_residential_units\n";

	#### Non-residential Floorspace
	if($content=~m/\d+\.\s*[a-z\:\s]*?Non\s*-?\s*residential\s*([\w\W]*?)\|Split\|\s*/is)
	{	
		my $Non_Residential_Units=$1;
		# open(FL,">Floor_Data.html");
		# print FL $Non_Residential_Units;
		# close FL;

		if($Non_Residential_Units=~m/Shops\s*Net\s*Tradable\s*Area\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_shops=$1;
			$non_residential_shops=&Tag_Clean($non_residential_shops);
		}
		if($Non_Residential_Units=~m/Financial\s*and\s*professional\s*services\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_financial=$1;
			$non_residential_financial=&Tag_Clean($non_residential_financial);
		}
		if($Non_Residential_Units=~m/Restaurants\s*and\s*cafes\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_restaurants=$1;
			$non_residential_restaurants=&Tag_Clean($non_residential_restaurants);
		}
		if($Non_Residential_Units=~m/Drinking\s*establ?ishments\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_drinking=$1;
			$non_residential_drinking=&Tag_Clean($non_residential_drinking);
		}
		if($Non_Residential_Units=~m/Hot\s*food\s*takeaways\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_food=$1;
			$non_residential_food=&Tag_Clean($non_residential_food);
		}
		if($Non_Residential_Units=~m/Office\s*[\w\W]*?<>\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_office=$1;
			$non_residential_office=&Tag_Clean($non_residential_office);
		}
		if($Non_Residential_Units=~m/Research\s*and\s*development\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_research=$1;
			$non_residential_research=&Tag_Clean($non_residential_research);
		}
		if($Non_Residential_Units=~m/Light\s*industrial\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_light=$1;
			$non_residential_light=&Tag_Clean($non_residential_light);
		}
		if($Non_Residential_Units=~m/General\s*industrial\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_general=$1;
			$non_residential_general=&Tag_Clean($non_residential_general);
		}
		if($Non_Residential_Units=~m/Storage\s*or\s*distribution\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_storage=$1;
			$non_residential_storage=&Tag_Clean($non_residential_storage);
		}
		if($Non_Residential_Units=~m/Hotels\s*and\s*halls\s*of\s*residence\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_hotels=$1;
			$non_residential_hotels=&Tag_Clean($non_residential_hotels);
		}
		if($Non_Residential_Units=~m/Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_institutions=$1;
			$non_residential_institutions=&Tag_Clean($non_residential_institutions);
		}
		$Non_Residential_Units=~s/Non\-residential\s*institutions\s*([\-\d\s\.\,<>]+)//igs;
		if($Non_Residential_Units=~m/residential\s*institutions\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_residential=$1;
			$non_residential_residential=&Tag_Clean($non_residential_residential);
		}
		if($Non_Residential_Units=~m/Assembly\s*and\s*leisure\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_assembly=$1;
			$non_residential_assembly=&Tag_Clean($non_residential_assembly);
		}
		if($Non_Residential_Units=~m/Please\s*Specify\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_other=$1;
			$non_residential_other=&Tag_Clean($non_residential_other);
		}
		if($Non_Residential_Units=~m/Total\s*<>\s*([\-\d\s\.\,<>]+)/is)
		{
			$non_residential_total=$1;
			$non_residential_total=&Tag_Clean($non_residential_total);
		}
	}	
	print "non_residential_shops		:: $non_residential_shops\n";
	print "non_residential_financial	:: $non_residential_financial\n";
	print "non_residential_restaurants	:: $non_residential_restaurants\n";
	print "non_residential_drinking	:: $non_residential_drinking\n";
	print "non_residential_food		:: $non_residential_food\n";
	print "non_residential_office		:: $non_residential_office\n";
	print "non_residential_research	:: $non_residential_research\n";
	print "non_residential_light		:: $non_residential_light\n";
	print "non_residential_general		:: $non_residential_general\n";
	print "non_residential_storage		:: $non_residential_storage\n";
	print "non_residential_hotels		:: $non_residential_hotels\n";
	print "non_residential_residential	:: $non_residential_residential\n";
	print "non_residential_institutions	:: $non_residential_institutions\n";
	print "non_residential_assembly	:: $non_residential_assembly\n";
	print "non_residential_other		:: $non_residential_other\n";
	print "non_residential_total		:: $non_residential_total\n";
	
	print "DB Insert Happens..\n";
	
	$applicant_telephone='' if($applicant_telephone=~m/[a-z]+/is);
	$applicant_mobile='' if($applicant_mobile=~m/[a-z]+/is);
	$applicant_fax='' if($applicant_fax=~m/[a-z]+/is);
	$agent_telephone='' if($agent_telephone=~m/[a-z]+/is);
	$agent_mobile='' if($agent_mobile=~m/[a-z]+/is);
	$agent_fax='' if($agent_fax=~m/[a-z]+/is);
	
	$Source =&clean11($Source);
	$Format_ID =&clean11($Format_ID);
	$Application_No =&clean11($Application_No);
	$councilcode =&clean11($councilcode);
	$Markup =&clean11($Markup);
	$site_house =&clean11($site_house);
	$site_suffix =&clean11($site_suffix);
	$site_house_name =&clean11($site_house_name);
	$site_street =&clean11($site_street);
	$site_city =&clean11($site_city);
	$Site_County =&clean11($Site_County);
	$site_country =&clean11($site_country);
	$site_postcode =&clean11($site_postcode);
	$easting =&clean11($easting);
	$northing =&clean11($northing);
	$applicant_title =&clean11($applicant_title);
	$applicant_first_name =&clean11($applicant_first_name);
	$applicant_surname =&clean11($applicant_surname);
	$applicant_company_name =&clean11($applicant_company_name);
	$applicant_street =&clean11($applicant_street);
	$applicant_town =&clean11($applicant_town);
	$applicant_county =&clean11($applicant_county);
	$applicant_country =&clean11($applicant_country);
	$applicant_postcode =&clean11($applicant_postcode);
	$applicant_telephone =&clean11($applicant_telephone);
	$applicant_mobile =&clean11($applicant_mobile);
	$applicant_fax =&clean11($applicant_fax);
	$applicant_email =&clean11($applicant_email);
	$agent_title =&clean11($agent_title);
	$agent_first_name =&clean11($agent_first_name);
	$agent_surname =&clean11($agent_surname);
	$agent_company_name =&clean11($agent_company_name);
	$agent_street =&clean11($agent_street);
	$agent_town =&clean11($agent_town);
	$agent_county =&clean11($agent_county);
	$agent_country =&clean11($agent_country);
	$agent_postcode =&clean11($agent_postcode);
	$agent_telephone =&clean11($agent_telephone);
	$agent_mobile =&clean11($agent_mobile);
	$agent_fax =&clean11($agent_fax);
	$agent_email =&clean11($agent_email);
	$oneapp_proposal =&clean11($oneapp_proposal);
	$existing_wall_material =&clean11($existing_wall_material);
	$proposed_wall_material =&clean11($proposed_wall_material);
	$existing_roof_material =&clean11($existing_roof_material);
	$proposed_roof_material =&clean11($proposed_roof_material);
	$existing_window_material =&clean11($existing_window_material);
	$proposed_window_material =&clean11($proposed_window_material);
	$existing_doors_material =&clean11($existing_doors_material);
	$proposed_doors_material =&clean11($proposed_doors_material);
	$existing_boundary_treatment_material =&clean11($existing_boundary_treatment_material);
	$proposed_boundary_treatment_material =&clean11($proposed_boundary_treatment_material);
	$existing_vehicle_access_material =&clean11($existing_vehicle_access_material);
	$proposed_vehicle_access_material =&clean11($proposed_vehicle_access_material);
	$existing_lightning_material =&clean11($existing_lightning_material);
	$proposed_lighning_material =&clean11($proposed_lighning_material);
	$existing_others_materials =&clean11($existing_others_materials);
	$proposed_others_materials =&clean11($proposed_others_materials);
	$material_additional_information =&clean11($material_additional_information);
	$material_additional_info_details =&clean11($material_additional_info_details);
	$vehicle_car =&clean11($vehicle_car);
	$vehicle_light_good =&clean11($vehicle_light_good);
	$vehicle_motorcycle =&clean11($vehicle_motorcycle);
	$vehicle_disability =&clean11($vehicle_disability);
	$vehicle_cycle =&clean11($vehicle_cycle);
	$vehicle_other =&clean11($vehicle_other);
	$vehicle_other_desc =&clean11($vehicle_other_desc);
	$existing_use =&clean11($existing_use);
	$existing_residential_house =&clean11($existing_residential_house);
	$proposed_houses =&clean11($proposed_houses);
	$proposed_flats_maisonettes =&clean11($proposed_flats_maisonettes);
	$proposed_live_work_units =&clean11($proposed_live_work_units);
	$proposed_cluster_flats =&clean11($proposed_cluster_flats);
	$proposed_sheltered_housing =&clean11($proposed_sheltered_housing);
	$proposed_bedsit_studios =&clean11($proposed_bedsit_studios);
	$proposed_unknown =&clean11($proposed_unknown);
	$proposed_market_housing_total =&clean11($proposed_market_housing_total);
	$existing_houses =&clean11($existing_houses);
	$existing_flats_maisonettes =&clean11($existing_flats_maisonettes);
	$existing_live_work_units =&clean11($existing_live_work_units);
	$existing_cluster_flats =&clean11($existing_cluster_flats);
	$existing_sheltered_housing =&clean11($existing_sheltered_housing);
	$existing_bedsit_studios =&clean11($existing_bedsit_studios);
	$existing_unknown =&clean11($existing_unknown);
	$existing_market_housing_total =&clean11($existing_market_housing_total);
	$total_proposed_residential_units =&clean11($total_proposed_residential_units);
	$total_existing_residential_units =&clean11($total_existing_residential_units);
	$non_residential_shops =&clean11($non_residential_shops);
	$non_residential_financial =&clean11($non_residential_financial);
	$non_residential_restaurants =&clean11($non_residential_restaurants);
	$non_residential_drinking =&clean11($non_residential_drinking);
	$non_residential_food =&clean11($non_residential_food);
	$non_residential_office =&clean11($non_residential_office);
	$non_residential_research =&clean11($non_residential_research);
	$non_residential_light =&clean11($non_residential_light);
	$non_residential_general =&clean11($non_residential_general);
	$non_residential_storage =&clean11($non_residential_storage);
	$non_residential_hotels =&clean11($non_residential_hotels);
	$non_residential_residential =&clean11($non_residential_residential);
	$non_residential_institutions =&clean11($non_residential_institutions);
	$non_residential_assembly =&clean11($non_residential_assembly);
	$non_residential_other =&clean11($non_residential_other);
	$non_residential_total =&clean11($non_residential_total);
	$existing_employee =&clean11($existing_employee);
	$proposed_employee =&clean11($proposed_employee);
	$site_area =&clean11($site_area);

	&DB_OneApp::SaveFormatPdf($dbh,$Source,$Format_ID,$Application_No,$councilcode,$Markup,$site_house,$site_suffix,$site_house_name,$site_street,$site_city,$Site_County,$site_country,$site_postcode,$easting,$northing,$applicant_title,$applicant_first_name,$applicant_surname,$applicant_company_name,$applicant_street,$applicant_town,$applicant_county,$applicant_country,$applicant_postcode,$applicant_telephone,$applicant_mobile,$applicant_fax,$applicant_email,$agent_title,$agent_first_name,$agent_surname,$agent_company_name,$agent_street,$agent_town,$agent_county,$agent_country,$agent_postcode,$agent_telephone,$agent_mobile,$agent_fax,$agent_email,$oneapp_proposal,$existing_wall_material,$proposed_wall_material,$existing_roof_material,$proposed_roof_material,$existing_window_material,$proposed_window_material,$existing_doors_material,$proposed_doors_material,$existing_boundary_treatment_material,$proposed_boundary_treatment_material,$existing_vehicle_access_material,$proposed_vehicle_access_material,$existing_lightning_material,$proposed_lighning_material,$existing_others_materials,$proposed_others_materials,$material_additional_information,$material_additional_info_details,$vehicle_car,$vehicle_light_good,$vehicle_motorcycle,$vehicle_disability,$vehicle_cycle,$vehicle_other,$vehicle_other_desc,$existing_use,$existing_residential_house,$proposed_houses,$proposed_flats_maisonettes,$proposed_live_work_units,$proposed_cluster_flats,$proposed_sheltered_housing,$proposed_bedsit_studios,$proposed_unknown,$proposed_market_housing_total,$existing_houses,$existing_flats_maisonettes,$existing_live_work_units,$existing_cluster_flats,$existing_sheltered_housing,$existing_bedsit_studios,$existing_unknown,$existing_market_housing_total,$total_proposed_residential_units,$total_existing_residential_units,$non_residential_shops,$non_residential_financial,$non_residential_restaurants,$non_residential_drinking,$non_residential_food,$non_residential_office,$non_residential_research,$non_residential_light,$non_residential_general,$non_residential_storage,$non_residential_hotels,$non_residential_residential,$non_residential_institutions,$non_residential_assembly,$non_residential_other,$non_residential_total,$existing_employee,$proposed_employee,$site_area);

	# my $SaveFormatPdf="(\'$Source\',\'$Format_ID\',\'$Application_No\',\'$councilcode\',\'$Markup\',\'$site_house\',\'$site_suffix\',\'$site_house_name\',\'$site_street\',\'$site_city\',\'$Site_County\',\'$site_country\',\'$site_postcode\',\'$easting\',\'$northing\',\'$applicant_title\',\'$applicant_first_name\',\'$applicant_surname\',\'$applicant_company_name\',\'$applicant_street\',\'$applicant_town\',\'$applicant_county\',\'$applicant_country\',\'$applicant_postcode\',\'$applicant_telephone\',\'$applicant_mobile\',\'$applicant_fax\',\'$applicant_email\',\'$agent_title\',\'$agent_first_name\',\'$agent_surname\',\'$agent_company_name\',\'$agent_street\',\'$agent_town\',\'$agent_county\',\'$agent_country\',\'$agent_postcode\',\'$agent_telephone\',\'$agent_mobile\',\'$agent_fax\',\'$agent_email\',\'$oneapp_proposal\',\'$existing_wall_material\',\'$proposed_wall_material\',\'$existing_roof_material\',\'$proposed_roof_material\',\'$existing_window_material\',\'$proposed_window_material\',\'$existing_doors_material\',\'$proposed_doors_material\',\'$existing_boundary_treatment_material\',\'$proposed_boundary_treatment_material\',\'$existing_vehicle_access_material\',\'$proposed_vehicle_access_material\',\'$existing_lightning_material\',\'$proposed_lighning_material\',\'$existing_others_materials\',\'$proposed_others_materials\',\'$material_additional_information\',\'$material_additional_info_details\',\'$vehicle_car\',\'$vehicle_light_good\',\'$vehicle_motorcycle\',\'$vehicle_disability\',\'$vehicle_cycle\',\'$vehicle_other\',\'$vehicle_other_desc\',\'$existing_use\',\'$existing_residential_house\',\'$proposed_houses\',\'$proposed_flats_maisonettes\',\'$proposed_live_work_units\',\'$proposed_cluster_flats\',\'$proposed_sheltered_housing\',\'$proposed_bedsit_studios\',\'$proposed_unknown\',\'$proposed_market_housing_total\',\'$existing_houses\',\'$existing_flats_maisonettes\',\'$existing_live_work_units\',\'$existing_cluster_flats\',\'$existing_sheltered_housing\',\'$existing_bedsit_studios\',\'$existing_unknown\',\'$existing_market_housing_total\',\'$total_proposed_residential_units\',\'$total_existing_residential_units\',\'$non_residential_shops\',\'$non_residential_financial\',\'$non_residential_restaurants\',\'$non_residential_drinking\',\'$non_residential_food\',\'$non_residential_office\',\'$non_residential_research\',\'$non_residential_light\',\'$non_residential_general\',\'$non_residential_storage\',\'$non_residential_hotels\',\'$non_residential_residential\',\'$non_residential_institutions\',\'$non_residential_assembly\',\'$non_residential_other\',\'$non_residential_total\',\'$existing_employee\',\'$proposed_employee\',\'$site_area\'),";

	my $query = "update FORMAT_PUBLIC_ACCESS set Application_Extracted = 'Y' where id = $Format_ID and council_code = $councilcode";
	&DB_OneApp::UpdateDB($dbh,$query);
	# return ($SaveFormatPdf);
	create_xml_last:
}
sub trim
{
	my $txt = shift;
	
	$txt =~ s/<>/ /g;
	$txt =~ s/\s\s+/ /g;
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
sub clean11()
{
	my $value1=shift;
	$value1=~s/(\s*<>\s*)+//igs;
	$value1=~s/^\s*|\s*$//igs;
	$value1=~s/\s+/ /igs;
	$value1=~s/\'/''/igs;
	return($value1);
}
sub Tag_Clean
{
	my $value=shift;
	$value=~s/\s*<>\s*/\|/igs;
	$value=~s/\s*\|\s*$//igs;
	$value=~s/^\|+|\|+$//igs;
	return($value);
}
