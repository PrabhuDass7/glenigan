package Download_DB;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);


###### DB Connection ####
sub DbConnection()
{
	# Test DB
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.183;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# Live DB
	my $dsn1 ='driver={SQL Server};Server=10.101.53.25;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}

sub Retrieve_Input()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $inputFormatID	= shift;
	
	print "Fetching council code $council_code......\n";
	
	
	my $query;
	if($inputFormatID=~m/^\d+$/is)
	{
		print "FormatID:=> $inputFormatID\n";

		$query = "select id,SOURCE ,Application_No ,Document_Url ,MARKUP ,URL, No_of_Documents,
		case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME 
		from ( 
		select ID,Source,Application_No,Document_Url,F.MARKUP,URL,No_of_Documents, 
		isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
		isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
		isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME
		from FORMAT_PUBLIC_ACCESS f,L_council co
		where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-350,112) and CONVERT(Varchar(10),GETDATE(),112)
		And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = $council_code And co.COUNCIL_CD_P!='486' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor') 
		and ID in ($inputFormatID)
		) as c ORDER BY ID DESC";
	}
	else
	{
		# This query to select records once in a day......
		
		$query = "select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
			case when project_id <> '' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME
			from (
			select ID, Source, Application_No, Document_Url, F.MARKUP, URL, No_of_Documents,
			isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),'') as project_id,
			isnull((select top 1 HOUSE_EXTN_ID_P from dr_house_Extn where Format_ID=f.id),'') as house_extn_id,
			isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME
			from format_public_access f,L_council co
			where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-100,112) and CONVERT(Varchar(10),GETDATE(),112)
			And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = \'".$council_code."\' And co.COUNCIL_CD_P <> '486' and All_Documents_Downloaded = 'N' and f.MARKUP in ('LARGE')
			) as c where project_status <> 'Processed Complete' ORDER BY ID DESC";
	}
		
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_ID,@Source,@Application_No,@Document_Url,@Markup,@URL,@PROJECT_ID,@PROJECT_STATUS,@COUNCIL_NAME,@NO_OF_DOCUMENTS);
	my $reccount;
	my $datetime = localtime();
	while(my @record = $sth->fetchrow)
	{
		push(@Format_ID,&Trim($record[0]));
		push(@Source,&Trim($record[1]));
		push(@Application_No,&Trim($record[2]));
		push(@Document_Url,&Trim($record[3]));
		push(@Markup,&Trim($record[4]));
		push(@URL,&Trim($record[5]));
		push(@NO_OF_DOCUMENTS,&Trim($record[6]));
		push(@PROJECT_ID,&Trim($record[7]));
		push(@PROJECT_STATUS,&Trim($record[8]));
		push(@COUNCIL_NAME,&Trim($record[9]));
	}
	$sth->finish();
	return (\@Format_ID,\@Source,\@Application_No,\@Document_Url,\@Markup,\@URL,\@PROJECT_ID,\@PROJECT_STATUS,\@COUNCIL_NAME,\@NO_OF_DOCUMENTS);
}

sub Retrieve_Input_ApprovedDecision()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $inputFormatID	= shift;
	
	print "Fetching council code $council_code......\n";
	
	
		my $query = "select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
		case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME
		from ( select a.ID,a.Source,a.Application_No,a.Document_Url,a.MARKUP,a.URL,a.No_of_Documents,
		isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT with (nolock) where  Format_ID=a.id),'') as project_id,
		isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  with (nolock) where  Format_ID=a.id),'') as house_extn_id,
		isnull((select  top 1 status  from Project_Status as c with (nolock),DR_PROJECT as d with (nolock) where  c.project_id=d.PROJECT_ID_P and d.Format_ID=a.id),'') as project_status,
		b.COUNCIL_NAME
		from glenigan..tbl_approved_large_project as a with (nolock),L_council b with (nolock)
		where a.id is not null and CONVERT(Varchar(10),a.imported_date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
		And a.council_Code=b.COUNCIL_CD_P  And b.COUNCIL_CD_P = $council_code
		And b.COUNCIL_CD_P <> '486'  And b.COUNCIL_CD_P <> '20'  and a.All_Documents_Downloaded = 'N') as f ORDER BY ID DESC";
		
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_ID,@Source,@Application_No,@Document_Url,@Markup,@URL,@PROJECT_ID,@PROJECT_STATUS,@COUNCIL_NAME,@NO_OF_DOCUMENTS);
	my $reccount;
	my $datetime = localtime();
	while(my @record = $sth->fetchrow)
	{
		push(@Format_ID,&Trim($record[0]));
		push(@Source,&Trim($record[1]));
		push(@Application_No,&Trim($record[2]));
		push(@Document_Url,&Trim($record[3]));
		push(@Markup,&Trim($record[4]));
		push(@URL,&Trim($record[5]));
		push(@NO_OF_DOCUMENTS,&Trim($record[6]));
		push(@PROJECT_ID,&Trim($record[7]));
		push(@PROJECT_STATUS,&Trim($record[8]));
		push(@COUNCIL_NAME,&Trim($record[9]));
	}
	$sth->finish();
	return (\@Format_ID,\@Source,\@Application_No,\@Document_Url,\@Markup,\@URL,\@PROJECT_ID,\@PROJECT_STATUS,\@COUNCIL_NAME,\@NO_OF_DOCUMENTS);
}


sub Retrieve_Input_CC()
{
	my $dbh = shift;
	
	# This query will select only those projects are not downloaded today. Ehich means a project could run only once a day.
	

	
	my $query= "select distinct COUNCIL_CD_P
				from (
				select ID, Source, Application_No, Document_Url, F.MARKUP, URL,
				isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),'') as project_id,
				isnull((select top 1 HOUSE_EXTN_ID_P  from dr_house_Extn where Format_ID=f.id),'') as house_extn_id,
				isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
				from format_public_access f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-90,112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P <> '486' and co.COUNCIL_CD_P >=2000 and All_Documents_Downloaded = 'N' and f.MARKUP in ('LARGE'))
				as c where project_status <> 'Processed Complete'";
				
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@COUNCIL_NAME,@COUNCIL_CD_P);	
	while(my @record = $sth->fetchrow)
	{
		# push(@COUNCIL_NAME,&Trim($record[0]));
		push(@COUNCIL_CD_P,&Trim($record[0]));
	}
	$sth->finish();
	
	my @filteredCC = uniq(@COUNCIL_CD_P);
	
	return (\@filteredCC);
}

sub Retrieve_Input_CC_Individual()
{
	my $dbh 			= shift;
	my $councilCode		= shift;
	my $query= "select distinct COUNCIL_CD_P,ID
	from (
	select ID,Source,Application_No,Document_Url,F.MARKUP,
	isnull((select  top 1 PROJECT_ID_P from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
	isnull((select  top 1 HOUSE_EXTN_ID_P    from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
	isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
	from FORMAT_PUBLIC_ACCESS f,L_council co
	where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
	And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = \'$councilCode\' and All_Documents_Downloaded = 'N' and f.MARKUP in ('HOUSE','LARGE','Smalls')
	and (CONVERT(varchar,f.oa_scraped_date ,103)<>CONVERT(varchar,getdate(),103) or f.oa_scraped_date is null)
	) as c where  project_status <> 'Processed Complete'";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@FormatID,@COUNCIL_CD_P);	
	while(my @record = $sth->fetchrow)
	{
		push(@COUNCIL_CD_P, &Trim($record[0]));
		push(@FormatID, &Trim($record[1]));
	}
	$sth->finish();
	
	return (\@COUNCIL_CD_P, \@FormatID);
}

sub Retrieve_CC_ApprovedDecision()
{
	my $dbh 			= shift;
	my $councilCode		= shift;
	my $query= "select a.council_code from glenigan..tbl_approved_large_project as a with (nolock),L_council b with (nolock)
	where a.id is not null and CONVERT(Varchar(10),a.imported_date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
	And a.council_Code=b.COUNCIL_CD_P 
	And b.COUNCIL_CD_P <> '486'  And b.COUNCIL_CD_P <> '20'  and a.All_Documents_Downloaded = 'N'
	group by  a.council_code order  by cast(a.council_code as int)";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@COUNCIL_CD_P);	
	while(my @record = $sth->fetchrow)
	{
		push(@COUNCIL_CD_P, &Trim($record[0]));
	}
	$sth->finish();
	
	return (\@COUNCIL_CD_P);
}

sub retrieveOnDemondInput_CC()
{
	my $dbh 			= shift;
	
	my $query= "select ID, council_code, format_id from TBL_ON_DEMAND_DOWNLOAD where download_type = 'document download' and (status is null or status = '')";

	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@ID, @council_code,@format_id);	
	while(my @record = $sth->fetchrow)
	{
		push(@ID,&Trim($record[0]));
		push(@council_code,&Trim($record[1]));
		push(@format_id,&Trim($record[2]));
		print "Record: $record[0]\n";
	}
	$sth->finish();
	return (\@ID, \@council_code,\@format_id);
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}


sub Execute()
{
	my $dbh		= shift;
	my $query 	= shift;
	my $sth = $dbh->prepare($query);
	my $ReTry_Flag = 1;
	Re_Execute:
	eval
	{
		$sth->execute();
		$sth->finish();	
		print "Executed\n";
	};
	
	if($@)
	{
		if($ReTry_Flag<=3)
		{
			$ReTry_Flag++;
			goto Re_Execute;
		}	
	
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub RetrieveMD5()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $format_id	 	= shift;
	
	my $query = "select PDF_MD5 from TBL_PDF_MD5 where council_code=\'$council_code\' and format_id=\'$format_id\'";
	print "MD5 Qry:::=> $query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@PDF_MD5);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@PDF_MD5,&Trim($record[0]));
	}
	$sth->finish();
	return (\@PDF_MD5);
}

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}