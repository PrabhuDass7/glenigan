use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use JSON::Parse 'parse_json';
use Cwd qw(abs_path);
use File::Basename;

use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);

my $mechz = WWW::Mechanize->new(autocheck => 0);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 

# Declare and load local directories and required private module
my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm');

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');

#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);

# my $Download_Location = 'C:\\Users\\mgoas\\Documents\\Document_Download_e_Plan\\data\\Just'; # New Shared Drive Location
my $Download_Location = '\\\\172.27.137.202\\One_app_download'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";

my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($inputTableName,$ApprovedDecisionUpdate_query);

my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	

&Download_DB::Execute($dbh,$Dashboard_Insert_Query);

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;
my $pdfcount_inc=1;

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $Proxy = $Config->{$Council_Code}->{'Proxy'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Accept_Cookie = $Config->{$Council_Code}->{'Accept_Cookie'};
my $Accept_Cookie_URL = $Config->{$Council_Code}->{'Accept_Cookie_URL'};
my $Accept_Post_Content = $Config->{$Council_Code}->{'Accept_Post_Content'};
my $Accept_Host = $Config->{$Council_Code}->{'Accept_Host'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Need_Host = $Config->{$Council_Code}->{'Need_Host'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $Accept_Language = $Config->{$Council_Code}->{'Accept_Language'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Landing_Page_Regex2 = $Config->{$Council_Code}->{'Landing_Page_Regex2'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $CSRFToken_Regex = $Config->{$Council_Code}->{'CSRFToken_Regex'};
my $Doc_Page_Num_Regex = $Config->{$Council_Code}->{'Doc_Page_Num_Regex'};
my $Doc_NumberOfRows_Regex = $Config->{$Council_Code}->{'Doc_NumberOfRows_Regex'};
my $Doc_POST_ID1_Regex = $Config->{$Council_Code}->{'Doc_POST_ID1_Regex'};
my $Doc_Post_Content = $Config->{$Council_Code}->{'Doc_Post_Content'};
my $Doc_Post_URL = $Config->{$Council_Code}->{'Doc_Post_URL'};
my $Doc_Post_Host = $Config->{$Council_Code}->{'Doc_Post_Host'};
my $DOC_PDF_ID_URL = $Config->{$Council_Code}->{'DOC_PDF_ID_URL'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Block_Regex4 = $Config->{$Council_Code}->{'Block_Regex4'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Download_URL_redirct = $Config->{$Council_Code}->{'Doc_Download_URL_redirct'};
my $Doc_Download_URL_redirct_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_Regex'};
my $Doc_Download_URL_redirct_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_ABS'};
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Current_Page_Number_Regex = $Config->{$Council_Code}->{'Current_Page_Number_Regex'};
my $Next_Page_Post_Content = $Config->{$Council_Code}->{'Next_Page_Post_Content'};
my $Next_Referer = $Config->{$Council_Code}->{'Next_Referer'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Application_Regex = $Config->{$Council_Code}->{'Application_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};
my $Search_POST = $Config->{$Council_Code}->{'Search_POST'};

my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;
my $MaxCount=0;

my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my $reccount = @{$Format_ID};

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code, $Available_Doc_Count);
	my $Landing_Page_Flag=0;

	my ($Doc_Details_Hash_Ref, $Document_Url, $Code,$Available_Doc_Count)=&Extract_Link($Method, $Home_URL, $Document_Url, $URL,\%Nextpage_Replace_Hash, $Application_No,$Format_ID);

	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;
	
	$Todays_Count = $Todays_Count + $pdfcount;
	my $No_Of_Doc_Downloaded=0;	
	
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	
	my $RetrieveMD5=[];
	# if(lc($Markup) eq 'large')
	# {
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;

	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
		
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];

		$Site_Status = $Code;
		
		
		if($Available_Doc_Count = ~m/^\s*$/is)
		{
			$Available_Doc_Count = $pdfcount;
		}	
		
		if($Council_Code=~m/^(2021|2011)$/is)
		{
			$Document_Url="$pdf_url";
		}
		print "Document_Url:: $Document_Url\n";
		
		$Pdf_Name=~s/WITHOUT_PRESONAL_DATA/Application_Form_without_personal_data/igs;
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				# my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				my ($D_Code, $Err_Msg, $MD5);
				if($Council_Code=~m/^(2015|2002|2000|2001|2017|2018|2023|2024|2026|2027|2028|2030)$/is)
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
				}
				else
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				}
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my ($Download_Status);
				if($pdf_url!~m/^\s*$/)
				{
					# my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					my ($D_Code, $Err_Msg, $MD5);
					if($Council_Code=~m/^(2015|2002|2000|2001|2017|2018|2023|2024|2026|2027|2028|2030)$/is)
					{
						
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$pageFetch,$Council_Code);
					}
					else
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					}
					($Download_Status)=&Download_Utility::Url_Status($D_Code);
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
				}
			}
		}	
		
		#--------- DB INSERTION above 1000 record ---------------
		$MaxCount++;
		if($MaxCount == 995)
		{
			($MaxCount, $Insert_Document, $MD5_Insert_Query, $Update_Query) = QueryExecuteMax($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
		}
	}	
	
	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	&Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		my $No_Of_Doc_Downloaded_TMP = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		$No_Of_Doc_Downloaded=$No_Of_Doc_Downloaded_TMP;
	}	
	# else
	# {
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }	
	
	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if($One_App_Name=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}		
	}	
	
	$pdfcount_inc=1;
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;
print "\n$Insert_OneAppLog\n";
print "\n$Insert_Document\n";

$Insert_OneAppLog=~s/\,$//igs;
# print "\n$Insert_OneAppLog\n";
&Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\n$Insert_Document\n";
&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
&Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);

print "Download Completed\n";


sub Extract_Link()
{
	my $Method = shift;
	my $Home_URL = shift;
	my $Document_Url = shift;
	my $URL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $Application_No = shift;
	my $Format_ID = shift;

	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my $Scrp_Flag=0;
	my ($Content,$Code,$Redir_Url,$Doc_Details_Hash_Ref,%Doc_Details_Hash,$Previous_page,$Post_Responce,$Post_Res_code,$Available_Doc_Count);
	my ($Landing_Page_Flag,$Landing_Page_Flag2)=(0,0);
	my $Previous_page=0;
	
	print "Method==>$Method\n";
	Re_Ping:
	if($Method=~m/GET/is)
	{
		print "get\n";
		if($Council_Code=~m/^2002$/is)
		{
			if($Document_Url!~m/http(?:s|)\:\/\/[\d]+\./is)
			{
				$Document_Url="http://193.178.30.219/iDocsWebDPSS/copyright.aspx?catalog=planning&id=".$1 if($Document_Url=~m/\&id\=([\d]+)$/is);
			}
		}
		
		if($Council_Code=~m/^2022|2009$/is)
		{	
			$Document_Url=~s/https/http/igs;
			$Document_Url=~s/http/https/igs;
			($Content,$Code) = &Download_Utility::Getcontent($Document_Url,$pageFetch);
		}
		elsif($Council_Code=~m/^(2008)$/is)
		{	
			$Document_Url="http://documents.fingalcoco.ie/NorthgateIM.WebSearch/ExternalEntryPoint.aspx?SEARCH_TYPE=1&DOC_CLASS_CODE=PL&FOLDER1_REF=$Application_No";
			($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
		}
		elsif($Council_Code=~m/^(2019)$/is)
		{	
			($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
			my $doc_url=$1 if($Content=~m/$Landing_Page_Regex/is);
			$doc_url=~s/amp\;//igs;
			($Content,$Code) = &Download_Utility::docMechMethod($doc_url,$pageFetch);
			
			my $doc_url1='http://137.191.225.173/mcc_dwviewer_external/'.$1 if($Content=~m/$Doc_URL_Regex/is);
			($Content,$Code) = &Download_Utility::docMechMethod($doc_url1,$pageFetch);
		}
		elsif($Council_Code=~m/^2021$/is)
		{
			$Document_Url='https://portal.monaghancoco.ie/WebLinkPlanningPortal/search.aspx?dbid=0&searchcommand={LF:Name~='.$Application_No.'}';
			($Content,$Code) = &Download_Utility::Getcontent($Document_Url,$pageFetch);
			my $doc_id=$1 if($Document_Url=~m/[^>]*?searchcommand=\s*([^>]*?)\s*$/is);
			my $posturl='https://portal.monaghancoco.ie/WeblinkPlanningPortal/SearchService.aspx/GetSearchListing';
			$Referer='https://portal.monaghancoco.ie/WebLinkPlanningPortal/search.aspx?dbid=0&searchcommand='.$doc_id.'&cr=1';
			my $post_cnt='{"repoName":"MONLFPLANNING","searchSyn":"'.$doc_id.'","searchUuid":"","sortColumn":"","startIdx":0,"endIdx":20,"getNewListing":true,"sortOrder":2,"displayInGridView":false}';
			
			($Content,$Code)=&Download_Utility::Post_Method($posturl,$Host,$Content_Type,$Referer,$post_cnt);
						
			my $search_Id=$1 if($Content=~m/\"searchUUID\"\:\"([^>]*?)\"/is);
			$search_Id=~s/\s*//igs;
			
			my %Doc_Details;
			while($Content=~m/\"entryId\"\:([\d]+)\,/igs)
			{
				my $doc_id1=$1;
				my $plan_url2='https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/GetBasicDocumentInfo';
				
				print "doc_id1:::$doc_id1\n\n";
			
				my $post_cnt='{"repoName":"MonLFPlanning","entryId":'.$doc_id1.'}';
								
				my ($Content1,$Codes)=&Download_Utility::Post_Method($plan_url2,$Host,$Content_Type,$Referer,$post_cnt);
				
				my $pg_count=$1 if($Content1=~m/pageCount\"\:([\d]+)\,/is);
				my $document_type=$1 if($Content1=~m/\"Document\s*Type\"\,\"values\"\:\[\"([^>]*?)\"\]\,/is);
				my $pdf_name=$document_type.".pdf";
				
				$plan_url2='https://portal.monaghancoco.ie/WeblinkPlanningPortal/GeneratePDF10.aspx?key='.$doc_id1.'&PageRange=1%20-%20'.$pg_count.'&Watermark=0';
				print "******plan_url2:: $plan_url2*******\n";
				my ($Content2,$Codes) = &Download_Utility::Getcontent($plan_url2,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
								
				my $idd=$1 if($Content2=~m/^([^>]*?)\n/is);
				
				$idd=~s/\s*//igs;
				
				print "******idd:: $idd*******\n";
				
				my $Doc_URL="https://portal.monaghancoco.ie/WeblinkPlanningPortal/PDF10/$idd/$doc_id1";
				print "Doc_URL**** $Doc_URL ***\n";
				
				$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,"",""];
			}

			return(\%Doc_Details,$Document_Url,$Code,$Available_Doc_Count);
		}
		elsif($Council_Code=~m/^2011$/is)
		{
			print "2011\n";
			print "Document_Url:::$Document_Url\n";
			# $pageFetch->get($Home_URL);
			# my $code=$pageFetch->status();
			# my $Doc_cont=$pageFetch->content();
			# my $cookie=$pageFetch->response->header('Set-Cookie');
			
			# $pageFetch->add_header( 'Cookie' =>$cookie);
			# $pageFetch->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36');
			# $pageFetch->add_header( 'Host' => $Host);
			# $pageFetch->get($Referer);

			# my $code=$pageFetch->status();
			# $Content=$pageFetch->content();
			
			# my $vw_st=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
			# my $vw_gnr=uri_escape($1) if($Content=~m/$Viewstategenerator_Regex/is);
			# my $valid=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);

			# my $acc_con="__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$vw_st&__VIEWSTATEGENERATOR=$vw_gnr&__EVENTVALIDATION=$valid&txtPlanningNo=$Application_No&btnFindByFileNum=Find&dpFromDate_DrpPnl_Calendar1=%253Cx%2520PostData%253D%25222020x8x-1x-1x-1%2522%253E%253C%2Fx%253E&dpFromDate_hidden=%253CDateChooser%2520Enabled%253D%2522false%2522%253E%253C%2FDateChooser%253E&dpToDate_DrpPnl_Calendar1=%253Cx%2520PostData%253D%25222020x8x-1x-1x-1%2522%253E%253C%2Fx%253E&dpToDate_hidden=%253CDateChooser%2520Enabled%253D%2522false%2522%253E%253C%2FDateChooser%253E&txtApplicantName=&txtLocationOfDevelopment=";
			
			# $pageFetch->post( $Referer, Content => "$acc_con");

			# my $Content = $pageFetch->content;
			# my $code = $pageFetch->status;
			# my $res = $pageFetch->res();
			# $pageFetch->get($Document_Url);
			# my $code=$pageFetch->status();
			# my $Doc_cont=$pageFetch->content();
			# print "code::$code\n";
			# my $Doc_URL="http://docstore.kerrycoco.ie/planningfiles/$Application_No.pdf" if($Content=~m/value\=\"View\s*Docs\"[^>]*?\"ShowInIPlan\(\'([^>]*?)\'/is);
			my $Doc_URL="http://docstore.kerrycoco.ie/planningfiles/$Application_No.pdf";
			print "Doc_URL::$Doc_URL\n";
			
			my %Doc_Details;
			my $pdf_name=$Application_No.".pdf";
			$Doc_Details{$Doc_URL}=["",$pdf_name,"",""];
			return(\%Doc_Details,$Document_Url,$Code,$Available_Doc_Count);
			
		}
		elsif($Council_Code=~m/^2010$/is)
		{	
			($Content,$Code) = &Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
		}
		elsif($Document_Url ne "")
		{	
			($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
		}
		
		if($Document_Url eq "")
		{
			print "Method:\t$Method. But Document URL not found for this App ID\n";
			
			($Content,$Code) = &Download_Utility::docMechMethod($Home_URL,$pageFetch);

			my $req_vry_no=uri_escape($1) if($Content=~m/<input\s*name\=\"\_\_RequestVerificationToken\"[^>]*?value\=\"([^>]*?)\"[^>]*?>[^>]*?<div\s*class\=\"form\-horizontal\">/is);
			my $CountyTownCouncilNames=uri_escape($1) if($Content=~m/id\=\"CountyTownCouncilNames\"[^>]*?value\=\"([^>]*?)\"/is);
			my $CheckBoxList=uri_escape($1) if($Content=~m/id\=\"CheckBoxList\_0\_\_Name\"[^>]*?value\=\"([^>]*?)\"/is);
			
			my $file_cont="__RequestVerificationToken=$req_vry_no".'&TxtFileNumber='."$Application_No".'&TxtName=&TxtAddress=&TxtDevdescription=&LstTimeLimit=0&CheckBoxList%5B0%5D.Id=0&CheckBoxList%5B0%5D.Name='."$CheckBoxList".'&CheckBoxList%5B0%5D.IsSelected=true&CheckBoxList%5B0%5D.IsSelected=false&SearchType=Exact&CountyTownCount=1&CountyTownCouncilNames='."$CountyTownCouncilNames";
			
			($Content,$Code) = &Download_Utility::docPostMechMethod($Doc_Post_URL,$file_cont,$pageFetch);
			
			my $File_url=$1 if($Content=~m/<td>\s*<a\s*href\=\"([^>]*?)\"\>/is);
			$File_url=$Abs_url.$File_url;
			($Content,$Code) = &Download_Utility::docMechMethod($File_url,$pageFetch);
			
			
			if($Content=~m/$Doc_URL_Regex/is)
			{
				my $DocUrl= $1;
				$Document_Url=$DocUrl;
			
				$Document_Url=$Abs_url.$Document_Url if($Document_Url!~m/^http(?:s|)?\:/is);
				$Document_Url=~s/&amp;/&/igs;
				$Document_Url=~s/^\s*//igs;
				$Document_Url=~s/\s*$//igs;

				($Content,$Code) = &Download_Utility::docMechMethod($Document_Url,$pageFetch);
			}
		}

		if($Council_Code=~m/^2010$/is)
		{
			if($Content=~m/onclick=\"__doPostBack\(\'([^>]*?)\'[^>]*?>\s*Click\s*here\s*if\s*you\s*have\s*read\s*and[^>]*?</is)
			{				
				my $vw_st=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
				my $vw_gnr=uri_escape($1) if($Content=~m/$Viewstategenerator_Regex/is);
				my $valid=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
				
				my $acc_con="__EVENTTARGET=ctl00%24MainContent%24cmdEnglishLng&__EVENTARGUMENT=&__VIEWSTATE=$vw_st&__VIEWSTATEGENERATOR=$vw_gnr&__EVENTVALIDATION=$valid&ctl00%24MainContent%24hfInEnglish=";
				$Document_Url=~s/\.aspx//igs;
				($Content,$Code)=&Download_Utility::Post_Method($Document_Url,$Host,$Content_Type,$Document_Url,$acc_con,$Proxy);
			}
		}
		
		if($Council_Code=~m/^2003$/is)
		{	
			($Content,$Code) = &Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
		}
		
		if($Content=~m/name\=\"chkAgree\"\s*onclick\=[\w\W]*?>\s*I\s*Agree</is)
		{
			
			my $vw_st=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
			my $vw_gnr=uri_escape($1) if($Content=~m/$Viewstategenerator_Regex/is);
			my $valid=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			
			my $acc_con="__EVENTTARGET=chkAgree&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=$vw_st&__VIEWSTATEGENERATOR=$vw_gnr&__EVENTVALIDATION=$valid&chkAgree=on";
						
			if($Council_Code=~m/^2003|2022$/is)
			{
				($Content,$Code)=&Download_Utility::Post_Method($Document_Url,$Host,$Content_Type,$Document_Url,$acc_con,$Proxy);
			}else{
				($Content,$Code) = &Download_Utility::docPostMechMethod($Document_Url,$acc_con,$pageFetch);
			}
		}
		
		if($Content=~m/name\=\"chkAgree\"\s*checked\=[\w\W]*?>\s*I\s*Agree</is)
		{
			my $btnViewFiles=uri_escape($1) if($Content=~m/name\=\"btnViewFiles\"\s*value\=\"([^>]*?)\"/is);
			
			my $vw_st=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
			my $vw_gnr=uri_escape($1) if($Content=~m/$Viewstategenerator_Regex/is);
			my $valid=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			
			my $acc_con="__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=$vw_st&__VIEWSTATEGENERATOR=$vw_gnr&__EVENTVALIDATION=$valid&chkAgree=on&btnViewFiles=$btnViewFiles";
			
			if($Council_Code=~m/^2003|2022$/is)
			{
				($Content,$Code)=&Download_Utility::Post_Method($Document_Url,$Host,$Content_Type,$Document_Url,$acc_con,$Proxy);
			}else{
				($Content,$Code) = &Download_Utility::docPostMechMethod($Document_Url,$acc_con,$pageFetch);
			}
		}
	}

	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($Content=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
	}	

	if($Format eq 'Table')
	{
		
		my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($Content,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}

	return (\%Doc_Details_Hash,$Document_Url,$Code,$Available_Doc_Count);
}


sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $Doc_URL_BY=shift;
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	print "Collect_Docs_Details**** \n";
	
	if (($Doc_Download_URL_Index =~m/([a-z]+)/is) and ($Doc_Download_URL_Index ne 'N/A'))
	{
		my ($Doc_Details_Hash_Ref_tmp,$p)=&Download_Utility::Doc_Download_Html_Header($Config,$Content,$Home_URL,$Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY,$Council_Code,$pdfcount_inc);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
		$pdfcount_inc=$p;
	}
	else
	{
		print "else\n";
		$Content=~s/<td\/>/<td><\/td>/igs;
		
		if($Block_Regex1 ne 'N/A')
		{
			my @Block1 = $Content =~m/$Block_Regex1/igs;
			if($Block_Regex2 ne 'N/A')
			{
				for my $Block1(@Block1)
				{	
					my @Block2 = $Block1=~m/$Block_Regex2/igs;
					if($Block_Regex3 ne 'N/A')
					{
						for my $Block2(@Block2)
						{	
							my @Block3 = $Block2=~m/$Block_Regex3/igs;
							
							my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$URL,$Content);
							my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
							%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
						}	
					}
					else
					{
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$URL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
			}	
			else
			{
				my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$URL,$Content);
				my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
			}	
		}	
	}
	return \%Doc_Details_Hash;
}


sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}

	$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
	
	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
	$document_type = &Download_Utility::Clean($document_type);
	
	if($Doc_URL_BY eq 'POST')
	{
		print "Doc_URL_BY eq POST\n";
		my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$Doc_POST_ID2,$CSRFToken);
		
		my $Doc_Post_Content_Tmp=$Doc_Post_Content;
		
		if($Viewstate_Regex ne 'N/A')
		{
			$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		}	
		if($Viewstategenerator_Regex ne 'N/A')
		{
			$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		}	
		if($CSRFToken_Regex ne 'N/A')
		{
			$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
		}
		if($Eventvalidation_Regex ne 'N/A')
		{
			$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		}	
		if($Doc_Page_Num_Regex ne 'N/A')
		{
			$Doc_Page_Num=$1 if($Content=~m/$Doc_Page_Num_Regex/is);
		}	
		if($Doc_NumberOfRows_Regex ne 'N/A')
		{
			$Doc_NumberOfRows=$1 if($Content=~m/$Doc_NumberOfRows_Regex/is);
		}	
		if($PDF_URL_Cont=~m/$Doc_POST_ID1_Regex/is)
		{
			$Doc_POST_ID1=uri_escape($1);
			$Doc_POST_ID2=uri_escape($2);
		}

		$Doc_Post_URL = $Document_Url if($Doc_Post_URL=~m/Document_Url/is);
		$Referer = $Document_Url if($Referer=~m/Document_Url/is);
		$Doc_Post_URL=decode_entities($Doc_Post_URL);
		$Referer=decode_entities($Referer);
		$Doc_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
		$Doc_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
		$Doc_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
		$Doc_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
		$Doc_Post_Content_Tmp=~s/<PageNum>/$Doc_Page_Num/igs;
		$Doc_Post_Content_Tmp=~s/<PageRows>/$Doc_NumberOfRows/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID1>/$Doc_POST_ID1/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID2>/$Doc_POST_ID2/igs;
		
		$Doc_Post_Host=$Host if($Doc_Post_Host eq '') or ($Doc_Post_Host eq 'N/A');
		
		($Post_Responce,$res_code)=&Download_Utility::Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Referer,$Doc_Post_Content_Tmp,$Proxy);

		my $URL_ID;
		if($Post_Responce=~m/$Doc_Download_URL_Regex/is)
		{
			my $val=$1;
			if($DOC_PDF_ID_URL=~m/ID/is)
			{
				$URL_ID=$val;
			}
			elsif($DOC_PDF_ID_URL=~m/URL/is)
			{
				my $u1=URI::URL->new($val,$Doc_Post_URL);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
			}
		}
		if($Doc_Download_URL_Replace ne 'N/A')
		{
			$Doc_URL = $Doc_Download_URL_Replace;
			$Doc_URL=~s/<SelectedID>/$URL_ID/igs;
		}
	}
	elsif($Doc_Download_URL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl=$1;
			
			if($Doc_Download_URL_ABS ne 'N/A') 
			{
				$Doc_URL=$Doc_Download_URL_ABS.$Durl;
			}
			else
			{
				if($Durl!~m/http/is)
				{

					# if($Council_Code=~m/^2017$/is)
					# {
						
						my $Durl=$1 if($Data[$Doc_Download_URL_Index]=~m/$Doc_Download_URL_Regex/is);
						
						my $u1=URI::URL->new($Durl,$Root_Url);
						my $u2=$u1->abs;
						$Doc_URL=$u2;
						
						my $img_id=$1 if($Doc_URL=~m/docid\=([\d]+)/is);
						my $dco_nm=$1 if($Document_Url=~m/(.*\/[^>]*?)/is);
						# my ($content5,$Codes) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
						
						my ($content5,$Codes);
						if($Council_Code=~m/^2003|2022$/is)
						{
							($content5,$Codes) = &Download_Utility::Getcontent($Doc_URL,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
						}else{
							($content5,$Codes) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
						}

						my $doc_code=$1 if($content5=~m/src\=\"View(?:DPSSFile|[^>]*?)\.aspx\?file\=([^>]*?)\"/is);
						my $doc_code3=$1 if($content5=~m/src\=\"(View(?:DPSSFile|[^>]*?)\.aspx\?file\=[^>]*?)\"/is);
						
						my $ppgglk=$dco_nm.$doc_code3;
						
						# my ($content6,$Codes) = &Download_Utility::docMechMethod($ppgglk,$pageFetch);
						
						my ($content6,$Codes);
						if($Council_Code=~m/^2003|2022$/is)
						{
							($content6,$Codes) = &Download_Utility::Getcontent($ppgglk,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
						}else{
							($content6,$Codes) = &Download_Utility::docMechMethod($ppgglk,$pageFetch);
						}
						
						$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
						$document_type = &Download_Utility::Clean($document_type);
						$Published_Date = &Download_Utility::Clean($Published_Date);
						
						my $document_type2=$document_type;
						$document_type2=~s/\//\_/igs;
						$document_type2=~s/\-/\_/igs;
						$document_type2=~s/\s+/ /igs;
						$document_type2=~s/\s/\_/igs;
						$document_type2=~s/\_+/\_/igs;

						my $doc_url_cc=$1 if($content6=~m/<body>[\w\W]*?<div\s*djvu\=\"([^>]*?)\"/is);
						
						if($doc_url_cc eq "")
						{
							$Doc_URL=$ppgglk;
							my $file_type=$1 if($Doc_URL=~m/(?:\.|\=|)((?:pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png|jpeg|rtf))$/is);
							
							my $pdf_name="$img_id"."_1"."_".$document_type2.".".$file_type;
							
							$Doc_URL=~s/ViewPdf\.aspx\?file\=/files\//igs;

							if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
							{
								$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
							}
							
							goto jump;
						}
						
						my $doc_code2=$doc_code;
						$doc_code2=~s/-//igs;
						
						my $pg_cnt_lk=$doc_url_cc.'/finfo%3Bcallback%3D%28function%28data%29%7B%24%28%22%23djvud'.$doc_code2.'ea%22%29.djvuwebviewer%28%22callback%22%2C%22finfo%22%2Cdata%29%7D%29?_='.time;
						
						my ($content7,$Codes);
						if($Council_Code=~m/^2003|2022$/is)
						{
							($content7,$Codes) = &Download_Utility::Getcontent($pg_cnt_lk,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
						}else{
							($content7,$Codes) = &Download_Utility::docMechMethod($pg_cnt_lk,$pageFetch);
						}

						my $pg_c=$1 if($content7=~m/\{\"count\"\:\s*([\d]+)\s*\,/is); #Pdf_Page_Count
						
						for(my $pdfcount_inc=1;$pdfcount_inc<=$pg_c;$pdfcount_inc++)
						{
							my $file_type=$1 if($Doc_URL=~m/(?:\.|\=|)((?:pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png|jpeg|rtf))$/is);
							my $pdf_url="$doc_url_cc/page=$pdfcount_inc;$file_type";
						
							my $pdf_name="$img_id"."_$pdfcount_inc"."_".$document_type2.".".$file_type;
							
							$Doc_URL=$pdf_url;
							
							if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
							{
								$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
							}
						}
						jump:
						return (\%Doc_Details);
				}	
				else
				{
					$Doc_URL=$Durl;
				}	
			}
			if($Council_Code=~m/^2009$/is)
			{
				my $Doc_ID=$1 if($Doc_URL=~m/documentId\=([^>]*?)$/is);
				my $Doc_link="https://plan.galwaycity.ie/VirtualViewerNetHTML5/AjaxServer?action=getDocumentModel&documentId=$Doc_ID";
				my ($content8,$Codes) = &Download_Utility::Getcontent($Doc_link,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
				
				my $pg_c=()=$content8=~/\"pageIndex\"\:([\d]+)\,/ig; #Pdf_Page_Count
				for(my $pdfcount_inc=0;$pdfcount_inc<$pg_c;$pdfcount_inc++)
				{
					my $pg_num=$pdfcount_inc+1;
					my $file_type=$1 if($Doc_URL=~m/(?:\.|\=|)((?:pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png|jpeg|rtf))$/is);
					
					my $pdf_url="https://plan.galwaycity.ie/VirtualViewerNetHTML5/AjaxServer?action=getImage&documentId=$Doc_ID&clientInstanceId&hiddenImageLayers&preferSvg=false&pageCount=$pg_c&zoomPercent=44&pageNumber=$pdfcount_inc&flipHorizontal=false&flipVertical=false&invertImage=false&brightness=0&contrast=0&gamma=100&despeckle=0&antiAliasOff=false&requestedHeight=-1&requestedWidth=-1&clientWidth=565&clientHeight=576&overlayPath&cacheBuster=0.".time;
					
					my $pdf_name;
					$pdf_name="$pg_num"."_"."$Doc_ID".".png" if($file_type eq "");
					$pdf_name="$pg_num"."_"."$Doc_ID".'.'."$file_type" if($file_type ne "");
					
					$Doc_URL=$pdf_url;
					
					if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					{
						$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
					}
				}
				return (\%Doc_Details);
			}
		}
	}
	
	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	my $file_type=$1 if($Doc_URL=~m/(?:\.|\=|)((?:pdf|xlsx|xls|docx|doc|zip|jpg|pptx|ppt|png|jpeg|rtf))$/is);
	$file_type='pdf' if($file_type eq "");

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	$Doc_URL=decode_entities($Doc_URL);
	$pdfcount_inc++;
	print "Doc_URL::$Doc_URL\n";
	print "pdf_name::$pdf_name\n";
	print "document_type::$document_type\n";
	print "pdf_name::$pdf_name\n";
	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	return (\%Doc_Details);
}	

sub Search_Application()
{
	my $Application_No = uri_escape(shift);
	my $cCode = shift;

	my ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Home_URL,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
	my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$CSRFToken);
	if($Viewstate_Regex ne 'N/A')
	{
		$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
	}	
	if($Viewstategenerator_Regex ne 'N/A')
	{
		$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
	}	
	if($CSRFToken_Regex ne 'N/A')
	{
		$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
	}	
	if($Eventvalidation_Regex ne 'N/A')
	{
		$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
	}	

	my $Post_Content_Tmp=$Post_Content;
	$Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
	$Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
	$Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
	$Post_Content_Tmp=~s/<APP_NUMBER>/$Application_No/igs;
	my($Post_Responce,$res_code)=&Download_Utility::Post_Method($Home_URL,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy);

	if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
	{
		if($Post_Responce=~m/$Landing_Page_Regex/is)
		{
			my $DocUrl=$Abs_url.$1;
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			$Document_Url=decode_entities($Document_Url);
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
		}
	}	
	

	if($Application_Regex ne 'N/A')
	{
		if($Content=~m/$Application_Regex/is)
		{
			$Document_Url = $1;
			my $u1=URI::URL->new($Document_Url, $Home_URL);
			$Document_Url=$u1->abs;			
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host);
		}
	}
	return($Content,$res_code);
}

sub QueryExecuteMax()
{	
	my ($dbh,$MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query)=@_;
	
	if($Insert_Document!~m/values\s*$/is)
	{
		$Insert_Document=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

		$Update_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

		$MD5_Insert_Query=~s/\,\s*$//igs;
		&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);
		
		$Insert_Document="";
		$Update_Query="";
		$MD5_Insert_Query="";
		
		$Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
		$MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

		$MaxCount=0;
	}
	return ($MaxCount,$Insert_Document,$MD5_Insert_Query,$Update_Query);
}