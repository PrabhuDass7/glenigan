use strict;
use Cwd qw(abs_path);
use File::Basename;
use POE qw(Wheel::Run Filter::Reference);
sub MAX_CONCURRENT_TASKS () { 10 }
# use DateTime;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

my $onDemandRequest = $ARGV[0];
print "onDemandRequest: $onDemandRequest\n";
####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'\\lib');
my $scriptDirectory = ($basePath.'/root');
my $logDirectory = ($basePath.'\\logs');

# print "scriptDirectory:: $scriptDirectory\n";<STDIN>;

# Private Module
require ($libDirectory.'\\Download_DB.pm'); 


#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();
#----------------------- Get input -----------------------#
my ($Council_Name,$Council_Code, $formatId, $onDemandID, @formatId, @cCode, @onDemandID);

($Council_Code) = &Download_DB::Retrieve_Input_CC($dbh);
@cCode = @{$Council_Code};


# use DateTime;
# my $current_date_time=DateTime->now;
my $current_date_time=localtime();

my $arrSize = @cCode;

my $schdule_time=$current_date_time;
$current_date_time=~s/\-|\:/_/igs;
my $status_log_file_name="Councile_code_".$current_date_time.".txt";

POE::Session->create(
  inline_states => {
    _start      => \&start_tasks,
    next_task   => \&start_tasks,
    task_result => \&handle_task_result,
    task_done   => \&handle_task_done,
    task_debug  => \&handle_task_debug,
    sig_child   => \&sig_child,
  }
);



sub start_tasks 
{
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	
	while (keys(%{$heap->{task}}) < MAX_CONCURRENT_TASKS) 
	{
		my $next_task = shift @cCode;
		my $next_formatId = shift @formatId;
		my $next_onDemandID = shift @onDemandID;
		last unless defined $next_task;
		sleep(5);
		print "Starting GDD task for $next_task $next_formatId...\n";
		
		my $task = POE::Wheel::Run->new(
		Program      => sub { do_stuff($next_task, $next_formatId, $next_onDemandID) },
		StdoutFilter => POE::Filter::Reference->new(),
		StdoutEvent  => "task_result",
		StderrEvent  => "task_debug",
		CloseEvent   => "task_done",
		);
		
		$heap->{task}->{$task->ID} = $task;
		$kernel->sig_child($task->PID, "sig_child");
	}
}

sub do_stuff 
{
	binmode(STDOUT);    # Required for this to work on MSWin32
	my $councilCode   	= shift;
	my $formatId   		= shift;
	my $next_onDemandID = shift;
	
	my $filter = POE::Filter::Reference->new();

	sleep(int(rand(15)+5));
	
		
	# elsif(($councilCode >= 2000) && ($councilCode <= 2030))
	# if($councilCode=~m/(?:2000|2001|2002|2003|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|2020|2022|2023|2024|2026|2027|2028|2030|2025)/is)
	if($councilCode=~m/(?:2000|2001|2002|2003|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2020|2022|2023|2024|2026|2027|2028|2030|2025|2008)/is)
	{
		my $script_name;	
		print "Document_Download_e-Plan script is Triggered";
		$script_name="$scriptDirectory/Document_Download_e-Plan.pl";
		
		system("start PERL $script_name $councilCode");
		
		sleep(60);
	
	}
	
	my $current_dateTime=localtime();
	print "current_date_time: $current_dateTime\n";
	open(my $fh,">>$logDirectory/status_log_file_name.txt");
	print $fh "$councilCode\t$current_dateTime\n";
	close ($fh);

	
	my %result = (
	task   => $councilCode,
	status => "seems ok to me",
	);

	my $output = $filter->put([\%result]);

	print @$output;
}

sub handle_task_result 
{
	my $result = $_[ARG0];
	print "Result for $result->{task}: $result->{status}\n";
}

sub handle_task_debug 
{
	my $result = $_[ARG0];
	print "Debug: $result\n";
}

sub handle_task_done {
	my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
	delete $heap->{task}->{$task_id};
	$kernel->yield("next_task");
}

sub sig_child {
	my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
	my $details = delete $heap->{$pid};

# warn "$$: Child $pid exited";
}

$poe_kernel->run();


if($onDemandRequest ne 'onDemand')
{
	my $current_dateTime=localtime();
	open(my $fh,">>$logDirectory/Retrieve_Input_council_count_log.txt");
	print $fh "$arrSize\t$current_dateTime\tTask End Time\n";
	close ($fh);
}
elsif($onDemandRequest ne 'ApprovedDecision')
{
	my $current_dateTime=localtime();
	open(my $fh,">>$logDirectory/ApprovedDecision_Input_council_count_log.txt");
	print $fh "$arrSize\t$current_dateTime\tTask End Time\n";
	close ($fh);
}
exit 0;