package Download_Generic;

use strict;
use DBI;
use filehandle;
use File::Path 'rmtree';
use File::stat;
use DBD::ODBC;
use Digest::MD5;
use HTML::Entities;
use HTTP::Request;
use LWP::UserAgent;
use URI::URL;
use File::Basename;
# use Data::Dumper;
use Mozilla::CA;
use HTTP::Cookies;
use HTTP::CookieMonster;
use WWW::Mechanize;
require Exporter;

my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);

###### DB Connection ####
sub DbConnection()
{
	my $dsn1 ='driver={SQL Server};Server=10.101.53.25;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return ($dbh,'Live');
}

sub testDbConnection()
{
	my $dsn1 ='driver={SQL Server};Server=172.27.137.184;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return ($dbh,'Test');
}

sub Execute()
{
	my $dbh		= shift;
	my $query 	= shift;
	my $DBtype 	= shift;
	my $sth = $dbh->prepare($query);
	my $ReTry_Flag = 1;
	Re_Execute:
	eval
	{
		$sth->execute();
		$sth->finish();	
		print "Executed\n";
	};
	
	if($@)
	{
		if($ReTry_Flag<=3)
		{
			$ReTry_Flag++;
			goto Re_Execute;
		}	
	
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		if($DBtype=~m/Live/is)
		{
			$dbh=&DbConnection();
		}
		else
		{
			$dbh=&testDbConnection();
		}
	}
}

sub Retrieve_Input()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $inputFormatID	= shift;
	
	print "Fetching council code $council_code......\n";
	
	
	my $query;
	if($inputFormatID=~m/^\d+$/is)
	{
		print "FormatID:=> $inputFormatID\n";

		$query = "select id,SOURCE ,Application_No ,Document_Url ,MARKUP ,URL, No_of_Documents,
		case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME 
		from ( 
		select ID,Source,Application_No,Document_Url,F.MARKUP,URL,No_of_Documents, 
		isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
		isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
		isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME
		from FORMAT_PUBLIC_ACCESS f,L_council co
		where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-350,112) and CONVERT(Varchar(10),GETDATE(),112)
		And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = $council_code And co.COUNCIL_CD_P!='486' and f.MARKUP in ('HOUSE','LARGE','Smalls','Minor') 
		and ID in ($inputFormatID)
		) as c ORDER BY ID DESC";
	}
	else
	{
		$query = "select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
		case when project_id <>'' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME
		from (
		select ID,Source,Application_No,Document_Url,F.MARKUP,URL,No_of_Documents,
		isnull((select  top 1 PROJECT_ID_P   from DR_PROJECT  where  Format_ID=f.id),'') as project_id,
		isnull((select  top 1 HOUSE_EXTN_ID_P  from dr_house_Extn  where  Format_ID=f.id),'') as house_extn_id,
		isnull((select  top 1 status  from Project_Status as a,DR_PROJECT as b where  a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),'') as project_status, co.COUNCIL_NAME
		from FORMAT_PUBLIC_ACCESS f,L_council co
		where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-30,112) and CONVERT(Varchar(10),GETDATE(),112)
		And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = $council_code And co.COUNCIL_CD_P <> '486'  And co.COUNCIL_CD_P <> '20'  and All_Documents_Downloaded = 'N' and f.MARKUP in ('HOUSE','LARGE','Smalls')
		and (CONVERT(varchar,f.oa_scraped_date ,103)<>CONVERT(varchar,getdate(),103) or f.oa_scraped_date is null)
		) as c where  project_status <> 'Processed Complete' ORDER BY ID DESC";
	}
		
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_ID,@Source,@Application_No,@Document_Url,@Markup,@URL,@PROJECT_ID,@PROJECT_STATUS,@COUNCIL_NAME,@NO_OF_DOCUMENTS);
	my $reccount;
	my $datetime = localtime();
	while(my @record = $sth->fetchrow)
	{
		push(@Format_ID,&Trim($record[0]));
		push(@Source,&Trim($record[1]));
		push(@Application_No,&Trim($record[2]));
		push(@Document_Url,&Trim($record[3]));
		push(@Markup,&Trim($record[4]));
		push(@URL,&Trim($record[5]));
		push(@NO_OF_DOCUMENTS,&Trim($record[6]));
		push(@PROJECT_ID,&Trim($record[7]));
		push(@PROJECT_STATUS,&Trim($record[8]));
		push(@COUNCIL_NAME,&Trim($record[9]));
	}
	$sth->finish();
	return (\@Format_ID,\@Source,\@Application_No,\@Document_Url,\@Markup,\@URL,\@PROJECT_ID,\@PROJECT_STATUS,\@COUNCIL_NAME,\@NO_OF_DOCUMENTS);
}


sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

sub PDFFilecreation()
{
	my $Doc_URL=shift;
	my $Doc_Desc=shift;
	my $document_type=shift;
	my $pdfcount_inc=shift;
	
	my ($file_type,$pdf_name);
	if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	
	if($document_type!~m/^\s*$/is)
	{		
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}
	
	$pdf_name = &PDF_Name($pdf_name);
	return($pdf_name);
}

sub PDF_Name()
{
	my $name=shift;
	$name=decode_entities($name);
	$name=~s/^\W+|\W+$//igs;
	$name=~s/\.\.+/\./igs;
	$name=~s/(\.pdf)+/\.pdf/igs;
	$name=~s/(\.docx)+/\.docx/igs;
	$name=~s/(\.doc)+/\.doc/igs;
	$name=~s/(\.xlsx)+/\.xlsx/igs;
	$name=~s/(\.xls)+/\.xls/igs;
	$name=~s/(\.rtf)+/\.rtf/igs;
	$name=~s/(\.jpg)+/\.jpg/igs;
	$name=~s/(\.jpeg)+/\.jpeg/igs;
	$name=~s/(\.png)+/\.png/igs;
	$name=~s/\./DotChar/igs;
	$name=~s/\W+/_/igs;
	$name=~s/\_\_+/_/igs;
	$name=~s/DotChar/\./igs;
	$name=~s/\W+(\.(?:pdf|docx?|xlsx?|rtf|jpg|jpeg|png))$/$1/igs;
	$name=~s/_+(\.(?:pdf|docx?|xlsx?|rtf|jpg|jpeg|png))$/$1/igs;
	return $name;
}

sub RetrieveMD5()
{
	my $dbh 			= shift;
	my $council_code 	= shift;
	my $format_id	 	= shift;
	
	my $query = "select PDF_MD5 from TBL_PDF_MD5 where council_code=\'$council_code\' and format_id=\'$format_id\'";
	# print "MD5 Qry:::=> $query\n";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@PDF_MD5);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@PDF_MD5,&Trim($record[0]));
	}
	$sth->finish();
	return (\@PDF_MD5);
}

sub Url_Status()
{
	my $Responce_Code=shift;
	my $Responce_Status;
	if($Responce_Code == 200)
	{
		$Responce_Status = 'Y';
	}
	else
	{
		$Responce_Status = 'N';
	}
	return ($Responce_Status);
}

sub Clean()
{
	my $name=shift;
	$name=~s/<[^>]*?>/ /igs;
	$name=~s/\s\s+/ /igs;
	$name=~s/\'//igs;
	$name=~s/\&amp\;/\&/igs;
	$name=~s/&nbsp;/ /igs;	
	$name=~s/^\s+|\s+$//igs;	
	return $name;
}

sub Move_Files()
{
	my $Download_Location = shift;
	my $Temp_pdf_store_path = shift;
	my $Current_Location = shift;
	my $Format_ID = shift;
	
	opendir(my $dh, "$Temp_pdf_store_path") || warn "can't opendir $Temp_pdf_store_path: $!";
	my @File_Names = grep {$_ ne '.' and $_ ne '..'} readdir $dh;
	closedir $dh;
	my $cnt=@File_Names;
	my $Comments;
	my $Spd_date=localtime();
	if($cnt==0)
	{
		print "Folder Empty\n";
		rmtree($Temp_pdf_store_path);
	}
	else
	{
		my $Download_Location_2 = "$Download_Location$Format_ID";
		my $Temp_pdf_store_path = "$Temp_pdf_store_path";
		my $filemove=1;
		re_movefiles:
		eval
		{
			$Download_Location_2=~s/\//\\/igs;
			$Temp_pdf_store_path=~s/\//\\/igs;
			opendir(my $dhf, $Temp_pdf_store_path) || die "can't opendir $Temp_pdf_store_path: $!";
			my @File_Count = grep {$_ ne '.' and $_ ne '..'} readdir $dhf;
			closedir $dhf;
			my $File_Count=@File_Count;			
			if($File_Count > 0)
			{
				unless (-d $Download_Location_2)
				{
					$Download_Location_2=~s/\//\\/igs;
					system("mkdir $Download_Location_2");
				}
				print "File Moving to $Download_Location_2\n";
				my $Copy_Status = system("copy $Temp_pdf_store_path $Download_Location_2");
				if($Copy_Status == 0)
				{
					print "Moved\n";
					if ($Temp_pdf_store_path ne $Current_Location)
					{
						opendir(my $dhf, $Temp_pdf_store_path) || die "can't opendir $Temp_pdf_store_path: $!";
						my @File_Names=readdir($dhf);
						closedir $dhf;
						for(@File_Names)
						{
							if($_!~m/\.pl|\.pm|\.py|\.ini/is)
							{
								my $f="$Temp_pdf_store_path\\$_";
								# print "File: $f\n";
								unlink($f);
							}
						}	
						rmtree($Temp_pdf_store_path);
					}	
				}
				else
				{
					print "Failed To Copy\n";
				}			
			}	
		};
		if($@)
		{
			my $Err=$@;
			print "Error Message: $Err\n";
			
			if($filemove <= 2)
			{				
				$filemove++;
				goto re_movefiles;
			}
			else
			{
				$Comments="$Comments|$Err";
			}
		}
	}
	return($Comments);
}
sub Download()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my $mech			= shift;
	my $ua	= shift;
	my %headers;
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5,$Content);
	my $download_flag=0;

	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "\nFile Already Downloaded\n";
		$code=600;
	}
	else
	{		
		my $downCount = 1;		
		my $pingcount = 1;		
		my ($pdf_content);
		Re_Download:		

		($Content,$code) =&Getcontent($ua,$pdf_url,'',\%headers);
		if($code=~m/^200$/is)
		{
			Re_Download1:
			my $pdf_content=$Content;			
			$pdf_name=&PDF_Name($pdf_name);
			$file_name=$Temp_Download_Location.'\\'.$pdf_name;
			# print "file_name::$file_name\n";
			eval
			{				
				$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "\nDoc Exist\n";
					$MD5='';
					$code=600;
				}
				else
				{
					print "\nDoc not Exist\n";
					
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $file_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();	
					
					my $size = stat($file_name)->size;
					# print "FileSizein_bytes  :: $size\n";
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";

					# $code=200;
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}
				}		
				
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download1;
				}	
			}
		}
		else
		{
			if($pingcount<=3)
			{
				$pingcount++;
				goto Re_Download;
			}
		}
		
	}
	return($code,$Err,$MD5);
}

sub MechDoc_postDownload()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my $mech			= shift;
	my $param	= shift;
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;

	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "\nFile Already Downloaded\n";
		$code=600;
	}
	else
	{		
		my $downCount = 1;		
		my $pingcount = 1;		
		my ($pdf_content);
		Re_Download:	
		$mech->post( $pdf_url, Content => "$param");
		my $Content = $mech->content;
		$code = $mech->status;
				
		if($code=~m/^200$/is)
		{
			Re_Download1:
			my $pdf_content=$Content;			
			$pdf_name=&PDF_Name($pdf_name);
			$file_name=$Temp_Download_Location.'\\'.$pdf_name;
			# print "file_name::$file_name\n";
			eval
			{				
				$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "\nDoc Exist\n";
					$MD5='';
					$code=600;
				}
				else
				{
					print "\nDoc not Exist\n";
					
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $file_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();	
					
					my $size = stat($file_name)->size;
					# print "FileSizein_bytes  :: $size\n";
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";

					# $code=200;
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}
				}		
				
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download1;
				}	
			}
		}
		else
		{
			if($pingcount<=3)
			{
				$pingcount++;
				goto Re_Download;
			}
		}
		
	}
	return($code,$Err,$MD5);
}

sub MechDocDownload()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my $mech			= shift;
	my $Council_Code	= shift;
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;

	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "\nFile Already Downloaded\n";
		$code=600;
	}
	else
	{		
		my $downCount = 1;		
		my $pingcount = 1;		
		my ($pdf_content);
		Re_Download:			
		$mech->get($pdf_url);
		my $Content = $mech->content;
		$code=$mech->status();
		
		if($code=~m/^200$/is)
		{
			Re_Download1:
			my $pdf_content=$Content;			
			$pdf_name=&PDF_Name($pdf_name);
			$file_name=$Temp_Download_Location.'\\'.$pdf_name;
			# print "file_name::$file_name\n";
			eval
			{				
				$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "\nDoc Exist\n";
					$MD5='';
					$code=600;
				}
				else
				{
					print "\nDoc not Exist\n";
					
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $file_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();	
					
					my $size = stat($file_name)->size;
					# print "FileSizein_bytes  :: $size\n";
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";

					# $code=200;
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 5))
					{
						unlink $file_name;
						$code=600;
						sleep(30);
						$downCount++;
						goto Re_Download;
					}
				}		
				
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download1;
				}	
			}
		}
		else
		{
			if($pingcount<=3)
			{
				$pingcount++;
				goto Re_Download;
			}
		}
		
	}
	return($code,$Err,$MD5);
}

sub LWPUA()
{
	my ($ua, $cookiefile, $cookie);
	$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
	$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
	$ua->max_redirect(0); 
	$ua->cookie_jar({});

	$cookiefile = $0;
	$cookiefile =~ s/\.pl/_cookie\.txt/g;
	$cookiefile =~ s/root/logs\/cookiefile/g;
	$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
	$ua->cookie_jar($cookie);
	return($ua,$cookie);
}

sub mechget()
{
	my $url=shift;
	my $mech=shift;
	
	my $pingcount=0;
	
	$mech->proxy(['http','https'], 'http://172.27.137.199:3128');
	reping:
	$mech->get($url);

	my $code=$mech->status();
	my $con=$mech->content();	
	print "code::::$code\n";
	if($code!~m/200/is)
	{
		if($pingcount<=3)
		{
			$mech->proxy(['http','https'], 'http://172.27.137.192:3128');
			$pingcount++;
			goto reping;
		}
	}
	
	return($con,$code);
}

sub mechpost()
{
	my $url=shift;
	my $mech=shift;
	my $param=shift;

	my $pingcount1=0;
	
	$mech->proxy(['http','https'], 'http://172.27.137.199:3128');
	
	reping1:
	$mech->post($url,content => $param);	
		
	my $code=$mech->status();
	my $con=$mech->content();
		# print "code::::$code\n";
	if($code!~m/200/is)
	{
		if($pingcount1<=3)
		{
			$mech->proxy(['http','https'], 'http://172.27.137.192:3128');
			$pingcount1++;
			goto reping1;
		}
	}
	return($con,$code);
}
sub Getcontent()
{
	my $ua=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $headers=shift;
	my $cookie=shift;
	
	my %headers=%$headers;
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	
	foreach my $key(keys%headers)
	{
		$req->header($key=> $headers{$key});
	}
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Cke=$res->header("Set-Cookie");
	print "CODE 1:: $code\n";
	
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		# print "CODE 2:: $code\n";
	}
	elsif($code=~m/30/is)
	{
		# print "CODE 3:: $code\n";
		my $loc=$res->header("location");
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				$Document_Url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		# print "CODE 5:: $code\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$code,$redir_url,$Cke);
}


sub Post_Method()
{
	my $ua=shift;
	my $Doc_Post_URL=shift;
	my $headers=shift;
	my $Post_Content=shift;
	my $cookie=shift;
	my $rerun_count=0;

	my %headers=%$headers;
	# print "Post_Content:::$Post_Content";<STDIN>;
	$Doc_Post_URL =~ s/^\s+|\s+$//g;
	$Doc_Post_URL =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$Doc_Post_URL);

	foreach my $key(keys%headers)
	{
		$req->header($key=> $headers{$key});
	}
	
	$req->content($Post_Content);
	my $res = $ua->request($req);
	
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Cke=$res->header("Set-Cookie");
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$Doc_Post_URL);
			my $u2=$u1->abs;
			my $Redir_url;
			($content,$Redir_url)=&Getcontent($ua,$u2,$Doc_Post_URL,\%headers,$cookie);
		}
	}
	else
	{
		if ($rerun_count < 1)
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	return ($content,$code,$Cke);
}

sub get_cookie_session_details()
{
	my $url = shift;
	my $mech = shift;
	my $keyName = shift;
	
	my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
	$mech->get( $url );	
	my $pingStatus = $mech->status;
	my @all_cookies = $monster->all_cookies;
	my ($cookie,$WebLinkSession,$lastSessionAccess,$MachineTag);
	foreach $cookie ( @all_cookies ) {
		# printf( "key: %s value: %s\n", $cookie->key, $cookie->val);
		$WebLinkSession= $cookie->val if($cookie->key =~m/WebLinkSession/is);
		$lastSessionAccess= $cookie->val if($cookie->key =~m/lastSessionAccess/is);
		$MachineTag= $cookie->val if($cookie->key =~m/MachineTag/is);
	}

	return($WebLinkSession,$lastSessionAccess,$MachineTag);
}