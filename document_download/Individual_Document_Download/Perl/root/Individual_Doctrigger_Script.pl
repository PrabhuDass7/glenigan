use strict;
use Cwd qw(abs_path);
use File::Basename;
use POE qw(Wheel::Run Filter::Reference);
sub MAX_CONCURRENT_TASKS () { 10 }

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";
####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $scriptDirectory = ($basePath.'/root');
my $logDirectory = ($basePath.'/logs');

# Private Module
# require ($libDirectory.'/docDatabase.pm'); 


#----------------------- Get input -----------------------#
my ($Council_Name,$Council_Code, $formatId, $onDemandID, @formatId, @cCode, @onDemandID);

@cCode = qw(302 413 474 2005 2029 628 120 613 2004 2021);
	
# print "cCode:::@cCode";
my $current_date_time=localtime();

my $arrSize = @cCode;

POE::Session->create(
  inline_states => {
    _start      => \&start_tasks,
    next_task   => \&start_tasks,
    task_result => \&handle_task_result,
    task_done   => \&handle_task_done,
    task_debug  => \&handle_task_debug,
    sig_child   => \&sig_child,
  }
);



sub start_tasks 
{
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	
	while (keys(%{$heap->{task}}) < MAX_CONCURRENT_TASKS) 
	{
		my $next_task = shift @cCode;
		my $next_formatId = shift @formatId;
		my $next_onDemandID = shift @onDemandID;
		last unless defined $next_task;
		
		print "Starting GDD task for $next_task $next_formatId...\n";
		
		my $task = POE::Wheel::Run->new(
		Program      => sub { do_stuff($next_task, $next_formatId, $next_onDemandID) },
		StdoutFilter => POE::Filter::Reference->new(),
		StdoutEvent  => "task_result",
		StderrEvent  => "task_debug",
		CloseEvent   => "task_done",
		);
		
		$heap->{task}->{$task->ID} = $task;
		$kernel->sig_child($task->PID, "sig_child");
	}
}

sub do_stuff 
{
	binmode(STDOUT);    # Required for this to work on MSWin32
	my $councilCode   	= shift;
	my $formatId   		= shift;
	my $next_onDemandID = shift;
	
	my $filter = POE::Filter::Reference->new();

	sleep(rand 15);

    my $script_name;	
	if($councilCode=~m/^(302|413|2004|2005)$/is)
	{
		$script_name="$scriptDirectory/Individual_Document_Download.pl";
	}
	elsif($councilCode=~m/^2029$/is)
	{
		$script_name="$scriptDirectory/2029_Doc_Download.pl";
	}
	elsif($councilCode=~m/^628$/is)
	{
		$script_name="$scriptDirectory/628_Doc_Download.pl";
	}
	elsif($councilCode=~m/^120$/is)
	{
		$script_name="$scriptDirectory/120_Doc_Download.pl";
	}
	elsif($councilCode=~m/^613$/is)
	{
		$script_name="$scriptDirectory/613_Doc_Download.pl";
	}
	elsif($councilCode=~m/^2021$/is)
	{
		$script_name="$scriptDirectory/2021_Doc_Download.pl";
	}
	else
	{
		print "councilCode==>$councilCode\n";
	}
	
	my $current_dateTime=localtime();
	
	print "current_date_time: $current_dateTime\n";
	open(my $fh,">>$logDirectory/status_log_file_name.txt");
	print $fh "$councilCode\t$current_dateTime\n";
	close ($fh);

	
	system("start PERL $script_name $councilCode");
	
	open(my $fh,">>$logDirectory/status_log_file_name_All.txt");
	print $fh "$councilCode\t$current_dateTime\tTriggered\n";
	close ($fh);			
		
	
	my %result = (
				task   => $councilCode,
				status => "seems ok to me",
				);

	my $output = $filter->put([\%result]);

	print @$output;
}

sub handle_task_result 
{
	my $result = $_[ARG0];
	print "Result for $result->{task}: $result->{status}\n";
}

sub handle_task_debug 
{
	my $result = $_[ARG0];
	print "Debug: $result\n";
}

sub handle_task_done {
	my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
	delete $heap->{task}->{$task_id};
	$kernel->yield("next_task");
}

sub sig_child {
	my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
	my $details = delete $heap->{$pid};

# warn "$$: Child $pid exited";
}

$poe_kernel->run();

exit 0;