use strict;
use WWW::Mechanize;
use Cwd qw(abs_path);
use Time::Piece;
use File::Basename;
use URI::Encode;
use URI::URL;
use URI::Escape;
use JSON;
use MIME::Base64;
use HTML::Entities;
use Data::Dumper;
use HTML::TableExtract;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
local $ENV{PERL_NET_HTTPS_SSL_SOCKET_CLASS} = 'Net::SSL';

my @ISA = qw(Exporter);
my @EXPORT = ();


my $basePath = dirname (dirname abs_path $0); 
print "$basePath\n";
my $libDirectory = ($basePath.'\\lib');
my $LogDirectory = ($basePath.'\\log');

require ($libDirectory.'\\Download_Generic.pm'); 

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

print "Council_Code: $Council_Code\n";
print "inputFormatID: $inputFormatID\n";
print "libDirectory: $libDirectory\n";

my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');
my $today_date = $time->strftime('%m_%d_%Y');

print "Downloaded_date: $Downloaded_date\n";

my $Application_Form_Keywords = '\s*(Application|Appliction|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';


my $One_App_Location='\\\\172.27.137.202\\One_app_download\\';
# my $One_App_Location=$basePath.'\\data\\';
my $Local_Download_Location=$basePath.'\\data\\documents';
print "Local_Download_Location::$Local_Download_Location\n";
print "One_App_Location::$One_App_Location\n";


my ($dbh,$DBtype)= &Download_Generic::DbConnection();
my $query='Yes';


my($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_Generic::Retrieve_Input($dbh,$Council_Code,$inputFormatID);


# my ($dbh,$DBtype) = &Download_Generic::testDbConnection();

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

##################### Query##################
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	
if($query=~m/Yes/is)
{
	if($COUNCIL_NAME_TMP ne '')
	{
		# print "Dashboard_Insert_Query:;$Dashboard_Insert_Query\n";
		&Download_Generic::Execute($dbh,$Dashboard_Insert_Query,$DBtype);
	}
}

############################################
my $mech;

if($Council_Code == 474)
{
	$mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
}else{

	$mech=WWW::Mechanize->new(autocheck => 0);
}
my($ua,$cookie);
if($Council_Code=~m/^302$/is)
{
	($ua,$cookie)=&Download_Generic::LWPUA();
}
#############################################
my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;
my $Current_Location = $basePath;
my($Update_Query);
my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	print "Format_ID :: $Format_ID\n";
	print "Document_Url :: $Document_Url\n";
	
	$NO_OF_DOCUMENTS=0 if($NO_OF_DOCUMENTS == '');
	
	my $Doc_Details_Hash_Ref=&main($Document_Url,$Council_Code,$Application_No);
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	
	my $pdfcount=keys %Doc_Details;
	print "pdfcount::::::::::$pdfcount\n";
	
	$Todays_Count = $Todays_Count + $pdfcount;
	my $No_Of_Doc_Downloaded=0;
	my $RetrieveMD5=[];
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$RetrieveMD5 =&Download_Generic::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
	}
	else
	{
		goto DB_Flag;
	}
	my $Temp_Download_Location = $Local_Download_Location.'\\'.$Format_ID;
	print "Temp_Download_Location=>$Temp_Download_Location\n";
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====> $Temp_Download_Location\n";
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		# print "pdf_url: $pdf_url\n";
		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		
		# print "Doc_Type: $Doc_Type\n";
		print "Pdf_Name: $Pdf_Name\n";
		# print "Doc_Published_Date: $Doc_Published_Date\n";
		# print "Doc_Desc: $Doc_Desc\n";
		# print "pdf_url: $pdf_url\n";
		# print "Markup: $Markup\n";
		$Pdf_Name=~s/WITHOUT_PRESONAL_DATA/Application_Form_without_personal_data/igs;
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				my ($D_Code, $Err_Msg, $MD5);
				
				if($Council_Code=~m/^302$/is)
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Generic::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech,$ua);
				}
				else
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Generic::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech);
				}
				print "code:::$D_Code\n";
				my ($Download_Status)=&Download_Generic::Url_Status($D_Code);
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;					
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my ($Download_Status);
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5);
				
					if($Council_Code=~m/^302$/is)
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Generic::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech,$ua);
					}
					else
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Generic::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech);
					}
					($Download_Status)=&Download_Generic::Url_Status($D_Code);				
						
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
					}	
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
				}
			}
		}	
	}	
	my($file_comment)=&Download_Generic::Move_Files($One_App_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	if(length($file_comment)!=0)
	{
		$Comments=$Comments.'|'.$file_comment if($file_comment!~m/^\s*$/is);
	}
	DB_Flag:
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		my $No_Of_Doc_Downloaded_TMP = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		$No_Of_Doc_Downloaded=$No_Of_Doc_Downloaded_TMP;
	}
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";
	
	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;
	
	if($One_App_Name=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}
	}	
}
if($COUNCIL_NAME_TMP ne '')
{
	my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
	$Update_Query.= $Status_Update_Query;
}
$Insert_OneAppLog=~s/\,$//igs;
$Insert_Document=~s/\,$//igs;
$Update_Query=~s/\,$//igs;
$MD5_Insert_Query=~s/\,$//igs;

print "\n$MD5_Insert_Query\n";
print "\n$Insert_Document\n";
print "\n$Insert_OneAppLog\n";
print "\n$Update_Query\n";

open ss,">>$LogDirectory\\Query_Execution_$today_date.txt";
print ss "$Downloaded_date\t$Update_Query\t$Insert_OneAppLog\t$Insert_Document\t$MD5_Insert_Query\n";
close ss;
if($query=~m/Yes/is)
{	
	&Download_Generic::Execute($dbh,$Insert_OneAppLog,$DBtype) if($Insert_OneAppLog!~m/values\s*$/is);	
	&Download_Generic::Execute($dbh,$Insert_Document,$DBtype)  if($Insert_Document!~m/values\s*$/is);	
	&Download_Generic::Execute($dbh,$Update_Query,$DBtype) if($Update_Query!~/^\s*$/is);	
	&Download_Generic::Execute($dbh,$MD5_Insert_Query,$DBtype) if($MD5_Insert_Query!~/values\s*$/is);
}

sub main()
{
	my $Document_Url=shift;
	my $Council_Code=shift;
	my $Application_No=shift;
	
	
	my %Doc_Details;
	if($Council_Code=~m/^2005$/is)
	{
		my($Doc_cont,$code)=&Download_Generic::mechget($Document_Url,$mech);
		if($Doc_cont=~m/(<table[^>]*?>[\w\W]*?<\/table>)/is)
		{
			my $block1=$1;
			$block1=~s/<tr[^>]*?valign="top"[^>]*?>[\w\W]*?<\/tr>/ /igs;

			my $pdfcount_inc=1;
			while($block1=~m/(<tr[^>]*?>[\w\W]*?<\/tr>)/igs)
			{
				my $block2=$1;
				my @fullblock;
				while($block2=~m/(<td[^>]*?>[\w\W]*?<\/td>)/igs)
				{
					my $values=$1;
					push(@fullblock,$values)
				}
				if(length(@fullblock)!=0)
				{
					my $Doclink_blk=$fullblock[1];
					my $Pub_date=$fullblock[2];
					my $doc_type=$fullblock[4];
					
					my $doc_link=$1 if($Doclink_blk=~m/href\s*=\s*(?:"|')\s*([^>]*?)\s*(?:"|')/is);
					$doc_link=~s/&amp;/&/igs;
					my($Doc_cont,$code)=&Download_Generic::mechget($doc_link,$mech);
					
					my $dochead_lnk="http://dccwebr10.donegalcoco.ie/R8miniweb/$1" if($Doc_cont=~m/<frame[^>]*?name="header"[^>]*?src\s*=\s*"\s*([^>]*?)\s*"/is);
					my($Doc_cont,$code)=&Download_Generic::mechget($dochead_lnk,$mech);
					
					my $VS=uri_escape($1) if($Doc_cont=~m/id="__VIEWSTATE"[^>]*?value\s*=\s*"\s*([^>]*?)\s*"/is);
					my $VSG=uri_escape($1) if($Doc_cont=~m/id="__VIEWSTATEGENERATOR"[^>]*?value\s*=\s*"\s*([^>]*?)\s*"/is);
					my $EV=uri_escape($1) if($Doc_cont=~m/id="__EVENTVALIDATION"[^>]*?value\s*=\s*"\s*([^>]*?)\s*"/is);
					
					if($Doc_cont=~m/id="CombineLink"/is)
					{
						my $param="__EVENTTARGET=CombineLink&__EVENTARGUMENT=&__VIEWSTATE=$VS&__VIEWSTATEGENERATOR=$VSG&__EVENTVALIDATION=$EV&hdnOffset=-330";
						($Doc_cont,$code)=&Download_Generic::mechpost($dochead_lnk,$mech,$param);
					}
								
					if($Doc_cont=~m/ShowOtherDoc\s*\(\s*'\s*([^>]*?)\s*'\s*,\s*'\s*([^>]*?)\s*'\s*,\s*'\s*([^>]*?)\s*'/is)
					{
						my $docs=$1;
						my $sess=$2;
						my $type=$3;
						my $gen_link="http://dccwebr10.donegalcoco.ie/R8miniweb/GeneralViewer.aspx?docurl=$docs&sessionid=$sess&miniwebtype=$type";
						my($Doc_cont,$code)=&Download_Generic::mechget($gen_link,$mech);
						
						my $finallink=($1) if($Doc_cont=~m/<iframe[^>]*?style[^>]*?src\s*=\s*"\s*([^>]*?)\s*"/is);
						
						my $Doc_URL=$finallink;
						my($Doc_Desc,$document_type,$Published_Date);
						$Doc_Desc = &Download_Generic::Clean($Doclink_blk);
						$document_type = &Download_Generic::Clean($doc_type);
						$Published_Date = &Download_Generic::Clean($Pub_date);
						$document_type=~s/&amp/&/igs;
						$Doc_Desc=~s/&amp/&/igs;
						my $pdf_name=&Download_Generic::PDFFilecreation($Doc_URL,$document_type,$Doc_Desc,$pdfcount_inc);
						$Doc_URL=decode_entities($Doc_URL);
						
						if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
						{
							$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
						}
					}
				}
				$pdfcount_inc++;
			}
		}
	}
	# if($Council_Code=~m/^474$/is)
	# {
		# my($Doc_cont,$code)=&Download_Generic::mechget($Document_Url,$mech);

		# my $OBJREF=uri_escape($1) if($Doc_cont=~m/var\s*objectref\s*=\s*\"\s*([^>]*?)\s*\"/is);
		# my $TYPE_ID=uri_escape($1) if($Doc_cont=~m/var\s*objecttypeid\s*=\s*\"\s*([^>]*?)\s*\"/is);
		# my $csrf_token=uri_escape($1) if($Doc_cont=~m/name=\"csrf-token\"\s*content\=\"\s*([^>]*?)\s*\"/is);
		# my $pingcount1=0;
	
		# my $Docurl="https://portal.peakdistrict.gov.uk/system/loadview";
		
		# $mech->add_header( 'Accept' => '*/*');
		# $mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
		# $mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
		# $mech->add_header( 'Connection' => 'keep-alive');
		# $mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8');
		# $mech->add_header( 'Host' => 'portal.peakdistrict.gov.uk');
		# $mech->add_header( 'Origin' => 'https://portal.peakdistrict.gov.uk');
		# $mech->add_header( 'Referer' => $Document_Url);
		# $mech->add_header( 'Sec-Fetch-Dest' => 'empty');
		# $mech->add_header( 'Sec-Fetch-Mode' => 'cors');
		# $mech->add_header( 'Sec-Fetch-Site' => 'same-origin');
		# $mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');
		# $mech->add_header( 'X-CSRF-TOKEN' => $csrf_token);
		# $mech->add_header( 'X-Requested-With' => 'XMLHttpRequest');
		
		# my $param="view=modules.PlanningSearch.partial.documents.documents&objectref=$OBJREF&objecttypeid=$TYPE_ID&placeholder_data=";
		
		# my($Doc_cont,$code)=&Download_Generic::mechpost($Docurl,$mech,$param);
		
		# my $cont;
		# eval
		# {
			# my $json = JSON->new;
			# my $data = $json->decode($Doc_cont);
			# $cont=$data->{'content'};
		# };
		# if($cont ne '')
		# {
			# my $pdfcount_inc=1;	
			# my $data=$1 if($cont=~m/(<table[^>]*?>[\w\W]*?<\/table>)/is);
			# $data=~s/<thead[\w\W]*?<\/thead>//igs;
			# while($data=~m/(<tr[^>]*?>[\w\W]*?<\/tr>)/igs)
			# {
				# my $val=$1;
				# my @rows;
				# while($val=~m/(<td[^>]*?>[\w\W]*?<\/td>)/igs)
				# {
					# my $column=$1;
					# push(@rows,$column);
				# }
				# if(length(@rows)!=0)
				# {
					# my $download_link=$rows[0];
					# my $Doctype=$rows[2];
					# my $document_type=$rows[1];
					# my $Published_Date=&Download_Generic::Clean($rows[4]);
					
					# my $Doc_URL=$1 if($download_link=~m/href\s*=\s*"\s*([^>]*?)\s*"/is);
					# my $Doc_Desc=$1 if($Doctype=~m/<span\s*class[^>]*?description[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
					# $document_type=$1 if($Doctype=~m/<span\s*class[^>]*?original_name[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);		
					
					# $Doc_Desc = &Download_Generic::Clean($Doc_Desc);
					# $document_type = &Download_Generic::Clean($document_type);
					# $Published_Date = &Download_Generic::Clean($Published_Date);				
					
					# $Doc_URL=decode_entities($Doc_URL);
					
					# my $pdf_name;
					# if(($Doc_Desc!~m/(?:Application|Appliction)/is) && ($document_type=~m/(?:Application|Appliction)/is))
					# {						
						# $pdf_name=&Download_Generic::PDFFilecreation($Doc_URL,$Doc_Desc,$document_type,$pdfcount_inc);
					# }
					# else
					# {						
						# $pdf_name=&Download_Generic::PDFFilecreation($Doc_URL,$document_type,$Doc_Desc,$pdfcount_inc);
					# }
					# if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					# {
						# $Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
					# }
					# $pdfcount_inc++;
				# }
			
			# }
		# }
	# }
	if($Council_Code=~m/^302$/is)
	{
		my %headers;
		my $home_url = 'https://www.nuneatonandbedworth.gov.uk/info/20025/planning_and_building_control/54/planning/2';
		
		my($home_content,$Code,$Redir_Url,$cke) =&Download_Generic::Getcontent($ua,$home_url,$URL,\%headers,$cookie);
		my $php_session_id=$1 if($cke=~m/PHPSESSID\=([^>]*?)\;/is);
		my $uri_obj = URI::Encode->new( { encode_reserved => 0 } );
		if($home_content=~m/<a[^<]*?href\=\"([^<]*?)\">\s*Search\s*planning\s*applications\s*<\/a>/is)
		{
			my $search_url=$1;
			my($content1,$Code,$Redir_Url,$cke) =&Download_Generic::Getcontent($ua,$search_url,$URL,\%headers,$cookie);

			my $php_session_id=$1 if($cke=~m/PHPSESSID\=([^>]*?)\;/is);
			my $form_id=$1 if($content1=~m/\"formID\"\:\"([^<]*?)\"/is);
			my $processID=$1 if($content1=~m/\"processID\"\:\"([^<]*?)\"/is);
			my $stage_id=$1 if($content1=~m/\"stage-id\"\:\"([^<]*?)\"/is);

			my $form_url = 'https://customer.nuneatonandbedworth.gov.uk/authapi/isauthenticated?uri=https://customer.nuneatonandbedworth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://'.$processID.'/'.$stage_id.'/definition.json&redirectlink=/en&cancelRedirectLink=/en&consentMessage=yes&noLoginPrompt=1&hostname=customer.nuneatonandbedworth.gov.uk';

			my($form_content,$Code,$Redir_Url,$Cke) =&Download_Generic::Getcontent($ua,$form_url,$URL,\%headers,$cookie);

			# print "php_session_id:$php_session_id";<STDIN>;
			# print "Cke:$Cke";<STDIN>;
			$form_content = $uri_obj->decode($form_content);
			$form_content=~s/\\//igs;

			my $sid=$1 if($form_content=~m/\"auth-session\"\:\"([^<]*?)\"/is);
			my $processID=$1 if($form_content=~m/\/(AF-Process[^<]*?)\//is);
			my $stage_id=$1 if($form_content=~m/\/(AF-Stage[^<]*?)\//is);
			my $AWSALB=$1 if($form_content=~m/AWSALB=\s*([^>]*?)\s*;/is);
			my $AWSALBCORS=$1 if($form_content=~m/AWSALBCORS=\s*([^>]*?)\s*;/is);
			my $csrftkn=$1 if($form_content=~m/SESSIONCHECK=\s*([^>]*?)\s*;/is);

			my $site_base_64='https://customer.nuneatonandbedworth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://'.$processID.'/'.$stage_id.'/definition.json&redirectlink=/en&cancelRedirectLink=/en&consentMessage=yes&noLoginPrompt=1';
			my $site_base_64_encoded=MIME::Base64::encode($site_base_64);
			$site_base_64_encoded =~ s/^\s+|\s+$//igs;
			$site_base_64_encoded =~ s/\s+//igs;

			my %manual_headers=("Accept-Language"=>"en-US,en;q=0.5","Content-Type"=>"application/json","Host"=>"customer.nuneatonandbedworth.gov.uk","X-Requested-With"=>"XMLHttpRequest","Cookie"=>"_ga=GA1.3.1623640699.1595319059; _gid=GA1.3.1024878546.1596432304; PHPSESSID=$php_session_id; APP_LANG=en; _gat=1; localtime=2020-08-03 12:51:48; AWSALB=$AWSALB; AWSALBCORS=$AWSALBCORS");
			
			my $Final_url='https://customer.nuneatonandbedworth.gov.uk/apibroker/runLookup?id=5d2f0189988c3&repeat_against=&noRetry=false&getOnlyTokens=undefined&log_id=&app_name=AF-Renderer::Self&_='.time.'&sid='.$sid;

			my $siteid="https://customer.nuneatonandbedworth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://AF-Process-bb4f1551-25f7-4c9d-9504-359555b4764c/AF-Stage-52bb890a-7580-4889-8b19-31d27fd8a3bd/definition.json&redirectlink=/en&cancelRedirectLink=/en&consentMessage=yes&noLoginPrompt=1";
			
			my $Fin_postcon='{"stopOnFailure":true,"usePHPIntegrations":true,"stage_id":"'.$stage_id.'","stage_name":"Search","formId":"AF-Form-'.$form_id.'","formValues":{"Section 1":{"selectedReference":{"name":"selectedReference","type":"text","id":"AF-Field-60be7b82-5064-4869-9d1d-eb4853b04099","value_changed":true,"section_id":"AF-Section-89ea27cc-be1d-4d9f-9cc6-ff94d8e813c3","label":"selected reference","value_label":"","hasOther":false,"value":"'.$Application_No.'","path":"root/selectedReference","valid":"","totals":"","suffix":"","prefix":"","summary":"","hidden":false,"_hidden":true,"isSummary":false,"staticMap":false,"isMandatory":false,"isRepeatable":false,"currencyPrefix":"","decimalPlaces":"","hash":""}}},"isPublished":true,"formName":"Search Planning Applications","processId":"'.$processID.'","tokens":"","env_tokens":"","site":"'.$site_base_64_encoded.'","processName":"Search planning applications","reference":"","formUri":"sandbox-publish://'.$processID.'/'.$stage_id.'/definition.json"}';
			
			
			my($final_content,$Code,$Redir_Url) =&Download_Generic::Post_Method($ua,$Final_url,\%manual_headers,$Fin_postcon,$cookie);
			
			my $cont;
			eval
			{
				my $json = JSON->new;
				my $data = $json->decode($final_content);
				$cont=$data->{'data'};					
			};
			my $pdfcount_inc=1;
			while($cont=~m/(<Row[\w\W]*?<\/Row>)/igs)
			{
				my $blk=$1;
				my $Doc_Desc=$1 if($blk=~m/<result[^>]*?column="display"[^>]*?>([\w\W]*?)<\/result>/is);
				my $Doc_URL=$1 if($blk=~m/<result[^>]*?column="value"[^>]*?>([\w\W]*?)<\/result>/is);
				
				my ($document_type,$Published_Date);
				$Doc_URL=decode_entities($Doc_URL);
				$Doc_Desc=&Download_Generic::Clean($Doc_Desc);
								
				my ($pdf_name)=&Download_Generic::PDFFilecreation($Doc_URL,$document_type,$Doc_Desc,$pdfcount_inc);
			
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
				}
				$pdfcount_inc++;
			
			}
		}
	}
	if($Council_Code=~m/^413$/is)
	{
		my %headers;
		my $URL="http://vogonline.planning-register.co.uk/Plastandard.aspx";		
		
		$headers{'Host'}='vogonline.planning-register.co.uk';
		$headers{'Content_Type'}='application/x-www-form-urlencoded; charset=UTF-8';
		$headers{'Referer'}=$Document_Url;
		print %headers;
		
		my($ua,$cookie)=&Download_Generic::LWPUA();
		my($Content,$Code,$Redir_Url) =&Download_Generic::Getcontent($ua,$Document_Url,$URL,\%headers,$cookie);
		my $Viewstate=uri_escape($1) if($Content=~m/<input[^>]*?name="__VIEWSTATE"[^>]*?value="\s*([^>]*?)\s*"[^>]*?>/is);
		my $Viewstategenerator=$1 if($Content=~m/<input[^>]*?name\s*=\s*"__VIEWSTATEGENERATOR"[^>]*?value="\s*([^>]*?)\s*"[^>]*?>/is);
		my $Eventvalidation=uri_escape($1) if($Content=~m/<input[^>]*?name="__EVENTVALIDATION"[^>]*?\s*value="\s*([^>]*?)\s*"[^>]*?>/is);
		
		my $CLIENTSTATE=($1) if($Content=~m/<input[^>]*?name="ContentPlaceHolder1_TabContainer1_ClientState"[^>]*?\s*value="\s*([^>]*?)\s*"[^>]*?>/is);
		my $RECNO=uri_escape($1) if($Content=~m/<input[^>]*?ContentPlaceHolder1_hidRecNo"[^>]*?\s*value="\s*([^>]*?)\s*"[^>]*?>/is);
		
		
		$CLIENTSTATE=decode_entities($CLIENTSTATE);
		$CLIENTSTATE=uri_escape($CLIENTSTATE);
		my $Post_Content_Tmp="ctl00%24ScriptManager1=ctl00%24ScriptManager1%7Cctl00%24ContentPlaceHolder1%24btnDocumentsTrigger&__EVENTTARGET=ctl00%24ContentPlaceHolder1%24btnDocumentsTrigger&__EVENTARGUMENT=&ContentPlaceHolder1_TabContainer1_ClientState=$CLIENTSTATE&__VIEWSTATE=$Viewstate&__VIEWSTATEGENERATOR=$Viewstategenerator&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=$Eventvalidation&ctl00%24ContentPlaceHolder1%24hidRecNo=$RECNO&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtLocation=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtEasting=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtNorthing=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtProposal=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtParish=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAppType=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtStatus=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAppName=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAgtName=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAppAddress=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAgtAddress=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtOffName=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtRec=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtOffPhone=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtComplete=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtExpiry=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtDecisionType=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDecision%24txtDecision=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDecision%24txtDecDate=&__ASYNCPOST=true&";
		my $nxtcon;
		
		my $pdfcount_inc=1;
		Nextping:
		($Content,my $res_code)=&Download_Generic::Post_Method($ua,$Document_Url,\%headers,$Post_Content_Tmp,$cookie);
		if($Content=~m/(<table[\w\W]*?<\/table>)/is)
		{
			my $data=$1;
			my $te = HTML::TableExtract->new(keep_html => 1,headers =>[qw(Created Name)]);
			$te->parse( $data );
				
			foreach my $ts ( $te->tables() )
			{			
				foreach my $row ( $ts->rows() )
				{
					my $link="@$row[1]"; 
					my $Doc_Desc = &Download_Generic::Clean(@$row[1]);
					my $Published_Date = &Download_Generic::Clean(@$row[0]);
					my($Doc_URL,$document_type);
					if($link=~m/href\=\"\s*([^>]*?)\s*\"/is)
					{
						my $docurl="$1";
						
						if($docurl!~m/http/is)
						{			
							my $Root_Url="http://vogonline.planning-register.co.uk/";
							my $u1=URI::URL->new($docurl,$Root_Url);
							my $u2=$u1->abs;
							$Doc_URL=$u2;
						}
						else
						{
							$Doc_URL=$docurl;
						}
					}
					$Doc_URL=decode_entities($Doc_URL);
					$Doc_Desc=&Download_Generic::Clean($Doc_Desc);
					$Published_Date=&Download_Generic::Clean($Published_Date);
					$document_type=&Download_Generic::Clean($document_type);
									
					my ($pdf_name)=&Download_Generic::PDFFilecreation($Doc_URL,$document_type,$Doc_Desc,$pdfcount_inc);
					
					if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					{
						$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
					}
					$pdfcount_inc++;					
				}
			}
		}
		$nxtcon.=$Content;
		if($Content=~m/<span>\s*[\d]+\s*<\/span>\s*[^>]*?<a[^>]*?href\s*=\s*"\s*javascript[^>]*?doPostBack\s*[^>]*?>\s*([\d]+)\s*<\/a>/is)
		{
			my $EVENTARGUMENT="Page%24$1";
			
			my $Viewstate=uri_escape($1) if($Content=~m/VIEWSTATE\|\s*([^>]*?)\s*\|/is);
			my $Viewstategenerator=$1 if($Content=~m/VIEWSTATEGENERATOR\|\s*([^>]*?)\s*\|/is);
			my $Eventvalidation=uri_escape($1) if($Content=~m/EVENTVALIDATION\|\s*([^>]*?)\s*\|/is);
			
			
			my $Next_Page_Post_ContentTMP="ctl00%24ScriptManager1=ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDocuments%24updatePanel3%7Cctl00%24ContentPlaceHolder1%24TabContainer1%24tabDocuments%24gvDocs&ContentPlaceHolder1_TabContainer1_ClientState=$CLIENTSTATE&ctl00%24ContentPlaceHolder1%24hidRecNo=$RECNO&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtLocation=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtEasting=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtNorthing=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtProposal=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtParish=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAppType=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtStatus=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAppName=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAgtName=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAppAddress=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDetails%24txtAgtAddress=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtOffName=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtRec=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtOffPhone=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtComplete=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtExpiry=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabOtherDetails%24txtDecisionType=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDecision%24txtDecision=&ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDecision%24txtDecDate=&__EVENTTARGET=ctl00%24ContentPlaceHolder1%24TabContainer1%24tabDocuments%24gvDocs&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$Viewstate&__VIEWSTATEGENERATOR=$Viewstategenerator&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=$Eventvalidation&__ASYNCPOST=true&";
			$Post_Content_Tmp=$Next_Page_Post_ContentTMP;	
			goto Nextping;			
		}
	}
	if($Council_Code=~m/^2004$/is)
	{
		print "Council_Code::$Council_Code\n";
		my $home_url = 'http://maps.corkcoco.ie/planningenquiryv3/MainFrames.aspx';
		my($Doc_cont,$code)=&Download_Generic::mechget($home_url,$mech);		
		
		my($Doc_cont,$code)=&Download_Generic::mechget($Document_Url,$mech);		
		
		my $doc_id=$1 if($Document_Url=~m/[^>]*?fullFileNumber=([^>]*?)$/is);
		
		my $docnav_url='http://maps.corkcoco.ie/SharepointReports/PlanningDocs.aspx?appFileNum='.$doc_id;
		$mech->add_header("Host" => "maps.corkcoco.ie");
		$mech->add_header("Accept" => "text/html, application/xhtml+xml, */*");
		$mech->add_header("Referer" => "$Document_Url");
		
		my($Doc_cont,$code)=&Download_Generic::mechget($docnav_url,$mech);		
		
		my $docnav_url1='http://maps.corkcoco.ie/SharepointReports/PlanningDocs.aspx?appFileNum='.$doc_id.'&StartSearch=true ';
	
		$mech->add_header("Host" => "maps.corkcoco.ie");
		$mech->add_header("Accept" => "text/html, application/xhtml+xml, */*");
		$mech->add_header("Referer" => "$docnav_url");
		
		my($Doc_cont,$code)=&Download_Generic::mechget($docnav_url1,$mech);		
		$Doc_cont=~s/\\u003c/</igs;
		$Doc_cont=~s/\\u003e/>/igs;
		$Doc_cont=~s/\\u0027/"/igs;
		$Doc_cont=~s/\\n//igs;
					
		while($Doc_cont=~m/<div[^>]*?\([\'\"](DisplayDocument.aspx\?DocURL=[^>]*?FileName=([^>]*?))[\'\"]\)[^>]*?>[\w\W]*?<span[^>]*?class=[\"\']treeDocLeaf\s*treeLeafHover[\"\'][^>]*?>([^>]*?\(?([\d][^>]*?)\)?)\s*</igs)
		{
			my $Doc_URL='http://maps.corkcoco.ie/SharepointReports/'.$1;
			my $Doc_Desc=$3;
			my $Doc_Published_Date=$4;
			$Doc_Desc = &Download_Generic::Clean($Doc_Desc);
			$Doc_Published_Date = &Download_Generic::Clean($Doc_Published_Date);
			my $Doc_Type = &Download_Generic::Clean($Doc_Desc);
			
			my $Pdf_Name=&Download_Generic::PDFFilecreation($Doc_URL,$Doc_Type,$Doc_Desc,$pdfcount_inc);
			
			$Doc_Details{$Doc_URL}=[$Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_URL,$Doc_Desc];
			$pdfcount_inc++;
		}
	}
	
	return(\%Doc_Details);
}