use strict;
use WWW::Mechanize;
use Cwd qw(abs_path);
use Time::Piece;
use File::Basename;
use URI::Encode;
use URI::URL;
use URI::Escape;
use JSON;
use MIME::Base64;
use HTML::Entities;
use Data::Dumper;
use HTML::TableExtract;
use IO::Socket::SSL qw();

my $basePath = dirname (dirname abs_path $0); 
print "$basePath\n";
my $libDirectory = ($basePath.'\\lib');
my $LogDirectory = ($basePath.'\\log');

require ($libDirectory.'\\Download_Generic.pm'); 

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

print "Council_Code: $Council_Code\n";
print "inputFormatID: $inputFormatID\n";
print "libDirectory: $libDirectory\n";

my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');
my $today_date = $time->strftime('%m_%d_%Y');

print "Downloaded_date: $Downloaded_date\n";

my $Application_Form_Keywords = '\s*(Application|Appliction|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';


my $One_App_Location='\\\\172.27.137.202\\One_app_download\\';
# my $One_App_Location=$basePath.'\\data\\';
my $Local_Download_Location=$basePath.'\\data\\documents';
print "Local_Download_Location::$Local_Download_Location\n";
print "One_App_Location::$One_App_Location\n";


my ($dbh,$DBtype)= &Download_Generic::DbConnection();
my $query='Yes';


my($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_Generic::Retrieve_Input($dbh,$Council_Code,$inputFormatID);

# my ($dbh,$DBtype) = &Download_Generic::testDbConnection();

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

##################### Query##################
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	
if($query=~m/Yes/is)
{
	if($COUNCIL_NAME_TMP ne '')
	{
		# print "Dashboard_Insert_Query:;$Dashboard_Insert_Query\n";
		&Download_Generic::Execute($dbh,$Dashboard_Insert_Query,$DBtype);
	}
}

############################################
my $mech = WWW::Mechanize->new( 
		agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
		ssl_opts => {
		SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
		verify_hostname => 0, 
		}, autocheck => 0);

my($ua,$cookie)=&Download_Generic::LWPUA();
#############################################

my $Todays_Count=0;
my $Todays_Downloaded_Count=0;
my $Current_Location = $basePath;
my($Update_Query);
my $reccount = @{$Format_ID};

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	print "Format_ID :: $Format_ID\n";
	
	$NO_OF_DOCUMENTS=0 if($NO_OF_DOCUMENTS == '');
	my $No_Of_Doc_Downloaded=0;
	my $RetrieveMD5=[];
	my %Doc_Details;
	
	my $pdfcount_inc=0;	
	
	if($Council_Code=~m/^2021$/is)
	{
		print "Council_Code::$Council_Code\n";
		my($Doc_cont,$code)=&Download_Generic::mechget($Document_Url,$mech);		
		
		my $Document_ListUrl='https://portal.monaghancoco.ie/WeblinkPlanningPortal/SearchService.aspx/GetSearchListing';
		my $doc_id=$1 if($Document_Url=~m/[^>]*?file=\s*([^>]*?)\s*$/is);
		my $param='{"repoName":"MONLFPLANNING","searchSyn":"{LF:Name~='.$doc_id.'}","searchUuid":"","sortColumn":"","startIdx":0,"endIdx":20,"getNewListing":true,"sortOrder":2,"displayInGridView":false}';
		
		$mech->add_header("Content-Type" => "application/json");
		$mech->add_header("Accept" => "application/json, text/plain, */*");
		$mech->add_header("Host" => "portal.monaghancoco.ie");
		my($Doc_contlist,$code)=&Download_Generic::mechpost($Document_ListUrl,$mech,$param);

		my $search_id=$1 if($Doc_contlist=~m/"\s*searchUUID\s*"\s*:\s*"\s*([^>]*?)\s*"/is);
		print "search_id::$search_id\n";

		if($Doc_contlist=~m/"entryId":\s*([^>]*?)\s*,/is)
		{
			while($Doc_contlist=~m/"entryId":\s*([^>]*?)\s*,/igs)
			{
				my $entryid=$1;
				print "entryid::$entryid\n";
				my $Document_ViewUrl="https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocView.aspx?id=$entryid&dbid=0&repo=MONLFPLANNING&searchid=$search_id";
				my($Doc_contview,$code)=&Download_Generic::mechget($Document_ViewUrl,$mech);
		
				my $Document_InfoUrl="https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/GetBasicDocumentInfo";
				my $param='{"repoName":"MONLFPLANNING","entryId":'.$entryid.'}';

				$mech->add_header("Content-Type" => "application/json");
				$mech->add_header("Referer" => $Document_ViewUrl);
				$mech->add_header("Accept" => "application/json, text/plain, */*");
				$mech->add_header("Host" => "portal.monaghancoco.ie");

				my($Doc_continfo,$code)=&Download_Generic::mechpost($Document_InfoUrl,$mech,$param);

				my $page=$1 if($Doc_continfo=~m/pageCount\"\:([\d]+)\,/is);
				print "page::$page\n\n";

				my $Document_GenUrl="https://portal.monaghancoco.ie/WeblinkPlanningPortal/GeneratePDF10.aspx?key=$entryid&PageRange=1%20-%20$page&Watermark=0";
				my $param="{}";
				
				$mech->add_header("Content-Type" => "application/json");
				$mech->add_header("Accept" => "application/json, text/plain, */*");
				$mech->add_header("Referer" => $Document_ViewUrl);
				$mech->add_header("Host" => "portal.monaghancoco.ie");
				print "Document_GenUrl::$Document_GenUrl\n";

				my($Doc_contgen,$code)=&Download_Generic::mechpost($Document_GenUrl,$mech,$param);

				my $id=$1 if($Doc_contgen=~m/^\s*([^>]*?)\s*<html/is);
				$id=~s/^\s*|\s*$//igs;
				print "id::$id\n\n";
				my $Document_WebUrl="https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/PDFTransition";
				my $param='{"Key":"'.$id.'"}';
				print "Document_WebUrl::$Document_WebUrl\n";

				my ($WebLinkSession,$lastSessionAccess,$MachineTag) = &Download_Generic::get_cookie_session_details($Document_WebUrl,$mech,'JSESSIONID');
				
				$mech->add_header("Accept" => "application/json, text/plain, */*");
				$mech->add_header("Content-Type" => "application/json");
				$mech->add_header("Host" => "portal.monaghancoco.ie");
				$mech->add_header("Referer" => $Document_ViewUrl);
				$mech->add_header( 'Cookie' => 'WebLinkSession='.$WebLinkSession.'; lastSessionAccess='.$lastSessionAccess.'; AcceptsCookies=1; MachineTag='.$MachineTag) if($WebLinkSession!~m/^WebLinkSession$/is);
				
				my($Doc_contgen,$code)=&Download_Generic::mechpost($Document_GenUrl,$mech,$param);

				my $Document_PdfUrl = 'https://portal.monaghancoco.ie/WeblinkPlanningPortal/PDF10/'.$id.'/'.$entryid;
				
				my $Doc_URL=$Document_PdfUrl;				
				my $Doc_Type=$1 if($Doc_continfo=~m/\"name\"\s*\:\s*\"Document Type\"\s*\,\s*\"values\"\s*\:\s*\[\s*\"([^>]*?)\"\s*\]/is);
				my $Doc_Desc=$1 if($Doc_continfo=~m/\"name\"\s*\:\s*\"([^>]*?)\"/is);
				my $Doc_Published_Date=$1 if($Doc_continfo=~m/\"created\"\s*\:\s*\"([^>]*?)\"/is);
				
				$Doc_Desc = &Download_Generic::Clean($Doc_Desc);
				$Doc_Published_Date = &Download_Generic::Clean($Doc_Published_Date);
				$Doc_Type = &Download_Generic::Clean($Doc_Desc);
				
				my $Pdf_Name=&Download_Generic::PDFFilecreation($Doc_URL,$Doc_Type,$Doc_Desc,$pdfcount_inc);
				
				$Doc_Details{$Doc_URL}=[$Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_URL,$Doc_Desc];
				$pdfcount_inc++;
			}
		}
	}
	my $pdfcount=$pdfcount_inc;
	$Todays_Count = $Todays_Count + $pdfcount;

	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$RetrieveMD5 =&Download_Generic::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
	}
	else
	{
		goto DB_Flag;
	}
	
	my $Temp_Download_Location = $Local_Download_Location.'\\'.$Format_ID;
	print "Temp_Download_Location=>$Temp_Download_Location\n";
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====> $Temp_Download_Location\n";
	my $pdfcount_inc=1;
	my($Comments,$One_App_Avail_Status,$One_App_Name);
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		print "pdf_url::$pdf_url\n";

		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_URL,$Doc_Desc);
				
		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_URL=$Doc_Details{$pdf_url}->[3];
		$Doc_Desc=$Doc_Details{$pdf_url}->[4];
		
		my $Document_Url=$Doc_URL;
		
		$Pdf_Name=~s/WITHOUT_PRESONAL_DATA/Application_Form_without_personal_data/igs;
		
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{				
				my ($D_Code, $Err_Msg, $MD5);
				
				if($Council_Code=~m/^2021$/is)
				{
					my $Document_WebUrl="https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/PDFTransition";
					my ($WebLinkSession,$lastSessionAccess,$MachineTag) = &Download_Generic::get_cookie_session_details($Document_WebUrl,$mech,'JSESSIONID');
					$mech->add_header( 'Accept-Language' => 'en-GB');
					$mech->add_header("Accept" => "text/html, application/xhtml+xml, */*");
					$mech->add_header("Connection" => 'keep-alive');
					$mech->add_header("Host" => "portal.monaghancoco.ie");
					$mech->add_header( 'Cookie' => 'WebLinkSession='.$WebLinkSession.'; lastSessionAccess='.$lastSessionAccess.'; AcceptsCookies=1; MachineTag='.$MachineTag) if($WebLinkSession!~m/^WebLinkSession$/is);
					($D_Code, $Err_Msg, $MD5)=&Download_Generic::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech,$ua);
				}
				else
				{
					($D_Code, $Err_Msg, $MD5)=&Download_Generic::Download($Doc_URL,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech,$ua);
				}
			
				my ($Download_Status)=&Download_Generic::Url_Status($D_Code);
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;					
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my ($Download_Status);
				if($pdf_url!~m/^\s*$/)
				{					
					my ($D_Code, $Err_Msg, $MD5);
				
					if($Council_Code=~m/^2021$/is)
					{
						my $Document_WebUrl="https://portal.monaghancoco.ie/WeblinkPlanningPortal/DocumentService.aspx/PDFTransition";
						my ($WebLinkSession,$lastSessionAccess,$MachineTag) = &Download_Generic::get_cookie_session_details($Document_WebUrl,$mech,'JSESSIONID');
						$mech->add_header( 'Accept-Language' => 'en-GB');
						$mech->add_header("Accept" => "text/html, application/xhtml+xml, */*");
						$mech->add_header("Connection" => 'keep-alive');
						$mech->add_header("Host" => "portal.monaghancoco.ie");
						$mech->add_header( 'Cookie' => 'WebLinkSession='.$WebLinkSession.'; lastSessionAccess='.$lastSessionAccess.'; AcceptsCookies=1; MachineTag='.$MachineTag) if($WebLinkSession!~m/^WebLinkSession$/is);
						($D_Code, $Err_Msg, $MD5)=&Download_Generic::MechDocDownload($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech,$ua);
					}
					else
					{
						($D_Code, $Err_Msg, $MD5)=&Download_Generic::Download($Doc_URL,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup,$mech,$ua);
					}
					($Download_Status)=&Download_Generic::Url_Status($D_Code);				
						
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
					}	
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
					
				}
			}
		}	
		$pdfcount_inc++;
	}

	my($file_comment)=&Download_Generic::Move_Files($One_App_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	if(length($file_comment)!=0)
	{
		$Comments=$Comments.'|'.$file_comment if($file_comment!~m/^\s*$/is);
	}
	DB_Flag:
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		my $No_Of_Doc_Downloaded_TMP = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		$No_Of_Doc_Downloaded=$No_Of_Doc_Downloaded_TMP;
	}
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";
	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;
	
	if($One_App_Name=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}
	}	
}

if($COUNCIL_NAME_TMP ne '')
{
	my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
	$Update_Query.= $Status_Update_Query;
}
$Insert_OneAppLog=~s/\,$//igs;
$Insert_Document=~s/\,$//igs;
$Update_Query=~s/\,$//igs;
$MD5_Insert_Query=~s/\,$//igs;

print "\n$MD5_Insert_Query\n";
print "\n$Insert_Document\n";
print "\n$Insert_OneAppLog\n";
print "\n$Update_Query\n";

open ss,">>$LogDirectory\\Query_Execution_$today_date.txt";
print ss "$Downloaded_date\t$Update_Query\t$Insert_OneAppLog\t$Insert_Document\t$MD5_Insert_Query\n";
close ss;
if($query=~m/Yes/is)
{	
	&Download_Generic::Execute($dbh,$Insert_OneAppLog,$DBtype) if($Insert_OneAppLog!~m/values\s*$/is);	
	&Download_Generic::Execute($dbh,$Insert_Document,$DBtype)  if($Insert_Document!~m/values\s*$/is);	
	&Download_Generic::Execute($dbh,$Update_Query,$DBtype) if($Update_Query!~/^\s*$/is);	
	&Download_Generic::Execute($dbh,$MD5_Insert_Query,$DBtype) if($MD5_Insert_Query!~/values\s*$/is);
}
