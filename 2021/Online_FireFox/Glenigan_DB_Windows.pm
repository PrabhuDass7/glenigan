package Glenigan_DB_Windows;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text");
require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);

###### DB Connection ####
sub DB_Planning()
{	
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}


sub DB_Decision()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}

sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DB_Decision();
	}
}

sub Date_Range()
{
	my $Keyboard_Input = shift;

	my %GCS=('GCS001'=> -0, 'GCS002'=> -7, 'GCS003'=> -14, 'GCS004'=> -21, 'GCS005'=> -28, 'GCS090'=> -90, 'GCS180'=> -180);

	my $Input_date_Range = $GCS{$Keyboard_Input};
	# my $Input_date_Range = $Keyboard_Input;
	my($year,$month,$day) = Add_Delta_Days( Today(), 0 ),"\n";
	# print "Today :$year/$month/$day\n";
	# print "Input_date_Range :$Input_date_Range\n";

	$Input_date_Range=$Input_date_Range;

	my($To_Year,$To_Month,$To_Day) = Add_Delta_Days( $year, $month, $day, $Input_date_Range),"\n";
	# print "Today :$From_Year/$From_Month/$From_Day\n";

	my($From_Year,$From_Month,$From_Day) = Add_Delta_Days($To_Year, $To_Month, $To_Day, -6),"\n";
	if($From_Day!~m/\d\d/is)
	{
		$From_Day=~s/(\d+)/0$1/gsi;
	}
	if($To_Month!~m/\d\d/is)
	{
		$To_Month=~s/(\d+)/0$1/gsi;
	}
	if($From_Month!~m/\d\d/is)
	{
		$From_Month=~s/(\d+)/0$1/gsi;
	}
	if($To_Day!~m/\d\d/is)
	{
		$To_Day=~s/(\d+)/0$1/gsi;
	}
	
	my $From_Date="$From_Day/$From_Month/$From_Year";
	my $To_Date="$To_Day/$To_Month/$To_Year";
	print "From Date :$From_Date\n";
	print "To Date :$To_Date\n";
	# <STDIN>;
	return($From_Date, $To_Date);
}	

sub Test_DB_Planning()
{	
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=172.27.137.184;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}

sub Test_DB_Decision()
{	
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=172.27.137.184;database=GLENIGAN;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}