#-*- coding: utf-8 -*-
import os
import sys
import time
from datetime import datetime, timedelta
import pymssql


## Get current Year ##
currentYear = time.strftime('%Y')

def dbConnection(database):
	connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
	return (connLive)


def deletePDF(upload_no, csv_date):

    (connLive) = dbConnection("SCREENSCRAPPER")
	
    logDirectory='C:/DocClassification/'
    testSharelogDirectory = '//10.101.53.31/commonglenigan/'+str(currentYear)+'/document_upload_test/log/'
    SharelogDirectory = '//10.101.53.31/commonglenigan/'+str(currentYear)+'/document_upload/log/'
	
    todaydate = time.strftime('%Y-%m-%d')
    datetimenow = time.strftime('%Y%m%d%H%M%S')
	
    if os.path.isdir(str(logDirectory)+'logs/'+str(todaydate)) == False:
        os.makedirs(str(logDirectory)+'logs/'+str(todaydate))
    if os.path.isdir(str(SharelogDirectory) + str(todaydate)) == False:
        os.makedirs(str(SharelogDirectory) + str(todaydate))

#    SelectQuery = """select Project_uploaded_date,project_id,doc_name,Document_Path from tbl_document_categorization where delete_flag='Yes' and upload_no = '%s' and csv_created = '%s'""" % (upload_no, csv_date)
    
    SelectQuery = """select DISTINCT a.Project_uploaded_date,a.project_id,a.doc_name,a.Document_Path from tbl_document_categorization as a 
    left join (select project_id,doc_name from tbl_document_categorization where csv_created = '%s' and Delete_flag ='No')
    as b on a.project_id =b.project_id and a.doc_name =b.doc_name 
    where a.delete_flag = 'Yes' and a.upload_no = '%s' and csv_created = '%s'
    and b.doc_name is null""" % (csv_date, upload_no, csv_date)
    
    print(SelectQuery)

    uploadno=''
    cursorLive = connLive.cursor()
    cursorLive.execute(SelectQuery)
    records = cursorLive.fetchall()

    sql_update_query = """Update tbl_document_categorization set delete_status = %s where delete_flag='Yes' and project_id = %s and Document_Path = %s"""
    finalresult=[]
    docsCount=0
    for record in records:
        date = record[0]
        project_id = record[1]
        doc_name = record[2]
        Document_Path = record[3]
        # upload_no = record[4]

        uploadno = upload_no
      
        try:
            # statinfo = os.stat(Document_Path)			
            statinfo = ""
            # print statinfo
            with open(str(logDirectory)+'logs/'+str(todaydate)+'/'+str(uploadno)+'_DeleteList_'+str(datetimenow)+'.log', 'a') as log:
                log.write(str(date)+"\t"+str(project_id)+"\t"+str(Document_Path)+"\t"+str(statinfo)+"\t"+str(datetime.now())+"\n")
            with open(str(SharelogDirectory)+str(todaydate)+'/'+str(uploadno)+'_DeleteList_'+str(datetimenow)+'.log', 'a') as log:
                log.write(str(date)+"\t"+str(project_id)+"\t"+str(Document_Path)+"\t"+str(statinfo)+"\t"+str(datetime.now())+"\n")
            
            # print Document_Path
            if os.path.isfile(Document_Path):
                os.remove(Document_Path)
                records_to_update=''
                if docsCount == 0:
                    records_to_update = ('Yes', project_id, Document_Path)
                    finalresult.append(records_to_update)
                    docsCount += 1
                elif docsCount == 5:
                    records_to_update = ('Yes', project_id, Document_Path)
                    finalresult.append(records_to_update)
                    cursorLive.executemany(sql_update_query, finalresult)
                    connLive.commit()
                    docsCount=0
                    finalresult[:]=[]
                else:
                    records_to_update = ('Yes', project_id, Document_Path)
                    finalresult.append(records_to_update)
                    # print finalresult
                    docsCount += 1
				
        except Exception as error:
            print ("File delete error:",error)
            with open(str(logDirectory)+'logs/'+str(todaydate)+'/'+str(uploadno)+'_DeleteException_'+str(datetimenow)+'.log', 'a') as log:
                log.write("File delete error"+"\t"+str(date)+"\t"+str(project_id)+"\t"+str(Document_Path)+"\t"+str(error)+"\t"+str(datetime.now())+"\n")
            with open(str(SharelogDirectory)+str(todaydate)+'/'+str(uploadno)+'_DeleteException_'+str(datetimenow)+'.log', 'a') as log:
                log.write("File delete error"+"\t"+str(date)+"\t"+str(project_id)+"\t"+str(Document_Path)+"\t"+str(error)+"\t"+str(datetime.now())+"\n")
    if finalresult:
        cursorLive.executemany(sql_update_query, finalresult)
        connLive.commit()
        cursorLive.close()
		
if __name__=="__main__":
    deletePDF(sys.argv[1], sys.argv[2])