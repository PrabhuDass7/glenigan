import pandas as pd
import warnings
import pickle
import scipy as sp
warnings.filterwarnings("ignore")
import gzip, os, re
from math import log
import pymssql
from pandas import DataFrame
import  numpy as np

# Read the Filename
stopwords = open('Models/stopwords_eng_emails.txt', encoding='utf8').read().split()
vec = pickle.load(open('Models/file_vec.pkl', 'rb'))
vect = pickle.load(open('Models/desc_vec.pkl', 'rb'))
lr_clf = pickle.load(open('Models/logistic_model.pkl', 'rb'))
lr_clf1 = pickle.load(open('Models/logistic_model1.pkl', 'rb'))
lr_clf2 = pickle.load(open('Models/logistic_model2.pkl', 'rb'))

# Database Connection
def dbConnection(database):
	connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
	return (connLive)

# Apply Wordninja

__version__ = '0.1.5'
with open('Models/wordninjaa_glenigan.txt', encoding='utf8') as f:
    # with open('wordninja_words_v3.txt') as f:
    # words = f.read().decode().split()
    words = f.read().split()
_wordcost = dict((k, log((i + 1) * log(len(words)))) for i, k in enumerate(words))
_maxword = max(len(x) for x in words)
_SPLIT_RE = re.compile("[^a-zA-Z0-9\.\(\)\,\!\@\#\$\%\^\&\*\?\:\;\"\'\[\]\{\}\+\=\-\_']+")


def split(s):
    """Uses dynamic programming to infer the location of spaces in a string without spaces."""
    l = [_split(x) for x in _SPLIT_RE.split(s)]
    # l = []
    # for x in _SPLIT_RE.split(s):
    # val = _split(x)
    # print(val)
    # l.append(val)
    return [item for sublist in l for item in sublist]

def _split(s):
    # Find the best match for the i first characters, assuming cost has
    # been built for the i-1 first characters.
    # Returns a pair (match_cost, match_length).
    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i - _maxword):i]))
        return min((c + _wordcost.get(s[i - k - 1:i].lower(), 9e999), k + 1) for k, c in candidates)

    # Build the cost array.
    cost = [0]
    for i in range(1, len(s) + 1):
        c, k = best_match(i)
        cost.append(c)

    # Backtrack to recover the minimal-cost string.
    out = []
    i = len(s)
    while i > 0:
        c, k = best_match(i)
        assert c == cost[i]
        # Apostrophe and digit handling (added by Genesys)
        newToken = True
        if not s[i - k:i] == "'":  # ignore a lone apostrophe
            if len(out) > 0:
                # re-attach split 's and split digits
                if out[-1] == "'s" \
                        or (s[i - 1].isdigit() and out[-1][0].isdigit()):  # digit followed by digit
                    out[-1] = s[i - k:i] + out[-1]  # combine current token with previous token
                    newToken = False
        # (End of Genesys addition)

        if newToken:
            out.append(s[i - k:i])

        i -= k
    return reversed(out)

# Added Basic Cleaning Steps

def cleaning(body):

    body = str(body)
    body = body.lower()

    body = re.sub("[_]", " ", body)
    body = re.sub("[^a-z\s\'\:]", " ", body)
    body = re.sub("\s+", " ", body)
    body = re.sub('\s*', '', body)
    body = split(body)
    body = ' '.join(body)
    words = body.split()
    words = [w for w in words if w not in stopwords]
    words = [re.sub(r'(.)\1{3,}', r'\1', w) for w in words]
    words = " ".join(words)
    return (words)

# Customize Cleaning

def Glenigan_Cleaning(df, col_name):
    df[col_name][df[col_name].isna()] = "1"
    list1 = ["cil", "app", "das", "da", "to:", "cc:"]

    description = df[col_name].map(cleaning)

    customized_stopwords = [w for w in list(" ".join(description).split()) if len(w) < 4]
    customized_stopwords = [w for w in customized_stopwords if w not in list1]
    customized_stopwords.append("part")

    def customized_stopwords1(body):
        comment = str(body)
        comment = comment.lower()
        words = comment.split()
        words = [w for w in words if w not in customized_stopwords]
        words = " ".join(words)
        return (words)

    description = description.map(customized_stopwords1)
    return description

def Prediction():

    print("Start InputData Reading")

    # Read input Records from tbl_document_rename table
    try:
        (connLive) = dbConnection("SCREENSCRAPPER")
        cur = connLive.cursor()
        # query = "select top 10  Sno,Doc_Name,Doc_Content,pdf_total_pages,Classifier_Category,rule_Category from tbl_document_rename where (classifier_category = \'\' or classifier_category is null) and CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"
        query = "select top 1 Sno,Doc_Name,Doc_Content,pdf_total_pages,Classifier_Category,rule_Category from tbl_document_rename"
        print(query)
        cur.execute(query)
        field_names = [i[0] for i in cur.description]
        get_data = cur.fetchall()
        cur.close()
        df = DataFrame(get_data)
        df.columns = field_names
    except Exception as e:
        print("Exception while getting records", e)

    print("Completed InputData Reading")

    replace_words=["Error to read PDF","PDF text extract timeout error","Error:PDF Image","Error: Doc body error"]

    data=[]
    for i in df["Doc_Content"]:
        if i in replace_words:
            data.append(np.nan)
        else:
            data.append(i)

    df["Final_Doc_Content"]=data
    print("Start  Data Cleaning")
    try:

        df["cleaned_filename"] = Glenigan_Cleaning(df, "Doc_Name")
        df["cleaned_description"] = Glenigan_Cleaning(df, "Final_Doc_Content")

    except Exception as e:
        print("Exception in Cleaning",e)

    print("Completed  Data Cleaning")

    print("Start  Prediction")

    try:
        y_pred = []
        for i in range(len(df)):
            if (df["cleaned_filename"][i] != '') & (df["cleaned_description"][i] != ''):
                vec_tf_file = vec.transform(pd.Series(df["cleaned_filename"][i]))
                vec_tf_desc = vect.transform(pd.Series(df["cleaned_description"][i]))
                stack = sp.sparse.hstack((vec_tf_file, vec_tf_desc, df[["pdf_total_pages"]][i:i + 1].values),
                                         format='csr')
                y_pred1 = lr_clf.predict(stack)
            elif (df["cleaned_filename"][i] != '') & (df["cleaned_description"][i] == ''):
                vec_tf_file = vec.transform(pd.Series(df["cleaned_filename"][i]))
                stack = sp.sparse.hstack((vec_tf_file, df[["pdf_total_pages"]][i:i + 1].values), format='csr')
                y_pred1 = lr_clf1.predict(stack)
            elif (df["cleaned_filename"][i] == '') & (df["cleaned_description"][i] != ''):
                vec_tf_desc = vect.transform(pd.Series(df["cleaned_description"][i]))
                stack = sp.sparse.hstack((vec_tf_desc, df[["pdf_total_pages"]][i:i + 1].values), format='csr')
                y_pred1 = lr_clf2.predict(stack)
            else:
                y_pred1 = 999
            if y_pred1!=999:
                y_pred1=y_pred1+1
            else:
                y_pred1=999
            y_pred.append(int(y_pred1))
        df["Category_final"] = y_pred
        # df.to_csv("output.csv")

    except Exception as e:
        print('Exception  in Prediction',e)

    print("completed   Prediction")
    
    print(y_pred)

    # print('start Category update')
    # # Read input Records from tbl_document_rename table
    # try:
    #     (connLive) = dbConnection("SCREENSCRAPPER")
    #     cur1 = connLive.cursor()
    #     for index, row in df.iterrows():
    #         sql="update tbl_document_rename set Classifier_Category='"+str(row['Category_final'])+"' where Sno='"+str(row['Sno'])+"'"
    #         print(sql)
    #         cur1.execute(sql)
    #     connLive.commit()
    #     cur1.close()

    # except Exception as e:
    #     print("Exception while getting records", e)
    # print('Complete Category update')

if __name__=="__main__":
    Prediction()


