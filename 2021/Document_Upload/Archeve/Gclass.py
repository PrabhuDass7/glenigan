# -*- coding: utf-8 -*-
# Anaconda Python 2.4

import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import re
import os
import pymssql
import time
from datetime import datetime, timedelta, date
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def tokenize_1(text):
    return text.split()


pipeline = Pipeline([('vec', CountVectorizer(encoding='cp874', tokenizer=tokenize_1, stop_words='english')),
                     ('tfidf', TfidfTransformer()), ('clf', MultinomialNB())])
tuned_parameters = {
    'vec__max_df': (0.5, 0.75),
    'vec__max_features': (15000, 17500),
    'vec__min_df': (20, 30),
    'tfidf__use_idf': (True, False),
    'vec__binary': (True, False),
    'tfidf__norm': ('l1', 'l2'),
    'clf    __alpha': (1, 0.1)
}


def dbConnection(database):
    conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
    connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
    return (conn,connLive)


def updateScript(rule_prediction, description):
    final_decision = ''
    svm = 0
    mnb = 0

    # Check the Description Length  >150
    if len(description) > 150:
        final_decision, svm, mnb = Prediction(rule_prediction, description)
        svm = svm
        mnb = mnb
    # print ("ML Final Output", final_decision)

    else:
        # print "rule_prediction::", type(rule_prediction)
        if rule_prediction == '0':
            final_decision = 999
        else:
            final_decision = rule_prediction

    # print ("Final Output(Length Problem)", final_decision)

    return final_decision, svm, mnb


def SVM_module(X_train, vectorizer_path, Model_path):
    # Load the vectorizer
    transformer = TfidfTransformer()
    loaded_vec = CountVectorizer(decode_error="replace", vocabulary=joblib.load(open(vectorizer_path, "rb")))
    tfidf = transformer.fit_transform(loaded_vec.fit_transform(X_train))

    # Load The SVM module
    clf = joblib.load(Model_path)

    # Predict the Label
    predicted = clf.predict(tfidf)

    predicted1 = predicted[0]
    print "predicted1==>", predicted1

    return predicted1


def Mnb_module(X_train, Model_path):
    clf = joblib.load(Model_path)

    predicted = clf.predict(X_train)

    predicted1 = predicted[0]
    print "predicted1======>", predicted1

    return predicted1


def removeStopwords(example_sent):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(example_sent)

    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    filtered_sentence = []
    outut_string = ''

    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)
            outut_string = outut_string + (str(w)) + ' '
    return outut_string


def Prediction(rule_prediction, description):
    Final_Decision = ''
    prediction_Svm_P = 0
    prediction_Mnb_P = 0

    vectorizer_path = 'Glenigan_Classifier/vectorizer_glenigan_big_5_Svm_class.pkl'
    Model_path_Svm_Precision = 'Glenigan_Classifier/Glenigan_classifier_Svm_precisionv2.pkl'
    Model_path_Mnb_precision = 'Glenigan_Classifier/Glenigan_classifier_Mnbprecisionv2.pkl'

    description = re.sub("\s+", " ", description)
    description = re.sub("[^\w\d\s]", " ", description)

    description = removeStopwords(description)

    description1 = []
    description1.append(description)

    # Svm Model Prediciton

    prediction_Svm_P = SVM_module(description1, vectorizer_path, Model_path_Svm_Precision)
    prediction_Svm_P = prediction_Svm_P + 1

    print prediction_Svm_P

    # Mnb Model Prediciton
    prediction_Mnb_P = Mnb_module(description1, Model_path_Mnb_precision)
    prediction_Mnb_P = prediction_Mnb_P + 1
    print prediction_Mnb_P

    print('Rule', rule_prediction)
    print('SVM', prediction_Svm_P)
    print('MNB', prediction_Mnb_P)

    Description_Length = len(description)
    # print(Description_Length)


    # if rule_prediction is not None or rule_prediction ==  0:

    if rule_prediction is '' or rule_prediction == '0' or rule_prediction == 0:

        # print ("Rule Category output is Empty")

        if prediction_Svm_P == prediction_Mnb_P:
            Final_Decision = prediction_Svm_P
            # print ("The Output is",Final_Decision)

        else:
            Final_Decision = '999'

    else:

        # print ("Rule output is Not Empty")

        if int(rule_prediction) == int(prediction_Svm_P) == int(prediction_Mnb_P):

            Final_Decision = rule_prediction

        elif int(rule_prediction) == int(prediction_Svm_P):

            Final_Decision = prediction_Svm_P

        elif int(rule_prediction) == int(prediction_Mnb_P):

            Final_Decision = prediction_Mnb_P

        # Two model  same rule diff

        elif int(prediction_Svm_P) == int(prediction_Mnb_P):

            Final_Decision = prediction_Svm_P

        # Rule Says 3 check the Two Model prediciton output is Same

        elif int(rule_prediction) == 3 and int(prediction_Svm_P) == int(prediction_Mnb_P):

            Final_Decision = prediction_Svm_P

        else:

            Final_Decision = '999'

    return Final_Decision, prediction_Svm_P, prediction_Mnb_P


# if __name__ == "__main__":
# fDate = sys.argv[1]
# tDate = sys.argv[2]

def db_commit(conn):
    i = 0
    status = ""
    while i < 3:
        try:
            conn.commit()
            status = "Success"
            break
        except:
            time.sleep(5)
        i = i + 1
    return conn, status


def main():
#    conn = dbConnection("SCREENSCRAPPER")
    (conn,connLive) = dbConnection("SCREENSCRAPPER")
#    cursor = conn.cursor()
    cursor = connLive.cursor()

    date = time.strftime('%Y-%m-%d')

    qry_getDescription = "select Sno, rule_Category, Doc_Content from TBL_DOCUMENT_RENAME where Doc_Content <> '' and Classifier_Category is null"
    sql_update_query = """Update TBL_DOCUMENT_RENAME set Classifier_Category = %s, SVM = %s, MNB = %s where Sno = %s"""

    cursor.execute(qry_getDescription)
    fetchResults = cursor.fetchall()

    # timestr = time.strftime("%Y%m%d-%H%M%S")
    # logDirectory = 'C:/Users/mgoas/Desktop/live_test/'
    #
    # if os.path.isdir(str(logDirectory) + 'logs/' + str(date)) == False:
    #     os.makedirs(str(logDirectory) + 'logs/' + str(date))

#    finalresult = []
#    docsCount = 0
    for i in range(len(fetchResults)):
        Sno = str(fetchResults[i][0])
        rule_Category = str(fetchResults[i][1])
        description = str(fetchResults[i][2])
        # print "Sno:",Sno
        # print "rule_Category:",rule_Category
        # print "description:",description
        # print "description Length:",len(description)
        # print(Sno, len(description), rule_Category)
        results = 0
        svm = 0
        mnb = 0

        try:
            Final_output, svm, mnb = updateScript(rule_Category, description)
            print(Final_output, svm, mnb)
            results = Final_output
            svm = svm
            mnb = mnb

            update_query = "update TBL_DOCUMENT_RENAME set Classifier_Category=" + str(results) + ", SVM=" + str(svm) + ", MNB =" + str(mnb) + "  where  Sno=" + str(Sno) + " "

            print(update_query)
            cursor.execute(update_query)
            connLive.commit()

        except Exception as e:
            print("Problem to call UpdateScript function", e, ' ', Sno)

    print "ALL fine ..."



    try:
        # --Rule 1:(correcting miss classified plans)

        qry1 = "update tbl_document_rename set Classifier_Category=4 where rule_category=3 and replace(doc_name,' ','_') like '%statement%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry2 = "update tbl_document_rename set Classifier_Category=4 where rule_category=3 and replace(doc_name,' ','_') like '%assessment%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry3 = "update tbl_document_rename set Classifier_Category=4 where rule_category=3 and replace(doc_name,' ','_') like '%report%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry4 = "update tbl_document_rename set Classifier_Category=4 where rule_category=3 and replace(doc_name,' ','_') like '%summary%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        # --Rule 2



        qry5 = "update tbl_document_rename set Classifier_Category=5 where rule_category!=5 and  rule_Category <>'' and   replace(doc_name,' ','_') like '%comment%'  and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry6 = "update tbl_document_rename set Classifier_Category=5 where rule_category!=5 and  rule_Category <>'' and   replace(doc_name,' ','_') like '%response%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry7 = "update tbl_document_rename set Classifier_Category=5 where rule_category!=5 and  rule_Category <>'' and   replace(doc_name,' ','_') like '%objection%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry8 = "update tbl_document_rename set Classifier_Category=5 where rule_category!=5 and  rule_Category <>'' and   replace(doc_name,' ','_') like '%Mrs_%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry9 = "update tbl_document_rename set Classifier_Category=5 where rule_category!=5 and  rule_Category <>'' and   replace(doc_name,' ','_') like '%Mr_%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry10 = "update tbl_document_rename set Classifier_Category=5 where   rule_Category is null and   replace(doc_name,' ','_') like '%Mrs_%'  and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry11 = "update tbl_document_rename set Classifier_Category=5 where   rule_Category is null and   replace(doc_name,' ','_') like '%Mr_%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry12 = "update tbl_document_rename set Classifier_Category=5 where   rule_Category is null and   replace(doc_name,' ','_') like '%Mr_%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry13 = "update tbl_document_rename set Classifier_Category=5 where Classifier_Category!=5 and Doc_Content like '%comments for planning appli%' and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        # --Rule 3:



        qry14 = "update tbl_document_rename set Classifier_Category=2 where rule_Category=2 and replace(doc_name,' ','_') like '%Design_statement%'  and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry15 = """with t as(
		select * , charindex('design', doc_content) as first_pos, (( charindex('design',	doc_content) - charindex('access', doc_content))) as ddd from tbl_document_rename where classifier_category=2 and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112))
		update t set classifier_category=2 where classifier_category=999 and ddd in (-7,-11) and first_pos>50"""

        qry16 = "update tbl_document_rename set Classifier_Category=2 where (rule_Category!=2 or Classifier_Category=999) and (replace(doc_name,' ','_') like '%D_&_A%' or replace(doc_name,' ','_') like '%D&A%')  and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        # --Rule 4:


        qry17 = "update tbl_document_rename set Classifier_Category=5 where Classifier_Category=999 and MNB=5 and SVM=1 and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry18 = "update tbl_document_rename set Classifier_Category=4 where (Classifier_Category in (999,3) or rule_Category=3) and Doc_Name like '%Construction_management%'  and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"

        qry19 = "update tbl_document_rename set rule_category='3' where rule_category  in ('1','2') and replace(doc_name,' ','_')  like '%Plan%' and (replace(doc_name,' ','_')  like '%location%' or replace(doc_name,' ','_')  like '%site%') and replace(doc_name,' ','_')  not like '%notice%'  and  CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112)"
        qry20 = "update tbl_document_rename set classifier_category=rule_category, pdf_extract_status='' where (classifier_category in ('999') or Classifier_Category is null) and rule_category in (1,2,3,4,5) and  CHARINDEX('.pdf',doc_name,1)>0"
        qry21 = "update tbl_document_rename set Classifier_Category = 999, pdf_extract_status='Unable to read the document' where Doc_Content='' and rule_category not in (1,2,3,4,5) and CHARINDEX('.pdf',doc_name,1)>0"
        cursor.execute(qry1)
        cursor.execute(qry2)
        cursor.execute(qry3)
        cursor.execute(qry4)
        cursor.execute(qry5)
        cursor.execute(qry6)
        cursor.execute(qry7)
        cursor.execute(qry8)
        cursor.execute(qry9)
        cursor.execute(qry10)
        cursor.execute(qry11)
        cursor.execute(qry12)
        cursor.execute(qry13)
        cursor.execute(qry14)
        cursor.execute(qry15)
        cursor.execute(qry16)
        cursor.execute(qry17)
        cursor.execute(qry18)
        cursor.execute(qry19)
        cursor.execute(qry20)
        cursor.execute(qry21)
        connLive.commit()

    except Exception as e:
        print("Multiple update Exeception :", e)
#        with open(str(logDirectory)+'logs/'+str(todaydate)+'/UpdateException.log', 'a') as log:
#			log.write(str(records)+"\t"+str(datetime.now())+"\n")



if __name__ == "__main__":
    main()