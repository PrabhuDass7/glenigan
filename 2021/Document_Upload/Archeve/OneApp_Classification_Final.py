# python 3.7
import pymssql
import sys
import re
import glob
from PyPDF2 import PdfFileReader
import subprocess
import os
import csv
import os.path
from os import walk
import time
from datetime import datetime, timedelta, date
import importlib
import multiprocessing
from multiprocessing.dummy import Pool as ThreadPool

## Get current Year ##
currentYear = time.strftime('%Y')


dataFilePath = 'C:/DocClassification/jar'
extract_text = 'C:/DocClassification/extract_text'
libraryFilePath = 'C:/DocClassification'

def dynamic_import(fileName, module): 
    print("module==>",module)
    return importlib.import_module(fileName,module)
	
 
def dbConnection(database):
    # conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
    connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
    
    return (connLive)

def run(file_name, page):
    command = "java -jar "+dataFilePath+"/pdfbox-app-2.0.16.jar ExtractText -sort {file_name} "+extract_text+"/{page}-extracted.txt -startPage {page} -endPage {page}"
    return subprocess.check_output(command.format(page=page, file_name=file_name), shell=True)


def number_of_pages(file_name):
	print ("No of page PDF read")
	# with open(file_name, "rb") as pdf_file:
		# return PdfFileReader(pdf_file).numPages
	try:
		with open(file_name, "rb") as pdf_file:
			return PdfFileReader(pdf_file).numPages		
	except Exception as e:
		print ("PDF zero kb size or corrupted\n")
		return 0
		
def getPDFContent(pdfName):
    try:
#        pages = number_of_pages(pdfName)
        # print ("try pdf read\n")
        pdfDoc = PdfFileReader(open(pdfName, "rb"), strict=False)
        content = ''
        fileType = ''
        
        textFileName = os.path.basename(pdfName)
        
        fulltextNamewithPath = extract_text+ '/' + textFileName
        
        fulltextNamewithPath = fulltextNamewithPath.replace('.pdf','.txt')
        
        
        for page in range(1, 4):
            run(pdfName, page)
            print('Completed {}'.format(page))
        filenames=[]
        path=extract_text+'/'
        for file in glob.glob(path+"*extracted.txt"):
            print(file)
            filenames.append(file)
            
        with open(fulltextNamewithPath, 'w') as outfile:
            for fname in filenames:
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)
                os.remove(fname)
        file = open(fulltextNamewithPath, 'r') 
        # need to add exception control for file to kill
        content = file.read()

        file.close()
        # input()
        if not content:
            if '/XObject' in pdfDoc.getPage(0)['/Resources']:
                xObject = pdfDoc.getPage(0)['/Resources']['/XObject'].getObject()
                fileType = pdfimagetype(xObject)
        elif re.findall(r'^\s*$', content):
            print("Image Check")
            content = "PDF text extract timeout error"
            if '/XObject' in pdfDoc.getPage(0)['/Resources']:
                xObject = pdfDoc.getPage(0)['/Resources']['/XObject'].getObject()
                fileType = pdfimagetype(xObject)
        elif content is None:
            if '/XObject' in pdfDoc.getPage(0)['/Resources']:
                xObject = pdfDoc.getPage(0)['/Resources']['/XObject'].getObject()
                fileType = pdfimagetype(xObject)
        else:
            fileType = "Readable PDF file"
        
        if fileType is None:
            fileType = "Image type PDF file"
        if fileType == '':
            fileType = "Image type PDF file"
                            
        os.remove(fulltextNamewithPath)
        
        return clean(content), fileType, testclean(content)
    
    except Exception as e:
        print("PDF path read Error: " + str(e))
        textFileName = os.path.basename(pdfName)        
        fulltextNamewithPath = extract_text+ '/' + textFileName        
        os.remove(fulltextNamewithPath.replace('.pdf','.txt'))
        return "Error to read PDF", "Error to read PDF", "Error to read PDF"


def clean(data):
    data = re.sub(r'[^\w\d\s]', ' ', data)
    data = re.sub(r'\n+', ' ', data)
    data = re.sub(r'\s\s+', ' ', data)
    data = re.sub(r'\'', ' ', data)
    data.strip()
    return data

def testclean(data):
    data = re.sub(r'[^\w\d\s\:]', ' ', data)
    data = re.sub(r'\n+', ' ', data)
    data = re.sub(r'\s\s+', ' ', data)
    data = re.sub(r'\'', ' ', data)
    data.strip()
    return data


def getpath(path, pID):
    try:
        documentLocation = ''
        # Get the list of all files in directory tree at given path
        for (dirpath, dirnames, filenames) in os.walk(path):
            print(dirpath)
            print(dirnames)
            print(filenames)
            documentLocation = (dirpath + '/' + filenames).replace("\\", "/")
            print(documentLocation)
            break
        return documentLocation
    except Exception as e:
        print("Unable to read file: " + str(e))
        return ("Error: Unable to read file")


def pdfimagetype(xObject):
    try:
        imageType = ''
        for obj in xObject:
            if xObject[obj]['/Subtype'] == '/Image':
                if '/Filter' in xObject[obj]:
                    if xObject[obj]['/Filter'] == '/FlateDecode':
                        imageType = 'Image file type .png'
                    elif xObject[obj]['/Filter'] == '/DCTDecode':
                        imageType = 'Image file type .png'
                    elif xObject[obj]['/Filter'] == '/JPXDecode':
                        imageType = 'Image file type .jp2'
                    elif xObject[obj]['/Filter'] == '/CCITTFaxDecode':
                        imageType = 'Image file type .tiff'
                else:
                    imageType = 'Image file type .png'
        return imageType
    except Exception as e:
        print("PDF Image Error: " + str(e))        
        return ("Error:PDF Image")


def filenameclean(data):
    try:
        data = re.sub(r'\'', ' ', data)
        data = re.sub(r'\s+', ' ', data)
        data.strip()
        return data
    except Exception as e:
        print("Filename clean Error: " + str(e))
        return ("Error:Filename clean")


def docDescriptionpart(desName):
    try:
        pathname, fname = os.path.split(desName)
        # print "pathname::", pathname
        # print "filename::", fname
        docDescription = ''
        docDesc = fname
        removeddocDesc = re.sub(r'^\d+_', '', str(docDesc))
        removeddocDesc1 = re.sub(r'\_', ' ', str(removeddocDesc))
        removeddocDesc1 = re.sub(r'\'', ' ', str(removeddocDesc1))
        removeddocDesc1 = re.sub(r'\.(pdf|PDF)', '', str(removeddocDesc1))
        docDescription = re.sub(r'\.(txt|jpg|jpeg|png|html|docx|doc|xml|rtf|RTF|db|jp2|tif|tiff|ipg)$', '', str(removeddocDesc1), re.I)
        docDescription = re.sub(r'\s+$', '', str(docDescription))
        # print "After Doc Desc Clean:::", docDescription

        return fname, docDescription
    except Exception as e:
        print("Doc description Error: " + str(e))
        return "Error: Doc description error", "Error: Doc description error"


def db_commit(conn,logDirectory,SharelogDirectory,date,upload_no):
    i = 0
    status = ""
    while i < 3:
        try:
            conn.commit()
            status = "Success"
            break
        except:
            time.sleep(5)
            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + upload_no + ".log", "a") as log:
                log.write("\tException DB Commit\t" + str(datetime.now()) + "\n")
            with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + upload_no + ".log", "a") as log:
                log.write("\tException DB Commit\t" + str(datetime.now()) + "\n")
        i = i + 1
    return conn, status


if __name__ == "__main__":

    insertQuery = 'insert into TBL_DOCUMENT_RENAME (Format_ID, project_id, Doc_Name, Doc_Content, test_doc_content, Doc_Description, pdf_extract_status, dedupe_process_status, Document_Path, updated_date, pdf_total_pages) values '
    (connLive) = dbConnection("SCREENSCRAPPER")
    cursorLive = connLive.cursor()

    timestr = time.strftime("%Y%m%d-%H%M%S")

    date = time.strftime('%Y-%m-%d')
    updated_date = time.strftime('%Y%m%d')

    print("date==>", date)
    print("updated_date==>", updated_date)

    logDirectory = 'C:/DocClassification'
    testSharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload_test/log/'
    SharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload/log/'
    
    
    if os.path.isdir(str(logDirectory) + '/logs/' + str(date)) == False:
        os.makedirs(str(logDirectory) + '/logs/' + str(date))
    if os.path.isdir(str(SharelogDirectory) + str(date)) == False:
        os.makedirs(str(SharelogDirectory) + str(date))

	
    with open(str(logDirectory) + '/logs/' + str(date) + '/' + "time_" + timestr + ".log", "a") as log:
        log.write("Start Time::::\t" + str(datetime.now()) + "\n")
    with open(str(SharelogDirectory) + str(date) + '/' + "time_" + timestr + ".log", "a") as log:
        log.write("Start Time::::\t" + str(datetime.now()) + "\n")
    
    qry_getDescription = "select project_id,format_id,destination_folder_path,dedupe_process_status,upload_no from tbl_document_uploaded where completed=\'\'"
    # qry_getDescription = "select project_id,format_id,destination_folder_path,dedupe_process_status,upload_no from tbl_document_uploaded where completed=\'\' and project_id not in ('MP2010120231419')" 

    print(qry_getDescription)
    

    upload_no = ''
    try:
        cursorLive.execute(qry_getDescription)
        fetchResults = cursorLive.fetchall()

        bulkValuesForQuery = ''
        docsCount = 0
        FormatIDList = []
        for i in range(len(fetchResults)):
            Project_ID = str(fetchResults[i][0])
            Format_ID = str(fetchResults[i][1])
            documentSourcePath = str(fetchResults[i][2])
            deDup_Status = str(fetchResults[i][3])
            upload_no = str(fetchResults[i][4])
                        
            print("Format_ID", Format_ID)
            print("Project_ID", Project_ID)
            print("documentSourcePath", documentSourcePath)

            finalpath = (documentSourcePath).replace("\\", "/")
            finalpath = (documentSourcePath).replace("\/", "/")
            finalpath = re.sub(r'\'', '\'\'', finalpath)
            finalpath = re.sub(r'10.101.53.31', 'ch1031sf02', finalpath)

            print("finalpath", finalpath)

            if not os.path.exists(finalpath):
                print("Folder not Exist - Folder Missing")
                with open(str(logDirectory) + '/logs/' + str(date) + '/' + "foldermissedindir_" + timestr + "_" + upload_no + ".log", "a") as log:
                    log.write(str(finalpath) + "\t" + str(Project_ID) + "\tMissing Folder in dir\t" + str(datetime.now()) + "\n")
                with open(str(SharelogDirectory) + str(date) + '/' + "foldermissedindir_" + timestr + "_" + upload_no + ".log", "a") as log:
                    log.write(str(finalpath) + "\t" + str(Project_ID) + "\tMissing Folder in dir\t" + str(datetime.now()) + "\n")
                    
                    

                joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','','','','','Project Folder Missing in dir','" + str(deDup_Status) + "','" + str(finalpath) + "','" + str(updated_date) + "','')"
                bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                if re.search('^\,\(', str(bulkValuesForQuery)):
                    bulkValuesForQuery = insertQuery + joinValues

                cursorLive.execute(bulkValuesForQuery)
                connLive, status2 = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
                if status2 == "":
                    print("Exception DB Commit")
                    with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                        log.write(str(finalpath) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                    with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                        log.write(str(finalpath) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                docsCount = 0
                bulkValuesForQuery = ''

            else:
                print("Folder Exist in Dir -No Issue")

                files_path = [os.path.join(finalpath, x) for x in os.listdir(finalpath)]
                # print(files_path)
                # raw_input()

                for pathandfile in files_path:
                    if pathandfile.endswith(('.pdf', '.PDF')):
                        pathandfile = pathandfile.replace("\\", "/")
                        pathandfile = pathandfile.replace("\/", "/")
                        pathandfile = pathandfile.replace(r'\'', '')
                        print("FileFullPath::", pathandfile)
                        # raw_input()
                        
                        try:
                            filename, docDescription = docDescriptionpart(pathandfile)
                            filename = re.sub(r'\'', '\'\'', filename)
                            pathandfile = re.sub(r'\'', '\'\'', pathandfile)
#                            filename = filenameclean(filename)
                            print("filename:::", filename)
                            print("docDescription:::", docDescription)
                            # print("pathandfile:::", pathandfile)
                            # raw_input()
                           
                            if os.path.isfile(pathandfile):
                                # print("if os is file==>", pathandfile)
                                # no_of_pages = number_of_pages(pathandfile)
                                no_of_pages=''
                                docFirstPageContent, pdffileType, testDocFirstPageContent='','',''
                                timeout = 5
                                p = ThreadPool(1)
                                res = p.apply_async(number_of_pages, args=(pathandfile,))
                                try:
                                    no_of_pages = res.get(timeout)
                                    # print(pathandfile,"::",no_of_pages)
                                except multiprocessing.TimeoutError:
                                    print(pathandfile,": corrupted pdf Aborting due to timeout")
                                    p.terminate()
                                print("no_of_pages==>", no_of_pages)
                                if no_of_pages=='':
                                    docFirstPageContent, pdffileType, testDocFirstPageContent = "Error","PDF","Error_content"
                                else:
                                    docFirstPageContent, pdffileType, testDocFirstPageContent = getPDFContent(pathandfile)
                                # docFirstPageContent, pdffileType, testDocFirstPageContent = getPDFContent(pathandfile)
                                print("pdffileType==>", pdffileType)
                                print("no_of_pages==>", no_of_pages)
                                # raw_input()

                                FormatIDList.append(Format_ID)

                                if re.search('^Error', str(docFirstPageContent)) is None:
                                    joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(
                                        filename) + "','" + str(docFirstPageContent) + "','" + str(testDocFirstPageContent) + "','" + str(
                                        docDescription) + "','" + str(pdffileType) + "','" + str(
                                        deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','" + str(no_of_pages) + "')"
                                    # print ("docsCount",docsCount)
                                    # raw_input()
                                    if docsCount == 0:
                                        bulkValuesForQuery = insertQuery + joinValues
                                        # print (bulkValuesForQuery)
                                        # raw_input()
                                        docsCount += 1
                                    elif docsCount == 10:
                                        print("Insert", docsCount)
                                        bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                                        cursorLive.execute(bulkValuesForQuery)
                                        connLive, status = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
                                        if status == "":
                                            print("Exception DB Commit")
                                            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                                log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                            with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                                log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                        # conn.commit()
                                        docsCount = 0
                                        bulkValuesForQuery = ''
                                        FormatIDList1 = set(FormatIDList)
                                        FormatIDList2 = ",".join(FormatIDList1)
                                        print ("FormatIDList:", FormatIDList2)
                                        ruleSP = "exec proc_get_document_category '','" + FormatIDList2 + "'"
                                        cursorLive.execute(ruleSP)
                                        connLive, status1 = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
                                        if status1 == "":
                                            print("Exception DB Commit")
                                            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "pro_exception_" + timestr + "_" + upload_no +".log", "a") as log:
                                                log.write(FormatIDList2 + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                            with open(str(SharelogDirectory) + str(date) + '/' + "pro_exception_" + timestr + "_" + upload_no + ".log", "a") as log:
                                                log.write(FormatIDList2 + "\tException DB Commit\t" + str(datetime.now()) + "\n")

                                        FormatIDList = []
                                    else:
                                        bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                                        docsCount += 1
                                else:
#                                    print ("docFirstPageContent==>", str(docFirstPageContent))
                                    print ("PDF Extraction Error")
                                    pathandfile = pathandfile.replace("\'", "")
                                    joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(filename) + "','','','" + str(docDescription) + "','PDF Extraction Error','" + str(deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','" + str(no_of_pages) + "')"
                                    bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                                    if re.search('^\,\(', str(bulkValuesForQuery)):
                                        bulkValuesForQuery = insertQuery + joinValues

                                    cursorLive.execute(bulkValuesForQuery)
                                    connLive, status1 = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
                                    if status1 == "":
                                        print("Exception DB Commit")
                                        with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                            log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                        with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                            log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                    docsCount = 0
                                    bulkValuesForQuery = ''
                            else:
                                print ("File Missing in Folder")
                                pathandfile = pathandfile.replace("\'", "")
                                joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(filename) + "','','','" + str(docDescription) + "','File Missing in project folder','" + str(deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','')"
                                bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                                if re.search('^\,\(', str(bulkValuesForQuery)):
                                    bulkValuesForQuery = insertQuery + joinValues
                                cursorLive.execute(bulkValuesForQuery)
                                connLive, status1 = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
                                if status1 == "":
                                    print("Exception DB Commit")
                                    with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                        log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                    with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                        log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                docsCount = 0
                                bulkValuesForQuery = ''
                        except Exception as error:
                            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "exception_" + timestr + "_" + upload_no + ".log", "a") as log:
                                log.write(str(pathandfile) + "\t" + str(error) + "\t" + str(sys.exc_info()[2].tb_lineno) + "\t" + str(datetime.now()) + "\n")
                            with open(str(SharelogDirectory) + str(date) + '/' + "exception_" + timestr + "_" + upload_no + ".log", "a") as log:
                                log.write(str(pathandfile) + "\t" + str(error) + "\t" + str(sys.exc_info()[2].tb_lineno) + "\t" + str(datetime.now()) + "\n")
                            
                            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "exception_query_" + timestr + "_" + upload_no + ".log", "a") as log:
                                log.write(str(bulkValuesForQuery) + "\n\n")
                            with open(str(SharelogDirectory) + str(date) + '/' + "exception_query_" + timestr + "_" + upload_no + ".log", "a") as log:
                                log.write(str(bulkValuesForQuery) + "\n\n")
                    else:
                        print ("fullfilepath::", pathandfile)
                        # raw_input()

                        filename, docDescription = docDescriptionpart(pathandfile)
                        filename = re.sub(r'\'', '\'\'', filename)
#                        filename = filenameclean(filename)
                        pathandfile = pathandfile.replace('\'', '')
                        pathandfile = pathandfile.replace("\\", "/")
                        pathandfile = pathandfile.replace("\/", "/")
                        pathandfile = re.sub(r'\'', '\'\'', pathandfile)
                        joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(filename) + "','','','','Not a PDF file type','" + str(deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','')"
                        print ("docsCount", docsCount)
                        # print (bulkValuesForQuery)
                        # raw_input()
                        if docsCount == 0:
                            bulkValuesForQuery = insertQuery + joinValues
                            docsCount += 1
                        elif docsCount == 10:
                            print("Insert", docsCount)
                            bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                            # with open(str(logDirectory) + '/logs/' + str(date) + '/' + "sqlquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                # log.write(str(bulkValuesForQuery) + "\n")
                            # print (bulkValuesForQuery)
                            # raw_input()
                            cursorLive.execute(bulkValuesForQuery)
                            connLive, status = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
                            if status == "":
                                print("Exception DB Commit")
                                with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                    log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                                with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                                    log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                            # conn.commit()
                            docsCount = 0
                            bulkValuesForQuery = ''

                        else:
                            bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
                            docsCount += 1
                        # raw_input()

        if (bulkValuesForQuery != ''):
            cursorLive.execute(bulkValuesForQuery)
            connLive, status = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
            if status == "":
                print("Exception DB Commit")
                with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                    log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + upload_no + ".log", "a") as log:
                    log.write(str(pathandfile) + "\t" + str(bulkValuesForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")

        FormatIDList1 = set(FormatIDList)
        FormatIDList2 = ",".join(FormatIDList1)
        print ("FormatIDList2::", FormatIDList2)
        ruleSP = "exec proc_get_document_category '','" + FormatIDList2 + "'"
        cursorLive.execute(ruleSP)
        connLive, status = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
        if status == "":
            print("Exception DB Commit")
            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "pro_exception_" + timestr + "_" + upload_no + ".log", "a") as log:
                log.write(FormatIDList2 + "\tException DB Commit\t" + str(datetime.now()) + "\n")
            with open(str(SharelogDirectory) + str(date) + '/' + "pro_exception_" + timestr + "_" + upload_no + ".log", "a") as log:
                log.write(FormatIDList2 + "\tException DB Commit\t" + str(datetime.now()) + "\n")

        # conn.close
        connLive.commit()
    # connLive.close

    except pymssql.Error as e:
        print ("SQL Error: " + str(e))
    
    
    gClassification = dynamic_import('Document_Classification_Upload', libraryFilePath+'/Document_Classification_Upload.py')
    gClassification.Prediction()
	
    # try:
        # os.system('C:\Anaconda2\python.exe ' + logDirectory + '/Gclass.py')
        # print "Triggered Succesfully - Pass"
    # except Exception as error:
        # print ("error", error)
        # with open(str(logDirectory) + 'logs/' + str(date) + '/TriggerException_' + upload_no +'.log', 'a') as log:
            # log.write("Gclass trigger issue" + "\t" + str(error) + "\t" + str(datetime.now()) + "\n")
        # with open(str(SharelogDirectory) + str(date) + '/TriggerException_' + upload_no +'.log', 'a') as log:
            # log.write("Gclass trigger issue" + "\t" + str(error) + "\t" + str(datetime.now()) + "\n")
	
	
    
    checkQuery = ''
    if int(upload_no) == int(1):
        checkQuery = "select Classifier_category from TBL_DOCUMENT_RENAME where CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112) and Document_Path like \'//ch1031sf02/commonglenigan/"+str(currentYear)+"/document_upload/first_upload%\' and (Classifier_category is NOT NULL or Classifier_category <> \'\')"
    elif int(upload_no) == int(2):
        checkQuery = "select Classifier_category from TBL_DOCUMENT_RENAME where CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112) and Document_Path like \'//ch1031sf02/commonglenigan/"+str(currentYear)+"/document_upload/second_upload%\' and (Classifier_category is NOT NULL or Classifier_category <> \'\')"
    print (checkQuery)
   
    cursorLive.execute(checkQuery)
    row = cursorLive.fetchone()
    if row == None:
        print("There are no results for this query")
        with open(str(logDirectory) + '/logs/' + str(date) + '/ClassCheck_' + str(upload_no) +'.log', 'a') as log:
            log.write("Problem found in Classification Process" + "\t" + str(datetime.now()) + "\n")
        with open(str(SharelogDirectory) + str(date) + '/ClassCheck_' + str(upload_no) +'.log', 'a') as log:
            log.write("Problem found in Classification Process" + "\t" + str(datetime.now()) + "\n")
        update_commonquery = "update tbl_common set document_upload_started=\'\'"
        cursorLive.execute(update_commonquery)
        sys.exit()   
 
    categorization_qry = """select  a.project_id,b.Doc_Name ,a.format_id,
							c.APPLICATION_NO,ltrim(c.council_cd) as council_code,c.APPLICATION_URL,b.pdf_extract_status,a.project_uploaded_date,
							a.upload_no,b.rule_Category,b.rule_Comment,b.Classifier_Category,b.Document_Path
							from  tbl_document_uploaded as a,tbl_document_rename as b,
							DR_APPLICATION as c
							where a.project_id =b.project_id
							and a.project_id =c.PROJECT_ID
							and a.completed =\'\'"""

    cursorLive.execute(categorization_qry)
    categ_results = cursorLive.fetchall()

    CateinsertQuery = 'insert into tbl_document_categorization ( project_id,doc_name,format_id,application_no,council_code,url,pdf_extract_status,Project_uploaded_date,upload_no,rule_Category,rule_Comment,delete_flag,Meta_level1,Document_Path,csv_created) values'

    bulkInsertForQuery = ''
    CateCount = 0
    for i in range(len(categ_results)):
        cateProject_ID = str(categ_results[i][0])
        cateDoc_Name = str(categ_results[i][1])
        cateFormat_ID = str(categ_results[i][2])
        cateApp_no = str(categ_results[i][3])
        cateCouncil_CD = str(categ_results[i][4])
        cateApp_URL = str(categ_results[i][5])
        catePDF_EStatus = str(categ_results[i][6])
        cateProject_UDate = str(categ_results[i][7])
        cateUpload_no = str(categ_results[i][8])
        rule_Category = str(categ_results[i][9])
        rule_Comment = str(categ_results[i][10])
        Classifier_Category = str(categ_results[i][11])
        Document_Path = str(categ_results[i][12])

        cateDoc_Name = re.sub(r'\'', "\'\'", str(cateDoc_Name))
        cateApp_URL = re.sub(r'\'', "\'\'", str(cateApp_URL))
        Document_Path = re.sub(r'\'', "\'\'", str(Document_Path))

#        if cateProject_UDate == 'None':
#            cateProject_UDate = updated_date

        meta_level1 = ''
        delete_flag = ''
        if Classifier_Category == "999" or Classifier_Category == '' or Classifier_Category == 'None':
            delete_flag = 'Yes'
        elif Classifier_Category == "0" or Classifier_Category == "5":
            delete_flag = 'Yes'
        elif (catePDF_EStatus == 'Project Folder Missing in dir' or catePDF_EStatus == 'Not a PDF file type' or catePDF_EStatus == 'None' or catePDF_EStatus == '') and (Classifier_Category == "0" or Classifier_Category == "5" or Classifier_Category == "999" or Classifier_Category == '' or Classifier_Category == 'None'):
            delete_flag = 'Yes'
        elif re.findall(r'\.(txt|jpg|jpeg|png|html|docx|doc|xml|rtf|RTF|db|tif|tiff|jp2|ipg)$', str(cateDoc_Name), re.I):
            delete_flag = 'Yes'
        else:
            delete_flag = 'No'

        if (Classifier_Category == "999" or Classifier_Category == '' or Classifier_Category == 'None' or Classifier_Category == "0") and catePDF_EStatus == 'Readable PDF file':
            catePDF_EStatus = 'Unable to classify - content not sufficient'
        elif type(Classifier_Category).__name__ == 'int':            
            if (1 <= int(Classifier_Category) <= 5) and catePDF_EStatus == 'Readable PDF file':
                catePDF_EStatus = ''
        
        print ("###############")
        print ("Classifier_Category==>", Classifier_Category)
        # print "catePDF_EStatus==>",catePDF_EStatus
        print ("rule_Category==>", rule_Category)
        print ("rule_Comment==>", rule_Comment)
        print ("delete_flag==>", delete_flag)
        print ("cateProject_ID==>", cateProject_ID)
        print ("cateDoc_Name==>", cateDoc_Name)
        # print ("cateFormat_ID==>",cateFormat_ID)
        print ("cateApp_no==>", cateApp_no)
        print ("cateCouncil_CD==>", cateCouncil_CD)
        # print ("cateApp_URL==>",cateApp_URL)
        # print ("cateProject_UDate==>",cateProject_UDate)
        # print ("cateUpload_no==>",cateUpload_no)
        # print ("Document_Path==>",Document_Path)
        # raw_input()

        ruleFormation = {
            "1": "Application Form",
            "2": "Design & Access Statement",
            "3": ["Plans - Existing", "Plans - Proposed"],
            "4": "Statements",
            "5": "Letters & Emails"
        }

        if rule_Comment == 'Existing' and Classifier_Category == '3':
            # print (ruleFormation["3"][0])
            meta_level1 = ruleFormation["3"][0]
        elif Classifier_Category == '3':
            meta_level1 = ruleFormation["3"][1]
        elif Classifier_Category != 'None' and Classifier_Category != '0' and Classifier_Category != '3' and Classifier_Category != '999':
            meta_level1 = ruleFormation[Classifier_Category]
        # print ("meta_level1:::",meta_level1)

        print ("meta_level1==>", meta_level1)
        # raw_input()

        CatejoinValues = "(" + "'" + str(cateProject_ID) + "','" + str(cateDoc_Name) + "','" + str(cateFormat_ID) + "','" + str(cateApp_no) + "','" + str(cateCouncil_CD) + "','" + str(cateApp_URL) + "','" + str(catePDF_EStatus) + "','" + str(cateProject_UDate) + "','" + str(cateUpload_no) + "','" + str(rule_Category) + "','" + str(rule_Comment) + "','" + str(delete_flag) + "','" + str(meta_level1) + "','" + str(Document_Path) + "','" + str(updated_date) + "')"
        # print (CatejoinValues)

        if CateCount == 0:
            bulkInsertForQuery = CateinsertQuery + CatejoinValues
            CateCount += 1
        elif CateCount == 10:
            print("Insert", CateCount)
            bulkInsertForQuery = bulkInsertForQuery + "," + CatejoinValues
            print (bulkInsertForQuery)
            cursorLive.execute(bulkInsertForQuery)
            connLive, status = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
            if status == "":
                print("Exception DB Commit")
                with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + str(upload_no) + ".log", "a") as log:
                    log.write(str(bulkInsertForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
                with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + str(upload_no) + ".log", "a") as log:
                    log.write(str(bulkInsertForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
            # conn.commit()
            CateCount = 0
            bulkInsertForQuery = ''
        else:
            bulkInsertForQuery = bulkInsertForQuery + "," + CatejoinValues
            CateCount += 1
            
        completedFinalUpdate1 = "update tbl_document_uploaded set completed=\'y\' where completed=\'\' and project_id = \'"+str(cateProject_ID)+"\' and format_id = \'"+str(cateFormat_ID)+"\'"
        print (completedFinalUpdate1)
        cursorLive.execute(completedFinalUpdate1)
        
    if (bulkInsertForQuery != ''):
        # print (bulkInsertForQuery)
        cursorLive.execute(bulkInsertForQuery)
        connLive, status = db_commit(connLive,logDirectory,SharelogDirectory,date,upload_no)
        if status == "":
            print("Exception DB Commit")
            with open(str(logDirectory) + '/logs/' + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + str(upload_no) + ".log", "a") as log:
                log.write(str(bulkInsertForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")
            with open(str(SharelogDirectory) + str(date) + '/' + "dbexceptionquery_" + timestr + "_" + str(upload_no) + ".log", "a") as log:
                log.write(str(bulkInsertForQuery) + "\tException DB Commit\t" + str(datetime.now()) + "\n")

	# try:
        # os.system('C:\Anaconda2\python.exe ' + logDirectory + '/Delete_PDF_Files_For_Others.py ' + str(upload_no) + " " +str(updated_date))
        # print ("Delete Script Triggered Succesfully - Pass")
    # except Exception as error:
        # print ("error", error)
        # with open(str(logDirectory) + 'logs/' + str(date) + '/TriggerException_' + upload_no +'.log', 'a') as log:
            # log.write("Delete_PDF_File Script trigger issue" + "\t" + str(error) + "\t" + str(datetime.now()) + "\n")
        # with open(str(SharelogDirectory) + str(date) + '/TriggerException_' + upload_no +'.log', 'a') as log:
            # log.write("Delete_PDF_File Script trigger issue" + "\t" + str(error) + "\t" + str(datetime.now()) + "\n")
    	
    deletePDFs = dynamic_import('Delete_PDF_Files_For_Others',libraryFilePath+'/Delete_PDF_Files_For_Others.py')
    deletePDFs.deletePDF(str(upload_no), str(updated_date))	
	
    datetimenow = time.strftime('%Y%m%d%H%M%S')
    year = datetime.strptime(date, "%Y-%m-%d").strftime("%Y")

    
    print ("upload_no==>", upload_no)

    uploadDirectory = ''
    if upload_no == '1' or upload_no == 1:
        uploadDirectory = '//ch1031sf02/commonglenigan/' + year + '/document_upload/first_upload'
    elif upload_no == '2' or upload_no == 2:
        uploadDirectory = '//ch1031sf02/commonglenigan/' + year + '/document_upload/second_upload'

    filename = uploadDirectory + "/Import" + datetimenow + ".csv";

    print (filename)

    finalcsvfileQuery = """select DISTINCT Project_uploaded_date,Project_ID,Format_ID,Application_No,Council_Code,doc_name,Meta_level1,Meta_level2,pdf_extract_status from tbl_document_categorization where delete_flag = 'No' and (delete_status = '' or delete_status is null ) and upload_no = '%s' and csv_created = '%s'""" % (
    upload_no, updated_date)
    # print finalcsvfileQuery
    cursorLive.execute(finalcsvfileQuery)
    fetchFinalResults = cursorLive.fetchall()

    try:
        with open(filename, 'w', newline='') as csvfile:
            fieldnames = ["Sno", "Project uploaded date", "Project_ID", "Format_ID", "Application_No", "Council_Code",
                          "File_Name", "Meta_level1", "Meta_level2", "Comments"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            sno = 0
            for i in range(len(fetchFinalResults)):
                Project_uploaded_date = str(fetchFinalResults[i][0])
                final_Project_ID = str(fetchFinalResults[i][1])
                final_Format_ID = str(fetchFinalResults[i][2])
                final_Application_No = str(fetchFinalResults[i][3])
                final_Council_Code = str(fetchFinalResults[i][4])
                final_File_Name = str(fetchFinalResults[i][5])
                final_Meta_level1 = str(fetchFinalResults[i][6])
                final_Meta_level2 = str(fetchFinalResults[i][7])
                final_Comments = str(fetchFinalResults[i][8])

                final_Comment = ''
                final_Meta_level2 = ''
                # print (Project_uploaded_date)
                # print (final_Project_ID)
                # print (final_Format_ID)
                # print (final_Application_No)
                # print (final_Council_Code)
                # print (final_File_Name)
                # print (final_Meta_level1)
                # print (final_Meta_level2)

                sno = sno + 1
                if re.findall(r'Image', str(final_Comments)):
                    final_Comments = 'Image File Type'
                # print (final_Comments)

                Project_uploaded_date = datetime.strptime(Project_uploaded_date, '%Y-%m-%d').strftime('%d/%m/%Y')

                writer.writerow({'Sno': sno, 'Project uploaded date': str(Project_uploaded_date),
                                 'Project_ID': str(final_Project_ID), 'Format_ID': str(final_Format_ID),
                                 'Application_No': str(final_Application_No), 'Council_Code': str(final_Council_Code),
                                 'File_Name': str(final_File_Name), 'Meta_level1': str(final_Meta_level1),
                                 'Meta_level2': str(final_Meta_level2), 'Comments': str(final_Comment)})
            # writer.writerow({'Project uploaded date': str(Project_uploaded_date),'Project_ID': str(final_Project_ID), 'Format_ID': str(final_Format_ID),'Application_No': str(final_Application_No), 'Council_Code': str(final_Council_Code),'doc_name': str(final_File_Name), 'Meta_level1': str(final_Meta_level1),'Meta_level2': str(final_Meta_level2), 'pdf_extract_status': str(final_Comment)})

    except Exception as e:
        print (e, sys.exc_traceback.tb_lineno)

    update_statusquery = "update tbl_common set document_upload_started=\'\'"
    cursorLive.execute(update_statusquery)
    completedFinalUpdate = "update tbl_document_uploaded set completed='y' where completed=\'\'"
    cursorLive.execute(completedFinalUpdate)
    connLive.commit()

    deletedQuery = """select DISTINCT Project_uploaded_date,Project_ID,Format_ID,Application_No,Council_Code,doc_name,Meta_level1,Meta_level2,pdf_extract_status,rule_Category from tbl_document_categorization where delete_flag = 'Yes' and upload_no = '%s' and csv_created = '%s'""" % (
    upload_no, updated_date)
    # print (deletedQuery)
    cursorLive.execute(deletedQuery)
    fetchdeletedResults = cursorLive.fetchall()

    deletedfilename = uploadDirectory + "/Import_deleted" + datetimenow + ".csv";

    try:
        with open(deletedfilename, 'w', newline='') as csvfile:
            fieldnames = ["Sno", "Project uploaded date", "Project_ID", "Format_ID", "Application_No", "Council_Code",
                          "File_Name", "Meta_level1", "Meta_level2", "Comments"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            sno = 0
            for i in range(len(fetchdeletedResults)):
                Project_uploaded_date = str(fetchdeletedResults[i][0])
                final_Project_ID = str(fetchdeletedResults[i][1])
                final_Format_ID = str(fetchdeletedResults[i][2])
                final_Application_No = str(fetchdeletedResults[i][3])
                final_Council_Code = str(fetchdeletedResults[i][4])
                final_File_Name = str(fetchdeletedResults[i][5])
                final_Meta_level1 = str(fetchdeletedResults[i][6])
                final_Meta_level2 = str(fetchdeletedResults[i][7])
                final_Comments = str(fetchdeletedResults[i][8])
                rule = str(fetchdeletedResults[i][9])
                final_Meta_level2 = ''

                # print (Project_uploaded_date)
                # print (final_Project_ID)
                # print (final_Format_ID)
                # print (final_Application_No)
                # print (final_Council_Code)
                # print (final_File_Name)
                # print (final_Meta_level1)
                # print (final_Meta_level2)

                sno = sno + 1
                if re.findall(r'Image', str(final_Comments)):
                    final_Comments = 'Image File Type'
                # print (final_Comments)

                Project_uploaded_date = datetime.strptime(Project_uploaded_date, '%Y-%m-%d').strftime('%d/%m/%Y')

                if rule == '1' and re.findall(r'\.(txt|db)', str(final_File_Name)):
                    print ('No need to entry in delete log')
                else:
                    writer.writerow({'Sno': sno, 'Project uploaded date': str(Project_uploaded_date),
                                     'Project_ID': str(final_Project_ID), 'Format_ID': str(final_Format_ID),
                                     'Application_No': str(final_Application_No),
                                     'Council_Code': str(final_Council_Code), 'File_Name': str(final_File_Name),
                                     'Meta_level1': str(final_Meta_level1), 'Meta_level2': str(final_Meta_level2),
                                     'Comments': str(final_Comments)})

    except Exception as e:
        print (e, sys.exc_traceback.tb_lineno)

    cursorLive.close

    with open(str(logDirectory) + '/logs/' + str(date) + '/' + "time_" + timestr + ".log", "a") as log:
        log.write("End Time::::\t" + str(datetime.now()) + "\n")
    with open(str(SharelogDirectory) + str(date) + '/' + "time_" + timestr + ".log", "a") as log:
        log.write("End Time::::\t" + str(datetime.now()) + "\n")