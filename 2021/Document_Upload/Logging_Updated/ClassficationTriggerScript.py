import os,sys 
import pymssql
import time
from datetime import datetime, timedelta
import logging

## Get current Year ##
currentYear = time.strftime('%Y')

#------------ Connect DataBase ------------------------
def dbConnection(database):
	# connLive = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
	return (connLive)
	
if __name__ == "__main__":
	todaydate = time.strftime('%Y-%m-%d')
	
	#---------- Create Script Location directory directory ---------------
	
	Script_directory='C:/DocClassification/'
	logDirectory=Script_directory+"logs/"+str(todaydate)
	testSharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload_test/log/'+str(todaydate)
	SharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload/log/'+str(todaydate)

	#---------- Folder or directory exists check------------------- 
	try:
		if os.path.isdir(str(logDirectory)) == False:
			os.makedirs(str(logDirectory))
		if os.path.isdir(str(SharelogDirectory)) == False:
			os.makedirs(str(SharelogDirectory))
		
		logFilename = "Trigger_Log_{}.txt".format(str(todaydate))
		Log_File_Path = logDirectory+"/"+logFilename
		logging.basicConfig(
			filename = Log_File_Path,
			format = "%(asctime)s\t%(levelname)s\t%(funcName)s\t%(lineno)s\t%(message)s",
			level = logging.DEBUG)	
		logging.info('Trigger script Initiated : '+"\t"+str(datetime.now())
		logging.info('logDirectory created : '+"\t"+str(datetime.now())
		
        try:
			(connLive) = dbConnection("SCREENSCRAPPER")	  
			cursorLive = connLive.cursor()
			cursorLive.execute("select document_upload_started from tbl_common ")
			records = cursorLive.fetchone()[0]
			print(records)
			logging.info('Record Status : '+"\t"+str(records)+"\t"+str(datetime.now())
			
			#-------------- Checking Record "YES" status for Script Initation -------------
			
			if records == 'yes':
				try:
					os.system('c:\Anaconda3\python.exe '+Script_directory+'/OneApp_Classification_Final.py')
					logging.info('OneApp_Classification_Final script triggered successfully : '+"\t"+str(datetime.now())
					print("OneApp_Classification_Final Triggered Succesfully - Pass")
					try:
						update_statusquery = "update tbl_common set document_upload_started='Started'"
						cursorLive.execute(update_statusquery)
					except Exception as er:
						logging.error('Update query execution failed '+"\t"+str(er)+"\t"+str(datetime.now()))
				except Exception as er:
					logging.error('OneApp_Classification_Final script trigger failed '+"\t"+str(er)+"\t"+str(datetime.now()))
					print ("Error",er)
				logging.info('Trigger script executed successfully : '+"\t"+str(datetime.now())
			elif records == 'Started':
				logging.info('Script initiated already : '+"\t"+str(datetime.now())
			else:
				logging.info('Input not found : '+"\t"+str(datetime.now())
		except Exception as er:
			logging.error('Trigger script failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
			print ("Error",er)
	except Exception as er:
		logging.error('Trigger script failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
		print ("Error",er)