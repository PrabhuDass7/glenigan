# python 3.7
import pymssql
import sys
import re
import glob
from PyPDF2 import PdfFileReader
import subprocess
import os
import csv
import os.path
from os import walk
import time
from datetime import datetime, timedelta, date
import importlib
import multiprocessing
from multiprocessing.dummy import Pool as ThreadPool
import logging

## Get current Year ##
currentYear = time.strftime('%Y')

Data_File_directory = 'C:/DocClassification/jar'
Models_File_directory = 'C:/DocClassification/Models'
extract_text_directory = 'C:/DocClassification/extract_text'
Script_directory = 'C:/DocClassification'

def dynamic_import(file_name, module): 
	print("module==>",module)
	return importlib.import_module(file_name,module)
	
def db_connection(database):
	conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	# connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
	return conn

def run(file_name, page):
	try:
		command = "java -jar "+Data_File_directory+"/pdfbox-app-2.0.16.jar ExtractText -sort {file_name} "+extract_text_directory+"/{page}-extracted.txt -startPage {page} -endPage {page}"
	except Exception as e:
		print (e)
	return subprocess.check_output(command.format(page=page, file_name=file_name), shell=True)

def pdf_validation(file_name):
	print ("PDF validation and read No.of.page")
	try:
		with open(file_name, "rb") as pdf_file:
			return PdfFileReader(pdf_file).numPages		
	except Exception as e:
		print ("PDF zero kb size or corrupted\n")
		return 0
		
def clean(data):
	data = re.sub(r'[^\w\d\s]', ' ', data)
	data = re.sub(r'\n+', ' ', data)
	data = re.sub(r'\s\s+', ' ', data)
	data = re.sub(r'\'', ' ', data)
	data.strip()
	return data

def testclean(data):
	data = re.sub(r'[^\w\d\s\:]', ' ', data)
	data = re.sub(r'\n+', ' ', data)
	data = re.sub(r'\s\s+', ' ', data)
	data = re.sub(r'\'', ' ', data)
	data.strip()
	return data
	
def getPDFContent(pdfName):
	try:
#		 pages = number_of_pages(pdfName)
		#---------- PDF file Reading ----------------------------
		pdfDoc = PdfFileReader(open(pdfName, "rb"), strict=False)
		content = ''
		fileType = ''
		
		textFileName = os.path.basename(pdfName)
		
		fulltextNamewithPath = extract_text_directory+ '/' + textFileName
		
		fulltextNamewithPath = fulltextNamewithPath.replace('.pdf','.txt')
		
		# ------------Extract PDF using jar.exe and Reading First 3 Pages from PDF -----------
		for page in range(1, 4):
			run(pdfName, page)
			print('Completed {}'.format(page))
		
		#------------- Append 3 pages Content and delete temp text files ---------------------
		filenames=[]
		path=extract_text_directory+'/'
		for file in glob.glob(path+"*extracted.txt"):
			print(file)
			filenames.append(file)
			
		with open(fulltextNamewithPath, 'w') as outfile:
			for fname in filenames:
				with open(fname) as infile:
					for line in infile:
						outfile.write(line)
				os.remove(fname)
		file = open(fulltextNamewithPath, 'r')
		# need to add exception control for file to kill
		content = file.read()
		file.close()
		
		if not content:
			if '/XObject' in pdfDoc.getPage(0)['/Resources']:
				xObject = pdfDoc.getPage(0)['/Resources']['/XObject'].getObject()
				fileType = pdfimagetype(xObject)
		elif re.findall(r'^\s*$', content):
			print("Image Check")
			content = "PDF text extract timeout error"
			if '/XObject' in pdfDoc.getPage(0)['/Resources']:
				xObject = pdfDoc.getPage(0)['/Resources']['/XObject'].getObject()
				fileType = pdfimagetype(xObject)
		elif content is None:
			if '/XObject' in pdfDoc.getPage(0)['/Resources']:
				xObject = pdfDoc.getPage(0)['/Resources']['/XObject'].getObject()
				fileType = pdfimagetype(xObject)
		else:
			fileType = "Readable PDF file"
		
		if fileType is None:
			fileType = "Image type PDF file"
		if fileType == '':
			fileType = "Image type PDF file"
							
		os.remove(fulltextNamewithPath)
		
		return clean(content), fileType, testclean(content)
	
	except Exception as e:
		print("PDF path read Error: " + str(e))
		textFileName = os.path.basename(pdfName)		
		fulltextNamewithPath = extract_text_directory+ '/' + textFileName		 
		os.remove(fulltextNamewithPath.replace('.pdf','.txt'))
		logging.error('Error in reading PDF: '+"\t"+str(fulltextNamewithPath)+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+"\t"+str(datetime.now()))
		return "Error to read PDF", "Error to read PDF", "Error to read PDF"

def getpath(path, pID):
	try:
		documentLocation = ''
		# Get the list of all files in directory tree at given path
		for (dirpath, dirnames, filenames) in os.walk(path):
			print(dirpath)
			print(dirnames)
			print(filenames)
			documentLocation = (dirpath + '/' + filenames).replace("\\", "/")
			print(documentLocation)
			break
		return documentLocation
	except Exception as e:
		print("Unable to read file: " + str(e))
		return ("Error: Unable to read file")

def pdfimagetype(xObject):
	try:
		imageType = ''
		for obj in xObject:
			if xObject[obj]['/Subtype'] == '/Image':
				if '/Filter' in xObject[obj]:
					if xObject[obj]['/Filter'] == '/FlateDecode':
						imageType = 'Image file type .png'
					elif xObject[obj]['/Filter'] == '/DCTDecode':
						imageType = 'Image file type .png'
					elif xObject[obj]['/Filter'] == '/JPXDecode':
						imageType = 'Image file type .jp2'
					elif xObject[obj]['/Filter'] == '/CCITTFaxDecode':
						imageType = 'Image file type .tiff'
				else:
					imageType = 'Image file type .png'
		return imageType
	except Exception as e:
		print("PDF Image Error: " + str(e))		   
		return ("Error:PDF Image")


def filenameclean(data):
	try:
		data = re.sub(r'\'', ' ', data)
		data = re.sub(r'\s+', ' ', data)
		data.strip()
		return data
	except Exception as e:
		print("Filename clean Error: " + str(e))
		return ("Error:Filename clean")
		
def Doc_Description(Doc_name):
	try:
		pathname, fname = os.path.split(Doc_name)
		docDescription = ''
		docDesc = fname
		removeddocDesc = re.sub(r'^\d+_', '', str(docDesc))
		removeddocDesc1 = re.sub(r'\_', ' ', str(removeddocDesc))
		removeddocDesc1 = re.sub(r'\'', ' ', str(removeddocDesc1))
		removeddocDesc1 = re.sub(r'\.(pdf|PDF)', '', str(removeddocDesc1))
		docDescription = re.sub(r'\.(txt|jpg|jpeg|png|html|docx|doc|xml|rtf|RTF|db|jp2|tif|tiff|ipg)$', '', str(removeddocDesc1), re.I)
		docDescription = re.sub(r'\s+$', '', str(docDescription))
		logging.info("Doc Description parsed: "+"\t"+str(datetime.now()))
		return fname, docDescription
	except Exception as e:
		print("Doc description Error: " + str(e))
		logging.error("Exception in Doc Description : "+"\t"+str(e)+"\t"+str(datetime.now()))
		return "Error: Doc description error", "Error: Doc description error"
		
		
def db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery):
	i = 0
	DB_Commit_status = ""
	while i < 3:
		try:
			connLive.commit()
			DB_Commit_status = "Success"
			logging.info('DB Committed Successfully: '+"\t"+str(DB_Commit_status)+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(datetime.now()))
			break
		except Exception as er:
			time.sleep(5)
			logging.error('DB Commit Failed: '+"\t"+str(DB_Commit_status)+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(datetime.now()))
			print ("Error",er)
		i = i + 1
	return (connLive, DB_Commit_status)

#------ OneApp Classification Main Function ----------------------

if __name__ == "__main__":
	timestr = time.strftime("%Y%m%d-%H%M%S")
	todaydate = time.strftime('%Y-%m-%d')
	updated_date = time.strftime('%Y%m%d')
			
	logDirectory=Script_directory+"/logs/"+str(todaydate)
	# testSharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload_test/log/'+str(todaydate)
	# SharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload/log/'+str(todaydate)
	
	#---------- Folder or directory exists check------------------- 
	try:
		if os.path.isdir(str(logDirectory)) == False:
			os.makedirs(str(logDirectory))
		if os.path.isdir(str(extract_text_directory)) == False:
			os.makedirs(str(extract_text_directory))
		
		# if os.path.isdir(str(SharelogDirectory)) == False:
			# os.makedirs(str(SharelogDirectory))
		
		logFilename = "OneAPP_Classification_Log_{}.txt".format(str(todaydate))
		Log_File_Path = logDirectory+"/"+logFilename
		logging.basicConfig(
			filename = Log_File_Path,
			format = "%(asctime)s\t%(levelname)s\t%(funcName)s\t%(lineno)s\t%(message)s",
			level = logging.DEBUG)	
			
		logging.info("OneApp classification script Initiated : "+"\t"+str(datetime.now()))
		logging.info("logDirectory created : "+"\t"+str(datetime.now()))
		
		#---------- Jar and Module file check-------------------	
		Support_file_check=['C:/DocClassification/jar/pdfbox-app-2.0.16.jar','C:/DocClassification/Models/wordninjaa_glenigan.txt','C:/DocClassification/Models/desc_vec.pkl','C:/DocClassification/Models/file_vec.pkl','C:/DocClassification/Models/logistic_model.pkl','C:/DocClassification/Models/logistic_model1.pkl','C:/DocClassification/Models/logistic_model2.pkl','C:/DocClassification/Models/stopwords_eng_emails.txt']
		Sfile_check = [Sfile for Sfile in Support_file_check if os.path.isfile(Sfile) == False]
		if Sfile_check:
			print ('Support file Missing ',Sfile_check)
			logging.info("Support file Missing !!: "+"\t"+str(Sfile_check)+"\t"+str(datetime.now()))
			sys.exit()
		#-------------------------------------------------------------------
		
		insert_Query = 'insert into TBL_DOCUMENT_RENAME (Format_ID, project_id, Doc_Name, Doc_Content, test_doc_content, Doc_Description, pdf_extract_status, dedupe_process_status, Document_Path, updated_date, pdf_total_pages) values '
		
		Input_Query = "select project_id,format_id,destination_folder_path,dedupe_process_status,upload_no from tbl_document_uploaded where completed=\'\'" 
		
		connLive = db_connection("SCREENSCRAPPER")	  
		cursorLive = connLive.cursor()
		#------------------------------------------------------------ OneApp Classification section-----------------------
		upload_no = ''
		Input_Count = 0
		bulkValuesForQuery = ''
		docsCount = 0
		FormatIDList = []
		try:
			
			cursorLive.execute(Input_Query)
			fetchResults = cursorLive.fetchall()
			Input_Count=len(fetchResults)
			print ('Input_Count',Input_Count)
			logging.info('Input Count from	tbl_document_uploaded: '+"\t"+str(Input_Count)+"\t"+str(datetime.now()))
			#------------ Checking Input Count to Forwarding Process, If Input_Count == 0 Script will be exit --------------
			
			if Input_Count == 0:
				print('Input not available')
				logging.info('OneApp Classification Final script terminated as there is no input: '+"\t"+str(datetime.now()))
				sys.exit()
				
			for i in range(Input_Count):
				Project_ID = str(fetchResults[i][0])
				Format_ID = str(fetchResults[i][1])
				documentSourcePath = str(fetchResults[i][2])
				deDup_Status = str(fetchResults[i][3])
				upload_no = str(fetchResults[i][4])
				
				#------- Fetching Input Details from Database -------------------------------------
				documentSourcePath='C:\DocClassification\second_upload' +"\\"+ str(Project_ID)
				print("Format_ID", Format_ID)
				print("Project_ID", Project_ID)
				print("documentSourcePath", documentSourcePath)
				
				Destination_Path = (documentSourcePath).replace("\\", "/")
				Destination_Path = (documentSourcePath).replace("\/", "/")
				Destination_Path = re.sub(r'\'', '\'\'', Destination_Path)
				Destination_Path = re.sub(r'10.101.53.31', 'ch1031sf02', Destination_Path)
				
				logging.info('OneApp classification processing Project_ID : '+"\t"+str(Project_ID)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
				print("Destination_Path::::::", Destination_Path)
				
				#-------------- Checking Project Folder Location --------------------
				
				if not os.path.exists(Destination_Path):
					print ("Folder Not Exists")
					logging.info('Folder not Exists : '+"\t"+str(Project_ID)+"\t"+str(Destination_Path)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
					joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','','','','','Project Folder not exists in directory','" + str(deDup_Status) + "','" + str(Destination_Path) + "','" + str(updated_date) + "','')"
					bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
					if re.search('^\,\(', str(bulkValuesForQuery)):
						bulkValuesForQuery = insert_Query + joinValues
					try:
						cursorLive.execute(bulkValuesForQuery)
						(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
					except Exception as er:
						logging.error('Folder Not exists DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
						print ("Error",er)
					docsCount = 0
					bulkValuesForQuery = ''
				else:
					print ("Folder exists")
					#-------------------------If Project Folder Exist and then Fetching all file details --------------
					logging.info('Folder Exists : '+"\t"+str(Project_ID)+"\t"+str(Destination_Path)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
					files_path = [os.path.join(Destination_Path, x) for x in os.listdir(Destination_Path)]
					if(files_path):
						logging.info('Files available in Folder : '+"\t"+str(Project_ID)+"\t"+str(Destination_Path)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
						#--------------------- Identifying all files detail ---------------------------------------------
						for pathandfile in files_path:
							#-----------------Only PDF File Processing --------------------------
							if pathandfile.endswith(('.pdf', '.PDF')):
								pathandfile = pathandfile.replace("\\", "/")
								pathandfile = pathandfile.replace("\/", "/")
								pathandfile = pathandfile.replace(r'\'', '')
								print("FileFullPath::", pathandfile)	
								logging.info('PDF File exists in folder : '+"\t"+str(Project_ID)+"\t"+str(pathandfile)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
								try:
									#---------- Spliting PDF File Name & description ex: "application.pdf" & "application"------
									filename, docDescription = Doc_Description(pathandfile)
									filename = re.sub(r'\'', '\'\'', filename)
									pathandfile = re.sub(r'\'', '\'\'', pathandfile)
									print("filename:::", filename)
									print("docDescription:::", docDescription)
									if os.path.isfile(pathandfile):
										pdf_response=''
										docFirstPageContent, pdffileType, testDocFirstPageContent='','',''
										
										timeout = 5
										p = ThreadPool(1)
										
										#----------with threading Concept for checking corrupted PDF Or Not & getting PDF Response -----------
										
										res = p.apply_async(pdf_validation, args=(pathandfile,))
										try:
											pdf_response = res.get(timeout)
											if pdf_response=='':
												docFirstPageContent, pdffileType, testDocFirstPageContent = "Error","PDF","Error_content"
											else:
												#-------------- Getting First 3 Pages PDF Content and FileType ----------
												docFirstPageContent, pdffileType, testDocFirstPageContent = getPDFContent(pathandfile)
											logging.info('PDF Extraction succeeded : '+"\t"+str(Project_ID)+"\t"+str(pathandfile)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
										except multiprocessing.TimeoutError:
											logging.error('Error occurred when extracting PDFContent : '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(er)+"\t"+str(datetime.now()))
											print(pathandfile,": corrupted pdf Aborting due to timeout")
											p.terminate()
										#------------ Insert PDF content, type and input details in "TBL_DOCUMENT_RENAME" table --------	
										FormatIDList.append(Format_ID)
										if re.search('^Error', str(docFirstPageContent)) is None:
											joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(
											filename) + "','" + str(docFirstPageContent) + "','" + str(testDocFirstPageContent) + "','" + str(
											docDescription) + "','" + str(pdffileType) + "','" + str(
											deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','" + str(pdf_response) + "')"
											if docsCount == 0:
												bulkValuesForQuery = insert_Query + joinValues
												docsCount += 1
											elif docsCount == 10:
												print("Insert", docsCount)
												bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
												try:
													cursorLive.execute(bulkValuesForQuery)
													(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
												except Exception as er:
													logging.error('PDF file extraction query DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
													print ("Error",er)
												#------------ ruleSP Process -----------------------------
												docsCount = 0
												bulkValuesForQuery = ''
												FormatIDList1 = set(FormatIDList)
												FormatIDList2 = ",".join(FormatIDList1)
												print ("FormatIDList:", FormatIDList2)
												ruleSP = "exec proc_get_document_category '','" + FormatIDList2 + "'"
												try:
													cursorLive.execute(ruleSP)
													(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
												except Exception as er:
													logging.error('Rule SP DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
													print ("Error",er)
													
												FormatIDList = []
											else:
												bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
												docsCount += 1
										else:
											#------ Corrupted PDF Or Other Exceptions Details --------------------
											print ("PDF Extraction Error")
											pathandfile = pathandfile.replace("\'", "")
											joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(filename) + "','','','" + str(docDescription) + "','PDF Extraction Error','" + str(deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','" + str(pdf_response) + "')"
											bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
											if re.search('^\,\(', str(bulkValuesForQuery)):
												bulkValuesForQuery = insert_Query + joinValues
											try:
												cursorLive.execute(ruleSP)
												(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
											except Exception as er:												   
												logging.error('PDF Extraction DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
												print ("Error",er)
											docsCount = 0
											bulkValuesForQuery = ''
										logging.info('PDF File extraction section completed successfully : '+"\t"+str(Project_ID)+"\t"+str(pathandfile)+"\t"+str(upload_no)+"\t"+str(datetime.now()))		
								except Exception as er:
									logging.error('Error occurred when checking PDF Files available in folder:'+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(er)+"\t"+str(datetime.now()))
									print ("Error",er)
							else:
								#-------------- Other file type Process (without PDF)------------
								try:
									filename, docDescription = Doc_Description(pathandfile)
									filename = re.sub(r'\'', '\'\'', filename)
									pathandfile = pathandfile.replace('\'', '')
									pathandfile = pathandfile.replace("\\", "/")
									pathandfile = pathandfile.replace("\/", "/")
									pathandfile = re.sub(r'\'', '\'\'', pathandfile)
									joinValues = "(" + "'" + str(Format_ID) + "','" + str(Project_ID) + "','" + str(filename) + "','','','','Not a PDF file type','" + str(deDup_Status) + "','" + str(pathandfile) + "','" + str(updated_date) + "','')"
									logging.info('Not a PDF file type : '+"\t"+str(Project_ID)+"\t"+str(filename)+"\t"+str(pathandfile)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
									if docsCount == 0:
										bulkValuesForQuery = insert_Query + joinValues
										docsCount += 1
									elif docsCount == 10:
										bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
										try:
											cursorLive.execute(bulkValuesForQuery)
											(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
										except Exception as er:
											logging.error('Not a PDF file type DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
											print ("Error",er)
										docsCount = 0
										bulkValuesForQuery = ''
									else:
										bulkValuesForQuery = bulkValuesForQuery + "," + joinValues
										docsCount += 1
									logging.info('Not a PDF file type checked successfully: '+"\t"+str(Project_ID)+"\t"+str(filename)+"\t"+str(pathandfile)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
								except Exception as er:
									logging.error('Error occurred when checking Not a pdf file type:'+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(er)+"\t"+str(datetime.now()))
					else:
						logging.info('Files not available in folder: '+"\t"+str(Project_ID)+"\t"+str(Destination_Path)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
			if (bulkValuesForQuery != ''):
				try:
					cursorLive.execute(bulkValuesForQuery)
					(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
				except Exception as er:
					logging.error('Bulk Query DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
					print ("Error",er)
			#---------------- ruleSP Process --------------------------------------		
			FormatIDList1 = set(FormatIDList)
			FormatIDList2 = ",".join(FormatIDList1)
			print ("FormatIDList2::", FormatIDList2)
			ruleSP = "exec proc_get_document_category '','" + FormatIDList2 + "'"
			try:
				cursorLive.execute(bulkValuesForQuery)
				(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
			except Exception as er:
				logging.error('proc_get_document_category DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
				print ("Error",er)
			connLive.commit()	 
		except Exception as er:
			logging.error('OneApp classification script Failed: '+"\t"+str(er)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
			print ("Error",er)
		#--------------------------------- Document_Classification_Upload section -----------------------
		try:
			gClassification = dynamic_import('Document_Classification_Upload', Script_directory+'/Document_Classification_Upload.py')
			gClassification.Prediction()
			######----------- Checking Classifier_category rows from TBL_DOCUMENT_RENAME-------------------
			checkQuery = ''
			if int(upload_no) == int(1):
				checkQuery = "select Classifier_category from TBL_DOCUMENT_RENAME where CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112) and Document_Path like \'//ch1031sf02/commonglenigan/"+str(currentYear)+"/document_upload/first_upload%\' and (Classifier_category is NOT NULL or Classifier_category <> \'\')"
			elif int(upload_no) == int(2):
				checkQuery = "select Classifier_category from TBL_DOCUMENT_RENAME where CONVERT(varchar,updated_date,112) = convert(varchar,GETDATE(),112) and Document_Path like \'//ch1031sf02/commonglenigan/"+str(currentYear)+"/document_upload/second_upload%\' and (Classifier_category is NOT NULL or Classifier_category <> \'\')"
			print (checkQuery)
			
			row=''
			try:
				cursorLive.execute(checkQuery)
				row = cursorLive.fetchone()
				######----------- If Classifier_category row is None,the script will be stop ------------------
				if row == None:
					print("There are no results for this query")
					logging.info('TBL_DOCUMENT_RENAME Classifier_category query has no result '+"\t"+str(upload_no)+"\t"+str(datetime.now()))
					update_commonquery = "update tbl_common set document_upload_started=\'\'"
					cursorLive.execute(update_commonquery)
					sys.exit()	  
				
				####------------ If Classifier_category row is available in TBL_DOCUMENT_RENAME table-------------
				
				categorization_qry = """select	a.project_id,b.Doc_Name ,a.format_id,
									c.APPLICATION_NO,ltrim(c.council_cd) as council_code,c.APPLICATION_URL,b.pdf_extract_status,a.project_uploaded_date,
									a.upload_no,b.rule_Category,b.rule_Comment,b.Classifier_Category,b.Document_Path
									from	tbl_document_uploaded as a,tbl_document_rename as b,
									DR_APPLICATION as c
									where a.project_id =b.project_id
									and a.project_id =c.PROJECT_ID
									and a.completed =\'\'"""
				try:					
					cursorLive.execute(categorization_qry)
					categ_results = cursorLive.fetchall()
					Cat_result = len(categ_results)
					
					CateinsertQuery = 'insert into tbl_document_categorization ( project_id,doc_name,format_id,application_no,council_code,url,pdf_extract_status,Project_uploaded_date,upload_no,rule_Category,rule_Comment,delete_flag,Meta_level1,Document_Path,csv_created) values'

					bulkInsertForQuery = ''
					CateCount = 0
					for i in range(Cat_result):
						cateProject_ID = str(categ_results[i][0])
						cateDoc_Name = str(categ_results[i][1])
						cateFormat_ID = str(categ_results[i][2])
						cateApp_no = str(categ_results[i][3])
						cateCouncil_CD = str(categ_results[i][4])
						cateApp_URL = str(categ_results[i][5])
						catePDF_EStatus = str(categ_results[i][6])
						cateProject_UDate = str(categ_results[i][7])
						cateUpload_no = str(categ_results[i][8])
						rule_Category = str(categ_results[i][9])
						rule_Comment = str(categ_results[i][10])
						Classifier_Category = str(categ_results[i][11])
						Document_Path = str(categ_results[i][12])
						
						cateDoc_Name = re.sub(r'\'', "\'\'", str(cateDoc_Name))
						cateApp_URL = re.sub(r'\'', "\'\'", str(cateApp_URL))
						Document_Path = re.sub(r'\'', "\'\'", str(Document_Path))
						
						######-------------- Mapping Classifier_Category ------------------------------
						
						meta_level1 = ''
						delete_flag = ''
						if Classifier_Category == "999" or Classifier_Category == '' or Classifier_Category == 'None':
							delete_flag = 'Yes'
						elif Classifier_Category == "0" or Classifier_Category == "5":
							delete_flag = 'Yes'
						elif (catePDF_EStatus == 'Project Folder not exists in directory' or catePDF_EStatus == 'Not a PDF file type' or catePDF_EStatus == 'None' or catePDF_EStatus == '') and (Classifier_Category == "0" or Classifier_Category == "5" or Classifier_Category == "999" or Classifier_Category == '' or Classifier_Category == 'None'):
							delete_flag = 'Yes'
						elif re.findall(r'\.(txt|jpg|jpeg|png|html|docx|doc|xml|rtf|RTF|db|tif|tiff|jp2|ipg)$', str(cateDoc_Name), re.I):
							delete_flag = 'Yes'
						else:
							delete_flag = 'No'
							
						if (Classifier_Category == "999" or Classifier_Category == '' or Classifier_Category == 'None' or Classifier_Category == "0") and catePDF_EStatus == 'Readable PDF file':
							catePDF_EStatus = 'Unable to classify - content not sufficient'
						elif type(Classifier_Category).__name__ == 'int':			 
							if (1 <= int(Classifier_Category) <= 5) and catePDF_EStatus == 'Readable PDF file':
								catePDF_EStatus = ''
								
						print ("###############")
						print ("Classifier_Category==>", Classifier_Category)
						print ("rule_Category==>", rule_Category)
						print ("rule_Comment==>", rule_Comment)
						print ("delete_flag==>", delete_flag)
						print ("cateProject_ID==>", cateProject_ID)
						print ("cateDoc_Name==>", cateDoc_Name)
						print ("cateApp_no==>", cateApp_no)
						print ("cateCouncil_CD==>", cateCouncil_CD)
						
						######----------- Mapping rule Formation and Meta level Process------------------------
						
						ruleFormation = {
							"1": "Application Form",
							"2": "Design & Access Statement",
							"3": ["Plans - Existing", "Plans - Proposed"],
							"4": "Statements",
							"5": "Letters & Emails"
						}
						
						if rule_Comment == 'Existing' and Classifier_Category == '3':
							meta_level1 = ruleFormation["3"][0]
						elif Classifier_Category == '3':
							meta_level1 = ruleFormation["3"][1]
						elif Classifier_Category != 'None' and Classifier_Category != '0' and Classifier_Category != '3' and Classifier_Category != '999':
							meta_level1 = ruleFormation[Classifier_Category]
							
						print ("meta_level1==>", meta_level1)
						
						CatejoinValues = "(" + "'" + str(cateProject_ID) + "','" + str(cateDoc_Name) + "','" + str(cateFormat_ID) + "','" + str(cateApp_no) + "','" + str(cateCouncil_CD) + "','" + str(cateApp_URL) + "','" + str(catePDF_EStatus) + "','" + str(cateProject_UDate) + "','" + str(cateUpload_no) + "','" + str(rule_Category) + "','" + str(rule_Comment) + "','" + str(delete_flag) + "','" + str(meta_level1) + "','" + str(Document_Path) + "','" + str(updated_date) + "')"
						
						if CateCount == 0:
							bulkInsertForQuery = CateinsertQuery + CatejoinValues
							CateCount += 1
						elif CateCount == 10:
							print("Insert", CateCount)
							bulkInsertForQuery = bulkInsertForQuery + "," + CatejoinValues
							print (bulkInsertForQuery)
							try:
								cursorLive.execute(bulkValuesForQuery)
								(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
							except Exception as er:
								logging.error('Meta level DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
								print ("Error",er)
							CateCount = 0
							bulkInsertForQuery = ''
						else:
							bulkInsertForQuery = bulkInsertForQuery + "," + CatejoinValues
							CateCount += 1
						
						######------------- Uploading in tbl_document_uploaded table -----------------------------
						
						completedFinalUpdate1 = "update tbl_document_uploaded set completed=\'y\' where completed=\'\' and project_id = \'"+str(cateProject_ID)+"\' and format_id = \'"+str(cateFormat_ID)+"\'"
						print (completedFinalUpdate1)
						
						try:					
							cursorLive.execute(completedFinalUpdate1)
						except Exception as er:
							logging.error('tbl_document_uploaded completed query execution failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
							print ("tbl_document_uploaded completed query execution failed")
					if (bulkInsertForQuery != ''):
						try:
							cursorLive.execute(bulkValuesForQuery)
							(connLive, DB_Commit_status) = db_commit(connLive,todaydate,upload_no,Project_ID,bulkValuesForQuery)
						except Exception as er:
							logging.error('tbl_document_categorization DB Commit Failed: '+"\t"+str(upload_no)+"\t"+str(Project_ID)+"\t"+str(bulkValuesForQuery)+"\t"+str(er)+"\t"+str(datetime.now()))
							print ("Error",er)
				except Exception as er:
					logging.error('tbl_document_categorization process failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
					print ("Error",er)
			except Exception as er:
				logging.error('tbl_document_categorization process failed: '+"\t"+str(er)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
				print ("Error",er)
		except Exception as er:
			logging.error('Document_Classification_Upload script execution failed: '+"\t"+str(er)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
			print ("Error",er)	
	#------------------------------------------------------------ Delete_PDF_Files_For_Others section-----------------------
		try:
			deletePDFs = dynamic_import('Delete_PDF_Files_For_Others',Script_directory+'/Delete_PDF_Files_For_Others.py')
			deletePDFs.deletePDF(str(upload_no), str(updated_date)) 
			datetimenow = time.strftime('%Y%m%d%H%M%S')
			year = datetime.strptime(date, "%Y-%m-%d").strftime("%Y")
			
			print ("upload_no==>", upload_no)

			uploadDirectory = ''
			if upload_no == '1' or upload_no == 1:
				uploadDirectory = '//ch1031sf02/commonglenigan/' + year + '/document_upload/first_upload'
			elif upload_no == '2' or upload_no == 2:
				uploadDirectory = '//ch1031sf02/commonglenigan/' + year + '/document_upload/second_upload'
			
			#------------------ csv File Created for imported File------------------------------------
			finalcsvfileQuery = """select DISTINCT Project_uploaded_date,Project_ID,Format_ID,Application_No,Council_Code,doc_name,Meta_level1,Meta_level2,pdf_extract_status from tbl_document_categorization where delete_flag = 'No' and (delete_status = '' or delete_status is null ) and upload_no = '%s' and csv_created = '%s'""" % (
			upload_no, updated_date)
			
			try:
				cursorLive.execute(finalcsvfileQuery)
				fetchFinalResults = cursorLive.fetchall()
				FinalResults_count = len(fetchFinalResults)
				filename = uploadDirectory + "/Import" + datetimenow + ".csv";
				try:
					with open(filename, 'w', newline='') as csvfile:
                        fieldnames = ["Sno", "Project uploaded date", "Project_ID", "Format_ID", "Application_No", "Council_Code", "File_Name", "Meta_level1", "Meta_level2", "Comments"]
                        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                        writer.writeheader()
					
					sno = 0
					for i in range(FinalResults_count):
						Project_uploaded_date = str(fetchFinalResults[i][0])
						final_Project_ID = str(fetchFinalResults[i][1])
						final_Format_ID = str(fetchFinalResults[i][2])
						final_Application_No = str(fetchFinalResults[i][3])
						final_Council_Code = str(fetchFinalResults[i][4])
						final_File_Name = str(fetchFinalResults[i][5])
						final_Meta_level1 = str(fetchFinalResults[i][6])
						final_Meta_level2 = str(fetchFinalResults[i][7])
						final_Comments = str(fetchFinalResults[i][8])
						
						final_Comment = ''
						final_Meta_level2 = ''
						
						sno = sno + 1
						if re.findall(r'Image', str(final_Comments)):
							final_Comments = 'Image File Type'
							
						Project_uploaded_date = datetime.strptime(Project_uploaded_date, '%Y-%m-%d').strftime('%d/%m/%Y')
						
						writer.writerow({'Sno': sno, 'Project uploaded date': str(Project_uploaded_date),
										 'Project_ID': str(final_Project_ID), 'Format_ID': str(final_Format_ID),
										 'Application_No': str(final_Application_No), 'Council_Code': str(final_Council_Code),
										 'File_Name': str(final_File_Name), 'Meta_level1': str(final_Meta_level1),
										 'Meta_level2': str(final_Meta_level2), 'Comments': str(final_Comment)})
				except Exception as e:
					print (e, sys.exc_traceback.tb_lineno)
			except Exception as er:
				logging.error('tbl_document_categorization & delete_flag No query execution failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
				print ("Error",er)
			# ------------------------- Update tbl_common as empty -----------------------------
			update_statusquery = "update tbl_common set document_upload_started=\'\'"
			cursorLive.execute(update_statusquery)
			
			#------------------------- Update tbl_document_uploaded as completed='y' -----------------------------
			completedFinalUpdate = "update tbl_document_uploaded set completed='y' where completed=\'\'"
			cursorLive.execute(completedFinalUpdate)
			connLive.commit()
			
			# -------------------------- CSV file create for deleted file list ------------------------------------
			deletedQuery = """select DISTINCT Project_uploaded_date,Project_ID,Format_ID,Application_No,Council_Code,doc_name,Meta_level1,Meta_level2,pdf_extract_status,rule_Category from tbl_document_categorization where delete_flag = 'Yes' and upload_no = '%s' and csv_created = '%s'""" % (
			upload_no, updated_date)
			try:
				cursorLive.execute(deletedQuery)
				fetchdeletedResults = cursorLive.fetchall()
				fetchdeletedResults_count = len(fetchdeletedResults)
				deletedfilename = uploadDirectory + "/Import_deleted" + datetimenow + ".csv";
				try:
					with open(deletedfilename, 'w', newline='') as csvfile:
                        fieldnames = ["Sno", "Project uploaded date", "Project_ID", "Format_ID", "Application_No", "Council_Code", "File_Name", "Meta_level1", "Meta_level2", "Comments"]
                        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                        writer.writeheader()
					
					sno = 0
					for i in range(fetchdeletedResults_count):
						Project_uploaded_date = str(fetchdeletedResults[i][0])
						final_Project_ID = str(fetchdeletedResults[i][1])
						final_Format_ID = str(fetchdeletedResults[i][2])
						final_Application_No = str(fetchdeletedResults[i][3])
						final_Council_Code = str(fetchdeletedResults[i][4])
						final_File_Name = str(fetchdeletedResults[i][5])
						final_Meta_level1 = str(fetchdeletedResults[i][6])
						final_Meta_level2 = str(fetchdeletedResults[i][7])
						final_Comments = str(fetchdeletedResults[i][8])
						rule = str(fetchdeletedResults[i][9])
						final_Meta_level2 = ''
						
						sno = sno + 1
						if re.findall(r'Image', str(final_Comments)):
							final_Comments = 'Image File Type'
							
						Project_uploaded_date = datetime.strptime(Project_uploaded_date, '%Y-%m-%d').strftime('%d/%m/%Y')

						if rule == '1' and re.findall(r'\.(txt|db)', str(final_File_Name)):
							print ('No need to entry in delete log')
						else:
							writer.writerow({'Sno': sno, 'Project uploaded date': str(Project_uploaded_date),
											 'Project_ID': str(final_Project_ID), 'Format_ID': str(final_Format_ID),
											 'Application_No': str(final_Application_No),
											 'Council_Code': str(final_Council_Code), 'File_Name': str(final_File_Name),
											 'Meta_level1': str(final_Meta_level1), 'Meta_level2': str(final_Meta_level2),
											 'Comments': str(final_Comments)})
				except Exception as e:
					print (e, sys.exc_traceback.tb_lineno)
			except Exception as er:
				logging.error('tbl_document_categorization & delete_flag Yes query execution failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
				print ("Error",er)
		except Exception as er:
			logging.error('Delete_PDF_Files_For_Others script execution failed: '+"\t"+str(er)+"\t"+str(upload_no)+"\t"+str(datetime.now()))
			print ("Error",er)
		cursorLive.close	
	except Exception as er:
		logging.error('OneApp classification script failed: '+"\t"+str(er)+"\t"+str(datetime.now()))
		print ("Error",er)
	
	
	logging.info('OneApp Classification Final script execution completed successfully: '+"\t"+str(datetime.now()))