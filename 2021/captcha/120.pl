use strict;
use Mozilla::CA;
use File::Path 'rmtree';
use File::Find;

use URI::Escape;
use HTTP::Cookies;
use HTML::Entities;
use WWW::Mechanize;
use IO::Socket::SSL qw();

my $pageFetch = WWW::Mechanize->new( 
		agent => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36',
		ssl_opts => {
		SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
		verify_hostname => 0, 
		}, autocheck => 0);
   
my $documentURL="https://publicaccess.carlisle.gov.uk/online-applications/search.do?action=advanced";
# $pageFetch->add_header("Host" => "planning.bournemouth.gov.uk");
my $Post_Content="";

# my ($docPageContent,$Code) = &docPostMechMethod($documentURL,$Post_Content,$pageFetch);
my ($docPageContent,$Code) = &docMechMethod($documentURL,$pageFetch);
print "Code::$Code\n";
open ss,">check.html";
print ss $docPageContent;
close ss;

=e
my($VS,$VSG,$EV,$Appno);

if($docPageContent=~m/id="[^>]*?VIEWSTATE"[^>]*?value="\s*([^>]*?)\s*"/is)
{
	$VS=uri_escape($1);
}
if($docPageContent=~m/id="[^>]*?VIEWSTATEGENERATOR"[^>]*?value="\s*([^>]*?)\s*"/is)
{
	$VSG=uri_escape($1);
}
if($docPageContent=~m/id="[^>]*?EVENTVALIDATION"[^>]*?value="\s*([^>]*?)\s*"/is)
{
	$EV=uri_escape($1);
}

my $Appno="7-2021-2471-D";

while($docPageContent=~m/(<tr[^>]*?onclick=\"__doPostBack[^>]*?>[\w\W]*?<\/tr>)/igs)
{
	my $block=$1;
	
	if($block=~m/onclick=\"__doPostBack\(\&\#39;ctl00\$MainContent\$gridDocuments\&\#39;,\&\#39;([^>]*?)\&\#39;\)"[^>]*?>\s*[\w\W]*?<input\s*type[^>]*?value=\".*\\([^>]*?)\"\s*\/>/is)
	{
		my $value=$1;
		my $filename=$2;
		print "value::$value\n";
		print "filename::$filename\n";
		my $param="__EVENTTARGET=ctl00%24MainContent%24gridDocuments&__EVENTARGUMENT=$value&__VIEWSTATE=$VS&__VIEWSTATEGENERATOR=$VSG&__EVENTVALIDATION=$EV&ctl00_MainContent_RadTabStrip1_ClientState=%7B%22selectedIndexes%22%3A%5B%222%22%5D%2C%22logEntries%22%3A%5B%5D%2C%22scrollState%22%3A%7B%7D%7D&ctl00%24MainContent%24txtAppNo=7-2021-2471-D&ctl00_MainContent_RadMultiPage1_ClientState=";

		my $documentURL="https://planning.bournemouth.gov.uk/plandisp.aspx?tab=2&recno=101796";
		$pageFetch->add_header("Host" => "planning.bournemouth.gov.uk");
		$pageFetch->add_header("content-type" => "application/x-www-form-urlencoded");


		my ($docPageContent,$Code) = &docPostMechMethod($documentURL,$param,$pageFetch);
		# my ($docPageContent,$Code) = &docMechMethod($documentURL,$pageFetch);
		print "Code::$Code\n";
		open ss,">check1_$value"."_"."$filename";
		binmode ss;
		print ss $docPageContent;
		close ss;
	}
}

=cut 

sub docMechMethod() 
{
    my $URL = shift;
    my $mech = shift;   

	$URL=~s/amp\;//igs;
	
	print "URL==>$URL\n";
	
	$mech->proxy(['http','https'], 'http://172.27.140.48:3128');
	
	
	# BEGIN {
	 # $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
	 # $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
	 # $ENV{HTTPS_DEBUG} = 1;  #Add debug output
	# }

	my $proxyFlag=0;
	rePingwith_199:
	$mech->get($URL);
	my $con = $mech->content;
    my $code = $mech->status;
	print "Code: $code\n";
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		$mech->proxy(['http','https'], 'http://172.27.137.192:3128');
		goto rePingwith_199;
	}
    return($con,$code);
}


sub docPostMechMethod() 
{
    my $URL = shift;
    my $postcontent = shift;
    my $mech = shift;
   
	# print "$postcontent\n"; <STDIN>;

	$URL=~s/amp\;//igs;
	
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
	
	
	BEGIN {
	 $ENV{HTTPS_PROXY} = 'http://172.27.140.48:3128';
	 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
	 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
	}
	
	my $proxyFlag=0;
	rePingwith_192:
	$mech->post( $URL, Content => "$postcontent");
	  
	my $con = $mech->content;
    my $code = $mech->status;
	my $res = $mech->res();
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		$mech->proxy(['http','https'], 'http://172.27.137.192:3128');
		goto rePingwith_192;
	}
	
	my $filename;
	if($res->is_success){
		$filename = $res->filename();
	}
	$filename=~s/\s+/_/igs;
    return($con,$code,$filename);
}