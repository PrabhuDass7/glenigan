package AntiCaptcha;
use strict;
use 5.008_005;
our $VERSION = '0.01';
use Carp 'croak';

use LWP::UserAgent;
use URI;

use MIME::Base64;
use JSON;

use vars qw/$errstr/;
sub errstr {​​​ $errstr }​​​

sub new {​​​
    my $class = shift;
    my %args = @_ % 2 ? %{​​​$_[0]}​​​ : @_;
    $args{​​​clientKey}​​​ or croak "clientKey is required.\n";
    $args{​​​ua}​​​ ||= LWP::UserAgent->new;
    $args{​​​url}​​​ ||= 'https://api.anti-captcha.com/';
    $args{​​​sleep}​​​ ||= 3;
    return bless \%args, $class;
}​​​

sub createTask {​​​
    my ($self, $task, $softId, $languagePool) = @_;
    $self->request('createTask',
        task => $task,
        $softId ? (softId => $softId) : (),
        $languagePool ? (languagePool => $languagePool) : (),
    );
}​​​
sub getTaskResult {​​​
    my ($self, $taskId) = @_;
    $self->request('getTaskResult', taskId => $taskId);
}​​​
sub getBalance {​​​
    my ($self) = @_;
    $self->request('getBalance');
}​​​
sub getQueueStats {​​​
    my ($self, $queueId) = @_;
    $self->request('getQueueStats', queueId => $queueId);
}​​​
sub reportIncorrectImageCaptcha {​​​
    my ($self, $taskId) = @_;
    $self->request('reportIncorrectImageCaptcha', taskId => $taskId);
}​​​
sub request {​​​
    my ($self, $url, %params) = @_;
    $params{​​​clientKey}​​​ = $self->{​​​clientKey}​​​;
    my $res = $self->{​​​ua}​​​->post($self->{​​​url}​​​ . $url,
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Content' => encode_json(\%params),
    );
    # print Dumper(\$res); use Data::Dumper;
    unless ($res->is_success) {​​​
        $errstr = "Failed to post $url: " . $res->status_line;
        return;
    }​​​
    return decode_json($res->decoded_content);
}​​​
1;