#!/usr/bin/perl
use strict;
use DateTime;
# use warnings;
use IO::Socket::SSL;
use WWW::Mechanize;
use List::Util qw(first);
# use Devel::Size qw(size total_size);
use Cwd;

# use 5.010;
# use AnyEvent;
# use AnyEvent::HTTP;

use LWP::UserAgent;
use URI::URL;
use HTTP::Cookies;
use Cwd qw(abs_path);
use File::Basename;
use LWP::Simple;
use IO::Socket::SSL qw();
use Mozilla::CA;

# $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
# use CACertOrg::CA;

my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);

my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);               
$ua->cookie_jar($cookie_jar);
$cookie_jar->save;


my $cwd = getcwd();

####
# Declare global variables
####
my $mech;
my $filename = "$cwd/AllLiveURLs.txt";

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date

if (open(my $fh, '<:encoding(UTF-8)', $filename)) 
{
    while (my $url = <$fh>) 
	{
		chomp $url;
		
		my ($CouncilCode,$URL)=split("\t",$url);
		
		my($SSL_VERIFICATION);
		print "URL=>$URL\n";
		my $rerunCount=0;
		my ($responseCode,$content,$newURL,$searchURL);
		Loop:
		if($URL=~m/^https/is)
		{
			$SSL_VERIFICATION = 'Y';
		}
		
		if($SSL_VERIFICATION=~m/^Y$/is)
		{
			$mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
					);
		}
		else
		{	
			$mech = WWW::Mechanize->new(autocheck => 0);			
		}	
		
		if($responseCode=~m/^(5\d{2}|4\d{2})$/is)
		{
			print "Council code:$CouncilCode === Working via Old proxy..\n";
			$mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
		}
		else
		{
			print "Council code:$CouncilCode === Working via New proxy..\n";
			$mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy	
		}
		
		$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$mech->get($URL);
		
		$content = $mech->content;
		$responseCode = $mech->status;
		$newURL = $mech->uri();
		print "responseCode=>$responseCode\n";
	
		if(($responseCode!~m/^\s*200\s*$/is) && ($rerunCount<=1))
		{
			$rerunCount++;
			goto Loop;
		}
		
		(my $Content,$responseCode,my $Redir_Url) = &Getcontent_with_Proxy($URL,"","","","","","") if($responseCode!~m/20/is);
		# (my $Content,$responseCode,my $Redir_Url) = &anyevent($URL) if($responseCode!~m/20/is);
		



		
		print "Code=>$responseCode\n";
		
		open(PP,">>${todaysDate}.txt");
		print PP "$CouncilCode\t$URL\t$responseCode\n";
		close(PP);
		
		if($responseCode!~m/20/is)
		{
			open(PP,">>500_${todaysDate}.txt");
			print PP "$CouncilCode\t$URL\t$responseCode\n";
			close(PP);
		}
	}
   close($fh);
}


# sub anyevent
# {
	# my $url = shift;
	# my $cv = AnyEvent->condvar;
	# my $c=0;
	# my $html;my $status;my $st;
	# say "Start $url";
	# $cv->begin;
	# http_get $url, sub {
		# ($html,$status) = @_;
		# $st = $status->{Status};
		# say $c;
		# say $st;
		# say "$url received, Size: ", length $html;
		# $cv->end;
	# };		
	# $cv->recv;
	# return($html,$st);
# }

sub Getcontent_with_Proxy()
{
	my $Document_Url=shift;
	my $URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $Accept_Language=shift;

	if($Proxy eq 'Y')
	{
		print "via 199...\n";
		$ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	}
	elsif($Proxy eq '192')
	{
		print "via 192...\n";
		$ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	}
	
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE 1:: $code :: $Proxy\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		print "CODE 2:: $code :: $Proxy\n";
	}
	elsif($code=~m/30/is)
	{
		print "CODE 3:: $code :: $Proxy\n";
		my $loc=$res->header("location");
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				if($Document_Url=~m/rugby\.gov\.uk/is)
				{
					$Document_Url=$Document_Url;					
				}
				else
				{
					$Document_Url=$u2;
				}
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	elsif($Proxy eq 'Y')
	{
		print "CODE 4:: $code :: $Proxy\n";
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			$proxyFlag=1;
			print "via [[192]]...\n";
			$ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		print "CODE 5:: $code :: $Proxy\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			sleep(1);
			goto Home;
		}
	}
	# print "redir_url==>$redir_url\n";<STDIN>;
	return ($content,$code,$redir_url);
}

