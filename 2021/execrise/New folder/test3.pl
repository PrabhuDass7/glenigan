use strict;
use warnings;

use Spreadsheet::ParseExcel::SaveParser;
use Spreadsheet::WriteExcel;

# check for right parameters
my ($file,$howlong,$sleep) = @ARGV;
unless (  defined $file &&  $file =~/.+\.xlsx/ &&
          defined $howlong &&  $howlong =~/^\d+$/ &&
          defined $sleep &&   $sleep  =~/^\d+$/){
     die "USAGE: $0 filename.xls minutes_of_duration sleep_seconds",
         "\n\n\t$0 file.xls 5 1\n\n",
         "will creates file.xls and each seconds writes to it, for 5 minutes.\n";}
$howlong *=1;
$howlong = $^T+$howlong;
print "\tNB: $0 will run until ",scalar localtime($howlong),"\n";

my ($col,$row);
$col = $row = 4;
# initialize ta new XLS file
# my $workbook_init = Spreadsheet::WriteExcel->new($file);
# my $worksheet_init = $workbook_init->add_worksheet();
# $worksheet_init->write( $row, $col, "XLS initialized at Dasss".scalar localtime(time));
# $workbook_init->close();
# $row++;

# modify the same xls in the while loop
while (time <= $howlong){
    print "writing row $row\n";
    my $parser   = Spreadsheet::ParseExcel::SaveParser->new();
    my $template = $parser->Parse($file);
    my $worksheet = $template->worksheet(0);
    $worksheet->AddCell( $row, $col, scalar localtime (time) );
    $template->SaveAs($file);
    ++$row;
    sleep $sleep;
}
print "\treached ",(scalar localtime(time)),"\n\tscheduled termination"
