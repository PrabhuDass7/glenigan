#!/usr/bin/perl -w

use strict;
use Spreadsheet::ParseExcel;
use Spreadsheet::WriteExcel;
use Spreadsheet::ParseExcel::SaveParser;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
print qq[Content-type:text/html \n\n];
my $cs='planing.xls';
 # Open the template with SaveParser
my $parser   = new Spreadsheet::ParseExcel::SaveParser;
#my $formatter=Spreadsheet::ParseExcel::FmtJapan->new();
my $template = $parser->Parse('e.xls');

my $sheet    = 0;
my $row      = 0;
my $col      = 2;

# Get the format from the cell
my $format   = $template->{Worksheet}[$sheet]
                        ->{Cells}[$row][$col]
                        ->{FormatNo};



# Write data to some cells
$template->AddCell(0, $row,   $col,   1,     $format);
$template->AddCell(0, $row+1, $col, "This is a hello world eg", $format);
#$format->{Lock};
# Add a new worksheet
# $template->AddWorksheet('New Data');

# The SaveParser SaveAs() method returns a reference to a
# Spreadsheet::WriteExcel object. If you wish you can then
# use this to access any of the methods that aren't
# available from the SaveParser object. If you don't need
# to do this just use SaveAs().
#
my $workbook;

{
    # SaveAs generates a lot of harmless warnings about unset
    # Worksheet properties. You can ignore them if you wish.
    local $^W = 0;

    # Rewrite the file or save as a new file
    $workbook = $template->SaveAs('new.xls');

}

# Use Spreadsheet::WriteExcel methods

my $worksheet  = $workbook->sheets(0);
# $worksheet->protect();
#$worksheet->write('A1:B1','=1+2');
#my $locked  = $workbook->add_format();
# $locked->set_locked(1); # A non-op

#my $unlocked = $workbook->add_format();
#$locked->set_locked(0);

# Enable worksheet protection
#$worksheet->protect();

# This cell cannot be edited.
#$worksheet->write('A1:B1', '=1+2', $locked);

$worksheet->write($row+2, $col, "World2");

$workbook->close();
# print qq[
# <head>
# <script> 

# </script>
# </head>
# <body>

# <p>The download should start shortly. If it doesn't, click
# <a id="downloadLink" href="http://128.9.45.168/~mint/MINT_Portal/macro/963/cgi/$cs"     download="$cs" target="_blank">here</a>.</p>
# </body>
# </html>

# ];