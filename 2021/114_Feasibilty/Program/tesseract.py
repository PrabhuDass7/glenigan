import os
import io
from PIL import Image
import pytesseract
from wand.image import Image as wi
import gc

pdf_path="C:\\Glenigan\\Dass\\Form3\\Multi_and_Scanned.pdf"
pdf_path="C:\\Users\\prabhud\\Desktop\\Format3\\Multi_and_Editable.pdf"

pdf=wi(filename=pdf_path,resolution=300)
pdfImg=pdf.convert('pdf')

imgBlobs=[]
extracted_text=[]

def Get_text_from_image(pdf_path):
    pdf=wi(filename=pdf_path,resolution=300)
    pdfImg=pdf.convert('pdf')
    imgBlobs=[]
    extracted_text=[]
    for img in pdfImg.sequence:
        page=wi(image=img)
        imgBlobs.append(page.make_blob('pdf'))

    for imgBlob in imgBlobs:
        im=Image.open(io.BytesIO(imgBlob))
        text=pytesseract.image_to_string(im,lang='eng')
        extracted_text.append(text)

    return (extracted_text)
	
extracted_text=Get_text_from_image(pdf_path)
print ("extracted_text:::",extracted_text)