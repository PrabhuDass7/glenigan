use warnings;
use strict;
use CGI;
use LWP::UserAgent;
use JSON::Parse 'parse_json';
use Data::Dumper;
use Captcha::reCAPTCHA::V2;

my $secret = '6Ld00HoaAAAAADYcqGe6uXu_TEaJnlwFEWJoBf1w';
my $gurl = 'https://www.google.com/recaptcha/api/siteverify';


my $rc = Captcha::reCAPTCHA::V2->new;

print Dumper$rc;

my $result = $rc->verify($secret, param('g-recaptcha-response'));

print Dumper$result;

# Check the result
if( !$result->{success} ){
  # The check failed, ignore the POST
  next;
}


=e

my $cgi = CGI->new ($secret,$gurl);

print Dumper$cgi;

if (! check_captcha ($cgi)) {
    # OK
	print "HI\n";
}

sub check_captcha
{
    my ($icgi) = @_;
    my $ua = LWP::UserAgent->new ();
    my $response = $cgi->param ('g-recaptcha-response');
    my $remoteip = $ENV{REMOTE_ADDR};
    my $greply = $ua->post (
        $gurl,
        {
            remoteip => $remoteip,
            response => $response,
            secret => $secret,
        },
    );
    if ($greply->is_success ()) {
        my $json = $greply->decoded_content ();
        my $out = parse_json ($json);
        if ($out->{success}) {
            return undef;
        }
        return $json;
    }
    return "Connection to reCAPTCHA failed";
}