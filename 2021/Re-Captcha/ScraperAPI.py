import requests
from bs4 import BeautifulSoup
import time
import os, re, sys
from datetime import datetime, timedelta
# import pymssql
import ssl

from scraper_api import ScraperAPIClient

captcha_api_key = "18c788d08a948697708ea27a0207871f"
recaptcha_key ='6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf';

"""creating request session"""
headers = {
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp"
	",image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	"Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
	"Connection": "keep-alive",
	"Upgrade-Insecure-Requests": "1",
	"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36"
	" (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
	"Host": "publicaccess.carlisle.gov.uk",
}
s = requests.session()
s.headers.update(headers)

url='https://publicaccess.carlisle.gov.uk/online-applications/search.do?action=advanced'
response = s.get(url)
soup = BeautifulSoup(response.text, "html.parser")

# print ("soup:: ", soup)

csrf = soup.find('input', {'name': '_csrf'}).get('value')
print ("csrf:: ", csrf)

"""send google recaptcha sitekey to captcha solving services and retrieve the g_recaptcha_response"""

# captcha_url = 'https://2captcha.com/in.php?key='+captcha_api_key+'&method=userrecaptcha&googlekey='+recaptcha_key+'&pageurl='+url+'&json=1'
# request_id = requests.get(captcha_url).json().get("request")

# print ("request_id:: ", request_id)

# captcha_result_url = 'https://2captcha.com/res.php?key='+captcha_api_key+'&action=get&id='+request_id+'&json=1'
# timeout = 60
# time_taken = 0

# while time_taken <= timeout:
	# time.sleep(3)
	# time_taken = 3 + time_taken
	# g_recaptcha_response = requests.get(captcha_result_url).json().get("request")
	# print("g_recaptcha_response:: ", g_recaptcha_response)
	# if g_recaptcha_response != "CAPCHA_NOT_READY":
		# break


headers={
	"Content-Type": "application/x-www-form-urlencoded",
    "Origin": "https://publicaccess.carlisle.gov.uk",
	"Referer": "https://publicaccess.carlisle.gov.uk/online-applications/search.do?action=advanced",
}

# payload = "_csrf="+csrf+"&searchCriteria.reference=&searchCriteria.planningPortalReference=&searchCriteria.alternativeReference=&searchCriteria.description=&searchCriteria.applicantName=&searchCriteria.caseType=&searchCriteria.ward=&searchCriteria.parish=&searchCriteria.conservationArea=&searchCriteria.agent=&searchCriteria.caseStatus=&searchCriteria.caseDecision=&searchCriteria.appealStatus=&searchCriteria.appealDecision=&searchCriteria.developmentType=&caseAddressType=Application&searchCriteria.address=&date%28applicationReceivedStart%29=&date%28applicationReceivedEnd%29=&date%28applicationValidatedStart%29=16%2F02%2F2021&date%28applicationValidatedEnd%29=23%2F02%2F2021&date%28applicationCommitteeStart%29=&date%28applicationCommitteeEnd%29=&date%28applicationDecisionStart%29=&date%28applicationDecisionEnd%29=&date%28appealDecisionStart%29=&date%28appealDecisionEnd%29=&searchType=Application&recaptchaToken="+g_recaptcha_response;

payload = "_csrf="+csrf+"&searchCriteria.reference=&searchCriteria.planningPortalReference=&searchCriteria.alternativeReference=&searchCriteria.description=&searchCriteria.applicantName=&searchCriteria.caseType=&searchCriteria.ward=&searchCriteria.parish=&searchCriteria.conservationArea=&searchCriteria.agent=&searchCriteria.caseStatus=&searchCriteria.caseDecision=&searchCriteria.appealStatus=&searchCriteria.appealDecision=&searchCriteria.developmentType=&caseAddressType=Application&searchCriteria.address=&date%28applicationReceivedStart%29=&date%28applicationReceivedEnd%29=&date%28applicationValidatedStart%29=16%2F02%2F2021&date%28applicationValidatedEnd%29=23%2F02%2F2021&date%28applicationCommitteeStart%29=&date%28applicationCommitteeEnd%29=&date%28applicationDecisionStart%29=&date%28applicationDecisionEnd%29=&date%28appealDecisionStart%29=&date%28appealDecisionEnd%29=&searchType=Application";


#-----------------------------------------------------------------------------------
# client = ScraperAPIClient('6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf')
client = ScraperAPIClient('990c1b20fc276d1cef5e89c71b4b60e6')
print("client:: ",client)

# curl "http://api.scraperapi.com/account?api_key=990c1b20fc276d1cef5e89c71b4b60e6"

postResult = client.post(url = 'https://publicaccess.carlisle.gov.uk/online-applications/advancedSearchResults.do?action=firstPage', body=payload, headers=headers).text

print(str(postResult))
with open("postResult.html", "wb") as f:
	f.write(postResult)

# putResult = client.put(url = 'https://publicaccess.carlisle.gov.uk/online-applications/advancedSearchResults.do?action=firstPage', body=payload).text

# print(putResult)

# with open("putResult.html", "wb") as f:
	# f.write(str(putResult))