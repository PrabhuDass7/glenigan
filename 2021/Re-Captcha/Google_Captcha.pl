use Captcha::reCAPTCHA::V2;
use Captcha::reCAPTCHA::V3;

use Google::reCAPTCHA::v3;

use Data::Dumper;
use strict;
use WWW::Mechanize;
use URI::Escape;
use IO::Socket::SSL qw();
use HTTP::CookieMonster;
use URI::URL;
use HTTP::Response;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use Encode;
use URI::Encode qw(uri_encode uri_decode);
use LWP::Simple;


my $mech = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);


# $mech->add_header( 'Host' => 'publicaccess.carlisle.gov.uk');
# $mech->add_header( 'Referer' => 'https://publicaccess.carlisle.gov.uk/online-applications/');

# my $councilAppResponse = $mech->get("https://publicaccess.carlisle.gov.uk/online-applications/search.do?action=advanced");


# $mech-> submit_form
# (
    # form_number => 1,
    # fields      => 
    # {
		# 'date(applicationValidatedStart)'  => '01/03/2021',
        # 'date(applicationValidatedEnd)'  => '05/03/2021',
	# }
# );

# my $pingStatus = $mech->status;
# my $councilAppResponse = $mech->content;
# my $currentPageURL = $mech->uri();
# my $scriptStatus = $mech->status;

# print "response::".$mech->response()."\n";

# my $res=$mech->res();

# print "REG:: $res\n";

# open ss,">content_142.html";
# print ss $councilAppResponse;
# close ss;

# print "**********\n";

# my $councilAppResponse = $mech->get("https://publicaccess.carlisle.gov.uk/online-applications/search.do?action=advanced");
my $councilAppResponse = $mech->get("https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf&co=aHR0cHM6Ly9wdWJsaWNhY2Nlc3MuY2FybGlzbGUuZ292LnVrOjQ0Mw..&hl=en&v=4eHYAlZEVyrAlR9UNnRUmNcL&size=invisible&cb=po4jko2xxl5n");

my $pingStatus = $mech->status;
my $councilAppResponse = $mech->content;
my $currentPageURL = $mech->uri();
my $scriptStatus = $mech->status;

print "response::".$mech->response()."\n";

# my $res=$mech->res();

# print "REG:: $res\n";

open ss,">content_14222.html";
print ss $councilAppResponse;
close ss;

my $recaptcha=$1 if($councilAppResponse=~m/id\=\"recaptcha\-token\"\s*value\=\"\s*([^>]*?)\s*\"/is);
print "recaptcha:: $recaptcha\n";

#-------------------------------------------------- LWP -----------------------------------------------
=e
my $ua = LWP::UserAgent->new;
$ua->agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36" . $ua->agent);
$ua->ssl_opts('verify_hostname' => 0);
# $ua->max_redirect(0);
my $cookie_jar = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);                    
$ua->cookie_jar($cookie_jar);

my $url='https://publicaccess.carlisle.gov.uk/online-applications/search.do?action=advanced';

my %headers;
my ($content,$cookie)=&Get_Content($url,"GET","",\%headers,"Login_Page.html");

open ss,">content_LWP.html";
print ss $content;
close ss;
=cut

#--------------------------------------- Google::reCAPTCHA::v3 -----------------------------------------------------

print "\n\n\***** Google::reCAPTCHA::v3 *******\n\n\n";

# SiteKEY: 6Ld00HoaAAAAAAN6_5q2yLA7kdCcLFHWybeTgXYI
# secretKEY: 6Ld00HoaAAAAADYcqGe6uXu_TEaJnlwFEWJoBf1w

my $grc = Google::reCAPTCHA::v3->new(
        {
                -secret => '6Ld00HoaAAAAADYcqGe6uXu_TEaJnlwFEWJoBf1w',
				
        }
);
 
print Dumper$grc;

my $r = $grc->request(
        { 
                # -response => $councilAppResponse,
                # -response => '6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf',
                -response => '6Ld00HoaAAAAAAN6_5q2yLA7kdCcLFHWybeTgXYI',
                # -response => $recaptcha,
        }
);

print "Dumper:: ".Dumper$r;<STDIN>;


#--------------------------------------- Captcha::reCAPTCHA::V2-----------------------------------------------------

print "\n\n***** Captcha::reCAPTCHA::V2 *******\n\n\n";

# Create a new instance of Captcha::reCAPTCHA::V2
my $rc = Captcha::reCAPTCHA::V2->new;
 
print "Captcha::reCAPTCHA::V2 :: ".Dumper$rc ."\n"; 
 
# Get HTML code to display the reCAPTCHA
# my $rc_html = $rc->html('site key');
# my $response = $rc->html('6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf');
my $response = $rc->html('6Ld00HoaAAAAAAN6_5q2yLA7kdCcLFHWybeTgXYI');

# https://www.google.com/recaptcha/api.js?render=6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf

print Dumper$response; 
# print "response::$response\n";<STDIN>;

# my $result = $rc->verify('secret', $response);
my $result = $rc->verify('6Ld00HoaAAAAADYcqGe6uXu_TEaJnlwFEWJoBf1w', $response);

print "result:: ".Dumper$result;<STDIN>;


#------------------------------------- Captcha::reCAPTCHA::V3 ----------------------------------------------------

print "\n\n\n***** Captcha::reCAPTCHA::V3 *******\n\n\n";

# SiteKEY: 6Ld00HoaAAAAAAN6_5q2yLA7kdCcLFHWybeTgXYI
# secretKEY: 6Ld00HoaAAAAADYcqGe6uXu_TEaJnlwFEWJoBf1w

my $rc = Captcha::reCAPTCHA::V3->new(
    secret  => '6Ld00HoaAAAAADYcqGe6uXu_TEaJnlwFEWJoBf1w',
    sitekey => '6Ld00HoaAAAAAAN6_5q2yLA7kdCcLFHWybeTgXYI',
    # sitekey => '6Ldlw-EZAAAAAJ8hq4jPEJygp1yvNctlW3O71unf',
);
 

print Dumper$rc;<STDIN>;


# my $content = $rc->verify($councilAppResponse{'recaptcha-token'});
# my $content = $rc->verify($param{'reCAPTCHA_Token'});
my $content = $rc->verify($recaptcha);

print "content::".Dumper$content;

if( $content->{'success'} ){
   print "code for succeeding";
}else{
   print "code for failing";
}



=e
sub Get_Content($$$$)
{
	my $mainurl=shift;
	my $method=shift;
	my $parameter=shift;
	my $headers=shift;		
	my $FName=shift;		

	my %headers=%$headers;
	
	$mainurl=~s/\&amp\;/\&/igs;

	my $debug=0;
	my $code=0;
	my $cook;
	home:

	my $req=HTTP::Request->new($method=>"$mainurl");
	
	if($method eq 'POST')
	{		
		$req->content("$parameter");	
	}
	
	my $cook2;
	if($code=~m/30|50/is)
	{
		$req->header('cookie'=> $cook);
		$cook2=$cook;
		goto nxt;
	}
	
	foreach my $key(keys%headers)
	{
		if($headers{$key} ne '')
		{
			$req->header("$key"=> "$headers{$key}");		
			print "$key => $headers{$key}\n";
		}
	}
	
	nxt:
	
	my $res=$ua->request($req);
	$cook=$res->header('set-cookie');
	print "\ncook :: $cook\n";
	$code=$res->code;
	print "\nCODEsss :: $code\n";
	
	my $req_head1=$res->headers_as_string();
	print "req_head1 :$req_head1\n";

	my $File_name=$1 if($req_head1=~m/Content\-Disposition\:\s*attachment\;\s*filename\=\"([^>]*?)\"/is);
	
	if($code=~m/40/is)
	{
		my $con = $res->content;
		return $con;
	}
	elsif($code=~m/50/is)
	{
		$debug++;
		if($debug<=3)
		{
			goto home;
		}
	}
	elsif($code=~m/20/is)
	{
		my $con = $res->content;
		open ss,">$FName";
		print ss $con;
		close ss;
		$cook = $cook2 if($cook eq "");
		return ($con,$cook,$File_name);
	}
	elsif($code=~m/30/is)
	{
		my $location=$res->header('location');
		print "location :: $location\n";
		my $loc_url=URI::URL->new_abs($location,$mainurl);
		print "REDIRECTED_URL :: $loc_url\n";
		($mainurl,$method)=($loc_url,'GET');
		goto home;
	}
}

my %data=%$grc;

my @names = keys %data;
foreach my $key(@names)
{
	my $value=$data{$key};
	print "key:: $key \t value:: $value\n";<>;
		
	if ($value=~m/HASH/is)
	{
		my %vv=%$value;
		my @names2 = keys %vv;
		foreach my $key2(@names2)
		{
			my $values=$vv{$key2};
			print "key2:: $key2 \t value2:: $values\n";<>;
		}
	}
	
	if ($value=~m/ARRAY/is)
	{
		my @vv=@$value;
		foreach my $key2(@vv)
		{
			print "key2:: $key2 \n";<>;
		}
	}
}