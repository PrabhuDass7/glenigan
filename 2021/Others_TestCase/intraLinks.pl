#!/usr/bin/perl
use strict;
use warnings;
use WWW::Mechanize;
use URI::Escape;
use IO::Socket::SSL;
use Date::Calc ("Add_Delta_Days","Today","Gmtime","Date_to_Text","Today_and_Now");
use DateTime;
use Time::Piece;
use Time::HiRes qw(gettimeofday);
use DateTime::Duration;

my $timestamp = int (gettimeofday * 1000);
print STDOUT "timestamp = $timestamp\n";

my $dt = DateTime->now();
$dt->set_time_zone( 'Asia/Kolkata' );

print "$dt\n";

my ($year, $mon, $day) = Today();
my $today = [Today_and_Now()];



my $todayDate = sprintf "%04d%02d%02d", $year, $mon, $day;

my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);

print "$todayDate\n";
print "$todayDateTime\n";


my $month_abbr  = $dt->month_abbr; 
my $day_abbr    = $dt->day_abbr;
my $day    = $dt->day; 
my $year   = $dt->year; 
my $hms    = $dt->hms;

my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
		);
		
$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');

my $geturl = 'https://services.intralinks.com/login/';
$mech->get($geturl);
my $getcontents = $mech->content;

$mech->add_header( 'Host' => 'services.intralinks.com');
$mech->add_header( 'Accept' => 'application/json');
$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');

$mech->post('https://services.intralinks.com/services/logon', Content => "client=LG-GWT&httpStatus=F&check2fa=T&xml=%7B%22username%22%3A%22monitor%40hamiltonlane.com%22%2C%22password%22%3A%22monitor458%22%7D");				

my $loginCon = $mech->content;
my $code= $mech->status();
print "Status::$code\n";
open(PP, ">loginCon.html");
print PP "$loginCon";
close(PP);

my $csfToken = uri_escape($1) if($loginCon=~m/\"csfToken\"\:\"([^\"]*?)\"/is);


my $post = "csfToken=$csfToken&method=PUT&client=LG-GWT&httpStatus=F&userClient=MA-SC";

$mech->add_header( 'Host' => 'services.intralinks.com');
$mech->add_header( 'Accept' => 'application/json');
$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
$mech->post('https://services.intralinks.com/services/client', Content => $post);	

my $clientCon = $mech->content;
my $clientCode = $mech->status();
print "client Status::$clientCode\n";
open(PP, ">clientCon.html");
print PP "$clientCon";
close(PP);


$mech->get('https://services.intralinks.com/web/index.html?versionOfLogin=646280');
my $getcontent = $mech->content;

open(PP, ">getcontent.html");
print PP "$getcontent";
close(PP);



$mech->add_header( 'Host' => 'services.intralinks.com');
$mech->add_header( 'Referer' => 'https://services.intralinks.com/web/index.html?versionOfLogin=646280');
$mech->add_header( 'Accept' => 'application/json, text/javascript, */*; q=0.01');
$mech->add_header( 'Content-Type' => 'application/json');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
$mech->get("https://services.intralinks.com/web/appconfig/appconfig.json?_=$timestamp");
my $jsoncontent = $mech->content;



open(PP, ">jsoncontent.html");
print PP "$jsoncontent";
close(PP);




my $mainURL = 'https://services.intralinks.com/services/sessions?client=IL-NEXT&httpStatus=F&method=GET';
$mech->get($mainURL);
my $mainURLcontents = $mech->content;

open(PP, ">mainURLcontents.html");
print PP "$mainURLcontents";
close(PP);


my $csfToken1 = uri_escape($1) if($mainURLcontents=~m/\"csfToken\"\:\"([^\"]*?)\"/is);


my $mainURL11 = "https://services.intralinks.com/services/flags?client=IL-NEXT&httpStatus=F&csfToken=$csfToken1&method=GET";
$mech->get($mainURL11);
my $mainURLcontent11 = $mech->content;

open(PP, ">mainURLcontents.html");
print PP "$mainURLcontents";
close(PP);

$mech->add_header( 'Host' => 'services.intralinks.com');
$mech->add_header( 'Referer' => 'https://services.intralinks.com/web/index.html?');
$mech->add_header( 'Accept' => 'application/xml, text/xml, */*; q=0.01');
$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');



my $mainURL1 = "https://services.intralinks.com/services/exchanges?includeActions=T&client=IL-NEXT&httpStatus=F&csfToken=$csfToken1";
$mech->post("https://services.intralinks.com/services/exchanges?includeActions=T&client=IL-NEXT&httpStatus=F&csfToken=$csfToken", Content => 'xml=%3CsearchRequest%3E%3CentityType%3EWORKSPACEWORKINGSET%3C%2FentityType%3E%3Cterm%2F%3E%3CcheckWorkspacesFiltered%3EF%3C%2FcheckWorkspacesFiltered%3E%3CsearchFields%3E%3Cfield%3E%3Cname%3Eexclude_exchange_types%3C%2Fname%3E%3Cterm%3Ecourier%3C%2Fterm%3E%3Cterm%3Edelta%3C%2Fterm%3E%3C%2Ffield%3E%3C%2FsearchFields%3E%3Cfacets%2F%3E%3CsortOrder%3E%3CfieldName%3Ename%3C%2FfieldName%3E%3Cdirection%3EASCENDING%3C%2Fdirection%3E%3C%2FsortOrder%3E%3CdashboardView%3ET%3C%2FdashboardView%3E%3C%2FsearchRequest%3E');
my $featurecontent = $mech->content;

open(PP, ">featurecontent.html");
print PP "$featurecontent";
close(PP);