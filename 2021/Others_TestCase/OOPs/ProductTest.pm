package ProductTest;
 
use strict;
use warnings;
 
# init product with serial, name and price
sub new{
    my ($class,$args) = @_;
    my $self = bless { pid => $args->{pid},
					   name => $args->{name},
                       price => $args->{price}, 
                       date => $args->{date}
                       qty => $args->{qty}
                     }, $class;
}

# get product id of the product
sub get_pid{
   my $self = shift;
   return $self->{pid};
}

# get name of the product
sub get_name{
   my $self = shift;
   return $self->{name};
}

# get price of the product
sub get_price{
   my $self = shift;
   return $self->{price};
}

# get quantity(unit sold) of the product
sub get_qty{
   my $self = shift;
   return $self->{qty};
}
 
 
# get purchased date
sub get_date{
   my $self = shift;
   return $self->{serial};   
}

# return formatted string of the product
sub to_string{
   my $self = shift;
   return "ID: $self->{id}\nName: $self->{name}\nPrice: $self->{price}INR\n";
}
 
1;