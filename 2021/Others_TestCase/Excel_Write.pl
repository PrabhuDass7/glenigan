use strict;
use DateTime::Format::DateParse;
use Excel::Writer::XLSX;
use Date::Manip;
use MIME::Lite;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text");
use Cwd qw(abs_path);
use File::Basename;
use POSIX 'strftime';

my $basePath = dirname (dirname abs_path $0); 


my $date = $ARGV[0];
my $Report_Id = $ARGV[1];

my($year,$month,$day) = Add_Delta_Days( Today(), 0 ),"\n";
if (length($day) == 1) {$day = "0$day";}
if (length($month) == 1) {$month = "0$month";}
my $Date="$day/$month/$year";
my $Date_For_FileName="$day$month$year";
print "Date: $Date\n";

my $Excel_file_name="Daily_Doc_Download_Report_".$Date_For_FileName.".xlsx";
my $Storefile=$basePath.'/'.$Excel_file_name;
my $fileType='xlsx';
print "Storefile: $Storefile\n";

my $text = 'https://planningon-line.rushcliffe.gov.uk/online-applications/online-applications/online-applications/online-applications/online-applications/online-applications/pagedSearchResults.do?searchCriteria.page=1&action=page&orderBy=DateReceived&orderByDirection=Descending&searchCriteria.resultsPerPage=500';

my (@project_id_publish,@project_id,@Council_Code,@Format_ID,@Application,@Markup,@New_Docs_Count,@Existing_Doc_Count,@Downloaded_date);
my $datetime = localtime();
push(@project_id_publish,&Trim('654646464'));
push(@project_id,&Trim('65464646464'));
push(@Council_Code,&Trim('101'));
push(@Format_ID,&Trim('123654'));
push(@Application,&Trim('456/456'));
push(@Markup,&Trim('Large'));
push(@New_Docs_Count,&Trim('10'));
push(@Existing_Doc_Count,&Trim($text));
push(@Downloaded_date,&Trim($date));

my $OPUS_Status_Update_Query;

my @Headers=("Project_ID_Publish", "Project_ID", "Council_Code", "Format_ID", "Application", "Markup", "New_Docs_Count", "Existing_Doc_Count", "Downloaded_date");

my $workbook  = Excel::Writer::XLSX->new( $Storefile );
my $worksheet = $workbook->add_worksheet("Status");

my $format = $workbook->add_format(); # Add a format
$format->set_font('Cambria');
$format->set_size('11');

my $Text_format = $workbook->add_format(); # Add a format
$Text_format->set_font('Cambria');
$Text_format->set_size('11');

my $date_format = $workbook->add_format(num_format => 'dd-mmm-yy');
$date_format->set_font('Calibri');
$date_format->set_size('11');
my $date_format1 = $workbook->add_format(num_format => 'dd/mm/yyyy');
$date_format1->set_font('Calibri');
$date_format1->set_size('11');
my $date_format2 =  $workbook->add_format(num_format => 'mmm-yy', align => 'left');
$date_format2->set_font('Calibri');
$date_format2->set_size('11');


$worksheet->set_column( 0, 12, 25 );
my $Headerformat = $workbook->add_format(
	center_across => 1,
	bold          => 1,
	size          => 11,
	font          => 'Cambria',
	align         => 'vcenter',
);	
my $Row_Count = 0;
my $Column_Count = 0;
foreach my $Title(@Headers)
{
	$worksheet->write($Row_Count, $Column_Count, $Title, $Headerformat);
	$Column_Count++;
}	
$Row_Count++;

my %MonthHash = ('JAN'=> '01','FEB'=> '02','MAR'=>'03','APR'=>'04','MAY'=>'05','JUN'=>'06','JUL'=>'07','AUG'=>'08','SEP'=>'09','OCT'=>'10','NOV'=>'11','DEC'=>'12','JANUARY'=> '01','FEBRUARY'=> '02','MARCH'=> '03','APRIL'=> '04','MAY'=>'05','JUNE'=>'06','JULY'=>'07','AUGUST'=> '08','SEPTEMBER' => '09','OCTOBER'=> '10','NOVEMBER'=> '11','DECEMBER'=> '12');

my $CurrentYear = strftime "%Y", localtime;

for(my $i=0; $i<@Format_ID; $i++)
{
	$Column_Count=0;
	$worksheet->write(  $Row_Count, $Column_Count, $project_id_publish[$i], $format );
	$Column_Count++;
	$worksheet->write(  $Row_Count, $Column_Count, $project_id[$i], $format );
	$Column_Count++;
	$worksheet->write(  $Row_Count, $Column_Count, $Council_Code[$i], $format );
	$Column_Count++;
	$worksheet->write(  $Row_Count, $Column_Count, $Format_ID[$i], $format );
	$Column_Count++;
	$worksheet->write(  $Row_Count, $Column_Count, $Application[$i], $format );
	$Column_Count++;
	$worksheet->write(  $Row_Count, $Column_Count, $Markup[$i], $format );
	$Column_Count++;
	$worksheet->write(  $Row_Count, $Column_Count, $New_Docs_Count[$i], $format );
	$Column_Count++;
	$worksheet->write_string(  $Row_Count, $Column_Count, $Existing_Doc_Count[$i], $format );
	$Column_Count++;	
	
	if($Downloaded_date[$i]=~m/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)([\d]{2,4})\s*$/is) ######## 18 Feb 2017,18-Feb-2017,18 Feb 17,18-Feb-17 ########
	{
		my $day = $1;
		my $month = uc($2);
		my $keyyear = $3;
		
		if($day ne "")
		{
			my $monthNnumber  = $MonthHash{$month};
			my $CurrentYear_TwoDigit = substr($CurrentYear, 0, 2);
			my $year;
			if (length($keyyear)==2)
			{
				$year = $CurrentYear_TwoDigit.$keyyear;
			}
			else
			{
				$year = $keyyear;
			}
			my $dt = $year.'-'.$monthNnumber.'-'.$day;
			
			my $formattedDate = DateTime::Format::DateParse->parse_datetime( $dt );
						
			$worksheet->write_date_time(  $Row_Count, $Column_Count, $formattedDate , $date_format );
		}
		else
		{
			my %MonthHashs = ('JAN'=> '01','FEB'=> '02','MAR'=>'03','APR'=>'04','MAY'=>'05','JUN'=>'06','JUL'=>'07','AUG'=>'08','SEP'=>'09','OCT'=>'10','NOV'=>'11','DEC'=>'12');
			my $months = $MonthHashs{$month};
			my $CurrentYear_TwoDigit = substr($CurrentYear, 0, 2);
			my $year;
			if (length($keyyear)==2)
			{
				$year = $CurrentYear_TwoDigit.$keyyear;
			}
			else
			{
				$year = $keyyear;
			}
			my $date = $year.'-'.$months.'-01T00:00:00';
			
			$worksheet->write_date_time($Row_Count, $Column_Count, $date, $date_format2);
		}
	}
	elsif($Downloaded_date[$i]=~m/^\s*([\d]{0,2})\/([\d]{0,2})\/([\d]{0,4})\s*$/is) ######## 10/09/2015 ########
	{	
		my $Day = $1;
		my $Month = $2;
		my $Year = $3;
		my $dt = $Year.'-'.$Month.'-'.$Day;	
		
		if($Month<=12)
		{
			my $formattedDate = DateTime::Format::DateParse->parse_datetime( $dt );
			
			if($Report_Id==20)
			{
				$worksheet->write_date_time($Row_Count, $Column_Count, $formattedDate, $date_format1);
			}
			elsif($Report_Id==628)
			{
				$worksheet->write_date_time($Row_Count, $Column_Count, $formattedDate, $date_format);
			}
		}
	}
	
	my $formattedDate = DateTime::Format::DateParse->parse_datetime(  );
	
	$Row_Count++;
}
$workbook->close;

sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
