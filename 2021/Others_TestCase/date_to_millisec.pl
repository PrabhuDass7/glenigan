# !/usr/bin/perl
# use DateTime::Format::Strptime;

# my $strp = DateTime::Format::Strptime->new(
    # pattern => '%m-%d-%Y',
# );

# my $dt = $strp->parse_datetime("07-09-2019");

# print $dt->millisecond."\n";


use Time::Piece;
use warnings;
use 5.010;

my $dt = Time::Piece->strptime('07-09-2019:00.00', '%m-%d-%Y');
say $dt->epoch;

my $output = 1000 * $dt->epoch;

print "$output\n";