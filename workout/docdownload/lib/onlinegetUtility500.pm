package onlinegetUtility500;

use strict;
use Time::Piece;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use HTML::TableExtract;
use HTTP::Request;
use WWW::Mechanize;
use URI::Escape;
use Digest::MD5;
use URI::URL;
use Config::Tiny;
use IO::Handle;
use IO::File;
# use Net::SSL;
use FileHandle;
use File::Basename qw(dirname);
# use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use Cwd qw(abs_path);
use LWP::Simple;
use MIME::Base64;
use URI::Escape;
use URI::Encode;
use Encode;
# use HTTP::Headers;
use File::Path 'rmtree';
use File::stat;
require Exporter;

require LWP::UserAgent;
use Digest::MD5;
# use Net::SSL (); # From Crypt-SSLeay
BEGIN {
	use Net::SSL; # From Crypt-SSLeay
	$Net::HTTPS::SSL_SOCKET_CLASS = "Net::SSL"; # Force use of Net::SSL
	$ENV{HTTPS_PROXY} = 'http://172.27.137.192:3128'; 
	# $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128'; 
	use Net::SSLeay;
	$Net::SSLeay::trace = 2;
}

my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0});
# my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0, SSL_version => 'TLSv1:!TLSv11:!TLSv12:!SSLv2:!SSLv3'});
# $ua->agent("Mozilla/8.0");
$ua->agent("Mozilla/48.0");
$ua->timeout(10);


###########################################################
#Function : getMethodLWP()
#Desc  	  : LWP::UserAgent objects can be used to dispatch web requests and returns a HTTP::Response object and content etc.
#Date	  : 05-03-2019
###########################################################
sub getMethodLWP()
{
	my $documentURL=shift;

	my $req = HTTP::Request->new( GET => $documentURL);
	$req->header( 'Accept' => 'text/html' );

	# send request
	my $res = $ua->request($req);
	my ($content,$code);
	# check the outcome
	if ( $res->is_success ) {
		$content=$res->decoded_content;
		$code=$res->status_line;
		# print $res->decoded_content;
	} else {
		$code=$res->status_line;
		print "Error: " . $res->status_line . "\n";
		print "code: $code\n";
	}

	return ($content,$code);
}

sub Download()
{
	my $pdfURL 		= shift;
	my $pdfName 	= shift;
	my $documentURL	= shift;
	my $tempDownloadLocation= shift;
	my $MD5_LIST	= shift;
	my $Markup		= shift;
	my $lwpAgent	= shift;
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$tempDownloadLocation" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;
	
	if( grep (/$pdfName/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		
		my ($pdf_content, $code, $reurl, $ct)=&getMethodLWP($pdfURL);
		# if($ct=~m/application\/pdf/is)
		# {
			# $pdfName=~s/\.(pdf)\s*$/\.pdf/i;
		# }
		$file_name=$tempDownloadLocation.'\\'.$pdfName;
		
		# open(DC,">pdf_content.html");
		# print DC $pdf_content;
		# close DC;
		# exit;
		
		eval
		{
			if(lc($Markup) eq 'large')
			{
				$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
			}	
			
			if(grep( /^$MD5$/, @MD5_LIST )) 
			{
				print "Doc Exist\n";
				$MD5  = '';
				$code = 600;
			}
			else
			{
				print "Doc not Exist\n";
				print "file_name==>$file_name\n";
				
				my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $file_name for write :$!";
				binmode($fh);
				$fh->print($pdf_content);
				$fh->close();
				
				my $size = stat($file_name)->size;
                my $return_size_kb=sprintf("%.2f", $size / 1024);
                print "FileSizein_kb  :: $return_size_kb\n";
               
				if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
                {
                    unlink $file_name;
					$code=600;
                    $downCount++;
                    goto Re_Download;
                }				
			} 
		};
		
		if($@)
		{
			$Err=$@;
			print "DownloadError==>$Err\n";
			# <STDIN>;
			if($download_flag == 0)
			{
				$download_flag = 1;
				goto Re_Download;
			}	
		}
		
	}
	
	return($code,$Err,$MD5);
}

1