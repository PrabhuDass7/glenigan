package Download_Utility;

### Package Name ####
use strict;
use LWP::UserAgent;
use URI::URL;
use HTTP::Cookies;
use HTML::Entities;
use Cwd qw(abs_path);
use File::Basename;
use LWP::Simple;
use MIME::Base64;
use URI::Escape;
use WWW::Mechanize;
use IO::Socket::SSL qw();
# use Unicode::Escape;
use URI::Encode;
use Config::Tiny;
use Encode;
use File::stat;
require Exporter;
use HTML::TableExtract;
use Mozilla::CA;
use File::Path 'rmtree';
use File::Find;

my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);


# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# $basePath="D:\\Glenigan\\Document_download\\";

####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/ctrl');
my $logDirectory = ($basePath.'/logs');
####
# Create new object of class Config::Tiny
####
my ($councilDetailsFile1) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils and control properties for date range clculation repsectively
####


$councilDetailsFile1 = Config::Tiny->read($ConfigDirectory.'/Scraping_Details.ini');


#--------- LWP UserAgent declaration  ----------------#
my ($ua, $cookiefile, $cookie);

# $ua=LWP::UserAgent->new(show_progress=>1);
$ua = LWP::UserAgent->new(
        ssl_opts => {
                verify_hostname => 0,
				show_progress=>1,
                SSL_ca_file => Mozilla::CA::SSL_ca_file()
        }) or die;
		
$ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)");
$ua->max_redirect(0); 
$ua->cookie_jar({});

$cookiefile = $0;
$cookiefile =~ s/\.pl/_cookie\.txt/g;
$cookiefile =~ s/root/logs\/cookiefile/g;
$cookie = HTTP::Cookies->new(file=>"$cookiefile", autosave=>1);
$ua->cookie_jar($cookie);


sub trim()
{
	my $txt = shift;
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	$txt =~ s/\s\s+/ /g;
	if($txt!~m/<[^>]*?>/is)
	{
		$txt =~ s/\'//g;
	}	
	return $txt;
}

sub Clean()
{
	my $name=shift;
	$name=~s/<[^>]*?>/ /igs;
	$name=~s/\s+/ /igs;
	$name=~s/\n|\t/ /igs;
	$name=~s/\s\s+/ /igs;
	$name=~s/\'//igs;
	$name=~s/\&amp\;/\&/igs;
	$name=~s/&nbsp;/ /igs;	
	return $name;
}

sub PDF_Name()
{
	my $name=shift;
	$name=decode_entities($name);
	$name=~s/^\W+|\W+$//igs;
	$name=~s/\\n|\\t/ /igs;
	$name=~s/\s+/ /igs;
	$name=~s/\.\.+/\./igs;
	$name=~s/(\.pdf)+/\.pdf/igs;
	$name=~s/(\.docx)+/\.docx/igs;
	$name=~s/(\.doc)+/\.doc/igs;
	$name=~s/(\.xlsx)+/\.xlsx/igs;
	$name=~s/(\.xls)+/\.xls/igs;
	$name=~s/(\.rtf)+/\.rtf/igs;
	$name=~s/(\.jpg)+/\.jpg/igs;
	$name=~s/(\.jpeg)+/\.jpeg/igs;
	$name=~s/(\.png)+/\.png/igs;
	$name=~s/\./DotChar/igs;
	$name=~s/\W+/_/igs;
	$name=~s/\_\_+/_/igs;
	$name=~s/DotChar/\./igs;
	return $name;
}

sub MechDocpostDownload()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my $Doc_Post_URL	= shift;
	my $Doc_Post_Host	= shift;
	my $Content_Type	= shift;
	my $Referer	= shift;
	my $Proxy	= shift;
	my $mech	= shift;
	
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;

	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount = 1;		
		my ($pdf_content);
		Re_Download:		
		eval
		{
			$mech->post( $Doc_Post_URL, Content => "$pdf_url");
			my $con = $mech->content;
			$code = $mech->status;
			my $res = $mech->res();
			$pdf_content=$con
		};
		if($@)
		{
			$Err=$@;
			print "Error Message: $Err\n";
			if($downCount <= 2)
			{				
				$downCount++;
				goto Re_Download;
			}
		}
		$pdf_name=&PDF_Name($pdf_name);
		$file_name=$Temp_Download_Location.'\\'.$pdf_name;
		if($code == 200)
		{
			eval
			{
				# if(lc($Markup) eq 'large')
				# {
					$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				# }	
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "Doc Exist\n";
					$MD5='';
					$code=600;
				}				
				else
				{
					print "Doc not Exist\n";			
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();	
					
					my $size = stat($file_name)->size;
					# print "FileSizein_bytes  :: $size\n";
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";

					
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}
				}		
				
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download;
				}	
			}
		}
	}
	return($code,$Err,$MD5);
}

sub MechDocDownload()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my $mech			= shift;
	my $Council_Code	= shift;
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;

	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount = 1;		
		my ($pdf_content);
		Re_Download:		
		eval
		{
			if($Council_Code=~m/^342$/is)
			{
				$mech->get($Document_Url);
			    my $Content = $mech->content;
			
				my ($AppDoc,$ViewDoc,$AppDoc1,$ViewDoc1,$AppDoc2,$ViewDoc2);
						
				if($pdf_url=~m/<input[^>]*?name=\"([^\"]*?)\"\s*value=\"(View\s*document)\"\s*[^>]*?>/is)
				{
					$AppDoc=uri_escape($1);
					$ViewDoc=uri_escape($2);
				}	
					
				if($pdf_url=~m/value=\"View\s*document\"\s*[^>]*?>\s*<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
				{
					$AppDoc1=uri_escape($1);
					$ViewDoc1=uri_escape($2);
				}		
				$ViewDoc2=$ViewDoc;
				$AppDoc2=$AppDoc1;
				$ViewDoc2=~s/btnViewDocument$/ctl00/gsi;
							
				my ($hdnWindowWidth,$hdnWindowWidthval);
				my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				if($Content=~m/<input[^>]*?name=\"([^\"]*?hdnWindowWidth)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
				{
					$hdnWindowWidth = uri_escape($1);
					$hdnWindowWidthval = $2;			
				}
				
				my $Post_Content = "__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24ContentPlaceHolder%24ddlPageSize=99999&$AppDoc2=$ViewDoc2&$AppDoc=$ViewDoc&$AppDoc1=$ViewDoc1&$hdnWindowWidth=$hdnWindowWidthval";
				
				$Post_Content=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
				$Post_Content=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
				
				
				my($App_Page_Content,$Code,$filename) = &docPostMechMethod($Document_Url,$Post_Content,$mech);
				
				# my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
				
				# $pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
				
				$Post_Content=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
				$Post_Content=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
				
				my $halfURL = $1 if($App_Page_Content=~m/window\.open\(\'([^\']*?)\'\)\;\/\/\]\]>/is);
				
				my $Doc_URL="http://dctmviewer.salford.gov.uk".$halfURL;
				$pdf_url=$Doc_URL;
				print "Doc_URL==>$pdf_url\n";
				
			}
			elsif($Council_Code=~m/^158$/is)
			{
				$mech->get($Document_Url);
			    my $Content = $mech->content;
				
				my ($AppDoc,$ViewDoc,$val);
				if($pdf_url=~m/<input[^>]*?name=\"([^\"]*?)\"\s*[^>]*?>/is)
				{
					$AppDoc=uri_escape($1);
					$ViewDoc='on';
				}	
				
				my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $VIEWSTATEGENERATOR = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $PREVIOUSPAGE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__PREVIOUSPAGE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTTARGET = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTTARGET\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTARGUMENT = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTARGUMENT\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				
				my $dumName = $pdf_name;
				my $val = $1 if($dumName=~m/^(\d+)\_/is);
				$val=$val-1;
				
				my $postCnt = "__EVENTTARGET=<EVENTTARGET>&__EVENTARGUMENT=<EVENTARGUMENT>&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00_ContentPlaceHolder1_RadTabStrip1_ClientState=%7B%22selectedIndexes%22%3A%5B%222%22%5D%2C%22logEntries%22%3A%5B%5D%2C%22scrollState%22%3A%7B%7D%7D&$AppDoc=$ViewDoc&ctl00_ContentPlaceHolder1_DocumentsGrid_ClientState=%7B%22selectedIndexes%22%3A%5B%22$val%22%5D%2C%22selectedCellsIndexes%22%3A%5B%5D%2C%22unselectableItemsIndexes%22%3A%5B%5D%2C%22reorderedColumns%22%3A%5B%5D%2C%22expandedItems%22%3A%5B%5D%2C%22expandedGroupItems%22%3A%5B%5D%2C%22expandedFilterItems%22%3A%5B%5D%2C%22deletedItems%22%3A%5B%5D%2C%22hidedColumns%22%3A%5B%5D%2C%22showedColumns%22%3A%5B%5D%2C%22groupColsState%22%3A%7B%7D%2C%22hierarchyState%22%3A%7B%7D%2C%22popUpLocations%22%3A%7B%7D%2C%22draggedItemsIndexes%22%3A%5B%5D%2C%22lastSelectedItemIndex%22%3A$val%7D&ctl00%24ContentPlaceHolder1%24Button1=Download+Selected+Files&ctl00_ContentPlaceHolder1_RadMultiPage1_ClientState=%7B%22selectedIndex%22%3A2%2C%22changeLog%22%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24tbName=&ctl00%24ContentPlaceHolder1%24tbEmailAddress=";
				
				$postCnt=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
				$postCnt=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/gsi;
				$postCnt=~s/<PREVIOUSPAGE>/$PREVIOUSPAGE/gsi;
				$postCnt=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
				$postCnt=~s/<EVENTTARGET>/$EVENTTARGET/gsi;
				$postCnt=~s/<EVENTARGUMENT>/$EVENTARGUMENT/gsi;
				

				# print "Document_Url==>$Document_Url\n";
				
				my ($App_Page_Content,$Code,$filename) = &docPostMechMethod($Document_Url,$postCnt,$mech);
				
				if($filename ne "")
				{
					my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
					
					if(($fTpye ne "") && ($fTpye!~m/\d+/is) && ($fTpye!~m/aspx/is))
					{
						$pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
					}				
				}
								
				$pdf_content = $App_Page_Content;
				$code=$Code;
				
				$postCnt=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
				$postCnt=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
				$postCnt=~s/$VIEWSTATEGENERATOR/<VIEWSTATEGENERATOR>/gsi;
				$postCnt=~s/$PREVIOUSPAGE/<PREVIOUSPAGE>/gsi;
				$postCnt=~s/$EVENTTARGET/<EVENTTARGET>/gsi;
				$postCnt=~s/$EVENTARGUMENT/<EVENTARGUMENT>/gsi;
				# exit;
			
			}
			elsif($Council_Code=~m/^(439)$/is)
			{
				$mech->get($Document_Url);
			    my $Content = $mech->content;
			
				my ($AppDoc,$ViewDoc,$val);
				if($pdf_url=~m/<input[^>]*?name=\"([^\"]*?)\"\s*[^>]*?>/is)
				{
					$AppDoc=uri_escape($1);
					$ViewDoc='on';
				}	
				
				my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $VIEWSTATEGENERATOR = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $PREVIOUSPAGE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__PREVIOUSPAGE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTTARGET = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTTARGET\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTARGUMENT = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTARGUMENT\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				
				my $dumName = $pdf_name;
				my $val = $1 if($dumName=~m/^(\d+)\_/is);
				$val=$val-1;
				
				my $postCnt = "__EVENTTARGET=<EVENTTARGET>&__EVENTARGUMENT=<EVENTARGUMENT>&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00_ContentPlaceHolder1_RadTabStrip1_ClientState=%7B%22selectedIndexes%22%3A%5B%222%22%5D%2C%22logEntries%22%3A%5B%5D%2C%22scrollState%22%3A%7B%7D%7D&$AppDoc=$ViewDoc&ctl00_ContentPlaceHolder1_DocumentsGrid_ClientState=%7B%22selectedIndexes%22%3A%5B%22$val%22%5D%2C%22selectedCellsIndexes%22%3A%5B%5D%2C%22unselectableItemsIndexes%22%3A%5B%5D%2C%22reorderedColumns%22%3A%5B%5D%2C%22expandedItems%22%3A%5B%5D%2C%22expandedGroupItems%22%3A%5B%5D%2C%22expandedFilterItems%22%3A%5B%5D%2C%22deletedItems%22%3A%5B%5D%2C%22hidedColumns%22%3A%5B%5D%2C%22showedColumns%22%3A%5B%5D%2C%22groupColsState%22%3A%7B%7D%2C%22hierarchyState%22%3A%7B%7D%2C%22popUpLocations%22%3A%7B%7D%2C%22draggedItemsIndexes%22%3A%5B%5D%2C%22lastSelectedItemIndex%22%3A$val%7D&ctl00%24ContentPlaceHolder1%24Button1=Download+Selected+Files&ctl00_ContentPlaceHolder1_RadMultiPage1_ClientState=%7B%22selectedIndex%22%3A2%2C%22changeLog%22%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24tbName=&ctl00%24ContentPlaceHolder1%24tbEmailAddress=";
				
				$postCnt=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
				$postCnt=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/gsi;
				$postCnt=~s/<PREVIOUSPAGE>/$PREVIOUSPAGE/gsi;
				$postCnt=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
				$postCnt=~s/<EVENTTARGET>/$EVENTTARGET/gsi;
				$postCnt=~s/<EVENTARGUMENT>/$EVENTARGUMENT/gsi;
				
				
				# print "Document_Url==>$Document_Url\n";
				
				my ($App_Page_Content,$Code,$filename) = &docPostMechMethod($Document_Url,$postCnt,$mech);
				
				if($filename ne "")
				{
					my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
					
					if(($fTpye ne "") && ($fTpye!~m/\d+/is) && ($fTpye!~m/aspx/is))
					{
						$pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
					}				
				}
				
				$code=$Code;								
				$pdf_content = $App_Page_Content;
				
				$postCnt=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
				$postCnt=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
				$postCnt=~s/$VIEWSTATEGENERATOR/<VIEWSTATEGENERATOR>/gsi;
				$postCnt=~s/$PREVIOUSPAGE/<PREVIOUSPAGE>/gsi;
				$postCnt=~s/$EVENTTARGET/<EVENTTARGET>/gsi;
				$postCnt=~s/$EVENTARGUMENT/<EVENTARGUMENT>/gsi;
				# exit;
			
			}
			elsif($Council_Code=~m/^(425)$/is)
			{
				print "Document_Url==>$Document_Url\n";
				$mech->get($Document_Url);
			    my $Content = $mech->content;
				
				my ($AppDoc,$ViewDoc,$val);
				if($pdf_url=~m/<input[^>]*?name=\"([^\"]*?)\"\s*[^>]*?>/is)
				{
					$AppDoc=uri_escape($1);
					$ViewDoc='on';
				}	
				
				my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $VIEWSTATEGENERATOR = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $PREVIOUSPAGE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__PREVIOUSPAGE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTTARGET = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTTARGET\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTARGUMENT = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTARGUMENT\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				
				my $dumName = $pdf_name;
				my $val = $1 if($dumName=~m/^(\d+)\_/is);
				$val=$val-1;
				
				my $postCnt = "__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00_ContentPlaceHolder1_RadTabStrip1_ClientState=%7B%22selectedIndexes%22%3A%5B%222%22%5D%2C%22logEntries%22%3A%5B%5D%2C%22scrollState%22%3A%7B%7D%7D&$AppDoc=$ViewDoc&ctl00_ContentPlaceHolder1_RadGrid1_ClientState=%7B%22selectedIndexes%22%3A%5B%22$val%22%5D%2C%22reorderedColumns%22%3A%5B%5D%2C%22expandedItems%22%3A%5B%5D%2C%22expandedGroupItems%22%3A%5B%5D%2C%22expandedFilterItems%22%3A%5B%5D%2C%22deletedItems%22%3A%5B%5D%2C%22hidedColumns%22%3A%5B%5D%2C%22showedColumns%22%3A%5B%5D%2C%22popUpLocations%22%3A%7B%7D%2C%22draggedItemsIndexes%22%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24Button1=Download+Selected+Files&ctl00_ContentPlaceHolder1_RadMultiPage1_ClientState=%7B%22selectedIndex%22%3A2%2C%22changeLog%22%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24tbName=&ctl00%24ContentPlaceHolder1%24tbEmailAddress=";
				
				$postCnt=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
				$postCnt=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/gsi;
				$postCnt=~s/<PREVIOUSPAGE>/$PREVIOUSPAGE/gsi;
				$postCnt=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
				
				my ($App_Page_Content,$Code,$filename) = &docPostMechMethod($Document_Url,$postCnt,$mech);
				
				if($filename ne "")
				{
					my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
					if(($fTpye ne "") && ($fTpye!~m/\d+/is) && ($fTpye!~m/aspx/is))
					{
						$pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
					}
				}
				
				$code=$Code;								
				$pdf_content = $App_Page_Content;
				
				$postCnt=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
				$postCnt=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
				$postCnt=~s/$VIEWSTATEGENERATOR/<VIEWSTATEGENERATOR>/gsi;
				$postCnt=~s/$PREVIOUSPAGE/<PREVIOUSPAGE>/gsi;
				
			}
			elsif($Council_Code=~m/^(428)$/is)
			{
				# print "Document_Url==>$Document_Url\n";
				$mech->get($Document_Url);
			    my $Content = $mech->content;
		
				
				my ($AppDoc,$ViewDoc,$val);
				if($pdf_url=~m/<input[^>]*?name=\"([^\"]*?)\"\s*[^>]*?>/is)
				{
					$AppDoc=uri_escape($1);
					$ViewDoc='on';
				}	
				
				my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $VIEWSTATEGENERATOR = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $PREVIOUSPAGE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__PREVIOUSPAGE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTTARGET = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTTARGET\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				my $EVENTARGUMENT = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTARGUMENT\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
				
				my $dumName = $pdf_name;
				my $val = $1 if($dumName=~m/^(\d+)\_/is);
				$val=$val-1;
				
				my $postCnt = "__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__PREVIOUSPAGE=<PREVIOUSPAGE>&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00_ContentPlaceHolder1_RadTabStrip1_ClientState=%7B%22selectedIndexes%22%3A%5B%224%22%5D%2C%22logEntries%22%3A%5B%5D%2C%22scrollState%22%3A%7B%7D%7D&ctl00%24ContentPlaceHolder1%24cpeTerms_ClientState=true&ctl00%24ContentPlaceHolder1%24chkAcceptDisclaimer=on&$AppDoc=$ViewDoc&ctl00_ContentPlaceHolder1_RadGrid1_ClientState=%7B%22selectedIndexes%22%3A%5B%22$val%22%5D%2C%22selectedCellsIndexes%22%3A%5B%5D%2C%22unselectableItemsIndexes%22%3A%5B%5D%2C%22reorderedColumns%22%3A%5B%5D%2C%22expandedItems%22%3A%5B%5D%2C%22expandedGroupItems%22%3A%5B%5D%2C%22expandedFilterItems%22%3A%5B%5D%2C%22deletedItems%22%3A%5B%5D%2C%22hidedColumns%22%3A%5B%5D%2C%22showedColumns%22%3A%5B%5D%2C%22groupColsState%22%3A%7B%7D%2C%22hierarchyState%22%3A%7B%7D%2C%22popUpLocations%22%3A%7B%7D%2C%22draggedItemsIndexes%22%3A%5B%5D%2C%22lastSelectedItemIndex%22%3A$val%7D&ctl00%24ContentPlaceHolder1%24Button1=Download+Selected+Files&ctl00_ContentPlaceHolder1_gridLink_ClientState=&ctl00_ContentPlaceHolder1_RadMultiPage1_ClientState=%7B%22selectedIndex%22%3A4%2C%22changeLog%22%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24hfEmailAddress=&ctl00%24ContentPlaceHolder1%24hfName=";
				
				$postCnt=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
				$postCnt=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/gsi;
				$postCnt=~s/<PREVIOUSPAGE>/$PREVIOUSPAGE/gsi;
				$postCnt=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
								
				
				my ($App_Page_Content,$Code,$filename) = &docPostMechMethod($Document_Url,$postCnt,$mech);
				
				
				my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
				
				if(($fTpye ne "") && ($fTpye!~m/\d+/is) && ($fTpye!~m/aspx/is))
				{
					$pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
				}
				
				
				$code=$Code;								
				$pdf_content = $App_Page_Content;
				
				$postCnt=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
				$postCnt=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
				$postCnt=~s/$VIEWSTATEGENERATOR/<VIEWSTATEGENERATOR>/gsi;
				$postCnt=~s/$PREVIOUSPAGE/<PREVIOUSPAGE>/gsi;
				
			}
			
			if($pdf_content eq "")
			{
				eval{
					$mech->get($pdf_url);
				};
				$pdf_content = $mech->content;
				# open(DC,">Home_Content1.html");
				# print DC $pdf_content;
				# close DC;
				# if($Council_Code=~m/^(333)$/is)
				# {
					# $mech->add_header( Referer => "http://www.planningportal.rugby.gov.uk/images.asp" );
					# $mech->add_header( Host => "planningportal.rugby.gov.uk" );
					# $mech->get($pdf_url);
					# $pdf_content = $mech->content;
					# open(DC,">Home_Content2.html");
					# print DC $pdf_content;
					# close DC;
				# }
				my $res = $mech->res();	
				my $filename;
				if($res->is_success){
					$filename = $res->filename();
				}
				
				$code=$mech->status();	
				print "code:::$code\n\n";
			}
			
		};
		
		if($@)
		{
			$Err=$@;
			print "Error Message: $Err\n";
			if($downCount <= 2)
			{				
				$downCount++;
				goto Re_Download;
			}
		}
		
		if($Council_Code=~m/^(376|461)$/is)
		{
			my $tempCont = $pdf_content;
			if($tempCont=~m/<span[^>]*?>I\s*agree\s*to\s*this\s*statement\s*/is)
			{		
				my $viewState = uri_escape($1) if($tempCont=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATE"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
				my $viewStateGen = uri_escape($1) if($tempCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"__VIEWSTATEGENERATOR"\s[^>]*?value="([^>]*?)"\s*\/>/is);
				my $eventVal = uri_escape($1) if($tempCont=~m/<input\s*\s*type=\"hidden\"\s*name="__EVENTVALIDATION"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
				
				my $tempClickPost = "__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__EVENTVALIDATION=<EVENTVALIDATION>&q=Search+the+site...&chkCopyrightCfirm=on&Button1=Proceed+to+Document";
				my $clickPost=$tempClickPost;
				$clickPost=~s/<VIEWSTATE>/$viewState/gsi;
				$clickPost=~s/<VIEWSTATEGENERATOR>/$viewStateGen/gsi;
				$clickPost=~s/<EVENTVALIDATION>/$eventVal/gsi;
				
				my ($docPageContent,$Code,$filename) = &docPostMechMethod($pdf_url,$clickPost,$mech);	
				
				# my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
				
				# $pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
				
				
				$code=$Code;
				$pdf_content = $docPageContent;			
				
			}
			elsif($tempCont=~m/<strong>\s*I\s*agree\s*to\s*this\s*statement/is)
			{								
				my $Description = uri_escape($1) if($tempCont=~m/<input[^>]*?name=\"Description\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
				my $ApplicationReferenceNumber = uri_escape($1) if($tempCont=~m/<input[^>]*?name=\"ApplicationReferenceNumber\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
				my $ref = uri_escape($1) if($tempCont=~m/<input[^>]*?name=\"Ref\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
				
				my $postCnt = "Description=$Description&ApplicationReferenceNumber=$ApplicationReferenceNumber&Ref=$ref&accept=on&proceed=Proceed+to+Document";
						
				my ($docPageContent,$Code,$filename) = &docPostMechMethod('https://services.gov.im/planningapplication/services/planning/copyright.iom',$postCnt,$mech);	
				
				if($filename ne "")
				{
					my $fTpye = $1 if($filename=~m/\.([\w]{3,4})\s*$/is);
					
					if($fTpye ne "")
					{
						$pdf_name=~s/\.([\w]{3,4})\s*$/\.$fTpye/gsi;
					}			
				}
				
				$code=$Code;
				$pdf_content = $docPageContent;
			}
			else
			{
				$pdf_content;
			}
		}
		elsif($Council_Code=~m/^120$/is)
		{
			
			my $Doc_Post_Host	= 'planning.bournemouth.gov.uk';
			my $Content_Type	= 'application/x-www-form-urlencoded';
			
			(my $Content,$code) =&Getcontent($Document_Url,$Document_Url,$Doc_Post_Host,$Content_Type,'','','');
			
			my $rc=$1 if($pdf_url=~m/<input[^>]*?id\=\"MainContent\_gridDocuments\_hidFullFileName\_([^\"]*?)\"/is);
			
			my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
			my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
			my $VIEWSTATEGENERATOR = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
			my $PREVIOUSPAGE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__PREVIOUSPAGE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
			my $EVENTTARGET = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTTARGET\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
			my $EVENTARGUMENT = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTARGUMENT\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
			
			my $postCnt='__EVENTTARGET=ctl00%24MainContent%24gridDocuments&__EVENTARGUMENT=rc'.$rc.'&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00_MainContent_RadTabStrip1_ClientState=%7B%22selectedIndexes%22%3A%5B%222%22%5D%2C%22logEntries%22%3A%5B%5D%2C%22scrollState%22%3A%7B%7D%7D&ctl00_MainContent_RadMultiPage1_ClientState=';
			
			$postCnt=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
			$postCnt=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/gsi;
			$postCnt=~s/<PREVIOUSPAGE>/$PREVIOUSPAGE/gsi;
			$postCnt=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
			$postCnt=~s/<EVENTTARGET>/$EVENTTARGET/gsi;
			$postCnt=~s/<EVENTARGUMENT>/$EVENTARGUMENT/gsi;
			
			($code,$Err,$MD5)=&Download_Post($postCnt,$pdf_name,$Document_Url,$Temp_Download_Location,$MD5_LIST,$Markup,$Document_Url,$Doc_Post_Host,$Content_Type,$Document_Url,"N");
		
			return($code,$Err,$MD5);
		}
		
		
		
		$pdf_name=&PDF_Name($pdf_name);
		$file_name=$Temp_Download_Location.'\\'.$pdf_name;
		if($code == 200)
		{
			eval
			{
				# if(lc($Markup) eq 'large')
				# {
					$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				# }	
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "Doc Exist\n";
					$MD5='';
					$code=600;
				}				
				else
				{
					print "Doc not Exist\n";			
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();	
					
					my $size = stat($file_name)->size;
					# print "FileSizein_bytes  :: $size\n";
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";

					
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}
				}		
				
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download;
				}	
			}
		}
		
	}
	
	return($code,$Err,$MD5);
}

sub Download()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;
	
	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		(my $pdf_content,$code,my $rurl) = &Getcontent($pdf_url,"","","",$Document_Url);
		
		if($pdf_content=~m/<form action=\"(\/Disclaimer\/Accept[^\"]*?)\"\s*method=\"post\">[\w\W]*?<input[^>]*?value=\"Accept\"/is)
		{
			my $acceptURL = $1;
			$acceptURL=~s/\&amp\;/\&/gsi;
			my $u1=URI::URL->new($acceptURL,$Document_Url);
			$pdf_url=$u1->abs;
			
			print "pdf_url==>$pdf_url\n";
			
			($pdf_content,$code,$rurl) =&Getcontent($pdf_url,"","","",$Document_Url);
			
		}
		
		$file_name=$Temp_Download_Location.'\\'.$pdf_name;
		
		if($code == 200)
		{
			eval{
				# if(lc($Markup) eq 'large')
				# {
					$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				# }	
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "Doc Exist*****\n";
					$MD5  = '';
					$code = 600;

				}
				else
				{
					print "Doc not Exist!!!!!\n";
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();
					
					my $size = stat($file_name)->size;
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";
				   
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}				
				}
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download;
				}	
			}
		}
	}
	return($code,$Err,$MD5);
}




###
# Subroutine to set the constructor for WWW::Mechanize->new()
###

sub mech_UserAgent()
{
    my($currentCouncilCode) = @_;
	my $mech;

    if($councilDetailsFile1->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
    {
        $mech = WWW::Mechanize->new(autocheck => 0);
    }
    else
    {	
		print "###############################\n";
        # $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$mech = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);
    }
    return($mech);
}



# Getting content by Mechanize Module
# arguments: url
# returns: content
sub docMechMethod() 
{
    my $URL = shift;
    my $mech = shift;   

	$URL=~s/amp\;//igs;
	
	print "URL==>$URL\n";
	
	# if($URL=~m/westberks/is)
	# {
		# $mech->proxy(['http','https'], 'http://172.27.140.48:3128');
	# }
	# else
	# {
		# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');
	# }
	
	# BEGIN {
	 # $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
	 # $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
	 # $ENV{HTTPS_DEBUG} = 1;  #Add debug output
	# }

	my $proxyFlag=0;
	rePingwith_199:
	$mech->get($URL);
	my $con = $mech->content;
    my $code = $mech->status;
	print "Code: $code\n";
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
		goto rePingwith_199;
	}
    return($con,$code);
}


sub Getcontent()
{
	my $Document_Url=shift;
	my $URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Proxy=shift;
	my $Need_Host=shift;
	my $Accept_Language=shift;
	
	if($Proxy eq 'Y')
	{
		print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	}
	elsif($Proxy eq '192')
	{
		print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	}
	
	my $rerun_count=0;
	my $redir_url;
	if($Document_Url=~m/^\s*$/is)
	{
		return ("Url Empty","Url Empty");		
	}
	$Document_Url =~ s/^\s+|\s+$//g;
	$Document_Url =~ s/amp;//igs;
	
	print "Document_Url==>$Document_Url\n";
	
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(GET=>$Document_Url);
	if($Need_Host eq 'Y')
	{
		if(($Host ne 'N/A') and ($Host ne ''))
		{
			$req->header("Host"=> "$Host");
		}	
	}	
	if(($Content_Type ne 'N/A') and ($Content_Type ne ''))
	{
		$req->header("Content-Type"=> "$Content_Type");
	}	
	if(($Referer ne 'N/A') and ($Referer ne ''))
	{
		$req->header("Referer"=> "$Referer");
	}	
	if(($Accept_Language ne 'N/A') and ($Accept_Language ne ''))
	{
		$req->header("Accept-Language"=> "en-US,en;q=0.5");
	}	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	# print "status_line:: $status_line\n";<>;
	print "CODE 1:: $code :: $Proxy\n";
	
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		print "CODE 2:: $code :: $Proxy\n";
	}
	elsif($code=~m/30/is)
	{
		print "CODE 3:: $code :: $Proxy\n";
		my $loc=$res->header("location");
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			print "Document_Url==>$Document_Url\n";
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$Document_Url);
				my $u2=$u1->abs;
				if($Document_Url=~m/rugby\.gov\.uk/is)
				{
					$Document_Url=$Document_Url;					
				}
				else
				{
					$Document_Url=$u2;
				}
				$redir_url=$u2;
			}
			else
			{
				$Document_Url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	elsif($Proxy eq 'Y')
	{
		print "CODE 4:: $code :: $Proxy\n";
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			$proxyFlag=1;
			print "via [[192]]...\n";
			$ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		print "CODE 5:: $code :: $Proxy\n";
		if ( $rerun_count < 1 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# print "redir_url==>$redir_url\n";<STDIN>;
	return ($content,$code,$redir_url);
}

sub Post_Method()
{
	my $Doc_Post_URL=shift;
	my $Host=shift;
	my $Content_Type=shift;
	my $Referer=shift;
	my $Post_Content=shift;
	my $Proxy=shift;
	my $rerun_count=0;

	# if($Proxy eq 'Y')
	# {
		# print "via 199...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.199:3128');
	# }
	# elsif($Proxy eq '192')
	# {
		# print "via 192...\n";
		# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
	# }
	
	$Doc_Post_URL =~ s/^\s+|\s+$//g;
	$Doc_Post_URL =~ s/amp;//igs;
	my $proxyFlag=0;
	Home:
	my $req = HTTP::Request->new(POST=>$Doc_Post_URL);
	$req->header("Host"=> "$Host");
	# $req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Content-Type"=>"$Content_Type"); 
	$req->header("Referer"=> "$Referer");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=> "en-us,en;q=0.5");
	$req->content($Post_Content);
	my $res = $ua->request($req);
	# my %res=%$res;
	# my $headers=$res{'_headers'};
	# my %headers=%$headers;

	# for my $k(keys %headers)
	# {
		# print "[$k	:$headers{$k}]\n";
	# }	
	# my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	# print "set-cookie:: $Cookie\n";	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$Doc_Post_URL);
			my $u2=$u1->abs;
			my $Redir_url;
			($content,$Redir_url)=&Getcontent($u2,$Doc_Post_URL,$Host,$Content_Type,$Referer,$Proxy);
		}
	}
	elsif($Proxy eq 'Y')
	{
		if(($code=~m/^50/is) && ($proxyFlag==0))
		{
			print "via 192...\n";
			$proxyFlag=1;
			# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
			goto Home;
		}
	}
	else
	{
		if ($rerun_count < 1)
		{
			$rerun_count++;
			# sleep 1;
			goto Home;
		}
	}
	return ($content,$code);
}

sub Move_Files()
{
	my $Download_Location = shift;
	my $Temp_pdf_store_path = shift;
	my $Current_Location = shift;
	my $Format_ID = shift;
	# print "Download_Location:	$Download_Location\n";
	# print "Temp_pdf_store_path:	$Temp_pdf_store_path\n";
	# print "Current_Location:	$Current_Location\n";

	opendir(my $dh, "$Temp_pdf_store_path") || warn "can't opendir $Temp_pdf_store_path: $!";
	# my @File_Names = readdir($dh);
	my @File_Names = grep {$_ ne '.' and $_ ne '..'} readdir $dh;
	closedir $dh;
	
	my $cnt=@File_Names;
	my $Folder_Size;
	find(sub { $Folder_Size += -s if -f }, $Temp_pdf_store_path);
	
	# print "Folder_Size:: $Folder_Size\n";<>;
	my $Comments;
	my $Spd_date=localtime();
	if(($cnt==0) or ($Folder_Size eq ""))
	{
		print "Folder Empty\n";
		# system("rmdir $Temp_pdf_store_path");
		rmtree($Temp_pdf_store_path);
		$Comments='No Documents found. Kindly check the script';
	}
	else
	{
		# my $Download_Location = "$Download_Location$Format_ID";
		my $Download_Location_2 = "\\\\172.27.137.202\\One_app_download\\$Format_ID";
		my $Temp_pdf_store_path = "$Temp_pdf_store_path";
		eval{
			# $Download_Location=~s/\//\\/igs;
			$Download_Location_2=~s/\//\\/igs;
			$Temp_pdf_store_path=~s/\//\\/igs;
			opendir(my $dhf, $Temp_pdf_store_path) || die "can't opendir $Temp_pdf_store_path: $!";
			my @File_Count = grep {$_ ne '.' and $_ ne '..'} readdir $dhf;
			closedir $dhf;
			my $File_Count=@File_Count;
			print "$File_Count\n";			
			if($File_Count > 0)
			{
				# system("mkdir $Download_Location");
				system("mkdir $Download_Location_2");
				print "File Moving to $Download_Location_2\n";
				my $Copy_Status = system("copy $Temp_pdf_store_path $Download_Location_2");
				if($Copy_Status == 0)
				{
					print "Moved\n";
					if ($Temp_pdf_store_path ne $Current_Location)
					{
						opendir(my $dhf, $Temp_pdf_store_path) || die "can't opendir $Temp_pdf_store_path: $!";
						my @File_Names=readdir($dhf);
						closedir $dhf;
						for(@File_Names)
						{
							if($_!~m/\.pl|\.pm|\.py|\.ini/is)
							{
								my $f="$Temp_pdf_store_path/$_";
								print "File: $f\n";
								unlink($f);
							}
						}	
						# system("rmdir $Temp_pdf_store_path");
						rmtree($Temp_pdf_store_path);
					}	
				}
				else
				{
					print "Failed To Copy\n";
				}			
			}	
		};
		# print "File Moving to $Download_Location\n";
		# my $Copy_Status = system("copy $Temp_pdf_store_path $Download_Location");
	}
}


sub docPostMechMethod() 
{
    my $URL = shift;
    my $postcontent = shift;
    my $mech = shift;
   
	# print "$postcontent\n"; <STDIN>;

	$URL=~s/amp\;//igs;
	
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
	
	
	BEGIN {
	 $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
	 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
	 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
	}
	
	my $proxyFlag=0;
	rePingwith_192:
	$mech->post( $URL, Content => "$postcontent");
	  
	my $con = $mech->content;
    my $code = $mech->status;
	my $res = $mech->res();
	if(($code=~m/^50/is) && ($proxyFlag==0))
	{
		$proxyFlag=1;
		$mech->proxy(['http','https'], 'http://172.27.137.192:3128');
		goto rePingwith_192;
	}
	
	my $filename;
	if($res->is_success){
		$filename = $res->filename();
	}
	$filename=~s/\s+/_/igs;
    return($con,$code,$filename);
}

sub Url_Status()
{
	my $Responce_Code=shift;
	my $Responce_Status;
	if($Responce_Code == 200)
	{
		$Responce_Status = 'Y';
	}
	else
	{
		$Responce_Status = 'N';
	}
	return $Responce_Status;
}


sub Download_Post()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	my $Doc_Post_URL	= shift;
	my $Doc_Post_Host	= shift;
	my $Content_Type	= shift;
	my $Referer			= shift;
	my $Proxy			= shift;

	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$code,$MD5);
	my $download_flag=0;
	
	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		(my $pdf_content,$code)=&Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Referer,$pdf_url,$Proxy);
		
		$file_name=$Temp_Download_Location.'\\'.$pdf_name;
		if($code == 200)
		{
			eval{
				# if(lc($Markup) eq 'large')
				# {
					$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
				# }	
				if(grep( /^$MD5$/, @MD5_LIST )) 
				{
					print "Doc Exist\n";
					$MD5='';
					$code=600;
				}
				else
				{
					print "Doc not Exist\n";
					my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
					binmode($fh);
					$fh->print($pdf_content);
					$fh->close();
					
					my $size = stat($file_name)->size;
					my $return_size_kb=sprintf("%.2f", $size / 1024);
					print "FileSizein_kb  :: $return_size_kb\n";			   
				   
					if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
					{
						unlink $file_name;
						$code=600;
						$downCount++;
						goto Re_Download;
					}
				}
			};
			if($@)
			{
				$Err=$@;
				if($download_flag == 0)
				{
					$download_flag = 1;
					goto Re_Download;
				}	
			}
		}
	}
	
	return($code,$Err,$MD5);
}



sub Doc_Download_Html_Header()
{

	my $councilDetailsFile=shift;
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $Doc_URL_BY=shift;
	my $Council_Code=shift;
	my $pdfcount_inc=shift;
	
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	
	$Content=~s/<th\/>/<th><\/th>/igs;
	$Content=~s/<td\/>/<td><\/td>/igs;

	if($councilDetailsFile->{$Council_Code}->{'Block_Regex1'} ne 'N/A')
	{
		
		my $data = $1 if($Content=~m/($councilDetailsFile->{$Council_Code}->{'Block_Regex1'})/is);
				
		chomp $data;
		
		$data=~s/(<input[^>]*?value=\"([^\"]*?)\"[^>]*?\/?>)\s*<\/th>/$2<\/th>/gsi;
		my @check_column;
		
		while ($data=~m/(<th[^<]*?>[\w\W]*?<\/th>)/igs)
		{
			my $y=$1;
			push(@check_column,$y);
		}
		my $check_column_size=@check_column;
		if ($check_column_size < 4)
		{
			
			while($check_column_size - 4)
			{
				
				$data=~s/(<table[^<]*?>[\w\W]*?)<\/tr>/$1 <th><\/th> <\/tr>/igs;
				
				$check_column_size=$check_column_size+1;
					
			}
		}
	
	
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'}='';
		}
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}='';
		}
		
		
		my $te = HTML::TableExtract->new(keep_html => 1,headers =>[$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}]);
		
		
		$te->parse( $data );		
		
		my ($Doc_URL,$Doc_Desc,$document_type,$Published_Date,$pdf_name,$Post_Responce,$res_code,$Root_Url);
				
		if($Redir_Url ne '')
		{
			$Root_Url=$Redir_Url;
		}
		elsif($Document_Url ne '')
		{
			$Root_Url=$Document_Url;
		}
		elsif($URL ne '')
		{
			$Root_Url=$URL;
		}
		
		
		foreach my $ts ( $te->tables() )
		{
			
			foreach my $row ( $ts->rows() )
			{
				
				my $link="@$row[3]"; 
				# print "Link===>$link\n";<STDIN>;
				if($Doc_URL_BY eq 'POST')
				{
					my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$Doc_POST_ID2,$CSRFToken);
					my $Doc_Post_Content_Tmp=$councilDetailsFile->{$Council_Code}->{'Doc_Post_Content'};
					
					if($councilDetailsFile->{$Council_Code}->{'Viewstate_Regex'} ne 'N/A')
					{
						$Doc_viewstate=uri_escape($1) if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Viewstate_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Viewstategenerator_Regex'} ne 'N/A')
					{
						$Doc_viewstategenerator=$1 if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Viewstategenerator_Regex'}/is);
					}	
					
					if($councilDetailsFile->{$Council_Code}->{'CSRFToken_Regex'} ne 'N/A')
					{
						$CSRFToken=uri_escape($1) if($Content=~m/$councilDetailsFile->{$Council_Code}->{'CSRFToken_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Eventvalidation_Regex'} ne 'N/A')
					{
						$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Eventvalidation_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Doc_Page_Num_Regex'} ne 'N/A')
					{
						$Doc_Page_Num=$1 if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Page_Num_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Doc_NumberOfRows_Regex'} ne 'N/A')
					{
						$Doc_NumberOfRows=$1 if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_NumberOfRows_Regex'}/is);
					}	
					if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_POST_ID1_Regex'}/is)
					{
						$Doc_POST_ID1=uri_escape($1);
						$Doc_POST_ID2=uri_escape($2);
						
					}

					
					$councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'}  = $Document_Url if($councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'}=~m/Document_Url/is);
					$councilDetailsFile->{$Council_Code}->{'Referer'}  = $Document_Url if($councilDetailsFile->{$Council_Code}->{'Referer'}=~m/Document_Url/is);
					$councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'}=decode_entities($councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'});
					
					$councilDetailsFile->{$Council_Code}->{'Referer'}=decode_entities($councilDetailsFile->{$Council_Code}->{'Referer'});
					$Doc_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
					$Doc_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
					$Doc_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
					$Doc_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
					$Doc_Post_Content_Tmp=~s/<PageNum>/$Doc_Page_Num/igs;
					$Doc_Post_Content_Tmp=~s/<PageRows>/$Doc_NumberOfRows/igs;
					$Doc_Post_Content_Tmp=~s/<SelectedID1>/$Doc_POST_ID1/igs;
					$Doc_Post_Content_Tmp=~s/<SelectedID2>/$Doc_POST_ID2/igs;
					
					
					
					$councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'}=$councilDetailsFile->{$Council_Code}->{'Host'} if($councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'} eq '' or $councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'} eq 'N/A');
					
					print "#####################################\n";
					if($Council_Code eq 187)
					{
						if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
						{	
							my $Durl=$1;
							
							if(($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne 'N/A') and ($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne '')) 
							{
								
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Durl;
								
						
								
							}
							else
							{
								my $u1=URI::URL->new($1,$Root_Url);
								my $u2=$u1->abs;
								$Doc_URL=$u2;
						
								
							}	
						}
					}
					else
					{
						($Post_Responce,$res_code)=&Post_Method($councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'},$councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'},$councilDetailsFile->{$Council_Code}->{'Content_Type'} ,$councilDetailsFile->{$Council_Code}->{'Referer'},$Doc_Post_Content_Tmp,$councilDetailsFile->{$Council_Code}->{'Proxy'});
					
					
						# open(DC,">187.html");
						# print DC "$Post_Responce";
						# close DC;
						# <STDIN>;					
					
						
						my $URL_ID;
						if($Post_Responce=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
						{
							my $val=$1;
							if($councilDetailsFile->{$Council_Code}->{'DOC_PDF_ID_URL'}=~m/ID/is)
							{
								$URL_ID=$val;
							}
							elsif($councilDetailsFile->{$Council_Code}->{'DOC_PDF_ID_URL'}=~m/URL/is)
							{
								my $u1=URI::URL->new($val,$councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'});
								my $u2=$u1->abs;
								$Doc_URL=$u2;
							}
						}
						if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A')
						{
							# next if($PDF_URL_Cont == '');
							$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
							$Doc_URL=~s/<SelectedID>/$URL_ID/igs;
						
						}
					}
					$Doc_URL=~s/amp\;//igs;
				}
				elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'} ne 'N/A')
				{
					
					if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
					{	
						my $Durl=$1;
						
						if(($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne 'N/A') and ($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne '')) 
						{
							
							$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Durl;
							
					
							
						}
						else
						{
							my $u1=URI::URL->new($1,$Root_Url);
							my $u2=$u1->abs;
							$Doc_URL=$u2;
					
							
						}	
					}
					if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_redirct'} eq 'Y')
					{
						my ($Redir_Content, $code, $reurl)=&Getcontent($Doc_URL,"","","","",$councilDetailsFile->{$Council_Code}->{'Proxy'},$councilDetailsFile->{$Council_Code}->{'Need_Host'});
						if($Redir_Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_redirct_Regex'}/is)
						{
							my $Durl=$1;
							if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_redirct_ABS'} ne 'N/A')
							{
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_redirct_ABS'}.$Durl;
								
							}
							else
							{
								my $u1=URI::URL->new($Durl,$Doc_URL);
								my $u2=$u1->abs;
								$Doc_URL=$u2;
								
							}	
						}
					}	
				}
				elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A')
				{
					next if($link == '');
					
					$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
					$Doc_URL=~s/<SelectedID>/$link/igs;
					
				}
				
				$Doc_Desc = &Clean(@$row[2]);
				$document_type = &Clean(@$row[1]);
				$Published_Date = &Clean(@$row[0]);
				print "\nDoc_Desc::$Doc_Desc";
				print "\ndocument_type::$document_type";
				print "\nPublished_Date::$Published_Date";
				print "\nLink::$link";
				print "\nDoc_URL::$Doc_URL\n";

			
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				$pdfcount_inc++;
				
				$pdf_name = &PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
			
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
				}
				
			}
		}

	}
	
	return \%Doc_Details,$pdfcount_inc;

}


sub Format1_Extented_Html_Header()
{
	my $councilDetailsFile=shift;
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $Doc_URL_BY=shift;
	my $Council_Code=shift;
	my $pdfcount_inc=shift;
	
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	$Content=~s/<th\/>/<th><\/th>/igs;
	$Content=~s/<td\/>/<td><\/td>/igs;
	
	
	if($councilDetailsFile->{$Council_Code}->{'Block_Regex1'} ne 'N/A')
	{
		
		my $data = $1 if($Content=~m/($councilDetailsFile->{$Council_Code}->{'Block_Regex1'})/is);
		
		chomp $data;
		$data=~s/(<input[^>]*?value=\"([^\"]*?)\"[^>]*?\/?>)\s*<\/th>/$2<\/th>/gsi;
		
		my @check_column;
		
		while ($data=~m/(<th[^<]*?>[\w\W]*?<\/th>)/igs)
		{
			my $y=$1;
			push(@check_column,$y);
		}
		my $check_column_size=@check_column;
		if ($check_column_size < 4)
		{
			while($check_column_size - 4)
			{
				$data=~s/(<table[^<]*?>[\w\W]*?)<\/tr>/$1 <th> <\/th> <\/tr>/igs;
				$check_column_size=$check_column_size+1;
					
			}
		}
		
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'}='';
		}
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}='';
		}
		
		
		
		my $te = HTML::TableExtract->new(keep_html => 1,headers =>[$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}]);
		
		$te->parse( $data );			
		
		my ($Doc_URL,$Doc_Desc,$document_type,$Published_Date,$pdf_name,$Post_Responce,$res_code,$Root_Url);
				
		if($Redir_Url ne '')
		{
			$Root_Url=$Redir_Url;
		}
		elsif($Document_Url ne '')
		{
			$Root_Url=$Document_Url;
		}
		elsif($URL ne '')
		{
			$Root_Url=$URL;
		}
		
		
		foreach my $ts ( $te->tables() )
		{
			
			foreach my $row ( $ts->rows() )
			{
				my $link="@$row[3]"; 
				if($Doc_URL_BY eq 'POST')
				{
					my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$Doc_POST_ID2,$CSRFToken);
					my $Doc_Post_Content_Tmp=$councilDetailsFile->{$Council_Code}->{'Doc_Post_Content'};;
					if($councilDetailsFile->{$Council_Code}->{'Viewstate_Regex'} ne 'N/A')
					{
						$Doc_viewstate=uri_escape($1) if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Viewstate_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Viewstategenerator_Regex'} ne 'N/A')
					{
						$Doc_viewstategenerator=$1 if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Viewstategenerator_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'CSRFToken_Regex'} ne 'N/A')
					{
						$CSRFToken=uri_escape($1) if($Content=~m/$councilDetailsFile->{$Council_Code}->{'CSRFToken_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Eventvalidation_Regex'} ne 'N/A') 
					{
						$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Eventvalidation_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Doc_Page_Num_Regex'} ne 'N/A')
					{
						$Doc_Page_Num=$1 if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Page_Num_Regex'}/is);
					}	
					if($councilDetailsFile->{$Council_Code}->{'Doc_NumberOfRows_Regex'} ne 'N/A')
					{
						$Doc_NumberOfRows=$1 if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_NumberOfRows_Regex'}/is);
					}	
					if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_POST_ID1_Regex'}/is)
					{
						$Doc_POST_ID1=uri_escape($1);
						$Doc_POST_ID2=uri_escape($2);
					}

					
					$councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'}  = $Document_Url if($councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'}=~m/Document_Url/is);
					$councilDetailsFile->{$Council_Code}->{'Referer'}  = $Document_Url if($councilDetailsFile->{$Council_Code}->{'Referer'}=~m/Document_Url/is);
					
					
					# $Doc_Post_URL=decode_entities($Doc_Post_URL);
					# $Referer=decode_entities($Referer);
					
					$councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'}=decode_entities($councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'});
					$councilDetailsFile->{$Council_Code}->{'Referer'}=decode_entities($councilDetailsFile->{$Council_Code}->{'Referer'});
					
					
					$Doc_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
					$Doc_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
					$Doc_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
					$Doc_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
					$Doc_Post_Content_Tmp=~s/<PageNum>/$Doc_Page_Num/igs;
					$Doc_Post_Content_Tmp=~s/<PageRows>/$Doc_NumberOfRows/igs;
					$Doc_Post_Content_Tmp=~s/<SelectedID1>/$Doc_POST_ID1/igs;
					$Doc_Post_Content_Tmp=~s/<SelectedID2>/$Doc_POST_ID2/igs;
					
					
				
					$councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'}=$councilDetailsFile->{$Council_Code}->{'Host'} if($councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'} eq '' or $councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'} eq 'N/A');
					
					($Post_Responce,$res_code)=&Post_Method($councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'},$councilDetailsFile->{$Council_Code}->{'Doc_Post_Host'},$councilDetailsFile->{$Council_Code}->{'Content_Type'},$councilDetailsFile->{$Council_Code}->{'Referer'},$Doc_Post_Content_Tmp,$councilDetailsFile->{$Council_Code}->{'Proxy'});
					
					# open(DC,">Post_Responce.html");
					# print DC "$Post_Responce";
					# close DC;					

					my $URL_ID;
					if($Post_Responce=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
					{
						my $val=$1;
						if($councilDetailsFile->{$Council_Code}->{'DOC_PDF_ID_URL'}=~m/ID/is)
						{
							$URL_ID=$val;
						}
						elsif($councilDetailsFile->{$Council_Code}->{'DOC_PDF_ID_URL'}=~m/URL/is)
						{
							my $u1=URI::URL->new($val,$councilDetailsFile->{$Council_Code}->{'Doc_Post_URL'});
							my $u2=$u1->abs;
							$Doc_URL=$u2;
						}
					}
					if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A')
					{
						# next if($PDF_URL_Cont == '');
						$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
						$Doc_URL=~s/<SelectedID>/$URL_ID/igs;
					
					}
					$Doc_URL=~s/amp\;//igs;
					
				
			
				}
				elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'} ne 'N/A')
				{
					
					if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
					{
						
						my $Durl1=$1;
						my $Durl2=$2;
						my $Durl3=$3;
						
						
						if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A') 
						{
							
							if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'}=~m/Redir_Url/is)
							{
								my $Doc_Download_URL_Replace_tmp=$Redir_Url;
								my @Replace_URL_Split=split('\|',$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'});
								$Doc_Download_URL_Replace_tmp=~s/$Replace_URL_Split[1]/$Durl1/igs;
								$Doc_URL=$Doc_Download_URL_Replace_tmp;
								print "Doc_URL: $Doc_URL";
							}	
							else
							{
								my $Doc_Download_URL_Replace_tmp=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
								$Doc_Download_URL_Replace_tmp=~s/<SelectedID1>/$Durl1/igs;
								$Doc_Download_URL_Replace_tmp=~s/<SelectedID2>/$Durl2/igs;
								$Doc_Download_URL_Replace_tmp=~s/<SelectedID3>/$Durl3/igs;
								$Doc_URL=$Doc_Download_URL_Replace_tmp;
							}	
						}
						elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne 'N/A') 
						{
							print "Doc_Download_URL_ABS: $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}\n";
							$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Durl1;
							print "Doc_URL1: $Doc_URL\n";
						}
						else
						{
							my $u1=URI::URL->new($Durl1,$Root_Url);
							my $u2=$u1->abs;
							$Doc_URL=$u2;
							print "Doc_URL2: $Doc_URL\n";
						}	
					}
					if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_redirct'} eq 'Y')
					{
						my ($Redir_Content, $code, $reurl)=&Getcontent($Doc_URL,"","","","",$councilDetailsFile->{$Council_Code}->{'Proxy'},$councilDetailsFile->{$Council_Code}->{'Need_Host'},$councilDetailsFile->{$Council_Code}->{'Accept_Language'});
						if($Redir_Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_redirct_Regex'}/is)
						{
							my $Durl=$1;
							if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne 'N/A')
							{
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Durl;
							}
							else
							{
								my $u1=URI::URL->new($Durl,$Doc_URL);
								my $u2=$u1->abs;
								$Doc_URL=$u2;
							}	
						}
					}		
				}
				elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A')
				{
					next if($link == '');
					$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
					$Doc_URL=~s/<SelectedID>/$link/igs;
				}

				$Doc_Desc = &Clean(@$row[2]);
				$document_type = &Clean(@$row[1]);
				$Published_Date = &Clean(@$row[0]);
				
				my ($file_type);
				if($councilDetailsFile->{$Council_Code}->{'Doc_File_Format_Regex'} ne 'N/A' and $councilDetailsFile->{$Council_Code}->{'Doc_File_Format_Regex'} ne '')
				{
					if($Content=~m/$councilDetailsFile->{$Council_Code}->{'Doc_File_Format_Regex'}/is)
					{
						$file_type=$1;
					}
				}
				elsif($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				$pdfcount_inc++;
			
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
				}
			}	
		}
	}
	return \%Doc_Details,$pdfcount_inc;
}



sub Doc_download_600_Html_Header()
{
	my $councilDetailsFile=shift;
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $ApplicationURL=shift;
	my $Doc_URL_BY=shift;
	my $Council_Code=shift;
	my $pdfcount_inc=shift;
	my $pageFetch=shift;
	
	
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	$Content=~s/<th\/>/<th><\/th>/igs;
	$Content=~s/<td\/>/<td><\/td>/igs;
	if($councilDetailsFile->{$Council_Code}->{'Block_Regex1'} ne 'N/A')
	{
		
		my $data = $1 if($Content=~m/($councilDetailsFile->{$Council_Code}->{'Block_Regex1'})/is);
		chomp $data;
		$data=~s/(<input[^>]*?value=\"([^\"]*?)\"[^>]*?\/?>)\s*<\/th>/$2<\/th>/gsi;
		
		my @check_column;
		
		while ($data=~m/(<th[^<]*?>[\w\W]*?<\/th>)/igs)
		{
			my $y=$1;
			push(@check_column,$y);
		}
		my $check_column_size=@check_column;
		if ($check_column_size < 4)
		{
			while($check_column_size - 4)
			{
				$data=~s/(<table[^<]*?>[\w\W]*?)<\/tr>/$1 <th> <\/th> <\/tr>/igs;
				$check_column_size=$check_column_size+1;
					
			}
		}
		
		my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code,$Root_Url);

		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'}='';
		}
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'}='';
		}
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}='';
		}
		
		if($Redir_Url ne '')
		{
			$Root_Url=$Redir_Url;
		}
		elsif($Document_Url ne '')
		{
			$Root_Url=$Document_Url;
		}
		elsif($ApplicationURL ne '')
		{
			$Root_Url=$ApplicationURL;
		}
		
		if($Redir_Url ne '')
		{
			$Root_Url=$Redir_Url;
		}
		elsif($Document_Url ne '')
		{
			$Root_Url=$Document_Url;
		}
		
	
	
		my $te = HTML::TableExtract->new(keep_html => 1,headers =>[$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}]);
		
		$te->parse( $data );
		foreach my $ts ( $te->tables() )
		{
			foreach my $row ( $ts->rows() )
			{
				my $link="@$row[3]"; 
				if($councilDetailsFile->{$Council_Code}->{'Doc_URL_BY_Regex'} eq 'By Click')
				{
					my ($AppDoc,$ViewDoc);
							
					if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
					{
						$AppDoc=$1;
						$ViewDoc=$2;
						 
						
					}	
							
					$pageFetch->form_number($councilDetailsFile->{$Council_Code}->{'FORM_NUMBER1'}); 
					$pageFetch->set_fields($AppDoc => $ViewDoc);
					$pageFetch->click();	
							
					my $Page_url = $pageFetch->uri();
					# print "$Page_url\n";
					my $App_Page_Content = $pageFetch->content;
					
					if($councilDetailsFile->{$Council_Code}->{'Docs_URL_Regex'} ne 'N/A')
					{
						if($App_Page_Content=~m/$councilDetailsFile->{$Council_Code}->{'Docs_URL_Regex'}/is)
						{
							$Doc_URL = $1;
							
							$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Doc_URL if($Doc_URL!~m/^http/is);	
						}
					}
					
					
				}
				elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'} ne 'N/A')
				{
					if($link=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
					{
						my $Durl=$1;				
						# print "Durl: $Durl\n"; <STDIN>;
						if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A') 
						{		
							my $docID=$1 if($Durl=~m/Download\.aspx\?ID=(\d+)$/is);	
							$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
							$docID=sprintf("%08d",$docID);
							$Doc_URL=~s/<SelectedID>/$docID/igs;
							# print "$Doc_URL\n"; <STDIN>;
						}
						elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne 'N/A') 
						{	
							if($Council_Code=~m/^8$/is)
							{
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'FILTER_URL'}.$Durl;
								if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}=~m/pa\.brent\.gov\.uk/is)
								{
									$Doc_URL;
								}
								else
								{
									my ($searchPageCon,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);	
									$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$1 if($searchPageCon=~m/<meta[^>]*?URL=\.\.\/\.\.\/([^\"]*?)\"[^>]*?>/is);
								}
							} 
							elsif($Durl!~m/^http/is)
							{
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Durl;
							}
							else
							{
								$Doc_URL=$Durl;
							}
							# print "Doc_URL1: $Doc_URL\n"; <STDIN>;
						}
						elsif($Durl!~m/^http/is)
						{							
							my $u1=URI::URL->new($1,$Root_Url);
							my $u2=$u1->abs;
							$Doc_URL=$u2;
							# print "Root_Url: $Root_Url\n";
							# print "Doc_URL2: $Doc_URL\n"; <STDIN>;
						}
						else
						{	
							$Doc_URL=$Durl;		
							# print "Doc_URL2: $Doc_URL\n"; <STDIN>;
							
						}	
					}
				}
				
				
				$Doc_Desc = &Clean(@$row[2]);
				$document_type = &Clean(@$row[1]);
				$Published_Date = &Clean(@$row[0]);
				
				
				if($Doc_URL=~m/^http\:\/\/planning\.cambridgeshire\.gov\.uk\/swift/is) # for 608 Council
				{
					my ($docPageContent,$Code) = &docMechMethod($Doc_URL,$pageFetch);
					my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
					$Doc_URL = "http://planning.cambridgeshire.gov.uk/swift".$URL if($URL!~m/^http/is);	
				}
				elsif($Doc_URL=~m/^http\:\/\/myeplanning2\.oxfordshire\.gov\.uk\/swiftlg/is) # for 628 Council
				{
					
					my ($docPageContent,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
					my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
					$Doc_URL = "http://myeplanning2.oxfordshire.gov.uk/swiftlg".$URL if($URL!~m/^http/is);
					
				}
				elsif($Doc_URL=~m/^https?\:\/\/planning\.warwickshire\.gov\.uk\/swiftlg/is) # for 621 Council
				{
					my ($docPageContent,$Code) = &Download_Utility::docMechMethod($Doc_URL,$pageFetch);
					my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
					$Doc_URL = "https://planning.warwickshire.gov.uk/swiftlg".$URL if($URL!~m/^http/is);	
				}
				elsif($Doc_URL=~m/^http\:\/\/e-planning\.worcestershire\.gov\.uk\/swift/is) # for 618 Council
				{
					my ($docPageContent,$Code) = &docMechMethod($Doc_URL,$pageFetch);
					my $URL = $1 if($docPageContent=~m/<meta[^>]*?URL=[^>]*?(\/MediaTemp\/[^\"]*?)\"[^>]*?>/is);
					$Doc_URL = "http://e-planning.worcestershire.gov.uk/swift".$URL if($URL!~m/^http/is);	
				}	
					
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDoc=$1;
					if($file_typeDoc eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDoc;
					}
				}
				elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDocType=$1;
					if($file_typeDocType eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDocType;
					}
				}

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				$Doc_URL=~s/\&amp\;/\&/gsi;
				# print "Doc_URL	: $Doc_URL\n"; <STDIN>;	
				
				$pdf_name = &PDF_Name($pdf_name);
				
				$Doc_URL=decode_entities($Doc_URL);
				
				$pdfcount_inc++;
			
				

				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					
					
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
				}
		
			}
	
		}
	
	}
	
	return \%Doc_Details;
}






sub Format8_Html_Header()
{
	my $councilDetailsFile=shift;
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $ApplicationURL=shift;
	my $Doc_URL_BY=shift;
	my $Council_Code=shift;
	my $pdfcount_inc=shift;
	my $pageFetch=shift;
	
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	
	# open(DC,">Search_Content.html");
	# print DC $Content;
	# close DC;
	$Content=~s/<td\/>/<td><\/td>/igs;
	$Content=~s/<th\/>/<th><\/th>/igs;
	if($councilDetailsFile->{$Council_Code}->{'Block_Regex1'} ne 'N/A')
	{
		my $data = $1 if($Content=~m/($councilDetailsFile->{$Council_Code}->{'Block_Regex1'})/is);
		chomp $data;
		$data=~s/(<input[^>]*?value=\"([^\"]*?)\"[^>]*?\/?>)\s*<\/th>/$2<\/th>/gsi;
		
		my @check_column;
		
		while ($data=~m/(<th[^<]*?>[\w\W]*?<\/th>)/igs)
		{
			my $y=$1;
			push(@check_column,$y);
		}
		my $check_column_size=@check_column;
		if ($check_column_size < 4)
		{
			while($check_column_size - 4)
			{
				$data=~s/(<table[^<]*?>[\w\W]*?)<\/tr>/$1 <th> <\/th> <\/tr>/igs;
				$check_column_size=$check_column_size+1;
					
			}
		}
		
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'}='';
		}
		
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'}='';
		}
		if ($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'} eq 'N/A')
		{
			$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}='';
		}
		
		my ($Doc_URL,$PDF_URL_Cont,$Doc_Desc,$document_type,$Published_Date,$pdf_name,$Post_Responce,$res_code,$Root_Url);
				
		if($Redir_Url ne '')
		{
			$Root_Url=$Redir_Url;
		}
		elsif($Document_Url ne '')
		{
			$Root_Url=$Document_Url;
		}
		elsif($ApplicationURL ne '')
		{
			$Root_Url=$ApplicationURL;
		}
	
		
		my $te = HTML::TableExtract->new(keep_html => 1,headers =>[$councilDetailsFile->{$Council_Code}->{'Doc_Published_Date_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Type_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Description_Index'},$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Index'}]);
		
		$te->parse( $data );
		
		foreach my $ts ( $te->tables() )
		{
			
			foreach my $row ( $ts->rows() )
			{
				
			
				my $link="@$row[3]"; 
				if($Council_Code=~m/^333$/is)
				{
					$PDF_URL_Cont = $link;
				}
				else
				{
					
					$PDF_URL_Cont = $link;
				}
		
				if($Doc_URL_BY eq 'Salford')
				{
					my ($AppDoc,$ViewDoc,$AppDoc1,$ViewDoc1,$AppDoc2,$ViewDoc2);
							
					if($PDF_URL_Cont=~m/$councilDetailsFile->{$Council_Code}->{'Doc_URL_BY_Regex'}/is)
					{
						$AppDoc=uri_escape($1);
						$ViewDoc=uri_escape($2);
					}	
						
					if($PDF_URL_Cont=~m/value=\"View\s*document\"\s*[^>]*?>\s*<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
					{
						$AppDoc1=uri_escape($1);
						$ViewDoc1=uri_escape($2);
					}		
					$ViewDoc2=$ViewDoc;
					$AppDoc2=$AppDoc1;
					$ViewDoc2=~s/btnViewDocument$/ctl00/gsi;
								
					my ($hdnWindowWidth,$hdnWindowWidthval);
					my $VIEWSTATE = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
					my $EVENTVALIDATION = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
					if($Content=~m/<input[^>]*?name=\"([^\"]*?hdnWindowWidth)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
					{
						$hdnWindowWidth = uri_escape($1);
						$hdnWindowWidthval = $2;			
					}
					
					my $Post_Content = "__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24ContentPlaceHolder%24ddlPageSize=99999&$AppDoc2=$ViewDoc2&$AppDoc=$ViewDoc&$AppDoc1=$ViewDoc1&$hdnWindowWidth=$hdnWindowWidthval";
					
					$Post_Content=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
					$Post_Content=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
					
					
					my ($App_Page_Content,$Code) = &Download_Utility::docPostMechMethod($Document_Url,$Post_Content,$pageFetch);	
					$Post_Content=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
					$Post_Content=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
					
					my $halfURL = $1 if($App_Page_Content=~m/window\.open\(\'([^\']*?)\'\)\;\/\/\]\]>/is);
					
					$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$halfURL;
					
					
				}
				elsif($Doc_URL_BY eq 'By Click')
				{
					my ($AppDoc,$ViewDoc);
							
					if($PDF_URL_Cont=~m/$councilDetailsFile->{$Council_Code}->{'Doc_URL_BY_Regex'}/is)
					{
						$AppDoc=$1;
						$ViewDoc=$2;
						# print "$ViewDoc=$AppDoc\n";
					}	
							
					$pageFetch->form_number($councilDetailsFile->{$Council_Code}->{'FORM_NUMBER1'}); 
					$pageFetch->set_fields($AppDoc => $ViewDoc);
					$pageFetch->click();	
							
					my $Page_url = $pageFetch->uri();
					# print "$Page_url\n";
					my $App_Page_Content = $pageFetch->content;
					
					
					
					# open(DC,">App_Page_Content.html");
					# print DC $App_Page_Content;
					# close DC;	
				}
				elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'} ne 'N/A')
				{
					if($PDF_URL_Cont=~m/$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Regex'}/is)
					{
						my $Durl=$1;	
						
						# print "$PDF_URL_Cont\n"; <STDIN>;
						if($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'} ne 'N/A') 
						{		
							my $docID=$1 if($Durl=~m/Download\.aspx\?ID=(\d+)$/is);	
							$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_Replace'};
							$Doc_URL=~s/<SelectedID>/$docID/igs;
						}
						elsif($councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'} ne 'N/A') 
						{
							if($Council_Code=~m/^8$/is)
							{
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'FILTER_URL'}.$Durl;
								my ($searchPageCon,$Code) = &docMechMethod($Doc_URL,$pageFetch);	
								$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$1 if($searchPageCon=~m/<meta[^>]*?URL=\.\.\/\.\.\/([^\"]*?)\"[^>]*?>/is);
							} 
							elsif($Council_Code=~m/^236$/is)
							{	
								if($Durl=~m/^([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)\'\,\s+\'([^\']*?)$/is)
								{
									my $app = uri_escape($1);
									my $folder3Ref = uri_escape($2);
									my $dateReceived = uri_escape($3);
									my $docRef = uri_escape($4);
									my $docRef2 = uri_escape($5);
									my $sDocType = uri_escape($6);
									my $modifyType = uri_escape($7);
									my $url = uri_escape($8);
									my $alt = uri_escape($9);
									
									my $pdfRUL = "https://ppc.ipswich.gov.uk/checkdoc.aspx?appid=$app&isanite=true&folder3Ref=$folder3Ref&dateReceived=$dateReceived&docRef=$docRef&docRef2=$docRef2&sDocType=$sDocType&modifyType=$modifyType&alt=$alt&url=$url";
															
									my ($searchPageCon,$Code) = &docMechMethod($pdfRUL,$pageFetch);	
									$Doc_URL = $councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$1 if($searchPageCon=~m/<a\s*[^>]*?\s*href\=(?:\'|\")([^<]*?)(?:\'|\")\s*[^>]*?>/is);
								}
							}
							else
							{					
								$Doc_URL=$councilDetailsFile->{$Council_Code}->{'Doc_Download_URL_ABS'}.$Durl;
							}
							# print "Doc_URL1: $Doc_URL\n";
						}
						elsif($Durl!~m/^http/is)
						{							
							my $u1=URI::URL->new($1,$Root_Url);
							my $u2=$u1->abs;
							$Doc_URL=$u2;
							# print "Doc_URL2: $Doc_URL\n"; <STDIN>;
						}
						else
						{	
							$Doc_URL=$Durl;		
							if($Council_Code=~m/^333$/is)
							{
								$Doc_URL=~s/\s/\%20/gsi;
							}
						}	
					}
				}
				
				if($Council_Code=~m/^(342|158|439|425|428|292)$/is)
				{
					$Doc_URL=$PDF_URL_Cont;
				}
							
				# $Doc_Desc = &Download_Utility::Clean($Doc_Desc);	
				# $document_type = &Download_Utility::Clean($document_type);
				# $Published_Date = &Download_Utility::Clean($Published_Date);
				
				$Doc_Desc = &Clean(@$row[2]);
				$document_type = &Clean(@$row[1]);
				$Published_Date = &Clean(@$row[0]);
				
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				
				if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDoc=$1;
					if($file_typeDoc eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDoc;
					}
				}
				elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
				{
					my $file_typeDocType=$1;
					if($file_typeDocType eq $file_type)
					{
						$file_type;
					}
					else
					{
						$file_type=$file_typeDocType;
					}
				}

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				# print "pdfcount_inc	: $pdfcount_inc\n"; <STDIN>;	
				
				$pdf_name = &PDF_Name($pdf_name);
				if($Council_Code!~m/^333$/is)
				{
					$Doc_URL=decode_entities($Doc_URL); 
				}
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "Doc_Type	: $document_type\n";
				print "Published_Date	: $Published_Date\n";

				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
				}
				
			}
		}
				
	}
	return (\%Doc_Details);
}

sub onDemandStatus()
{
	my $No_Of_Doc_Downloaded_onDemand=shift;
	my $Site_Status=shift;
	my $onDemandStatus;
	print "No_Of_Doc_Downloaded_onDemand: $No_Of_Doc_Downloaded_onDemand\n";
	print "Site_Status: $Site_Status\n";
	if($No_Of_Doc_Downloaded_onDemand == 0)
	{
		if($Site_Status != 200)
		{
			$onDemandStatus = 'Site issue';
		}
		else
		{
			$onDemandStatus = 'No new docs downloaded';
		}	
	}
	elsif($No_Of_Doc_Downloaded_onDemand > 0)
	{
		$onDemandStatus = $No_Of_Doc_Downloaded_onDemand.' new docs downloaded';
	}

	return $onDemandStatus;
}
