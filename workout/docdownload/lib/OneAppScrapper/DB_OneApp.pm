package DB_OneApp;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);
use Cwd qw(abs_path);
use File::Basename;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 

# Declare and load local directories and required private module
my $iniDirectory = ($basePath.'/etc/OneAppScrapper');

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/OneAppConfig.ini');

my $selectDB	= $Config->{'selectDB'}->{'dbname'};

my $database 	= $Config->{$selectDB}->{'database'};
my $driver 		= $Config->{$selectDB}->{'driver'};
my $pwd 		= $Config->{$selectDB}->{'pwd'};
my $server 		= $Config->{$selectDB}->{'server'};
my $uid 		= $Config->{$selectDB}->{'uid'};

###### DB Connection ####
sub DbConnection()
{
	my $dsn1 ="driver={$driver};Server=$server;database=$database;uid=$uid;pwd=$pwd;";
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}


sub Retrieve_OneApp_For_Scrape()
{
	my $dbh 			= shift;
	my $Date 			= shift;
	
	my $query = "select ID, COUNCIL_CODE, DOCUMENT_NAME, APPLICATION_NO, SOURCE, MARKUP from FORMAT_PUBLIC_ACCESS where 
	One_App = 'N' and 
	Document_Name is not null and  Document_Name != '' and
	Application_Extracted = 'N' and 
	MARKUP <> 'Minor' and 
	ID >= 7214294";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	my (@Format_id,@Council_Code,@DOCUMENT_NAME,@APPLICATION_NO,@SOURCE,@MARKUP);
	my $reccount;
	while(my @record = $sth->fetchrow)
	{
		push(@Format_id,&Trim($record[0]));
		push(@Council_Code,&Trim($record[1]));
		push(@DOCUMENT_NAME,&Trim($record[2]));
		push(@APPLICATION_NO,&Trim($record[3]));
		push(@SOURCE,&Trim($record[4]));
		push(@MARKUP,&Trim($record[5]));
	}
	$sth->finish();
	return (\@Format_id,\@Council_Code,\@DOCUMENT_NAME,\@APPLICATION_NO,\@SOURCE,\@MARKUP);
}

###### Update DB ####
sub UpdateDB
{
	my $dbh		= shift;
	my $query 	= shift;
	my $sth = $dbh->prepare($query);
	print "\nOK\n";
	eval
	{
		$sth->execute();
		$sth->finish();	
		print "Updated\n";
	};
	if($@)
	{
		print "QUERY:: $query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
