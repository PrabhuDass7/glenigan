# python 3.2

import pymssql
import sys
import re
from PyPDF2 import PdfFileReader

def dbConnection(database):
	# conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	conn = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
	return conn

def getPDFContent(path):
    try:
        pdfDoc = PdfFileReader(open(path, "rb"))
        content = ''
        for i in range(pdfDoc.numPages):
            print ('Page:', i)
            content = content+' '+pdfDoc.getPage(i).extractText()
            if len(content) > 100:
                break
        return clean(content)
    except Exception as e:
        return ("Error:"+str(e))

def clean(data):
	data = re.sub(r'\s\s+', ' ', data)
	data = re.sub(r'\n', ' ', data)
	data = re.sub(r'\s', 'spaceforcleaning', data)
	data = re.sub(r'_+', ' ', data)
	data = re.sub(r'\?+', ' ', data)
	data = re.sub(r'\^+', ' ', data)
	data = re.sub(r'\W+', '', data)
	data = re.sub(r'spaceforcleaning', ' ', data)
	return data

if __name__ == "__main__":
	documentSourcePath = '//172.27.137.202/One_app_download/'
	insertQuery = 'insert into TBL_DOCUMENT_RENAME(Format_ID, Doc_Name, Doc_Content) values'
	conn = dbConnection("SCREENSCRAPPER")
	cursor = conn.cursor()
	# qry_getDescription = "select distinct Batch_ID, Project_ID, original_filename, Number_Of_Pages, PDF_Length, Category from BASIL..NEED_OCR where batch_id in ('B1808290019') and  category not  in (4,7) and category!=''"
	# qry_getDescription = "select * Format_ID, Filename from Format_Document where Classifier_Status='N'"
	qry_getDescription = "select top 10 Format_ID, Filename from Format_Document order by Format_ID desc"
	cursor.execute(qry_getDescription)
	fetchResults = cursor.fetchall()
	bulkValuesForQuery=''
	docsCount=0
	for i in range (len(fetchResults)):
	# for i in range (0,10):
		Format_ID = str(fetchResults[i][0])
		Filename = str(fetchResults[i][1])
		documentLocation = documentSourcePath+'/'+Format_ID+'/'+Filename
		Format_ID = str(fetchResults[i][0])
		print ("documentLocation:"+documentLocation)
		docFirstPageContent = getPDFContent(documentLocation)
		joinValues="("+"'"+str(Format_ID)+"','"+str(Filename)+"','"+str(docFirstPageContent)+"')"
		print ("docsCount",docsCount)
		if docsCount == 0:
			bulkValuesForQuery = insertQuery+joinValues
			docsCount += 1
		elif docsCount == 50:
			print("Insert",docsCount)
			bulkValuesForQuery = bulkValuesForQuery+","+joinValues
			cursor.execute(bulkValuesForQuery)
			conn.commit()
			docsCount=0
			bulkValuesForQuery=''
		else:
			bulkValuesForQuery = bulkValuesForQuery+","+joinValues
			docsCount += 1
	if (bulkValuesForQuery != ''):
		cursor.execute(bulkValuesForQuery)
		conn.commit()
	conn.close
