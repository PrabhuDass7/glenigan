use strict;
use Cwd qw(abs_path);
use File::Basename;

use DateTime;
my $current_date_time=DateTime->now;

my $schdule_time=$current_date_time;
$current_date_time=~s/\-|\:/_/igs;



my $status_log_file_name="Councile_code_".$current_date_time.".txt";

open(my $fh,">",$status_log_file_name);
print $fh "Council_code\tDatetime\n";
close ($fh);




# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'\\lib');
my $Script_Directory = ($basePath.'\\root');

# Private Module
require ($libDirectory.'\\Download_DB.pm'); 


#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Council_Name,$Council_Code) = &Download_DB::Retrieve_Input_CC($dbh);

my $council_count = @{$Council_Code};

# print "council_count==>$council_count\n"; <STDIN>;

my $script_name;

# for(my $councilcnt = 0; $councilcnt < $council_count; $councilcnt++ )
for(my $councilcnt = 0; $councilcnt < 50; $councilcnt++ )
{
	# print "CouncilCNT :: $councilcnt \n";
	my $COUNCIL_NAME 		= @$Council_Name[$councilcnt];
	my $COUNCIL_CODE 		= @$Council_Code[$councilcnt];
	
	next if($COUNCIL_CODE == 486);
	
	# print "COUNCIL_NAME==>$COUNCIL_NAME\n";
	print "COUNCIL_CODE==>$COUNCIL_CODE\n";
		
	if($COUNCIL_CODE=~m/^5\d{2}$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/DocDownload_500s.pl";
	}
	elsif($COUNCIL_CODE=~m/^(600)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/DocDownload_600s.pl";
	}
	elsif($COUNCIL_CODE=~m/^(8|22|25|90|103|112|133|147|148|158|179|193|195|201|236|240|247|275|280|285|289|292|308|316|332|333|334|342|375|376|378|385|388|394|423|425|428|432|439|444|449|454|459|461|470)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/DocDownloadFormat8.pl";
	}
	elsif($COUNCIL_CODE=~m/^(124|94|91|244|367|157|14|92|166|229|297|379|431|437|22|46|224|362|207|132|185|393|9|32|95|110|116|128|287|406|424|438|98|16|21|323|339|370|327|416|216|450|216)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/Doc_Download_Format1_Extented.pl";
	}
	elsif($COUNCIL_CODE=~m/^(34|27|355|480|414|218)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/Doc_Download_Others.pl";
	}
	elsif($COUNCIL_CODE=~m/^(451|428|261)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/Doc_Download_Direct.pl";
	}
	else
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/Doc_Download.pl";
	}
	
	# system("start PERL $script_name $COUNCIL_CODE");
	if($COUNCIL_CODE!~m/^\s*486\s*$/is)
	{
		system("start PERL $script_name $COUNCIL_CODE");
	}	
	else
	{
		open(my $fh,">>status_log_file_name_486.txt");
		print $fh "$COUNCIL_CODE\t$schdule_time\n";
		close ($fh);		
	}
			
	open(my $fh,">>$status_log_file_name");
	print $fh "$COUNCIL_CODE\t$schdule_time\n";
	close ($fh);


}