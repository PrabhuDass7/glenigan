use strict;
use filehandle;
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use JSON::Parse 'parse_json';
use Cwd qw(abs_path);
use File::Basename;
use URI::Escape;
use File::stat;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\\/\//igs;

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 


# print "LibDirectory::\t$libDirectory\n";
# print "INIDirectory::\t$iniDirectory\n";
# print "DataDirectory::\t$dataDirectory\n";

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];
print "onDemand: $onDemand\n";

if($Council_Code=~m/486/is)
{
	exit;
}

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

next if($Council_Code=~m/^50$/is);

#---------------- Get local date & time ----------------#
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();


#-------------------------- Global Variable Declaration -------------------------#
my $docDownload_Location = $dataDirectory.'/Documents';

#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);

####
# Create new object of class Config::Tiny and Config::Simple
####
my ($regexFile) = Config::Tiny->new();

$regexFile = Config::Tiny->read($iniDirectory.'/Scraping_Details_500.ini');

my $Method = $regexFile->{$Council_Code}->{'Method'};
my $Format = $regexFile->{$Council_Code}->{'Format'};
my $Home_URL = $regexFile->{$Council_Code}->{'Home_URL'};
my $Accept_Host = $regexFile->{$Council_Code}->{'Accept_Host'};
my $Content_Type = $regexFile->{$Council_Code}->{'Content_Type'};
my $RTF_Content_Type = $regexFile->{$Council_Code}->{'Content_Type'};
my $Referer = $regexFile->{$Council_Code}->{'Referer'};
my $FORM_NUMBER = $regexFile->{$Council_Code}->{'FORM_NUMBER'};
my $FILTER_URL = $regexFile->{$Council_Code}->{'FILTER_URL'};
my $ApplicationReference = $regexFile->{$Council_Code}->{'ApplicationReference'};
my $Need_Host = $regexFile->{$Council_Code}->{'Need_Host'};
my $Landing_Page_Regex = $regexFile->{$Council_Code}->{'Landing_Page_Regex'};
my $ShowCase_URL = $regexFile->{$Council_Code}->{'ShowCase_URL'};
my $GUID_URL = $regexFile->{$Council_Code}->{'GUID_URL'};
my $Doc_URL_Regex = $regexFile->{$Council_Code}->{'Doc_URL_Regex'};
my $PDF_URL = $regexFile->{$Council_Code}->{'PDF_URL'};
my $GUID_Regex = $regexFile->{$Council_Code}->{'GUID_Regex'};
my $Viewstate_Regex = $regexFile->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $regexFile->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $regexFile->{$Council_Code}->{'Eventvalidation_Regex'};
my $PDFGUID_URL = $regexFile->{$Council_Code}->{'PDFGUID_URL'};
my $PDFDownload_Regex = $regexFile->{$Council_Code}->{'PDFDownload_Regex'};
my $Host = $regexFile->{$Council_Code}->{'Host'};


#---------------- Create Global variable here -------------------#
my ($Update_Query,$COUNCIL_NAME_TMP);
my $OneAppDoc_Location = '\\\\172.27.138.180\\Council_PDFs\\';
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;
my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';


#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	

if($onDemand eq 'Y')
{
	print "This is an On demand request\n";
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\'";	
	&Download_DB::Execute($dbh,$onDemand_Update_Query);
}

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;


#---------------- Declare Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	


#---------------- Get Council Name from Retrieve Input lis -------------------#
$COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];


#---------------- Insert Dashboard query in DB -------------------#
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	
# &Download_DB::Execute($dbh,$Dashboard_Insert_Query);


my $reccount = @{$Format_ID};
print "COUNCIL      :: <$Council_Code>\n";
print "COUNCIL NAME :: <$COUNCIL_NAME_TMP>\n";
print "RECCNT       :: <$reccount>\n";


for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
		
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code);
	my $Landing_Page_Flag=0;
		
	my $Download_Location = $docDownload_Location."/$Format_ID";
	
	($Doc_Details_Hash_Ref, $Document_Url, $Code)=&Extract_Link($Home_URL,$Document_Url,$URL,$Council_Code,$Accept_Host,$Content_Type,$Referer,$Need_Host,$Application_No,$Download_Location,$Markup,$NO_OF_DOCUMENTS,$Format_ID);
	$Site_Status = $Code;	
	
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;
	$Todays_Count = $Todays_Count + $pdfcount;
	
	print "pdfcount=>$pdfcount\n";
	print "Todays_Count=>$Todays_Count\n";
	
	my $RetrieveMD5=[];
	my $No_Of_Doc_Downloaded=0;
	# if(lc($Markup) eq 'large')
	# {
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }
		
	unless ( -d $Download_Location )
	{
		$Download_Location=~s/\//\\/igs;
		system("mkdir $Download_Location");
	}
	
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);
		print "PDF_URL==>$pdf_url\n";
		# <STDIN>;
		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				my ($D_Code, $Err_Msg, $MD5)=&Download($pdf_url,$Pdf_Name,$Document_Url,$Download_Location,$RetrieveMD5,$Markup);
							
				
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);				
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{			
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my $Download_Status;
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5)=&Download($pdf_url,$Pdf_Name,$Document_Url,$Download_Location,$RetrieveMD5,$Markup);
					($Download_Status)=&Download_Utility::Url_Status($D_Code);
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}	
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
						
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
					
					
				}
				
			}
			elsif($Doc_Type=~m/$Application_Form_Keywords/is  && $Doc_Type !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my $Download_Status;
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5)=&Download($pdf_url,$Pdf_Name,$Document_Url,$Download_Location,$RetrieveMD5,$Markup);
					($Download_Status)=&Download_Utility::Url_Status($D_Code);
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
				}
			}
		}	
	}	
	
	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	&Download_Utility::Move_Files($OneAppDoc_Location, $Download_Location, $basePath,$Format_ID);
	
	DB_Flag:
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
	}	
	# else
	# {
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "NO_OF_DOCUMENTS from DB: $NO_OF_DOCUMENTS\n";

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	print "Todays_Downloaded_Count: $Todays_Downloaded_Count\n";
	
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if(($One_App_Name=~m/^\s*$/is) && ($pdfcount != 0))
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	elsif($pdfcount != 0)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	if ($onDemand eq 'ApprovedDecision')
	{
		my $Flag_Update_query = "update GLENIGAN..TBL_APPROVED_LARGE_PROJECT set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\', All_Documents_Downloaded = 'Y' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
		$ApprovedDecisionUpdate_query.= $Flag_Update_query;
	}	
	
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

$Insert_OneAppLog=~s/\,$//igs;
print "\n$Insert_OneAppLog\n";
# &Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
print "\n$Insert_Document\n";
# &Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
# &Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
# &Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
# &Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);

if($onDemand eq 'Y')
{
	print "Update & Delete This Ondemand request\n";
	my $onDemandStatus = &Download_Utility::onDemandStatus($No_Of_Doc_Downloaded_onDemand,$Site_Status);
	my $onDemandUpdateQuery = "update tbl_on_demand_download_backup set Status = \'$onDemandStatus\' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and download_id = \'$onDemandID\'";
	print "onDemandUpdateQuery: $onDemandUpdateQuery\n";
	# &Download_DB::Execute($dbh,$onDemandUpdateQuery);
	my $onDemandDeleteQuery = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and status = 'Inprogress'";	
	# &Download_DB::Execute($dbh,$onDemandDeleteQuery);
}

sub Download()
{
	my $pdf_url 		= shift;
	my $pdf_name 		= shift;
	my $Document_Url	= shift;
	my $Temp_Download_Location	= shift;
	my $MD5_LIST		= shift;
	my $Markup			= shift;
	
	my @MD5_LIST = @$MD5_LIST;
	
	opendir ODH , "$Temp_Download_Location" or warn "Archive directory not found \n";
	my @Files_avil_in_Temp_Loc = grep(/\.pdf$/,readdir(ODH));
	close ODH;
		
	my ($file_name,$Err,$Code,$MD5);
	my $download_flag=0;
	
	if( grep (/$pdf_name/, @Files_avil_in_Temp_Loc))
	{
		print "File Already Downloaded\n";
	}
	else
	{
		my $downCount=1;
		Re_Download:
		
		my ($pdfContent,$guidCode) = &Download_Utility::docMechMethod($pdf_url,$pageFetch);
						
		my $PDFGUID = $1 if($pdfContent=~m/^(.*?)$/is);
		
		my $tempPDF_URL=$PDFGUID_URL.$PDFGUID;
		my ($pdf_content,$code) = &Download_Utility::docMechMethod($tempPDF_URL,$pageFetch);
		$Code=$code;		
		# print "pdf_name::$pdf_name\n";
		$file_name=$Temp_Download_Location.'\\'.$pdf_name;
		# print "file_name::$file_name\n";
		# <STDIN>;
		eval{
			
			if(lc($Markup) eq 'large')
			{
				$MD5 = Digest::MD5->new->add($pdf_content)->hexdigest;
			}
			
			if (grep( /^$MD5$/, @MD5_LIST )) 
			{
				print "Doc Exist\n";
				$MD5='';
			}
			else
			{
				print "Doc not Exist\n";
				my $fh = FileHandle->new("$file_name",'w') or warn "Cannot open $Temp_Download_Location/$pdf_name for write :$!";
				binmode($fh);
				$fh->print($pdf_content);
				$fh->close();
				
				
				my $size = stat($file_name)->size;
                my $return_size_kb=sprintf("%.2f", $size / 1024);
                print "FileSizein_kb  :: $return_size_kb\n";
               
				if((($return_size_kb eq "0") or ($return_size_kb <= "1.00")) && ($downCount <= 2))
                {
                    unlink $file_name;
					$Code=600;
                    $downCount++;
                    goto Re_Download;
                }
			}
		};
		if($@)
		{
			$Err=$@;
			if($download_flag == 0)
			{
				$download_flag = 1;
				goto Re_Download;
			}	
		}
		
	}
	
	return($Code,$Err,$MD5);
}



sub Extract_Link()
{
	my $homeURL = shift;
	my $documentURL = shift;
	my $URL = shift;
	my $councilCode = shift;
	my $Accept_Host = shift;
	my $Content_Type = shift;
	my $Referer = shift;
	my $Need_Host = shift;
	my $Application_No = shift;
	my $Download_Location = shift;
	my $Markup = shift;
	my $NO_OF_DOCUMENTS = shift;
	my $Format_ID = shift;
		
	my ($docPageContent,$Code,$totalDownloadFileCount,$totalAvailableFileCount,$errorMsg);
	
		
	# print "documentURL<==>$documentURL\n"; 
	# 
		
	if($documentURL ne '')
	{
		if($Method=~m/GET/is)
		{
			($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
		}
	}
	else
	{
		print "Search Application method id processing..(Document URL not found)\n";
		($docPageContent,$Code)=&Search_Application($Application_No);
	}	
					
	my %Doc_Details;
	if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
	{
		my $tempGUID_URL;
		if(($ShowCase_URL ne '') && ($GUID_URL ne ''))
		{
			$tempGUID_URL=$GUID_URL;
			my $App_URL = $ShowCase_URL.$Application_No;
			
			print "Application Number==>$Application_No\n";
			print "App_URL==>$App_URL\n";
			
			my ($guidContent,$guidCode) = &Download_Utility::docMechMethod($App_URL,$pageFetch);
			# open F1, ">C:/Glenigan/Merit/Projects/docdownload_perl/root/content500guidContent.html";
				# print F1 $guidContent;
				# close F1;
			my $GUID = $1 if($guidContent=~m/^(.*?)$/is);				
			$tempGUID_URL=$tempGUID_URL.$GUID;				
			
			($docPageContent,$Code) = &Download_Utility::docMechMethod($tempGUID_URL,$pageFetch);
					
		}
		else
		{
			print "GUID number and that URL not found. Please check!..\n";
		}
		
		
		my $viewstate = uri_escape($1) if($docPageContent=~m/$Viewstate_Regex/is);
		my $viewstategenerator = uri_escape($1) if($docPageContent=~m/$Viewstategenerator_Regex/is);
		my $eventvalidation = uri_escape($1) if($docPageContent=~m/$Eventvalidation_Regex/is);
					
		if($GUID_Regex ne "")
		{
			my $a=1;
			my $totalFileCount;
			my $No_Of_Doc_Downloaded=0;
			$docPageContent=~s/\&\#39\;/\'/igs;
			# open F1, ">C:/Glenigan/Merit/Projects/docdownload_perl/root/content500docPageContent.html";
				# print F1 $docPageContent;
				# close F1;		
			while($docPageContent=~m/$GUID_Regex/mgsi)
			{
				my $docGroup=uri_escape($1);
				my $docGroupVal=$2;
				my $fcount=$3;
				
				$totalFileCount = $totalFileCount+$fcount;
				# print "docGroup:::$docGroup\n\n";
				# print "tempGUID_URL:::$tempGUID_URL\n\n";
									
				my $postdocContent = "__EVENTTARGET=$docGroup&__EVENTARGUMENT=&__VIEWSTATE=$viewstate&__VIEWSTATEGENERATOR=$viewstategenerator&__EVENTVALIDATION=$eventvalidation";
				
				# print "postdocContent:::$postdocContent\n\n";
				$pageFetch->post( $tempGUID_URL, Content => "$postdocContent");
				
				my $Code_status = $pageFetch->status;
				my $Content = $pageFetch->content;
				$Content=~s/\&\#39\;/\'/igs;
				print "Code_status:::$Code_status\n\n";
				# open F1, ">C:/Glenigan/Merit/Projects/docdownload_perl/root/content500$docGroup.html";
				# print F1 $Content;
				# close F1;
				
				my ($tempPDF_URL,$PDFdoc,$pdfCode,$dupPDF_URL);
				if(($PDFGUID_URL ne '') && ($PDF_URL ne ''))
				{
					my $count=1;
					while($Content=~m/$PDFDownload_Regex/mgsi)
					{
						my $publishedDate=$1;
						# my $documentType=$2;
						my $documentType=&Download_Utility::Clean($2);
						my $Doc_ID=$3;
						my $fileType=$4;
						my $openDocCountVal=uri_escape($5);													
						# my $docDescription=$6;													
						my $docDescription=&Download_Utility::Clean($6);		
												
						$dupPDF_URL=$PDF_URL;
						$dupPDF_URL=~s/<URI>/$Doc_ID/gsi;
						$dupPDF_URL=~s/<FILETYPE>/$fileType/gsi;
						$docDescription=~s/\//\-/gsi;						
						$docDescription=~s/\\/\-/gsi;						
						$documentType=~s/\//\-/gsi;						
						$documentType=~s/\\/\-/gsi;						
						my $pdf_name;
						if($docDescription!~m/^\s*$/is)
						{
							$pdf_name=$count."_".$docDescription.".".$fileType;
						}
						elsif($documentType!~m/^\s*$/is)
						{
							$pdf_name=$count."_".$documentType.".".$fileType;
						}
						else
						{
							$pdf_name=$count."_".$Doc_ID.".".$fileType;
						}
						$docDescription=~s/\-/\//gsi;						
						$docDescription=~s/\-/\\/gsi;						
						$documentType=~s/\-/\//gsi;						
						$documentType=~s/\-/\\/gsi;	
																	
						$count++;
												
						print "$fileType Download File Name => $pdf_name\n";
						# <STDIN>;
						if(($Doc_ID!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
						{
							$Doc_Details{$dupPDF_URL}=[$documentType,$pdf_name,$publishedDate,$docDescription];
						}
					}				
				}
				
				
				$a++;
			}
			
			
			$totalDownloadFileCount=$No_Of_Doc_Downloaded;
			$totalAvailableFileCount=$totalFileCount;
			
		}
	}
		
	return(\%Doc_Details,$documentURL,$Code);	
}


sub Search_Application()
{
	my $Application_No = shift;
	
	my ($homePageContent,$Code) = &Download_Utility::docMechMethod($Home_URL,$pageFetch);	
		
	$pageFetch->form_number($FORM_NUMBER);
	$pageFetch->set_fields( $ApplicationReference => $Application_No );
	$pageFetch->click();
	
	my $searchPageCon = $pageFetch->content;
	my $searchCode= $pageFetch->status();
	
	my ($docPageContent,$docRespCode);
	if($searchPageCon=~m/$Doc_URL_Regex/is)
	{
		my $docURL = $1;
		$docURL=~s/amp\;//igs;
		
		if($docURL!~m/^http\:/is)
		{
			$docURL = $FILTER_URL.$docURL;
		}		
		print "docURL::$docURL\n";
		
		($docPageContent,$docRespCode) = &Download_Utility::docMechMethod($docURL,$pageFetch);	
	}
	
	return($docPageContent,$docRespCode);
}


sub Terminated()
{
	my $statusCode = shift;
	my $Format_ID = shift;
	my $Council_Code = shift;
	my $Markup = shift;
	my $documentURL = shift;
	my $Downloaded_date = shift;
		
	my ($Update_Query,$Insert_OneAppLog);
		
	my $No_Of_Documents_Avail=0;
	my $No_Of_Doc_Downloaded=0;
	my $pdf_name_list;
	my $pdf_Url_list;
	my $One_App_Avail_Status;
	my $Comments=$statusCode;
	
	$Insert_OneAppLog = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$documentURL\', \'$No_Of_Documents_Avail\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";
	
	$Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Terminated\', Responce_Code = \'$statusCode\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";

	
	return($Update_Query,$Insert_OneAppLog);
}