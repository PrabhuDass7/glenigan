use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use JSON::Parse 'parse_json';
use Cwd qw(abs_path);
use File::Basename;


# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 

my $Council_Code = $ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];
print "onDemand: $onDemand\n";

# $Council_Code = 16;
if($Council_Code=~m/486/is)
{
	exit;
}

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

# &Download_Utility::User_Agent_Define();

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');

#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";
# my $Input_Table = 'FORMAT_PUBLIC_ACCESS';
# my $Output_Table = 'format_pdf';
# my $Log_Table = 'OneAppLog';
# my $MD5_Table = 'TBL_PDF_MD5';
# my $DashBoard_Table = 'TBL_ONEAPP_STATUS';
my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	
my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	
&Download_DB::Execute($dbh,$Dashboard_Insert_Query);
if($onDemand eq 'Y')
{
	print "This is an On demand request\n";
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\'";	
	&Download_DB::Execute($dbh,$onDemand_Update_Query);
}

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Doc_Download_Others.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $Proxy = $Config->{$Council_Code}->{'Proxy'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Primary_Url = $Config->{$Council_Code}->{'Primary_Url'};
my $Document_Url_Regex = $Config->{$Council_Code}->{'Document_Url_Regex'};
my $Proceed_Without_GET = $Config->{$Council_Code}->{'Proceed_Without_GET'};;
my $Accept_Cookie = $Config->{$Council_Code}->{'Accept_Cookie'};
my $Accept_Cookie_URL = $Config->{$Council_Code}->{'Accept_Cookie_URL'};
my $Accept_Post_Content = $Config->{$Council_Code}->{'Accept_Post_Content'};
my $Accept_Host = $Config->{$Council_Code}->{'Accept_Host'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Need_Host = $Config->{$Council_Code}->{'Need_Host'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $Accept_Language = $Config->{$Council_Code}->{'Accept_Language'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Search_URL = $Config->{$Council_Code}->{'Search_URL'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};
my $Navigate_By_Post1 = $Config->{$Council_Code}->{'Navigate_By_Post1'};
my $Navi1_Info_Regex1 = $Config->{$Council_Code}->{'Navi1_Info_Regex1'};
my $Navi1_Info_Regex2 = $Config->{$Council_Code}->{'Navi1_Info_Regex2'};
my $Navi1_Info_Regex3 = $Config->{$Council_Code}->{'Navi1_Info_Regex3'};
my $Navi1_Info_Regex4 = $Config->{$Council_Code}->{'Navi1_Info_Regex4'};
my $Navi1_Info_Regex5 = $Config->{$Council_Code}->{'Navi1_Info_Regex5'};
my $Navi1_Host = $Config->{$Council_Code}->{'Navi1_Host'};
my $Navi1_Post_Url = $Config->{$Council_Code}->{'Navi1_Post_Url'};
my $Navi1_Info_Need_UriEscape = $Config->{$Council_Code}->{'Navi1_Info_Need_UriEscape'};
my $Navi1_Post_Content = $Config->{$Council_Code}->{'Navi1_Post_Content'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Landing_Page_ABS_Replace = $Config->{$Council_Code}->{'Landing_Page_ABS_Regex'};
my $Landing_Page_ABS = $Config->{$Council_Code}->{'Landing_Page_ABS'};
my $Landing_Page_Regex2 = $Config->{$Council_Code}->{'Landing_Page_Regex2'};
my $Landing_Page_Regex3 = $Config->{$Council_Code}->{'Landing_Page_Regex3'};
my $Landing_Page_Regex4 = $Config->{$Council_Code}->{'Landing_Page_Regex4'};
my $Landing_Page_ABS4 = $Config->{$Council_Code}->{'Landing_Page_ABS4'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $CSRFToken_Regex = $Config->{$Council_Code}->{'CSRFToken_Regex'};
my $Doc_Page_Num_Regex = $Config->{$Council_Code}->{'Doc_Page_Num_Regex'};
my $Doc_NumberOfRows_Regex = $Config->{$Council_Code}->{'Doc_NumberOfRows_Regex'};
my $Doc_POST_ID1_Regex = $Config->{$Council_Code}->{'Doc_POST_ID1_Regex'};
my $Doc_Post_Content = $Config->{$Council_Code}->{'Doc_Post_Content'};
my $Doc_Post_URL = $Config->{$Council_Code}->{'Doc_Post_URL'};
my $Doc_Post_Host = $Config->{$Council_Code}->{'Doc_Post_Host'};
my $DOC_PDF_ID_URL = $Config->{$Council_Code}->{'DOC_PDF_ID_URL'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Block_Regex4 = $Config->{$Council_Code}->{'Block_Regex4'};
my $Block_Regex5 = $Config->{$Council_Code}->{'Block_Regex5'};
my $Block_Regex6 = $Config->{$Council_Code}->{'Block_Regex6'};
my $Doc_Published_Date_Regex = $Config->{$Council_Code}->{'Doc_Published_Date_Regex'};
my $Doc_Type_Regex = $Config->{$Council_Code}->{'Doc_Type_Regex'};
my $Doc_File_Format_Index = $Config->{$Council_Code}->{'Doc_File_Format_Index'};
my $Doc_File_Format_Regex = $Config->{$Council_Code}->{'Doc_File_Format_Regex'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Description_Regex = $Config->{$Council_Code}->{'Doc_Description_Regex'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Download_URL_redirct = $Config->{$Council_Code}->{'Doc_Download_URL_redirct'}; #New
my $Doc_Download_URL_redirct_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_Regex'};#New
my $Doc_Download_URL_redirct_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_ABS'};#New
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Current_Page_Number_Regex = $Config->{$Council_Code}->{'Current_Page_Number_Regex'};
my $Next_Page_Post_Content = $Config->{$Council_Code}->{'Next_Page_Post_Content'};
my $Next_Referer = $Config->{$Council_Code}->{'Next_Referer'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Application_Regex = $Config->{$Council_Code}->{'Application_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};
my $Navi1_Info_BlockRegex1 = $Config->{$Council_Code}->{'Navi1_Info_BlockRegex1'};

$Navigate_By_Post1 = 'N/A' if($Navigate_By_Post1 eq '');$Document_Url_Regex = 'N/A' if($Document_Url_Regex eq '');$Primary_Url = 'N/A' if($Primary_Url eq '');$Navi1_Info_Regex1 = 'N/A' if($Navi1_Info_Regex1 eq '');$Navi1_Info_Regex2 = 'N/A' if($Navi1_Info_Regex2 eq '');
$Navi1_Post_Url = 'N/A' if($Navi1_Post_Url eq '');$Navi1_Post_Content = 'N/A' if($Navi1_Post_Content eq '');$Method = 'N/A' if($Method eq '');	$Proxy = 'N/A' if($Proxy eq '');	$Format = 'N/A' if($Format eq '');	$Home_URL = 'N/A' if($Home_URL eq '');	
$Accept_Cookie = 'N/A' if($Accept_Cookie eq '');	$Proceed_Without_GET = 'N/A' if($Proceed_Without_GET eq '');	$Accept_Cookie_URL = 'N/A' if($Accept_Cookie_URL eq '');	$Accept_Post_Content = 'N/A' if($Accept_Post_Content eq '');	
$Accept_Host = 'N/A' if($Accept_Host eq '');	$Host = 'N/A' if($Host eq '');	$Need_Host = 'N/A' if($Need_Host eq '');	$Content_Type = 'N/A' if($Content_Type eq '');	
$Abs_url = 'N/A' if($Abs_url eq '');	$Doc_URL_Regex = 'N/A' if($Doc_URL_Regex eq '');	$Referer = 'N/A' if($Referer eq '');	$Search_URL = 'N/A' if($Search_URL eq '');	$Post_Content = 'N/A' if($Post_Content eq '');	
$Landing_Page_Regex = 'N/A' if($Landing_Page_Regex eq '');	$Landing_Page_Regex2 = 'N/A' if($Landing_Page_Regex2 eq '');$Landing_Page_ABS = 'N/A' if($Landing_Page_ABS eq '');	$Landing_Page_Regex3 = 'N/A' if($Landing_Page_Regex3 eq '');	$Doc_URL_BY = 'N/A' if($Doc_URL_BY eq '');	
$Viewstate_Regex = 'N/A' if($Viewstate_Regex eq '');	$Viewstategenerator_Regex = 'N/A' if($Viewstategenerator_Regex eq '');	$Eventvalidation_Regex = 'N/A' if($Eventvalidation_Regex eq '');	
$CSRFToken_Regex = 'N/A' if($CSRFToken_Regex eq '');	$Doc_Page_Num_Regex = 'N/A' if($Doc_Page_Num_Regex eq '');	$Doc_NumberOfRows_Regex = 'N/A' if($Doc_NumberOfRows_Regex eq '');
$Doc_POST_ID1_Regex = 'N/A' if($Doc_POST_ID1_Regex eq '');	$Doc_Post_Content = 'N/A' if($Doc_Post_Content eq '');	$Doc_Post_URL = 'N/A' if($Doc_Post_URL eq '');	
$Doc_Post_Host = 'N/A' if($Doc_Post_Host eq '');	$DOC_PDF_ID_URL = 'N/A' if($DOC_PDF_ID_URL eq '');	$Block_Regex1 = 'N/A' if($Block_Regex1 eq '');	$Block_Regex2 = 'N/A' if($Block_Regex2 eq '');	
$Block_Regex3 = 'N/A' if($Block_Regex3 eq '');	$Block_Regex4 = 'N/A' if($Block_Regex4 eq '');	$Doc_Published_Date_Index = 'N/A' if($Doc_Published_Date_Index eq '');	
$Doc_Type_Index = 'N/A' if($Doc_Type_Index eq '');	$Doc_Description_Index = 'N/A' if($Doc_Description_Index eq '');	$Doc_Download_URL_Index = 'N/A' if($Doc_Download_URL_Index eq '');	
$Doc_Download_URL_Regex = 'N/A' if($Doc_Download_URL_Regex eq '');	$Doc_Download_URL_Replace = 'N/A' if($Doc_Download_URL_Replace eq '');	$Doc_Download_URL_redirct = 'N/A' if($Doc_Download_URL_redirct eq '');	
$Doc_Download_URL_redirct_Regex = 'N/A' if($Doc_Download_URL_redirct_Regex eq '');	$Doc_Download_URL_redirct_ABS = 'N/A' if($Doc_Download_URL_redirct_ABS eq '');	
$Doc_Download_URL_ABS = 'N/A' if($Doc_Download_URL_ABS eq '');	$Pagination_Type = 'N/A' if($Pagination_Type eq '');	$Total_No_Of_Pages_Regex = 'N/A' if($Total_No_Of_Pages_Regex eq '');	
$Next_Page_Regex = 'N/A' if($Next_Page_Regex eq '');	$Next_Page_Replace_Info = 'N/A' if($Next_Page_Replace_Info eq '');	$Next_Page_Replace_Key = 'N/A' if($Next_Page_Replace_Key eq '');	
$Current_Page_Number_Regex = 'N/A' if($Current_Page_Number_Regex eq '');	$Next_Page_Post_Content = 'N/A' if($Next_Page_Post_Content eq '');	$Next_Referer = 'N/A' if($Next_Referer eq '');	
$Abs_url = 'N/A' if($Abs_url eq '');	$Application_Regex = 'N/A' if($Application_Regex eq '');	$Available_Doc_Count_Regex = 'N/A' if($Available_Doc_Count_Regex eq '');	$Accept_Language = 'N/A' if($Accept_Language eq ''); 
$Doc_File_Format_Index = 'N/A' if($Doc_File_Format_Index eq '');	$Doc_File_Format_Regex = 'N/A' if($Doc_File_Format_Regex eq '');	$Doc_Description_Regex = 'N/A' if($Doc_Description_Regex eq '');	$Doc_Type_Regex = 'N/A' if($Doc_Type_Regex eq '');	
$Doc_Published_Date_Regex = 'N/A' if($Doc_Published_Date_Regex eq '');	$Navi1_Host = 'N/A' if($Navi1_Host eq '');	$Navi1_Info_Need_UriEscape = 'N/A' if($Navi1_Info_Need_UriEscape eq ''); $Landing_Page_ABS_Replace = 'N/A' if($Landing_Page_ABS_Replace eq '');	
$Navi1_Info_Regex3 = 'N/A' if($Navi1_Info_Regex3 eq ''); $Navi1_Info_Regex4 = 'N/A' if($Navi1_Info_Regex4 eq ''); $Navi1_Info_BlockRegex1 = 'N/A' if($Navi1_Info_BlockRegex1 eq ''); $Navi1_Info_Regex5 = 'N/A' if($Navi1_Info_Regex5 eq '');

my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;

my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $Document_Url 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $URL 			= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	print "Format_ID :: $Format_ID \n";

	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code, $Available_Doc_Count);
	my $Landing_Page_Flag=0;
	
	if(lc($Primary_Url) eq 'url')
	{
		if(($Document_Url_Regex ne '') && ($Document_Url_Regex ne 'N/A'))
		{
			my ($Content, $Code, $Redir_Url) = &Download_Utility::Getcontent($URL,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			if($Content=~m/$Document_Url_Regex/is)
			{
				$Document_Url=$1;
				my $u1=URI::URL->new($Document_Url,$URL);
				$Document_Url=$u1->abs;
			}
		}
		else
		{
			$Document_Url=$URL;
		}
	}

	($Doc_Details_Hash_Ref, $Document_Url, $Code,$Available_Doc_Count)=&Extract_Link($Method, $Home_URL, $Document_Url, $URL,\%Nextpage_Replace_Hash, $Application_No);
	$Site_Status = $Code;
	print "Site_Status = $Code\n";
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;

	$Todays_Count = $Todays_Count + $pdfcount;
	if($Available_Doc_Count = ~m/^\s*$/is)
	{
		$Available_Doc_Count = $pdfcount;
	}	
	my $RetrieveMD5=[];
	my $No_Of_Doc_Downloaded=0;
	
	# if(lc($Markup) eq 'large')
	# {
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			$RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			goto DB_Flag;
		}
	# }	

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;
	print "Temp_Download_Location=>$Temp_Download_Location\n";
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====> $Temp_Download_Location\n";
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				sleep(5) if($Council_Code==97);
				my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;

				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
				
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my ($Download_Status);
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					($Download_Status)=&Download_Utility::Url_Status($D_Code);
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}	
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;
					
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query if($D_Code=~m/200/is);
					
				}
			}
		}	
	}	

	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	&Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:
	
	
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
	}	
	# else
	# {
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if(($One_App_Name=~m/^\s*$/is) && ($pdfcount != 0))
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	elsif($pdfcount != 0)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = getdate() where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}		
	}	
	if ($onDemand eq 'ApprovedDecision')
	{
		my $Flag_Update_query = "update GLENIGAN..TBL_APPROVED_LARGE_PROJECT set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\', All_Documents_Downloaded = 'Y' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
		$ApprovedDecisionUpdate_query.= $Flag_Update_query;
	}	
	$pdfcount_inc=1;
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

$Insert_OneAppLog=~s/\,$//igs;
# print "\n$Insert_OneAppLog\n";
&Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\n$Insert_Document\n";
&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
&Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);

if($onDemand eq 'Y')
{
	print "Update & Delete This Ondemand request\n";
	print "Site_Status: $Site_Status\n";
	my $onDemandStatus = &Download_Utility::onDemandStatus($No_Of_Doc_Downloaded_onDemand,$Site_Status);
	my $onDemandUpdateQuery = "update tbl_on_demand_download_backup set Status = \'$onDemandStatus\' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and download_id = \'$onDemandID\'";
	print "onDemandUpdateQuery: $onDemandUpdateQuery\n";
	&Download_DB::Execute($dbh,$onDemandUpdateQuery);

	my $onDemandDeleteQuery = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and status = 'Inprogress'";	
	&Download_DB::Execute($dbh,$onDemandDeleteQuery);
}


sub Extract_Link()
{
	my $Method = shift;
	my $Home_URL = shift;
	my $Document_Url = shift;
	my $URL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $Application_No = shift;

	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my $Scrp_Flag=0;
	my ($Content,$Code,$Redir_Url,$Doc_Details_Hash_Ref,%Doc_Details_Hash,$Previous_page,$Post_Responce,$Post_Res_code,$Available_Doc_Count);
	my ($Landing_Page_Flag,$Landing_Page_Flag2,$Landing_Page_Flag3,$Landing_Page_Flag4)=(0,0,0,0);
	my $Previous_page=0;
	my ($document_type,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code,$Total_No_Of_Pages);
	
	if($Council_Code == 20)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		my $Viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		my $Viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		my $Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		my $Accept_Post_Content_Tmp=$Accept_Post_Content;
		$Accept_Post_Content_Tmp=~s/<VIEWSTATE>/$Viewstate/igs;
		$Accept_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Viewstategenerator/igs;
		$Accept_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Eventvalidation/igs;
		($Post_Responce,$Post_Res_code)=&Download_Utility::Post_Method($Accept_Cookie_URL,$Accept_Host,$Content_Type,$Accept_Cookie_URL,$Accept_Post_Content_Tmp,$Proxy);
		Re_Ping:
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);

		if($Content=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_No_Of_Pages=$1;
		}
		if($Content=~m/$Landing_Page_Regex/is)
		{
			my $DocUrl=$1;
			$DocUrl=~s/\&amp\;/\&/gsi;
			my $u1=URI::URL->new($DocUrl,$Document_Url);
			$Document_Url=$u1->abs;
			$Landing_Page_Flag = 1;
			goto Re_Ping;
		}
		my $current_page=1;
		Next_page20:
		while($Content=~m/$Block_Regex1/igs)
		{
			my $table=$1;
			while($table=~m/$Block_Regex2/igs)
			{
				my $row=$1;
				my @Data;
				my $cnt=0;
				while($row=~m/$Block_Regex3/igs)
				{
					my $Col=$1;
					push(@Data,$Col);
				}
				
				my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
				my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
				my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
				my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
				if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
				{
					$Doc_URL=$Doc_Download_URL_ABS.$1;
				}
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				$pdfcount_inc++;
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
					$Available_Doc_Count++;
				}				
			}	
		}
		if($Next_Page_Regex ne 'N/A')
		{
			if($current_page < $Total_No_Of_Pages )
			{
				if($Content=~m/$Next_Page_Regex/igs)
				{
					my $Next_part1=$1;
					my $Next_part2=$2;
					my $Next_part3=$3;
					$current_page=$Next_part3;
					my $Next_page_Url=$Next_Page_Replace_Info.$Next_part1.$Next_part2.$Next_part3;
					($Content, $Code, $Redir_Url) = &Download_Utility::Getcontent($Next_page_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
					goto Next_page20;
				}
			}	
		}	
	}
	elsif($Council_Code == 34)
	{
		next if($Document_Url!~m/londonlegacy/is);
		
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(HM,">34Home_Content.html");
		# print HM "$Content";
		# close HM;		
		while($Content=~m/$Block_Regex1/igs)
		{
			my $table=$1;
			while($table=~m/$Block_Regex2/igs)
			{
				my $row=$1;
				my @Data;
				my $cnt=0;
				while($row=~m/$Block_Regex3/igs)
				{
					my $Col=$1;
					push(@Data,$Col);
				}
				
				my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
				my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
				my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
				my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
				$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
				if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
				{
					$Doc_URL=$Doc_Download_URL_ABS.$1;
				}
				($Content, $Code, $Redir_Url) = &Download_Utility::Getcontent($Doc_URL,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
				
				if($Content=~m/$Doc_Download_URL_redirct_Regex/is)
				{
					$Doc_URL=$Doc_Download_URL_redirct_ABS.$1;
				}
				print "document_type: $document_type\n";
				print "Doc_Desc: $Doc_Desc\n";
				print "Published_Date: $Published_Date\n\n";
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				$pdfcount_inc++;
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
					$Available_Doc_Count++;
				}				
			}	
		}
	}
	elsif($Council_Code == 27)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(HM,">Home_Content.html");
		# print HM "$Content";
		# close HM;		
		
		while($Content=~m/$Landing_Page_Regex/igs)
		{
			my $Category_Link='http://www2.richmond.gov.uk/PlanData2/'.$1;
			(my $Category_Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Category_Link,$Document_Url,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			# open(HM,">Category_Content.html");
			# print HM "$Category_Content";
			# close HM;		
			# print "Category_Link: $Category_Link";

			while($Category_Content=~m/$Block_Regex1/igs)
			{
				my $table=$1;
				while($table=~m/$Block_Regex2/igs)
				{
					my $row=$1;
					my @Data;
					my $cnt=0;
					while($row=~m/$Block_Regex3/igs)
					{
						my $Col=$1;
						push(@Data,$Col);
					}
					
					my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
					my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
					my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
					my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
					$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
					$document_type=&Download_Utility::Clean($document_type);
					if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
					{
						$Doc_URL=$1;
					}
				
					my ($file_type);
					if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
					{
						$file_type=$1;
					}
					else
					{
						$file_type='pdf';
					}	

					if($Doc_Desc!~m/^\s*$/is)
					{
						
						$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
					}	
					else
					{
						$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
					}
					
					$pdf_name = &Download_Utility::PDF_Name($pdf_name);
					$Doc_URL=decode_entities($Doc_URL);
					# print "Published_Date: $Published_Date\n\n";
					$pdfcount_inc++;
					if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					{
						$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
						$Available_Doc_Count++;
					}				
				}	
			}
		}	
	}
	elsif($Council_Code == 480)
	{
		next if($Document_Url!~m/nnpa\.planning-register\.co\.uk/is);
		
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(HM,">Home_Content.html");
		# print HM "$Content";
		# close HM;		

		while($Content=~m/$Block_Regex1/igs)
		{
			my $table=$1;
			while($table=~m/$Block_Regex2/igs)
			{
				my $row=$1;
				my @Data;
				my $cnt=0;
				while($row=~m/$Block_Regex3/igs)
				{
					my $Col=$1;
					push(@Data,$Col);
				}
				
				my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
				my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
				my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
				my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
				$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
				if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
				{
					$Doc_URL=$Doc_Download_URL_ABS.$1;
				}
				
				# if($Content=~m/$Doc_Download_URL_redirct_Regex/is)
				# {
					# $Doc_URL=$Doc_Download_URL_redirct_ABS.$1;
				# }
				# print "document_type: $document_type\n";
				# print "Doc_Desc: $Doc_Desc\n";
				# print "Published_Date: $Published_Date\n\n";
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				$pdfcount_inc++;
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
					$Available_Doc_Count++;
				}				
			}	
		}
	}
	elsif($Council_Code == 218)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		
		my $Post_Content_tmp=$Post_Content;
		my $Application_No_tmp = uri_escape($Application_No);
		$Post_Content_tmp=~s/<App_number>/$Application_No_tmp/igs;
		
		(my $Category_Content,$Code) = &Download_Utility::Post_Method($URL,$Host,$Content_Type,$Referer,$Post_Content_tmp,$Proxy);	
		
		my @pdf_url;
		if($Category_Content=~m/$Block_Regex1/is)
		{
			my $block1=$1;
			while($block1=~m/$Block_Regex2/igs)
			{
				my $block2=$1;
				while($block2=~m/$Block_Regex3/igs)
				{
					my $url=$1;
					my $name=$2;
					if($url!~m/.pdf$/is)
					{
						$url=$Doc_Download_URL_ABS.$url;
						(my $Page_Content,$Code,my $Redir_Url)=&Download_Utility::Getcontent($url);
						if($Page_Content=~m/$Block_Regex4/is)
						{
							my $block4=$1;
							while($block4=~m/$Block_Regex5/igs)
							{
								my $pdf_url=$1;
								$pdf_url=$Doc_Download_URL_ABS.$pdf_url;
								$pdf_url=join("|",$pdf_url, $name);
								push(@pdf_url,$pdf_url);
							}
						}
					}
					else
					{
						$url=$Doc_Download_URL_ABS.$url;
						$url=join("|",$url, $name);
						push(@pdf_url,$url);
					}
				}
				
				while($block2=~m/$Block_Regex6/igs)
				{
					my $url=$1;
					my $name=$2;
					$url=$Doc_Download_URL_ABS.$url;
					$url=join("|",$url, $name);
					push(@pdf_url,$url);
				}
			}
		}
	
		my ($Doc_Desc,$document_type);
		foreach my $Doc_URL1(@pdf_url)
		{
			($Doc_URL,$pdf_name)=split ('\|',"$Doc_URL1");
			chomp($pdf_name);
			my ($file_type);
			if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
			{
				$file_type=$1;
			}
			else
			{
				$file_type='pdf';
			}	

			$pdf_name = $pdfcount_inc."_".$pdf_name.".".$file_type;
			
			$pdf_name = &Download_Utility::PDF_Name($pdf_name);
			$Doc_URL=decode_entities($Doc_URL);
			
			$pdfcount_inc++;
			if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
			{
				$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
				$Available_Doc_Count++;
			}
		}
	}
	elsif($Council_Code == 251)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		if($Content=~m/$Landing_Page_Regex/is)
		{
			my $DocUrl=$1;
			$DocUrl=~s/\&amp\;/\&/gsi;
			my $u1=URI::URL->new($DocUrl,$Document_Url);
			$Document_Url=$u1->abs;
			$Landing_Page_Flag = 1;
			my $Next_Page_Post_Content_Tmp='{"refType":"PBDC","searchFields":{"ref_no":"'.$Application_No.'"}}';
			my $post_url='http://padocs.lewes-eastbourne.gov.uk/civica/Resource/Civica/Handler.ashx/keyobject/pagedsearch';
			($Content,my $res_code)=&Download_Utility::Post_Method($post_url,$Host,$Content_Type,$Document_Url,$Next_Page_Post_Content_Tmp,$Proxy);
			my $keyNumber;
			if($Content=~m/\"KeyNumber\"\s*\:\s*\"([^>]*?)\"/is)
			{
				$keyNumber=$1;
			}
			my $Next_Page_Post_Content_Tmp='{"KeyNumb":"'.$keyNumber.'","RefType":"PBDC"}';
			my $post_url='http://padocs.lewes-eastbourne.gov.uk/civica/Resource/Civica/Handler.ashx/doc/list';
			($Content,my $res_code)=&Download_Utility::Post_Method($post_url,$Host,$Content_Type,$Document_Url,$Next_Page_Post_Content_Tmp,$Proxy);
		}
		
		my @docDetailsList = split('\},\{',$Content);	
		my $pdfcount_inc = 1;
		for my $docDetail(@docDetailsList)
		{
			my $docNo;
			if($docDetail=~m/\"DocNo\"\s*\:\s*\"([^>]*?)\"\s*,/is)
			{
				$docNo=$1;
			}
			if($docDetail=~m/\"DocDesc\"\s*\:\s*\"([^>]*?)\"\s*,/is)
			{
				$Doc_Desc=$1;
				$Doc_Desc=~s/\(|\)/ /igs;
				$Doc_Desc=~s/^\s+|\s+$//igs;
				$document_type=$Doc_Desc;
			}
			if($docDetail=~m/\"DocDate\"\s*\:\s*\"([^>]*?)\"\s*,/is)
			{
				$Published_Date=$1;
			}
			$pdf_name = $pdfcount_inc."_".$Doc_Desc.".pdf";
			$Doc_URL = 'http://padocs.lewes-eastbourne.gov.uk/civica/Resource/Civica/Handler.ashx/doc/pagestream?DocNo='.$docNo.'&pdf=true'; 
			$pdfcount_inc++;
			$pdf_name = &Download_Utility::PDF_Name($pdf_name);
			$Doc_URL=decode_entities($Doc_URL);
			print "$pdf_name\n";
			if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
			{
				$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
				$Available_Doc_Count++;
			}
		}
	}
	elsif($Council_Code == 414)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(HM,">Home_Content.html");
		# print HM "$Content";
		# close HM;		
		
		my @pdf_url;
		# my $filter_url="http://www.whitehorsedc.gov.uk";
		if($Content=~m/$Block_Regex1/is)
		{
			my $block1=$1;
			while($block1=~m/$Block_Regex2/igs)
			{
				my $Doc_URL=$1;
				my $Doc_Desc=$2;
				$Doc_URL=$Doc_Download_URL_ABS.$Doc_URL;
		
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
					
				$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				# print "pdf_name: $pdf_name\n";
				# print "Doc_URL: $Doc_URL\n";<STDIN>;
				$pdfcount_inc++;
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$Doc_Desc,$pdf_name,$Published_Date];
					$Available_Doc_Count++;
				}
			}
		}
	}
	elsif($Council_Code == 355)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(HM,">Home_Content.html");
		# print HM "$Content";
		# close HM;	
		# while($Content=~m/$Navi1_Info_BlockRegex1/igs)
		# {
			# my $Application_Block=$1;

			# my $x=$1 if($Application_Block=~m/$Navi1_Info_Regex1/is);
			# my $y=$1 if($Application_Block=~m/$Navi1_Info_Regex2/is);
			# my $sf=$1 if($Application_Block=~m/$Navi1_Info_Regex3/is);
			# my $name=$1 if($Application_Block=~m/$Navi1_Info_Regex4/is);
			# my $DBName=$1 if($Application_Block=~m/$Navi1_Info_Regex5/is);
			# $name=~s/\s+/\+/igs;
			my $Post_Content_tmp=$Navi1_Post_Content;
			my $Application_No_tmp = uri_escape($Application_No);
			# $Post_Content_tmp=~s/<X>/$x/igs;
			# $Post_Content_tmp=~s/<Y>/$y/igs;
			# $Post_Content_tmp=~s/<Number>/$sf/igs;
			# $Post_Content_tmp=~s/<Category>/$name/igs;
			$Post_Content_tmp=~s/<App_number>/$Application_No_tmp/igs;
			# $Post_Content_tmp=~s/<DBName>/$DBName/igs;
			# print "Post_Content_tmp:::$Post_Content_tmp\n";
			# my ($Post_Responce,$res_code)=&Download_Utility::Post_Method($Navi1_Post_Url,$Host,$Content_Type,$Document_Url,$Post_Content_tmp,$Proxy);
			# my ($Post_Responce,$res_code)=&Download_Utility::Post_Method($Navi1_Post_Url,$Host,'','https://www.sbcplanning.co.uk/plansearch.php',$Post_Content_tmp,$Proxy);
			
			$pageFetch->add_header( "Accept-Encoding" => "gzip, deflate" );
			$pageFetch->add_header( "Accept-Language" => "en-US,en;q=0.9" );
			$pageFetch->add_header( "Host" => $Host );
			$pageFetch->add_header( "Content-Type" => "application/x-www-form-urlencoded" );
			$pageFetch->post($Navi1_Post_Url, Content => "$Post_Content_tmp");			
			my $searchPageCon = $pageFetch->content;
			my $searchCode= $pageFetch->status();
			
			$Post_Responce = $searchPageCon;
			$Code = $searchCode;
			
			print "Click response: $Code\n";
			# print "Click Post_Responce: $Post_Responce\n";
			if($Post_Responce=~m/$Total_No_Of_Pages_Regex/is)
			{
				my $total_page=$1;
				my $Post_Content_tmp1="&DBName=planapp&Searchfield=Number&st=$Application_No_tmp";
				my $Post_Content_tmp2="Page_No=";
				
				for(my $i=2;$i<=$total_page;$i++)
				{
					my $Post_Content_tmp=$Post_Content_tmp2.$i.$Post_Content_tmp1;
					$pageFetch->post($Navi1_Post_Url, Content => "$Post_Content_tmp");			
					my $searchPageCon = $pageFetch->content;
					my $searchCode= $pageFetch->status();
					$Post_Responce = $Post_Responce.$searchPageCon;
				}
				print "next page is available\n";
			}
			else
			{
				print  "no nextpage\n\n";
			}			
			
			# open(DC,">Post_Responce-355.html");
			# print DC "$Post_Responce";
			# close DC;
			# <STDIN>;
			while($Post_Responce=~m/$Block_Regex1/igs)
			{
				my $table=$1;
				while($table=~m/$Block_Regex2/igs)
				{
					my $row=$1;
					my @Data;
					my $cnt=0;
					while($row=~m/$Block_Regex3/igs)
					{
						my $Col=$1;
						push(@Data,$Col);
					}
					
					my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
					my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
					my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
					my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
					$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
					$document_type=&Download_Utility::Clean($document_type);
					# print "PDF_URL_Cont: $PDF_URL_Cont\n\n";
					if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
					{
						$Doc_URL=$Doc_Download_URL_ABS.$1;
						$Doc_URL=~s/\#[^>]*?$//igs;
					}
				
					# print "Doc_URL: $Doc_URL\n\n";
					my ($file_type);
					if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
					{
						$file_type=$1;
					}
					else
					{
						$file_type='pdf';
					}	

					if($Doc_Desc!~m/^\s*$/is)
					{
						
						$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
					}	
					else
					{
						$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
					}
					
					$pdf_name = &Download_Utility::PDF_Name($pdf_name);
					$Doc_URL=decode_entities($Doc_URL);
					print "Pdf_name: $pdf_name\n";
					print "Doc_URL: $Doc_URL\n\n";
					print "document_type: $document_type\n\n";
					print "Published_Date: $Published_Date\n";
					$pdfcount_inc++;
					if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
					{
						$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
						$Available_Doc_Count++;
					}				
				}	
			}			
		# }
	}
	elsif($Council_Code == 287)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(HM,">Home_Content287.html");
		# print HM "$Content";
		# close HM;

		if($Content=~m/$Landing_Page_Regex/is)
		{
			$Document_Url=$1;
			print "Document_Url:::: $Document_Url\n";
			($Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		}
		
		while($Content=~m/$Block_Regex1/igs)
		{
			my $table=$1;
			while($table=~m/$Block_Regex2/igs)
			{
				my $row=$1;
				my @Data;
				my $cnt=0;
				while($row=~m/$Block_Regex3/igs)
				{
					my $Col=$1;
					push(@Data,$Col);
				}
				
				my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
				my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
				my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
				my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
				$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
				$document_type=&Download_Utility::Clean($document_type);
				
				if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
				{
					$Doc_URL=$1;
				}
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				elsif($Doc_Desc!~m/\.([\w]{3,4})\s*$/is)
				{
					(my $SubContent, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Doc_URL,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
					$Doc_URL='';
						while($SubContent=~m/$Block_Regex1/igs)
						{
							my $table=$1;
							while($table=~m/(<li[^>]*?>\s*[\w\W]*?\s*<\/li>)/igs)
							{
								my $row=$1;
								my @Data;
								my $cnt=0;
								while($row=~m/<li[^>]*?>\s*([\w\W]*?)\s*<\/li>/igs)
								{
									my $Col=$1;
									push(@Data,$Col);
								}
								
								my $PDF_URL_Cont = &Download_Utility::trim($Data[0]);
								my $document_type = &Download_Utility::trim($Data[0]);
								my $Doc_Desc = &Download_Utility::trim($Data[0]);
								my $Published_Date = &Download_Utility::trim($Data[0]);
								$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
								$document_type=&Download_Utility::Clean($document_type);
								
								if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
								{
									$Doc_URL=$1;
								}
								my ($file_type);
								if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
								{
									$file_type=$1;
								}
								else
								{
									$file_type='pdf';
								}	
								
								if($Doc_Desc!~m/^\s*$/is)
								{
									
									$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
								}	
								else
								{
									$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
								}
								
								$pdf_name = &Download_Utility::PDF_Name($pdf_name);
								$Doc_URL=decode_entities($Doc_URL);
								print "Pdf_name: $pdf_name\n";
								print "Doc_URL: $Doc_URL\n\n";
								$pdfcount_inc++;
								if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
								{
									$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
									$Available_Doc_Count++;
								}				
							}	
						}						
				}	
				else
				{
					$file_type='pdf';
				}	
				if($Doc_Desc!~m/^\s*$/is)
				{
					
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				print "Pdf_name: $pdf_name\n";
				print "Doc_URL: $Doc_URL\n\n";
				print "document_type: $document_type\n\n";
				print "Published_Date: $Published_Date\n";
				$pdfcount_inc++;
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
					$Available_Doc_Count++;
				}				
			}	
		}			
	}
	elsif($Council_Code==477)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(DC,">477Content.html");
		# print DC $Content;
		# close DC;
		my $url2=$1 if($Content=~m/>\s*Parish\s*\:\s*<\/div>\s*<[^>]*?>\s*([^>]*?)\s*</is);
		my ($v1,$v2,$v3,$url3);
		if($Application_No=~m/^\s*([a-zA-Z]{1})\/([^>]*?)\/([^>]*?)(?:\/[^>]*?)?$/is)
		{
			$v1=$1.$2;
			$v2=$3;
			# print "v1::$v1\n\n";
			# print "v2::$v2\n\n";
		}
		$v3=$1 if($v2=~m/^\s*([\d]{3})[a-zA-Z]{1}$/is);
		$v3='0'.$1 if($v2=~m/^\s*([\d]{2})[a-zA-Z]{1}$/is);
		$v3='00'.$1 if($v2=~m/^\s*([\d]{1})[a-zA-Z]{1}$/is);
		$v3=$1 if($v2=~m/^\s*([\d]{3})$/is);
		$v3='0'.$1 if($v2=~m/^\s*([\d]{2})$/is);
		$v3='00'.$1 if($v2=~m/^\s*([\d]{1})$/is);
		# print "v3::$v3\n\n";
		$url3=$v1.'/'.$v3.'/';
		my $pdf_key=$Application_No;
		$pdf_key=~s/\//_/igs;
		# $url3=~s/\/\/$/\//igs;
		# print "pdf_key::$pdf_key\n\n";
		# print "url2::$url2\n\n";
		# print "url3::$url3\n\n";
		# https://pacsplanning.yorkshiredales.org.uk/Sedbergh/S03/209
		my $url1='https://pacsplanning.yorkshiredales.org.uk/'.$url2.'%20'.$url3;
		# print "Method:\t$Method. But Document URL not found for this App ID\n";
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($url1,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(DC,">477Content1.html");
		# print DC $Content;
		# close DC;
		# exit;
		$Doc_Download_URL_Regex=~s/<App_Num>/$pdf_key/igs;
		
		while($Content=~m/$Doc_Download_URL_Regex/igs)
		{
			my $Doc_URL=$Doc_Download_URL_ABS.$1;
			my $pdf_name=$2;	
			$Doc_URL=~s/\#[^>]*?$//igs;
			# print "Doc_URL: $Doc_URL\n\n";
						
			$pdf_name = &Download_Utility::PDF_Name($pdf_name);
			$Doc_URL=decode_entities($Doc_URL);
			print "Pdf_name: $pdf_name\n";
			print "Doc_URL: $Doc_URL\n\n";
			# print "document_type: $document_type\n\n";
			# print "Published_Date: $Published_Date\n";
			my $Published_Date;
			my $document_type;
			# $pdfcount_inc++;
			if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
			{
				$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
				$Available_Doc_Count++;
			}				
		}	
		
	}
	elsif($Council_Code==474)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(DC,">474Content.html");
		# print DC $Content;
		# close DC;
		
		my $OBJREF=uri_escape($1) if($Content=~m/var\s*objectref\s*=\s*\"\s*([^>]*?)\s*\"/is);
		my $csrf_token=uri_escape($1) if($Content=~m/name=\"csrf-token\"\s*content\=\"\s*([^>]*?)\s*\"/is);
		
		my $Post_Content_tmp=$Navi1_Post_Content;
		$Post_Content_tmp=~s/<OBJREF>/$OBJREF/igs;
		print "OBJREF:::$OBJREF\n\n";
		print "csrf_token:::$csrf_token\n\n";
		print "Post_Content_tmp:::$Post_Content_tmp\n\n";
		$pageFetch->add_header( "Accept-Encoding" => "gzip, deflate" );
		$pageFetch->add_header( "Accept-Language" => "en-US,en;q=0.9" );
		$pageFetch->add_header( "Referer" => "$Referer" );
		$pageFetch->add_header( "Accept" => '*/*' );
		$pageFetch->add_header( "Host" => $Host );
		$pageFetch->add_header( "Content-Type" => "application/x-www-form-urlencoded; charset=UTF-8" );
		# $pageFetch->add_header( "Cookie" => "ASP.NET_SessionId=ecbgd5p13a3wdq0mqjhaemdp" );
		# Cookie	ASP.NET_SessionId=ecbgd5p13a3wdq0mqjhaemdp
		# $pageFetch->add_header( "Origin" => "http://tdccomweb.tandridge.gov.uk" );
		# $pageFetch->add_header( "X-CSRF-TOKEN" => $csrf_token );
		$pageFetch->add_header( "X-Requested-With" => 'XMLHttpRequest' );

		$pageFetch->post($Navi1_Post_Url, Content => "$Post_Content_tmp");			
		my $searchPageCon = $pageFetch->content;
		my $searchCode= $pageFetch->status();
		$Post_Responce = $searchPageCon;
		$Code = $searchCode;
		
		# print "Click Post_Responce: $Post_Responce\n";
		print "Click response: $Code\n";
		my($Post_Responce,$res_code)=&Download_Utility::Post_Method($Navi1_Post_Url,$Host,'application/x-www-form-urlencoded; charset=UTF-8',$Referer,$Post_Content_tmp,$Proxy);
		
		# print "Click Post_Responce: $Post_Responce\n";
		# open(DC,">Post_Responce-474.html");
		# print DC "$Post_Responce";
		# close DC;
		# <STDIN>;
		
	}
	elsif($Council_Code==97)
	{
		(my $Content, $Code, my $Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		# open(DC,">97Content.html");
		# print DC $Content;
		# close DC;
		
		(my $Content, $Code, $Redir_Url) = &Download_Utility::Getcontent($Navi1_Post_Url,$URL,$Accept_Host,$Content_Type,$Document_Url,$Proxy,$Need_Host,$Accept_Language);
		# open(DC,">97Content1.html");
		# print DC $Content;
		# close DC;
		my $Content1=$Content;
		my($Doc_eventarg,$Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation);
		home:
		if($Content1=~m/$Next_Page_Regex/is)
		{
			$Doc_eventarg=uri_escape($1);
			
			if($Viewstate_Regex ne 'N/A')
			{
				$Doc_viewstate=uri_escape($1) if($Content1=~m/$Viewstate_Regex/is);
			}	
			if($Viewstategenerator_Regex ne 'N/A')
			{
				$Doc_viewstategenerator=$1 if($Content1=~m/$Viewstategenerator_Regex/is);
			}		
			if($Eventvalidation_Regex ne 'N/A')
			{
				$Doc_Eventvalidation=uri_escape($1) if($Content1=~m/$Eventvalidation_Regex/is);
			}	
			my $Post_Content_tmp='__EVENTTARGET='.$Doc_eventarg.'&__EVENTARGUMENT=&__VIEWSTATE='.$Doc_viewstate.'&__VIEWSTATEGENERATOR='.$Doc_viewstategenerator.'&__EVENTVALIDATION='.$Doc_Eventvalidation.'&RadGrid1_ClientState=';
			# print "Doc_eventarg::$Doc_eventarg\n";
			# print "Doc_viewstate::$Doc_viewstate\n";
			# print "Doc_viewstategenerator::$Doc_viewstategenerator\n";
			# print "Doc_Eventvalidation::$Doc_Eventvalidation\n";
			# print "Post_Content_tmp::$Post_Content_tmp\n";
			my ($Post_Responce,$res_code)=&Download_Utility::Post_Method($Navi1_Post_Url,$Host,'application/x-www-form-urlencoded',$Navi1_Post_Url,$Post_Content_tmp,$Proxy);
			# my($Post_Responce,$res_code)=&Download_Utility::Post_Method($post_Url2,'planningdocs.wigan.gov.uk','application/json; charset=utf-8',$Referer,$Post_Content_Tmp1,$Proxy);
			# open(DC,">97Content1next1.html");
			# print DC $Post_Responce;
			# close DC;
			# <STDIN>;
			$Content=$Content.$Post_Responce;
			$Content1=$Post_Responce;
			goto home;
			
		}
		while($Content=~m/$Block_Regex1/igs)
		{
			my $table=$1;
			# print "table::$table\n\n";
			while($table=~m/$Block_Regex2/igs)
			{
				my $row=$1;
				my @Data;
				my $cnt=0;
				while($row=~m/$Block_Regex3/igs)
				{
					my $Col=$1;
					push(@Data,$Col);
				}
				
				my $PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
				my $document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
				my $Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
				my $Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
				$Doc_Desc=&Download_Utility::Clean($Doc_Desc);
				$document_type=&Download_Utility::Clean($document_type);
				# print "PDF_URL_Cont: $PDF_URL_Cont\n\n";
				# if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
				if($PDF_URL_Cont=~m/([\d]+)$/is)
				{
					# $Doc_URL=$Doc_Download_URL_ABS.$1;
					$Doc_URL='https://www1.arun.gov.uk/PublicViewer/Authenticated/FrameImage.aspx?pg_id='.$1;
					$Doc_URL=~s/\#[^>]*?$//igs;
				}
			
				# print "Doc_URL: $Doc_URL\n\n";
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	

				if($Doc_Desc!~m/^\s*$/is)
				{
					
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
				$pdf_name = &Download_Utility::PDF_Name($pdf_name);
				$Doc_URL=decode_entities($Doc_URL);
				print "Pdf_name: $pdf_name\n";
				print "Doc_URL: $Doc_URL\n\n";
				print "document_type: $document_type\n\n";
				print "Published_Date: $Published_Date\n";
				$pdfcount_inc++;
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date];
					$Available_Doc_Count++;
				}				
			}	
		}
		
	}
	return (\%Doc_Details,$Document_Url,$Code,$Available_Doc_Count);
}

sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $Doc_URL_BY=shift;
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;

	$Content=~s/<td\/>/<td><\/td>/igs;

	if($Block_Regex1 ne 'N/A')
	{
		my @Block1 = $Content =~m/$Block_Regex1/igs;
		if($Block_Regex2 ne 'N/A')
		{
			for my $Block1(@Block1)
			{	
				my @Block2 = $Block1=~m/$Block_Regex2/igs;
				if($Block_Regex3 ne 'N/A')
				{
					for my $Block2(@Block2)
					{	
						my @Block3 = $Block2=~m/$Block_Regex3/igs;
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$URL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
				else
				{
					my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$URL,$Content);
					my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
					%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
				}	
			}
		}	
		else
		{
			my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$URL,$Content);
			my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
			%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
		}	
	}	
	return \%Doc_Details_Hash;
}

sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	my ($document_type,$Doc_File_Format_Cont,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}

	# my $cnt=0;
	# for my $d(@Data)
	# {
		# print "$cnt: $d\n";
		# $cnt++;
	# }
	$Doc_File_Format_Cont = &Download_Utility::trim($Data[$Doc_File_Format_Index]) if($Doc_File_Format_Index ne 'N/A');
	$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
	# print "PDF_URL_Cont\n$PDF_URL_Cont\n";
	if($Doc_Published_Date_Regex ne 'N/A')
	{
		if($Published_Date=~m/$Doc_Published_Date_Regex/is)
		{
			$Published_Date=$1;
		}
	}
	if($Doc_Type_Regex ne 'N/A')
	{
		if($document_type=~m/$Doc_Type_Regex/is)
		{
			$document_type=$1;
		}
	}
	if($Doc_Description_Regex ne 'N/A')
	{
		if($Doc_Desc=~m/$Doc_Description_Regex/is)
		{
			$Doc_Desc=$1;
		}
	}
	if($Doc_URL_BY eq 'POST')
	{
		my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$Doc_POST_ID2,$Doc_POST_ID3,$CSRFToken);
		my $Doc_Post_Content_Tmp=$Doc_Post_Content;
		if($Viewstate_Regex ne 'N/A')
		{
			$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		}	
		if($Viewstategenerator_Regex ne 'N/A')
		{
			$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		}	
		if($CSRFToken_Regex ne 'N/A')
		{
			$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
		}	
		if($Eventvalidation_Regex ne 'N/A')
		{
			$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		}	
		if($Doc_Page_Num_Regex ne 'N/A')
		{
			$Doc_Page_Num=$1 if($Content=~m/$Doc_Page_Num_Regex/is);
		}	
		if($Doc_NumberOfRows_Regex ne 'N/A')
		{
			$Doc_NumberOfRows=$1 if($Content=~m/$Doc_NumberOfRows_Regex/is);
		}	
		if($PDF_URL_Cont=~m/$Doc_POST_ID1_Regex/is)
		{
			$Doc_POST_ID1=uri_escape($1);
			$Doc_POST_ID2=uri_escape($2);
			$Doc_POST_ID3=uri_escape($3);
		}

		$Doc_Post_URL = $Document_Url if($Doc_Post_URL=~m/Document_Url/is);
		$Referer = $Document_Url if($Referer=~m/Document_Url/is);
		$Doc_Post_URL=decode_entities($Doc_Post_URL);
		$Referer=decode_entities($Referer);
		$Doc_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
		$Doc_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
		$Doc_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
		$Doc_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
		$Doc_Post_Content_Tmp=~s/<PageNum>/$Doc_Page_Num/igs;
		$Doc_Post_Content_Tmp=~s/<PageRows>/$Doc_NumberOfRows/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID1>/$Doc_POST_ID1/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID2>/$Doc_POST_ID2/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID3>/$Doc_POST_ID3/igs;
	
		$Doc_Post_Host=$Host if($Doc_Post_Host eq '');
		
		($Post_Responce,$res_code)=&Download_Utility::Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Referer,$Doc_Post_Content_Tmp,$Proxy);
		
		my $fh = FileHandle->new("Post_Responce.html",'w');
		binmode($fh);
		$fh->print($Post_Responce);
		$fh->close();
		# <stdin>;
		my $URL_ID;
		if($Post_Responce=~m/$Doc_Download_URL_Regex/is)
		{
			my $val=$1;
			if($DOC_PDF_ID_URL=~m/ID/is)
			{
				$URL_ID=$val;
			}
			elsif($DOC_PDF_ID_URL=~m/URL/is)
			{
				my $u1=URI::URL->new($val,$Doc_Post_URL);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
			}
		}
		if($Doc_Download_URL_Replace ne 'N/A')
		{
			# next if($PDF_URL_Cont == '');
			$Doc_URL = $Doc_Download_URL_Replace;
			$Doc_URL=~s/<SelectedID>/$URL_ID/igs;
		}
		# exit;

	}
	elsif($Doc_Download_URL_Regex ne 'N/A')
	{
		print "\nOK\n";
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl1=$1;
			my $Durl2=$2;
			my $Durl3=$3;
			# print "Durl1: $Durl1\n";
			if($Doc_Download_URL_Replace ne 'N/A') 
			{
				print "Doc_Download_URL_Replace: $Doc_Download_URL_Replace\n";
				if($Doc_Download_URL_Replace=~m/Redir_Url/is)
				{
					my $Doc_Download_URL_Replace_tmp=$Redir_Url;
					my @Replace_URL_Split=split('\|',$Doc_Download_URL_Replace);
					$Doc_Download_URL_Replace_tmp=~s/$Replace_URL_Split[1]/$Durl1/igs;
					$Doc_URL=$Doc_Download_URL_Replace_tmp;
					print "Doc_URL: $Doc_URL";
				}	
				else
				{
					my $Doc_Download_URL_Replace_tmp=$Doc_Download_URL_Replace;
					$Doc_Download_URL_Replace_tmp=~s/<SelectedID1>/$Durl1/igs;
					$Doc_Download_URL_Replace_tmp=~s/<SelectedID2>/$Durl2/igs;
					$Doc_Download_URL_Replace_tmp=~s/<SelectedID3>/$Durl3/igs;
					$Doc_URL=$Doc_Download_URL_Replace_tmp;
				}	
			}
			elsif($Doc_Download_URL_ABS ne 'N/A') 
			{
				print "Doc_Download_URL_ABS: $Doc_Download_URL_ABS\n";
				$Doc_URL=$Doc_Download_URL_ABS.$Durl1;
				print "Doc_URL1: $Doc_URL\n";
			}
			else
			{
				my $u1=URI::URL->new($Durl1,$Root_Url);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
				print "Doc_URL2: $Doc_URL\n";
			}	
		}
		if($Doc_Download_URL_redirct eq 'Y')
		{
			my ($Redir_Content, $code, $reurl)=&Download_Utility::Getcontent($Doc_URL,"","","","",$Proxy,$Need_Host,$Accept_Language);
			if($Redir_Content=~m/$Doc_Download_URL_redirct_Regex/is)
			{
				my $Durl=$1;
				if($Doc_Download_URL_redirct_ABS ne 'N/A')
				{
					$Doc_URL=$Doc_Download_URL_redirct_ABS.$Durl;
				}
				else
				{
					my $u1=URI::URL->new($Durl,$Doc_URL);
					my $u2=$u1->abs;
					$Doc_URL=$u2;
				}	
			}
		}		
	}
	elsif($Doc_Download_URL_Replace ne 'N/A')
	{
		next if($PDF_URL_Cont == '');
		$Doc_URL = $Doc_Download_URL_Replace;
		$Doc_URL=~s/<SelectedID>/$PDF_URL_Cont/igs;
	}

	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	my ($file_type);
	if($Doc_File_Format_Regex ne 'N/A')
	{
		if($Doc_File_Format_Cont=~m/$Doc_File_Format_Regex/is)
		{
			$file_type=$1;
		}
	}
	elsif($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	$Doc_URL=decode_entities($Doc_URL);
	$pdfcount_inc++;
	print "\nDoc_URL\t\t: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date	: $Published_Date\n";

	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	return (\%Doc_Details);
}	

sub Search_Application()
{
	my $Application_No = uri_escape(shift);
	my ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Home_URL,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
	
	open(DC,">Home_Content.html");
	print DC $Content;
	close DC;
	
	my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$CSRFToken);
	if($Viewstate_Regex ne 'N/A')
	{
		$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
	}	
	if($Viewstategenerator_Regex ne 'N/A')
	{
		$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
	}	
	if($CSRFToken_Regex ne 'N/A')
	{
		$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
	}	
	if($Eventvalidation_Regex ne 'N/A')
	{
		$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
	}	

	my $Post_Content_Tmp=$Post_Content;
	$Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
	$Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
	$Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
	$Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
	$Post_Content_Tmp=~s/<APP_NUMBER>/$Application_No/igs;

	$Search_URL = $Home_URL if($Search_URL eq 'N/A');
	print "Host: $Host\n";
	my($Post_Responce,$res_code)=&Download_Utility::Post_Method($Search_URL,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy);
	
	open(DC,">Search_Post_Responce.html");
	print DC "$Post_Responce";
	close DC;

	if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
	{
		if($Post_Responce=~m/$Landing_Page_Regex/is)
		{
			my $DocUrl=$Abs_url.$1;
			print "Document_Url:::: $DocUrl\n";
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			$Document_Url=decode_entities($Document_Url);
			# print "Document_Url:::: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			open(DC,">Landing_Content.html");
			print DC $Content;
			close DC;
		}
		elsif($Post_Responce=~m/$Application_Regex/is)
		{
			my $DocUrl=$Abs_url.$1;
			print "Document_Url:::: $DocUrl\n";
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			$Document_Url=decode_entities($Document_Url);
			# print "Document_Url:::: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			open(DC,">Application_Content.html");
			print DC $Content;
			close DC;
		}
	}	
	else
	{
		$Content=$Post_Responce;
	}

	if($Application_Regex ne 'N/A')
	{
		if($Content=~m/$Application_Regex/is)
		{
			$Document_Url = $1;
			my $u1=URI::URL->new($Document_Url, $Home_URL);
			$Document_Url=$u1->abs;			
			print "Document_Url: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			open(DC,">Doc_Page_Content.html");
			print DC $Content;
			close DC;
		}
	}
	else
	{
		$Content=$Post_Responce;
	}
	
	
	return($Content,$res_code);
}