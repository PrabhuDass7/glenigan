use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use Cwd qw(abs_path);
use File::Basename;
use JSON;


# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 


my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];
print "onDemand: $onDemand\n";

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');


#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);


#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."/Documents";

my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	

my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";
	
# &Download_DB::Execute($dbh,$Dashboard_Insert_Query);


my ($Update_Query);

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details_Format8.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Host1 = $Config->{$Council_Code}->{'Host1'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $FILTER_URL1 = $Config->{$Council_Code}->{'FILTER_URL1'};
my $FILTER_URL2 = $Config->{$Council_Code}->{'FILTER_URL2'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_NUMBER1 = $Config->{$Council_Code}->{'FORM_NUMBER1'};
my $FORM_REFERENECE = $Config->{$Council_Code}->{'FORM_REFERENECE'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Docs_URL_Regex = $Config->{$Council_Code}->{'Docs_URL_Regex'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_Filter_URL = $Config->{$Council_Code}->{'Doc_Download_Filter_URL'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Redir_Url = $Config->{$Council_Code}->{'Redir_Url'};
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $All_Page_Name = $Config->{$Council_Code}->{'All_Page_Name'};
my $All_Page_Name_Value = $Config->{$Council_Code}->{'All_Page_Name_Value'};
my $Doc_URL_BY_Regex = $Config->{$Council_Code}->{'Doc_URL_BY_Regex'};
my $Accept_Name = $Config->{$Council_Code}->{'Accept_Name'};
my $Accept_Value = $Config->{$Council_Code}->{'Accept_Value'};
my $Search_Type = $Config->{$Council_Code}->{'Search_Type'};
my $Search_POST = $Config->{$Council_Code}->{'Search_POST'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};


$Method = "N/A" if($Method eq ""); $Format = "N/A" if($Format eq ""); $Home_URL = "N/A" if($Home_URL eq ""); $Host = "N/A" if($Host eq ""); $Host1 = "N/A" if($Host1 eq ""); $FILTER_URL = "N/A" if($FILTER_URL eq ""); $FILTER_URL1 = "N/A" if($FILTER_URL1 eq ""); $FILTER_URL2 = "N/A" if($FILTER_URL2 eq ""); $Content_Type = "N/A" if($Content_Type eq ""); $FORM_NUMBER = "N/A" if($FORM_NUMBER eq ""); $FORM_NUMBER1 = "N/A" if($FORM_NUMBER1 eq ""); $FORM_REFERENECE = "N/A" if($FORM_REFERENECE eq ""); $Referer = "N/A" if($Referer eq ""); $POST_URL = "N/A" if($POST_URL eq ""); $Doc_URL_Regex = "N/A" if($Doc_URL_Regex eq ""); $Docs_URL_Regex = "N/A" if($Docs_URL_Regex eq "");
$Landing_Page_Regex = "N/A" if($Landing_Page_Regex eq ""); $Viewstate_Regex = "N/A" if($Viewstate_Regex eq ""); $Viewstategenerator_Regex = "N/A" if($Viewstategenerator_Regex eq ""); $Eventvalidation_Regex = "N/A" if($Eventvalidation_Regex eq ""); $Block_Regex1 = "N/A" if($Block_Regex1 eq ""); $Block_Regex2 = "N/A" if($Block_Regex2 eq ""); $Block_Regex3 = "N/A" if($Block_Regex3 eq ""); $Doc_Published_Date_Index = "N/A" if($Doc_Published_Date_Index eq ""); $Doc_Download_URL_Replace = "N/A" if($Doc_Download_URL_Replace eq ""); $Doc_Type_Index = "N/A" if($Doc_Type_Index eq ""); $Doc_Description_Index = "N/A" if($Doc_Description_Index eq "");
$Doc_Download_URL_Index = "N/A" if($Doc_Download_URL_Index eq ""); $Doc_Download_URL_Regex = "N/A" if($Doc_Download_URL_Regex eq ""); $Doc_Download_Filter_URL = "N/A" if($Doc_Download_Filter_URL eq ""); $Total_No_Of_Pages_Regex = "N/A" if($Total_No_Of_Pages_Regex eq ""); $Available_Doc_Count_Regex = "N/A" if($Available_Doc_Count_Regex eq ""); $Doc_URL_BY = "N/A" if($Doc_URL_BY eq ""); $Pagination_Type = "N/A" if($Pagination_Type eq ""); $Next_Page_Regex = "N/A" if($Next_Page_Regex eq ""); $Next_Page_Replace_Info = "N/A" if($Next_Page_Replace_Info eq ""); $Next_Page_Replace_Key = "N/A" if($Next_Page_Replace_Key eq "");
$Redir_Url = "N/A" if($Redir_Url eq ""); $Doc_Download_URL_ABS = "N/A" if($Doc_Download_URL_ABS eq ""); $SSL_VERIFICATION = "N/A" if($SSL_VERIFICATION eq ""); $All_Page_Name = "N/A" if($All_Page_Name eq ""); $All_Page_Name_Value = "N/A" if($All_Page_Name_Value eq ""); $Doc_URL_BY_Regex = "N/A" if($Doc_URL_BY_Regex eq ""); $Accept_Name = "N/A" if($Accept_Name eq ""); $Accept_Value = "N/A" if($Accept_Value eq ""); $Search_Type = "N/A" if($Search_Type eq ""); $Search_POST = "N/A" if($Search_POST eq ""); $Post_Content = "N/A" if($Post_Content eq "");


my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;


my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my $reccount = @{$Format_ID};
print "COUNCIL :: <$Council_Code>\n";
# print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	# print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_ID[$reccnt];
	my $Source 			= @$Source[$reccnt];
	my $Application_No 	= @$Application_No[$reccnt];
	my $documentURL 	= @$Document_Url[$reccnt];
	my $Markup 			= @$Markup[$reccnt];
	my $ApplicationURL 	= @$URL[$reccnt];
	my $PROJECT_STATUS 	= @$PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= @$PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= @$COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS[$reccnt];
	
	print "Format_ID :: $Format_ID \n";
	
	my $Temp_Download_Location = $Local_Download_Location."/$Format_ID";
	
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====>$Temp_Download_Location\n";
	
	# if($Council_Code=~m/^(342)$/is)
	# {
		# $documentURL="";
		# print "documentURL<==>$documentURL\n"; 
	# }
	
	my ($docPageContent,$Code,$Available_Doc_Count);
	my $Landing_Page_Flag=0;
	
	Re_Ping:	
	if($documentURL ne '')
	{
		if($Method=~m/GET/is)
		{
			($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
		}
		elsif($Method=~m/POST/is)
		{
			($docPageContent,$Code) = &Download_Utility::docPostMechMethod($documentURL,$Post_Content,$pageFetch);	
		}
		
	}
	else
	{
		print "Search Application method id processing..(Document URL not found)\n";
		($docPageContent,$Code)=&Search_Application($Application_No,$ApplicationURL);
	}	
	
	# open(DC,">Home_Content.html");
	# print DC $docPageContent;
	# close DC;
	# exit;
	
	# <STDIN>;
	
		
	if($docPageContent=~m/<[^>]*?=\"$Accept_Name\"[^>]*?>I\s*agree\s*to\s*this\s*statement\s*<\//is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# <STDIN>;		
	}
	elsif($docPageContent=~m/>\s*I\s*agree\s*<\//is)
	{
		my $appNo = $1 if($docPageContent=~m/<h3>Downloads\s*for\s*this\s*application<\/h3>\s*<form[^>]*?planningApplication\/([^\"]*?)\"[^>]*?>/is);
		$POST_URL=~s/<APPNUM>/$appNo/gsi;
		
		($docPageContent,$Code) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$POST_URL=~s/$appNo/<APPNUM>/gsi;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# exit;
	}
	elsif($docPageContent=~m/By\s*clicking\s*the\s*button\s*below\s*you\s*agree\s*to\s*accept\s*these\s*terms\s*and\s*conditions/is)
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $Accept_Name => $Accept_Value );
		$pageFetch->click();
		
		# print "Welcome\n";
				
		$docPageContent = $pageFetch->content;
		$Code= $pageFetch->status();
	
		$Landing_Page_Flag = 1;
		
		# open(DC,">Home_Content.html");
		# print DC $docPageContent;
		# close DC;
		# exit;
	}
	elsif($Council_Code=~m/^615$/is)
	{
		if($docPageContent=~m/<form action=\"(\/Disclaimer\/Accept[^\"]*?)\"\s*method=\"post\">[\w\W]*?<input[^>]*?value=\"Accept\"/is)
		{
			my $acceptURL = $1;
			$acceptURL=~s/\&amp\;/\&/gsi;
			my $u1=URI::URL->new($acceptURL,$Home_URL);
			$documentURL=$u1->abs;
			
			($docPageContent,$Code) = &Download_Utility::docMechMethod($documentURL,$pageFetch);
		}
	}
	
	my %Doc_Details;
	if($Landing_Page_Flag == 0)
	{
		if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
		{
			print "Application Number==>$Application_No\n";
			
			if($docPageContent=~m/$Landing_Page_Regex/is)
			{
				my $DocUrl=$1;
				$DocUrl=~s/\&amp\;/\&/gsi;
				
				my $u1=URI::URL->new($DocUrl,$documentURL);
				$documentURL=$u1->abs;
				
				$documentURL = $FILTER_URL.$documentURL if($documentURL!~m/^http/is);
				print "Document_Url: $documentURL\n";
				# <STDIN>;
				
				$Landing_Page_Flag = 1;
				goto Re_Ping;
			}	
		}
	}
		
	open(DC,">Home_Content.html");
	print DC $docPageContent;
	close DC;
	exit;	
	
	my ($Total_no_Pages);
	if($Total_No_Of_Pages_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_no_Pages=$1;
		}
		print "Total_no_Pages: $Total_no_Pages\n";
	}

	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($docPageContent=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
		
		print "Available_Doc_Count<==>$Available_Doc_Count\n"; 
		# <STDIN>;	
	}	
	
	if($Council_Code=~m/^342$/is)
	{
		my $NXT_BTN_NME = $1 if($docPageContent=~m/<select[^>]*?PageSize\"\s*name=\"([^\"]*?)\"[^>]*?>/is);
		my $NXT_BTN_VAL = $1 if($docPageContent=~m/<option[^>]*?value=\"([^\"]*?)\">\s*All\s*<\/option>/is);
		
		$pageFetch-> submit_form(
				form_number => $FORM_NUMBER,
				fields      => {
					$NXT_BTN_NME	=> $NXT_BTN_VAL,
					},
				);
		
		
		my $Page_url = $pageFetch->uri();		
		my $searchPageCon = $pageFetch->content;
		my $searchCode= $pageFetch->status();
		
		# open(DC,">Search_Content.html");
		# print DC $searchPageCon;
		# close DC;
		# exit;
		
		$docPageContent = $searchPageCon;
	}
			
	my $page_inc=1;	
	my ($Doc_URL,$PDF_URL_Cont,$document_type,$Doc_Desc,$Published_Date,$pdf_name);
	my %Doc_Details;
	Next_Page:
	if($Format eq 'Table')
	{
		# $docPageContent=~s/<td\/>/<td><\/td>/igs;
		my %Doc_Details_Hash;
		if($Block_Regex1 ne 'N/A')
		{
			while($docPageContent=~m/$Block_Regex1/igs)
			{
				my $tableTag = $1;
				if($Block_Regex2 ne 'N/A')
				{
					while($tableTag=~m/$Block_Regex2/igs)
					{
						my $trTag = $1;
						if($Block_Regex3 ne 'N/A')
						{
							my @tdTag = $trTag=~m/$Block_Regex3/igs;
							
							$PDF_URL_Cont = &Download_Utility::trim($tdTag[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
							$document_type = &Download_Utility::trim($tdTag[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
							$Doc_Desc = &Download_Utility::trim($tdTag[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
							$Published_Date = &Download_Utility::trim($tdTag[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
							
							# print "PDF_URL_Cont=>$PDF_URL_Cont\n";
							# print "document_type=>$document_type\n";
							# print "Doc_Desc=>$Doc_Desc\n";
							# print "Published_Date=>$Published_Date\n";
							
							if($Doc_URL_BY eq 'Salford')
							{
								my ($AppDoc,$ViewDoc,$AppDoc1,$ViewDoc1,$AppDoc2,$ViewDoc2);
										
								if($PDF_URL_Cont=~m/$Doc_URL_BY_Regex/is)
								{
									$AppDoc=uri_escape($1);
									$ViewDoc=uri_escape($2);
								}	
									
								if($PDF_URL_Cont=~m/value=\"View\s*document\"\s*[^>]*?>\s*<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
								{
									$AppDoc1=uri_escape($1);
									$ViewDoc1=uri_escape($2);
								}		
								$ViewDoc2=$ViewDoc;
								$AppDoc2=$AppDoc1;
								$ViewDoc2=~s/btnViewDocument$/ctl00/gsi;
											
								my ($hdnWindowWidth,$hdnWindowWidthval);
								my $VIEWSTATE = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
								my $EVENTVALIDATION = uri_escape($1) if($docPageContent=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
								if($docPageContent=~m/<input[^>]*?name=\"([^\"]*?hdnWindowWidth)\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is)
								{
									$hdnWindowWidth = uri_escape($1);
									$hdnWindowWidthval = $2;			
								}
								
								my $Post_Content = "__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24ContentPlaceHolder%24ddlPageSize=99999&$AppDoc2=$ViewDoc2&$AppDoc=$ViewDoc&$AppDoc1=$ViewDoc1&$hdnWindowWidth=$hdnWindowWidthval";
								
								$Post_Content=~s/<VIEWSTATE>/$VIEWSTATE/gsi;
								$Post_Content=~s/<EVENTVALIDATION>/$EVENTVALIDATION/gsi;
								
								
								my ($App_Page_Content,$Code) = &Download_Utility::docPostMechMethod($documentURL,$Post_Content,$pageFetch);	
								$Post_Content=~s/$VIEWSTATE/<VIEWSTATE>/gsi;
								$Post_Content=~s/$EVENTVALIDATION/<EVENTVALIDATION>/gsi;
								
								my $halfURL = $1 if($App_Page_Content=~m/window\.open\(\'([^\']*?)\'\)\;\/\/\]\]>/is);
								
								$Doc_URL=$Doc_Download_URL_ABS.$halfURL;
								
							}
							
							$Doc_Desc = &Download_Utility::Clean($Doc_Desc);	
							$document_type = &Download_Utility::Clean($document_type);
							$Published_Date = &Download_Utility::Clean($Published_Date);
							
							my ($file_type);
							if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
							{
								$file_type=$1;
							}
							else
							{
								$file_type='pdf';
							}	
							
							
							if($Doc_Desc=~m/\.([\w]{3,4})\s*$/is)
							{
								my $file_typeDoc=$1;
								if($file_typeDoc eq $file_type)
								{
									$file_type;
								}
								else
								{
									$file_type=$file_typeDoc;
								}
							}
							elsif($document_type=~m/\.([\w]{3,4})\s*$/is)
							{
								my $file_typeDocType=$1;
								if($file_typeDocType eq $file_type)
								{
									$file_type;
								}
								else
								{
									$file_type=$file_typeDocType;
								}
							}

							if($Doc_Desc!~m/^\s*$/is)
							{
								$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
							}	
							else
							{
								$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
							}
							
							$pdf_name = &Download_Utility::PDF_Name($pdf_name);
							
							$pdfcount_inc++;
							
							print "Doc_URL	: $Doc_URL\n";
							print "pdf_name	: $pdf_name\n";
							print "Doc_Type	: $document_type\n";
							print "Published_Date: $Published_Date\n";
							
							
							my $file_name=$Temp_Download_Location.'/'.$pdf_name;
							print "file_name	: $file_name\n";
							
							$pageFetch->get($Doc_URL);
							my $pdf_content = $pageFetch->content;
							
							my $fh = FileHandle->new("$file_name",'w') or warn "$!";
							binmode($fh);
							$fh->print($pdf_content);
							$fh->close();
							
						}
					}
				}
			}			
		}		
	}
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

$Insert_OneAppLog=~s/\,$//igs;
# print "\n$Insert_OneAppLog\n";
&Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# print "\n$Insert_Document\n";
&Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
&Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
&Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);


sub Search_Application()
{
	my $Application_No = shift;
	my $ApplicationURL = shift;

	my ($homePageContent,$Code) = &Download_Utility::docMechMethod($Home_URL,$pageFetch);	
	
	my ($docPageContent,$docRespCode,$searchPageCon,$searchCode);
	
	if($Search_Type eq "By Click")
	{
		$pageFetch->form_number($FORM_NUMBER);
		$pageFetch->set_fields( $FORM_REFERENECE => $Application_No );
		$pageFetch->click();
			
		$searchPageCon = $pageFetch->content;
		$searchCode= $pageFetch->status();
	}
	else
	{
		my $appNum;
		
		$appNum = uri_escape($Application_No);	
		
		my $viewState = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATE"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateGen = uri_escape($1) if($homePageContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"__VIEWSTATEGENERATOR"\s[^>]*?value="([^>]*?)"\s*\/>/is);
		my $eventVal = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__EVENTVALIDATION"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		my $viewStateZip = uri_escape($1) if($homePageContent=~m/<input\s*\s*type=\"hidden\"\s*name="__VIEWSTATEZIP"\s*id="[^>]*?\s*value="([^>]*?)"[^>]*?>/is);
		
		$Search_POST =~s/<VIEWSTATE>/$viewState/gsi;
		$Search_POST =~s/<VIEWSTATEGENERATOR>/$viewStateGen/gsi;
		$Search_POST =~s/<EVENTVALIDATION>/$eventVal/gsi;
		$Search_POST =~s/<APPNUM>/$appNum/gsi;
		$Search_POST =~s/<VIEWSTATEZIP>/$viewStateZip/gsi;
		# print "$Search_POST\n"; <STDIN>;
		
		($searchPageCon,$searchCode) = &Download_Utility::docPostMechMethod($POST_URL,$Search_POST,$pageFetch);	
		
		$Search_POST =~s/$appNum/<APPNUM>/gsi;
		$Search_POST =~s/$viewState/<VIEWSTATE>/gsi;
		$Search_POST =~s/$viewStateGen/<VIEWSTATEGENERATOR>/gsi;
		$Search_POST =~s/$eventVal/<EVENTVALIDATION>/gsi;
		$Search_POST =~s/$viewStateZip/<VIEWSTATEZIP>/gsi;
		
	}
	
	# open(DC,">searchPageCon.html");
	# print DC $searchPageCon;
	# close DC;	
	# exit;
	
	if(($searchPageCon=~m/$Doc_URL_Regex/is) && ($Doc_URL_Regex ne 'N/A'))
	{
		my $docURL = $1;
		$docURL=~s/amp\;//igs;
		
		if($docURL!~m/^http\:/is)
		{
			$docURL = $FILTER_URL.$docURL;
		}	

		($docPageContent,$docRespCode) = &Download_Utility::docMechMethod($docURL,$pageFetch);
	}
	else
	{
		$docPageContent = $searchPageCon;
	}
	
	return($docPageContent,$docRespCode);
}


sub clean()
{
	my $Data=shift;
	$Data=~s/\s*<[^>]*?>\s*//igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;//igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&nsbp//igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	return($Data);
}