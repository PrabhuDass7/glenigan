use strict;
use HTML::Entities;
use filehandle;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use Digest::MD5;
use Time::Piece;
use Config::Tiny;
use JSON;
use Cwd qw(abs_path);
use File::Basename;
use WWW::Mechanize;
use IO::Socket::SSL qw();

my $pageFetch2 = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
print "$basePath\n";
# $basePath="D:\\Glenigan\\Document_download\\";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# Private Module
require ($libDirectory.'/Download_Utility.pm'); 
require ($libDirectory.'/Download_DB.pm'); 

my $Council_Code=$ARGV[0];
my $inputFormatID=$ARGV[1];
my $onDemand=$ARGV[2];
my $onDemandID=$ARGV[3];
print "onDemand: $onDemand\n";

if($Council_Code!~m/[\d]+/is)
{
	print "\nEnter Council Code!!!\n";
	exit;
}

# &Download_Utility::User_Agent_Define();

my $Current_Location = $basePath;
my $time = Time::Piece->new;
my $Downloaded_date = $time->strftime('%m/%d/%Y %H:%M');

#-------------------------- Global Variable Declaration -------------------------#
my $Download_Location = '\\\\172.27.138.180\\Council_PDFs\\'; # New Shared Drive Location
my $Local_Download_Location = $dataDirectory."\\Documents\\";
# my $Input_Table = 'FORMAT_PUBLIC_ACCESS';
# my $Output_Table = 'format_pdf';
# my $Log_Table = 'OneAppLog';
# my $MD5_Table = 'TBL_PDF_MD5';
# my $DashBoard_Table = 'TBL_ONEAPP_STATUS';
my $Application_Form_Keywords = '\s*(Application|App|Planning|No)(?:\s*|_)\(?(?:Forms?|pdf|information|Documents?|plans?|Details?|Applications?|Public_View|without_personal_data|personal_data|Permission|(?:\s*|_)for(?:\s*|_)plan)\)?\s*';
my $Application_Form_Negative_Keywords = 'COVER[ING]+_?LETTER';

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS);
my ($inputTableName,$ApprovedDecisionUpdate_query);

if ($onDemand eq 'ApprovedDecision')
{
	# ($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input_ApprovedDecision($dbh,$Council_Code,$inputFormatID);
}	
else
{
	($Format_ID, $Source, $Application_No, $Document_Url, $Markup, $URL, $PROJECT_ID, $PROJECT_STATUS,$COUNCIL_NAME,$NO_OF_DOCUMENTS) = &Download_DB::Retrieve_Input($dbh,$Council_Code,$inputFormatID);
}	

my $COUNCIL_NAME_TMP = @{$COUNCIL_NAME}[0];

#---------------- Get mechanize userAgent global variable -------------------#
my $pageFetch = &Download_Utility::mech_UserAgent($Council_Code);

#---------------- Queries For Bulk Hit -------------------#
my $Insert_Document = "insert into Format_Document(Format_Id, Filename, Document_Type, Description, Created_Date, Doc_Published_Date, Download_Status) values";	
my $Insert_OneAppLog = "insert into OneAppLog(Format_ID,Council_CD,Markup,Document_URL,No_Of_Documents,One_App,Scraped_Date,Comments,No_Of_Doc_Downloaded) values";	
my $MD5_Insert_Query = "insert into TBL_PDF_MD5(FORMAT_ID, COUNCIL_CODE, PDF_MD5, PDF_NAME) values ";	
my $Dashboard_Insert_Query = "insert into TBL_ONEAPP_STATUS(Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values (\'$Council_Code\', \'$COUNCIL_NAME_TMP\', \'Running\', \'$Downloaded_date\')";	
&Download_DB::Execute($dbh,$Dashboard_Insert_Query);

if($onDemand eq 'Y')
{
	print "This is an On demand request\n";
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\'";
	&Download_DB::Execute($dbh,$onDemand_Update_Query);
}

my ($Update_Query,$No_Of_Doc_Downloaded_onDemand,$Site_Status);
$No_Of_Doc_Downloaded_onDemand = 0;

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read($iniDirectory.'/Scraping_Details_Format1_Extented.ini');
my $Method = $Config->{$Council_Code}->{'Method'};
my $App_No_UriEscape = $Config->{$Council_Code}->{'App_No_UriEscape'};
my $Proxy = $Config->{$Council_Code}->{'Proxy'};
my $Format = $Config->{$Council_Code}->{'Format'};
my $Home_URL = $Config->{$Council_Code}->{'Home_URL'};
my $Primary_Url = $Config->{$Council_Code}->{'Primary_Url'};
my $Document_Url_Regex = $Config->{$Council_Code}->{'Document_Url_Regex'};
my $Proceed_Without_GET = $Config->{$Council_Code}->{'Proceed_Without_GET'};
my $Accept_Cookie = $Config->{$Council_Code}->{'Accept_Cookie'};
my $Accept_Cookie_URL = $Config->{$Council_Code}->{'Accept_Cookie_URL'};
my $Accept_Post_Content = $Config->{$Council_Code}->{'Accept_Post_Content'};
my $Accept_Host = $Config->{$Council_Code}->{'Accept_Host'};
my $Host = $Config->{$Council_Code}->{'Host'};
my $Need_Host = $Config->{$Council_Code}->{'Need_Host'};
my $Content_Type = $Config->{$Council_Code}->{'Content_Type'};
my $Accept_Language = $Config->{$Council_Code}->{'Accept_Language'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Doc_URL_Regex = $Config->{$Council_Code}->{'Doc_URL_Regex'};
my $Search_URL = $Config->{$Council_Code}->{'Search_URL'};
my $Referer = $Config->{$Council_Code}->{'Referer'};
my $Post_Content = $Config->{$Council_Code}->{'Post_Content'};
my $Navigate_By_Post1 = $Config->{$Council_Code}->{'Navigate_By_Post1'};
my $Navi1_Info_Regex1 = $Config->{$Council_Code}->{'Navi1_Info_Regex1'};
my $Navi1_Info_Regex2 = $Config->{$Council_Code}->{'Navi1_Info_Regex2'};
my $Navi1_Host = $Config->{$Council_Code}->{'Navi1_Host'};
my $Navi1_Post_Url = $Config->{$Council_Code}->{'Navi1_Post_Url'};
my $Navi1_Post_Content = $Config->{$Council_Code}->{'Navi1_Post_Content'};
my $Landing_Page_Regex = $Config->{$Council_Code}->{'Landing_Page_Regex'};
my $Navi1_Info_Need_UriEscape = $Config->{$Council_Code}->{'Navi1_Info_Need_UriEscape'};
my $Landing_Page_ABS = $Config->{$Council_Code}->{'Landing_Page_ABS'};
my $Landing_Page_Regex2 = $Config->{$Council_Code}->{'Landing_Page_Regex2'};
my $Landing_Page_Regex3 = $Config->{$Council_Code}->{'Landing_Page_Regex3'};
my $Doc_URL_BY = $Config->{$Council_Code}->{'Doc_URL_BY'};
my $Viewstate_Regex = $Config->{$Council_Code}->{'Viewstate_Regex'};
my $Viewstategenerator_Regex = $Config->{$Council_Code}->{'Viewstategenerator_Regex'};
my $Eventvalidation_Regex = $Config->{$Council_Code}->{'Eventvalidation_Regex'};
my $CSRFToken_Regex = $Config->{$Council_Code}->{'CSRFToken_Regex'};
my $Doc_Page_Num_Regex = $Config->{$Council_Code}->{'Doc_Page_Num_Regex'};
my $Doc_NumberOfRows_Regex = $Config->{$Council_Code}->{'Doc_NumberOfRows_Regex'};
my $Doc_POST_ID1_Regex = $Config->{$Council_Code}->{'Doc_POST_ID1_Regex'};
my $Doc_Post_Content = $Config->{$Council_Code}->{'Doc_Post_Content'};
my $Doc_Post_URL = $Config->{$Council_Code}->{'Doc_Post_URL'};
my $Doc_Post_Host = $Config->{$Council_Code}->{'Doc_Post_Host'};
my $DOC_PDF_ID_URL = $Config->{$Council_Code}->{'DOC_PDF_ID_URL'};
my $Block_Regex1 = $Config->{$Council_Code}->{'Block_Regex1'};
my $Block_Regex2 = $Config->{$Council_Code}->{'Block_Regex2'};
my $Block_Regex3 = $Config->{$Council_Code}->{'Block_Regex3'};
my $Block_Regex4 = $Config->{$Council_Code}->{'Block_Regex4'};
my $Doc_Published_Date_Regex = $Config->{$Council_Code}->{'Doc_Published_Date_Regex'};
my $Doc_Type_Regex = $Config->{$Council_Code}->{'Doc_Type_Regex'};
my $Doc_File_Format_Index = $Config->{$Council_Code}->{'Doc_File_Format_Index'};
my $Doc_File_Format_Regex = $Config->{$Council_Code}->{'Doc_File_Format_Regex'};
my $Doc_Published_Date_Index = $Config->{$Council_Code}->{'Doc_Published_Date_Index'};
my $Doc_Type_Index = $Config->{$Council_Code}->{'Doc_Type_Index'};
my $Doc_Description_Index = $Config->{$Council_Code}->{'Doc_Description_Index'};
my $Doc_Description_Regex = $Config->{$Council_Code}->{'Doc_Description_Regex'};
my $Doc_Download_URL_Index = $Config->{$Council_Code}->{'Doc_Download_URL_Index'};
my $Doc_Download_URL_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_Regex'};
my $Doc_Download_URL_Replace = $Config->{$Council_Code}->{'Doc_Download_URL_Replace'};
my $Doc_Download_URL_redirct = $Config->{$Council_Code}->{'Doc_Download_URL_redirct'}; #New
my $Doc_Download_URL_redirct_Regex = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_Regex'};#New
my $Doc_Download_URL_redirct_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_redirct_ABS'};#New
my $Doc_Download_URL_ABS = $Config->{$Council_Code}->{'Doc_Download_URL_ABS'};
my $Pagination_Type = $Config->{$Council_Code}->{'Pagination_Type'};
my $Total_No_Of_Pages_Regex = $Config->{$Council_Code}->{'Total_No_Of_Pages_Regex'};
my $Next_Page_Regex = $Config->{$Council_Code}->{'Next_Page_Regex'};
my $Next_Page_Replace_Info = $Config->{$Council_Code}->{'Next_Page_Replace_Info'};
my $Next_Page_Replace_Key = $Config->{$Council_Code}->{'Next_Page_Replace_Key'};
my $Current_Page_Number_Regex = $Config->{$Council_Code}->{'Current_Page_Number_Regex'};
my $Next_Page_Post_Content = $Config->{$Council_Code}->{'Next_Page_Post_Content'};
my $Next_Referer = $Config->{$Council_Code}->{'Next_Referer'};
my $Abs_url = $Config->{$Council_Code}->{'Abs_url'};
my $Application_Regex = $Config->{$Council_Code}->{'Application_Regex'};
my $Available_Doc_Count_Regex = $Config->{$Council_Code}->{'Available_Doc_Count_Regex'};

$Navigate_By_Post1 = 'N/A' if($Navigate_By_Post1 eq '');$Document_Url_Regex = 'N/A' if($Document_Url_Regex eq '');$Primary_Url = 'N/A' if($Primary_Url eq '');$Navi1_Info_Regex1 = 'N/A' if($Navi1_Info_Regex1 eq '');$Navi1_Info_Regex2 = 'N/A' if($Navi1_Info_Regex2 eq '');
$Navi1_Post_Url = 'N/A' if($Navi1_Post_Url eq '');$Navi1_Post_Content = 'N/A' if($Navi1_Post_Content eq '');$Method = 'N/A' if($Method eq '');	$Proxy = 'N/A' if($Proxy eq '');	$Format = 'N/A' if($Format eq '');	$Home_URL = 'N/A' if($Home_URL eq '');	
$Accept_Cookie = 'N/A' if($Accept_Cookie eq '');	$Proceed_Without_GET = 'N/A' if($Proceed_Without_GET eq '');	$Accept_Cookie_URL = 'N/A' if($Accept_Cookie_URL eq '');	$Accept_Post_Content = 'N/A' if($Accept_Post_Content eq '');	
$Accept_Host = 'N/A' if($Accept_Host eq '');	$Host = 'N/A' if($Host eq '');	$Need_Host = 'N/A' if($Need_Host eq '');	$Content_Type = 'N/A' if($Content_Type eq '');	
$Abs_url = 'N/A' if($Abs_url eq '');	$Doc_URL_Regex = 'N/A' if($Doc_URL_Regex eq '');	$Referer = 'N/A' if($Referer eq '');	$Search_URL = 'N/A' if($Search_URL eq '');	$Post_Content = 'N/A' if($Post_Content eq '');	
$Landing_Page_Regex = 'N/A' if($Landing_Page_Regex eq '');	$Landing_Page_Regex2 = 'N/A' if($Landing_Page_Regex2 eq ''); $Landing_Page_Regex3 = 'N/A' if($Landing_Page_Regex3 eq '');	$Doc_URL_BY = 'N/A' if($Doc_URL_BY eq '');	
$Viewstate_Regex = 'N/A' if($Viewstate_Regex eq '');	$Viewstategenerator_Regex = 'N/A' if($Viewstategenerator_Regex eq '');	$Eventvalidation_Regex = 'N/A' if($Eventvalidation_Regex eq '');	
$CSRFToken_Regex = 'N/A' if($CSRFToken_Regex eq '');	$Doc_Page_Num_Regex = 'N/A' if($Doc_Page_Num_Regex eq '');	$Doc_NumberOfRows_Regex = 'N/A' if($Doc_NumberOfRows_Regex eq '');
$Doc_POST_ID1_Regex = 'N/A' if($Doc_POST_ID1_Regex eq '');	$Doc_Post_Content = 'N/A' if($Doc_Post_Content eq '');	$Doc_Post_URL = 'N/A' if($Doc_Post_URL eq '');	
$Doc_Post_Host = 'N/A' if($Doc_Post_Host eq '');	$DOC_PDF_ID_URL = 'N/A' if($DOC_PDF_ID_URL eq '');	$Block_Regex1 = 'N/A' if($Block_Regex1 eq '');	$Block_Regex2 = 'N/A' if($Block_Regex2 eq '');	
$Block_Regex3 = 'N/A' if($Block_Regex3 eq '');	$Block_Regex4 = 'N/A' if($Block_Regex4 eq '');	$Doc_Published_Date_Index = 'N/A' if($Doc_Published_Date_Index eq '');	
$Doc_Type_Index = 'N/A' if($Doc_Type_Index eq '');	$Doc_Description_Index = 'N/A' if($Doc_Description_Index eq '');	$Doc_Download_URL_Index = 'N/A' if($Doc_Download_URL_Index eq '');	
$Doc_Download_URL_Regex = 'N/A' if($Doc_Download_URL_Regex eq '');	$Doc_Download_URL_Replace = 'N/A' if($Doc_Download_URL_Replace eq '');	$Doc_Download_URL_redirct = 'N/A' if($Doc_Download_URL_redirct eq '');	
$Doc_Download_URL_redirct_Regex = 'N/A' if($Doc_Download_URL_redirct_Regex eq '');	$Doc_Download_URL_redirct_ABS = 'N/A' if($Doc_Download_URL_redirct_ABS eq '');	
$Doc_Download_URL_ABS = 'N/A' if($Doc_Download_URL_ABS eq '');	$Pagination_Type = 'N/A' if($Pagination_Type eq '');	$Total_No_Of_Pages_Regex = 'N/A' if($Total_No_Of_Pages_Regex eq '');	
$Next_Page_Regex = 'N/A' if($Next_Page_Regex eq '');	$Next_Page_Replace_Info = 'N/A' if($Next_Page_Replace_Info eq '');	$Next_Page_Replace_Key = 'N/A' if($Next_Page_Replace_Key eq '');	
$Current_Page_Number_Regex = 'N/A' if($Current_Page_Number_Regex eq '');	$Next_Page_Post_Content = 'N/A' if($Next_Page_Post_Content eq '');	$Next_Referer = 'N/A' if($Next_Referer eq '');	
$Abs_url = 'N/A' if($Abs_url eq '');	$Application_Regex = 'N/A' if($Application_Regex eq '');	$Available_Doc_Count_Regex = 'N/A' if($Available_Doc_Count_Regex eq '');	$Accept_Language = 'N/A' if($Accept_Language eq ''); $Doc_File_Format_Index = 'N/A' if($Doc_File_Format_Index eq '');	$Doc_File_Format_Regex = 'N/A' if($Doc_File_Format_Regex eq '');	$Doc_Description_Regex = 'N/A' if($Doc_Description_Regex eq '');	$Doc_Type_Regex = 'N/A' if($Doc_Type_Regex eq '');	$Doc_Published_Date_Regex = 'N/A' if($Doc_Published_Date_Regex eq '');	$Landing_Page_ABS = 'N/A' if($Landing_Page_ABS eq '');
$Navi1_Host = 'N/A' if($Navi1_Host eq '');$Navi1_Info_Need_UriEscape = 'N/A' if($Navi1_Info_Need_UriEscape eq '');

my $pdfcount_inc=1;
my $Todays_Count=0;
my $Todays_Downloaded_Count=0;

my @Nextpage_Replace_Array = split('\|',$Next_Page_Replace_Key);
my %Nextpage_Replace_Hash;
for(@Nextpage_Replace_Array)
{
	my @Sp_Arr = split(/:/, $_);
	$Nextpage_Replace_Hash{$Sp_Arr[0]} = $Sp_Arr[1];
}	

my @Format_ID 		= @$Format_ID;
my @Source 			= @$Source;
my @Application_No 	= @$Application_No;
my @Document_Url 	= @$Document_Url;
my @Markup 			= @$Markup;
my @URL 			= @$URL;
my @PROJECT_STATUS 	= @$PROJECT_STATUS;
my @PROJECT_ID 		= @$PROJECT_ID;
my @COUNCIL_NAME 	= @$COUNCIL_NAME;
my @NO_OF_DOCUMENTS	= @$NO_OF_DOCUMENTS;

my $reccount = @Format_ID;
print "COUNCIL :: <$Council_Code>\n";
print "RECCNT  :: <$reccount>\n";

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{
	print "RECCNT :: $reccnt \n";
	my $Format_ID 		= $Format_ID[$reccnt];
	my $Source 			= $Source[$reccnt];
	my $Application_No 	= $Application_No[$reccnt];
	my $Document_Url 	= $Document_Url[$reccnt];
	my $Markup 			= $Markup[$reccnt];
	my $URL 			= $URL[$reccnt];
	my $PROJECT_STATUS 	= $PROJECT_STATUS[$reccnt];
	my $PROJECT_ID 		= $PROJECT_ID[$reccnt];
	my $COUNCIL_NAME 	= $COUNCIL_NAME[$reccnt];
	my $NO_OF_DOCUMENTS	= $NO_OF_DOCUMENTS[$reccnt];
	print "Format_ID :: $Format_ID\n";
	
	my ($Doc_Details_Hash_Ref,%Doc_Details_Hash, $Code, $Available_Doc_Count);
	my $Landing_Page_Flag=0;
	
	if(lc($Primary_Url) eq 'url')
	{
		if(($Document_Url_Regex ne '') or ($Document_Url_Regex ne 'N/A'))
		{
			print "URL==>$URL\n";
			my ($Content, $Code, $Redir_Url) = &Download_Utility::Getcontent($URL,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			# open(DC,">Doc_Content.html");
			# print DC $Content;
			# close DC;
			
			if($Content=~m/$Document_Url_Regex/is)
			{
				$Document_Url=$1;
				if($Council_Code == 26)
				{
					$Document_Url=~s/\s+//igs;
					$Document_Url=~s/&#xD;//igs;
					$Document_Url=~s/&#xA;//igs;
				}
				my $u1=URI::URL->new($Document_Url,$URL);
				$Document_Url=$u1->abs;
			}
		}
	}

	($Doc_Details_Hash_Ref, $Document_Url, $Code,$Available_Doc_Count)=&Extract_Link($Method, $Home_URL, $Document_Url, $URL,\%Nextpage_Replace_Hash, $Application_No,$Format_ID);
	$Site_Status = $Code;
	
	if(!$Doc_Details_Hash_Ref)
	{
		print "No Docs Found\n";
		next;
	}
	my %Doc_Details=%$Doc_Details_Hash_Ref;
	my $pdfcount=keys %Doc_Details;
	# foreach my $pdf_url(keys %Doc_Details)
	# {
		# my ($Doc_Type,$Pdf_Name,$Doc_Published_Date);

		# $Doc_Type = $Doc_Details{$pdf_url}->[0];
		# $Pdf_Name = $Doc_Details{$pdf_url}->[1];
		# $Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		# print "Pdf_url\t: $pdf_url\n";
		# print "Doc_Type: $Doc_Type\n";
		# print "Pdf_Name: $Pdf_Name\n";
	# }	
	$Todays_Count = $Todays_Count + $pdfcount;
	if($Available_Doc_Count = ~m/^\s*$/is)
	{
		$Available_Doc_Count = $pdfcount;
	}	
	my $RetrieveMD5=[];
	my $No_Of_Doc_Downloaded=0;
	# if(lc($Markup) eq 'large')
	# {
		if($pdfcount > $NO_OF_DOCUMENTS)
		{
			# $RetrieveMD5 =&Download_DB::RetrieveMD5($dbh, $Council_Code, $Format_ID); 
		}
		else
		{
			# goto DB_Flag;
		}
	# }

	my $Temp_Download_Location = $Local_Download_Location.$Format_ID;
	print "Temp_Download_Location=>$Temp_Download_Location\n";<STDIN>;
	
	unless ( -d $Temp_Download_Location )
	{
		$Temp_Download_Location=~s/\//\\/igs;
		system("mkdir $Temp_Download_Location");
	}

	print "MKDIR====> $Temp_Download_Location\n";
	my ($pdf_name_list, $pdf_Url_list,$One_App_Avail_Status,$Comments,$One_App_Name);
	
	foreach my $pdf_url(keys %Doc_Details)
	{
		my ($Doc_Type,$Pdf_Name,$Doc_Published_Date,$Doc_Desc);

		$Doc_Type = $Doc_Details{$pdf_url}->[0];
		$Pdf_Name = $Doc_Details{$pdf_url}->[1];
		$Doc_Published_Date=$Doc_Details{$pdf_url}->[2];
		$Doc_Desc=$Doc_Details{$pdf_url}->[3];
		
		if(($Pdf_Name=~m/Application\W*full\W*planning/is) && ($Council_Code=~m/^339$/is))
		{
			$Pdf_Name=~s/Application/Application_Form/igs;
		}
		
		if(lc($Markup) eq 'large')
		{
			if($pdf_url!~m/^\s*$/)
			{
				print "pdf_url:: $pdf_url\n";<STDIN>;
				# $pdf_url='https://www2.ashfield.gov.uk/Planning/StreamDocPage/obj.pdf?DocNo=18162746&PageNo=1&content=obj.pdf';
				my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
				my ($Download_Status)=&Download_Utility::Url_Status($D_Code);
				if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
				{
					$One_App_Avail_Status = 'Y';
					$One_App_Name = $Pdf_Name;
				}	
				if($MD5 ne '')
				{
					my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
					$MD5_Insert_Query.= $MD5_InsertQuery;
					
				}	
				my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
				$Insert_Document.=$Insert_Document_List;
					
				$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
				$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
			}
		}	
		else
		{
			if($Pdf_Name=~m/$Application_Form_Keywords/is  && $Pdf_Name !~m/$Application_Form_Negative_Keywords/is)
			{
				$One_App_Avail_Status = 'Y';
				$One_App_Name = $Pdf_Name;
				my ($Download_Status);
				if($pdf_url!~m/^\s*$/)
				{
					my ($D_Code, $Err_Msg, $MD5)=&Download_Utility::Download($pdf_url,$Pdf_Name,$Document_Url,$Temp_Download_Location,$RetrieveMD5,$Markup);
					($Download_Status)=&Download_Utility::Url_Status($D_Code);
					$No_Of_Doc_Downloaded++ if($D_Code=~m/200/is);
					$Comments=$Comments.'|'.$Err_Msg if($Err_Msg!~m/^\s*$/is);
					
					
					if($MD5 ne '')
					{
						my $MD5_InsertQuery="(\'$Format_ID\', \'$Council_Code\', \'$MD5\', \'$Pdf_Name\'),";
						$MD5_Insert_Query.= $MD5_InsertQuery;
						
					}
					my $Insert_Document_List = "(\'$Format_ID\', \'$Pdf_Name\', \'$Doc_Type\', \'$Doc_Desc\', \'$Downloaded_date\', \'$Doc_Published_Date\', \'$Download_Status\'),";	
					$Insert_Document.=$Insert_Document_List;				
						
					my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', All_Documents_Downloaded = 'Y', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
					$Update_Query.= $Flag_Update_query 	if($D_Code=~m/200/is);
				}
				
			}
		}	
	}	

	$No_Of_Doc_Downloaded_onDemand = $No_Of_Doc_Downloaded;
	# &Download_Utility::Move_Files($Download_Location, $Temp_Download_Location, $Current_Location,$Format_ID);
	
	DB_Flag:
		
	if($pdfcount > $NO_OF_DOCUMENTS)
	{
		$No_Of_Doc_Downloaded = $No_Of_Doc_Downloaded + $NO_OF_DOCUMENTS;
		# $No_Of_Doc_Downloaded = $pdfcount;
	}	
	# else
	# {
		# $No_Of_Doc_Downloaded = $NO_OF_DOCUMENTS;
	# }	
	print "No_Of_Doc_Downloaded: $No_Of_Doc_Downloaded\n";
	print "pdfcount: $pdfcount\n";
	print "NO_OF_DOCUMENTS: $NO_OF_DOCUMENTS\n";

	$Todays_Downloaded_Count = $Todays_Downloaded_Count + $No_Of_Doc_Downloaded;
	
	$Document_Url=~s/\'/\'\'/igs;
	my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$pdfcount\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";	
	$Insert_OneAppLog.= $insert_query;

	if($One_App_Name=~m/^\s*$/is)
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
		else
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set No_of_Documents = \'$pdfcount\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}	
	}
	else
	{	
		if(lc($Markup) eq 'large')	
		{
			my $Flag_Update_query = "update FORMAT_PUBLIC_ACCESS set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
			$Update_Query.= $Flag_Update_query;
		}
	}	
	if ($onDemand eq 'ApprovedDecision')
	{
		my $Flag_Update_query = "update GLENIGAN..TBL_APPROVED_LARGE_PROJECT set Document_Name = \'$One_App_Name\', No_of_Documents = \'$pdfcount\', OA_Scraped_Date = \'$Downloaded_date\', All_Documents_Downloaded = 'Y' where id = \'$Format_ID\' and COUNCIL_CODE = \'$Council_Code\';";
		$ApprovedDecisionUpdate_query.= $Flag_Update_query;
	}	
	$pdfcount_inc=1;
}

my $Status_Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Completed\', Todays_Application_Count = \'$Todays_Count\', Downloaded_Count = \'$Todays_Downloaded_Count\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
$Update_Query.= $Status_Update_Query;

# print "\n$Insert_OneAppLog\n";
# print "\n$Insert_Document\n";

$Insert_OneAppLog=~s/\,$//igs;
# &Download_DB::Execute($dbh,$Insert_OneAppLog) if($Insert_OneAppLog!~m/values\s*$/is);

$Insert_Document=~s/\,$//igs;
# &Download_DB::Execute($dbh,$Insert_Document)  if($Insert_Document!~m/values\s*$/is);

$Update_Query=~s/\,$//igs;
# print "\n$Update_Query\n";
# &Download_DB::Execute($dbh,$Update_Query) if($Update_Query!~/^\s*$/is);

$MD5_Insert_Query=~s/\,$//igs;
# print "\n$MD5_Insert_Query\n";
# &Download_DB::Execute($dbh,$MD5_Insert_Query) if($MD5_Insert_Query!~/values\s*$/is);

$ApprovedDecisionUpdate_query=~s/\,$//igs;
# print "\n$ApprovedDecisionUpdate_query\n";
# &Download_DB::Execute($dbh,$ApprovedDecisionUpdate_query) if($ApprovedDecisionUpdate_query!~/values\s*$/is);

# if($onDemand eq 'Y')
# {
	# print "Update & Delete This Ondemand request\n";
	# my $onDemandStatus = &Download_Utility::onDemandStatus($No_Of_Doc_Downloaded_onDemand,$Site_Status);
	# my $onDemandUpdateQuery = "update tbl_on_demand_download_backup set Status = \'$onDemandStatus\' where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and download_id = \'$onDemandID\'";
	# print "onDemandUpdateQuery: $onDemandUpdateQuery\n";
	# &Download_DB::Execute($dbh,$onDemandUpdateQuery);
	# my $onDemandDeleteQuery = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and format_id = \'$inputFormatID\' and status = 'Inprogress'";	
	# &Download_DB::Execute($dbh,$onDemandDeleteQuery);
# }


sub Extract_Link()
{
	my $Method = shift;
	my $Home_URL = shift;
	my $Document_Url = shift;
	my $URL = shift;
	my $Nextpage_Replace_Hash = shift;
	my $Application_No = shift;
	my $Format_ID = shift;

	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my $Scrp_Flag=0;
	my ($Content,$Code,$Redir_Url,$Doc_Details_Hash_Ref,%Doc_Details_Hash,$Previous_page,$Post_Responce,$Post_Res_code,$Available_Doc_Count);
	my ($Landing_Page_Flag,$Landing_Page_Flag2,$Landing_Page_Flag3)=(0,0,0);
	
	$Previous_page=0;
	print "Extract_Link\n";
	# print "$Accept_Cookie\n";

	if($Accept_Cookie ne 'N/A')
	{
		($Content,$Code,$Redir_Url) = &Download_Utility::Getcontent($Document_Url,$URL,$Accept_Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		my $Viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		my $Viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		my $Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		my $Accept_Post_Content_Tmp=$Accept_Post_Content;
		my $Application_No_tmp = uri_escape($Application_No);
		$Accept_Post_Content_Tmp=~s/<VIEWSTATE>/$Viewstate/igs;
		$Accept_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Viewstategenerator/igs;
		$Accept_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Eventvalidation/igs;
		$Accept_Post_Content_Tmp=~s/<SELECTED_ID>/$Application_No_tmp/igs;
		($Post_Responce,$Post_Res_code)=&Download_Utility::Post_Method($Accept_Cookie_URL,$Accept_Host,$Content_Type,$Accept_Cookie_URL,$Accept_Post_Content_Tmp,$Proxy);
		# open(DC,">Post_Responce.html");
		# print DC $Post_Responce;
		# close DC;
		if($Proceed_Without_GET eq 'Y')
		{
			$Content = $Post_Responce;
			goto Proceed_without_GET;
		}	
	}
	Re_Ping:
	if($Method=~m/GET/is)
	{
		print "Ping\n$Document_Url\n";
		
		if($Council_Code=~m/^95$/i)
		{
			$Document_Url=~s/https?\:\/\/info\.ambervalley[^>]*?refVal=([A-Z]+\%2F[0-9]+\%2F[0-9]+)$/https\:\/\/info\.ambervalley\.gov\.uk\/WebServices\/AVBCFeeds\/IdoxEDMJSON\.asmx\/GetIdoxEDMDocListForCase\?refVal=$1&docApplication=planning/is;
		}
		elsif($Council_Code=~m/^91$/is)
		{
			if($Document_Url=~m/keyVal\=([^>]*?)\&/is)
			{
				$Document_Url="https://planningonline.npt.gov.uk/online-applications/applicationDetails.do?activeTab=externalDocuments&keyVal=$1";
			}			
		}
		elsif($Council_Code=~m/^9$/is)
		{
			$Document_Url="https://development.havering.gov.uk/OcellaWeb/planningDetails?reference=$Application_No&from=planningSearch";
		}
		
		($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		
		if($Council_Code=~m/^474$/is)
		{
			($Content,$Code,$Redir_Url) =&Download_Utility::docMechMethod($Document_Url, $pageFetch);
			my $OBJREF=uri_escape($1) if($Content=~m/var\s*objectref\s*=\s*\"\s*([^>]*?)\s*\"/is);
			my $TYPE_ID=uri_escape($1) if($Content=~m/var\s*objecttypeid\s*=\s*\"\s*([^>]*?)\s*\"/is);
			$CSRFToken_Regex=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);

			$pageFetch->add_header( 'Referer' => $Document_Url);
			$pageFetch->add_header( 'X-CSRF-TOKEN' => $CSRFToken_Regex);
			$pageFetch->add_header( 'X-Requested-With' => 'XMLHttpRequest');
			
			my $param="view=modules.PlanningSearch.partial.documents.documents&objectref=$OBJREF&objecttypeid=$TYPE_ID&placeholder_data=";
			
			($Content,$Code)=&Download_Utility::docPostMechMethod($Doc_Post_URL,$param,$pageFetch);
			
			my $json = JSON->new;
			my $data = $json->decode($Content);
			$Content=$data->{'content'};
		}
		if($Code=~m/^500$/is)
		{
			($Content,$Code,$Redir_Url) =&Download_Utility::docMechMethod($Document_Url, $pageFetch);
		}
		if($Council_Code=~m/^9$/is)
		{
			my $post_Url1='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/doc/list';		
			my $Post_Content_Tmp=$Post_Content;
			$Post_Content_Tmp=~s/<APP_NUMBER>/$Application_No/igs;
			my($Post_Responce,$res_code)=&Download_Utility::Post_Method($post_Url1,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy);
			$Content=$Post_Responce;
		}
		
		if($Council_Code=~m/^200$/is)
		{
			$Content = $pageFetch2->get("https://register.civicacx.co.uk/Erewash/Planning/");
			
			$pageFetch2->form_number(1);
			$pageFetch2->set_fields( 'PlanningReferenceTextBox' => "$Application_No",);
			$pageFetch2->click();
			
			$Content = $pageFetch2->content;
			$Code= $pageFetch2->status();
			
			if($Content=~m/<a\s*class\=\"btn\s*btn\-default\"\s*href\=\"([^>]*?)\">\s*Documents\s*<\/a>/is)
			{
				my $relatedDocURL= $1;
				$Document_Url=$Abs_url.$relatedDocURL if($relatedDocURL!~m/^https?\:/is);
				($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			}
			elsif($Document_Url ne "")
			{
				($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			}
			
			
		}
		
		
		if($Council_Code=~m/^(433)$/is)
		{			
			my $docurl='http://docs.west-lindsey.gov.uk/publisher/mvc/getDocumentList?_=1591792135294';
						
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($docurl,$URL,$Host,$Content_Type,$Document_Url,$Proxy,$Need_Host,$Accept_Language);
			# open(DC,">433Post_Responce.html");
			# print DC "$Content";
			# close DC;	
			# exit;
		}
		if($Council_Code=~m/^(?:438|38|149|185|207|320|326|329|378|455|339|216|260)$/is)
		{			
			my ($post_Url1,$post_Url2,$Post_Content_Tmp,$Post_Content_Tmp1);
			if($Council_Code=~m/^185$/is)
			{
				$post_Url1='https://'.$Host.'/EasySiteWeb/EasySite/StyleData/Civica/Resources/Resource/Civica/Handler.ashx/keyobject/search';
				$Post_Content_Tmp='{"refType":"APPPlanCase","fromRow":1,"toRow":1,"keyText":"'.$Application_No.'"}';
			}
			elsif($Council_Code=~m/^455$/is)
			{
				$post_Url1='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/keyobject/pagedsearch';
				$Post_Content_Tmp='{"refType":"PBDC","searchFields":{"ref_no":"'.$Application_No.'"}}';
			}
			elsif($Council_Code=~m/^339$/is)
			{
				$post_Url1='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/keyobject/search';
				my $Application_No1=$1 if($Document_Url=~m/KeyNo\=([^>]*?)\s*$/is);
				$Post_Content_Tmp='{"refType":"PBDC","fromRow":1,"toRow":1,"keyNumb":"'.$Application_No1.'"}';
			}
			elsif($Council_Code=~m/^216$/is)
			{
				$post_Url1='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/keyobject/search';
				$Post_Content_Tmp='{"refType":"PLANNINGCASE","fromRow":1,"toRow":1,"keyText":"'.$Application_No.'"}';
			}
			else
			{				
				$post_Url1='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/keyobject/pagedsearch';
				$Post_Content_Tmp='{"refType":"GFPlanning","searchFields":{"SDescription":"'.$Application_No.'"}}';
			}
			
			$Referer=~s/<APP_NUMBER>/$Application_No/igs;
			# print "post_Url1::$post_Url1\n\n";
			# print "Post_Content_Tmp::$Post_Content_Tmp\n\n";
			# print "Referer::$Referer\n\n";
			my($Post_Responce,$res_code)=&Download_Utility::Post_Method($post_Url1,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy);
			# print "Post_Responce::$Post_Responce\n\n";
			
			my $post2=$1 if($Post_Responce=~m/\"KeyNumber\"\s*:\s*"([^>]*?)\"/is);
			# print "post2::$post2\n\n";
			if($Council_Code=~m/^185$/is)
			{
				$post2=$1 if($Post_Responce=~m/\"keyText\"\s*:\s*"([^>]*?)\"/is);
				$post_Url2='https://'.$Host.'/EasySiteWeb/EasySite/StyleData/Civica/Resources/Resource/Civica/Handler.ashx/doc/list';
				$Post_Content_Tmp1='{"KeyNumb":"0","KeyText":"'.$post2.'","RefType":"APPPlanCase"}';
			}
			elsif($Council_Code=~m/^(?:455|339)$/is)
			{
				$post_Url2='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/doc/list';
				$Post_Content_Tmp1='{"KeyNumb":"'.$post2.'","RefType":"PBDC"}';
			}
			elsif($Council_Code=~m/^216$/is)
			{
				$post_Url2='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/doc/list';
				$Post_Content_Tmp1='{"KeyNumb":"0","KeyText":"'.$Application_No.'","RefType":"PLANNINGCASE"}';
			}
			else
			{
				$post_Url2='https://'.$Host.'/civica/Resource/Civica/Handler.ashx/doc/list';
				$Post_Content_Tmp1='{"KeyNumb":"'.$post2.'","KeyText":"Subject","RefType":"GFPlanning"}';
			}
		
			my($Post_Responce,$res_code)=&Download_Utility::Post_Method($post_Url2,$Host,$Content_Type,$Referer,$Post_Content_Tmp1,$Proxy);
			# print "Post_Responce::$Post_Responce\n\n";
			$Content=$Post_Responce;
		}
		if($Council_Code=~m/^5$/is)
		{
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,$URL,$Host,$Content_Type,"",$Proxy,$Need_Host,$Accept_Language);
			
			my $Viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
			my $Viewstategen=uri_escape($1) if($Content=~m/$Viewstategenerator_Regex/is);
			my $Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			my $Content1=$Content;
			my $Content2='';
			while($Content1=~m/<a[^>]*?__doPostBack\(\&\#39\;([^>]*?)\&\#39\;[^>]*?>\s*List\s*documents\s*<\/a>/igs)
			{
				my $Eventtarget=uri_escape($1);
				# $Document_Url='https://myservice.erewash.gov.uk/Planning/lg/GFPlanningSearch.page';
				
				$Post_Content=~s/<EVENTTARGET>/$Eventtarget/igs;
				$Post_Content=~s/([^>]*?\=)(gvDocs[^>]*?)(\&[^>]*?)$/$1$Eventtarget$3/igs;
				$Post_Content=~s/<VIEWSTATE>/$Viewstate/igs;
				$Post_Content=~s/<VIEWSTATEGENERATOR>/$Viewstategen/igs;
				$Post_Content=~s/<EVENTVALIDATION>/$Eventvalidation/igs;
				# print "Post_Content::$Post_Content\n";
				($Content,$Code)=&Download_Utility::Post_Method($Document_Url,$Host,$Content_Type,$Home_URL,$Post_Content,$Proxy);
				$Content2=$Content2.$Content;
				$Content='';
			}
			$Content=$Content2;
		}
		if($Document_Url eq "")
		{
			# print "Method:\t$Method. But Document URL not found for this App ID\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			if(($Doc_URL_Regex ne '') or ($Doc_URL_Regex ne 'N/A'))
			{
				if($Content=~m/$Doc_URL_Regex/is)
				{
					my $relatedDocURL= $1;
					my $DocUrl=$Abs_url.$relatedDocURL if($relatedDocURL!~m/^https?\:/is);
					($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($DocUrl,$URL,$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
				}
			}	
		}
		
		if($Code!~m/20/is)
		{
			my $pdfcount=0;
			my $No_Of_Documents_Avail=0;
			my $No_Of_Doc_Downloaded=0;
			my $pdf_name_list;
			my $pdf_Url_list;
			my $One_App_Avail_Status;
			my $Comments=$Code;
			$Document_Url=~s/\'/\'\'/igs;
			my $insert_query = "(\'$Format_ID\', \'$Council_Code\', \'$Markup\', \'$Document_Url\', \'$No_Of_Documents_Avail\', \'$One_App_Avail_Status\', \'$Downloaded_date\', \'$Comments\', \'$No_Of_Doc_Downloaded\'),";
			$Insert_OneAppLog.= $insert_query;
			
			my $Update_Query = "Update TBL_ONEAPP_STATUS set Current_Status = \'Terminated\', Responce_Code = \'$Code\' where Council_Code = \'$Council_Code\' and Scraped_Date= \'$Downloaded_date\';";
			$Update_Query.= $Update_Query;
		}
	}
	elsif($Method eq 'Search')
	{
		($Content, $Post_Res_code)=&Search_Application($Application_No);
		# open(DC,">Post_Responce.html");
		# print DC "$Content";
		# close DC;	

		if($Council_Code=~m/^132$/i)
		{
			# open(DC,">132Content.html");
			# print DC $Content;
			# close DC;
			# exit;
		}		
	}	
	
	my $Post_Responce_Concat;
	my $Navigate_Flag = 0;
	
	if($Navigate_By_Post1 eq 'Y')
	{
		if($Navi1_Post_Url=~m/^\s*Document_Url\s*$/is)
		{
			$Navi1_Post_Url=$Document_Url;
		}
		if($Referer=~m/^\s*Document_Url\s*$/is)
		{
			$Referer=$Document_Url;
		}
		while($Content=~m/$Navi1_Info_Regex1/igs)
		{
			my $captured_Info1=$1;
			my $captured_Info2=$2;
			my $captured_Info3=$3;
			if($Navi1_Info_Need_UriEscape ne 'N')
			{
				$captured_Info1=uri_escape($captured_Info1);
				$captured_Info2=uri_escape($captured_Info2);
				$captured_Info3=uri_escape($captured_Info3);
			}
			my $Viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
			my $Viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
			my $Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			my $CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
			my $Navi1_Post_Content_Tmp=$Navi1_Post_Content;
			$Navi1_Post_Content_Tmp=~s/<Replace_Info1>/$captured_Info1/igs;
			$Navi1_Post_Content_Tmp=~s/<Replace_Info2>/$captured_Info2/igs;
			$Navi1_Post_Content_Tmp=~s/<Replace_Info3>/$captured_Info3/igs;
			$Navi1_Post_Content_Tmp=~s/<VIEWSTATE>/$Viewstate/igs;
			$Navi1_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Viewstategenerator/igs;
			$Navi1_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Eventvalidation/igs;
			$Navi1_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
			$Navi1_Host = $Host if($Navi1_Host=~m/N\/A/is);
			($Post_Responce,my $res_code)=&Download_Utility::Post_Method($Navi1_Post_Url,$Navi1_Host,$Content_Type,$Referer,$Navi1_Post_Content_Tmp,$Proxy);
			if($Council_Code=~m/^323$/is)
			{
				if($Post_Responce=~m/var\s*model\s*=\s*([\w\W]*?);/is)
				{
					my $subcon=$1;
					$subcon=~s/txt[\w\W]*?$//igs;
					$subcon=~s/\,\s*$//igs;
					$Post_Responce=$subcon;
					# print "Post_Responce:::$Post_Responce\n";<STDIN>;
				}
			}
			$Navigate_Flag = 1;
			# if($Format eq 'Table')
			# {
				# my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($Post_Responce,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY);
				# my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				# %Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
				# # $Doc_Details_Hash_Ref = ($Doc_Details_Hash_Ref,$Doc_Details_Hash_Ref_tmp);
				# $Content=$Post_Responce;
			# }
			$Post_Responce_Concat=$Post_Responce_Concat.$Post_Responce;
		}
	}
	
	if($Navigate_Flag == 1)
	{
		$Content=$Post_Responce_Concat;
	}	
	
	Proceed_without_GET:
	if($Landing_Page_Flag == 0)
	{
		if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
		{
			if($Content=~m/$Landing_Page_Regex/is)
			{
				my $DocUrl=$1;
				$DocUrl=~s/\s\s+/ /igs;
				$DocUrl=~s/&#xD;&#xA;\s*//igs;
				$DocUrl=~s/\&amp;/\&/igs;
				my $u1;
				if($Landing_Page_ABS ne 'N/A')
				{
					$Document_Url=$Landing_Page_ABS.$DocUrl;
				}
				elsif($Document_Url!~m/^\s*$/is)
				{
					$u1=URI::URL->new($DocUrl,$Document_Url);
					$Document_Url=$u1->abs;
				}
				elsif($URL!~m/^\s*$/is)	
				{
					$u1=URI::URL->new($DocUrl,$URL);
					$Document_Url=$u1->abs;
				}	
				$Landing_Page_Flag = 1;
				$Method='GET';
				if( $Council_Code eq 424 )
				{
					$Document_Url=$Landing_Page_ABS;
					$Document_Url='http://plandocs.waverley.gov.uk/Planning/lg/dialog.page?org.apache.shale.dialog.DIALOG_NAME=gfplanningsearch&Param=lg.Planning&SDescription='.$Application_No.'&viewdocs=true';
				}
				print "Document_Url:> $Document_Url\n";
				goto Re_Ping;
			}
		}	
	}	
	if($Landing_Page_Flag2 == 0)
	{
		if(($Landing_Page_Regex2 ne 'N/A') and ($Landing_Page_Regex2 ne ''))
		{
			if($Content=~m/$Landing_Page_Regex2/is)
			{
				my $DocUrl=$1;
				my $u1;
				if($Document_Url!~m/^\s*$/is)
				{
					$u1=URI::URL->new($DocUrl,$Document_Url);
				}
				elsif($URL!~m/^\s*$/is)	
				{
					$u1=URI::URL->new($DocUrl,$URL);
				}	
				$Document_Url=$u1->abs;
				print "Landing URL: $Document_Url\n";
				$Landing_Page_Flag2 = 1;
				
				if( $Council_Code eq 97 )
				{
					
					$Document_Url='http://www1.arun.gov.uk/PublicViewer/Authenticated/Results.aspx?tab=ALL%20RECORDS';
					print "Document_Url l2 --> $Document_Url";
				}
				
				
				goto Re_Ping;
			}
		}	
	}	
	if($Landing_Page_Flag3 == 0)
	{
		if(($Landing_Page_Regex3 ne 'N/A') and ($Landing_Page_Regex3 ne ''))
		{
			if($Content=~m/$Landing_Page_Regex3/is)
			{
				my $DocUrl=$1;
				my $u1;
				if($Document_Url!~m/^\s*$/is)
				{
					$u1=URI::URL->new($DocUrl,$Document_Url);
				}
				elsif($URL!~m/^\s*$/is)	
				{
					$u1=URI::URL->new($DocUrl,$URL);
				}	
				$Document_Url=$u1->abs;
				print "Landing URL: $Document_Url\n";
				$Landing_Page_Flag3 = 1;
				goto Re_Ping;
			}
		}	
	}	
	
	my ($Total_no_Pages);
	if($Total_No_Of_Pages_Regex ne 'N/A')
	{
		if($Content=~m/$Total_No_Of_Pages_Regex/is)
		{
			$Total_no_Pages=$1;
		}
	}
	print "Total_no_Pages: $Total_no_Pages\n";
	if($Available_Doc_Count_Regex ne 'N/A')
	{
		if($Content=~m/$Available_Doc_Count_Regex/is)
		{
			$Available_Doc_Count=$1;
		}
	}	
	my $page_inc=1;
	
	my $Ltime = localtime();
	$Ltime=~s/\s+/_/igs;
	$Ltime=~s/\://igs;
	print "Time: $time\n";
	
	my $htmlFileLocation = '//172.27.137.202/One_app_download/SourceFiles/'.$Council_Code.'_'.$Format_ID.'_'.$Ltime.'.html';
	# print "htmlFileLocation: $htmlFileLocation\n";
	# open(DC,">$htmlFileLocation");
	# print DC "$Content";
	# close DC;	
	# exit;
	
	
	if($Council_Code=~m/^21$/is)
	{
		my $KeyNo=$1 if($Document_Url=~m/KeyNo\=([\d]+)\&/is);
		$Post_Content=~s/<KeyNo>/$KeyNo/igs;
				
		($Content,$Post_Res_code)=&Download_Utility::Post_Method($Search_URL,$Host,$Content_Type,$Document_Url,$Post_Content,$Proxy);
	}
	if($Council_Code=~m/^31$/is)
	{
		$URL="https://planningsearch.rbkc.gov.uk/publisher/mvc/getDocumentList?";
		($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($URL,'',$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
	}
	Next_Page:
	if($Format eq 'Table')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_Docs_Details($Content,$Home_URL,\%Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
		# $Doc_Details_Hash_Ref = ($Doc_Details_Hash_Ref,$Doc_Details_Hash_Ref_tmp);
	}
	elsif($Format eq 'JSON')
	{
		my $Doc_Details_Hash_Ref_tmp=&Collect_JsonDocs_Details($Content,$Application_No,$Host);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
	}

	if($Pagination_Type eq 'N/A')
	{
		return (\%Doc_Details_Hash,$Document_Url,$Code);
	}
	elsif($Pagination_Type eq 'By Number')
	{
		print "page_inc	: $page_inc\n";
		print "Total_no_Pages	: $Total_no_Pages\n";
		if($page_inc < $Total_no_Pages)
		{
			my ($Next_page_link,$Next_Page_Values);
			my @Next_Page_Values = $Content=~m/$Next_Page_Regex/g;
			if($Next_Page_Replace_Info ne 'N/A')
			{
				my @Replace_array_Raw=split('\|',$Next_Page_Replace_Info);
				
				if($Replace_array_Raw[0]=~m/Redir_Url/is)
				{
					$Next_page_link = $Redir_Url;
				}
				else
				{
					$Next_page_link = $Replace_array_Raw[0];
				}	
				
				
				if($Replace_array_Raw[2]=~m/AllValues/is)
				{
					$Next_Page_Values = join("",@Next_Page_Values);
				}	

				
				if($Replace_array_Raw[1]=~m/Replace/is)
				{
					for my $Replace_Key(keys %Nextpage_Replace_Hash)
					{
						if($Nextpage_Replace_Hash{$Replace_Key} eq '')
						{
							$Next_page_link=~s/$Replace_Key/$Next_Page_Values/igs;
						}	
						else
						{
							$Next_page_link=~s/$Replace_Key/$Nextpage_Replace_Hash{$Replace_Key}/igs;
						}
					}
				}
				# print "Next_page_link: $Next_page_link\n";
				# elsif($Replace_array_Raw[1]=~m/Append/is)
				# {
					# $Next_Page_Values = join("",@Next_Page_Values);
				# }
			}
			$page_inc++;
			($Content,my $code,my $reurl)=&Download_Utility::Getcontent($Next_page_link,"","","","",$Proxy,$Need_Host,$Accept_Language);
			goto Next_Page;
		}	
	}
	elsif($Pagination_Type eq 'By Format')
	{
		print "Next Navigation By Format\n";
		# if($Council_Code=~m/^132$/is)
		# {
			# open(PP,">132.html");
			# print PP $Content;
			# close(PP);
			# exit;
		# }
		if($Content=~m/$Current_Page_Number_Regex/is)
		{
			my $Current_Page=$1;
			my ($Doc_viewstate,$Doc_viewstategenerator,$CSRFToken,$Doc_Eventvalidation);
			print "Current_Page: $Current_Page\n";
			
			if($Viewstate_Regex ne 'N/A')
			{
				$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
				# print "Doc_viewstate: $Doc_viewstate\n";
			}	
			if($Viewstategenerator_Regex ne 'N/A')
			{
				$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
			}	
			if($CSRFToken_Regex ne 'N/A')
			{
				$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
			}	
			if($Eventvalidation_Regex ne 'N/A')
			{
				$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
			}	
			print "Page 1:: $Previous_page = $Current_Page\n";
			if($Previous_page ne $Current_Page)
			{
				print "Page 2:: $Previous_page = $Current_Page\n";
				# $Previous_page=$Current_Page;
				$Previous_page++;
				my $Next_Page_Post_Content_Tmp = $Next_Page_Post_Content;
				$Next_Page_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
				$Next_Page_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
				$Next_Page_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
				$Next_Page_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
				
				# ($Content)=&Download_Utility::Postcontent($Next_Page_Post_Content_Tmp);
				$Doc_Post_Host=$Host if($Doc_Post_Host eq 'N/A');
				$Doc_Post_URL=$Document_Url if($Doc_Post_URL eq 'N/A');
				
				$Doc_Post_URL='https://myservice.erewash.gov.uk/Planning/lg/GFPlanningDocuments.page' if($Council_Code=~m/^200$/is);
				
				
				($Content,my $res_code)=&Download_Utility::Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Next_Referer,$Next_Page_Post_Content_Tmp,$Proxy);
				
				# open(my $fh,">next.html");
				# print $fh $Content;
				# close($fh);
				
				
				goto Next_Page;
			}
		}		
		else
		{
			print "Current page number regex not matched";
			
		}
	}
	return (\%Doc_Details_Hash,$Document_Url,$Code,$Available_Doc_Count);
}



sub Collect_JsonDocs_Details()
{
	my $Content=shift;
	my $Application_No=shift;
	my $Host=shift;
	my %Doc_Details;
	
	next if($Content=~m/^\s*\{\s*\"RowCount\"\s*:\s*\"0\"\s*\}\s*$/is);
	
	my ($Doc_URL,$Doc_Desc,$pdf_name,$DateReceived,$document_type);
	my $json = JSON->new;
	my $data = $json->decode($Content);
	# print $data;
	eval{
		if($Council_Code=~m/^(?:438|38|149|185|207|320|326|329|378|455|339|216|260|9|323)$/is)
		{			
			my @navigation_menus=@{$data->{'CompleteDocument'}};
			foreach my $Keys ( @navigation_menus) 
			{	
				$Doc_Desc = $Keys->{'DocDesc'};
				$document_type = $Keys->{'DocCategory'};
				my $DocNo = $Keys->{'DocNo'};
				$DateReceived = $Keys->{'docDate'};
				$DateReceived = $Keys->{'DocDate'} if($DateReceived=~m/^\s*$/is);
				# print $Doc_Desc."\n";
				# print $document_type."\n";
				print $DocNo."\n";
				# print $DateReceived."\n";
				# print $Doc_Download_URL_Replace."\n";
				my $tempURL;
				if($Council_Code=~m/^185$/is)
				{
					$tempURL = 'https://'.$Host.'/EasySiteWeb/EasySite/StyleData/Civica/Resources/Resource/Civica/Handler.ashx/doc/pagestream?DocNo=';
				}
				elsif($Council_Code=~m/^9$/is)
				{
					$tempURL = 'https://'.$Host.'/civica/Resource/Civica/Handler.ashx/doc/pagestream?DocNo=<SelectedID1>&pdf=true';
				}
				else
				{				
					$tempURL = 'https://'.$Host.'/civica/Resource/Civica/Handler.ashx/Doc/pagestream?cd=inline&pdf=true&docno=';
				}
				
				$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
				# $tempURL=~s/<SelectedID1>/$DocNo/gsi;
				if($Council_Code=~m/^9$/is)
				{
					$tempURL=~s/<SelectedID1>/$DocNo/gsi;
					$Doc_URL = $tempURL;
				}
				else
				{
					$Doc_URL = $tempURL.$DocNo;	
				}
				$document_type = &Download_Utility::trim($document_type);			
				$Doc_Desc=~s/\//\-/gsi;
				$document_type=~s/\//\-/gsi;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
							
				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
												
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				# <STDIN>; 
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}			
			}
		}
		elsif($Council_Code=~m/^(95)$/is)
		{
			# my @KeyObjects = @{$data->{'array'}};
			
			foreach my $Keys ( @{$data}) 
			{	
				$Doc_Desc = $Keys->{'docDescription'};
				$document_type = $Keys->{'docType'};
				my $DocNo = $Keys->{'docId'};
				$DateReceived = $Keys->{'docDate'};
				
				print $Doc_Desc."\n";
				print $document_type."\n";
				print $DocNo."\n";
				print $DateReceived."\n";
				
				my $tempURL = $Doc_Download_URL_Replace;
				
				$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
				$tempURL=~s/<SelectedID1>/$DocNo/gsi;
				$Doc_URL = $tempURL;	
				$document_type = &Download_Utility::trim($document_type);			
				$Doc_Desc=~s/\//\-/gsi;
				$document_type=~s/\//\-/gsi;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				
				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
									
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				# 
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
				
			}
		}
		elsif($Council_Code=~m/^(433)$/is)
		{
			my @navigation_menus=@{$data->{'data'}};
			foreach my $Keys ( @navigation_menus) 
			{	
				$Doc_Desc = $Keys->[3];
				$document_type = $Keys->[2];
				my $DocNo = $Keys->[4];
				$DateReceived = $Keys->[1];
				
				print $Doc_Desc."\n";
				print $document_type."\n";
				print $DocNo."\n";
				print $DateReceived."\n";
				# <STDIN>;
				$Doc_URL = $Doc_Download_URL_Replace.$DocNo;
				
				$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
				# $tempURL=~s/<SelectedID1>/$DocNo/gsi;
				# $Doc_URL = $tempURL;	
				$document_type = &Download_Utility::trim($document_type);			
				$Doc_Desc=~s/\//\-/gsi;
				$document_type=~s/\//\-/gsi;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				
				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
									
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				# 
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
				
			}
		}
		elsif($Council_Code=~m/^(31)$/is)
		{
			my @navigation_menus=@{$data->{'data'}};
			foreach my $Keys ( @navigation_menus) 
			{	
				$Doc_Desc = $Keys->[2];
				$document_type = $Keys->[1];
				my $DocNo = $Keys->[3];
				$DateReceived = $Keys->[0];
				
				print $Doc_Desc."\n";
				print $document_type."\n";
				print $DocNo."\n";
				print $DateReceived."\n";
				# <STDIN>;
				$Doc_URL = $Doc_Download_URL_Replace.$DocNo;
				
				$DateReceived=~s/^([^<]*?)T[^<]*?$/$1/gsi;	
				# $tempURL=~s/<SelectedID1>/$DocNo/gsi;
				# $Doc_URL = $tempURL;	
				$document_type = &Download_Utility::trim($document_type);			
				$Doc_Desc=~s/\//\-/gsi;
				$document_type=~s/\//\-/gsi;
				
				my ($file_type);
				if($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
				{
					$file_type=$1;
				}
				else
				{
					$file_type='pdf';
				}	
				
				
				if($Doc_Desc!~m/^\s*$/is)
				{
					$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
				}	
				else
				{
					$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
				}
				
									
				$pdfcount_inc++;
				
				print "Doc_URL	: $Doc_URL\n";
				print "pdf_name	: $pdf_name\n";
				print "file_type: $file_type\n";
				print "DateReceived: $DateReceived\n";
				# 
				
				if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
				{
					$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$DateReceived,$Doc_Desc];
				}	
				
			}
		}
	};
	if($@)
	{
		print "Excepytion ERR::: $@\n";
	}
	
	Loop_End:
	return (\%Doc_Details);	
}

sub Collect_Docs_Details()
{
	my $Content=shift;
	my $Home_URL=shift;
	my $Nextpage_Replace_Hash=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;
	my $Doc_URL_BY=shift;
	my %Doc_Details;
	my %Nextpage_Replace_Hash=%$Nextpage_Replace_Hash;
	my %Doc_Details_Hash;
	$Content=~s/<!--[\w\W]*?-->//igs;
	print "content\n";
	if (($Doc_Download_URL_Index =~m/([a-z]+)/is) and ($Doc_Download_URL_Index ne 'N/A'))
	{
		
		my ($Doc_Details_Hash_Ref_tmp,$p)=&Download_Utility::Format1_Extented_Html_Header($Config,$Content,$Home_URL,$Nextpage_Replace_Hash,$Redir_Url,$Document_Url,$URL,$Doc_URL_BY,$Council_Code,$pdfcount_inc);
		my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
		%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);
		
		$pdfcount_inc=$p;
	
	}
	else
	{
		$Content=~s/<td\/>/<td><\/td>/igs;
		$Content=~s/<!--[\w\W]*?-->//igs;

		if($Block_Regex1 ne 'N/A')
		{
			my @Block1 = $Content =~m/$Block_Regex1/igs;
			if($Block_Regex2 ne 'N/A')
			{
				for my $Block1(@Block1)
				{	
					my @Block2 = $Block1=~m/$Block_Regex2/igs;
					
					if($Block_Regex3 ne 'N/A')
					{
						for my $Block2(@Block2)
						{	
							my @Block3 = $Block2=~m/$Block_Regex3/igs;
							my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block3,$Redir_Url,$Document_Url,$URL,$Content);
							my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
							%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
						}	
					}
					else
					{
						my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block2,$Redir_Url,$Document_Url,$URL,$Content);
						my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
						%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
					}	
				}
			}	
			else
			{
				my $Doc_Details_Hash_Ref_tmp=&Field_Mapping(\@Block1,$Redir_Url,$Document_Url,$URL,$Content);
				my %Doc_Details_Hash_Ref_tmp=%$Doc_Details_Hash_Ref_tmp;
				%Doc_Details_Hash = (%Doc_Details_Hash,%Doc_Details_Hash_Ref_tmp);					
			}	
		}
	}
	return \%Doc_Details_Hash;
}

sub Field_Mapping()
{
	my $Data=shift;
	my $Redir_Url=shift;
	my $Document_Url=shift;
	my $URL=shift;	
	my $Content=shift;	
	my @Data = @$Data;
	my ($document_type,$Doc_File_Format_Cont,$PDF_URL_Cont,$Doc_URL,$Doc_Desc,$Published_Date,$pdf_name,$Post_Responce,$res_code);
	my $Root_Url;
	my %Doc_Details;
	
	if($Redir_Url ne '')
	{
		$Root_Url=$Redir_Url;
	}
	elsif($Document_Url ne '')
	{
		$Root_Url=$Document_Url;
	}
	elsif($URL ne '')
	{
		$Root_Url=$URL;
	}

	# my $cnt=0;
	# for my $d(@Data)
	# {
		# print "$cnt: $d\n";
		# $cnt++;
	# }
	$Doc_File_Format_Cont = &Download_Utility::trim($Data[$Doc_File_Format_Index]) if($Doc_File_Format_Index ne 'N/A');
	$PDF_URL_Cont = &Download_Utility::trim($Data[$Doc_Download_URL_Index]) if($Doc_Download_URL_Index ne 'N/A');
	$document_type = &Download_Utility::trim($Data[$Doc_Type_Index]) if($Doc_Type_Index ne 'N/A');
	$Doc_Desc = &Download_Utility::trim($Data[$Doc_Description_Index]) if($Doc_Description_Index ne 'N/A');
	$Published_Date = &Download_Utility::trim($Data[$Doc_Published_Date_Index]) if($Doc_Published_Date_Index ne 'N/A');
	print "PDF_URL_Cont:\n$PDF_URL_Cont\n";

	if($Doc_Published_Date_Regex ne 'N/A')
	{
		if($Published_Date=~m/$Doc_Published_Date_Regex/is)
		{
			$Published_Date=$1;
		}
	}
	if($Doc_Type_Regex ne 'N/A')
	{
		if($document_type=~m/$Doc_Type_Regex/is)
		{
			$document_type=$1;
		}
	}
	if($Doc_Description_Regex ne 'N/A')
	{
		if($Doc_Desc=~m/$Doc_Description_Regex/is)
		{
			$Doc_Desc=$1;
		}
	}
	
	if($Doc_URL_BY eq 'POST')
	{
		my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$Doc_POST_ID2,$Doc_POST_ID3,$CSRFToken);
		my $Doc_Post_Content_Tmp=$Doc_Post_Content;
		if($Viewstate_Regex ne 'N/A')
		{
			$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
		}
		if($Viewstategenerator_Regex ne 'N/A')
		{
			$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
		}	
		if($CSRFToken_Regex ne 'N/A')
		{
			$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
		}	
		if($Eventvalidation_Regex ne 'N/A')
		{
			$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
		}
		if($Doc_Page_Num_Regex ne 'N/A')
		{
			$Doc_Page_Num=$1 if($Content=~m/$Doc_Page_Num_Regex/is);
		}
		if($Doc_NumberOfRows_Regex ne 'N/A')
		{
			$Doc_NumberOfRows=$1 if($Content=~m/$Doc_NumberOfRows_Regex/is);
		}	
		if($PDF_URL_Cont=~m/$Doc_POST_ID1_Regex/is)
		{
			$Doc_POST_ID1=uri_escape($1);
			$Doc_POST_ID2=uri_escape($2);
			$Doc_POST_ID3=uri_escape($3);
		}

		$Doc_Post_URL = $Document_Url if($Doc_Post_URL=~m/Document_Url/is);
		$Referer = $Document_Url if($Referer=~m/Document_Url/is);
		$Doc_Post_URL=decode_entities($Doc_Post_URL);
		$Referer=decode_entities($Referer);
		$Doc_Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
		$Doc_Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
		$Doc_Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
		$Doc_Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
		$Doc_Post_Content_Tmp=~s/<PageNum>/$Doc_Page_Num/igs;
		$Doc_Post_Content_Tmp=~s/<PageRows>/$Doc_NumberOfRows/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID1>/$Doc_POST_ID1/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID2>/$Doc_POST_ID2/igs;
		$Doc_Post_Content_Tmp=~s/<SelectedID3>/$Doc_POST_ID3/igs;
		$Doc_Post_Host=$Host if($Doc_Post_Host eq '');
		
		print "Content_Type==>$Content_Type\n";
		print "Doc_Post_URL==>$Doc_Post_URL\n";
		print "Doc_Post_Host==>$Doc_Post_Host\n";
		print "Referer==>$Referer\n";
		print "Doc_Post_Content_Tmp==>$Doc_Post_Content_Tmp\n";<STDIN>;
		
		
		
		my $applicationNo=$1 if($Content=~m/<[^>]*?\"w2documentViewKeyObjectInfo\">\s*Case\s*\:\s*([^>]*?)\s*\,/is);
		
		# $Referer="https://www2.ashfield.gov.uk/Planning/lg/plansearch.page?org.apache.shale.dialog.DIALOG_NAME=gfplanningsearch&Param=lg.Planning&viewdocs=true&SDescription=$applicationNo" if($Council_Code==98);
		
		
		($Post_Responce,$res_code)=&Download_Utility::Post_Method($Doc_Post_URL,$Doc_Post_Host,$Content_Type,$Referer,$Doc_Post_Content_Tmp,$Proxy);
		
		# undef $Doc_Post_Content_Tmp;
		# if($Council_Code=~m/^98$/is)
		# {
			open(DC,">Post_Responce 98.html");
			print DC "$Post_Responce";
			close DC;
		# }
		
		my $URL_ID;
		if($Post_Responce=~m/$Doc_Download_URL_Regex/is)
		{
			my $val=$1;
			if($DOC_PDF_ID_URL=~m/ID/is)
			{
				$URL_ID=$val;
			}
			elsif($DOC_PDF_ID_URL=~m/URL/is)
			{
				my $u1=URI::URL->new($val,$Doc_Post_URL);
				my $u2=$u1->abs;
				$Doc_URL=$u2;
			}
		}
		if($Doc_Download_URL_Replace ne 'N/A')
		{
			# next if($PDF_URL_Cont == '');
			$Doc_URL = $Doc_Download_URL_Replace;
			$Doc_URL=~s/<SelectedID>/$URL_ID/igs;
		}
	}
	elsif($Doc_Download_URL_Regex ne 'N/A')
	{
		if($PDF_URL_Cont=~m/$Doc_Download_URL_Regex/is)
		{
			my $Durl1=$1;
			my $Durl2=$2;
			my $Durl3=$3;
			
			if($Council_Code == 450)
			{
				$Durl1="http://planning.wrexham.gov.uk/Planning/StreamDocPage/obj.pdf?DocNo=$Durl1&PageNo=1&content=obj.pdf";
			}
			
			if($Council_Code==21)
			{
				my $abr_lk=$1 if($Document_Url=~m/^([^>]*?)\#/is);
				
				$Durl1=$abr_lk."#DOC?DocNo=$Durl1";
			}
			
			if($Doc_Download_URL_Replace ne 'N/A') 
			{
				print "Doc_Download_URL_Replace: $Doc_Download_URL_Replace\n";
				if($Doc_Download_URL_Replace=~m/Redir_Url/is)
				{
					my $Doc_Download_URL_Replace_tmp=$Redir_Url;
					my @Replace_URL_Split=split('\|',$Doc_Download_URL_Replace);
					$Doc_Download_URL_Replace_tmp=~s/$Replace_URL_Split[1]/$Durl1/igs;
					$Doc_URL=$Doc_Download_URL_Replace_tmp;
					print "\nDoc_Download_URL_Replace::Doc_URL: $Doc_URL";
					
				}
				else
				{
					my $Doc_Download_URL_Replace_tmp=$Doc_Download_URL_Replace;
					$Doc_Download_URL_Replace_tmp=~s/<SelectedID1>/$Durl1/igs;
					$Doc_Download_URL_Replace_tmp=~s/<SelectedID2>/$Durl2/igs;
					$Doc_Download_URL_Replace_tmp=~s/<SelectedID3>/$Durl3/igs;
					$Doc_URL=$Doc_Download_URL_Replace_tmp;
				}	
			}
			elsif($Doc_Download_URL_ABS ne 'N/A') 
			{
				print "Doc_Download_URL_ABS: $Doc_Download_URL_ABS\n";
				$Doc_URL=$Doc_Download_URL_ABS.$Durl1;
				print "\nDoc_Download_URL_ABS::Doc_URL1: $Doc_URL\n";
			}
			else
			{
				if($Durl1!~m/http/is)
				{
					my $u1=URI::URL->new($Durl1,$Root_Url);
					my $u2=$u1->abs;
					$Doc_URL=$u2;
					print "\nDoc_URL2:--> $Doc_URL\n";
				}
				else
				{
					$Doc_URL=$Durl1;
				}
			}	
		}
		if($Doc_Download_URL_redirct eq 'Y')
		{
			my ($Redir_Content, $code, $reurl)=&Download_Utility::Getcontent($Doc_URL,"","","","",$Proxy,$Need_Host,$Accept_Language);
			if($Redir_Content=~m/$Doc_Download_URL_redirct_Regex/is)
			{
				my $Durl=$1;
				if($Doc_Download_URL_redirct_ABS ne 'N/A')
				{
					$Doc_URL=$Doc_Download_URL_redirct_ABS.$Durl;
				}
				else
				{
					my $u1=URI::URL->new($Durl,$Doc_URL);
					my $u2=$u1->abs;
					$Doc_URL=$u2;
				}	
			}
		}		
	}
	elsif($Doc_Download_URL_Replace ne 'N/A')
	{
		next if($PDF_URL_Cont == '');
		$Doc_URL = $Doc_Download_URL_Replace;
		$Doc_URL=~s/<SelectedID>/$PDF_URL_Cont/igs;
	}

	$Doc_Desc = &Download_Utility::Clean($Doc_Desc);
	$document_type = &Download_Utility::Clean($document_type);
	$Published_Date = &Download_Utility::Clean($Published_Date);
	
	my ($file_type);
	
	if($Doc_File_Format_Regex ne 'N/A')
	{
		
		if($Doc_File_Format_Cont=~m/$Doc_File_Format_Regex/is)
		{
			$file_type=$1;
		}
	}
	elsif($Doc_URL=~m/\.([\w]{3,4})\s*$/is)
	{
		$file_type=$1;
	}
	else
	{
		$file_type='pdf';
	}	

	if($Doc_Desc!~m/^\s*$/is)
	{
		$pdf_name = $pdfcount_inc."_".$Doc_Desc.".".$file_type;
	}	
	else
	{
		$pdf_name = $pdfcount_inc."_".$document_type.".".$file_type;
	}
	
	$pdf_name = &Download_Utility::PDF_Name($pdf_name);
	$Doc_URL=decode_entities($Doc_URL);
	$pdfcount_inc++;
	print "\nDoc_URL\t\t: $Doc_URL\n";
	print "pdf_name	: $pdf_name\n";
	print "Doc_Type	: $document_type\n";
	print "Published_Date : $Published_Date\n";

	if(($Doc_URL!~m/^\s*$/is) && ($pdf_name!~m/^\s*$/is))
	{
		$Doc_Details{$Doc_URL}=[$document_type,$pdf_name,$Published_Date,$Doc_Desc];
	}
	return (\%Doc_Details);
}	

sub Search_Application()
{
	my $Application_No = shift;
	$Application_No = uri_escape($Application_No) if($App_No_UriEscape eq 'Y');
	
	my ($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Home_URL,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
	
	
	if($Council_Code=~m/^393$/i)
	{
		# open(DC,">393Content.html");
		# print DC $Content;
		# close DC;
	}
	my ($Doc_viewstate,$Doc_viewstategenerator,$Doc_Eventvalidation,$Doc_Page_Num,$Doc_NumberOfRows,$Doc_POST_ID1,$CSRFToken);
	if($Viewstate_Regex ne 'N/A')
	{
		$Doc_viewstate=uri_escape($1) if($Content=~m/$Viewstate_Regex/is);
	}	
	if($Viewstategenerator_Regex ne 'N/A')
	{
		$Doc_viewstategenerator=$1 if($Content=~m/$Viewstategenerator_Regex/is);
	}	
	if($CSRFToken_Regex ne 'N/A')
	{
		$CSRFToken=uri_escape($1) if($Content=~m/$CSRFToken_Regex/is);
	}	
	if($Eventvalidation_Regex ne 'N/A')
	{
		$Doc_Eventvalidation=uri_escape($1) if($Content=~m/$Eventvalidation_Regex/is);
	}	

	my $token=uri_escape($1) if($Content=~m/<input[^>]*?name=\"org\.apache\.struts\.taglib\.html\.TOKEN\"\s*value=\"([^\"]*?)\"[^>]*?>/is);

	eval{
	$Document_Url=@{$Document_Url}[0];
	# $Document_Url=@$Document_Url[0];
	};
	$Referer = $Document_Url if($Referer=~m/Document_Url/is);
	my $Post_Content_Tmp=$Post_Content;
	$Post_Content_Tmp=~s/<VIEWSTATE>/$Doc_viewstate/igs;
	$Post_Content_Tmp=~s/<VIEWSTATEGENERATOR>/$Doc_viewstategenerator/igs;
	$Post_Content_Tmp=~s/<EVENTVALIDATION>/$Doc_Eventvalidation/igs;
	$Post_Content_Tmp=~s/<CSRFTOKEN>/$CSRFToken/igs;
	$Post_Content_Tmp=~s/<APP_NUMBER>/$Application_No/igs;
	$Post_Content_Tmp=~s/<TOKEN>/$token/igs;
	# $Post_Content_Tmp='{"caseID": "CB/18/01656/RM"}';
	
	if($Council_Code=~m/^103$/is)
	{
		my $salt=$1 if($Content=~m/value\s*=\s*"\s*([^>]*?)\s*"[^>]*?id\s*=\s*"pSalt\s*"/is);
		my $p_instance=$1 if($Content=~m/value\s*=\s*"\s*([^>]*?)\s*"[^>]*?id\s*=\s*"pInstance\s*"/is);
		my $requst=$1 if($Content=~m/"\s*ajaxIdentifier\s*"\s*:\s*"\s*([^>]*?)\s*"/is);
		my $worksheet_id=$1 if($Content=~m/worksheet_id"[^>]*?value\s*=\s*"\s*([^>]*?)\s*"/is);
		my $report_id=$1 if($Content=~m/report_id"[^>]*?value\s*=\s*"\s*([^>]*?)\s*"/is);
		$Post_Content_Tmp=~s/<REPORT_ID>/$report_id/igs;
		$Post_Content_Tmp=~s/<WORKSHEET_ID>/$worksheet_id/igs;
		$Post_Content_Tmp=~s/<REQUEST>/$requst/igs;
		$Post_Content_Tmp=~s/<INSTANCE>/$p_instance/igs;
		$Post_Content_Tmp=~s/<SALT>/$salt/igs;
	}
	
	$Search_URL = $Home_URL if($Search_URL eq 'N/A');
	my($Post_Responce,$res_code)=&Download_Utility::Post_Method($Search_URL,$Host,$Content_Type,$Referer,$Post_Content_Tmp,$Proxy);
	
	# open(DC,">Search_Post_Responce.html");
	# print DC "$Post_Responce";
	# close DC;
	
	if($Council_Code=~m/^323$/is)
	{
		if($Post_Responce=~m/var\s*model\s*=\s*([\w\W]*?);/is)
		{
			$Post_Responce=$1;
		}
	}
	if($Council_Code=~m/^103$/is)
	{
		$Post_Responce=decode_entities($Post_Responce);
		my $doc_url=$1 if($Post_Responce=~m/javascript\:apex\.navigation\.dialog\(\s*'\s*([^>]*?)\s*'/is);		
		$doc_url=~s/\\u0026/&/igs;
		my $fin_url="https://webapps.barrowbc.gov.uk/webapps/$doc_url";
		
		($Post_Responce,$Code,$Redir_Url) =&Download_Utility::Getcontent($fin_url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
		$Post_Responce=decode_entities($Post_Responce);
		$Post_Responce=~s/<tr[^>]*?>\s*<th[^>]*?>[\w\W]*?<\/th>\s*<\/tr>//igs;
	}
	
	if(($Landing_Page_Regex ne 'N/A') and ($Landing_Page_Regex ne ''))
	{
		if($Post_Responce=~m/$Landing_Page_Regex/is)
		{
			my $DocUrl=$Abs_url.$1;
			print "Document_Url:::: $DocUrl\n";
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			# $Document_Url=~s/\&amp;/\&/igs;
			$Document_Url=decode_entities($Document_Url);
			# print "Document_Url:::: $Document_Url\n"; 
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			# open(DC,">Landing_Content.html");
			# print DC $Content;
			# close DC;
		}
		elsif($Post_Responce=~m/$Application_Regex/is)
		{
			my $DocUrl=$Abs_url.$1;
			print "Document_Url:::: $DocUrl\n";
			my $u1=URI::URL->new($DocUrl,$Home_URL);
			$Document_Url=$u1->abs;
			$Document_Url=~s/&#x[a-z]+?;//igs;
			$Document_Url=~s/%09+//igs;
			$Document_Url=decode_entities($Document_Url);
			# print "Document_Url:::: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			# open(DC,">Application_Content.html");
			# print DC $Content;
			# close DC;
		}
	}	
	else
	{
		$Content=$Post_Responce;
	}

	if($Application_Regex ne 'N/A')
	{
		if($Content=~m/$Application_Regex/is)
		{
			$Document_Url = $1;
			my $u1=URI::URL->new($Document_Url, $Home_URL);
			$Document_Url=$u1->abs;			
			print "Document_Url: $Document_Url\n";
			($Content,$Code,$Redir_Url) =&Download_Utility::Getcontent($Document_Url,"",$Host,$Content_Type,$Referer,$Proxy,$Need_Host,$Accept_Language);
			
			# open(DC,">Doc_Page_Content.html");
			# print DC $Content;
			# close DC;
		}
	}
	else
	{
		$Content=$Post_Responce;
	}
	
	
	return($Content,$res_code);
}