use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use filehandle;
use URI::Escape;
use Digest::MD5;
use JSON::Parse 'parse_json';
require "C:/Glenigan/Merit/Projects/docdownload_perl/lib/Scrape_Oneapp.pm";
# require "D:/One_App/DB_OneApp.pm";
require "C:/Glenigan/Merit/Projects/docdownload_perl/lib/DB_OneApp_Test.pm";
use Time::Piece;

# my $PDF_Path='//172.27.138.188/OneApp/';
# my $PDF_Path='//172.27.138.180/Council_PDFs/';
my $PDF_Path='//172.27.137.202/One_app_download/';

my $dbh = &DB_OneApp_Test::DbConnection();

my ($Format_id,$Council_Code,$DOCUMENT_NAME,$APPLICATION_NO,$SOURCE,$MARKUP) = &DB_OneApp_Test::Retrieve_OneApp_For_Scrape($dbh);

for(my $reccnt = 0; $reccnt < @{$Format_id}; $reccnt++ )
{
	# print "RECCNT :: $reccnt \n";
	my $Format_ID 		= @$Format_id[$reccnt];
	my $Council_Code 	= @$Council_Code[$reccnt];
	my $Document_Name 	= @$DOCUMENT_NAME[$reccnt];
	my $Application_No 	= @$APPLICATION_NO[$reccnt];
	my $Source 	= @$SOURCE[$reccnt];
	my $Markup 	= @$MARKUP[$reccnt];
	
	my $InsertFormatPdf = "insert into format_pdf (Source,Format_ID,Application_No,Council_Code,Markup,Site_House,Site_Suffix,Site_HouseName,Site_Street,Site_City,Site_County,Site_Country,Site_PostCode,Easting,Northing,Applicant_Title,Applicant_FirstName,Applicant_SurName,Applicant_CompanyName,Applicant_Street,Applicant_Town,Applicant_County,Applicant_Country,Applicant_Postcode,Applicant_Telephone,Applicant_Mobile,Applicant_Fax,Applicant_Email,Agent_Title,Agent_FirstName,Agent_SurName,Agent_CompanyName,Agent_Street,Agent_Town,Agent_County,Agent_Country,Agent_Postcode,Agent_Telephone,Agent_Mobile,Agent_Fax,Agent_Email,OneApp_Proposal,Existing_Wall_Material,Proposed_Wall_Material,Existing_Roof_Material,Proposed_Roof_Material,Existing_Window_Material,Proposed_Window_Material,Existing_Doors_Material,Proposed_Doors_Material,Existing_Boundary_treatment_Material,Proposed_Boundary_treatment_Material,Existing_Vehicle_access_Material,Proposed_Vehicle_access_Material,Existing_Lightning_Material,Proposed_Lightning_Material,Existing_Others_Materials,Proposed_Others_Materials,Material_Additional_Information,Material_Additional_Info_Details,Vehicle_Car,Vehicle_Light_Good,Vehicle_Motorcycle,Vehicle_Disablility,Vehicle_cycle,Vehicle_other,Vehicle_Other_Desc,Existing_use,Existing_Residential_House,Proposed_Houses,Proposed_Flats_Maisonettes,Proposed_Live_Work_units,Proposed_Cluster_flats,Proposed_Sheltered_housing,Proposed_Bedsit_Studios,Proposed_Unknown,Proposed_Market_Housing_Total,Existing_Houses,Existing_Flats_Maisonettes,Existing_Live_Work_units,Existing_Cluster_flats,Existing_Sheltered_housing,Existing_Bedsit_Studios,Existing_Unknown,Existing_Market_Housing_Total,Total_proposed_residential_units,Total_existing_residential_units,Non_residential_Shops,Non_residential_Financial,Non_residential_Restaurants,Non_residential_Drinking,Non_residential_food,Non_residential_Office,Non_residential_Research,Non_residential_Light,Non_residential_General,Non_residential_Storage,Non_residential_Hotels,Non_residential_Residential,Non_residential_institutions,Non_residential_Assembly,Non_residential_Other,Non_residential_Total,Existing_Employee,Proposed_Employee,Site_area) values ";
	print "CC: Format_ID => $Council_Code : $Format_ID \n";

	my $SaveFormatPdf_Query=&Scrape_Oneapp::Scrape_pdf($dbh,$Document_Name,$Format_ID,$Council_Code,$Application_No,$Source,$Markup,$PDF_Path);
	$InsertFormatPdf.=$SaveFormatPdf_Query;
	# print "$InsertFormatPdf\n";
	$InsertFormatPdf=~s/\,\s*$//igs;
	# if($Council_Code==434)
	# {
		# open(QR,">>FormatPdf_Query.txt");
		# print QR $SaveFormatPdf_Query;
		# close (QR);
	# }	

	if($InsertFormatPdf!~m/values\s*$/is)
	{
		print "$InsertFormatPdf\n";
		&DB_OneApp_Test::UpdateDB($dbh,$InsertFormatPdf);
	}
	
}	