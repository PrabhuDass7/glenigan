use strict;
use Cwd qw(abs_path);
use File::Basename;
use POE qw(Wheel::Run Filter::Reference);
sub MAX_CONCURRENT_TASKS () { 3 }

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
# print "$basePath\n";

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'\\lib');

# Private Module
require ($libDirectory.'\\Download_DB.pm'); 


#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();

#----------------------- Get input -----------------------#
my ($Council_Name,$Council_Code) = &Download_DB::Retrieve_Input_CC($dbh);
my @cCode = @{$Council_Code};

POE::Session->create(
  inline_states => {
    _start      => \&start_tasks,
    next_task   => \&start_tasks,
    task_result => \&handle_task_result,
    task_done   => \&handle_task_done,
    task_debug  => \&handle_task_debug,
    sig_child   => \&sig_child,
  }
);

sub start_tasks 
{
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	
	while (keys(%{$heap->{task}}) < MAX_CONCURRENT_TASKS) 
	{
		my $next_task = shift @cCode;
		last unless defined $next_task;
		
		print "Starting task for $next_task...\n";
		
		my $task = POE::Wheel::Run->new(
		Program      => sub { do_stuff($next_task) },
		StdoutFilter => POE::Filter::Reference->new(),
		StdoutEvent  => "task_result",
		StderrEvent  => "task_debug",
		CloseEvent   => "task_done",
		);
		
		$heap->{task}->{$task->ID} = $task;
		$kernel->sig_child($task->PID, "sig_child");
	}
}

sub do_stuff 
{
	binmode(STDOUT);    # Required for this to work on MSWin32
	my $councilCode   = shift;
	my $filter = POE::Filter::Reference->new();

	sleep(rand 5);
	
    my $script_name;	
	if($councilCode=~m/^5\d{2}$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/DocDownload_500s.pl";
		print "DocDownload_500s==>$script_name\n"; <STDIN>;
	}
	elsif($councilCode=~m/^(600)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/DocDownload_600s.pl";
		print "DocDownload_600s==>$script_name\n"; <STDIN>;
	}
	elsif($councilCode=~m/^(8|22|25|90|103|112|133|147|148|179|193|195|201|236|240|247|275|280|285|289|292|308|316|332|333|334|375|376|378|385|388|394|423|432|444|449|454|459|470)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/DocDownloadFormat8.pl";
		print "DocDownloadFormat8==>$script_name\n"; <STDIN>;
	}
	elsif($councilCode=~m/^(124|94|91|244|367|157|14|92|166|229|297|317|379|431|437|22|46|224|362|132|185|393|9|32|95|110|116|128|287|406|424|438|98|16|21|323|339|370|327|416)$/is)
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/Doc_Download_Format1_Extented.pl";
		print "Doc_Download_Format1_Extented==>$script_name\n"; <STDIN>;
	}
	else
	{
		$script_name="C:/Glenigan/Merit/Projects/docdownload_perl/root/Doc_Download.pl";
		print "Doc_Download==>$script_name\n"; <STDIN>;
	}

	system("start PERL $script_name $councilCode");
	
	
	my %result = (
	task   => $councilCode,
	status => "seems ok to me",
	);

	my $output = $filter->put([\%result]);

	print @$output;
}

sub handle_task_result 
{
	my $result = $_[ARG0];
	print "Result for $result->{task}: $result->{status}\n";
}

sub handle_task_debug 
{
	my $result = $_[ARG0];
	print "Debug: $result\n";
}

sub handle_task_done {
	my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
	delete $heap->{task}->{$task_id};
	$kernel->yield("next_task");
}

sub sig_child {
	my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
	my $details = delete $heap->{$pid};

# warn "$$: Child $pid exited";
}

$poe_kernel->run();
exit 0;