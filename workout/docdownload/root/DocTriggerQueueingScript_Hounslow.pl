use strict;
use Cwd qw(abs_path);
use File::Basename;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 

my $inputCouncilCode=$ARGV[0];

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'\\lib');
my $scriptDirectory = ($basePath.'\\root');

# Private Module
require ($libDirectory.'\\Download_DB.pm'); 

#---------------- Establish DB Connection ----------------#
my $dbh = &Download_DB::DbConnection();
#----------------------- Get input -----------------------#
my ($formatId, $onDemandID, @formatId, @onDemandID);
my ($Council_Code,$formatId) = &Download_DB::Retrieve_Input_CC_Individual($dbh,$inputCouncilCode);
my @cCode = @{$Council_Code};
my @formatId = @{$formatId};


for(my $reccnt = 0; $reccnt < @formatId; $reccnt++ )
{
	my $councilCode   	= $cCode[$reccnt];
	my $formatId   		= $formatId[$reccnt];

	my $script_name;
	# if($councilCode == '20')
	# {
		# $script_name="$scriptDirectory/Doc_Download_Hounslow.pl";
	# }
	if($councilCode == '338')
	{
		$script_name="$scriptDirectory/Doc_Download.pl";
	}	
		
	print "Hounslow=> $councilCode : $formatId\n";
	my $current_dateTime=localtime();
	if($formatId=~m/^\d+$/is)
	{
		system("start PERL $script_name $councilCode $formatId");
		open(my $fh,">>status_log_file_name_Hounslow.txt");
		print $fh "$councilCode\t$formatId\t$current_dateTime\n";
		close ($fh);
	}	
	print "30 Sec Waiting\n";
	sleep(30);
}
