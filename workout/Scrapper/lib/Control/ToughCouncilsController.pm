package ToughCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use URI::Escape;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use Time::HiRes qw(gettimeofday);
use IO::Socket::SSL;
use Net::Ping;


# BEGIN 
# {	
	# my $target = '172.27.137.199';
	# my $ping_obj = Net::Ping->new('tcp');

	# $ping_obj->port_number('3128');

	# if ($ping_obj->ping($target)) {
		# print "Yes, I can ping $target\n";
		
		# $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		# $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		# $ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		
	# } else {
		# print "No, I cannot ping $target\n";
	# }

	# $ping_obj->close();
# }


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
$basePath=~s/\\root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Tough');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####



my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Tough_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Tough_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Tough_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Tough_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');

###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType, $session_id);
	
			
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Tough_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Tough_Decision";
	}	

	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		# &CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
		exit;
	}
		
	my $rerunCount=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
	if($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working via Old proxy..\n";
		# $councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		# $councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	}
	
	my $searchURL = $commonFileDetails->{$currentCouncilCode}->{'URL'};
	
	print "Search Page URL==>$searchURL\n";
		
	($session_id) = &CouncilsScrapperCore::get_cookie_session_details($searchURL,$councilApp,'JSESSIONID');
	print "session_id=>$session_id\n";
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$councilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	
	$responseContent = $councilApp->get($searchURL);
	
	if($currentCouncilCode=~m/^(158|290|425|439|608|612|614)$/is)
	{
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'}); 
		$councilApp->field( $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(304)$/is)
	{
		my $acceptURL = "http://planning.bridgend.gov.uk/Disclaimer/Planning/Accept?returnUrl=%2F";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2F");
	}
	elsif($currentCouncilCode=~m/^(316)$/is)
	{
		my $acceptURL = "https://planningsearch.purbeck-dc.gov.uk/Disclaimer/Accept?returnUrl=%2F";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2F");
		
	}
	elsif($currentCouncilCode=~m/^(249)$/is)
	{		
		my $acceptURL = "https://planning.leicester.gov.uk/Disclaimer/Accept?returnUrl=%2F";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2F");		
	}
	elsif($currentCouncilCode=~m/^(609)$/is)
	{
		my $acceptURL = "https://plan.dorsetcc.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(607)$/is)
	{
		my $acceptURL = "https://planning.derbyshire.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(624)$/is)
	{
		my $acceptURL = "https://myeplanning.oxfordshire.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(625)$/is)
	{
		my $acceptURL = "https://planning.somerset.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(168)$/is)
	{
		my $acceptURL = "https://planningregister.crawley.gov.uk/Disclaimer/Accept?returnUrl=%2F";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2F");
	}
	elsif($currentCouncilCode=~m/^(606)$/is)
	{
		my $acceptURL = "https://planning.cumbria.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(615)$/is)
	{
		my $acceptURL = "https://www.planning.hertfordshire.gov.uk/Disclaimer/Accept?returnUrl=%2F";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2F");
	}
	elsif($currentCouncilCode=~m/^(619)$/is)
	{
		my $acceptURL = "http://eplanning.lincolnshire.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(627)$/is)
	{
		my $acceptURL = "http://suffolk.planning-register.co.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(247)$/is)
	{
		my $acceptURL = "https://planning.redcar-cleveland.gov.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FPlanning%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FPlanning%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(616)$/is)
	{		
		my $acceptURL = "https://www.kentplanningapplications.co.uk/Disclaimer/Accept?returnUrl=%2FSearch%2FAdvanced";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced");
	}
	elsif($currentCouncilCode=~m/^(618)$/is)
	{
		my $acceptURL = "http://leicestershire.planning-register.co.uk/Disclaimer?returnUrl=%2FSearch%2FAdvanced&accepted=True";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FAdvanced&accepted=True");
	}

	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}
	
    return($councilApp, $pingStatus, $responseContent);
}

####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $AGENT_PHONE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $AGENT_NAME, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL, $TARGET_DATE,$AGENT_COMPANY,$NORTHING,$EASTING,$GRIDREFERENCE);
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$REGISTRATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
	$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	$VALID_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
	$APPLICANT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
	$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
	$AGENT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
	$AGENT_PHONE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
	$AGENT_COMPANY = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_COMPANY'}/is);
	$TARGET_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$EASTING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'EASTING'}/is);
	$NORTHING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'NORTHING'}/is);
	$GRIDREFERENCE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'GRIDREFERENCE'}/is);
	$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
	
	
	print "PLANNING_APPLICATION:: $PLANNING_APPLICATION\n";<STDIN>;
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}	
		
	if($Easting eq "")
	{
		if($EASTING ne "")
		{
			$Easting = $EASTING;
		}
	}		
	
	if($Northing eq "")
	{
		if($NORTHING ne "")
		{
			$Northing = $NORTHING;
		}
	}		
	
	if($gridReference eq "")
	{
		if($GRIDREFERENCE ne "")
		{
			$gridReference = $GRIDREFERENCE;
			
			if($gridReference=~m/^\s*([^>]*?)\s*,\s*([^>]*?)\s*$/is)
			{
				my $newEasting=$1;
				my $newNorthing=$2;
				if($Easting eq "")
				{
					$Easting = $newEasting if($newEasting ne "");
				}
				if($Northing eq "")
				{
					$Northing = $newNorthing if($newNorthing ne "");
				}
			}
		}
	}	
	
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}		
	
	if($agentCompanyName eq "")
	{
		if($AGENT_COMPANY ne "")
		{
			$agentCompanyName = $AGENT_COMPANY;
		}
	}		
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}	
		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
	}		
	
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}
	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
	}
	
	if($agentName eq "")
	{
		if($AGENT_NAME ne "")
		{
			$agentName = $AGENT_NAME;
		}
	}				
			
	if($agentTelephone1 eq "")
	{
		if($AGENT_PHONE ne "")
		{
			$agentTelephone1 = $AGENT_PHONE;
		}
	}				
			
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}				
					
		
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}					
		
	if($applicationStatus eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$applicationStatus = $APPLICATION_STATUS;
		}
	}	
	
	if($applicantName eq "")
	{
		if($APPLICANT_NAME ne "")
		{
			$applicantName = $APPLICANT_NAME;
		}
	}	
	
	if($targetDecdt eq "")
	{
		if($TARGET_DATE ne "")
		{
			$targetDecdt = $TARGET_DATE;
		}
	}	
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			$documentURL = $applicationLink;
		}
	
		$documentURL =~s/\'/\'\'/igs;
		$documentURL =~s/\&amp\;/\&/igs;
	}
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/gsi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/gsi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	print "Planning insert_query:: $insert_query\n";
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
		
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_ISSUED_DATE);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
	$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
	$DECISION_ISSUED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}		
			
	if($Decision_Issued_Date eq "")
	{
		if($DECISION_ISSUED_DATE ne "")
		{
			$Decision_Issued_Date=$DECISION_ISSUED_DATE;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
			
			$Date_Decision_Made=~s/^Not\s*Known$//is;
		}
		
		if(($councilCode=~m/^477$/is) && ($Date_Decision_Made=~m/^01\/01\/1970$/is))
		{
			$Date_Decision_Made=~s/^01\/01\/1970$//is;
		}
	}		
		
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}		
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
	}		
	
	if($Decision_Status eq "")
	{
		if($DECISION_STATUS ne "")
		{
			$Decision_Status = $DECISION_STATUS;
		}
	}	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/gsi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/gsi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/gsi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/gsi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	
	print "Application:: $Application\n";
	print "Proposal:: $Proposal\n";
	print "Decision_Status:: $Decision_Status\n";
	print "Date_Decision_Made:: $Date_Decision_Made\n";
	print "Decision_Issued_Date:: $Decision_Issued_Date\n";
	print "councilCode:: $councilCode\n";
	print "applicationLink:: $applicationLink\n";
	print "sourceWithTime:: $sourceWithTime\n";
	print "Application_Status:: $Application_Status\n";
	print "scheduleDate:: $scheduleDate\n";
	
	
	
	undef $PLANNING_APPLICATION;  undef $PROPOSAL;  undef $DECISION_STATUS;  undef $APPLICATION_STATUS;  undef $DECISION_DATE;  undef $DECISION_ISSUED_DATE;  undef $applicationLink;
	undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Application_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
	
	print "insert_query:: $insert_query";<STDIN>;
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
   
	$URL=~s/amp\;//igs;
	
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy

	# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy

	
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
    return($con,$code);
}


####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;
	
    my ($councilAppResponse, $commonFileDetails, $scriptStatus,$postContent, $searchURL);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	$searchURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};	
	$postContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
	my $formNumber = $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'};
	my $fromDate = $commonFileDetails->{$receivedCouncilCode}->{'FORM_START_DATE'};
	my $toDate = $commonFileDetails->{$receivedCouncilCode}->{'FORM_END_DATE'};
	my $searchButton = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'};
	my $searchButtonVal = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_VAL'};
	my $RADIO_BTN_NME = $commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_NME'};
	my $RADIO_BTN_VAL = $commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_VAL'};
	
	$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'}); 
	
	if($receivedCouncilCode=~m/^(120|413|480|612)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click( $searchButton );
	}
	elsif($receivedCouncilCode=~m/^(492|618|619|428)$/is)
	{
		if($receivedCouncilCode=~m/^(619)$/is)
		{		
			my $url;
			if($category eq "Planning")
			{
				$receivedCouncilApp->form_number($formNumber); 
				$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
				$receivedCouncilApp->click();
			}
			elsif($category eq "Decision")
			{
				$url = 'http://lincolnshire.planning-register.co.uk/Search/Standard?searchType=Decided&days=90';	
				$receivedCouncilApp->get($url);
			}
		}
		else
		{
			$receivedCouncilApp->form_number($formNumber); 
			$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
			$receivedCouncilApp->click();
		}
	}
	elsif($receivedCouncilCode=~m/^(630)$/is)
	{
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."\/".$2."\/".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."\/".$2."\/".$1;
		}		
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields($fromDate => $Temp_From_Date, $toDate => $Temp_To_Date );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(447)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields($RADIO_BTN_NME => $RADIO_BTN_VAL );
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(304)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber);
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(316)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};		
		
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(249|261|451)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};		
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*(?:<[^>]*?>)?\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
	}
	elsif($receivedCouncilCode=~m/^(609)$/is)
	{		
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."\/".$2."\/".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."\/".$2."\/".$1;
		}		
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(607)$/is)
	{		
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."\/".$2."\/".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."\/".$2."\/".$1;
		}		
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(289)$/is)
	{		
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(151)$/is)
	{		
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(200)$/is)
	{		
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(625)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber);
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(627)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
		# $receivedCouncilApp->form_number($formNumber);
		# $receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		# $receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^315$/is)
	{
		$receivedCouncilApp-> submit_form(
				form_number => $formNumber,
				fields      => {
					$fromDate	=> $receivedFromDate,
					$toDate	=> $receivedToDate,
					$searchButton	=> $searchButtonVal,
					},
					button          => $searchButton
				);
	}
	elsif($receivedCouncilCode=~m/^(467)$/is)
	{				
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'}); 
		my $getPageContent = $receivedCouncilApp->content;
		
		my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$requestDigest);
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATE'}/is)
		{
			$VIEWSTATE = uri_escape($1);
		}
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATEGENERATOR'}/is)
		{
			$VIEWSTATEGENERATOR = uri_escape($1);
		}
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'EVENTVALIDATION'}/is)
		{
			$EVENTVALIDATION = uri_escape($1);
		}
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATEENCRYPTED'}/is)
		{
			$requestDigest = uri_escape($1);
		}
				
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);

		if($receivedFromDate=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
			$sDay=~s/^0//si;
			$sMonth=~s/^0//si;
		}
		if($receivedToDate=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
			
			$eDay=~s/^0//si;
			$eMonth=~s/^0//si;
		}
		
		$postContent=~s/<VIEWSTATE>/$VIEWSTATE/si;
		$postContent=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/si;
		$postContent=~s/<EVENTVALIDATION>/$EVENTVALIDATION/si;
		$postContent=~s/<REQUESTDIGEST>/$requestDigest/is;
		
		$postContent=~s/<FromDay>/$sDay/igs;
		$postContent=~s/<ToDay>/$eDay/igs;
		$postContent=~s/<FromMonth>/$sMonth/igs;
		$postContent=~s/<ToMonth>/$eMonth/igs;
		$postContent=~s/<FromYear>/$sYear/igs;
		$postContent=~s/<ToYear>/$eYear/igs;
		
		$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' );
		$receivedCouncilApp->add_header( "Host" => 'www.gov.je' );
		$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
		$receivedCouncilApp->add_header( "Origin" => 'https://www.gov.je' );
		$receivedCouncilApp->add_header( "Referer" => "$searchURL" );	
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");				
				
	}
	elsif($receivedCouncilCode=~m/^(628)$/is)
	{
		# $receivedCouncilApp-> submit_form(
				# form_number => $formNumber,
				# fields      => {
					# $fromDate	=> $receivedFromDate,
					# $toDate	=> $receivedToDate,
					# $searchButton	=> $searchButtonVal,
					# },
					# button          => $searchButton
				# );
				
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'}); 
		my $getPageContent = $receivedCouncilApp->content;
		
		my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION);
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATE'}/is)
		{
			$VIEWSTATE = uri_escape($1);
		}
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATEGENERATOR'}/is)
		{
			$VIEWSTATEGENERATOR = uri_escape($1);
		}
		if($getPageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'EVENTVALIDATION'}/is)
		{
			$EVENTVALIDATION = uri_escape($1);
		}
				
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<VIEWSTATE>/$VIEWSTATE/si;
		$postContent=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/si;
		$postContent=~s/<EVENTVALIDATION>/$EVENTVALIDATION/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");				
				
	}
	elsif($receivedCouncilCode=~m/^(384)$/is)
	{		
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		$startDateDisplay=~s/\//\%2F/gsi;
		$endDateDisplay=~s/\//\%2F/gsi;
		
		$searchURL=~s/<FromDate>/$startDateDisplay/is;
		$searchURL=~s/<ToDate>/$endDateDisplay/is;
		
		$receivedCouncilApp->get($searchURL);
	
	}
	elsif($receivedCouncilCode =~ m/^(620)$/is)	
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		my $val_from = $Temp_From_Date."-00-00-00";
		my $val_to = $Temp_To_Date."-00-00-00";
		
		if($category eq "Planning")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtCompleteDateFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtCompleteDateTo$dateInput' => $val_to );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtDecisionDateFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtDecisionDateTo$dateInput' => $val_to );
		}
		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(425)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		my $val_from = $Temp_From_Date."-00-00-00";
		my $val_to = $Temp_To_Date."-00-00-00";
		
		if($category eq "Planning")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtDateReceivedFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtDateReceivedTo$dateInput' => $val_to );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtDateDeterminedFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtDateDeterminedTo$dateInput' => $val_to );
		}

		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(168)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
	}
	elsif($receivedCouncilCode=~m/^(606)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
	}
	elsif($receivedCouncilCode=~m/^(247)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(616)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(158|244|290|439|608)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
				
		if($category eq "Planning")
		{
			$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDateReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDateReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{
			if($receivedCouncilCode=~m/^(428|608)$/is)
			{
				$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDateDeterminedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDateDeterminedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
			}
			else
			{
				$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDateIssuedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDateIssuedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
			}
		}
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(621)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		my $val_from = $Temp_From_Date."-00-00-00";
		my $val_to = $Temp_To_Date."-00-00-00";
		
		if($category eq "Planning")
		{			
			$receivedCouncilApp->set_fields( 'ctl00$MainContent$txtValidatedDateFrom$dateInput' => $val_from, 'ctl00$MainContent$txtValidatedDateTo$dateInput' => $val_to );
			
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtValidatedDateFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}', 'ctl00_MainContent_txtValidatedDateTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}' );			
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00$MainContent$txtDecisionDateFrom$dateInput' => $val_from, 'ctl00$MainContent$txtDecisionDateTo$dateInput' => $val_to );
			
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtDecisionDateFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}', 'ctl00_MainContent_txtDecisionDateTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}' );		
		}

		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(617)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		if($category eq "Planning")
		{		
			$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtAppValFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtAppValTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDecFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDecTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(369)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		if($category eq "Planning")
		{		
			$receivedCouncilApp->set_fields( 'ctl00_ctl00_MainContent_MainContent_txtDateReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ctl00_MainContent_MainContent_txtDateReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
			
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00_ctl00_MainContent_MainContent_txtDateIssuedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ctl00_MainContent_MainContent_txtDateIssuedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(623)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		if($category eq "Planning")
		{		
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtDateReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_MainContent_txtDateReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtDateDeterminedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_MainContent_txtDateDeterminedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click($searchButton);
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
		$receivedCouncilApp->click();
		
	}
	elsif($receivedCouncilCode=~m/^(394)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		$startDateDisplay=~s/\//\-/gsi;
		$endDateDisplay=~s/\//\-/gsi;
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		$postContent=~s/<FromDate>/$Temp_From_Date/si;
		$postContent=~s/<FromDateS>/$receivedFromDate/si;
		$postContent=~s/<ToDateS>/$receivedToDate/si;
		$postContent=~s/<ToDate>/$Temp_To_Date/si;
			
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(302)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my %monthFull = ('01' =>'January', '02' => 'February', '03' => 'March','04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $monthFull{$2};
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $monthFull{$2};
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$eDay=~s/^0//is;
				
		$postContent=~s/<SDATE>/$sDay/si;
		$postContent=~s/<EDATE>/$eDay/si;
		$postContent=~s/<SMONTH>/$sMonth/si;
		$postContent=~s/<EMONTH>/$eMonth/si;
		$postContent=~s/<SYEAR>/$sYear/si;
		$postContent=~s/<EYEAR>/$eYear/si;
			
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(477)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my %monthFull = ('01' =>'January', '02' => 'February', '03' => 'March','04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $monthFull{$2};
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $monthFull{$2};
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$eDay=~s/^0//is;
				
		$searchURL=~s/<SDATE>/$sDay/si;
		$searchURL=~s/<EDATE>/$eDay/si;
		$searchURL=~s/<SMONTH>/$sMonth/si;
		$searchURL=~s/<EMONTH>/$eMonth/si;
		$searchURL=~s/<SYEAR>/$sYear/si;
		$searchURL=~s/<EYEAR>/$eYear/si;
			
		$receivedCouncilApp->get( $searchURL );
		
	}
	elsif($receivedCouncilCode=~m/^(615)$/is)
	{	
		$receivedCouncilApp->get( $searchURL );
		
		my $gtCnt = $receivedCouncilApp->content;
		
		if($category eq "Planning")
		{		
			my $planURL = $1 if($gtCnt=~m/<li[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*View\s*planning\s*applications\s*open[^>]*?<\/a>\s*<\/li>/is);
			$planURL=~s/amp\;//gsi;
			$planURL = URI::URL->new($planURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
			$receivedCouncilApp->get( $planURL );
		}
		elsif($category eq "Decision")
		{		
			my $decURL = $1 if($gtCnt=~m/<li[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*View\s*planning\s*applications\s*that[^>]*?<\/a>\s*<\/li>/is);
			$decURL=~s/amp\;//gsi;
			$decURL = URI::URL->new($decURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
			$receivedCouncilApp->get( $decURL );
		}
		
	}
	elsif($receivedCouncilCode=~m/^(614)$/is)
	{	
		$receivedCouncilApp->get( $searchURL );
	}
	elsif($receivedCouncilCode=~m/^(325)$/is)
	{	
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$eDay=~s/^0//is;
		$sMonth=~s/^0//is;
		$eMonth=~s/^0//is;
				
		$searchURL=~s/<SDATE>/$sDay/si;
		$searchURL=~s/<EDATE>/$eDay/si;
		$searchURL=~s/<SMONTH>/$sMonth/si;
		$searchURL=~s/<EMONTH>/$eMonth/si;
		$searchURL=~s/<SYEAR>/$sYear/si;
		$searchURL=~s/<EYEAR>/$eYear/si;
		
		$receivedCouncilApp->get( $searchURL );
	}
	elsif($receivedCouncilCode=~m/^(353)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
		}
		
		$startDateDisplay="$sYear-$sMonth-$sDay";
		$endDateDisplay="$eYear-$eMonth-$eDay";
			
		my $responseCont = $receivedCouncilApp->content;
		
		my $Mode=$1 if($responseCont=~m/mode\%22\%3A\%22([^>]*?)\%22/is);
		my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
		my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
		
		$Mode=~s/\\\"//igs;
		$Mode=~s/\///igs;
		$Mode=~s/\\//igs;
		
		$fwuid=~s/\\\"//igs;
		$fwuid=~s/\///igs;
		$fwuid=~s/\\//igs;
		
		$loaded=~s/\\\"//igs;
		$loaded=~s/\///igs;
		$loaded=~s/\\//igs;		
		
		$postContent=~s/<MODE>/$Mode/si;
		$postContent=~s/<FWUID>/$fwuid/si;
		$postContent=~s/<LOADED>/$loaded/si;
		
		$postContent=~s/<FROM_DATE>/$startDateDisplay/si;
		$postContent=~s/<To_DATE>/$endDateDisplay/si;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(456)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
		}
		
		$startDateDisplay="$sYear-$sMonth-$sDay";
		$endDateDisplay="$eYear-$eMonth-$eDay";
			
		my $responseCont = $receivedCouncilApp->content;
		
		my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
		my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
				
		$fwuid=~s/\\\"//igs;
		$fwuid=~s/\///igs;
		$fwuid=~s/\\//igs;
		
		$loaded=~s/\\\"//igs;
		$loaded=~s/\///igs;
		$loaded=~s/\\//igs;		
		
		$postContent=~s/<FWUID>/$fwuid/si;
		$postContent=~s/<LOADED>/$loaded/si;
				
		$postContent=~s/<FROM_DATE>/$startDateDisplay/si;
		$postContent=~s/<To_DATE>/$endDateDisplay/si;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(143|92|489)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
		}
		
		$startDateDisplay="$sYear-$sMonth-$sDay";
		$endDateDisplay="$eYear-$eMonth-$eDay";
			
		my $responseCont = $receivedCouncilApp->content;
		
		my $Mode=$1 if($responseCont=~m/mode\%22\%3A\%22([^>]*?)\%22/is);
		my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
		my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
		
		$Mode=~s/\\\"//igs;
		$Mode=~s/\///igs;
		$Mode=~s/\\//igs;
		
		$fwuid=~s/\\\"//igs;
		$fwuid=~s/\///igs;
		$fwuid=~s/\\//igs;
		
		$loaded=~s/\\\"//igs;
		$loaded=~s/\///igs;
		$loaded=~s/\\//igs;		
		
		$postContent=~s/<MODE>/$Mode/si;
		$postContent=~s/<FWUID>/$fwuid/si;
		$postContent=~s/<LOADED>/$loaded/si;
		
		$postContent=~s/<FROM_DATE>/$startDateDisplay/si;
		$postContent=~s/<To_DATE>/$endDateDisplay/si;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(624)$/is)
	{
		my $url = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		$receivedCouncilApp->get($url);
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");		
	}
	
	$councilAppResponse = $receivedCouncilApp->content;
	
	my $currentPageURL = $receivedCouncilApp->uri();
	
	$scriptStatus = $receivedCouncilApp->status;		
	print "councilAppStatus==>$scriptStatus\n";
			
    return ($councilAppResponse,$scriptStatus,$currentPageURL);
}

####
# Method to POST the pagenation settings for Online Councils
####

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $pageContent, $receivedFromDate, $receivedToDate) = @_;
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	$commonContent .= $pageContent;
	
	my $councilType = $commonFileDetails->{$receivedCouncilCode}->{'FORMAT_TYPE'};
	
	if($councilType=~m/^PAGENUMBER$/is)
	{
		my $pageCheck = 2;
		
		
		if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'TOTAL_PAGE_COUNT'}/is)
		{
			my $totalNumPage = $1;
			
			NextPage:
			if($pageCheck<=$totalNumPage)
			{
				print "CurrentPage::$pageCheck==>Total::$totalNumPage\n";
				$receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'}); 
				
				if($receivedCouncilCode=~m/^(120|158|168|247|290|369|413|425|428|439|480|608|612|614|621|623)$/is)
				{
					my $nextPage = $1 if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}/is);
					eval
					{
						$receivedCouncilApp->click( $nextPage );
					};					
				}
				
				my $nextPageContent = $receivedCouncilApp->content;	
				$pageContent = $nextPageContent;
		
				if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}/is)
				{
					$commonContent .= $pageContent;
					$pageCheck++;
					goto NextPage;
				}
				else
				{
					$commonContent .= $pageContent;
				}
			}
		}		
	}
	elsif($councilType=~m/^POSTFORMAT$/is)
	{		
		my $pageCheck = 2;		
		my $MaxPage = 2;		
				
		NextPostPage:
		my $nextPagecheck = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'};		
		$nextPagecheck=~s/<PAGENUM>/$pageCheck/gsi;
		
		if($pageContent=~m/$nextPagecheck/is)
		{		
			my $eventTargetRegex = $commonFileDetails->{$receivedCouncilCode}->{'EVENTTARGET'};
			my $eventArgumentRegex = $commonFileDetails->{$receivedCouncilCode}->{'EVENTARGUMENT'};
			$eventTargetRegex=~s/<PAGENUM>/$pageCheck/gsi;
			my $eventTarget = uri_escape($1) if($pageContent=~m/$eventTargetRegex/is);
			$eventArgumentRegex=~s/<PAGENUM>/$pageCheck/gsi;
			my $eventArgument = uri_escape($1) if($pageContent=~m/$eventArgumentRegex/is);
			my $viewState = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATE'}/is);
			my $viewStateGen = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATEGENERATOR'}/is);
			my $eventValidation = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'EVENTVALIDATION'}/is);
			my $activityID = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'ACTIVITYID'}/is);
			my $scrollPos = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SCROLLPOSITION'}/is);
			my $scrollX = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SCROLLPOSITIONX'}/is);
			my $scrollY = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SCROLLPOSITIONY'}/is);
			
			my $nextPagePostCnt = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_POST_CONTENT'};
			
			$nextPagePostCnt=~s/<VIEWSTATE>/$viewState/is;
			$nextPagePostCnt=~s/<EVENTTARGET>/$eventTarget/is;
			$nextPagePostCnt=~s/<EVENTARGUMENT>/$eventArgument/is;
			$nextPagePostCnt=~s/<VIEWSTATEGENERATOR>/$viewStateGen/is;
			$nextPagePostCnt=~s/<EVENTVALIDATION>/$eventValidation/is;
			$nextPagePostCnt=~s/<ACTIVITYID>/$activityID/is;
			$nextPagePostCnt=~s/<SCROLL>/$scrollPos/is;
			$nextPagePostCnt=~s/<SCROLLX>/$scrollX/is;
			$nextPagePostCnt=~s/<SCROLLY>/$scrollY/is;
			if($receivedCouncilCode=~m/^467$/is)
			{			
				my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
				if($receivedFromDate=~m/^(\d+)\-(\d+)\-(\d+)/is)
				{
					$sDay = $3;
					$sMonth = $2;
					$sYear = $1;
					$sDay=~s/^0//si;
					$sMonth=~s/^0//si;
				}
				if($receivedToDate=~m/^(\d+)\-(\d+)\-(\d+)/is)
				{
					$eDay = $3;
					$eMonth = $2;
					$eYear = $1;
					
					$eDay=~s/^0//si;
					$eMonth=~s/^0//si;
				}
				
				$nextPagePostCnt=~s/<Page_number>/$pageCheck/igs;
				$nextPagePostCnt=~s/<Total_Page>/$MaxPage/igs;
				$nextPagePostCnt=~s/<FromDay>/$sDay/igs;
				$nextPagePostCnt=~s/<ToDay>/$eDay/igs;
				$nextPagePostCnt=~s/<FromMonth>/$sMonth/igs;
				$nextPagePostCnt=~s/<ToMonth>/$eMonth/igs;
				$nextPagePostCnt=~s/<FromYear>/$sYear/igs;
				$nextPagePostCnt=~s/<ToYear>/$eYear/igs;
			}
			my $SEARCH_URL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
	
			if($receivedCouncilCode=~m/^(384)$/is)
			{
				$SEARCH_URL=~s/<FromDate>/$receivedFromDate/is;
				$SEARCH_URL=~s/<ToDate>/$receivedToDate/is;
				if($category eq "Planning")
				{
					$SEARCH_URL=~s/(\&datereceivedfrom=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
					$SEARCH_URL=~s/(\&datereceivedto=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
				}
				elsif($category eq "Decision")
				{
					$SEARCH_URL=~s/(\&dateissuedfrom=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
					$SEARCH_URL=~s/(\&dateissuedto=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
				}
			}
			
			if($receivedCouncilCode=~m/^(151)$/is)
			{
				my $nextPageURL = $1 if($pageContent=~m/$nextPagecheck/is);
				if($nextPageURL!~m/^https?/is)
				{
					$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				}
				
				my $timestamp = int (gettimeofday * 1000);
				print STDOUT "timestamp = $timestamp\n";
				my $date_in_Millisec = $timestamp;
				$nextPageURL=~s/\&amp\;/\&/gsi;
				
				my $nextPageDateURL=$nextPageURL."&_=$date_in_Millisec";
				
				$receivedCouncilApp->add_header( 'Accept' => '*/*');	
				$receivedCouncilApp->add_header( 'accept-encoding' => 'gzip, deflate, br');	
				$receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.9');	
				$receivedCouncilApp->add_header( 'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');		
				$receivedCouncilApp->add_header( 'x-requested-with' => 'XMLHttpRequest');	
				$receivedCouncilApp->add_header( 'Referer' => $SEARCH_URL);	
				
				print "nextPageURL==>$nextPageDateURL\n";
				
				$receivedCouncilApp->get( $nextPageDateURL );
			}
			elsif($receivedCouncilCode=~m/^(304|316|609|289|447|625|627|168)$/is)
			{
				my $nextPageURL = $1 if($pageContent=~m/$nextPagecheck/is);
				if($nextPageURL!~m/^https?/is)
				{
					$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				}
				
				$nextPageURL=~s/\&amp\;/\&/gsi;
				$receivedCouncilApp->get( $nextPageURL );
			}
			elsif($receivedCouncilCode=~m/^467$/is)
			{
				$receivedCouncilApp->add_header( "Accept" => 'text/plain, */*; q=0.01' );
				$receivedCouncilApp->add_header( "Host" => 'www.gov.je' );
				$receivedCouncilApp->add_header( "Content-Type" => 'application/json; charset=UTF-8' );
				$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.9' );
				$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
				$receivedCouncilApp->add_header( "Sec-Fetch-Mode" => 'cors' );
				$receivedCouncilApp->add_header( "Sec-Fetch-Site" => 'same-origin' );
				$receivedCouncilApp->add_header( "Origin" => 'https://www.gov.je' );
				$receivedCouncilApp->add_header( "Referer" => 'https://www.gov.je/citizen/Planning/Pages/PlanningApplicationSearch.aspx' );	
				
				$receivedCouncilApp->post('https://www.gov.je/_layouts/15/PlanningAjaxServices/PlanningSearch.svc/Search', Content => "$nextPagePostCnt");
			}
			else
			{
				$receivedCouncilApp->post( $SEARCH_URL, Content => "$nextPagePostCnt");
			}
			
			my $nextPageContent = $receivedCouncilApp->content;
			$pageContent = $nextPageContent;
			
			my $nextPageCheckRegex = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_CHECK_REGEX'};
			$nextPageCheckRegex=~s/<PAGENUM>/$pageCheck/is;
			
			if(($receivedCouncilCode=~m/^467$/is) && ($nextPageContent=~m/$nextPageCheckRegex/is))
			{
				print "Current page number is $pageCheck\n";
				$commonContent .= $pageContent;
			}
			elsif($pageContent=~m/$nextPageCheckRegex/is)
			{
				print "Current page number is $pageCheck\n";
				$commonContent .= $pageContent;
			}
			else
			{
				$commonContent .= $pageContent;				
				print "Next page number is $pageCheck\n";
				
				$pageCheck++;
				$MaxPage++;
				goto NextPostPage;
			}			
		}
		else
		{
			$commonContent;
		}
	}
	elsif($councilType=~m/^NEXTPAGEURL$/is)
	{	
		my $pageCheck = 2;		
				
		NextPostURLPage:	

		my $nextPageRegex=$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'};
		$nextPageRegex=~s/<PAGENUM>/$pageCheck/is;
		
		print "nextPageRegex:: $nextPageRegex\n";<STDIN>;
		
		if($pageContent=~m/$nextPageRegex/is)
		{		
			my $nextPageURL = $1;
			
			if($nextPageURL!~m/^https?/is)
			{
				$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
			}		
				
			$nextPageURL=~s/\&amp\;/\&/gsi;	
			if($receivedCouncilCode=~m/^(200)$/is)
			{
				$nextPageURL=~s/Planning\/MoveToPage/Planning\/Results\/MoveToPage/gsi;	
			}
			if($receivedCouncilCode=~m/^(249|261|451)$/is)
			{
				my $timestamp = int (gettimeofday * 1000);
				print STDOUT "timestamp = $timestamp\n";
				my $date_in_Millisec = $timestamp;
				# print "$date_in_Millisec\n";
				my $nextPageDateURL=$nextPageURL."&_=$date_in_Millisec";
				print "nextPageURL==>$nextPageDateURL\n";
				my $Host= $1 if($nextPageDateURL=~m/\/\/([^>]*?)\/Search\//is);
				$receivedCouncilApp->add_header( 'Accept' => '*/*');	
				$receivedCouncilApp->add_header( 'accept-encoding' => 'gzip, deflate, br');	
				$receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.9');	
				$receivedCouncilApp->add_header( 'Host' => $Host);	
				$receivedCouncilApp->add_header( 'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');	
				$receivedCouncilApp->add_header( 'x-requested-with' => 'XMLHttpRequest');	
				$receivedCouncilApp->add_header( 'Referer' => $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'});	
				
				$receivedCouncilApp->get( $nextPageDateURL );
			}
			else
			{			
				$receivedCouncilApp->get( $nextPageURL );	
			}		
			
			my $nextPageContent = $receivedCouncilApp->content;	
			$pageContent = $nextPageContent;
			
			
			if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_CHECK_REGEX'}/is)
			{
				print "Current page number is $pageCheck\n";
				$commonContent .= $pageContent;
			}
			else
			{
				$commonContent .= $pageContent;
				
				print "Next page number is $pageCheck\n";
				if(($receivedCouncilCode=~m/^(?:477|617)$/is)&&($pageCheck==51))
				{
					goto returnpage;
				}
				$pageCheck++;
				goto NextPostURLPage;
			}
		}		
	}
	returnpage:
	
	open HH,">PaginationcommonContent.html";
	print HH "$commonContent";
	close HH;
	
	print "*** PaginationcommonContent Completed ***\n";
	
	return($commonContent);
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	if($councilCode=~m/^249$/is)
	{
		print("receivedApplicationUrl==>$receivedApplicationUrl\n");
		my $halfURL;
		if($receivedApplicationUrl=~m/(\/Planning\/Display\/[^>]*?)$/is)
		{
			$halfURL = uri_escape($1);
		}
		
		my $pURL = "https://planning.leicester.gov.uk/Disclaimer/Accept?returnUrl=$halfURL";
		my $postCnt = "returnUrl=$halfURL";
		# print("pURL==>$pURL\n");
		$applicationDetailsPageContent = $receivedCouncilApp->post($pURL, Content => $postCnt);
		
	}
	elsif($councilCode=~m/^353$/is)
	{
		my ($application_id,$application_name);
		if($receivedApplicationUrl=~m/application\/([^>]*?)\/([^>]*?)$/is)
		{
			$application_id=$1;
			$application_name=$2;
		}
		
		my $receivedCouncilApptemp=$receivedCouncilApp;
		
		my $pURL = "https://folkestonehythedc.force.com/pr/s/sfsites/aura?r=3&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1";
		my ($commonFileDetails,$postCnt);
		if($category eq "Planning")
		{
			$commonFileDetails=$councilDetailsFile;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
		
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22123%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22'.$loaded.'%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22zFb-UkcV_261W4HH5KIVrg%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%224Rab0rbd6r-1SSTXUGa2lw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
		}
		elsif($category eq "Decision")
		{
			$commonFileDetails=$councilDetailsDecision;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
		
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22818%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AinputDate%22%3A%22z-qp3GvEQ1EfL_McUqcz6Q%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputURL%22%3A%22OLB65JHY2G3Wcv7H_yUspQ%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputDate%22%3A%22MYbs4tpQL_tXyVnDCgNKCQ%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22vIrcwOEdKZm7pHxSmXJJSA%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22CscY-pMeaNQlltytn0ADZQ%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id&aura.token=undefined";
			
		}
				
		$applicationDetailsPageContent = $receivedCouncilApp->post($pURL, Content => $postCnt);
	
	}
	elsif($councilCode=~m/^456$/is)
	{		
		my ($application_id,$application_name);
		if($receivedApplicationUrl=~m/application\/([^>]*?)\?[^>]*?$/is)
		{
			$application_id=$1;
			# $application_name=$2;
		}
		
		my $receivedCouncilApptemp=$receivedCouncilApp;
		
		my $pURL = "https://ioacc.force.com/s/sfsites/aura?r=3&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1";
		my ($commonFileDetails,$postCnt);
		if($category eq "Planning")
		{
			$commonFileDetails=$councilDetailsFile;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
		
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
						
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22120%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%220ysoRmJ9098Q_cVgsWsJKw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22ct%22%3A1%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fs%2Fpapplication%2F$application_id%2Ffpl2020131%3Flanguage%3Den_GB&aura.token=undefined";
			
		}
		elsif($category eq "Decision")
		{
			$commonFileDetails=$councilDetailsDecision;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
		
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
						
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22131%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%220ysoRmJ9098Q_cVgsWsJKw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22ct%22%3A1%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fs%2Fpapplication%2F$application_id%2Fdis202042%3Flanguage%3Den_GB&aura.token=undefined";
			
		}
		
		$applicationDetailsPageContent = $receivedCouncilApp->post($pURL, Content => $postCnt);
	
	}
	elsif($councilCode=~m/^143$/is)
	{	
		my ($application_id,$application_name);
		if($receivedApplicationUrl=~m/application\/([^>]*?)\/([^>]*?)$/is)
		{
			$application_id=$1;
			$application_name=$2;
		}
		
		my $receivedCouncilApptemp=$receivedCouncilApp;
		
		my $pURL = "https://carmarthenshire-pr.force.com/en/s/sfsites/aura?r=4&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1";
		my ($commonFileDetails,$postCnt);
		if($category eq "Planning")
		{
			$commonFileDetails=$councilDetailsFile;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
			
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22160%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22PPe5c5wz_nCfBqa3gvxl_g%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22pfR3KLQw7RzZsXzq-Jljkw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22ct%22%3A1%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fen%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
		}
		elsif($category eq "Decision")
		{
			$commonFileDetails=$councilDetailsDecision;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
			
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22160%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22PPe5c5wz_nCfBqa3gvxl_g%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22pfR3KLQw7RzZsXzq-Jljkw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22ct%22%3A1%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fen%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
		}
		$applicationDetailsPageContent = $receivedCouncilApp->post($pURL, Content => $postCnt);
	}
	elsif($councilCode=~m/^(92)$/is)
	{	
		my ($application_id,$application_name);
		if($receivedApplicationUrl=~m/application\/([^>]*?)\/([^>]*?)$/is)
		{
			$application_id=$1;
			$application_name=$2;
		}
		
		my $receivedCouncilApptemp=$receivedCouncilApp;
		
		my $pURL = "https://allerdalebc.force.com/pr/s/sfsites/aura?r=3&arcshared.RecordServiceCont.getRecord=1&ui-comm-runtime-components-aura-components-siteforce-qb.Quarterback.validateRoute=1&ui-communities-components-aura-components-forceCommunity-controller.RecordValidation.getOnLoadErrorMessage=1&ui-communities-components-aura-components-forceCommunity-richText.RichText.getParsedRichTextValue=2&ui-communities-components-aura-components-forceCommunity-seoAssistant.SeoAssistant.getRecordAndLanguageData=1&ui-communities-components-aura-components-forceCommunity-tabset.Tabset.getLocalizedRegionLabels=1&ui-force-components-controllers-detail.Detail.getEntityConfig=1&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1&ui-force-components-controllers-recordLayoutBroker.RecordLayoutBroker.getRecordAndLayout=1";
		my ($commonFileDetails,$postCnt);
		if($category eq "Planning")
		{
			$commonFileDetails=$councilDetailsFile;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
			
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22160%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22PPe5c5wz_nCfBqa3gvxl_g%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22pfR3KLQw7RzZsXzq-Jljkw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22ct%22%3A1%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fen%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
		}
		elsif($category eq "Decision")
		{
			$commonFileDetails=$councilDetailsDecision;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
			
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22160%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22PPe5c5wz_nCfBqa3gvxl_g%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22pfR3KLQw7RzZsXzq-Jljkw%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22ct%22%3A1%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fen%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
		}
			
		$applicationDetailsPageContent = $receivedCouncilApp->post($pURL, Content => $postCnt);
	
	}
	elsif($councilCode=~m/^(489)$/is)
	{	
		my ($application_id,$application_name);
		if($receivedApplicationUrl=~m/application\/([^>]*?)\/([^>]*?)$/is)
		{
			$application_id=$1;
			$application_name=$2;
		}
		
		my $receivedCouncilApptemp=$receivedCouncilApp;
	
		my $pURL = "https://development.wiltshire.gov.uk/pr/s/sfsites/aura?r=0&arcshared.RecordServiceCont.getRecord=1&ui-comm-runtime-components-aura-components-siteforce-qb.Quarterback.validateRoute=1&ui-communities-components-aura-components-forceCommunity-controller.RecordValidation.getOnLoadErrorMessage=2&ui-communities-components-aura-components-forceCommunity-richText.RichText.getParsedRichTextValue=2&ui-communities-components-aura-components-forceCommunity-seoAssistant.SeoAssistant.getRecordAndTranslationData=1&ui-communities-components-aura-components-forceCommunity-tabset.Tabset.getLocalizedRegionLabels=1&ui-force-components-controllers-detail.Detail.getEntityConfig=1";
		
		
		my ($commonFileDetails,$postCnt);
		if($category eq "Planning")
		{
			$commonFileDetails=$councilDetailsFile;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
			print "receivedCouncilApptemp:: $commonFileDetails->{$councilCode}->{'URL'}\n";<STDIN>;
			my $responseCont = $receivedCouncilApptemp->content;
			
			open KK,">responseCont.html";
			print KK "$responseCont";
			close KK;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			print "application_id:: $application_id\n";
			print "application_name:: $application_name\n";
			print "fwuid:: $fwuid\n";
			print "loaded:: $loaded\n";
			
			my $pURL_Date='https://development.wiltshire.gov.uk/pr/s/sfsites/aura?r=1&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1';
			
			my $postCnt2="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22158%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.0123z000000VWo9AAG.null.null.null.Name.VIEW.false.null.Name%2CId%2Carcusbuiltenv__Status__c%2CLastModifiedDate%2Carcusbuiltenv__Planning_Application__r%3B2Id%2Carcusbuiltenv__Planning_Application__c%2Carcusbuiltenv__Type__c%2Carcusbuiltenv__Officer__r%3B2Id%2Carcusbuiltenv__Latest_Decision_Date__c%2Carcusbuiltenv__Consultation_Deadline__c%2Carcusbuiltenv__Planning_Application__r%3B2RecordTypeId%2Carcusbuiltenv__Officer__c%2Carcusbuiltenv__Current_Decision_Expiry_Date__c%2CSystemModstamp%2Carcusbuiltenv__Date_of_Committee__c%2Carcusbuiltenv__Wards__c%2Carcusbuiltenv__Decision_Notice_Sent_Date_Manual__c%2CWilts_Web_Decision__c%2CRecordTypeId%2CCreatedDate%2Carcusbuiltenv__Planning_Application__r%3B2Name%2Carcusbuiltenv__Parishes__c%2CLastModifiedById%2Carcusbuiltenv__Officer__r%3B2Name%2Carcusbuiltenv__Valid_Date__c.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2Fforce%3AoutputField%22%3A%22AicR8X3GofV-uF385LcFAg%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22TvkWWYjc5lgUaBHUxjkWQw%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22jSiyr0ijnSHr5crHwZ5K5g%22%2C%22COMPONENT%40markup%3A%2F%2Fruntime_sales_pathassistant%3ApathAssistant%22%3A%22koOSI2hh5QEPDRQL8w_sxA%22%2C%22COMPONENT%40markup%3A%2F%2Fsiteforce%3AregionLoaderWrapper%22%3A%22MSlWg5juejNIoUafZbExJA%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AinputDate%22%3A%22bFizltER6sNf_S6GJgikFw%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputURL%22%3A%229RQ8WeNmi1COAzt-FFMs3A%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
			$receivedCouncilApp->post($pURL_Date, Content => $postCnt2);
			$applicationDetailsPageContent= $receivedCouncilApp->content;
			
			open KK,">application Date.html";
			print KK $receivedCouncilApp->content;
			close KK;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%2253%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.seoAssistant.SeoAssistantController%2FACTION%24getRecordAndTranslationData%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3AseoAssistant%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%2C%22fields%22%3A%5B%5D%2C%22activeLanguageCodes%22%3A%5B%5D%7D%2C%22version%22%3A%2251.0%22%7D%2C%7B%22id%22%3A%22110%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.richText.RichTextController%2FACTION%24getParsedRichTextValue%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22html%22%3A%22%3Cdiv%20class%3D%5C%22slds-clearfix%5C%22%3E%5Cn%20%20%20%20%3Ca%20href%3D%5C%22%2Fpr%2Fs%2F%5C%22%20class%3D%5C%22slds-button%20slds-button_brand%20slds-float_right%5C%22style%3D%5C%22color%3A%20white%3B%5C%22%3EPublic%20register%20home%3C%2Fa%3E%5Cn%3C%2Fdiv%3E%22%7D%2C%22version%22%3A%2251.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%2262%3Ba%22%2C%22descriptor%22%3A%22apex%3A%2F%2Farcshared.RecordServiceCont%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2Farcshared%3AAttributeConverter%22%2C%22params%22%3A%7B%22objName%22%3A%22arcusbuiltenv__Planning_application__c%22%2C%22objFields%22%3A%22Name%2C%20arcusbuiltenv__Site_Address__c%2C%20arcusbuiltenv__Proposal__c%22%2C%22recordId%22%3A%22$application_id%22%2C%22showPicklistFieldsLabelsInsteadOfValues%22%3Atrue%7D%7D%2C%7B%22id%22%3A%22111%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.richText.RichTextController%2FACTION%24getParsedRichTextValue%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22html%22%3A%22%3Cp%3E%26nbsp%3B%26nbsp%3B%3C%2Fp%3E%22%7D%2C%22version%22%3A%2251.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%22113%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.detail.DetailController%2FACTION%24getEntityConfig%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%2C%22layoutType%22%3A%22FULL%22%2C%22mode%22%3A%22VIEW%22%2C%22isCreateOrClone%22%3Afalse%2C%22isCloneWithRelated%22%3Afalse%7D%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%2282%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.controller.RecordValidationController%2FACTION%24getOnLoadErrorMessage%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3ArecordDetail%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%7D%2C%22version%22%3A%2251.0%22%7D%2C%7B%22id%22%3A%2286%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.controller.RecordValidationController%2FACTION%24getOnLoadErrorMessage%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%7D%2C%22version%22%3A%2251.0%22%7D%2C%7B%22id%22%3A%22126%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.tabset.TabsetController%2FACTION%24getLocalizedRegionLabels%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22componentId%22%3A%228903c6e7-8901-47b1-a4f7-81c9b69e9ba7%22%7D%2C%22version%22%3A%2251.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%22105%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.comm.runtime.components.aura.components.siteforce.qb.QuarterbackController%2FACTION%24validateRoute%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22routeId%22%3A%2247d7e095-2156-4afa-ba57-d82a32bcc73a%22%2C%22viewParams%22%3A%7B%22viewid%22%3A%22621bee52-e8d3-44fe-8a3a-61f0d2f6061d%22%2C%22view_uddid%22%3A%220I33z000007QIGZ%22%2C%22entity_name%22%3A%22arcusbuiltenv__Planning_Application__c%22%2C%22audience_name%22%3A%22Default%22%2C%22recordId%22%3A%22$application_id%22%2C%22recordName%22%3A%222101686ful%22%2C%22picasso_id%22%3A%2247d7e095-2156-4afa-ba57-d82a32bcc73a%22%2C%22routeId%22%3A%2247d7e095-2156-4afa-ba57-d82a32bcc73a%22%7D%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2Fforce%3AoutputField%22%3A%22AicR8X3GofV-uF385LcFAg%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22TvkWWYjc5lgUaBHUxjkWQw%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22jSiyr0ijnSHr5crHwZ5K5g%22%2C%22COMPONENT%40markup%3A%2F%2Fruntime_sales_pathassistant%3ApathAssistant%22%3A%22koOSI2hh5QEPDRQL8w_sxA%22%2C%22COMPONENT%40markup%3A%2F%2Fsiteforce%3AregionLoaderWrapper%22%3A%22MSlWg5juejNIoUafZbExJA%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AinputDate%22%3A%22bFizltER6sNf_S6GJgikFw%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputURL%22%3A%229RQ8WeNmi1COAzt-FFMs3A%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
		}
		elsif($category eq "Decision")
		{
			$commonFileDetails=$councilDetailsDecision;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
			
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			my $pURL_Date='https://development.wiltshire.gov.uk/pr/s/sfsites/aura?r=1&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1';
			my $postCnt2="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22158%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.0123z000000VWo9AAG.null.null.null.Name.VIEW.false.null.Id%2CName%2Carcusbuiltenv__Status__c%2CLastModifiedDate%2Carcusbuiltenv__Planning_Application__r%3B2Id%2Carcusbuiltenv__Planning_Application__c%2Carcusbuiltenv__Type__c%2Carcusbuiltenv__Officer__r%3B2Id%2Carcusbuiltenv__Latest_Decision_Date__c%2Carcusbuiltenv__Consultation_Deadline__c%2Carcusbuiltenv__Planning_Application__r%3B2RecordTypeId%2Carcusbuiltenv__Officer__c%2Carcusbuiltenv__Current_Decision_Expiry_Date__c%2CSystemModstamp%2Carcusbuiltenv__Date_of_Committee__c%2Carcusbuiltenv__Wards__c%2Carcusbuiltenv__Decision_Notice_Sent_Date_Manual__c%2CWilts_Web_Decision__c%2CRecordTypeId%2CCreatedDate%2Carcusbuiltenv__Planning_Application__r%3B2Name%2Carcusbuiltenv__Parishes__c%2CLastModifiedById%2Carcusbuiltenv__Officer__r%3B2Name%2Carcusbuiltenv__Valid_Date__c.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2Fforce%3AoutputField%22%3A%22AicR8X3GofV-uF385LcFAg%22%2C%22COMPONENT%40markup%3A%2F%2Fforce%3ApreviewPanel%22%3A%22KAmhatpwZOI1HkC51ERrKQ%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22TvkWWYjc5lgUaBHUxjkWQw%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22jSiyr0ijnSHr5crHwZ5K5g%22%2C%22COMPONENT%40markup%3A%2F%2Fruntime_sales_pathassistant%3ApathAssistant%22%3A%22koOSI2hh5QEPDRQL8w_sxA%22%2C%22COMPONENT%40markup%3A%2F%2Fsiteforce%3AregionLoaderWrapper%22%3A%22MSlWg5juejNIoUafZbExJA%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AinputDate%22%3A%22bFizltER6sNf_S6GJgikFw%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputURL%22%3A%229RQ8WeNmi1COAzt-FFMs3A%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
			$receivedCouncilApp->post($pURL_Date, Content => $postCnt2);
			$applicationDetailsPageContent= $receivedCouncilApp->content;
			
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%2253%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.seoAssistant.SeoAssistantController%2FACTION%24getRecordAndTranslationData%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3AseoAssistant%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%2C%22fields%22%3A%5B%5D%2C%22activeLanguageCodes%22%3A%5B%5D%7D%2C%22version%22%3A%2251.0%22%7D%2C%7B%22id%22%3A%2256%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.richText.RichTextController%2FACTION%24getParsedRichTextValue%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3ArichText%22%2C%22params%22%3A%7B%22html%22%3A%22%3Cdiv%20class%3D%5C%22slds-clearfix%5C%22%3E%5Cn%20%20%20%20%3Ca%20href%3D%5C%22%2Fpr%2Fs%2F%5C%22%20class%3D%5C%22slds-button%20slds-button_brand%20slds-float_right%5C%22style%3D%5C%22color%3A%20white%3B%5C%22%3EPublic%20register%20home%3C%2Fa%3E%5Cn%3C%2Fdiv%3E%22%7D%2C%22version%22%3A%2251.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%2262%3Ba%22%2C%22descriptor%22%3A%22apex%3A%2F%2Farcshared.RecordServiceCont%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2Farcshared%3AAttributeConverter%22%2C%22params%22%3A%7B%22objName%22%3A%22arcusbuiltenv__Planning_application__c%22%2C%22objFields%22%3A%22Name%2C%20arcusbuiltenv__Site_Address__c%2C%20arcusbuiltenv__Proposal__c%22%2C%22recordId%22%3A%22$application_id%22%2C%22showPicklistFieldsLabelsInsteadOfValues%22%3Atrue%7D%7D%2C%7B%22id%22%3A%2267%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.richText.RichTextController%2FACTION%24getParsedRichTextValue%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3ArichText%22%2C%22params%22%3A%7B%22html%22%3A%22%3Cp%3E%26nbsp%3B%26nbsp%3B%3C%2Fp%3E%22%7D%2C%22version%22%3A%2251.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%2278%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.detail.DetailController%2FACTION%24getEntityConfig%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%2C%22layoutType%22%3A%22FULL%22%2C%22mode%22%3A%22VIEW%22%2C%22isCreateOrClone%22%3Afalse%2C%22isCloneWithRelated%22%3Afalse%7D%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%2282%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.controller.RecordValidationController%2FACTION%24getOnLoadErrorMessage%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3ArecordDetail%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%7D%2C%22version%22%3A%2251.0%22%7D%2C%7B%22id%22%3A%2286%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.controller.RecordValidationController%2FACTION%24getOnLoadErrorMessage%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%2C%22params%22%3A%7B%22recordId%22%3A%22$application_id%22%7D%2C%22version%22%3A%2251.0%22%7D%2C%7B%22id%22%3A%2296%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.tabset.TabsetController%2FACTION%24getLocalizedRegionLabels%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3Atabset%22%2C%22params%22%3A%7B%22componentId%22%3A%228903c6e7-8901-47b1-a4f7-81c9b69e9ba7%22%7D%2C%22version%22%3A%2251.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%22105%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.comm.runtime.components.aura.components.siteforce.qb.QuarterbackController%2FACTION%24validateRoute%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22routeId%22%3A%2247d7e095-2156-4afa-ba57-d82a32bcc73a%22%2C%22viewParams%22%3A%7B%22viewid%22%3A%22621bee52-e8d3-44fe-8a3a-61f0d2f6061d%22%2C%22view_uddid%22%3A%220I33z000007QIGZ%22%2C%22entity_name%22%3A%22arcusbuiltenv__Planning_Application__c%22%2C%22audience_name%22%3A%22Default%22%2C%22recordId%22%3A%22$application_id%22%2C%22recordName%22%3A%222102264tca%22%2C%22picasso_id%22%3A%2247d7e095-2156-4afa-ba57-d82a32bcc73a%22%2C%22routeId%22%3A%2247d7e095-2156-4afa-ba57-d82a32bcc73a%22%7D%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2Fforce%3AoutputField%22%3A%22AicR8X3GofV-uF385LcFAg%22%2C%22COMPONENT%40markup%3A%2F%2Fforce%3ApreviewPanel%22%3A%22KAmhatpwZOI1HkC51ERrKQ%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22TvkWWYjc5lgUaBHUxjkWQw%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22jSiyr0ijnSHr5crHwZ5K5g%22%2C%22COMPONENT%40markup%3A%2F%2Fruntime_sales_pathassistant%3ApathAssistant%22%3A%22koOSI2hh5QEPDRQL8w_sxA%22%2C%22COMPONENT%40markup%3A%2F%2Fsiteforce%3AregionLoaderWrapper%22%3A%22MSlWg5juejNIoUafZbExJA%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AinputDate%22%3A%22bFizltER6sNf_S6GJgikFw%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputURL%22%3A%229RQ8WeNmi1COAzt-FFMs3A%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id%2F$application_name&aura.token=undefined";
			
		}
			
		$receivedCouncilApp->post($pURL, Content => $postCnt);
		
	}
	else
	{
		$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	}
	$applicationDetailsPageResponse = $receivedCouncilApp->status();
	$applicationDetailsPageContent.= $receivedCouncilApp->content;
	
	print "applicationDetailsPageResponse:: $applicationDetailsPageResponse\n";
	
	open KK,">applicationDetailsPageContent.html";
	print KK "$applicationDetailsPageContent";
	close KK;
	
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		if($receivedApplicationUrl=~m/reference\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/pRecordID\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/iAppID\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/appno\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/TheSystemkey\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/ApplicationReference\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/Planning\/Display\/([^\n]*?)$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/applicationNumber\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/AppNo\=([^<]*?)\$/s)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/id\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/recno\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/reference\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/appkey\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}

####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/\\n/ /igs;
	$data2Clean=~s/\\r/ /igs;
	$data2Clean=~s/\\u/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	$data2Clean=~s/ô/�/igs;
	$data2Clean=~s/á/�/igs;

	return($data2Clean);
}
1;