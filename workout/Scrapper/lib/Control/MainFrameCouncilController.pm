package MainFrameCouncilController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use URI::Escape;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
# use Time::Piece;
use Time::HiRes qw(gettimeofday);
use IO::Socket::SSL;
# use Net::SSL;
use Net::Ping;
use HTTP::CookieMonster;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/MainFrame');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/MainFrame_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/MainFrame_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/MainFrame_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/MainFrame_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');


###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType, $session_id);
	
	print "currentCouncilCode==>$currentCouncilCode\n";
	print "category==>$category\n";
			
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "MainFrame_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "MainFrame_Decision";
	}	

	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
		
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
		exit;
	}
		
	my $rerunCount=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
	if($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working via Old proxy..\n";
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	}
	
	# $responseContent = $councilApp->get($commonFileDetails->{$currentCouncilCode}->{'URL'});
	
	
	my $searchURL = $commonFileDetails->{$currentCouncilCode}->{'URL'};
			
	($session_id) = &get_cookie_session_details($searchURL,$councilApp,'JSESSIONID');
	print "session_id=>$session_id\n";
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$councilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	
	$responseContent = $councilApp->get($searchURL);
	# print "responseContent==>$responseContent\n";
		
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}
	# open(TIME,">fileName.html");
	# print TIME "$responseContent\n";
	# close TIME;	
	# exit;
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $AGENT_PHONE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $AGENT_NAME, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL, $TARGET_DATE,$AGENT_COMPANY,$NORTHING,$EASTING,$GRIDREFERENCE);
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
	$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}	
		
	if($Easting eq "")
	{
		if($EASTING ne "")
		{
			$Easting = $EASTING;
		}
	}		
	
	if($Northing eq "")
	{
		if($NORTHING ne "")
		{
			$Northing = $NORTHING;
		}
	}		
	
	if($gridReference eq "")
	{
		if($GRIDREFERENCE ne "")
		{
			$gridReference = $GRIDREFERENCE;
			
			if($gridReference=~m/^\s*([^>]*?)\s*,\s*([^>]*?)\s*$/is)
			{
				my $newEasting=$1;
				my $newNorthing=$2;
				if($Easting eq "")
				{
					$Easting = $newEasting if($newEasting ne "");
				}
				if($Northing eq "")
				{
					$Northing = $newNorthing if($newNorthing ne "");
				}
			}
		}
	}	
	
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}		
	
	if($agentCompanyName eq "")
	{
		if($AGENT_COMPANY ne "")
		{
			$agentCompanyName = $AGENT_COMPANY;
		}
	}		
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}	
		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
	}		
	
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}
	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
	}
	
	if($agentName eq "")
	{
		if($AGENT_NAME ne "")
		{
			$agentName = $AGENT_NAME;
		}
	}				
			
	if($agentTelephone1 eq "")
	{
		if($AGENT_PHONE ne "")
		{
			$agentTelephone1 = $AGENT_PHONE;
		}
	}				
			
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}				
					
		
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}					
		
	if($applicationStatus eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$applicationStatus = $APPLICATION_STATUS;
		}
	}	
	
	if($applicantName eq "")
	{
		if($APPLICANT_NAME ne "")
		{
			$applicantName = $APPLICANT_NAME;
		}
	}	
	
	if($targetDecdt eq "")
	{
		if($TARGET_DATE ne "")
		{
			$targetDecdt = $TARGET_DATE;
		}
	}	
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			$documentURL = $applicationLink;
		}
	
		$documentURL =~s/\'/\'\'/igs;
		$documentURL =~s/\&amp\;/\&/igs;
	}
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/gsi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/gsi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	# print "insert_query::$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
		
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_ISSUED_DATE,$DEVELOPMENT_ADDRESS,$GRANT_DATE);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
	$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
	$DECISION_ISSUED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'GRANT_DATE'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}		
			
	if($Decision_Issued_Date eq "")
	{
		if($DECISION_ISSUED_DATE ne "")
		{
			$Decision_Issued_Date=$DECISION_ISSUED_DATE;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
			
			$Date_Decision_Made=~s/^Not\s*Known$//is;
		}	
		
	}		
		
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}		
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
	}		
	
	if($Decision_Status eq "")
	{
		if($DECISION_STATUS ne "")
		{
			$Decision_Status = $DECISION_STATUS;
		}
	}
			
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/gsi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/gsi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/gsi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/gsi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $PLANNING_APPLICATION;  undef $PROPOSAL;  undef $DECISION_STATUS;  undef $APPLICATION_STATUS;  undef $DECISION_DATE;  undef $DECISION_ISSUED_DATE;  undef $applicationLink;
	undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Application_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
	# print "$insert_query\n";
	# <STDIN>;
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
   
	$URL=~s/amp\;//igs;
	
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy

	# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy

	
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
    return($con,$code);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;
	
	
    my ($councilAppResponse, $commonFileDetails, $scriptStatus,$postContent, $searchURL,$HOST,$URL,$cur_year,$cur_month,$cur_date,$prev_year,$prev_Month,$prev_date);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	$HOST = $commonFileDetails->{$receivedCouncilCode}->{'HOST'};	
	$URL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};	
	$searchURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};	
	$postContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
	my $formNumber = $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'};
	my $fromDate = $commonFileDetails->{$receivedCouncilCode}->{'FORM_START_DATE'};
	my $toDate = $commonFileDetails->{$receivedCouncilCode}->{'FORM_END_DATE'};
	my $searchButton = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'};
	my $searchButtonVal = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_VAL'};
	my $RADIO_BTN_NME = $commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_NME'};
	my $RADIO_BTN_VAL = $commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_VAL'};
	
	$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'}); 
		
	$councilAppResponse = $receivedCouncilApp->content;	
	my $currentPageURL = $receivedCouncilApp->uri();
	# print "currentPageURL==>$currentPageURL\n";
	
	$scriptStatus = $receivedCouncilApp->status;		
	# print "councilAppStatus==>$scriptStatus\n";
	
	$receivedCouncilApp->get($searchURL);
	my $homecont = $receivedCouncilApp->content;
	my $pingStatus = $receivedCouncilApp->status;
	
	# open(DC,">$councilCode"."_Home.html");
	# print DC $homecont;
	# close DC;
	
	my $View=uri_escape($1) if($homecont=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATE_REGEX'}/is);
	my $Event=uri_escape($1) if($homecont=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_EVENTVALIDATION'}/is);
	my $View_gen=uri_escape($1) if($homecont=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATEGENERATOR'}/is);
	
	my $session_id = &get_cookie_session_details($URL,$receivedCouncilApp, 'JSESSIONID');
	print "session_id=>$session_id\n";
	
	$receivedCouncilApp->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
	$receivedCouncilApp->add_header( 'Accept-Encoding' => 'gzip, deflate');
	$receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
	$receivedCouncilApp->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
	$receivedCouncilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	$receivedCouncilApp->add_header( 'Host' => $HOST);
	$receivedCouncilApp->add_header( 'Referer' => $URL);
	$receivedCouncilApp->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');

	my $curFromDate=$receivedToDate;
	my $prevToDate=$receivedFromDate;
	
	if($curFromDate=~m/([\d]+)\/([\d]+)\/([\d]+)/is)
	{
		$cur_date=$1;
		$cur_month=$2;
		$cur_year=$3;
		$cur_month=~s/^0//gsi;
		$cur_date=~s/^0//gsi;
	}
	if($prevToDate=~m/([\d]+)\/([\d]+)\/([\d]+)/is)
	{
		$prev_date=$1;
		$prev_Month=$2;
		$prev_year=$3;
		$prev_Month=~s/^0//gsi;		
		$prev_date=~s/^0//gsi;	
	}
	$receivedFromDate=~s/\//\%2F/gsi;
	$receivedToDate=~s/\//\%2F/gsi;
	
	$postContent=~s/<FromDate>/$receivedFromDate/gsi;
	$postContent=~s/<ToDate>/$receivedToDate/gsi;
	$postContent=~s/<Year>/$cur_year/gsi;
	$postContent=~s/<Month>/$cur_month/gsi;
	$postContent=~s/<Date>/$cur_date/gsi;
	$postContent=~s/<Year180>/$prev_year/gsi;
	$postContent=~s/<Month180>/$prev_Month/gsi;
	$postContent=~s/<Date180>/$prev_date/gsi;
	$postContent=~s/<VIEWSTATEGENERATOR>/$View_gen/gsi;
	$postContent=~s/<VIEWSTATE>/$View/gsi;
	$postContent=~s/<EVENTVALIDATION>/$Event/gsi;
	
	$receivedCouncilApp->post($searchURL, Content => "$postContent");
	my $Content = $receivedCouncilApp->content;
	my $pingStatus = $receivedCouncilApp->status;
	print "pingStatus=>$pingStatus\n"; 
	# print "Content=>$Content\n"; 
	# open(TIME,">/home/merit/Glenigan_New/councilscrapers-new-dev/lib/fileName.html");
	# print TIME "$Content\n";
	# close TIME;
	# exit;
    return ($Content,$scriptStatus,$currentPageURL);
}

####
# Method to POST the pagenation settings for MainFrame Councils
####

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $pageContent, $receivedFromDate, $receivedToDate) = @_;
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	$commonContent .= $pageContent;
	
	my $councilType = $commonFileDetails->{$receivedCouncilCode}->{'FORMAT_TYPE'};
		
	if($councilType=~m/^POSTFORMAT$/is)
	{		
		my $pageCheck = 2;		
		my $MaxPage = 2;		
				
		NextPostPage:
		my $nextPagecheck = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'};		
		$nextPagecheck=~s/<PAGENUM>/$pageCheck/gsi;
		
		# print "nextPagecheck==>$nextPagecheck\n";
		
		if($pageContent=~m/$nextPagecheck/is)
		{		
			my $eventTargetRegex = $commonFileDetails->{$receivedCouncilCode}->{'EVENTTARGET'};
			my $eventArgumentRegex = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_EVENTARGUMENT'};
			$eventTargetRegex=~s/<PAGENUM>/$pageCheck/gsi;
			# my $eventTarget = uri_escape($1) if($pageContent=~m/$eventTargetRegex/is);
			my $eventTarget = uri_escape($1) if($pageContent=~m/$nextPagecheck/is);
			$eventArgumentRegex=~s/<PAGENUM>/$pageCheck/gsi;
			my $eventArgument = uri_escape($1) if($pageContent=~m/$eventArgumentRegex/is);
			my $viewState = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATE_REGEX'}/is);
			my $viewStateGen = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATEGENERATOR'}/is);
			my $eventValidation = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_EVENTVALIDATION'}/is);
			
			my $nextPagePostCnt = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_POST_CONTENT'};			
			$nextPagePostCnt=~s/<VIEWSTATE>/$viewState/is;
			$nextPagePostCnt=~s/<EVENTTARGET>/$eventTarget/is;
			$nextPagePostCnt=~s/<EVENTARGUMENT>/$eventArgument/is;
			$nextPagePostCnt=~s/<VIEWSTATEGENERATOR>/$viewStateGen/is;
			$nextPagePostCnt=~s/<EVENTVALIDATION>/$eventValidation/is;
			# print "nextPagePostCnt==>$nextPagePostCnt\n";
			# my $HOST = $commonFileDetails->{$receivedCouncilCode}->{'HOST'};	
			# my $URL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};	
			# print "URL=>$URL\n";
			# my $session_id = &get_cookie_session_details($URL,$receivedCouncilApp, 'JSESSIONID');
			# print "session_id=>$session_id\n";
			# my $origin='http://'.$HOST;
			# $receivedCouncilApp->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
			# $receivedCouncilApp->add_header( 'Accept-Encoding' => 'gzip, deflate');
			# $receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
			# $receivedCouncilApp->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
			# $receivedCouncilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
			# $receivedCouncilApp->add_header( 'Host' => $HOST);
			# # $receivedCouncilApp->add_header( 'Origin' => $origin);
			# # $receivedCouncilApp->add_header( 'Referer' => $SEARCH_URL);
			# my $User_Agent = &UA();	
			# $receivedCouncilApp->add_header( 'User-Agent' => $User_Agent);
			# # $receivedCouncilApp->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');
			
			my $SEARCH_URL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};	
			my $Next_URL = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_URL'};
				
			$receivedCouncilApp->post( $Next_URL, Content => "$nextPagePostCnt");
			
			my $nextPageContent = $receivedCouncilApp->content;
			$pageContent = $nextPageContent;
			my $pingStatus = $receivedCouncilApp->status;
			print "pingStatus=>$pingStatus\n"; 
			# print "pageContent=>$pageContent\n"; 
			sleep(5+rand(5));
									
			my $nextPageCheckRegex = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'};
			$nextPageCheckRegex=~s/<PAGENUM>/$pageCheck/is;
			
			# print "nextPageCheckRegex=>$nextPageCheckRegex\n";			
			if($pageContent=~m/$nextPageCheckRegex/is)
			{
				print "Current page number is $pageCheck\n";
				$commonContent .= $pageContent;
				$pageCheck++;
				$MaxPage++;
				goto NextPostPage;
			}
			else
			{
				$commonContent .= $pageContent;				
				print "Next page number is $pageCheck\n";				
			}			
		}
		else
		{
			print "Current page number is 1\n";
			$commonContent;
		}
	}
	
	# open(TIME,">/home/merit/Glenigan_New/councilscrapers-new-dev/lib/commonContent.html");
	# print TIME "$commonContent\n";
	# close TIME;
	# <STDIN>;
	return($commonContent);
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);	
	$applicationDetailsPageResponse = $receivedCouncilApp->status();
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
		
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		if($receivedApplicationUrl=~m/reference\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/pRecordID\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/iAppID\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/appno\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/TheSystemkey\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/ApplicationReference\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/Planning\/Display\/([^\n]*?)$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/applicationNumber\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/AppNo\=([^<]*?)\$/s)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/id\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/recno\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/reference\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/appkey\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	# open(TIME,">fileName.html");
	# print TIME "$applicationDetailsPageContent\n";
	# close TIME;
	# exit;
	print "Current Application number::$applicationNumber\n";
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}

####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	
	return($data2Clean);
}

sub get_cookie_session_details()
{
	my $url = shift;
	my $mech = shift;
	my $keyName = shift;
	
	my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
	$mech->get( $url );	
	my $pingStatus = $mech->status;
	# my $responseContent = $mech->content;	
	my $cookie = $monster->get_cookie($keyName);	
	my $sessionID;
	eval
	{
		$sessionID = $cookie->val();
	};
	if($@)
	{
		print "JSESSIONCOOKIEISSUE_REASON==>$@\n";
		$sessionID = "JSESSION_ISSUE";
	}
	
	return($sessionID);
}

sub UA()
{
	# my $useragent=shift;
	my @useragent=('Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.0.04506.30)','Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; Media Center PC 3.0; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1)','Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; FDM; .NET CLR 1.1.4322)','Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; fr-FR)','Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; en-US)','Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.2; WOW64; .NET CLR 2.0.50727)','Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; .NET CLR 2.7.58687; SLCC2; Media Center PC 5.0; Zune 3.4; Tablet PC 3.6; InfoPath.3)','Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)','Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322)','Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727)','Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727)','Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; SLCC1; .NET CLR 1.1.4322)','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; InfoPath.3; MS-RTC LM 8; Zune 4.7)','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; InfoPath.3; MS-RTC LM 8; Zune 4.7','Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Zune 4.0; InfoPath.3; MS-RTC LM 8; .NET4.0C; .NET4.0E)','Mozilla/5.0 (MSIE 7.0; Macintosh; U; SunOS; X11; gu; SV1; InfoPath.2; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)','Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; c .NET CLR 3.0.04506; .NET CLR 3.5.30707; InfoPath.1; el-GR)','Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; c .NET CLR 3.0.04506; .NET CLR 3.5.30707; InfoPath.1; el-GR)','Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; fr-FR)','Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0','Mozilla/5.0 (X11; Linux i586; rv:63.0) Gecko/20100101 Firefox/63.0','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0','Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.10; rv:62.0) Gecko/20100101 Firefox/62.0','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:10.0) Gecko/20100101 Firefox/62.0','Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.13; ko; rv:1.9.1b2) Gecko/20081201 Firefox/60.0','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Firefox/58.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/58.0','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.59.12) Gecko/20160044 Firefox/52.59.12','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36','Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2224.3 Safari/537.36','Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36','Mozilla/5.0 (Windows NT 4.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36','Mozilla/5.0 (X11; OpenBSD i386) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2117.157 Safari/537.36','Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/23.0','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0');
	my $useragent = $useragent[rand @useragent];
	# print "useragent::$useragent\n\n";
	
	return $useragent;
}

1;
