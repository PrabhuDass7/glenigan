package WexfordCouncilController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use IO::Socket::SSL;
# use Net::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}



my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Wexford');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Wexford_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Wexford_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Wexford_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Wexford_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');



###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType);
	
			
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Wexford_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Wexford_Decision";
	}	

	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
		exit;
	}
		
	my $rerunCount=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
		
	if($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working via Old proxy..\n";
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	}
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5') if($currentCouncilCode=~m/^94$/is);
	
	$responseContent = $councilApp->get($commonFileDetails->{$currentCouncilCode}->{'URL'});
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}
	
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $AGENT_PHONE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $AGENT_NAME, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL, $TARGET_DATE,$AGENT_COMPANY,$NORTHING,$EASTING,$GRIDREFERENCE);
	
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$REGISTRATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
	$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	$VALID_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);	
	$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
	$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
	$APPLICANT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);	
	$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
	$AGENT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);	
	$AGENT_PHONE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
	$TARGET_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$EASTING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'EASTING'}/is);
	$NORTHING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'NORTHING'}/is);
	$GRIDREFERENCE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'GRIDREFERENCE'}/is);
	$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
	# print "DOC_URL:::$DOC_URL\n\n";
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
		else
		{
			$Application = $1 if($applicationLink=~m/Ref(?:Val)?\=([^<]*?)$/is);
		}
	}	
			
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}		
			
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}	
	if($Proposal eq "")
	{	
		if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is)
		{
			my $block=$1;
			if($block=~m/<iframe[^>]*?src\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>/is)
			{
				my $proposal_url=$1;

				if($proposal_url!~m/https?\:/is)
				{
					$proposal_url = URI::URL->new($proposal_url)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
				}
				my ($proposalContent, $dateCode) = &getMechContent($proposal_url, $councilApp);
				$Proposal = &htmlTagClean($1) if($proposalContent=~m/<p>\s*([\w\W]*?)\s*<\/p>\s*<\/body>/is);
			}
		}
	}		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
	}		
	
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}
	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
	}
	
	if($agentName eq "")
	{
		if($AGENT_NAME ne "")
		{
			$agentName = $AGENT_NAME;
		}
	}				
			
	if($agentTelephone1 eq "")
	{
		if($AGENT_PHONE ne "")
		{
			$agentTelephone1 = $AGENT_PHONE;
		}
	}				
			
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}				
					
		
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}					
		
	if($applicationStatus eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$applicationStatus = $APPLICATION_STATUS;
		}
	}	
	
	if($Easting eq "")
	{
		if($EASTING ne "")
		{
			$Easting = $EASTING;
		}
	}	
	if($Northing eq "")
	{
		if($NORTHING ne "")
		{
			$Northing = $NORTHING;
		}
	}		
	
	if($gridReference eq "")
	{
		if($GRIDREFERENCE ne "")
		{
			$gridReference = $GRIDREFERENCE;
		}
	}	
	
	if($applicantName eq "")
	{
		if($APPLICANT_NAME ne "")
		{
			$applicantName = $APPLICANT_NAME;
		}
	}	
	
	if($targetDecdt eq "")
	{
		if($TARGET_DATE ne "")
		{
			$targetDecdt = $TARGET_DATE;
		}
	}	
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			$documentURL = $applicationLink;
		}
		$documentURL=~s/\&amp\;/\&/gsi;
	
		$documentURL =~s/\'/\'\'/igs;
	}
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	# print "documentURL::::$documentURL\n";
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
		
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_STATUS_NEW,$DECISION_ISSUED_DATE);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
	$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
	$DECISION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS_NEW'}/is);
	$DECISION_ISSUED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
		else
		{
			$Application = $1 if($applicationLink=~m/Ref(?:Val)?\=([^<]*?)$/is);
		}
	}		
			
	if($Decision_Issued_Date eq "")
	{
		if($DECISION_ISSUED_DATE ne "")
		{
			$Decision_Issued_Date=$DECISION_ISSUED_DATE;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
		}
		
	}		
		
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}
	if($Proposal eq "")
	{	
		if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is)
		{
			my $block=$1;
			if($block=~m/<iframe[^>]*?src\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>/is)
			{
				my $proposal_url=$1;

				if($proposal_url!~m/https?\:/is)
				{
					$proposal_url = URI::URL->new($proposal_url)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
				}	
				my ($proposalContent, $dateCode) = &getMechContent($proposal_url, $councilApp);
				$Proposal = &htmlTagClean($1) if($proposalContent=~m/<p>\s*([\w\W]*?)\s*<\/p>\s*<\/body>/is);
			}
		}
	}			
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
	}		
	
	if($Decision_Status eq "")
	{
		
		if($DECISION_STATUS ne "")
		{
			$Decision_Status = $DECISION_STATUS;
		}
		elsif($DECISION_STATUS_NEW ne "")
		{
			$Decision_Status = $DECISION_STATUS_NEW;
		}
	}	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $PLANNING_APPLICATION;  undef $PROPOSAL;  undef $DECISION_STATUS;  undef $APPLICATION_STATUS;  undef $DECISION_DATE;  undef $DECISION_ISSUED_DATE;  undef $applicationLink;
	undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Application_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
    my $cCode = shift;
   
	$URL=~s/amp\;//igs;
	
	# if($cCode!~m/^478$/is)
	# {
		# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy
	# }
	# else
	# {
		# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy
	# }
	
	my $count = 0;
	Loop:
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
	if(($code!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($con=~m/A\s*problem\s*has\s*ocurred\s*in\s*the\s*repeater\s*process/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($con=~m/<h2>\s*404\s*\-\s*File\s*or\s*directory\s*not\s*found\.\s*<\/h2>/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($con=~m/temporarily\s*unavailable/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
    return($con,$code);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;
	
    # Get search results using date ranges	
	my $tempFromDate=$receivedFromDate;
	my $tempToDate=$receivedToDate;
	$tempFromDate=~s/\//\-/gsi;
	$tempToDate=~s/\//\-/gsi;
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $commonFileDetails, $scriptStatus,$postContent);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	$postContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
	$postContent =~s/<FromDate>/$receivedFromDate/igs;
	$postContent =~s/<ToDate>/$receivedToDate/igs;
	
	$councilAppResponse = $receivedCouncilApp->post($commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'}, Content => "$postContent");
	$councilAppResponse = $receivedCouncilApp->content;
	
	$scriptStatus = $receivedCouncilApp->status;		
	print "councilAppStatus==>$scriptStatus\n";
	
	
    return ($councilAppResponse,$scriptStatus);
}

####
# Method to POST the pagenation settings for Online Councils
####

sub searchNextWeeklyRange()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $weeklyURLs, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent, $rawFileName) = @_;
	
	
	my (@appURLs, $appLinkDup, $appLinkCount, $responseContent, $newAppLinkcheck,$dumFlagCheck);
	
	if($postContent ne "")
	{
		my ($totalAppURL, $redisDup, $totalCount, $newAppcheck) = &collectApplicationDetails($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent,$receivedCouncilApp);
		@appURLs = @{$totalAppURL};
		$appLinkDup = $redisDup;
		$appLinkCount = $totalCount;
		$newAppLinkcheck = $newAppcheck;
		
		$responseContent = $postContent;
	}
	else
	{
		my @weeklyURL = @{$weeklyURLs};
		my $weekCount = 1;
		for my $WeekUrl(@weeklyURL)
		{
			print "WeekUrl=>$WeekUrl\n"; 
			# <STDIN>;		
			print "\nweekCount=>$weekCount\n\n";
			$weekCount++;			
			
			my ($weeklyListCnt,$responseCode) = &getMechContent($WeekUrl,$receivedCouncilApp,$receivedCouncilCode);
			
			# open(TIME,">fileName.html");
			# print TIME "$weeklyListCnt\n";
			# close TIME;
			# exit;
	
			if($receivedCouncilCode=~m/^103$/is)
			{	
				next if($weeklyListCnt=~m/Red\;\">\s*No\s*records\s*found/is);
			}
			elsif($receivedCouncilCode=~m/^133$/is)
			{
				next if($weeklyListCnt=~m/\{\"SearchResults\"\:\[\]\,\"ResultCount\"\:0\}/is);
			}
			my ($totalAppURL, $redisDup, $totalCount, $newAppcheck) = &collectApplicationDetails($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $weeklyListCnt,$receivedCouncilApp);
			
			$responseContent .= $weeklyListCnt;
			
			if($newAppcheck=~m/^Yes$/is)
			{	
				$newAppLinkcheck = "Yes";
				$dumFlagCheck = "1";
			}
			else
			{
				$newAppLinkcheck = "No";
			}
			
			
			push(@appURLs, @{$totalAppURL});
			$appLinkDup .= $redisDup;
			$appLinkCount = $totalCount + $appLinkCount;	
		}
		
		if($dumFlagCheck=~m/^1$/is)
		{	
			$newAppLinkcheck = "Yes";
		}
	}
		
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$responseContent,$searchLogsDirectoryPath,$category,'Success');	
		
	return(\@appURLs, $appLinkDup, $appLinkCount, $newAppLinkcheck);
}


sub collectApplicationDetails
{
	my ($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent, $receivedCouncilApp) = @_;
	
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my (@appLinks, $deDup_Query);
	my $pageNumber=1;
	my $actualSearchCount=0;
	my ($newAppLinkcheck,$dumFlagCheck);
	
	
	
	NextPage:
	while($postContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'APPLICATION_REGEX'}/igs)
	{
		my $firstMatch=$1;
		my $secondMatch=$2;
				
		my ($applicationURL,$applicationReference);
		
		if($receivedCouncilCode=~m/^2029$/is)
		{
			# my $AppURLDomin = $commonFileDetails->{$receivedCouncilCode}->{'APPLICATION_URL_DOMAIN'};
			$applicationURL = $secondMatch;		
			$applicationReference = $firstMatch;		
			$applicationURL="https://dms.wexfordcoco.ie/fsdocumentsx.php?q=$firstMatch&recid=0" if($applicationURL=~m/^\s*$/is);
		}
		else
		{
			$applicationURL = $firstMatch;
			$applicationReference = $secondMatch;
		}
		
		
		if($applicationURL!~m/^https?/is)
		{
			$applicationURL = URI::URL->new($applicationURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		}
		
		$applicationURL=~s/\&amp\;/\&/gsi;
			
		
		if($applicationReference!~m/^\s*$/is)
		{
			my $checkRedis;
			if($category eq "Planning")
			{
				$checkRedis = $receivedCouncilCode."_".$applicationReference;
			}
			elsif($category eq "Decision")
			{
				$checkRedis = "D_".$receivedCouncilCode."_".$applicationReference;				
			}
			
			if($redis->exists($checkRedis))	
			{
				print "Exists : $applicationReference\n";
		
				$actualSearchCount++;
				my $deDupQuery="($scheduleNo, \'$scheduleDate\', $receivedCouncilCode, \'$applicationReference\', \'$applicationURL\', \'$category\', \'$source\', \'$councilName\'),";
				$deDup_Query.=$deDupQuery;
				
				$newAppLinkcheck = "No";
				
				next;
			}
			else
			{	
				$actualSearchCount++;
				$dumFlagCheck = "1";
				$newAppLinkcheck = "Yes";
				
				push ( @appLinks, $applicationURL);	
			}
		}
	}	
	
		
	if($dumFlagCheck=~m/^1$/is)
	{	
		$newAppLinkcheck = "Yes";
	}
	
	# print "newAppLinkcheck::$newAppLinkcheck\n";
	print "Page Number::$pageNumber\n";
		
	if($postContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}/is)
	{
		my $nextPageURL=$1;
		
		if($nextPageURL eq "")
		{
			goto NEXTEND;
		}
		
		
		$nextPageURL=~s/\s*\&\#xD\;\&\#xA\;\s*//gsi;
		
		
		if($nextPageURL!~m/^https?/is)
		{
			$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		}
		
		$nextPageURL=~s/\&amp\;/\&/gsi;
		# print "nextPageURL=>$nextPageURL\n"; 
		# <STDIN>;
		
		if($nextPageURL ne "")
		{
			my ($weeklyListCnt,$responseCode) = &getMechContent($nextPageURL,$receivedCouncilApp,$receivedCouncilCode);
			$postContent = $weeklyListCnt;
			
			$pageNumber++;			
			# print "pageNumber:::$pageNumber\n\n";
			# print "receivedCouncilCode:::$receivedCouncilCode\n\n";
			my $count478 = 51;
			if(($count478 > $pageNumber) && ($receivedCouncilCode=~m/^2025$/is))
			{
				if($pageNumber==51)
				{
					goto NEXTEND;	
				}
				else
				{
					goto NextPage;	
				}
			}
			elsif($receivedCouncilCode!~m/^2025$/is)
			{
				goto NextPage;	
			}
		}
		
	}
	
	NEXTEND:
				
	print "$newAppLinkcheck\n"; 
	# <STDIN>;	
	
	return (\@appLinks, $deDup_Query, $actualSearchCount, $newAppLinkcheck);
			
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	
	
	print "receivedApplicationUrl==>$receivedApplicationUrl\n";
	
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	
	# print "applicationDetailsPageContent:::$applicationDetailsPageContent\n\n";
	# open(TIME,">fileName.html");
	# print TIME "$applicationDetailsPageContent\n";
	# close TIME;
	# exit;
	
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
	else
	{
		$applicationNumber = $1 if($receivedApplicationUrl=~m/Ref(?:Val)?\=([^<]*?)$/is);
	}
	
	print "Current Application number::$applicationNumber\n";
	# print "applicationDetailsPageResponse::$applicationDetailsPageResponse\n";
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		if($receivedApplicationUrl=~m/Ref(?:Val)?\=([^<]*?)$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/articleid=([^<]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/frmId\=([^<]*?)$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/APASID\=([^<]*?)\#planapps$/is)
		{
			$logName = $1;
		}
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}

####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\\r\\n/ /igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	
	return($data2Clean);
}
1;