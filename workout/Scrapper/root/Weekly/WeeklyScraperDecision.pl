#!/usr/bin/perl
use strict;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Basename;
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use URI::Escape;
use URI::URL;
use RedisDB;


############
# How to Run
#
# perl WeeklyScraperDecision.pl "3" "Weekly_Decision" "2018/06/11T11:30:00" "2018/06/11T11:30:00"
#
# For ONDEMND#
#
# perl WeeklyScraperDecision.pl 1 Weekly_Decision "2018/07/16T10:53:00" "2018/07/16T10:53:00" onDemand
#
##############

####
# Declare global variables
####

my $councilCode = $ARGV[0];
my $category = $ARGV[1];
my $startDateTime = $ARGV[2];
my $scheduleDateTime = $ARGV[3];
my $schedule = $ARGV[4];
my $onDemandID = $ARGV[5];
my $onDemand = $ARGV[6];

$startDateTime=~s/T/ /s;
$scheduleDateTime=~s/T/ /s;
print "\n===========================================================================================================\n";
print "\tCouncil Code::$councilCode\tCategory::$category\tScriptStartDate::$startDateTime\tScheduleDateTime::$scheduleDateTime\n";
print "===========================================================================================================\n";

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');

print "ScheduleDate==>$scheduleDate\n";
# <STDIN>;

my ($fromDate, $toDate, $rangeName,$Schedule_no,$categoryType);
my ($searchResultCount, $responseContents, $responseStatus);
my ($divCounter, $scheduleCounter);
my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n";

if($category=~m/^Weekly_Decision$/is)
{
	$categoryType="Decision";
}

####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/etc/Weekly');
my $libControlDirectory = ($basePath.'/lib/Control');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $logsDataDirectory = ($basePath.'/data');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');


require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($libControlDirectory.'/WeeklyCouncilsController.pm'); # Private Module
require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Redis_connection_Decision
####
my $redisConnection = &Glenigan_DB::Redis_connection_Decision();


####
# Create new object of class Config::Tiny
####

my ($regexFile,$councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils, control properties for date range clculation and list of all councils repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Weekly_Council_Decision_Details.ini');
$regexFile = Config::Tiny->read($ConfigDirectory.'/Weekly_Regex_Heap_Decision.ini' );

####
# Get Schedule number based on Forenoon or Afternoon
####
$Schedule_no=&CouncilsScrapperCore::scheduleNumber(localtime());

####
# Get Council Name From INI file
####
my $councilName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
my $weeklyListURLs = $councilDetailsFile->{$councilCode}->{'URL'};



if(($onDemand eq '') or ($onDemand=~m/^\s*$/is))
{
	if( !$councilCode || !$category || !$startDateTime || !$scheduleDateTime) 
	{
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Input Argument Missing',$categoryType,$councilName);
		exit;
	}
}


####
# Get class name for Mechanize method
####
my ($councilApp,$pingStatus,$paginationContent) = &WeeklyCouncilsController::setVerficationSSL($councilCode,$categoryType,'Weekly','20',$scheduleDate,$Schedule_no,$councilName);


####
# Get file name to store search page content in log folder
####	
my $rawFileName;
if($onDemand ne "")
{
	$rawFileName=&WeeklyCouncilsController::rawFileName($startDateTime,$councilCode,'ONDEMAND001',$categoryType);
}
else
{
	$rawFileName=&WeeklyCouncilsController::rawFileName($startDateTime,$councilCode,'GCS001',$categoryType);
}


if($pingStatus!~m/^\s*(200|Ok)\s*$/is)
{
	&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Terminated',$categoryType,$councilName);	
	
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContent,$searchLogsDirectoryPath,$categoryType,'Failed');	
	
	my $dt = DateTime->now;
	my $FromDate = $dt->ymd('-');
	my $ToDate = $FromDate;
		
	if($onDemand ne "")
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
		
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
		
		my $onDemandStatus;
		if($pingStatus > 500)
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		elsif( $pingStatus >= 400 and $pingStatus <= 499 )
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		else
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		
		&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
	}
	else
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
	}
	
	exit;
}


####
# Beginining of script execution
####

&main();



sub main
{

	print "\n#####################################################################\n";
	print "CouncilCode:$councilCode\tCouncilName:$councilName\tCategory::$categoryType";
	print "\n#####################################################################\n";

	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandUpdate($councilCode,$categoryType,$onDemandID);
	}
	
	my ($weeklyListCnt,$responseCode) = &WeeklyCouncilsController::getMechContent($weeklyListURLs,$councilApp,$councilCode);
	
	my $councilType = $councilDetailsFile->{$councilCode}->{'TYPE'};
	my $WEEKLY_LIST_REGEX = $councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_REGEX'};
	
	my @WeeklyListUrl;
	my ($councilAppResponse, $pingResponseStatus);
	if($councilType=~m/^GET$/is)
	{
		my $URLContent = $1 if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_CONTENT_REGEX'}/is);
					
		while($URLContent=~m/$WEEKLY_LIST_REGEX/igs)
		{
			my $weeklyURL=$1;
			my $WeeklyDateRange=$2;
		
			my $weeklyListURL;
			if($councilCode=~m/^(133|461|482)$/is)
			{
				my $weeklyURLDomin = $councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_DOMAIN'};
				$weeklyURLDomin=~s/<DateRange>/$weeklyURL/si;
				
				$weeklyListURL = $weeklyURLDomin;
			}
			elsif($weeklyURL!~m/^https?/is)
			{
				$weeklyURL = URI::URL->new($weeklyURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				
				$weeklyListURL = $weeklyURL;
			}
			else
			{
				$weeklyListURL = $weeklyURL;
			}
			
			$weeklyListURL=~s/\&amp\;/\&/gsi;
			
			# print "$weeklyListURL\n"; <STDIN>;
			
			push(@WeeklyListUrl,$weeklyListURL);	
			
			
		}
		
		$pingResponseStatus=$responseCode;
	}
	elsif($councilType=~m/^POST$/is)
	{
		
		my ($ViewState,$ViewstateGenerator,$EventValidation);
		if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_VIEWSTATE_REGEX'}/is)
		{
			$ViewState=uri_escape($2);
		}
		if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_VIEWSTATEGENERATOR'}/is)
		{
			$ViewstateGenerator=uri_escape($2);
		}
		if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_EVENTVALIDATION'}/is)
		{
			$EventValidation=uri_escape($2);
		}
		
		my $postContent = $councilDetailsFile->{$councilCode}->{'SEARCH_POST_CONTENT'};
		my $year = $dt->year();
		my $month = $dt->month();
		
		$postContent=~s/<Year>/$year/is;
		$postContent=~s/<Month>/$month/is;
		
		$postContent=~s/<VIEWSTATE>/$ViewState/is;
		$postContent=~s/<VIEWSTATEGENERATOR>/$ViewstateGenerator/is;
		$postContent=~s/<EVENTVALIDATION>/$EventValidation/is;
				
				
		$councilAppResponse = $councilApp->post($councilDetailsFile->{$councilCode}->{'SEARCH_URL'}, Content => "$postContent");
		$councilAppResponse = $councilApp->content;
		$pingResponseStatus = $councilApp->status;
			
		# open(TIME,">fileName.html");
		# print TIME "$councilAppResponse\n";
		# close TIME;
		# exit;
	}
	elsif($councilType=~m/^NORMAL$/is)
	{				
		my $searchURL = $councilDetailsFile->{$councilCode}->{'SEARCH_URL'};		
		my $year = $dt->year();
		
		$searchURL=~s/<Year>/$year/is;
		
		$councilAppResponse = $councilApp->get($searchURL);
		$councilAppResponse = $councilApp->content;
		$pingResponseStatus = $councilApp->status;
			
		# open(TIME,">fileName.html");
		# print TIME "$councilAppResponse\n";
		# close TIME;
		# exit;
	}
	elsif($councilType=~m/^POSTDATE$/is)
	{
		my $postContent = $councilDetailsFile->{$councilCode}->{'SEARCH_POST_CONTENT'};		
			
		my $ToDate = $dt->dmy('/');
		
		my $FromdayDate;
		if($councilCode=~m/^607$/is)
		{
			$FromdayDate = $dt - DateTime::Duration->new( days => 182 );
		}
		else
		{
			$FromdayDate = $dt - DateTime::Duration->new( days => 7 );
		}
		
		my $FromDate = $FromdayDate->dmy('/');
		
		# print "Fromdate=>$FromDate\tTodate=>$ToDate\n"; 
		if($councilCode=~m/^353$/is)
		{
			$FromDate=~s/^([\d]+)\/([\d]+)\/([\d]+)$/$3-$2-$1/gsi;
		}
		
		$FromDate=~s/\//%2F/gsi;
		$ToDate=~s/\//%2F/gsi;
		
		my $fwuid=$1 if($weeklyListCnt=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
		my $loaded=$1 if($weeklyListCnt=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
		
		$fwuid=~s/\\\"//igs;
		$fwuid=~s/\///igs;
		$fwuid=~s/\\//igs;
		
		$loaded=~s/\\\"//igs;
		$loaded=~s/\///igs;
		$loaded=~s/\\//igs;		
		
		$postContent=~s/<FWUID>/$fwuid/si;
		$postContent=~s/<LOADED>/$loaded/si;
		
		$postContent=~s/<FromDate>/$FromDate/is;
		$postContent=~s/<ToDate>/$ToDate/is;
		
		
		$councilAppResponse = $councilApp->post($councilDetailsFile->{$councilCode}->{'SEARCH_URL'}, Content => "$postContent");
		my $councilAppCnt = $councilApp->content;
		my $councilAppStatus = $councilApp->status;
		
		$councilAppResponse = $councilAppCnt;
			
		# open(TIME,">fileName.html");
		# print TIME "$councilAppResponse\n";
		# close TIME;
		# exit;
		
		
		$pingResponseStatus=$councilAppStatus;
	}
	elsif($councilType=~m/^DATE$/is)
	{
		my $searchURL = $councilDetailsFile->{$councilCode}->{'SEARCH_URL'};		
			
		my $ToDate = $dt->dmy('/');
		
		my $FromdayDate = $dt - DateTime::Duration->new( days => 7 );
		
		my $FromDate = $FromdayDate->dmy('/');
		
		print "Fromdate=>$FromDate\tTodate=>$ToDate\n"; 
		# <STDIN>;
		
		$FromDate=~s/\//%2F/gsi;
		$ToDate=~s/\//%2F/gsi;
		
		$searchURL=~s/<FromDate>/$FromDate/is;
		$searchURL=~s/<ToDate>/$ToDate/is;
		
		# print "searchURL=>$searchURL\n"; <STDIN>;
		
		my $councilAppCnt = $councilApp->get($searchURL);
		my $councilAppCnt = $councilApp->content;
		my $councilAppStatus = $councilApp->status;
		
		$councilAppResponse = $councilAppCnt;
			
		# open(TIME,">fileName.html");
		# print TIME "$councilAppResponse\n";
		# close TIME;
		# exit;
		
		
		$pingResponseStatus=$councilAppStatus;
	}
	elsif($councilType=~m/^N\/A$/is)
	{
		my $searchURL = $councilDetailsFile->{$councilCode}->{'SEARCH_URL'};		
		
		my $councilAppCnt = $councilApp->get($searchURL);
		my $councilAppCnt = $councilApp->content;
		my $councilAppStatus = $councilApp->status;
		
		$councilAppResponse = $councilAppCnt;
	}
	
	my ($appURLs, $appLinkDup, $actualSearchCount, $newAppLinkcheck);
	if($onDemand ne "")
	{
		($appURLs, $appLinkDup, $actualSearchCount, $newAppLinkcheck) = &WeeklyCouncilsController::searchNextWeeklyRange($councilCode, $councilApp, $categoryType, \@WeeklyListUrl, 'ONDEMAND001', $councilName, $Schedule_no, $scheduleDateTime, $redisConnection, $councilAppResponse, $rawFileName);
	}
	else
	{	
		($appURLs, $appLinkDup, $actualSearchCount, $newAppLinkcheck) = &WeeklyCouncilsController::searchNextWeeklyRange($councilCode, $councilApp, $categoryType, \@WeeklyListUrl, 'GCS001', $councilName, $Schedule_no, $scheduleDateTime, $redisConnection, $councilAppResponse, $rawFileName);
	}
	
	my @appLinks = @{$appURLs};
	
		
	###
	# Insert deDup details
	###
	if($appLinkDup ne "")
	{
		&CouncilsScrapperCore::deDupInsertion($appLinkDup);
	}
	
	
	if($newAppLinkcheck!~m/^Yes$/is)
	{
		# print "newAppLinkcheck=>$newAppLinkcheck\n"; <STDIN>;
		
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
		
		print "No new Application found\n";
		
		print "Total Application Links Count for ths weekly List range is \:\t$actualSearchCount\n";
			
		print "But actual Scrapped Count is\:\t0\n";		
			
		my $dt = DateTime->now;
		my $FromDate = $dt->ymd('-');
		
		my $ToDate = $FromDate;
			
		###
		# Insert actual scrapped details for Dashboard
		###
		if($onDemand ne "")
		{
			&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, $actualSearchCount, '0', '200', $rawFileName, $scheduleDate);
		}
		else
		{
			&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, $actualSearchCount, '0', '200', $rawFileName, $scheduleDate);
		}
		
		
		###
		# Insert Scrape Status in Database
		###
		if($onDemand ne "")
		{
			&CouncilsScrapperCore::writtingToLog('End','ONDEMAND001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		}
		else
		{
			&CouncilsScrapperCore::writtingToLog('End','GCS001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		}
	}
	else
	{		
		# print "actualSearchCount==>$actualSearchCount\n"; <STDIN>;
		# print "pingResponseStatus==>$pingResponseStatus\n"; <STDIN>;
			
		
		###
		# Insert input date range ping response into DB 
		###
		if($pingResponseStatus!~m/^\s*(?:200|ok|\s*)\s*$/is) # Only If Fails
		{
			if($onDemand ne "")
			{
				&CouncilsScrapperCore::writtingToStatus('ONDEMAND001',$councilCode,$councilName,$pingResponseStatus,$logsDirectory,$categoryType);
				
		
				&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
				
				my $onDemandStatus;
				if($pingResponseStatus > 500)
				{
					$onDemandStatus = "Site not Working. Response code: $pingResponseStatus";
				}
				elsif( $pingResponseStatus >= 400 and $pingResponseStatus <= 499 )
				{
					$onDemandStatus = "Site not Working. Response code: $pingResponseStatus";
				}
				else
				{
					$onDemandStatus = "Site not Working. Response code: $pingResponseStatus";
				}
				
				&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
			}
			else
			{
				&CouncilsScrapperCore::writtingToStatus('GCS001',$councilCode,$councilName,$pingResponseStatus,$logsDirectory,$categoryType);
			}

			###
			# Update input date range search terminated response count into TBL_SCRAP_STATUS 
			###
			&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Terminated',$categoryType,$councilName);		
		}
		else
		{
			my ($insertQuery, $actualScrappedCount);
			if($onDemand ne "")
			{
				($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@appLinks,'ONDEMAND001');
			}
			else
			{
				($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@appLinks,'GCS001');
			}
			
			
			###
			# Insert application details in ScreenScrapper Database for Decision
			###
			&CouncilsScrapperCore::scrapeDetailsInsertion($insertQuery,$categoryType);
			# <STDIN>;		
				
			print "Total Application Links Count for ths weekly List range is \:\t$actualSearchCount\n";
			
			print "But actual Scrapped Count is\:\t $actualScrappedCount\n";
			
			# print "actualScrappedCount==>$actualScrappedCount\n"; <STDIN>;
			
			if($actualScrappedCount>=1)
			{	
				###
				# Update input date range search success response count into TBL_SCRAP_STATUS 
				###
				
				&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
				
				
				my $dt = DateTime->now;
				my $FromDate = $dt->ymd('-');
				
				my $ToDate = $FromDate;
					
				###
				# Insert actual scrapped details for Dashboard
				###
				if($onDemand ne "")
				{
					&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, $actualSearchCount, $actualScrappedCount, '200', $rawFileName, $scheduleDate);
				}
				else
				{
					&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, $actualSearchCount, $actualScrappedCount, '200', $rawFileName, $scheduleDate);
				}
			}
			
			####
			# Insert Scrape Status in onDemand Database
			####
			if($onDemand eq 'onDemand')
			{
				my $onDemandStatus;
				if($actualSearchCount ne "")
				{
					if(($actualSearchCount=~m/^0$/is) or ($actualSearchCount=~m/^\s*$/is))
					{
						$onDemandStatus = "No Application found";
					}
					elsif(($actualScrappedCount=~m/^0$/is) or ($actualScrappedCount=~m/^\s*$/is))
					{
						$onDemandStatus = "No new application found";
					}
					else
					{
						$onDemandStatus = "\"$actualSearchCount\" Application found. But Valid application count: \"$actualScrappedCount\"";
					}
				}
				&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
			}	
			
			
			###
			# Insert Scrape Status in Database
			###
			if($onDemand ne "")
			{
				&CouncilsScrapperCore::writtingToLog('End','ONDEMAND001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
			}
			else
			{
				&CouncilsScrapperCore::writtingToLog('End','GCS001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
			}
			
			# return(\@appLinks, $deDup_Query);
		}
	}
	
	
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
	}
}



####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationDetails()
{
    my $passedApplicationLink = shift;
    my $rangeName = shift;
	
	my @passedApplicationLinks = @{$passedApplicationLink};


	my $insertQuery;
	
	print "GCS==>$rangeName\n";
	
	my $resultsCount=0;
	my ($fullAppPageLink);
    foreach my $eachApplicationLink (@passedApplicationLinks)
    {		
		$fullAppPageLink=$eachApplicationLink;
		# print "Going to fetch ==> $fullAppPageLink\n"; <STDIN>;
		my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &WeeklyCouncilsController::getApplicationDetails($fullAppPageLink, $councilApp, $councilCode, $categoryType);
		
		if($councilCode=~m/^376$/is)
		{
			$applicationNumber=~s/^\s*[^<]*?\s*\,\s*([^<]*?)\s*$/$1/is;
		}
		elsif($councilCode=~m/^478$/is)
		{	
			$applicationNumber=~s/^\s*([^<]*?)\s*\-/$1/is;
		}
		
		if(($applicationNumber ne "") && ($pageResponse=~m/^\s*(200|ok)\s*$/is))
		{
			$resultsCount++;
			
			## Insert new application in redis server			
			$redisConnection->set("D_".$councilCode."_".$applicationNumber => $applicationNumber);
			print "Not Exists : $applicationNumber\n";
		}
		
		
		my $tempSource;
		if($onDemand eq 'onDemand')
		{
			$tempSource = "onDemand_".$rangeName;
		}
		else
		{
			$tempSource = $rangeName;
		}
		
		if($applicationNumber ne "")
		{
			my ($insert_query_From_Func) = &WeeklyCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$fullAppPageLink,$tempSource,$councilCode,$councilName,$scheduleDate,$scheduleDateTime,$categoryType, $councilApp);
			
			$insertQuery.=$insert_query_From_Func;
		}
    }
	
	# print "$resultsCount\n"; 
		
	return($insertQuery, $resultsCount);
}