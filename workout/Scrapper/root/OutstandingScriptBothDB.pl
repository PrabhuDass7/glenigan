use strict;
use WWW::Mechanize;
use URI::URL;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use Config::Tiny;
use URI::Escape;
use DateTime;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Spreadsheet::ParseExcel;
use Spreadsheet::ParseXLSX;
use Spreadsheet::XLSX;
use Parallel::ForkManager;
use File::Path qw(make_path);

######### Get Directory path Local ############

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
# print "$basePath\n"; <STDIN>;


######### Get Current Local date ############
my $dt = DateTime->now();
my $todayDate = $dt->dmy('');
my $inputDate = $dt->dmy('_');

print "todayDate==>$todayDate\n";
print "inputDate==>$inputDate\n";

my $categoryType = $ARGV[0];

if($categoryType eq "")
{
	print "\nCouncil category type is missing!!!\n";
	exit;
}

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $onlineConfigDirectory = ($ConfigDirectory.'/Online');
my $apacheConfigDirectory = ($ConfigDirectory.'/Apache');
my $eplanningConfigDirectory = ($ConfigDirectory.'/ePlanning');
my $fastwebConfigDirectory = ($ConfigDirectory.'/Fastweb');
my $northgateConfigDirectory = ($ConfigDirectory.'/Northgate');
my $ocellaConfigDirectory = ($ConfigDirectory.'/Ocella');
my $onepageConfigDirectory = ($ConfigDirectory.'/OnePage');
my $servletConfigDirectory = ($ConfigDirectory.'/Servlet');
my $simpleConfigDirectory = ($ConfigDirectory.'/Simple');
my $toughConfigDirectory = ($ConfigDirectory.'/Tough');
my $indiConfigDirectory = ($ConfigDirectory.'/Indi');
my $weeklyConfigDirectory = ($ConfigDirectory.'/Weekly');
my $wphConfigDirectory = ($ConfigDirectory.'/WPH');
my $logsDirectory = ($basePath.'/logs');
my $dataDirectory = ($basePath.'/data');
my $coreLibDirectory = ($basePath.'/lib/Core');
my $controlLibDirectory = ($basePath.'/lib/Control');

require ($coreLibDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($coreLibDirectory.'/Glenigan_DB.pm'); # Private Module
require ($controlLibDirectory.'/ApacheCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/ePlanningCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/FastwebCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/NorthgateCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/OcellaCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/OnePageCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/OnlineCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/ServletCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/SimpleCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/ToughCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/WeeklyCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/WPHCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/IndiCouncilsController.pm'); # Private Module


####
# Create new object of class Redis_connection_Decision
####
my $dbh = &Glenigan_DB::gleniganDB();


my %councilNames =  ( 120 => 'Bournemouth',158 => 'Christchurch',247 => 'Redcar & Cleveland',261 => 'Malvern Hills',316 => 'Purbeck',168 => 'Crawley',369 => 'South Northamptonshire',425 => 'Wealden',428 => 'Welwyn Hatfield',439 => 'East Dorset',451 => 'Wychavon',480 => 'Northumberland National Park',608 => 'Devon County Council',612 => 'Essex County Council',614 => 'Hampshire County Council',620 => 'Norfolk County Council',621 => 'North Yorkshire County Council',623 => 'Nottinghamshire County Council',244 => 'Kirklees',384 => 'Stratford-On-Avon',249 => 'Leicester',302 => 'Nuneaton & Bedworth',304 => 'Bridgend',315 => 'Preston',394 => 'Tandridge',413 => 'Vale Of Glamorgan',606 => 'Cumbria County Council',617 => 'Lancashire County Council',616 => 'Kent County Council',628 => 'Surrey County Council',477 => 'Yorkshire Dales National Park',492 => 'Central Bedfordshire',618 => 'Leicestershire County Council',619 => 'Lincolnshire County Council',625 => 'Somerset County Council',627 => 'Suffolk County Council',1 => 'Croydon',2 => 'Bromley',3 => 'Barnet',4 => 'Ealing',6 => 'Lambeth',7 => 'Enfield',8 => 'Brent',10 => 'Lewisham',13 => 'Newham',15 => 'Southwark',17 => 'Bexley',18 => 'Westminster',19 => 'Greenwich',25 => 'Sutton',28 => 'Hammersmith & Fulham',29 => 'Barking & Dagenham',30 => 'Tower Hamlets',33 => 'City of London',35 => 'Aberdeen',36 => 'Angus',38 => 'Argyll & Bute',40 => 'Aberdeenshire',41 => 'East Dunbarton',42 => 'Scottish Borders',44 => 'Clackmannanshire',46 => 'South Lanarkshire',48 => 'East Ayrshire',49 => 'North Ayrshire',51 => 'Dundee',52 => 'Fife',54 => 'East Lothian',55 => 'East Renfrewshire',56 => 'Edinburgh',58 => 'Falkirk',59 => 'Glasgow',62 => 'Inverclyde',63 => 'Highland',67 => 'South Ayrshire',70 => 'Midlothian',72 => 'Moray',73 => 'North Lanarkshire',75 => 'Dumfries & Galloway',77 => 'Perth & Kinross',78 => 'Renfrewshire',83 => 'Stirling',87 => 'West Lothian',100 => 'Aylesbury Vale',104 => 'Basildon',105 => 'Basingstoke & Deane',106 => 'Bassetlaw',111 => 'Blaby',113 => 'Blackpool',116 => 'Bolsover',117 => 'Bolton',121 => 'Bracknell Forest',122 => 'Bradford',123 => 'Braintree',126 => 'Brentwood',128 => 'Brighton & Hove',129 => 'Bristol',131 => 'Bromsgrove',135 => 'Bury',136 => 'Calderdale',137 => 'Cambridge',139 => 'Canterbury',141 => 'Cardiff',142 => 'Carlisle',146 => 'Castle Point',149 => 'Chelmsford',150 => 'Cheltenham',151 => 'Cherwell',153 => 'Chesterfield',155 => 'Chichester',156 => 'Chiltern',157 => 'Chorley',159 => 'North East Lincolnshire',164 => 'Corby',165 => 'Cotswold',167 => 'Craven',171 => 'Dacorum',172 => 'Darlington',173 => 'Dartford',176 => 'Derby',179 => 'Doncaster',180 => 'Dover',186 => 'East Cambridgeshire',187 => 'East Devon',188 => 'East Hampshire',189 => 'East Hertfordshire',191 => 'East Lindsey',192 => 'East Northamptonshire',194 => 'East Riding of Yorkshire',199 => 'Epsom & Ewell',201 => 'Exeter',203 => 'Fenland',205 => 'Forest Of Dean',206 => 'Fylde',207 => 'Gateshead',208 => 'Gedling',211 => 'Gloucester',213 => 'Gosport',214 => 'Gravesham',217 => 'Guildford',219 => 'Hambleton',220 => 'Harborough',221 => 'Harlow',222 => 'Harrogate',223 => 'Hart',225 => 'Hastings',226 => 'Havant',228 => 'Hertsmere',230 => 'Hinckley & Bosworth',232 => 'Horsham',234 => 'Huntingdonshire',237 => 'Caerphilly',241 => 'Kings Lynn & West Norfolk',242 => 'Hull City Council',245 => 'Knowsley',246 => 'Lancaster',248 => 'Leeds',251 => 'Lewes',252 => 'Lichfield',253 => 'Lincoln',257 => 'Luton',259 => 'Maidstone',260 => 'Maldon',262 => 'Manchester',263 => 'Mansfield',266 => 'Melton',267 => 'Mendip',268 => 'Merthyr Tydfil',270 => 'Mid Devon',271 => 'Middlesbrough',272 => 'Mid Suffolk',273 => 'Mid Sussex',274 => 'Milton Keynes',276 => 'Monmouthshire',279 => 'Newark & Sherwood',280 => 'West Berkshire',281 => 'Newcastle-Under-Lyme',282 => 'Newcastle-Upon-Tyne',283 => 'New Forest District Council',286 => 'South Gloucestershire',287 => 'Bedford',290 => 'North Dorset',291 => 'North East Derbyshire',292 => 'North Hertfordshire',293 => 'North Kesteven',294 => 'North Norfolk',296 => 'North Tyneside',298 => 'North West Leicestershire',300 => 'Norwich',301 => 'Nottingham',303 => 'Oadby & Wigston',305 => 'Oldham',307 => 'Oxford',308 => 'Pendle',310 => 'Peterborough',311 => 'Plymouth',312 => 'Poole',313 => 'Portsmouth',317 => 'Powys',320 => 'Reigate & Banstead',322 => 'Rhondda Cynon Taff',326 => 'Richmondshire',327 => 'Rochdale',328 => 'Medway',330 => 'Rossendale',335 => 'Rushcliffe',336 => 'Rushmoor',337 => 'Rutland',338 => 'Ryedale',341 => 'St. Helens',342 => 'Salford',344 => 'Sandwell',345 => 'Scarborough',349 => 'Sefton',350 => 'Selby',351 => 'Sevenoaks',352 => 'Sheffield',353 => 'Folkstone & Hythe',356 => 'Solihull',357 => 'Southampton',362 => 'Southend-On-Sea',368 => 'South Norfolk',372 => 'South Ribble',374 => 'South Staffordshire',378 => 'Stafford',380 => 'Stevenage',381 => 'Stockport',382 => 'Stockton-On-Tees',383 => 'Stoke-On-Trent',385 => 'Stroud',387 => 'Sunderland',390 => 'Swansea',391 => 'Swindon',398 => 'Tendring',399 => 'Test Valley',400 => 'Tewkesbury',402 => 'Thanet',403 => 'Three Rivers',404 => 'Thurrock',405 => 'Tonbridge & Malling',406 => 'Torbay',407 => 'Torfaen',408 => 'Torridge',409 => 'Trafford',410 => 'Tunbridge Wells',412 => 'Uttlesford',416 => 'Wakefield',421 => 'Warwick',422 => 'Watford',427 => 'Wellingborough',429 => 'Derbyshire Dales',432 => 'West Lancashire',434 => 'West Oxfordshire',435 => 'West Somerset',440 => 'Winchester',441 => 'Windsor & Maidenhead',442 => 'Wirral',443 => 'Woking',445 => 'Wolverhampton',446 => 'North Somerset',452 => 'Wycombe',453 => 'Wyre',457 => 'York',458 => 'Orkney Isles',459 => 'Shetland Isles',460 => 'Western Isles',476 => 'Brecon Beacons National Park',481 => 'Broads National Park',483 => 'Loch Lomond National Park',484 => 'New Forest National Park',485 => 'Shropshire',487 => 'Cheshire West & Chester',488 => 'Northumberland County Council',490 => 'Durham County Council',491 => 'Cornwall',500 => 'Ards',531 => 'West Suffolk',532 => 'East Suffolk',533 => 'Adur & Worthing',602 => 'Ebbsfleet Development Corporation',604 => 'Buckinghamshire County Council',613 => 'Gloucestershire County Council',632 => 'South Downs NP',633 => 'Cairngorms National Park',16 => 'Waltham Forest',20 => 'Hounslow',27 => 'Richmond-Upon-Thames',31 => 'Kensington & Chelsea',32 => 'Kingston-On-Thames',50 => 'West Dunbartonshire',102 => 'Barnsley',107 => 'Bath & N E Somerset',119 => 'Boston',134 => 'Burnley',143 => 'Carmarthenshire',147 => 'Ceredigion',185 => 'Eastbourne',197 => 'Elmbridge',235 => 'Hyndburn',285 => 'Northampton',329 => 'Rochford',370 => 'South Oxfordshire',377 => 'Spelthorne',388 => 'Surrey Heath',395 => 'Taunton Deane',397 => 'Teignbridge',414 => 'Vale Of White Horse',424 => 'Waverley',433 => 'West Lindsey',437 => 'Weymouth & Portland',438 => 'Wigan',475 => 'Dartmoor National Park',477 => 'Yorkshire Dales National Park',98 => 'Ashfield',236 => 'Ipswich',363 => 'South Hams',455 => 'South Somerset',611 => 'East Sussex County Council',94 => 'Flintshire',133 => 'Broxtowe',160 => 'Colchester',227 => 'Herefordshire',240 => 'Kettering',346 => 'North Lincolnshire',366 => 'South Kesteven',376 => 'Isle of Wight',461 => 'Isle Of Man',468 => 'Isles of Scilly',474 => 'Peak District National Park',478 => 'Pembrokeshire Coast Natl Park',482 => 'Exmoor National Park',607 => 'Derbyshire County Council',609 => 'Dorset County Council',615 => 'Hertfordshire County Council',630 => 'West Sussex County Council',21 => 'Harrow',132 => 'Broxbourne',323 => 'Denbighshire',339 => 'St. Albans',450 => 'Wrexham',91 => 'Neath & Port Talbot',195 => 'Eden',289 => 'North Devon',318 => 'Reading',332 => 'Rotherham',333 => 'Rugby',367 => 'South Lakeland',444 => 'Wokingham',454 => 'Wyre Forest',14 => 'Haringey',92 => 'Allerdale',166 => 'Coventry',224 => 'Hartlepool',229 => 'High Peak',297 => 'North Warwickshire',379 => 'Staffordshire Moorlands',465 => 'Guernsey Island Dev Committee',5 => 'Wandsworth',22 => 'Hackney',23 => 'Camden',24 => 'Islington',26 => 'Merton',89 => 'Conwy County',110 => 'Birmingham',112 => 'Blackburn with Darwen',130 => 'Broadland',148 => 'Charnwood',193 => 'East Staffordshire',198 => 'Epping Forest',254 => 'Liverpool',334 => 'Runnymede',375 => 'South Tyneside',393 => 'Tamworth',470 => 'North York Moors National Park',489 => 'Wiltshire',9 => 'Havering',11 => 'Hillingdon',97 => 'Arun',124 => 'Breckland',202 => 'Fareham',216 => 'Great Yarmouth',331 => 'Rother',365 => 'South Holland',447 => 'Worcester',12 => 'Redbridge',34 => 'London Legacy Development Corporation',96 => 'Gwynedd',138 => 'Cannock Chase',174 => 'Daventry',181 => 'Dudley',275 => 'Mole Valley',284 => 'Newport',355 => 'Slough',360 => 'South Cambridgeshire',371 => 'Pembrokeshire',417 => 'Walsall',420 => 'Warrington',469 => 'Snowdonia National Park',479 => 'Lake District National Park',603 => 'Old Oak & Park Royal Development Corporation (OPDC)',605 => 'Cambridgeshire County Council',624 => 'Oxfordshire County Council',629 => 'Warwickshire County Council',631 => 'Worcestershire County Council');



####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );

						 
my $Query = "select * from GLENIGAN..tbl_outstanding_status";

my $inputCheck;
eval 
{
	$inputCheck = $dbh->selectrow_array($Query);
};
	
if ( $@ ) 
{ 
	print "ERROR: $@";
	
	open(ERR,">>$logsDirectory/Outstanding_Failed_${todayDate}.txt");
	print ERR $@."\n";
	close ERR;
}
else
{
	print "QUERY:: $Query\n";
	
	print "inputCheck==>$inputCheck\n";
	
	if($inputCheck eq 'yes')
	{
		open(ERR,">>$logsDirectory/Outstanding_logs_${todayDate}.txt");
		print ERR $inputCheck."\t".$todayDate."\n";
		close ERR;
		
		my $update_statusquery = "update tbl_outstanding_status set import_completed=\'Started\'";
		my $sth= $dbh->prepare($update_statusquery);
		$sth->execute();	
		$sth->finish();

		####
		# Get input
		####
		my $inputQuery = "select application_no,council_code from tbl_outstanding where source like \'outstanding_$inputDate%\'";
		# my $inputQuery = "select application_no,council_code from tbl_outstanding where source like \'outstanding_20_09_2019%\'";

		print "InputQuery==>$inputQuery\n";

		my $sth1;
		eval {
			$sth1= $dbh->prepare($inputQuery);
			$sth1->execute();
		};
		
		if ( $@ ) 
		{ 
			print "ERROR: $@";
			open(PP,">>$logsDirectory/Failed_selectInputQuery_$todayDate.txt");
			print PP $inputQuery."==>".$@."\n";
			close PP;
			exit;
		}

		my (@application,@conCode);
		while(my @record = $sth1->fetchrow)
		{
			push(@application,Trim($record[0]));
			push(@conCode,Trim($record[1]));
		}

		$sth1->finish();

		####
		# Create new object of class Config::Tiny
		####

		my ($councilDetailsFile, $onlineCouncil, $apacheCouncil, $eplanningCouncil, $fastwebCouncil, $northgateCouncil, $indiCouncil, $ocellaCouncil, $onepageCouncil, $servletCouncil, $simpleCouncil, $toughCouncil, $weeklyCouncil, $wphCouncil, $fastwebRegex) = Config::Tiny->new();

		####
		# Read INI files
		####
		$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Outstanding_Format.ini');
		$onlineCouncil = Config::Tiny->read($onlineConfigDirectory.'/Online_Council_Decision_Details.ini');
		$ocellaCouncil = Config::Tiny->read($ocellaConfigDirectory.'/Ocella_Council_Decision_Details.ini');
		$onepageCouncil = Config::Tiny->read($onepageConfigDirectory.'/OnePage_Council_Decision_Details.ini');
		$servletCouncil = Config::Tiny->read($servletConfigDirectory.'/Servlet_Council_Decision_Details.ini');
		$toughCouncil = Config::Tiny->read($toughConfigDirectory.'/Tough_Council_Decision_Details.ini');
		$indiCouncil = Config::Tiny->read($indiConfigDirectory.'/Indi_Council_Decision_Details.ini');
		$wphCouncil = Config::Tiny->read($wphConfigDirectory.'/WPH_Council_Decision_Details.ini');
		$fastwebRegex = Config::Tiny->read($fastwebConfigDirectory.'/Fastweb_Regex_Heap_Decision.ini' );

			
		my $count = 0;
		my $tempQuery;
		for(my $Category=0; $Category<@application; $Category++)
		{
			my $applicationNo				= $application[$Category];
			my $councilCode				= $conCode[$Category];
			my $councilName = $councilNames{$councilCode};
						
			print "applicationNo=>$applicationNo\n";
			print "councilCode=>$councilCode\n";
			print "councilName=>$councilName\n";
			

			my $cCode 			= $councilCode;
			my $appNo			= $applicationNo;
			my $cName 			= $councilName;
			
			chomp($appNo);
			chomp($cCode);
			chomp($cName);
			
					
			if($cCode=~m/^(501|502|503|504|505|506|507|508|509|510|511|512|513|514|515|516|517|518|519|520|521|522|523|524|525)$/is)
			{
				$cCode = "500";
			}
			elsif($cCode=~m/^(431)$/is)
			{
				$cCode = "437";			
			}
			elsif($cCode=~m/^(401)$/is)
			{
				$cCode = "391";			
			}
			elsif($cCode=~m/^(389)$/is)
			{
				$cCode = "259";			
			}
			elsif($cCode=~m/^(359)$/is)
			{
				$cCode = "156";			
			}
			elsif($cCode=~m/^(319)$/is)
			{
				$cCode = "131";			
			}
			elsif($cCode=~m/^(101)$/is)
			{
				$cCode = "272";			
			}
			
			my $Format = $councilDetailsFile->{'Format'}->{$cCode};
			
			print "councilCode: $cCode\n";
			print "Format: $Format\n";
			print "Application: $appNo\n";
			
			next if($Format=~m/^Indi$/is);
							
			my $tempSource = 'OUT001';				
			my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
			my $scheduleDateTime = $dt->ymd('/'). " " . $dt->hms(':');
			# if($Format=~m/^Indi/is)
			# {
			if($Format=~m/^Online$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &OnlineCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($responseContent=~m/<div\s*class=\"messagebox\">\s*([\w\W]*?)\s*<\/div>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*results\s*found/is);
				}
				elsif($searchStatus=~m/^200$/is)
				{			

					$fullAppPageLink = $1 if($responseContent=~m/<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Summary\s*<\/span>/is);
					$fullAppPageLink = URI::URL->new($fullAppPageLink)->abs( $onlineCouncil->{$cCode}->{'URL'}, 1 );
					$fullAppPageLink=&URLclean($fullAppPageLink);
					my @activeTabList = ('summary', 'details', 'dates');
					my $forks = 3;
					# print "Forking up to $forks at a time\n";

					my $parallelScrapeManager = Parallel::ForkManager->new($forks);
					my %results;
					
					my ($applicationNumber);
					my ($activeTabCnt,$pageResponse,$appNumber);
					$parallelScrapeManager->run_on_finish( sub {
						my ($pid, $exit_code, $ident, $exit_signal, $core_dump, $data_structure_reference) = @_;
						my $activeTab = $data_structure_reference->{input}; ## for active tab
						$results{$activeTab} = $data_structure_reference->{result};	## for get active tab page content
						$pageResponse = $data_structure_reference->{response};	## for get active tab page ping response
						$appNumber = $data_structure_reference->{number}; ## for get application page number
						$applicationNumber = $appNumber;
					});
					
					
					foreach my $activeTab (@activeTabList)
					{
						my $pid = $parallelScrapeManager->start and next;
						# print "New started :$pid\n"; 
						
						my $applicationLinkUri = URI::URL->new($fullAppPageLink)->abs( $onlineCouncil->{$cCode}->{'URL'}, 1 );
						# print "applicationLinkUri :$applicationLinkUri\n"; 
						my %queryParameters = $applicationLinkUri->query_form;
			
						$applicationLinkUri->query_form( activeTab => $activeTab, keyVal => $queryParameters{keyVal} );
						
						# print "AppPageLink==>$applicationLinkUri\n"; <STDIN>;
						my ($applicationPageContent,$applicationPageContentResponse, $appNumber) = &OnlineCouncilsController::getApplicationDetails($applicationLinkUri, $mech, $activeTab, $categoryType,$cCode);
											
						print "ActiveTab:::$activeTab==>PageResponse:::$applicationPageContentResponse\n";
						
						# print "$pid Ended\n";
						
						$parallelScrapeManager->finish(0, { result => $applicationPageContent, input => $activeTab, response => $applicationPageContentResponse, number => $appNumber});
					}
					
					$parallelScrapeManager->wait_all_children;
					
					my %dataDetails;
					foreach my $activeTab(sort keys %results)
					{	
						# print "$activeTab<=>$results{$activeTab}\n";
						# <STDIN>;
						my %activeTabContents  = &OnlineCouncilsController::tabDetailsCollection($results{$activeTab}, $activeTab, $categoryType);
											
						%dataDetails = (%activeTabContents,%dataDetails);
					}

					print "applicationNumber==>$applicationNumber\n";
					
					if($applicationNumber ne "")
					{	
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &OnlineCouncilsController::applicationsDataDumpPlanning(\%dataDetails,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &OnlineCouncilsController::applicationsDataDumpDecision(\%dataDetails,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime);
						}
						
						print "$insert_query_From_Func\n"; 
						# <STDIN>;
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^WPH$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &WPHCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($responseContent=~m/<td[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>/is)
				{
					my $currentPageURL=$1;
					$currentPageURL = URI::URL->new($currentPageURL)->abs( $wphCouncil->{$cCode}->{'URL'}, 1 );
					$currentPageURL=&URLclean($currentPageURL);
					print "currentPageURL==>$currentPageURL\n"; 
					
					my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &WPHCouncilsController::getApplicationDetails($currentPageURL, $mech, $cCode, $categoryType);
					
					if($applicationNumber ne "")
					{
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &WPHCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &WPHCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####						
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^Ocella$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &OcellaCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				if($responseContent=~m/<td[^>]*?>\s*<strong>\s*<span\s*style=color\:red>\s*([^<]*?)\s*<\/span>\s*<\/strong>\s*<\/td>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*applications\s*found/is);
				}
				
				if($responseContent=~m/<td[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>/is)
				{
					my $currentPageURL=$1;
					$currentPageURL = URI::URL->new($currentPageURL)->abs( $ocellaCouncil->{$cCode}->{'URL'}, 1 );
					$currentPageURL=&URLclean($currentPageURL);
					print "currentPageURL==>$currentPageURL\n"; 
					
					my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &OcellaCouncilsController::getApplicationDetails($currentPageURL, $mech, $cCode, $categoryType);
					
					if($applicationNumber ne "")
					{
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &OcellaCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &OcellaCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^Northgate$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &NorthgateCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
			
				if($responseContent=~m/<p>\s*<span\s*id=\"lblPagePosition\s*\"[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/p>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*results\s*found/is);
				}
				my ($applicationDetailsPageContent) = &NorthgateCouncilsController::searchPageNext($cCode, $mech, $categoryType, $responseContent);
				
				my ($insert_query_From_Func);
				if($categoryType eq 'Planning')
				{
					($insert_query_From_Func) = &NorthgateCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,'OUT','2',$mech);				
				}
				elsif($categoryType eq 'Decision')
				{
					($insert_query_From_Func) = &NorthgateCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,'OUT','2',$mech);
				}
				
				
				print "$insert_query_From_Func\n"; 
				
				####
				# Insert application details in Glenigan Database for Decision
				####
				if($insert_query_From_Func!~m/^\(\'\'\,/is)
				{
					$count++;
					$tempQuery .= $insert_query_From_Func;
					&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
				}
			}
			elsif($Format=~m/^Fastweb$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &FastwebCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
							
				if($responseContent=~m/<\/h1>\s*<p[^>]*?>\s*([^<]*?)\s*<\/p>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*results\s*were\s*found/is);
				}

				my $applicationNumber;
				if($responseContent=~m/$fastwebRegex->{$cCode}->{'PLANNING_APPLICATION'}/is)
				{
					my $matchedText = $1;
					$applicationNumber = &FastwebCouncilsController::htmlTagClean($matchedText);	
				}
				
							
				if($applicationNumber ne "")
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &FastwebCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &FastwebCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}			
			}
			elsif($Format=~m/^Servlet$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &ServletCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				# open(PP,">a.html");
				# print PP "$responseContent";
				# close(PP);
										
				if($responseContent=~m/>\s*(Sorry\,\s*but\s*your\s*query\s*did\s*not\s*return\s*any\s*results)/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/Sorry\,\s*but\s*your\s*query/is);
				}

				if($responseContent=~m/<td[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>/is)
				{
					my $currentPageURL=$1;
					
					$currentPageURL = URI::URL->new($currentPageURL)->abs( $servletCouncil->{$cCode}->{'URL'}, 1 );
					$currentPageURL=&URLclean($currentPageURL);
					# print "currentPageURL==>$currentPageURL\n"; 
					# <STDIN>;
					
					my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &ServletCouncilsController::getApplicationDetails($currentPageURL, $mech, $cCode, $categoryType);
						
					if($applicationNumber ne "")
					{
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &ServletCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &ServletCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}					
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
				
			}
			elsif($Format=~m/^Apache$/is)
			{		
				my ($insert_query_From_Func, $fullAppPageLink, $searchStatus) = &ApacheCouncilsController::searchAppNumLogic($cCode, $cName, $appNo, $mech, $tempSource, $scheduleDate, $scheduleDateTime, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^ePlanning$/is)
			{	
				my ($responseContent, $fullAppPageLink, $searchStatus) = &ePlanningCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &ePlanningCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &ePlanningCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Simple$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &SimpleCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &SimpleCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &SimpleCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Weekly$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &WeeklyCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					
					if($cCode=~m/^(366)$/is)
					{				
						$insert_query_From_Func=$responseContent;
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
					else
					{
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &WeeklyCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &WeeklyCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
						}
						
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^OnePage$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &OnePageCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &OnePageCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &OnePageCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Tough$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &ToughCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &ToughCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &ToughCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						&CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Indi/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &IndiCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				open(PP, ">a.html");
				print PP $responseContent;
				close(PP);
				# exit;
			}

			if($count=~m/^500$/is)
			{
				$tempQuery=~s/\,\s*$//gsi;

				&CouncilsScrapperCore::scrapeDetailsInsertion($tempQuery,$categoryType);
				undef $tempQuery;
				
				# print "query==>$tempQuery\n";<STDIN>;
				

				$count = 0;
			}
			
		}		
			
		$tempQuery=~s/\,\s*$//gsi;

		&CouncilsScrapperCore::scrapeDetailsInsertion($tempQuery,$categoryType);
		undef $tempQuery;

		my $updateQuery = "update tbl_outstanding_status set import_completed=\'\' where import_completed=\'Started\'";

		print "updateQuery==>$updateQuery\n";

		my $sth2= $dbh->prepare($updateQuery);
		$sth2->execute();
		$sth2->finish();
	}
	elsif($inputCheck eq 'Started')
	{
		open(ERR,">>$logsDirectory/Outstanding_logs_${todayDate}.txt");
		print ERR "Already Started\t".$todayDate."\n";
		close ERR;
	}
	else
	{
		print "No input Found\n";
		open(ERR,">>$logsDirectory/Outstanding_logs_${todayDate}.txt");
		print ERR "Input not found\t".$todayDate."\n";
		close ERR;
	}
}

sub URLclean()
{
	my $url = shift;
	
	$url=~s/\&amp\;/\&/gsi;
	$url=~s/\'/\'\'/gsi;
	
	return $url;	
}


###### Trim the given text ######
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}