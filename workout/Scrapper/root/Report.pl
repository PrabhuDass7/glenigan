use strict;
# use encoding 'utf8';
use utf8;
use List::Util qw( max );
use POSIX 'strftime';
use Spreadsheet::WriteExcel;
use Spreadsheet::ParseExcel::SaveParser;
use Excel::Writer::XLSX;
use Date::Manip;
use MIME::Lite;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text");
use File::Basename;
use Cwd  qw(abs_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;

my $libCoreDirectory = ($basePath.'/lib/Core');
my $dataDirectory = ($basePath.'/data');

require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module


my $Schedule 	= $ARGV[0]; #Schedule type (12:00AM-'1' and 12:00PM-'2')
my @sp_names=('proc_daily_perl_schedule','proc_perl_error_log');
my ($workbook,$format,$date_format);

my($year,$month,$day) = Add_Delta_Days( Today(), 0 ),"\n";
if (length($day) == 1) {$day = "0$day";}
if (length($month) == 1) {$month = "0$month";}
my $Date="$day/$month/$year";
my $Date_For_FileName="$day$month$year";
print "Date: $Date\n";

my $Excel_file_name="Daily_Perl_Automation_Report_".$Date_For_FileName.".xlsx";
my $Storefile=$dataDirectory.'/Report/'.$Excel_file_name;
my $fileType='xlsx';

$workbook = Excel::Writer::XLSX->new( $Storefile );

for(my $i=0;$i<=$#sp_names;$i++)
{
	my $Sp_Name = @sp_names[$i];
	my $dbh = &Glenigan_DB::screenscrapperDB();
	my ($Dataref_table,$titles) = &Glenigan_DB::DATAGET_MultipleResultSet($dbh,$Sp_Name,$Date,$Schedule);

	my @Headers1	= @{$titles};
	my @ExcelRef	= @{$Dataref_table};
	my @Headers	= @{$titles};
	
	$format = $workbook->add_format(); # Add a format
	$format->set_font('Calibri');
	$format->set_size('11');

	$date_format = $workbook->add_format(num_format => 'dd/mm/yyyy');
	# print "Date::$date_format\n";
	$date_format->set_font('Calibri');
	$date_format->set_size('11');
	# print "Date::$date_format\n";


	# my $worksheet = $workbook->add_worksheet();
	my $Multisheet_Flg=1;
	my $Sheet_Name;
	if ($i == 0)
	{
		$Sheet_Name='Count';
	}
	else
	{
		$Sheet_Name='Status';
	}
	
	&Excel_Formation(\@ExcelRef,$workbook,$Multisheet_Flg,\@Headers,$Sheet_Name,$format,$date_format);
	
}
$workbook->close();
&Glenigan_DB::Send_Report($Date,$Excel_file_name,$Storefile);


sub Excel_Formation()
{
	my $ExcelRef1			= shift;
	my $workbookRef			= shift;
	my $Multisheet_Flg1		= shift;
	my $Titless				= shift;
	my $Sheet_Name1			= shift;
	my $format_temp			= shift;
	my $date_format			= shift;
	
	
	my @ExcelRef 	= @{$ExcelRef1};
	my @HeaderS  	= @{$Titless};
	
	
	my @sheetNames = split ('\:',$Sheet_Name1);
	
	print "Inside the Excel Formation\n";

	my $count=0;
	my $row=1;
	my $Sheet_reference;
	
	for (my $i=0;$i<=$#ExcelRef;$i++) #### Fetch_All_Array Reference
	{
		my $Data_key 	= $ExcelRef[$i];
		my $header_key	= $HeaderS[$i];

		if ($Multisheet_Flg1==1)
		{
			my $SheetNames = $sheetNames[$count];
			### Add a worksheet ####
			my $worksheet = $workbookRef->add_worksheet($SheetNames);
			if($SheetNames eq 'Count')
			{
				&ExcelWrite($Data_key,$worksheet,$row,$header_key,$format_temp,$date_format);
			}
			elsif($SheetNames eq 'Status')			
			{
				&ExcelWrite_2($Data_key,$worksheet,$row,$header_key,$format_temp,$date_format);
			}
		}
		else
		{
			my $worksheet = $workbookRef->add_worksheet();
			$row = &ExcelWrite($Data_key,$worksheet,$row,$header_key,$format_temp,$date_format);
		}
		$count++;
	}
	print "Excel Formation End\n";
}

sub ExcelWrite()
{
	my $array_Reference 	= shift;
	my $sheet_Reference		= shift;
	my $Row					= shift;
	my $titless				= shift;
	my $format_temps		= shift;
	my $date_format			= shift;
	
	print "Inside the Excel Write\n";
	my @ExcelRef1	= @{$array_Reference};
	my @HeaderS		= @{$titless};

	my $col_header=0;
	$sheet_Reference->set_column( 0, 12, 18 );
	
	my $Headerformat = $workbook->add_format(
		center_across => 1,
		bold          => 1,
		size          => 11,
		align         => 'vcenter',
	);	
	for( my $h=0; $h<=$#HeaderS; $h++)
	{
		next if($HeaderS[$h]=~m/(?:GSC|GCS)[\d]+_DATE_RANGE/is);
		$sheet_Reference->write(1, $col_header, $HeaderS[$h], $Headerformat);
		$col_header++;
	}
	$Row++;
	
	my @DateRange;
	my $DateRange_Flag=0;
	foreach my $key1 (@ExcelRef1) #### Row Reference
	{
		print "key :: $key1\n";
		
		my @ExcelRef2 = @{$key1};

		if($DateRange_Flag == 0)
		{	
			$DateRange_Flag=1;
			for(my $index=0;$index<@ExcelRef2;$index++) #### Coloumn Reference
			{
				my $key2 = $ExcelRef2[$index];
				push(@DateRange,$key2) if($key2=~m/[\d]+\/[\d]+\/[\d]+/is);
			}	
		}	
		
		my $col = 0;
		for(my $index=0;$index<@ExcelRef2;$index++) #### Coloumn Reference
		{
			my $key2 = $ExcelRef2[$index];
			next if($key2=~m/[\d]+\/[\d]+\/[\d]+/is);
			print "$index: Values :: $key2\n";
			Encode::_utf8_on($key2);
			$sheet_Reference->write($Row, $col, $key2, $format_temps);
			$col++;
		}
		$Row++;
	}
	
	my $col=1;
	for(my $index=0;$index<@DateRange;$index++) #### Coloumn Reference
	{
		my $key2 = $DateRange[$index];
		$sheet_Reference->write(0, $col, $key2, $Headerformat);
		$col=$col+2;
	}	
	undef(@DateRange);
	return $Row;
}

sub ExcelWrite_2()
{
	my $array_Reference 	= shift;
	my $sheet_Reference		= shift;
	my $Row					= shift;
	my $titless				= shift;
	my $format_temps		= shift;
	my $date_format			= shift;
	
	print "Inside the Excel Write\n";
	my @ExcelRef1	= @{$array_Reference};
	my @HeaderS		= @{$titless};

	my $col_header=0;
	$sheet_Reference->set_column( 0, 12, 24 );
	my $Headerformat = $workbook->add_format(
		center_across => 1,
		bold          => 1,
		size          => 11,
		align         => 'vcenter',
	);	
	for( my $h=0; $h<=$#HeaderS; $h++)
	{
		next if($HeaderS[$h]=~m/(?:GSC|GCS)[\d]+_DATE_RANGE/is);
		$sheet_Reference->write(1, $col_header, $HeaderS[$h], $Headerformat);
		$col_header++;
	}
	$Row++;
	
	my @DateRange;
	my $DateRange_Flag=0;
	my @date_column=(1,3,5,7,9,11);
	for(my $Col_index=0;$Col_index<@date_column;$Col_index++) #### Coloumn Reference
	{
		my $column=$date_column[$Col_index];
		for(my $index=0;$index<@ExcelRef1;$index++) #### Coloumn Reference
		{
			my @ExcelRef2 = @{$ExcelRef1[$index]};
			my $Ret_Daterange=$ExcelRef2[$column];
			if($Ret_Daterange~~@DateRange)
			{
				# print "DateRange Avail in list\n";
			}	
			else
			{
				push(@DateRange,$Ret_Daterange) if($Ret_Daterange=~m/[\d]+\/[\d]+\/[\d]+/is);
			}	
		}	
	}

	foreach my $key1 (@ExcelRef1) #### Row Reference
	{
		print "key :: $key1\n";
		
		my @ExcelRef2 = @{$key1};
		
		my $col = 0;
		for(my $index=0;$index<@ExcelRef2;$index++) #### Coloumn Reference
		{
			my $key2 = $ExcelRef2[$index];
			next if($index=~m/^(?:1|3|5|7|9|11)$/is);
			print "$index: Values :: $key2\n";
			Encode::_utf8_on($key2);
			$sheet_Reference->write($Row, $col, $key2, $format_temps);
			$col++;
		}
		$Row++;
	}
	
	my $col=1;
	for(my $index=0;$index<@DateRange;$index++) #### Coloumn Reference
	{
		my $key2 = $DateRange[$index];
		$sheet_Reference->write(0, $col, $key2, $Headerformat);
		$col++;
	}	
	undef(@DateRange);
	return $Row;
}