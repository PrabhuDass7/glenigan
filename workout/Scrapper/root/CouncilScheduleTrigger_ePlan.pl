use strict;
use Cwd qw(abs_path);
use File::Basename;
use POE qw(Wheel::Run Filter::Reference);
use Config::Tiny;
use Time::Piece;
use File::Path qw/make_path/;
use DateTime;


my $scheduleSerial = $ARGV[0]; 	# Council code set (1, 2, 3, ...)
my $category = $ARGV[1]; 		# Council code set (Online_Planning, Online_Decision, ..)
my $scheduledTime = $ARGV[2];	# Source i.e. 12:00:00,11:00:00,00:30:00...


####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $rootDirectory = ($basePath.'/root');

####
# Create new object of class Config::Tiny and Config::Simple
####
my ($councilDetailsFile,$coreCouncilFile) = Config::Tiny->new();


#----------------------- Get input -----------------------#
$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Input_Council_Code.ini');
my $councilCode = $councilDetailsFile->{$scheduleSerial}->{$category};

$coreCouncilFile = Config::Tiny->read($ConfigDirectory.'/Core_Config.ini');
my $threadLimits = $coreCouncilFile->{'pidControl'}->{'Thread'};


sub MAX_CONCURRENT_TASKS () { $threadLimits }


my $time = Time::Piece->new;
my $scheduleDate= $time->strftime('%d/%m/%Y');
$scheduleDate=$scheduleDate." ".$scheduledTime;

my $runDateTime = Time::Piece->strptime($scheduleDate, '%d/%m/%Y %H:%M:%S')->strftime('%Y/%m/%dT%H:%M:%S');
print "RunDateTime:::: $runDateTime\n";

my $startDateTime= $time->strftime('%Y/%m/%dT%H:%M:%S');
print "StartDateTime:::: $startDateTime\n";

my @cCode=split(',',$councilCode);


POE::Session->create(
  inline_states => {
    _start      => \&start_tasks,
    next_task   => \&start_tasks,
    task_result => \&handle_task_result,
    task_done   => \&handle_task_done,
    task_debug  => \&handle_task_debug,
    sig_child   => \&sig_child,
  }
);

sub start_tasks 
{
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	
	while (keys(%{$heap->{task}}) < MAX_CONCURRENT_TASKS) 
	{
		my $next_task = shift @cCode;
		last unless defined $next_task;
		
		print "Starting task for $next_task...\n";
		
		my $task = POE::Wheel::Run->new(
		Program      => sub { do_stuff($next_task) },
		StdoutFilter => POE::Filter::Reference->new(),
		StdoutEvent  => "task_result",
		StderrEvent  => "task_debug",
		CloseEvent   => "task_done",
		);
		
		$heap->{task}->{$task->ID} = $task;
		$kernel->sig_child($task->PID, "sig_child");
	}
}

sub do_stuff 
{
	binmode(STDOUT);    # Required for this to work on MSWin32
	my $councilCode   = shift;
	my $filter = POE::Filter::Reference->new();

	sleep(rand 5);
	
	# next if($councilCode=~m/^339$/is); # for St.Albans(IP blockd issue)
	
    my ($scriptName,$Execution_Status,$Return_Code,$formatType,$categoryType);	
	
	if($category=~m/^([^_]*?)_([^<]*?)$/is)
	{
		$formatType = $1;
		$categoryType = $2;
		
		$scriptName = $formatType."ScraperAlpha".$categoryType.".pl";
	}
	
	
	my $scriptRootDirectory = $rootDirectory.'/'.$formatType;
	
	print "PATH==>perl $scriptRootDirectory/$scriptName $councilCode $category $startDateTime $runDateTime"; 
	
	if($category=~m/^(Weekly_Planning|Weekly_Decision)$/is)
	{
		eval{		
			$Return_Code = system("perl $scriptRootDirectory/$scriptName $councilCode $category $startDateTime $runDateTime '' '' &");
		};
	}
	elsif($category=~m/^(ePlan_Planning|ePlan_Decision)$/is)
	{	
		eval{
			$Return_Code = system("perl $scriptRootDirectory/$scriptName $councilCode $category $startDateTime $runDateTime '' '' &");
		};
	}
	else
	{	
		eval{		
			$Return_Code = system("perl $scriptRootDirectory/$scriptName $councilCode $category '' '' '' $startDateTime $runDateTime '' '' &");
		};
	}
	
	my $statusLog = 'ScheduleTriggerLog';	
	my $logLocation = $logsDirectory.'/'.$statusLog.'/'.$todaysDate;
	
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	
	open(ERR,">>$logLocation/Trigger_Log.txt");
	if($Return_Code!=0)
	{
		$Execution_Status="Failed to Execute";
		print "CouncilCode:$councilCode\tCategory:$category\tStatus:$Execution_Status : $!\n";
		print ERR "CouncilCode::$councilCode\tCategory::$category\tTime::$scheduledTime\tStatus::$Execution_Status: $!\n";
	}
	else
	{
		$Execution_Status="Success";
		print "Status: $Execution_Status : $!\n";
		print ERR "CouncilCode::$councilCode\tCategory::$category\tTime::$scheduledTime\tStatus::$Execution_Status: $!\n";
	}
	close ERR;
	
	my %result = (
	task   => $councilCode,
	status => "seems ok to me",
	);

	my $output = $filter->put([\%result]);

	print @$output;
}

sub handle_task_result 
{
	my $result = $_[ARG0];
	print "Result for $result->{task}: $result->{status}\n";
}

sub handle_task_debug 
{
	my $result = $_[ARG0];
	print "Debug: $result\n";
}

sub handle_task_done {
	my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
	delete $heap->{task}->{$task_id};
	$kernel->yield("next_task");
}

sub sig_child {
	my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
	my $details = delete $heap->{$pid};

# warn "$$: Child $pid exited";
}

$poe_kernel->run();
exit 0;