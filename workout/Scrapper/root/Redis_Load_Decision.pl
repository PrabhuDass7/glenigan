use strict;
use RedisDB;
use File::Basename;
use Cwd  qw(abs_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;

my $libCoreDirectory = ($basePath.'/lib/Core');
# my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

my $dbh = &Glenigan_DB::gleniganDB();
my $redis = RedisDB->new(host => '127.0.0.1', port => 6379, database => 7);

my $FlushDB_Flag = $redis->flushdb();
print "FlushDB_Flag: $FlushDB_Flag\n";

my $Select_Query='select COUNCIL_CODE, APPLICATION_NO from PD_UPLOAD_DATA where convert(varchar,Upload_date,111) between
convert(varchar,getdate()-179,111)  and  convert(varchar,getdate(),111) and (len(Permission_Date)<>0 or len(refused_date)<>0 or len(Withdrawn_date)<>0)
order by  substring(convert(varchar,getdate(),103),7,4) ,
substring(convert(varchar,getdate(),103),3,2),
substring(convert(varchar,getdate(),103),1,2),id';


my $sth = $dbh->prepare($Select_Query);
$sth->execute();
my (@COUNCIL_CODE,@APPLICATION_NO);
my $reccount;
while(my @record = $sth->fetchrow)
{
	if(length($record[1])<=30)
	{
		my $COUNCIL_CODE = &Trim($record[0]);
		my $APPLICATION_NO = &Trim($record[1]);
		print "D_".$COUNCIL_CODE."_".$APPLICATION_NO => $APPLICATION_NO."\n";
		$redis->set("D_".$COUNCIL_CODE."_".$APPLICATION_NO => $APPLICATION_NO);
	}	
}
$sth->finish();



sub Trim
{
	my $txt = shift;
	$txt =~ s/^\s+|\s+$//igs;
	$txt =~ s/^\n+/\n/igs;
	return $txt;
}
