############################################################
# This docDatabase module have DB connection,
# query retrive, log insert, status update functions
# etc..
############################################################
package docDatabase;
use strict;
use DBI;
use DBD::ODBC;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd qw(abs_path);
require Exporter;

my @ISA = qw(Exporter);
my @EXPORT = ();

####
# Set root path for accessing files
####
my $basePath = dirname (dirname abs_path $0);

####
# Declare and load local directories
####
my $ConfigDirectory = ($basePath.'/ctrl');


####
# Declare new object of class for Config::Tiny
####
my $controlFile = Config::Tiny->new();
$controlFile = Config::Tiny->read($ConfigDirectory.'/docDownloadConfig.ini' );

####
# Declare global variable for tables
####
my $md5Table	= $controlFile->{'databaseTableName'}->{'md5Table'};
my $onDemandTable	= $controlFile->{'databaseTableName'}->{'onDemandTable'};
my $onDemandTableBackup	= $controlFile->{'databaseTableName'}->{'onDemandTableBackup'};
my $formatTable	= $controlFile->{'databaseTableName'}->{'formatTable'};
my $oneAppStatus	= $controlFile->{'databaseTableName'}->{'oneAppStatus'};
my $oneAppLog	= $controlFile->{'databaseTableName'}->{'OneAppLog'};
my $formatDocTable	= $controlFile->{'databaseTableName'}->{'Format_Document'};

####
# Declare variable for retrive Query days
####
my $days	= $controlFile->{'retriveQueryDays'}->{'days'};
my $onDemanddays	= $controlFile->{'retriveQueryDays'}->{'onDemanddays'};

####
# Declare global variable to select Prod or Devp DB
####
my $dbname	= $controlFile->{'selectDB'}->{'dbname'};

####
# Declare global variable for DB
####
my $driver	= $controlFile->{$dbname}->{'driver'};
my $server	= $controlFile->{$dbname}->{'server'};
my $database= $controlFile->{$dbname}->{'database'};
my $uid		= $controlFile->{$dbname}->{'uid'};
my $pwd		= $controlFile->{$dbname}->{'pwd'};

###########################################################
#Function : DbConnection()
#Desc  	  : Establish the connection to the database
#Date	  : 05-03-2019
###########################################################
sub DbConnection()
{
	my $dsn ='driver={'.$driver.'};Server='.$server.';database='.$database.';uid='.$uid.';pwd='.$pwd.';'; 
	
	my $dbh;
	my $reConFlag=1;
	Reconnect:
	if($dbh = DBI->connect("DBI:ODBC:$dsn"))
	{
		print "\nDB SERVER CONNECTED\n";
	}
	else
	{
		print "\nDB CONNENCTION FAILED\n";
		if($reConFlag<=3)
		{
			$reConFlag++;
			goto Reconnect;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}

###########################################################
#Function : retrieveInput()
#Desc  	  : Retrieve input records from Format Document Table
#Date	  : 05-03-2019
###########################################################
sub retrieveInput()
{
	my $dbh 			= shift;
	my $councilCode 	= shift;
	my $inputFormatID	= shift;
	
	print "Fetching council code $councilCode......\n";
		
	my $query;
	my %dbDatas;
	if($inputFormatID=~m/^\d+$/is)
	{
		print "FormatID:=> $inputFormatID\n";

		# $query = 'select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
				# case when project_id <> \'\' then project_id else house_extn_id end as project_id, project_status, COUNCIL_NAME 
				# from ( 
				# select ID, Source, Application_No, Document_Url, F.MARKUP, URL, No_of_Documents, 
				# isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				# isnull((select top 1 HOUSE_EXTN_ID_P from dr_house_Extn where Format_ID=f.id), \'\') as house_extn_id,
				# isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME
				# from '.$formatTable.' f,L_council co
				# where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$onDemanddays.',112) and CONVERT(Varchar(10),GETDATE(),112)
				# And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = '.$councilCode.' And co.COUNCIL_CD_P != \'486\' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\',\'Minor\') 
				# and ID in ('.$inputFormatID.')) as c ORDER BY ID DESC';
		$query = 'select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
				case when project_id <> \'\' then project_id else house_extn_id end as project_id, project_status, COUNCIL_NAME 
				from ( 
				select ID, Source, Application_No, Document_Url, F.MARKUP, URL, No_of_Documents, 
				isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				isnull((select top 1 HOUSE_EXTN_ID_P from dr_house_Extn where Format_ID=f.id), \'\') as house_extn_id,
				isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME
				from '.$formatTable.' f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$onDemanddays.',112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = '.$councilCode.' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\',\'Minor\') 
				and ID in ('.$inputFormatID.')) as c ORDER BY ID DESC';
	}
	else
	{	
		# $query = 'select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
				# case when project_id <> \'\' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME
				# from (
				# select ID, Source, Application_No, Document_Url, F.MARKUP, URL, No_of_Documents,
				# isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				# isnull((select top 1 HOUSE_EXTN_ID_P from dr_house_Extn where Format_ID=f.id),\'\') as house_extn_id,
				# isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME
				# from '.$formatTable.' f,L_council co
				# where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$days.',112) and CONVERT(Varchar(10),GETDATE(),112)
				# And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = '.$councilCode.' And co.COUNCIL_CD_P <> \'486\' and All_Documents_Downloaded = \'N\' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\')
				# ) as c where project_status <> \'Processed Complete\' ORDER BY ID DESC';
				
		
		$query = 'select id, SOURCE, Application_No, Document_Url, MARKUP, URL, No_of_Documents,
				case when project_id <> \'\' then project_id else house_extn_id end as project_id,project_status,COUNCIL_NAME
				from (
				select ID, Source, Application_No, Document_Url, F.MARKUP, URL, No_of_Documents,
				isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				isnull((select top 1 HOUSE_EXTN_ID_P from dr_house_Extn where Format_ID=f.id),\'\') as house_extn_id,
				isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME
				from '.$formatTable.' f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$days.',112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P = '.$councilCode.' and All_Documents_Downloaded = \'N\' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\')
				) as c where project_status <> \'Processed Complete\' ORDER BY ID DESC';
	}
		
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	while(my @record = $sth->fetchrow)
	{
		$dbDatas{$record[0]} = [$record[0],$record[1],$record[2],$record[3],$record[4],$record[5],$record[6],$record[7],$record[8],$record[9]];
	}
	$sth->finish();
	
=pod		
	$dbDatas{'8688507'} = ['8688507','','19/01711/FUL','https://planningapps.sheffield.gov.uk/online-applications/applicationDetails.do?activeTab=documents&keyVal=PRHCTWNYHGN00','Large','https://planningapps.sheffield.gov.uk/online-applications/applicationDetails.do?keyVal=PRHCTWNYHGN00&activeTab=summary','','','','']; # 531
=cut	
	return \%dbDatas;
}

###########################################################
#Function : retrieveInputCC()
#Desc  	  : Retrieve council name,code from Format Document Table for trigger script
#Date	  : 05-03-2019
###########################################################
sub retrieveInputCC()
{
	my $dbh 			= shift;

=pod	
	# This query will select only those projects are not downloaded today. Ehich means a project could run only once a day.
	my $query= 'select COUNCIL_NAME,COUNCIL_CD_P
				from (
				select ID,Source,Application_No,Document_Url,F.MARKUP,URL,
				isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				isnull((select top 1 HOUSE_EXTN_ID_P  from dr_house_Extn where Format_ID=f.id),\'\') as house_extn_id,
				isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
				from '.$formatTable.' f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$days.',112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P <> \'486\' and All_Documents_Downloaded = \'N\' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\')
				and (CONVERT(varchar,f.oa_scraped_date ,103)<>CONVERT(varchar,getdate(),103) or f.oa_scraped_date is null)
				) as c where project_status <> \'Processed Complete\' order by id desc';
=cut
				
	# This query will select all the projects which are not downloaded. which means a project could run all schedules in a day.
	# my $query= 'select COUNCIL_NAME, COUNCIL_CD_P
				# from (
				# select ID, Source, Application_No, Document_Url, F.MARKUP, URL,
				# isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				# isnull((select top 1 HOUSE_EXTN_ID_P  from dr_house_Extn where Format_ID=f.id),\'\') as house_extn_id,
				# isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
				# from '.$formatTable.' f,L_council co
				# where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$days.',112) and CONVERT(Varchar(10),GETDATE(),112)
				# And f.council_Code=co.COUNCIL_CD_P And co.COUNCIL_CD_P <> \'486\' and All_Documents_Downloaded = \'N\' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\')
				# ) as c where project_status <> \'Processed Complete\' order by id desc';
	
	my $query= 'select COUNCIL_NAME, COUNCIL_CD_P
				from (
				select ID, Source, Application_No, Document_Url, F.MARKUP, URL,
				isnull((select top 1 PROJECT_ID_P from DR_PROJECT where Format_ID=f.id),\'\') as project_id,
				isnull((select top 1 HOUSE_EXTN_ID_P  from dr_house_Extn where Format_ID=f.id),\'\') as house_extn_id,
				isnull((select top 1 status from Project_Status as a,DR_PROJECT as b where a.project_id=b.PROJECT_ID_P and b.Format_ID=f.id),\'\') as project_status, co.COUNCIL_NAME, co.COUNCIL_CD_P
				from '.$formatTable.' f,L_council co
				where CONVERT(Varchar(10),f.Schedule_Date,112) between CONVERT(Varchar(10),GETDATE()-'.$days.',112) and CONVERT(Varchar(10),GETDATE(),112)
				And f.council_Code=co.COUNCIL_CD_P and All_Documents_Downloaded = \'N\' and f.MARKUP in (\'HOUSE\',\'LARGE\',\'Smalls\')
				) as c where project_status <> \'Processed Complete\' order by id desc';
				
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@councilName,@councilCodeP);	
	while(my @record = $sth->fetchrow)
	{
		push(@councilName,&Trim($record[0]));
		push(@councilCodeP,&Trim($record[1]));
	}
	$sth->finish();
	
	my @filteredCC = uniq(@councilCodeP);
	my @filteredCN = uniq(@councilName);
	
	return (\@filteredCN,\@filteredCC);
}

###########################################################
#Function : retrieveOnDemondInput_CC()
#Desc  	  : Retrieve ondemand council code,id,format_id from Format Document Table for trigger script
#Date	  : 05-03-2019
###########################################################
sub retrieveOnDemondInput_CC()
{
	my $dbh 			= shift;
	
	my $query= 'select ID, council_code, format_id from '.$onDemandTable.' where download_type = \'document download\' and (status is null or status = \'\')';

	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@ID, @council_code,@format_id);	
	while(my @record = $sth->fetchrow)
	{
		push(@ID,&Trim($record[0]));
		push(@council_code,&Trim($record[1]));
		push(@format_id,&Trim($record[2]));
		print "Record: $record[0]\n";
	}
	$sth->finish();
	return (\@ID, \@council_code,\@format_id);
}


###########################################################
#Function : updateOndemondStatus()
#Desc  	  : Update ondemand council status from Document download
#Date	  : 05-03-2019
###########################################################
sub updateOndemondStatus()
{
	my $dbh		= shift;
	my $cCode	= shift;
	my $inputFormatID	= shift;
	
	my $onDemandUpdateQuery = 'update '.$onDemandTable.' set status = \'Inprogress\' where council_code='.$cCode.' and format_id = '.$inputFormatID.'';	
	
	&Execute($dbh,$onDemandUpdateQuery);
}

###########################################################
#Function : updateOneAppStatus()
#Desc  	  : Update oneApp council status from Document download
#Date	  : 05-03-2019
###########################################################
sub updateOneAppStatus()
{
	my $dbh		= shift;
	my $cCode	= shift;
	my $Downloaded_date	= shift;
	my $Todays_Count	= shift;
	my $Todays_Downloaded_Count	= shift;
	my $status	= shift;
	
	
	my $oneAppUpdateQuery = 'update '.$oneAppStatus.' set Current_Status = \''.$status.'\', Todays_Application_Count = \''.$Todays_Count.'\', Downloaded_Count = \''.$Todays_Downloaded_Count.'\' where Council_Code=\''.$cCode.'\' and Scraped_Date = \''.$Downloaded_date.'\'';	
	
	&Execute($dbh,$oneAppUpdateQuery);
}


###########################################################
#Function : updateOnDemandBackupStatus()
#Desc  	  : Update oneApp council status from Document download
#Date	  : 05-03-2019
###########################################################
sub updateOnDemandBackupStatus()
{
	my $dbh		= shift;
	my $cCode	= shift;
	my $onDemandStatus	= shift;
	my $inputFormatID	= shift;
	my $onDemandID	= shift;
	
	my $OnDemandBackupUpdateQuery = 'update '.$onDemandTableBackup.' set Status = '.$onDemandStatus.' where Council_Code='.$cCode.' and format_id = '.$inputFormatID.' and download_id = '.$onDemandID.'';	
	
	
	&Execute($dbh,$OnDemandBackupUpdateQuery);
}

###########################################################
#Function : deleteOnDemandDownload()
#Desc  	  : delete OnDemand status from Document download
#Date	  : 05-03-2019
###########################################################
sub deleteOnDemandDownload()
{
	my $dbh		= shift;
	my $cCode	= shift;
	my $onDemandID	= shift;
	my $inputFormatID	= shift;
	my $Status	= shift;
	
	
	my $deleteOnDemandDownloadQuery = 'delete from '.$onDemandTable.' where council_code='.$cCode.' and format_id = '.$inputFormatID.' and status = '.$Status.' and download_id = '.$onDemandID.'';	
	
	
	&Execute($dbh,$deleteOnDemandDownloadQuery);
}


###########################################################
#Function : insertOneAppStatus()
#Desc  	  : Insert oneApp council status from Document download
#Date	  : 05-03-2019
###########################################################
sub insertOneAppStatus()
{
	my $dbh		= shift;
	my $cCode	= shift;
	my $cName	= shift;
	my $downloadDate	= shift;
	my $status	= shift;
	
	my $insertQuery = 'insert into '.$oneAppStatus.' (Council_Code, COUNCIL_NAME, Current_Status, Scraped_Date) values ('.$cCode.', \''.$cName.'\', \''.$status.'\', \''.$downloadDate.'\')';
	
	print "$insertQuery\n";
	
	&Execute($dbh,$insertQuery);
}

###########################################################
#Function : Execute()
#Desc  	  : Execute queries function 
#Date	  : 05-03-2019
###########################################################
sub Execute()
{
	my $dbh		= shift;
	my $query 	= shift;
	
	print "Query::$query\n";
	
	my $sth = $dbh->prepare($query);
	
	my $reConFlag = 1;
	Reconnect:
	eval
	{
		$sth->execute();
		$sth->finish();
		
		print "Query Executed Successfully\n";
	};
	
	if($@)
	{
		if($reConFlag<=3)
		{
			$reConFlag++;
			goto Reconnect;
		}	
	
		print "Failed Query:: $query\n";
		
		open(ERR,">>Failed_Query.txt");
		print ERR $query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###########################################################
#Function : RetrieveMD5()
#Desc  	  : This Retrieve MD5 function retrive PDF_MD5 from TBL_PDF_MD5 table 
#Date	  : 05-03-2019
###########################################################
sub RetrieveMD5()
{
	my $dbh 			= shift;
	my $councilCode 	= shift;
	my $formatID	 	= shift;
	
	my $query = 'select PDF_MD5 from '.$md5Table.' where council_code='.$councilCode.' and format_id='.$formatID.'';
	
	print "MD5 Qry:::=> $query\n";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@pdfMD5);
	
	while(my @record = $sth->fetchrow)
	{
		push(@pdfMD5, &Trim($record[0]));
	}
	$sth->finish();
	
	return (\@pdfMD5);
}


###########################################################
#Function : uniq()
#Desc  	  : This function to filter out duplicate values from an array
#Date	  : 05-03-2019
###########################################################
sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}


###########################################################
#Function : Trim()
#Desc  	  : This function to check and remove space, junk values
#Date	  : 05-03-2019
###########################################################
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}

1;