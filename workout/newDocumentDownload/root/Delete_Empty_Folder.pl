use strict;
use Time::Piece; 
use File::Path 'rmtree';

my $path = 'C:/Glenigan/newDocumentDownload/data/Documents/';

print "$path\n";
opendir(my $dh, $path) || die "can't opendir $path: $!";
my $i=1;
open(FN,">C:/Glenigan/newDocumentDownload/logs/File_Size.txt");
while (my $folder_name=readdir ($dh)) 
{
	print "Folder: $folder_name\n";
	if($folder_name!~m/^\d+$/is)
	{
		print "Next ID\n";
		next;
	}
	my $Temp_pdf_store_path="$path$folder_name";
	print "Loc: $Temp_pdf_store_path\n";
	my @stats = stat $Temp_pdf_store_path;
	my $modifiedTime = localtime $stats[9];
	print "modtime: $modifiedTime\n";
	my $now=localtime();
	print "Currenttime: $now\n";

	my $format = '%a %b %d %H:%M:%S %Y';

	my $diff = Time::Piece->strptime($now, $format)
			 - Time::Piece->strptime($modifiedTime, $format);		
	my $Diff_Days=int($diff->days);				 
	print "Diff: $Diff_Days\n";
	# <STDIN>;
	if($Diff_Days>2)	
	{
		if (-d $Temp_pdf_store_path)
		{
			opendir(my $dh, "$Temp_pdf_store_path") || die "can't opendir $Temp_pdf_store_path: $!";
			my @File_Names = sort(grep(!/^(\.|\.\.|Thumbs\.db)$/, readdir($dh)));
			closedir $dh;
			
			my $cnt=@File_Names;
			print "Count: $cnt\n";
			if($cnt == 0)
			{
				print "Folder Empty\n";
				
				# system("rmdir -rf $Temp_pdf_store_path");
				rmtree($Temp_pdf_store_path);
				print FN "$folder_name\t$cnt\n";
			}
			else
			{
				print "$folder_name Not empty\n";
			}
		}	
	}	
}
close FN;
closedir $dh;

