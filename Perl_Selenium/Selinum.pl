use strict;
use DBI;
use DBD::ODBC;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Config::Tiny;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;

my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();

my $HOME_URL='https://publicaccess.leeds.gov.uk/online-applications/search.do?action=advanced';
print "HOME_URL==>$HOME_URL\n";


my %visited = ();
my $depth = 1;

spider_site($driver, $HOME_URL, $depth);

$driver->quit();



$driver->get($HOME_URL);
sleep(10);
my $model_cont=$driver->get_page_source($driver);

open(PPP,">Search_Content.html");
print PPP "$model_cont\n";
close(PPP);

$driver->find_element('//*[@id="applicationValidatedStart"]')->send_keys("13/10/2020");
sleep(5);
# print "FromDate: $input_from_date\n";

$driver->find_element('//*[@id="applicationValidatedEnd"]')->send_keys("20/10/2020");
sleep(5);	
# print "ToDate: $input_to_date\n";

$driver->find_element_by_xpath('//*[@id="advancedSearchForm"]/div[4]/input[2]')->click;

sleep(10);	

my $searchContent=$driver->get_page_source($driver);

open FH, ">Home.html";
print FH $searchContent;
close FH;
# exit;

$driver->quit();
$driver->close();

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}

