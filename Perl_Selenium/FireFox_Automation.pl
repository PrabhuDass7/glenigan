use strict;
use WWW::Mechanize;
use WWW::Mechanize::Firefox;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;
use File::Basename;
use Cwd  qw(abs_path);
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"248\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

# Establish connection with DB server
# my $dbh = &Glenigan_DB_Windows::DB_Planning();
my $dbh = &Glenigan_DB_Windows::Test_DB_Planning();

system('"C:/Program Files/Mozilla Firefox/firefox.exe"');

### Get Council Details from ini file ###

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "Leeds_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
my $FORM_START_ID = $Config->{$Council_Code}->{'FORM_START_ID'};
my $FORM_END_ID = $Config->{$Council_Code}->{'FORM_END_ID'};

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";

#### UserAgent Declaration ####

my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize::Firefox->new(autocheck => 0);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize::Firefox->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			autoclose => 1,
			});
}	

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

# Get search results using date ranges

my ($Responce, $javascript, $Ping_Status1);
$Responce = $mech->get($HOME_URL);
sleep(10);

my $contents = $mech->content;
my $Home_Content=$contents;

open(PPP,">Home_Content.html");
print PPP "$Home_Content\n";
close(PPP);
# exit;

$mech->form_number($FORM_NUMBER);
$mech->set_fields($FORM_START_ID => $From_Date);
$mech->set_fields($FORM_END_ID => $To_Date);
sleep(5);

$mech->click({ xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' });
sleep(10);

my $content = $mech->content;
my $code = $mech->status;
print "$code\n";
my $Search_Content=$content;
	
open(PPP,">Search_Content1.html");
print PPP "$Search_Content\n";
close(PPP);
# $mech->click({ xpath => '//*[@id="searchResultsContainer"]/p[1]/a'[$totalPage] });
	$mech->click({ selector => '.next', 1 });
	# $mech->field(".my_select_element_by_class", 1); 
	# $mech->field("#my_input", 100);
	# $mech->click({ selector => '#some_id' });
	