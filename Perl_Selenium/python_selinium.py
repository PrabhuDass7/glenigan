# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Planning/Nuneatonandbedworth'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		driver.maximize_window()
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source		
		advfinalCnt = content.encode("utf-8")

		
		driver.find_element_by_xpath('//*[@id="content"]/div[1]/p[2]/a').click()
		#Need to wait for the page to load
		time.sleep(20)
		main_content=driver.page_source
		
		with open(folder_path+"/302_out1.html", 'wb') as fd:
			fd.write(main_content.encode("utf-8"))
		raw_input()
		
		driver.find_element_by_xpath('//*[@id="doYouWantToSearc"]/span[2]/label').click()
		#Need to wait for the page to load
		time.sleep(10)
		searchfinalCnt=driver.page_source
		
		with open(folder_path+"/302_out.html", 'wb') as fd:
			fd.write(searchfinalCnt.encode("utf-8"))
		# raw_input()

		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno



# To collect Application URLs here
def collectAppURL(content, home_url, driver, CouncilCode, Source, conn, cursor, pagenumber):	
	try:		 
		bulkValuesForQuery=''
		if re.findall(r'<tr[^>]*?>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(content), re.IGNORECASE):
			# AppNum=re.findall(r'<tr[^>]*?>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(content), re.IGNORECASE)       
			AppNum=re.findall(r'<tr[^>]*?>\s*<td>\s*([^>]*?)\s*<\/td>', str(content), re.IGNORECASE)       
			print(AppNum)
			aplNum = 0
			appCount = 0
			insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date,Easting,Northing) values '
			with open(folder_path+"/361_out.html", 'wb') as fd:
				fd.write(content)
			
			print("pagenumber ==>",pagenumber)
			print("App Length ==>",len(AppNum))
			if pagenumber >= 2:
				aplNum = 3
			elif pagenumber == 1 and len(AppNum) >= 15:
				aplNum = 3
			elif pagenumber == 1 and len(AppNum) <= 14:
				aplNum = 2
			elif len(AppNum) <= 15:
				aplNum = 2
			else:
				aplNum = 3
				
			print("aplNum ==>",aplNum)
			for appNumber in AppNum:
				print("appNumber::::", appNumber)
				driver.find_element_by_xpath('//*[@id="Mainpage_gridMain"]/tbody/tr['+str(aplNum)+']/td[8]/input').click()
				time.sleep(10)  
				Appcontent=driver.page_source
				# print ("aplNum:::",aplNum)
				appURL=driver.current_url
				print("appURL::",appURL)
				# raw_input()
				# with open(folder_path+"/361_out_"+str(aplNum)+".html", 'wb') as fd:
					# fd.write(Appcontent)
				aplNum = aplNum + 1 
				# sys.exit()
				if re.search(r'^https?\:\/\/southderbyshirepr\.force\.com\/', str(appURL), re.IGNORECASE):
					Application_Number=re.findall(r'>\s*Application\s*Reference\s*<\/p>\s*<h1[^>]*?>\s*([^<]*?)\s*<\/h1>', str(Appcontent), re.IGNORECASE)
					DtAppReceived=re.findall(r'>\s*Received\s*Date\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Date\s*that\s*the\s*application\s*was[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					DtAppValid=re.findall(r'>\s*Valid\s*Date\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Date\s*that\s*the\s*application\s*was[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Application_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Address=re.findall(r'>\s*Site\s*Location\s*<\/span>\s*<div[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/div>\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
					Proposal=re.findall(r'>\s*Proposal\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Proposal[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Easting=re.findall(r'>\s*Easting\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Northing=re.findall(r'>\s*Northing\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					docURL=re.findall(r'<a[^>]*?title=\"Documents\"[^>]*?href=\"([^\"]*?)\"[^>]*?>', str(Appcontent), re.IGNORECASE)
					
					(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url,east,north) = ('','','','','','','','','','','','','','','','','','')

					if len(Application_Number) > 0:
						Application=clean(Application_Number[0])
					else:
						Application = appNumber

					if len(Proposal) > 0:
						ProposalCnt=clean(Proposal[0])

					if len(Address) > 0:
						AppAddress=clean(Address[0])
						
					Document_Url=appURL+docURL[0]
					print("Document_Url::",Document_Url)

					if len(Application_Status) > 0:
						ApplicationStatus=clean(Application_Status[0])

					if len(Easting) > 0:
						east=clean(Easting[0])
						
					if len(Northing) > 0:
						north=clean(Northing[0])
					
					if len(DtAppReceived) > 0:
						DateApplicationReceived=DtAppReceived[0]
						
						
					if len(DtAppValid) > 0:
						DateApplicationvalidated=DtAppValid[0]
						
					
					format1 = "%Y/%m/%d %H:%M"					
					Schedule_Date = datetime.now().strftime(format1)

					Source_With_Time = Source+"_"+Schedule_Date+"-perl"
					
					Council_Name = "South Derbyshire"
					joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived)+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated)+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"','"+str(east)+"','"+str(north)+"')"
					
					# print "ApplicationValues: ", joinValues
					bulkValuesForQuery = insertQuery+joinValues
					print "bulkValuesForQuery: ", bulkValuesForQuery
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					bulkValuesForQuery=''

					# if appCount == 0:
						# bulkValuesForQuery = insertQuery+joinValues
						# appCount += 1
					# elif appCount == 5:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# cursor.execute(bulkValuesForQuery)
						# conn.commit()
						# appCount=0
						# bulkValuesForQuery=''
					# else:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# appCount += 1	
					# driver.back()
					# if len(Application) > 0:
						# driver.execute_script("window.history.go(-2)")					
						# time.sleep(10)  				
					
					driver.get(home_url)
					#-- Wait funtion
					time.sleep(10)
					content=driver.page_source	
					
					# with open(folder_path+"/361_outback.html", 'wb') as fd:
						# fd.write(content)
				else:
				
					Application_Number=re.findall(r'Application\s*Reference\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					DtAppReceived=re.findall(r'Date\s*Received\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					DtAppValid=re.findall(r'Date\s*Valid\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)					
					# Application_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)					
					Address=re.findall(r'Site\s*Location\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Proposal=re.findall(r'Proposed\s*Development\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([\w\W]*?)\s*(?:<[^>]*?>)?\s*<\/td>\s*<\/tr>', str(Appcontent), re.IGNORECASE)
					Easting=re.findall(r'Easting\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Northing=re.findall(r'Northing\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					TargetDecdate=re.findall(r'Target\s*Date\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					
					(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url,east,north) = ('','','','','','','','','','','','','','','','','','')

					if len(Application_Number) > 0:
						Application=clean(Application_Number[0])
					else:
						Application = appNumber

					if len(Proposal) > 0:
						ProposalCnt=clean(Proposal[0])

					if len(Address) > 0:
						AppAddress=clean(Address[0])
						
					Document_Url=appURL
					print("Document_Url::",Document_Url)


					if len(Easting) > 0:
						east=clean(Easting[0])
						
					if len(Northing) > 0:
						north=clean(Northing[0])
					
					if len(DtAppReceived) > 0:
						DateApplicationReceived=DtAppReceived[0]
						
						
					if len(DtAppValid) > 0:
						DateApplicationvalidated=DtAppValid[0]
						
					if len(TargetDecdate) > 0:
						TargetDecdt=TargetDecdate[0]
						
					
					format1 = "%Y/%m/%d %H:%M"					
					Schedule_Date = datetime.now().strftime(format1)

					Source_With_Time = Source+"_"+Schedule_Date+"-perl"
					
					Council_Name = "South Derbyshire"
					joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived)+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated)+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"','"+str(east)+"','"+str(north)+"')"
					
					# print "ApplicationValues: ", joinValues
					bulkValuesForQuery = insertQuery+joinValues
					print "bulkValuesForQuery: ", bulkValuesForQuery
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					bulkValuesForQuery=''

					# if appCount == 0:
						# bulkValuesForQuery = insertQuery+joinValues
						# appCount += 1
					# elif appCount == 5:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# cursor.execute(bulkValuesForQuery)
						# conn.commit()
						# appCount=0
						# bulkValuesForQuery=''
					# else:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# appCount += 1					
					
					driver.get(home_url)
					#-- Wait funtion
					time.sleep(10)
					content=driver.page_source	
		else:
			print("ERROR!!!!!")
			
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# nextPageSection parse section	
def nextPageSection(searchcontent, homeURL, driver, CouncilCode, Source, conn, cursor):
	try:
		
		pageNumber = 2
		pageNum = 2
		fullQuery=''
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent)):
			fullQuery = collectAppURL(str(searchcontent), homeURL, driver, CouncilCode, Source, conn, cursor, 1)
			cursor.execute(fullQuery)
			conn.commit()
			check_next_page = re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>('+str(pageNumber)+'|\.\.\.)<\/a>\s*<\/td>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>('+str(pageNumber)+'|\.\.\.)<\/a>\s*<\/td>', str(searchcontent), re.I):					
					driver.find_element_by_xpath('//*[@id="Mainpage_gridMain"]/tbody/tr[1]/td/table/tbody/tr/td['+str(pageNum)+']/a').click()	
					#-- Wait funtion
					time.sleep(10)  
					searchcontent=driver.page_source
					
					# with open(folder_path+"/361_out1.html", 'wb') as fd:
						# fd.write(searchcontent)
					# sys.exit()
					
					fullQuery = collectAppURL(str(searchcontent), homeURL, driver, CouncilCode, Source, conn, cursor, pageNum)	
					cursor.execute(fullQuery)
					conn.commit()
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(searchcontent), re.I):
						check_next_page = re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(searchcontent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			fullQuery = collectAppURL(str(searchcontent), homeURL, driver, CouncilCode, Source, conn, cursor, 1)		
			
		
		return fullQuery

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

