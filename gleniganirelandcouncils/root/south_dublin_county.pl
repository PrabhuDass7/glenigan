use strict;
use WWW::Mechanize;
use IO::Socket::SSL;
use URI::Escape;
use URI::URL;
use Config::Tiny;
use MIME::Lite;
use DBI;
use DBD::ODBC;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;
use Redis;

# Establish connection with DB server
my $dbh = &DbConnection();

####
# Create new object of class Redis_connection
####
my $redisConnection = &Redis_connection();

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
my $todayDate = $dt->ymd('');

print "ScheduleDate==>$scheduleDate\n";
print "todayDate==>$todayDate\n";

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');

my $logLocation = $logsDirectory.'/FailedQuery/'.$todayDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

my $dbFailedLogLocation = $logsDirectory.'/dbConnFailedQuery/'.$todayDate;
my $appCounttDetails = $logsDirectory.'/ApplicationCountDetails/'.$todayDate;
my $dupAppDirectory = $logsDirectory.'/deDupApplicationDetails/'.$todayDate;

unless ( -d $dbFailedLogLocation )
{
	make_path($dbFailedLogLocation);
}

unless ( -d $appCounttDetails )
{
	make_path($appCounttDetails);
}

unless ( -d $dupAppDirectory )
{
	make_path($dupAppDirectory);
}


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );


print "Working via Old proxy..\n";
$mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
					 
my $Council_Code = $ARGV[0];

### Get Council Details from ini file ###
my ($Config,$ConfigRegex) = Config::Tiny->new();
$Config = Config::Tiny->read( $ConfigDirectory.'/ePlan_Alpha_Council_Details.ini');
$ConfigRegex = Config::Tiny->read( $ConfigDirectory.'/ePlan_Regex_Heap.ini');


my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $SEARCH_POST_CONTENT = $Config->{$Council_Code}->{'SEARCH_POST_CONTENT'};
my $TOKEN_REGEX = $Config->{$Council_Code}->{'TOKEN_REGEX'};
my $SEARCH_EVENTARGUMENT = $Config->{$Council_Code}->{'SEARCH_EVENTARGUMENT'};
my $SEARCH_EVENTTARGET = $Config->{$Council_Code}->{'SEARCH_EVENTTARGET'};
my $SEARCH_VIEWSTATE_REGEX = $Config->{$Council_Code}->{'SEARCH_VIEWSTATE_REGEX'};
my $SEARCH_VIEWSTATEGENERATOR = $Config->{$Council_Code}->{'SEARCH_VIEWSTATEGENERATOR'};
my $SEARCH_EVENTVALIDATION = $Config->{$Council_Code}->{'SEARCH_EVENTVALIDATION'};
my $SEARCH_PREVIOUSPAGE = $Config->{$Council_Code}->{'SEARCH_PREVIOUSPAGE'};
my $TYPE = $Config->{$Council_Code}->{'TYPE'};
my $WEEKLY_LIST_CONTENT_REGEX = $Config->{$Council_Code}->{'WEEKLY_LIST_CONTENT_REGEX'};
my $WEEKLY_LIST_REGEX = $Config->{$Council_Code}->{'WEEKLY_LIST_REGEX'};
my $WEEKLY_LIST_DOMAIN = $Config->{$Council_Code}->{'WEEKLY_LIST_DOMAIN'};
my $NEXT_PAGE_REGEX = $Config->{$Council_Code}->{'NEXT_PAGE_REGEX'};
my $NEXT_PAGE_POST_CONTENT = $Config->{$Council_Code}->{'NEXT_PAGE_POST_CONTENT'};
my $TOTAL_PAGES_REGEX = $Config->{$Council_Code}->{'TOTAL_PAGES_REGEX'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $APPLICATION_URL_DOMAIN = $Config->{$Council_Code}->{'APPLICATION_URL_DOMAIN'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $HOST = $Config->{$Council_Code}->{'HOST'};

$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate');
$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
$mech->add_header( 'Host' => $HOST);
$mech->add_header( 'Referer' => $SEARCH_URL);
$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');

$mech->get( $HOME_URL );
my $Content = $mech->content;
# open(DC,">Home_Content.html");
# print DC $Content;
# close DC;
# exit;
my @totalAppLinks;
my ($totalLinks) = &collectApplicationLinks($Content,$HOME_URL,$Council_Code,$APPLICATION_REGEX);
my @AppLinks = @{$totalLinks};
# print "Size of array==>@AppLinks\n"; <STDIN>;

my $balanceInsertQuery = &collectApplicationDetails(\@AppLinks, $scheduleDate, $Config, $ConfigRegex, $Council_Code, $COUNCIL_NAME, $HOST);
&DB_Insert($dbh,$balanceInsertQuery,'1');
	
push(@totalAppLinks,@AppLinks);

my $pageCount = $1 if($Content=~m/$TOTAL_PAGES_REGEX/is);
if($pageCount ne "")
{
	my $subCount = $pageCount - 1;
	print "subCount==>$subCount\n";
	my $subAppCount = $subCount * 20;
	
	# my $nextPageURL = $SEARCH_URL."\?p=".$pageCount;
	# print "nextPageURL==>$nextPageURL\n";
	
	my $num = 2;	
	while($Content=~m/$NEXT_PAGE_REGEX/gsi)
	{
		my $nextPageURL = $1;
		if($nextPageURL!~m/^https?/is)
		{
			$nextPageURL = URI::URL->new($nextPageURL)->abs( $HOME_URL, 1 );
		}
		$mech->get( $nextPageURL );
		# print "nextPageURL==>$nextPageURL\n"; <STDIN>;
		my $nextPageContent = $mech->content;
		$Content = $nextPageContent;
		my @nextPageAppLinks;
		if($nextPageContent=~m/$APPLICATION_REGEX/si)
		{
			my ($totalLinks) = &collectApplicationLinks($nextPageContent,$HOME_URL,$Council_Code,$APPLICATION_REGEX);
			@nextPageAppLinks = @{$totalLinks};
			push(@totalAppLinks,@nextPageAppLinks);		
			# print "Welcome\n";
			if($num eq $pageCount)
			{
				my $count = 0;
				while($nextPageContent=~m/$APPLICATION_REGEX/gsi)
				{
					$count++;
				}	
				
				my $finalCount = $subAppCount + $count;
				print "Actual page count is $pageCount and final count is $finalCount\n";
				
				open(DC,">>$appCounttDetails/CountDetail_$Council_Code.txt");
				print DC "$pageCount\t$finalCount\n";
				close DC;
			}
		}
		
		my $balanceInsertQuery = &collectApplicationDetails(\@nextPageAppLinks, $scheduleDate, $Config, $ConfigRegex, $Council_Code, $COUNCIL_NAME, $HOST);
		&DB_Insert($dbh,$balanceInsertQuery,$num);
			
		$num++;
	}
}

sub collectApplicationLinks()
{
	my ($Content,$homeURL,$receivedCouncilCode,$appRegex) = @_;
	
	my @appLinks;
	while($Content=~m/$appRegex/gsi)
	{
		my $firstMatch=$1;
		my $secondMatch=$2;
				
		my ($applicationURL,$applicationReference);
		
		$applicationURL = $secondMatch;
		$applicationReference = $firstMatch;
		
		if($applicationURL!~m/^https?/is)
		{
			$applicationURL = URI::URL->new($applicationURL)->abs( $homeURL, 1 );
		}
		
		$applicationURL=~s/\&amp\;/\&/gsi;
		
		# print "applicationURL=>$applicationURL\n"; <STDIN>;
		# print "applicationReference=>$applicationReference\n"; <STDIN>;
				
		if($applicationReference!~m/^\s*$/is)
		{
			my $checkRedis = $receivedCouncilCode."_".$applicationReference;
			
			if($redisConnection->exists($checkRedis))	
			{
				print "Exists : $applicationReference\n";
				my $today = [Today_and_Now()];
				my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
				open(ERR,">>$dupAppDirectory/${receivedCouncilCode}_deDupAppDetails.txt");
				print ERR "$receivedCouncilCode\t$applicationURL\t$todayDateTime\n";
				close ERR;
							
				next;
			}
			else
			{	
				# print "applicationURL=>$applicationURL\n"; <STDIN>;
				$redisConnection->set($receivedCouncilCode."_".$applicationReference => $applicationReference);
				print "Not Exists : $applicationReference\n";
				push ( @appLinks, $applicationURL);	
			}
		}
	}
	
	return (\@appLinks);
}

sub clean()
{
	my ($data2Clean) = @_;
	
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/,\s*(?:,\s*)+/, /igs;
	$data2Clean=~s/^\s*,\s*|\s*,\s*$//igs;
	$data2Clean=~s/\&\#039\;/'/igs;
	$data2Clean=~s/\\r\\n/ /igs;
	$data2Clean=~s/\&\#39\;/\'/igs;
	$data2Clean=~s/\&\#039\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/\s\s+/ /igs;
	
	return($data2Clean);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;
	my $num	= shift;
	
	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
		
		open(ERR,">>$logLocation/success_Query_$num.txt");
		print ERR $Query."\n";
		close ERR;
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	my $today = [Today_and_Now()];
	my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
	if($dbh = DBI->connect("dbi:Sybase:server=172.27.137.184;database=GLENIGAN_IRELAND_COUNCILS", 'User2', 'Merit456'))
	{
		print "\nGlenigan Ireland SERVER CONNECTED\n";
		$Connection_Flag=1;
		if($Recon_Flag_Main > 1)
		{	
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Success$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
		}
	}
	else
	{
		print "\nGlenigan Ireland SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed_$Recon_Flag_Main==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}

##### REDIS CONN ######
sub Redis_connection()
{
	my $redis;
	
	eval{
		$redis = Redis->new(server => '127.0.0.1:6379');
		print "Planning Redis Connected\n";
	};
	if($@)
	{
		print "Planning Redis not Connected\n";
		eval{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = Redis->new(server => '127.0.0.1:6379');
		};
		if($@)
		{
			&send_mail_redis("Redis Connection", "Unable to connect Redis", "Planning");
		}	
	}

	return $redis;
}

####### SENT MAIL ########
sub send_mail_redis()
{
    my $Subject=shift;
    my $Body=shift;
    my $decision=shift;
	
	my $Current_dateTime=localtime();
	
    my $subject="Redis connection failed- $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='parielavarasan.surulinathan@meritgroup.co.uk';
    my $pass='sXNdrc6JU';
    my $body = "Hi Team, \n\n\t\tUnable to connect Redis $decision server.\n\nRegards\nGlenigan Team";
	
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 # Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";

    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}


###### collect Application Details ######
sub collectApplicationDetails()
{
	my ($applicationLink, $scheduleDate, $Config, $ConfigRegex, $councilCode, $councilName, $host) = @_;
	my @applicationLink = @{$applicationLink};
	
	my $insertQuery = "insert into IreLand_Council_New_Data values ";
	my $bulkValuesForQuery='';
	my $applicationCount=0;
	my $number=0;
	foreach my $appPageURL (@applicationLink)
    {
		my $Recon = 1;
		
		ReTry:		
		$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
		$mech->add_header( 'Accept-Encoding' => 'gzip, deflate');
		$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
		$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
		$mech->add_header( 'Host' => $host);
		$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');
		
		if($Recon >= 2)
		{
			$mech->get($Config->{$councilCode}->{'URL'});
		}
		$mech->get($appPageURL);
		my $appPageContent = $mech->content;
		# if($Recon<=3)
		# {
			# open(DC,">appPageContent.html");
			# print DC $appPageContent;
			# close DC;
			# exit;
		# }
		
		$appPageContent=~s/\<\!\-\-[\w\W]*?\-\-\>//gsi;
			
		my $Planning_Application_Ref = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PLANNING_APPLICATION_REF'}/is);		
		# my $Application_URL = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_URL'}/is);
		my $Application_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_DATE'}/is);
		my $Registration_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'REGISTRATION_DATE'}/is);
		my $Decision_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DATE'}/is);
		my $Application_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_TYPE'}/is);
		my $Extension_of_time_to = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'EXTENSION_OF_TIME_TO'}/is);
		my $Main_Location = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'MAIN_LOCATION'}/is);
		my $Proposal = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PROPOSAL'}/is);
		my $Full_Description = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FULL_DESCRIPTION'}/is);
		my $Status = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'STATUS'}/is);
		my $Status_Description = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'STATUS_DESCRIPTION'}/is);
		my $Comment = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'COMMENT'}/is);
		my $Applicant_Titles = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_TITLES'}/is);
		my $Applicant_Forename_Initial = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_FORENAME_INITIAL'}/is);
		my $Applicant_Surname_Company_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_SURNAME_COMPANY_NAME'}/is);
		my $Applicant_Company_Contact_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_COMPANY_CONTACT_NAME'}/is);
		my $Applicant_Case_Officer = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_CASE_OFFICER'}/is);
		my $Agent_Titles = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_TITLES'}/is);
		my $Agent_Forename_Initial = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_FORENAME_INITIAL'}/is);
		my $Agent_Surname_Company_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_SURNAME_COMPANY_NAME'}/is);
		my $Agent_Company_Contact_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_COMPANY_CONTACT_NAME'}/is);
		my $Agent_Address = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_ADDRESS'}/is);
		my $Decision = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION'}/is);
		my $Decision_Decision_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DECISION_DATE'}/is);
		my $Publicity_Start_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PUBLICITY_START_DATE'}/is);
		my $Publicity_End_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PUBLICITY_END_DATE'}/is);
		my $Type_of_Appeal = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'TYPE_OF_APPEAL'}/is);
		my $Appeal_Lodged_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_LODGED_DATE'}/is);
		my $Appeal_Decision = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_DECISION'}/is);
		my $Appeal_Decision_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_DECISION_DATE'}/is);
		my $Date_Received = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DATE_RECEIVED'}/is);
		my $Last_Action = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'LAST_ACTION'}/is);
		my $Submission_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'SUBMISSION_TYPE'}/is);
		my $Date_for_Submissions = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DATE_FOR_SUBMISSIONS'}/is);
		my $Decision_Due = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DUE'}/is);
		my $Final_Grant_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FINAL_GRANT_DATE'}/is);
		my $Document_Url = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DOCUMENT_URL'}/is);
		
		if($Document_Url ne "")
		{
			if($Document_Url!~m/^https?/is)
			{
				$Document_Url = URI::URL->new($Document_Url)->abs( $Config->{$councilCode}->{'URL'}, 1 );
			}
		}
		else
		{
			$Document_Url = $appPageURL;
		}
		# print "Document_Url==>$Document_Url\n"; <STDIN>;
		
		if(($Planning_Application_Ref eq "") && ($Recon<=3))
		{	
			print "Reconnecting..\n";
			$Recon++;
			goto ReTry;
		}		
		
		print "Planning_Application_Ref==>$Planning_Application_Ref\n";
			
		my $joinValues="(\'$councilCode\', \'$councilName\', \'$appPageURL\', \'$scheduleDate\', \'$Application_Date\', \'$Planning_Application_Ref\', \'$Registration_Date\', \'$Decision_Date\', \'$Application_Type\', \'$Extension_of_time_to\', \'$Main_Location\', \'$Proposal\', \'$Full_Description\', \'$Status\', \'$Status_Description\', \'$Comment\', \'$Applicant_Titles\', \'$Applicant_Forename_Initial\', \'$Applicant_Surname_Company_Name\', \'$Applicant_Company_Contact_Name\', \'$Applicant_Case_Officer\', \'$Agent_Titles\', \'$Agent_Forename_Initial\', \'$Agent_Surname_Company_Name\', \'$Agent_Company_Contact_Name\', \'$Agent_Address\', \'$Decision\', \'$Decision_Decision_Date\', \'$Publicity_Start_Date\', \'$Publicity_End_Date\', \'$Type_of_Appeal\', \'$Appeal_Lodged_Date\', \'$Appeal_Decision\', \'$Appeal_Decision_Date\', \'$Date_Received\', \'$Last_Action\', \'$Submission_Type\', \'$Date_for_Submissions\', \'$Decision_Due\', \'$Final_Grant_Date\', \'$Document_Url\'),";
		print "$joinValues\n";
		if($applicationCount == 0)
		{
			$bulkValuesForQuery = $insertQuery.$joinValues;
			$applicationCount++;
		}
		elsif($applicationCount == 9)
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			
			if($bulkValuesForQuery!~m/values\s*$/is)
			{
				$bulkValuesForQuery=~s/\,\s*$//is;
				$number++;
				&DB_Insert($dbh,$bulkValuesForQuery,$number);
				$applicationCount = 0;
				$bulkValuesForQuery="";
			}
		}
		else
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			$applicationCount++;
		}
	}
	
	$bulkValuesForQuery=~s/\,\s*$//is;
	
	return ($bulkValuesForQuery);
}