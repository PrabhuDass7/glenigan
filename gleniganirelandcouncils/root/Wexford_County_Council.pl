use strict;
use WWW::Mechanize;
use HTTP::CookieMonster;
use IO::Socket::SSL;
use URI::Escape;
use URI::URL;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use MIME::Lite;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Path qw/make_path/;
use POSIX;
use Redis;# Establish connection with DB server
my $dbh = &DbConnection();

####
# Create new object of class Redis_connection
####
my $redisConnection = &Redis_connection();

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
my $todayDate = $dt->ymd('');

print "ScheduleDate==>$scheduleDate\n";
print "todayDate==>$todayDate\n";

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $libCoreDirectory = ($basePath.'/lib');

# require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

my $logLocation = $logsDirectory.'/FailedQuery/'.$todayDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

my $dbFailedLogLocation = $logsDirectory.'/dbConnFailedQuery/'.$todayDate;
my $appCounttDetails = $logsDirectory.'/ApplicationCountDetails/'.$todayDate;
my $dupAppDirectory = $logsDirectory.'/deDupApplicationDetails/'.$todayDate;

unless ( -d $dbFailedLogLocation )
{
	make_path($dbFailedLogLocation);
}

unless ( -d $appCounttDetails )
{
	make_path($appCounttDetails);
}

unless ( -d $dupAppDirectory )
{
	make_path($dupAppDirectory);
}


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );


print "Working via Old proxy..\n";
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# Old Proxy	
					 
my $councilCode = $ARGV[0];

### Get Council Details from ini file ###
my ($Config,$ConfigRegex,$controlFile) = Config::Tiny->new();
$controlFile = Config::Tiny->read($ConfigDirectory.'/Core_Config.ini' );
$Config = Config::Tiny->read( $ConfigDirectory.'/Wexford_Council_Details.ini');
$ConfigRegex = Config::Tiny->read( $ConfigDirectory.'/Wexford_Regex_Heap.ini');

my $COUNCIL_NAME = $Config->{$councilCode}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$councilCode}->{'URL'};
my $SEARCH_URL = $Config->{$councilCode}->{'SEARCH_URL'};
my $SEARCH_POST_CONTENT = $Config->{$councilCode}->{'SEARCH_POST_CONTENT'};

print "SEARCH_URL==>$SEARCH_URL\n";
print "SEARCH_POST_CONTENT==>$SEARCH_POST_CONTENT\n";
	
$mech->get( "https://dms.wexfordcoco.ie/terms.php?q=1");
$mech->get( "https://dms.wexfordcoco.ie/index.php");
$mech->post( $SEARCH_URL, Content => "$SEARCH_POST_CONTENT");

$mech->get( "https://dms.wexfordcoco.ie/searchj.php?pr=&an=&pp=&dl=&lr=0&sid=0.11579800980031663");

$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
$mech->add_header( 'Accept' => 'application/json, text/javascript, */*');
$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
$mech->add_header( 'Host' => 'dms.wexfordcoco.ie');
$mech->add_header( 'Referer' => 'https://dms.wexfordcoco.ie/index.php');
$mech->add_header( 'X-Requested-With' => 'XMLHttpRequest');
$mech->post( $SEARCH_URL, Content => "$SEARCH_POST_CONTENT");

my $responseContent = $mech->content;	
my $pingStatus = $mech->status;	
# open(PP, ">a.html");
# print PP "$responseContent\n";
# close(PP);

my $count = 0;
my $Council_Name = 'Wexford County Council';
my $tempQuery;
my $query = "INSERT INTO Ireland_Council_Wexford (Council_Code, Council_Name, Application_URL, Schedule_Date, Document_URL, Application_No, Application_Type, Application_Date, Date_Registered, Applicant_Name, Development, Proposal, Area, Decision, Decision_Stage, Decision_Date) VALUES ";
while($responseContent=~m/\{id\s*\:\s*\'[^\']*?\'\,\s*cell\s*\:\s*\[\'[^\']*?\'\s*\,\s*\'([^\']*?)\'/gsi)
{
	my $appNum = $1;
	Loop:
	my $appURL = "https://dms.wexfordcoco.ie/fsdocumentsx.php?q=$appNum&recid=0";
	print "$appNum<==>$appURL\n";
	$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
	$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
	$mech->add_header( 'Host' => 'dms.wexfordcoco.ie');
	$mech->add_header( 'Referer' => 'https://dms.wexfordcoco.ie/index.php');
	$mech->get($appURL);
	my $appContent = $mech->content;	
	my $status = $mech->status;	
	
	my $Application_No = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_NO'}/is);
	my $Application_Type = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_TYPE'}/is);
	my $Application_Date = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_DATE'}/is);
	my $Date_Registered = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'DATE_REGISTERED'}/is);
	my $Applicant_Name = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_NAME'}/is);
	my $Development = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'DEVELOPMENT'}/is);
	my $Proposal;
	if($appContent=~m/$ConfigRegex->{$councilCode}->{'PROPOSAL'}/is)
	{
		my $description = $1;
		if($description=~m/<iframe[^>]*?src=\s*\"([^\"]*?)\"[^>]*?>/is)
		{
			my $pURL = $1;
			if($pURL!~m/https?\:/is)
			{
				$pURL = URI::URL->new($pURL)->abs( $Config->{$councilCode}->{'URL'}, 1 );
			}	
		
			if($pURL ne "")
			{
				$mech->get($pURL);
				my $appContent = $mech->content;	
				$Proposal = &htmlTagClean($1) if($appContent=~m/<p[^>]*?>\s*([\w\W]*?)\s*<\/p>\s*<\/body>/is);
				# print "Sub==>$Proposal\n";
			} 
		}
		else
		{
			$Proposal = &htmlTagClean($description);
			# print "Proposal==>$Proposal\n";
		}
	}
	my $Area = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'AREA'}/is);
	my $Decision = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'DECISION'}/is);
	my $Decision_Stage = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_STAGE'}/is);
	my $Decision_Date = &htmlTagClean($1) if($appContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DATE'}/is);
	
	my $queryValue = "(\'$councilCode\', \'$Council_Name\', \'$appURL\', \'$scheduleDate\', \'$appURL\', \'$Application_No\', \'$Application_Type\', \'$Application_Date\', \'$Date_Registered\', \'$Applicant_Name\', \'$Development\', \'$Proposal\', \'$Area\', \'$Decision\', \'$Decision_Stage\', \'$Decision_Date\'), ";
	
	print "$count==>$Application_No\n";
	
	undef $Application_No; undef $Application_Type; undef $Application_Date; undef $Date_Registered; undef $Applicant_Name; undef $Development; undef $Proposal; undef $Area; undef $Decision; undef $Decision_Stage; undef $Decision_Date;
	
	$tempQuery .= $queryValue;
	$count++;
	
	if(($status!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	if($count=~m/^50$/is)
	{
		$tempQuery=~s/\,\s*$//gsi;

		$tempQuery = $query.$tempQuery;
		
		if($tempQuery!~m/values\s*$/is)
		{
			&DB_Insert($dbh,$tempQuery);
			undef $tempQuery;
		}
		
		my $random_number = rand(3);
		
		my $sleepRandomNumber = ceil($random_number);
		
		print "Random Sleep Number:: $sleepRandomNumber\n";

		sleep($sleepRandomNumber);
		
		$count = 0;
	}
}
$tempQuery=~s/\,\s*$//gsi;
		
$query .= $tempQuery;

if($query!~m/values\s*$/is)
{
	&DB_Insert($dbh,$query);
	undef $tempQuery;
}




###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;
	my $num	= shift;
	
	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
		
		open(ERR,">>$logLocation/success_Query_$num.txt");
		print ERR $Query."\n";
		close ERR;
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	my $today = [Today_and_Now()];
	my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
	if($dbh = DBI->connect("dbi:Sybase:server=172.27.137.184;database=GLENIGAN_IRELAND_COUNCILS", 'User2', 'Merit456'))
	{
		print "\nGlenigan Ireland SERVER CONNECTED\n";
		$Connection_Flag=1;
		if($Recon_Flag_Main > 1)
		{	
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Success$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
		}
	}
	else
	{
		print "\nGlenigan Ireland SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed_$Recon_Flag_Main==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}

##### REDIS CONN ######
sub Redis_connection()
{
	my $redis;
	
	eval{
		$redis = Redis->new(server => '127.0.0.1:6379');
		print "Planning Redis Connected\n";
	};
	if($@)
	{
		print "Planning Redis not Connected\n";
		eval{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = Redis->new(server => '127.0.0.1:6379');
		};
		if($@)
		{
			&send_mail_redis("Redis Connection", "Unable to connect Redis", "Planning");
		}	
	}

	return $redis;
}

####### SENT MAIL ########
sub send_mail_redis()
{
    my $Subject=shift;
    my $Body=shift;
    my $decision=shift;
	
	my $Current_dateTime=localtime();
	
    my $subject="Redis connection failed- $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='parielavarasan.surulinathan@meritgroup.co.uk';
    my $pass='sXNdrc6JU';
    my $body = "Hi Team, \n\n\t\tUnable to connect Redis $decision server.\n\nRegards\nGlenigan Team";
	
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 # Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";

    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}



sub get_cookie_session_details()
{
	my $url = shift;
	my $keyName = shift;
	
	my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
	$mech->get( $url );	
	my $pingStatus = $mech->status;
	# my $responseContent = $mech->content;	
	my $cookie = $monster->get_cookie($keyName);	
	my $sessionID;
	eval
	{
		$sessionID = $cookie->val();
	};
	if($@)
	{
		print "JSESSIONCOOKIEISSUE_REASON==>$@\n";
		$sessionID = "JSESSION_ISSUE";
	}
	
	return($sessionID);
}


####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	$data2Clean=~s/^\s+//igs;
	$data2Clean=~s/\s+$//igs;
	
	return($data2Clean);
}