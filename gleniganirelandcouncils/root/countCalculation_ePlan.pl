use strict;
use WWW::Mechanize;
use IO::Socket::SSL;
use URI::Escape;
use URI::URL;
use Config::Tiny;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;


my $dt = DateTime->now();
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
my $todayDate = $dt->ymd('');

print "ScheduleDate==>$scheduleDate\n";
print "todayDate==>$todayDate\n";

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');

my $logLocation = $logsDirectory.'/FailedQuery/'.$todayDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

my $dbFailedLogLocation = $logsDirectory.'/dbConnFailedQuery/'.$todayDate;
my $appCounttDetails = $logsDirectory.'/ApplicationCountDetails/'.$todayDate;
my $dupAppDirectory = $logsDirectory.'/deDupApplicationDetails/'.$todayDate;

unless ( -d $dbFailedLogLocation )
{
	make_path($dbFailedLogLocation);
}

unless ( -d $appCounttDetails )
{
	make_path($appCounttDetails);
}

unless ( -d $dupAppDirectory )
{
	make_path($dupAppDirectory);
}


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );
			

print "Working via Old proxy..\n";
# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
			 
my $Council_Code = $ARGV[0];

### Get Council Details from ini file ###
my ($Config,$ConfigRegex) = Config::Tiny->new();
$Config = Config::Tiny->read( $ConfigDirectory.'/ePlan_Alpha_Council_Details.ini');
$ConfigRegex = Config::Tiny->read( $ConfigDirectory.'/ePlan_Regex_Heap.ini');

my @alphabet = ('A'..'Z');
foreach my $alpha (@alphabet) {	
	print "Now Alphabet $alpha is started processing\n";
	my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
	my $HOME_URL = $Config->{$Council_Code}->{'URL'};
	my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
	my $SEARCH_POST_CONTENT = $Config->{$Council_Code}->{'SEARCH_POST_CONTENT'};
	my $TOKEN_REGEX = $Config->{$Council_Code}->{'TOKEN_REGEX'};
	my $SEARCH_EVENTARGUMENT = $Config->{$Council_Code}->{'SEARCH_EVENTARGUMENT'};
	my $SEARCH_EVENTTARGET = $Config->{$Council_Code}->{'SEARCH_EVENTTARGET'};
	my $SEARCH_VIEWSTATE_REGEX = $Config->{$Council_Code}->{'SEARCH_VIEWSTATE_REGEX'};
	my $SEARCH_VIEWSTATEGENERATOR = $Config->{$Council_Code}->{'SEARCH_VIEWSTATEGENERATOR'};
	my $SEARCH_EVENTVALIDATION = $Config->{$Council_Code}->{'SEARCH_EVENTVALIDATION'};
	my $SEARCH_PREVIOUSPAGE = $Config->{$Council_Code}->{'SEARCH_PREVIOUSPAGE'};
	my $TYPE = $Config->{$Council_Code}->{'TYPE'};
	my $WEEKLY_LIST_CONTENT_REGEX = $Config->{$Council_Code}->{'WEEKLY_LIST_CONTENT_REGEX'};
	my $WEEKLY_LIST_REGEX = $Config->{$Council_Code}->{'WEEKLY_LIST_REGEX'};
	my $WEEKLY_LIST_DOMAIN = $Config->{$Council_Code}->{'WEEKLY_LIST_DOMAIN'};
	my $NEXT_PAGE_REGEX = $Config->{$Council_Code}->{'NEXT_PAGE_REGEX'};
	my $NEXT_PAGE_POST_CONTENT = $Config->{$Council_Code}->{'NEXT_PAGE_POST_CONTENT'};
	my $TOTAL_PAGES_REGEX = $Config->{$Council_Code}->{'TOTAL_PAGES_REGEX'};
	my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
	my $APPLICATION_URL_DOMAIN = $Config->{$Council_Code}->{'APPLICATION_URL_DOMAIN'};
	my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};

	$mech->get( $HOME_URL );
	my $getContent = $mech->content;
	# open(DC,">Home_Content.html");
	# print DC $getContent;
	# close DC;
	# exit;
	my $token = uri_escape($1) if($getContent=~m/$TOKEN_REGEX/is);
	my $previousPage = uri_escape($1) if($getContent=~m/$SEARCH_PREVIOUSPAGE/is);
	my $eventValidation = uri_escape($1) if($getContent=~m/$SEARCH_EVENTVALIDATION/is);
	my $viewStateGen = uri_escape($1) if($getContent=~m/$SEARCH_VIEWSTATEGENERATOR/is);
	my $viewState = uri_escape($1) if($getContent=~m/$SEARCH_VIEWSTATE_REGEX/is);
	my $eventTarget = uri_escape($1) if($getContent=~m/$SEARCH_EVENTTARGET/is);
	my $eventArgument = uri_escape($1) if($getContent=~m/$SEARCH_EVENTARGUMENT/is);
	

	$SEARCH_POST_CONTENT=~s/<TOKEN>/$token/is;
	$SEARCH_POST_CONTENT=~s/<ALPHA>/$alpha/is;
	$SEARCH_POST_CONTENT=~s/<EVENTARGUMENT>/$eventArgument/is;
	$SEARCH_POST_CONTENT=~s/<VIEWSTATE>/$viewState/is;
	$SEARCH_POST_CONTENT=~s/<VIEWSTATEGENERATOR>/$viewStateGen/is;
	$SEARCH_POST_CONTENT=~s/<PREVIOUSPAGE>/$previousPage/is;
	$SEARCH_POST_CONTENT=~s/<EVENTVALIDATION>/$eventValidation/is;	
	if($eventTarget eq "")
	{
		$SEARCH_POST_CONTENT=~s/<EVENTTARGET>/ctl00%24MainContentPlaceHolder%24SearchButton/is;
	}
	else
	{
		$SEARCH_POST_CONTENT=~s/<EVENTTARGET>/$eventTarget/is;
	}
	
	$mech->post($SEARCH_URL, Content=> $SEARCH_POST_CONTENT);
	my $Content = $mech->content;
	# print $mech->status()."\n";
	# open(DC,">Content.html");
	# print DC $Content;
	# close DC;
	# exit;
	
	my $pageCount = $1 if($Content=~m/$TOTAL_PAGES_REGEX/is);
	if($pageCount ne "")
	{
		my $subCount = $pageCount - 1;
		# print "subCount==>$subCount\n";
		my $subAppCount = $subCount * 10;
			
		my $nextPageURL;
		
		if($Council_Code=~m/^1104$/is)
		{
			$nextPageURL = "http://www.donegalcdb.ie/eplan/internetenquiry/frmSelCritSearch.asp?page_num=$pageCount&Op=1";
		}
		else
		{
			$nextPageURL = $SEARCH_URL."/Default/".$pageCount;
		}
		
		print "$nextPageURL\n";
		
		$mech->get($nextPageURL);
		my $nextPageContent = $mech->content;
		my $count = 0;
		while($nextPageContent=~m/$APPLICATION_REGEX/gsi)
		{
			$count++;
		}	
		
		my $finalCount = $subAppCount + $count;
		print "$alpha: Actual page count is $pageCount and final count is $finalCount\n";
		
		open(DC,">>$appCounttDetails/CountDetail_$Council_Code.txt");
		print DC "$alpha\t$pageCount\t$finalCount\n";
		close DC;		
	}
}
