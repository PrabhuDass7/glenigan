use strict;
use WWW::Mechanize;
use HTTP::CookieMonster;
use IO::Socket::SSL;
use URI::Escape;
use URI::URL;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use MIME::Lite;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Path qw/make_path/;
use Redis;# Establish connection with DB server
my $dbh = &DbConnection();

####
# Create new object of class Redis_connection
####
my $redisConnection = &Redis_connection();

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
my $todayDate = $dt->ymd('');

print "ScheduleDate==>$scheduleDate\n";
print "todayDate==>$todayDate\n";

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $libCoreDirectory = ($basePath.'/lib');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

my $logLocation = $logsDirectory.'/FailedQuery/'.$todayDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

my $dbFailedLogLocation = $logsDirectory.'/dbConnFailedQuery/'.$todayDate;
my $appCounttDetails = $logsDirectory.'/ApplicationCountDetails/'.$todayDate;
my $dupAppDirectory = $logsDirectory.'/deDupApplicationDetails/'.$todayDate;

unless ( -d $dbFailedLogLocation )
{
	make_path($dbFailedLogLocation);
}

unless ( -d $appCounttDetails )
{
	make_path($appCounttDetails);
}

unless ( -d $dupAppDirectory )
{
	make_path($dupAppDirectory);
}


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );


print "Working via Old proxy..\n";
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# Old Proxy	
					 
my $councilCode = $ARGV[0];

### Get Council Details from ini file ###
my ($Config,$ConfigRegex,$controlFile) = Config::Tiny->new();
$controlFile = Config::Tiny->read($ConfigDirectory.'/Core_Config.ini' );
$Config = Config::Tiny->read( $ConfigDirectory.'/ePlan_Alpha_Council_Details.ini');
$ConfigRegex = Config::Tiny->read( $ConfigDirectory.'/ePlan_Regex_Heap.ini');

my %defaultDateRanges = &CouncilsScrapperCore::defaultGcsRange( DateTime->now );
my ($divCounter, $scheduleCounter);

my ($fromDate, $toDate, $rangeName);
my ($searchResultCount, $responseContents, $responseStatus);

foreach my $rangeName (sort keys %defaultDateRanges) 
{
	$divCounter = 1;
	$scheduleCounter = 0;
	my $COUNCIL_NAME = $Config->{$councilCode}->{'COUNCIL_NAME'};
	my $HOME_URL = $Config->{$councilCode}->{'URL'};
	my $SEARCH_URL = $Config->{$councilCode}->{'SEARCH_URL'};
	my $SEARCH_POST_CONTENT = $Config->{$councilCode}->{'SEARCH_POST_CONTENT'};
	my $TOKEN_REGEX = $Config->{$councilCode}->{'TOKEN_REGEX'};
	my $SEARCH_EVENTARGUMENT = $Config->{$councilCode}->{'SEARCH_EVENTARGUMENT'};
	my $SEARCH_EVENTTARGET = $Config->{$councilCode}->{'SEARCH_EVENTTARGET'};
	my $SEARCH_VIEWSTATE_REGEX = $Config->{$councilCode}->{'SEARCH_VIEWSTATE_REGEX'};
	my $SEARCH_VIEWSTATEGENERATOR = $Config->{$councilCode}->{'SEARCH_VIEWSTATEGENERATOR'};
	my $SEARCH_EVENTVALIDATION = $Config->{$councilCode}->{'SEARCH_EVENTVALIDATION'};
	my $SEARCH_PREVIOUSPAGE = $Config->{$councilCode}->{'SEARCH_PREVIOUSPAGE'};
	my $TYPE = $Config->{$councilCode}->{'TYPE'};
	my $WEEKLY_LIST_CONTENT_REGEX = $Config->{$councilCode}->{'WEEKLY_LIST_CONTENT_REGEX'};
	my $WEEKLY_LIST_REGEX = $Config->{$councilCode}->{'WEEKLY_LIST_REGEX'};
	my $WEEKLY_LIST_DOMAIN = $Config->{$councilCode}->{'WEEKLY_LIST_DOMAIN'};
	my $NEXT_PAGE_REGEX = $Config->{$councilCode}->{'NEXT_PAGE_REGEX'};
	my $NEXT_PAGE_POST_CONTENT = $Config->{$councilCode}->{'NEXT_PAGE_POST_CONTENT'};
	my $TOTAL_PAGES_REGEX = $Config->{$councilCode}->{'TOTAL_PAGES_REGEX'};
	my $APPLICATION_REGEX = $Config->{$councilCode}->{'APPLICATION_REGEX'};
	my $APPLICATION_URL_DOMAIN = $Config->{$councilCode}->{'APPLICATION_URL_DOMAIN'};
	my $SSL_VERIFICATION = $Config->{$councilCode}->{'SSL_VERIFICATION'};
	my $HOST = $Config->{$councilCode}->{'HOST'};

	my $FromDate = $defaultDateRanges{$rangeName}[0];
	my $ToDate = $defaultDateRanges{$rangeName}[1];
		
	print "FromDate:$FromDate\n";
	print "ToDate:$ToDate\n";
	
	my $checkResult = &defaultRangeCheck(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1], $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);
	my (@totalApplicationLinks,$deDupRedisQuery,$pageSearchCode, $actualSearchCount);
	print "checkResult==>$checkResult\n";
	if ($checkResult == 1)
	{
		print "Search Page Response::$checkResult(Expected value also 1).\n";
		($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
		my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($rangeName, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);		
		@totalApplicationLinks = @$recievedApplicationLinks;
		$pageSearchCode = $responseCode;
		$actualSearchCount = $SearchCount;
		$deDupRedisQuery = $deDupQuery;
	}
	else
	{
		print "Search Page Response::$checkResult(Expected value is 1).\n";
		my %subRanges = &calculateSubRanges($divCounter, @{$defaultDateRanges{$rangeName}}[1],$rangeName, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);
		my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &subRangesSearchResult(\%subRanges, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);		
		@totalApplicationLinks = @$recievedApplicationLinks;
		$pageSearchCode = $responseCode;	
		$actualSearchCount = $SearchCount;		
		$deDupRedisQuery = $deDupQuery;			
	}

	my $totalLinksCount =@totalApplicationLinks;
	print "Total Application Links Count for the date range ".$FromDate->dmy('/')." to ".$ToDate->dmy('/')." is \:\t$actualSearchCount\n";
	
	print "\nBut valid count for this date range is \:\t$totalLinksCount\n";
	
	my ($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@totalApplicationLinks,$rangeName, $COUNCIL_NAME);	
	$insertQuery=~s/\,\s*$//is;
	my $insertDetailsQuery = "insert into Ireland_Council_WPHData (Council_Code, Council_Name, Application_URL, Schedule_Date, Planning_Application_Ref, Application_Date, Registration_Date, Decision_Date, Application_Type, Extension_of_time_to, Main_Location, Proposal, Full_Description, Status, Status_Description, Comment, Applicant_Title, Applicant_Forename_Initial, Applicant_Surname_Company_Name, Applicant_Company_Contact_Name, Case_Officer, Agent_Title, Agent_Forename_Initial, Agent_Surname_Company_Name, Agent_Company_Contact_Name, Agent_Address, Decision, Decision_Decision_Date, Publicity_Start_Date, Publicity_End_Date, Type_of_Appeal, Appeal_Lodged_Date, Appeal_Decision, Appeal_Decision_Date, Document_URL, Web_Reference, Final_Grant_Date, Last_date_for_Observations, Submission_Observation) values ".$insertQuery;
	
	if($insertDetailsQuery!~m/values\s*$/is)
	{
		&DB_Insert_Planning($dbh,$insertDetailsQuery);
	}
	
}


####
# Function to calculate GCS sub-ranges and pass the resulting date range into rangeCheckMethod is see the $resultValue results with 1
####

sub calculateSubRanges
{
    my ($divCount, $passedToDate, $gcsRange, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) = @_;
    my (%subRangeCheck, $resultValue);
	
	print "gcsRange==>$gcsRange\n";
	
    do
    {
        %subRangeCheck = &CouncilsScrapperCore::subRangesCalcMethod($divCount, $passedToDate);
        
		my ($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'GCS001'}}[0], @{$subRangeCheck{'GCS001'}}[1]);
		
        $resultValue = &rangeCheckMethod($subRangeFromDate, $subRangeToDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);
		
        $divCount = $divCount + 1;
		if($divCount==5)
		{
			# last;
			return(%subRangeCheck);
		}

    } while ($resultValue == 0);
	
    return(%subRangeCheck);
}

sub DB_Insert_Planning()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);

	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

sub collectApplicationLinks()
{
	my ($Content,$homeURL,$receivedCouncilCode,$appRegex) = @_;
	
	my @appLinks;
	while($Content=~m/$appRegex/gsi)
	{
		my $firstMatch=$1;
		my $secondMatch=$2;
				
		my ($applicationURL,$applicationReference);
		
		$applicationURL = $firstMatch;
		$applicationReference = $secondMatch;
		
		if($applicationURL!~m/^https?/is)
		{
			$applicationURL = URI::URL->new($applicationURL)->abs( $homeURL, 1 );
		}
		
		$applicationURL=~s/\&amp\;/\&/gsi;
		
		# print "applicationURL=>$applicationURL\n"; <STDIN>;
		# print "applicationReference=>$applicationReference\n"; <STDIN>;
				
		if($applicationReference!~m/^\s*$/is)
		{
			my $checkRedis = $receivedCouncilCode."_".$applicationReference;
			
			if($redisConnection->exists($checkRedis))	
			{
				print "Exists : $applicationReference\n";
				my $today = [Today_and_Now()];
				my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
				open(ERR,">>$dupAppDirectory/${receivedCouncilCode}_deDupAppDetails.txt");
				print ERR "$receivedCouncilCode\t$applicationURL\t$todayDateTime\n";
				close ERR;
							
				next;
			}
			else
			{	
				# print "applicationURL=>$applicationURL\n"; <STDIN>;
				$redisConnection->set($receivedCouncilCode."_".$applicationReference => $applicationReference);
				print "Not Exists : $applicationReference\n";
				push ( @appLinks, $applicationURL);	
			}
		}
	}
	
	return (\@appLinks);
}

sub clean()
{
	my ($data2Clean) = @_;
	
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/,\s*(?:,\s*)+/, /igs;
	$data2Clean=~s/^\s*,\s*|\s*,\s*$//igs;
	$data2Clean=~s/\&\#039\;/'/igs;
	$data2Clean=~s/\\r\\n/ /igs;
	$data2Clean=~s/\&\#39\;/\'/igs;
	$data2Clean=~s/\&\#039\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/\s\s+/ /igs;
	
	return($data2Clean);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;
	my $num	= shift;
	
	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
		
		open(ERR,">>$logLocation/success_Query_$num.txt");
		print ERR $Query."\n";
		close ERR;
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	my $today = [Today_and_Now()];
	my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
	if($dbh = DBI->connect("dbi:Sybase:server=172.27.137.184;database=GLENIGAN_IRELAND_COUNCILS", 'User2', 'Merit456'))
	{
		print "\nGlenigan Ireland SERVER CONNECTED\n";
		$Connection_Flag=1;
		if($Recon_Flag_Main > 1)
		{	
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Success$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
		}
	}
	else
	{
		print "\nGlenigan Ireland SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed_$Recon_Flag_Main==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}

##### REDIS CONN ######
sub Redis_connection()
{
	my $redis;
	
	eval{
		$redis = Redis->new(server => '127.0.0.1:6379');
		print "Planning Redis Connected\n";
	};
	if($@)
	{
		print "Planning Redis not Connected\n";
		eval{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = Redis->new(server => '127.0.0.1:6379');
		};
		if($@)
		{
			&send_mail_redis("Redis Connection", "Unable to connect Redis", "Planning");
		}	
	}

	return $redis;
}

####### SENT MAIL ########
sub send_mail_redis()
{
    my $Subject=shift;
    my $Body=shift;
    my $decision=shift;
	
	my $Current_dateTime=localtime();
	
    my $subject="Redis connection failed- $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='parielavarasan.surulinathan@meritgroup.co.uk';
    my $pass='sXNdrc6JU';
    my $body = "Hi Team, \n\n\t\tUnable to connect Redis $decision server.\n\nRegards\nGlenigan Team";
	
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 # Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";

    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}



sub get_cookie_session_details()
{
	my $url = shift;
	my $keyName = shift;
	
	my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
	$mech->get( $url );	
	my $pingStatus = $mech->status;
	# my $responseContent = $mech->content;	
	my $cookie = $monster->get_cookie($keyName);	
	my $sessionID;
	eval
	{
		$sessionID = $cookie->val();
	};
	if($@)
	{
		print "JSESSIONCOOKIEISSUE_REASON==>$@\n";
		$sessionID = "JSESSION_ISSUE";
	}
	
	return($sessionID);
}



####
# Function to check the default date rnages from the GCS schedule needs to be split
####

sub defaultRangeCheck
{
    my ($passedFromDate, $passedToDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) = @_;

    return( &rangeCheckMethod($passedFromDate, $passedToDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) );
}


####
# Function to check the search result page response to see if it returns in error
####

sub rangeCheckMethod
{
    my ($passedFromDate,$passedToDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) = @_;

    ($responseContents,$responseStatus) = &callCouncilsMechMethod($passedFromDate, $passedToDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);
	
    my $searchValue;
    
	if($responseContents=~m/(\d+)\s*matches\s*are\s*currently\s*available\s*to\s*view\.?\s*</is)
	{
		$searchValue = $1;
	}
	elsif($responseContents=~m/Your\s*search\s*returned\s*(\d+)\s*matches/is)
	{
		$searchValue = $1;
	}
	
	if($searchValue=~m/$controlFile->{'gcsControl'}->{'MAX_SEARCH_RESULT'}/mis)
	{
		return(0);
	}
    else
    {
        return(1);
    }
}


####
# Common method to inisialise Mechanize user agent
####

sub callCouncilsMechMethod
{
    my($passedFromDate, $passedToDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) = @_;
	my ($councilAppResponse, $scriptStatus) = &councilsMechMethod($passedFromDate->dmy('/'), $passedToDate->dmy('/'), $councilCode, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);
    return ($councilAppResponse, $scriptStatus);
}

sub councilsMechMethod
{
	my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT ) = @_;
	
	print "\nFromDate==>$receivedFromDate\tToDate==>$receivedToDate\n";
	
	print "HOME_URL==>$HOME_URL\n";
	
	my $session_id = &get_cookie_session_details($HOME_URL, 'JSESSIONID');
	print "session_id=>$session_id\n"; 
	# <STDIN>;

	$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
	$mech->add_header( 'Accept-Encoding' => 'gzip, deflate');
	$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
	$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
	$mech->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	$mech->add_header( 'Host' => $HOST);
	$mech->add_header( 'Referer' => $HOME_URL);
	$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');


	$receivedFromDate=~s/\//\%2F/gsi;
	$receivedToDate=~s/\//\%2F/gsi;

	$SEARCH_POST_CONTENT=~s/<FromDate>/$receivedFromDate/gsi;
	$SEARCH_POST_CONTENT=~s/<ToDate>/$receivedToDate/gsi;

	$mech->post($SEARCH_URL, Content => "$SEARCH_POST_CONTENT");
	my $Content = $mech->content;
	my $pingStatus = $mech->status;
	# open(DC,">Home_Content.html");
	# print DC $Content;
	# close DC;
	# exit;
	
	return ($Content, $pingStatus);
}




####
# Function to pass GCS sub-ranges dates to fetch search result
####

sub subRangesSearchResult
{
    my ($subRangesParam, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) = @_;
    my %subRanges = %$subRangesParam;
	
	my (@recAppLinks, $deDupQuery, $recAppLink, $responseCode, $SearchCount);
    foreach my $subRangeName (sort keys %subRanges)
    {
        ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$subRanges{$subRangeName}}[0], @{$subRanges{$subRangeName}}[1]);
        ($recAppLink, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($subRangeName, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT); 
		my @recAppLink = @$recAppLink;	
		push(@recAppLinks,@recAppLink);
    }

    return(\@recAppLinks, $deDupQuery, $responseCode, $SearchCount);
}

####
# Function to match the search result count from response
####

sub fetchSearchResult
{
    my($passedsubRangeName, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT) = @_;
    my($difference);

	my @totalURL;
    $difference = $toDate->delta_days( $fromDate );
    $scheduleCounter = $scheduleCounter + $difference->delta_days;
    print "GCS::$passedsubRangeName ==> FromDate::".$fromDate->dmy('/')." to ToDate::".$toDate->dmy('/')." ~ NoOfDaysRange::".$difference->delta_days."\t";
    ($responseContents,$responseStatus) = &callCouncilsMechMethod($fromDate, $toDate, $HOST,  $HOME_URL,  $SEARCH_URL,  $SEARCH_POST_CONTENT);
	
    # Get number of results
    $searchResultCount = $1 if($responseContents =~ m/>\s*Your\s*search\s*returned\s*(\d+)\s*matches\s*/is);
		
    my $paginationContents = &paginationResponse($councilCode, $responseContents);
	
	if($searchResultCount eq "")
	{
		my $resultcnt=0;
		while($paginationContents=~m/$Config->{$councilCode}->{'APPLICATION_REGEX'}/igs)
		{
			$resultcnt++;
		}
		
		$searchResultCount = $resultcnt;
	}
    print "SearchResult: $searchResultCount\n";
	
	my ($totalURL, $duplicateQuery) = &getApplicationLinks($paginationContents, $passedsubRangeName);
	@totalURL=@$totalURL;
	
    return(\@totalURL, $duplicateQuery, $responseStatus, $searchResultCount);
}


####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationLinks()
{
	my $totalContent = shift;
	my $gcsRange = shift;
		
	my (@appLinks, $deDup_Query); 
	
	while($totalContent =~ m/$Config->{$councilCode}->{'APPLICATION_REGEX'}/igs)
	{
		my $Applink = $1;
		my $ApplicationNumber = $2;
				
		# print "Before::$Applink\n";
		my $fullAppPageLink;		
		if($Applink!~m/https?\:/is)
		{
			$fullAppPageLink = URI::URL->new($Applink)->abs( $Config->{$councilCode}->{'URL'}, 1 );
		}	
		else
		{
			$fullAppPageLink = $Applink;
		}	
		$fullAppPageLink =~ s/\&amp\;/\&/g;
		$fullAppPageLink =~s/\'/\'\'/igs;
		
		# print "After::$fullAppPageLink\n"; 
		# <STDIN>;

		
		if($redisConnection->exists($councilCode."_".$ApplicationNumber))	
		{
			print "Exists : $ApplicationNumber\n";	
			
			next;
		}
	
		
		push ( @appLinks, $fullAppPageLink);
	}
	
	# print "deDup_Query==>$deDup_Query\n"; <STDIN>;
	
	return(\@appLinks, $deDup_Query);
}



####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationDetails()
{
    my $passedApplicationLink = shift;
    my $rangeName = shift;
    my $councilName = shift;
	
	my @passedApplicationLinks = @{$passedApplicationLink};


	my $insertQuery;
	
	print "GCS==>$rangeName\n";
	
	my $resultsCount=0;
	my ($fullAppPageLink);
    foreach my $eachApplicationLink (@passedApplicationLinks)
    {		
		$fullAppPageLink=$eachApplicationLink;
		# print "Going to fetch ==> $fullAppPageLink\n";
		my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &CouncilsScrapperCore::getApplicationDetails($fullAppPageLink, $mech, $councilCode);
		sleep(2);
		
		if(($applicationNumber ne "") && ($pageResponse=~m/^\s*(200|ok)\s*$/is))
		{
			$resultsCount++;
			
			## Insert new application in redis server			
			$redisConnection->set($councilCode."_".$applicationNumber => $applicationNumber);
			print "Not Exists : $applicationNumber==>$resultsCount\n";
		}
		
		my $tempSource = $rangeName;
		
		if($applicationNumber ne "")
		{
			my ($insert_query_From_Func) = &CouncilsScrapperCore::applicationsDataDumpPlanning($applicationDetailsPageContent,$fullAppPageLink,$tempSource,$councilCode,$councilName,$scheduleDate,$mech);
			
			$insertQuery.=$insert_query_From_Func;
		}
    }
	
	# print "$resultsCount\n"; 
		
	return($insertQuery, $resultsCount);
}




####
# Function to fetch pagenation response for resulting search result page
####

sub paginationResponse
{	
	my ($councilCode, $responseContents) = @_;
	
    $responseContents = &CouncilsScrapperCore::searchPageNext($councilCode, $mech, $responseContents);
		
    return($responseContents);
}