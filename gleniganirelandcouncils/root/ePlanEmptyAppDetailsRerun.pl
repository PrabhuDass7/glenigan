use strict;
use WWW::Mechanize;
use IO::Socket::SSL;
use URI::Escape;
use URI::URL;
use Config::Tiny;
use MIME::Lite;
use DBI;
use DBD::ODBC;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;

# Establish connection with DB server
my $dbh = &DbConnection();

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
my $todayDate = $dt->ymd('');

print "ScheduleDate==>$scheduleDate\n";
print "todayDate==>$todayDate\n";

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );
						 

####
# Get input
####
my $inputQuery = "select council_code,Application_URL from Ireland_Council_Data where [FILE Number] = \'\'";

print "InputQuery==>$inputQuery\n";

my $sth;
eval {
	$sth= $dbh->prepare($inputQuery);
	$sth->execute();
};

my (@applicationLinks,@conCode);
while(my @record = $sth->fetchrow)
{
	push(@conCode,Trim($record[0]));
	push(@applicationLinks,Trim($record[1]));
}

$sth->finish();

####
# Create new object of class Config::Tiny
####
my ($Config,$ConfigRegex) = Config::Tiny->new();
####
# Read INI files
####
my $RegexCtrlDetailsFile = Config::Tiny->read($ConfigDirectory.'/ePlan_Alpha_Council_Details.ini');
my $RegexCtrlFile = Config::Tiny->read($ConfigDirectory.'/ePlan_Regex_Heap.ini');


my $applicationCount=0;
for(my $Category=0; $Category<@applicationLinks; $Category++)
{
	my $councilCode = $conCode[$Category];	
	my $appPageURL = $applicationLinks[$Category];

	print "$applicationCount<==>$appPageURL\n";
	# <STDIN>;

	my $Recon = 1;
	ReTry:
	$mech->get($appPageURL);
	my $appPageContent = $mech->content;
	# if($Recon<=3)
	# {
		# open(DC,">appPageContent.html");
		# print DC $appPageContent;
		# close DC;
		# exit;
	# }
	
	my $File_Number = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'FILE_NUMBER'}/is);
	my $File_Number_New = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'FILE_NUMBER_NEW'}/is);
	my $Application_Type = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	my $Planning_Status = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'PLANNING_STATUS'}/is);
	my $Received_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	my $Decision_Due_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DECISION_DUE_DATE'}/is);
	my $Validated_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'VALIDATED_DATE'}/is);
	my $Invalidated_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'INVALIDATED_DATE'}/is);
	my $Further_Info_Requested = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'FURTHER_INFO_REQUESTED'}/is);
	my $Further_Info_Received = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'FURTHER_INFO_RECEIVED'}/is);
	my $Withdrawn_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'WITHDRAWN_DATE'}/is);
	my $Extend_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'EXTEND_DATE'}/is);
	my $Decision_Type = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DECISION_TYPE'}/is);
	my $Decision_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DECISION_DATE'}/is);
	my $Leave_to_Appeal = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'LEAVE_TO_APPEAL'}/is);
	my $Appeal_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_DATE'}/is);
	my $Commenced_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'COMMENCED_DATE'}/is);
	my $Submissions_By = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'SUBMISSIONS_BY'}/is);
	my $Grid_Northings = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'GRID_NORTHINGS'}/is);
	my $Grid_Eastings = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'GRID_EASTINGS'}/is);
	my $Site_Area = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'SITE_AREA'}/is);
	my $Floor_Area = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'FLOOR_AREA'}/is);
	my $Number_of_Units_Developed = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'NUMBER_OF_UNITS_DEVELOPED'}/is);
	my $Number_of_Floors = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'NUMBER_OF_FLOORS'}/is);
	my $Sewerage_Type = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'SEWERAGE_TYPE'}/is);
	my $Water_Type = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'WATER_TYPE'}/is);
	my $National_Grid_Reference = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'NATIONAL_GRID_REFERENCE'}/is);
	my $Road_Number = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'ROAD_NUMBER'}/is);
	my $OS_Map_Ref_Number = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'OS_MAP_REF_NUMBER'}/is);
	my $Control_Officer = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'CONTROL_OFFICER'}/is);
	my $Advertisement_Size = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'ADVERTISEMENT_SIZE'}/is);
	my $ESB_Cable_Length = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'ESB_CABLE_LENGTH'}/is);
	my $Development_LEA = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DEVELOPMENT_LEA'}/is);
	my $GEO_Code = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'GEO_CODE'}/is);
	my $Functional_Area = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'FUNCTIONAL_AREA'}/is);
	my $Num_Houses_Units = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'NUM_HOUSES_UNITS'}/is);
	my $Applicant_name = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPLICANT_NAME'}/is);
	my $Applicant_Address = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
	my $Applicant_Phone_Number = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPLICANT_PHONE_NUMBER'}/is);
	my $Applicant_Corresp_Address = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPLICANT_CORRESP_ADDRESS'}/is);
	my $Applicant_Fax_Number = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPLICANT_FAX_NUMBER'}/is);
	my $Development_Description = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DEVELOPMENT_DESCRIPTION'}/is);
	my $Development_Address = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DEVELOPMENT_ADDRESS'}/is);
	my $Architect_Name = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'ARCHITECT_NAME'}/is);
	my $Location_Key = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'LOCATION_KEY'}/is);
	my $Electoral_Division = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'ELECTORAL_DIVISION'}/is);
	my $Planner = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'PLANNER'}/is);
	my $Social_Housing_Exempt = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'SOCIAL_HOUSING_EXEMPT'}/is);
	my $Plan_Enforcement1 = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'PLAN_ENFORCEMENT1'}/is);
	my $IPC_Licence_Required = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'IPC_LICENCE_REQUIRED'}/is);
	my $Waste_Licence_Required = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'WASTE_LICENCE_REQUIRED'}/is);
	my $Protected_Structure = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'PROTECTED_STRUCTURE'}/is);
	my $Protected_Structure1 = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'PROTECTED_STRUCTURE1'}/is);
	my $Development_Name = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DEVELOPMENT_NAME'}/is);
	my $Decision_DecisionDate = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DECISION_DECISIONDATE'}/is);
	my $Manager_Order = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'MANAGER_ORDER'}/is);
	my $Decision_Decision_Type = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DECISION_DECISION_TYPE'}/is);
	my $Number_of_Conditions = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'NUMBER_OF_CONDITIONS'}/is);
	my $Grant_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'GRANT_DATE'}/is);
	my $Grant_Managers_Order1 = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'GRANT_MANAGERS_ORDER1'}/is);
	my $Section_47_Apply = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'SECTION_47_APPLY'}/is);
	my $Part_5_Apply = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'PART_5_APPLY'}/is);
	my $Expiry_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'EXPIRY_DATE'}/is);
	my $Decision_Description = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DECISION_DESCRIPTION'}/is);
	my $Appeal_Notification_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_NOTIFICATION_DATE'}/is);
	my $Appeal_BP_Reference1 = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_BP_REFERENCE1'}/is);
	my $Appeal_Type = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_TYPE'}/is);
	my $Appeal_File_Forward_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_FILE_FORWARD_DATE'}/is);
	my $Appeal_Submission_Due_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_SUBMISSION_DUE_DATE'}/is);
	my $Appeal_Submission_Sent_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_SUBMISSION_SENT_DATE'}/is);
	my $Appeal_Decision = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_DECISION'}/is);
	my $Appeal_Decision_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_DECISION_DATE'}/is);
	my $Appeal_Withdrawn_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_WITHDRAWN_DATE'}/is);
	my $Appeal_Dismissed_Date = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_DISMISSED_DATE'}/is);
	my $Appeal_Reason = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'APPEAL_REASON'}/is);
	my $Comments_Significant_Case_Flag = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'COMMENTS_SIGNIFICANT_CASE_FLAG'}/is);
	my $Comments = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'COMMENTS'}/is);
	my $Agent_Name = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'AGENT_NAME'}/is);
	my $Agent_Address = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
	my $Agent_Phone = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'AGENT_PHONE'}/is);
	my $Agent_Fax = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'AGENT_FAX'}/is);
	my $Agent_Email = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'AGENT_EMAIL'}/is);
	my $Document_Url = &clean($1) if($appPageContent=~m/$RegexCtrlFile->{$councilCode}->{'DOCUMENT_URL'}/is);
	
	if($Document_Url!~m/^https?/is)
	{
		$Document_Url = URI::URL->new($Document_Url)->abs( $RegexCtrlDetailsFile->{$councilCode}->{'URL'}, 1 );
	}
		
	if($File_Number eq "")
	{
		if($File_Number_New ne "")
		{
			$File_Number = $File_Number_New;
		}
	}
	if(($File_Number eq "") && ($Recon<=3))
	{	
		print "Reconnecting..\n";
		$Recon++;
		goto ReTry;
	}	
	
	print "File_Number==>$File_Number\n";
	
    my $updateQuery = "UPDATE Ireland_Council_Data SET [File Number] = \'$File_Number\', [Application Type] = \'$Application_Type\', [Planning Status] = \'$Planning_Status\', [Received Date] = \'$Received_Date\', [Decision Due Date] = \'$Decision_Due_Date\', [Validated Date] = \'$Validated_Date\', [Invalidated Date] = \'$Invalidated_Date\', [Further Info Requested] = \'$Further_Info_Requested\', [Further Info Received] = \'$Further_Info_Received\', [Withdrawn Date] = \'$Withdrawn_Date\', [Extend Date] = \'$Extend_Date\', [Decision Type] = \'$Decision_Type\', [Decision Date] = \'$Decision_Date\', [Leave to Appeal] = \'$Leave_to_Appeal\', [Appeal Date] = \'$Appeal_Date\', [Commenced Date] = \'$Commenced_Date\', [Submissions By] = \'$Submissions_By\', [Grid Northings] = \'$Grid_Northings\', [Grid Eastings] = \'$Grid_Eastings\', [Site Area] = \'$Site_Area\', [Floor Area] = \'$Floor_Area\', [Number of Units Developed] = \'$Number_of_Units_Developed\', [Number of Floors] = \'$Number_of_Floors\', [Sewerage Type] = \'$Sewerage_Type\', [Water Type] = \'$Water_Type\', [National Grid Reference] = \'$National_Grid_Reference\', [Road Number] = \'$Road_Number\', [OS Map Ref. Number] = \'$OS_Map_Ref_Number\', [Control Officer] = \'$Control_Officer\', [Advertisement Size] = \'$Advertisement_Size\', [ESB Cable Length] = \'$ESB_Cable_Length\', [Development LEA] = \'$Development_LEA\', [GEO Code] = \'$GEO_Code\', [Functional Area] = \'$Functional_Area\', [Num Houses/Units] = \'$Num_Houses_Units\', [Applicant name] = \'$Applicant_name\', [Applicant Address] = \'$Applicant_Address\', [Applicant Phone Number] = \'$Applicant_Phone_Number\', [Applicant Corresp. Address] = \'$Applicant_Corresp_Address\', [Applicant Fax Number] = \'$Applicant_Fax_Number\', [Development Description] = \'$Development_Description\', [Development Address] = \'$Development_Address\', [Architect Name] = \'$Architect_Name\', [Location Key] = \'$Location_Key\', [Electoral Division] = \'$Electoral_Division\', [Planner] = \'$Planner\', [Social Housing Exempt] = \'$Social_Housing_Exempt\', [Plan Enforcement1] = \'$Plan_Enforcement1\', [IPC Licence Required] = \'$IPC_Licence_Required\', [Waste Licence Required] = \'$Waste_Licence_Required\', [Protected Structure] = \'$Protected_Structure\', [Protected Structure1] = \'$Protected_Structure1\', [Development Name] = \'$Development_Name\', [Decision_DecisionDate] = \'$Decision_DecisionDate\', [Manager Order] = \'$Manager_Order\', [Decision_Decision Type] = \'$Decision_Decision_Type\', [Number of Conditions] = \'$Number_of_Conditions\', [Grant Date] = \'$Grant_Date\', [Grant Managers Order1] = \'$Grant_Managers_Order1\', [Section 47 Apply?] = \'$Section_47_Apply\', [Part 5 Apply?] = \'$Part_5_Apply\', [Expiry Date] = \'$Expiry_Date\', [Decision Description] = \'$Decision_Description\', [Appeal Notification Date] = \'$Appeal_Notification_Date\', [Appeal BP Reference1] = \'$Appeal_BP_Reference1\', [Appeal Type] = \'$Appeal_Type\', [Appeal File Forward Date] = \'$Appeal_File_Forward_Date\', [Appeal Submission Due Date] = \'$Appeal_Submission_Due_Date\', [Appeal Submission Sent Date] = \'$Appeal_Submission_Sent_Date\', [Appeal Decision] = \'$Appeal_Decision\', [Appeal Decision Date] = \'$Appeal_Decision_Date\', [Appeal Withdrawn Date] = \'$Appeal_Withdrawn_Date\', [Appeal Dismissed Date] = \'$Appeal_Dismissed_Date\', [Appeal Reason] = \'$Appeal_Reason\', [Comments Significant Case Flag] = \'$Comments_Significant_Case_Flag\', [Comments] = \'$Comments\', [Agent Name] = \'$Agent_Name\', [Agent Address] = \'$Agent_Address\', [Agent Phone] = \'$Agent_Phone\', [Agent FaX] = \'$Agent_Fax\', [Agent Email] = \'$Agent_Email\', [Document URL] = \'$Document_Url\' WHERE [FILE Number] = \'\' and Council_Code = \'$councilCode\' and Application_URL = \'$appPageURL\';";	
	
	if($File_Number ne "")
	{
		print "$updateQuery\n";
		&DB_Insert($dbh,$updateQuery);	
	}
	
	$applicationCount++;
}






sub clean()
{
	my ($data2Clean) = @_;
	
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/,\s*(?:,\s*)+/, /igs;
	$data2Clean=~s/^\s*,\s*|\s*,\s*$//igs;
	$data2Clean=~s/\&\#039\;/'/igs;
	$data2Clean=~s/\\r\\n/ /igs;
	$data2Clean=~s/\s\s+/ /igs;
	
	return($data2Clean);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;
	
	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_UpdateQuery.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	my $today = [Today_and_Now()];
	my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
	if($dbh = DBI->connect("dbi:Sybase:server=172.27.137.184;database=GLENIGAN_IRELAND_COUNCILS", 'User2', 'Merit456'))
	{
		print "\nGlenigan Ireland SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nGlenigan Ireland SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}


###### Trim the given text ######
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}