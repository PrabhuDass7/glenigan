use strict;
use Redis;
use DBI;
use DBD::ODBC;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


my $dbh = &DbConnection();
my $redis = Redis->new(host => '127.0.0.1', port => 6379);

my $COUNCIL_CODE = $ARGV[0];

if($COUNCIL_CODE eq "")
{
	print "Input Council Code was missing..\n";
	exit;
}

my $Select_Query="select [File Number] from Ireland_Council_Data where Council_code = \'$COUNCIL_CODE\'";
my $sth = $dbh->prepare($Select_Query);

$sth->execute();
my (@APPLICATION_NO);
my $reccount;
while(my @record = $sth->fetchrow)
{
	my $APPLICATION_NO = &Trim($record[0]);
	print "${COUNCIL_CODE}_${APPLICATION_NO} => $APPLICATION_NO\n";
	$redis->set($COUNCIL_CODE."_".$APPLICATION_NO => $APPLICATION_NO);
}
$sth->finish();



sub Trim
{
	my $txt = shift;
	$txt =~ s/^\s+|\s+$//igs;
	$txt =~ s/^\n+/\n/igs;
	return $txt;
}

###### DB Connection ####
sub DbConnection()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	my $today = [Today_and_Now()];
	my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
	if($dbh = DBI->connect("dbi:Sybase:server=172.27.137.184;database=GLENIGAN_IRELAND_COUNCILS", 'User2', 'Merit456'))
	{
		print "\nGlenigan Ireland SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nGlenigan Ireland SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}

