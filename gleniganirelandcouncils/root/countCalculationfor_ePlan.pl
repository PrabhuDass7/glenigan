use strict;
use WWW::Mechanize;
use IO::Socket::SSL;
use URI::Escape;
use URI::URL;
use Config::Tiny;
use MIME::Lite;
use DBI;
use DBD::ODBC;
use File::Basename;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;
use Redis;

# Establish connection with DB server
my $dbh = &DbConnection();

####
# Create new object of class Redis_connection
####
my $redisConnection = &Redis_connection();

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
my $todayDate = $dt->ymd('');

print "ScheduleDate==>$scheduleDate\n";
print "todayDate==>$todayDate\n";

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');

my $logLocation = $logsDirectory.'/FailedQuery/'.$todayDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

my $dbFailedLogLocation = $logsDirectory.'/dbConnFailedQuery/'.$todayDate;
my $appCounttDetails = $logsDirectory.'/ApplicationCountDetails/'.$todayDate;
my $dupAppDirectory = $logsDirectory.'/deDupApplicationDetails/'.$todayDate;

unless ( -d $dbFailedLogLocation )
{
	make_path($dbFailedLogLocation);
}

unless ( -d $appCounttDetails )
{
	make_path($appCounttDetails);
}

unless ( -d $dupAppDirectory )
{
	make_path($dupAppDirectory);
}


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );
						 
my $Council_Code = $ARGV[0];

### Get Council Details from ini file ###
my ($Config,$ConfigRegex) = Config::Tiny->new();
$Config = Config::Tiny->read( $ConfigDirectory.'/ePlan_Alpha_Council_Details.ini');
$ConfigRegex = Config::Tiny->read( $ConfigDirectory.'/ePlan_Regex_Heap.ini');

my @alphabet = ('A'..'Z');
foreach my $alpha (@alphabet) {	
	print "Now Alphabet $alpha is started processing\n";
	my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
	my $HOME_URL = $Config->{$Council_Code}->{'URL'};
	my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
	my $SEARCH_POST_CONTENT = $Config->{$Council_Code}->{'SEARCH_POST_CONTENT'};
	my $TOKEN_REGEX = $Config->{$Council_Code}->{'TOKEN_REGEX'};
	my $SEARCH_EVENTARGUMENT = $Config->{$Council_Code}->{'SEARCH_EVENTARGUMENT'};
	my $SEARCH_EVENTTARGET = $Config->{$Council_Code}->{'SEARCH_EVENTTARGET'};
	my $SEARCH_VIEWSTATE_REGEX = $Config->{$Council_Code}->{'SEARCH_VIEWSTATE_REGEX'};
	my $SEARCH_VIEWSTATEGENERATOR = $Config->{$Council_Code}->{'SEARCH_VIEWSTATEGENERATOR'};
	my $SEARCH_EVENTVALIDATION = $Config->{$Council_Code}->{'SEARCH_EVENTVALIDATION'};
	my $SEARCH_PREVIOUSPAGE = $Config->{$Council_Code}->{'SEARCH_PREVIOUSPAGE'};
	my $TYPE = $Config->{$Council_Code}->{'TYPE'};
	my $WEEKLY_LIST_CONTENT_REGEX = $Config->{$Council_Code}->{'WEEKLY_LIST_CONTENT_REGEX'};
	my $WEEKLY_LIST_REGEX = $Config->{$Council_Code}->{'WEEKLY_LIST_REGEX'};
	my $WEEKLY_LIST_DOMAIN = $Config->{$Council_Code}->{'WEEKLY_LIST_DOMAIN'};
	my $NEXT_PAGE_REGEX = $Config->{$Council_Code}->{'NEXT_PAGE_REGEX'};
	my $NEXT_PAGE_POST_CONTENT = $Config->{$Council_Code}->{'NEXT_PAGE_POST_CONTENT'};
	my $TOTAL_PAGES_REGEX = $Config->{$Council_Code}->{'TOTAL_PAGES_REGEX'};
	my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
	my $APPLICATION_URL_DOMAIN = $Config->{$Council_Code}->{'APPLICATION_URL_DOMAIN'};
	my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
	my $HOST = $Config->{$Council_Code}->{'HOST'};

	$mech->get( $HOME_URL );
	my $getContent = $mech->content;
	# open(DC,">Home_Content.html");
	# print DC $getContent;
	# close DC;
	# exit;
	my $token = uri_escape($1) if($getContent=~m/$TOKEN_REGEX/is);
	my $previousPage = uri_escape($1) if($getContent=~m/$SEARCH_PREVIOUSPAGE/is);
	my $eventValidation = uri_escape($1) if($getContent=~m/$SEARCH_EVENTVALIDATION/is);
	my $viewStateGen = uri_escape($1) if($getContent=~m/$SEARCH_VIEWSTATEGENERATOR/is);
	my $viewState = uri_escape($1) if($getContent=~m/$SEARCH_VIEWSTATE_REGEX/is);
	my $eventTarget = uri_escape($1) if($getContent=~m/$SEARCH_EVENTTARGET/is);
	my $eventArgument = uri_escape($1) if($getContent=~m/$SEARCH_EVENTARGUMENT/is);
	

	$SEARCH_POST_CONTENT=~s/<TOKEN>/$token/is;
	$SEARCH_POST_CONTENT=~s/<ALPHA>/$alpha/is;
	$SEARCH_POST_CONTENT=~s/<EVENTARGUMENT>/$eventArgument/is;
	$SEARCH_POST_CONTENT=~s/<VIEWSTATE>/$viewState/is;
	$SEARCH_POST_CONTENT=~s/<VIEWSTATEGENERATOR>/$viewStateGen/is;
	$SEARCH_POST_CONTENT=~s/<PREVIOUSPAGE>/$previousPage/is;
	$SEARCH_POST_CONTENT=~s/<EVENTVALIDATION>/$eventValidation/is;	
	if($eventTarget eq "")
	{
		$SEARCH_POST_CONTENT=~s/<EVENTTARGET>/ctl00%24MainContentPlaceHolder%24SearchButton/is;
	}
	else
	{
		$SEARCH_POST_CONTENT=~s/<EVENTTARGET>/$eventTarget/is;
	}
	
	$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
	$mech->add_header( 'Accept-Encoding' => 'gzip, deflate');
	$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
	$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
	$mech->add_header( 'Host' => $HOST);
	$mech->add_header( 'Referer' => $SEARCH_URL);
	$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');
	
	$mech->post($SEARCH_URL, Content=> $SEARCH_POST_CONTENT);
	my $Content = $mech->content;
	# print $mech->status()."\n";
	
	my @totalAppLinks;
	my ($totalLinks) = &collectApplicationLinks($Content,$HOME_URL,$Council_Code,$APPLICATION_REGEX);
	my @AppLinks = @{$totalLinks};
	push(@totalAppLinks,@AppLinks);
	
	my $pageCount = $1 if($Content=~m/$TOTAL_PAGES_REGEX/is);
	if($pageCount ne "")
	{
		my $subCount = $pageCount - 1;
		# print "subCount==>$subCount\n";
		my $subAppCount = $subCount * 10;
			
		my $nextPageURL = $SEARCH_URL."/Default/".$pageCount;
		# print "$nextPageURL\n";
		
		my @numArray = (2..$pageCount);
		foreach my $num(@numArray)
		{
			my $nextPageURL = $SEARCH_URL."/Default/".$num;
			print "$num<==>$nextPageURL\n";
			
			my $nextPageGetcount = 1;
			GetNextPageAgain:
			if($Council_Code=~m/^(1127)$/is)
			{	
				$NEXT_PAGE_POST_CONTENT=~s/<PAGENUM>/$num/is;
				$NEXT_PAGE_POST_CONTENT=~s/<VIEWSTATE>/$viewState/is;
				$NEXT_PAGE_POST_CONTENT=~s/<VIEWSTATEGENERATOR>/$viewStateGen/is;
				$NEXT_PAGE_POST_CONTENT=~s/<EVENTVALIDATION>/$eventValidation/is;	
				$mech->post($SEARCH_URL, Content=> $NEXT_PAGE_POST_CONTENT);
			}
			else
			{		
				$mech->get($nextPageURL);
			}
			my $nextPageContent = $mech->content;
			# open(DC,">Home_Content.html");
			# print DC $nextPageContent;
			# close DC;
			# exit;
			
			if($nextPageContent=~m/$APPLICATION_REGEX/si)
			{		
				my ($totalLinks) = &collectApplicationLinks($nextPageContent,$HOME_URL,$Council_Code,$APPLICATION_REGEX);
				my @AppLinks = @{$totalLinks};
				push(@totalAppLinks,@AppLinks);
				
				if($num eq $pageCount)
				{
					my $count = 0;
					while($nextPageContent=~m/$APPLICATION_REGEX/gsi)
					{
						$count++;
					}	
					
					my $finalCount = $subAppCount + $count;
					print "$alpha: Actual page count is $pageCount and final count is $finalCount\n";
					
					open(DC,">>$appCounttDetails/CountDetail_$Council_Code.txt");
					print DC "$alpha\t$pageCount\t$finalCount\n";
					close DC;
				}
			}
			else
			{
				if($nextPageGetcount<=3)
				{
					$nextPageGetcount++;
					goto GetNextPageAgain;
				}				
			}
		}
		
		# Open a file named "output.txt"; die if there's an error
		open my $fh, '>>', "$appCounttDetails/${Council_Code}_fullURL_Lists_$alpha.txt" or die "Cannot open full url lists.txt: $!";

		# Loop over the array
		foreach (@totalAppLinks)
		{
			print $fh "$_\n"; # Print each entry in our array to the file
		}
		close $fh; # Not necessary, but nice to do
		
		my $balanceInsertQuery = &collectApplicationDetails(\@totalAppLinks, $scheduleDate, $Config, $ConfigRegex, $Council_Code, $COUNCIL_NAME, $HOST);
		&DB_Insert($dbh,$balanceInsertQuery,$alpha);
	}
}

sub collectApplicationLinks()
{
	my ($Content,$homeURL,$receivedCouncilCode,$appRegex) = @_;
	
	my @appLinks;
	while($Content=~m/$appRegex/gsi)
	{
		my $firstMatch=$1;
		my $secondMatch=$2;
				
		my ($applicationURL,$applicationReference);
		
		$applicationURL = $firstMatch;
		$applicationReference = $secondMatch;
		
		if($applicationURL!~m/^https?/is)
		{
			$applicationURL = URI::URL->new($applicationURL)->abs( $homeURL, 1 );
		}
		
		$applicationURL=~s/\&amp\;/\&/gsi;
		
		# print "applicationURL=>$applicationURL\n"; <STDIN>;
		# print "applicationReference=>$applicationReference\n"; <STDIN>;
				
		if($applicationReference!~m/^\s*$/is)
		{
			my $checkRedis = $receivedCouncilCode."_".$applicationReference;
			
			if($redisConnection->exists($checkRedis))	
			{
				print "Exists : $applicationReference\n";
				my $today = [Today_and_Now()];
				my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
				open(ERR,">>$dupAppDirectory/${receivedCouncilCode}_deDupAppDetails.txt");
				print ERR "$receivedCouncilCode\t$applicationURL\t$todayDateTime\n";
				close ERR;
							
				next;
			}
			else
			{	
				$redisConnection->set($receivedCouncilCode."_".$applicationReference => $applicationReference);
				print "Not Exists : $applicationReference\n";
				push ( @appLinks, $applicationURL);	
			}
		}
	}
	
	return (\@appLinks);
}

sub clean()
{
	my ($data2Clean) = @_;
	
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/,\s*(?:,\s*)+/, /igs;
	$data2Clean=~s/^\s*,\s*|\s*,\s*$//igs;
	$data2Clean=~s/\&\#039\;/'/igs;
	$data2Clean=~s/\\r\\n/ /igs;
	$data2Clean=~s/\&\#39\;/\'/igs;
	$data2Clean=~s/\&\#039\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;	
	$data2Clean=~s/\s\s+/ /igs;
	
	return($data2Clean);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;
	my $num	= shift;
	
	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
		
		open(ERR,">>$logLocation/success_Query_$num.txt");
		print ERR $Query."\n";
		close ERR;
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	my $today = [Today_and_Now()];
	my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);
	
	if($dbh = DBI->connect("dbi:Sybase:server=172.27.137.184;database=GLENIGAN_IRELAND_COUNCILS", 'User2', 'Merit456'))
	{
		print "\nGlenigan Ireland SERVER CONNECTED\n";
		$Connection_Flag=1;
		if($Recon_Flag_Main > 1)
		{	
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Success$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
		}
	}
	else
	{
		print "\nGlenigan Ireland SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed_$Recon_Flag_Main==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}

##### REDIS CONN ######
sub Redis_connection()
{
	my $redis;
	
	eval{
		$redis = Redis->new(server => '127.0.0.1:6379');
		print "Planning Redis Connected\n";
	};
	if($@)
	{
		print "Planning Redis not Connected\n";
		eval{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = Redis->new(server => '127.0.0.1:6379');
		};
		if($@)
		{
			&send_mail_redis("Redis Connection", "Unable to connect Redis", "Planning");
		}	
	}

	return $redis;
}

####### SENT MAIL ########
sub send_mail_redis()
{
    my $Subject=shift;
    my $Body=shift;
    my $decision=shift;
	
	my $Current_dateTime=localtime();
	
    my $subject="Redis connection failed- $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='parielavarasan.surulinathan@meritgroup.co.uk';
    my $pass='sXNdrc6JU';
    my $body = "Hi Team, \n\n\t\tUnable to connect Redis $decision server.\n\nRegards\nGlenigan Team";
	
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 # Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";

    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}


###### collect Application Details ######
sub collectApplicationDetails()
{
	my ($applicationLink, $scheduleDate, $Config, $ConfigRegex, $councilCode, $councilName, $host) = @_;
	my @applicationLink = @{$applicationLink};
	
	# my $insertQuery = "insert into Ireland_Council_Data (Council_Code, Council_Name, Application_URL, Schedule_Date, \[File Number\], \[Application Type\], \[Planning Status\], \[Received Date\], \[Decision Due Date\], \[Validated Date\], \[Invalidated Date\], \[Further Info Requested\], \[Further Info Received\], \[Withdrawn Date\], \[Extend Date\], \[Decision Type\], \[Decision Date\], \[Leave to Appeal\], \[Appeal Date\], \[Commenced Date\], \[Submissions By\], \[Grid Northings\], \[Grid Eastings\], \[Site Area\], \[Floor Area\], \[Number of Units Developed\], \[Number of Floors\], \[Sewerage Type\], \[Water Type\], \[National Grid Reference\], \[Road Number\], \[OS Map Ref\. Number\], \[Control Officer\], \[Advertisement Size\], \[ESB Cable Length\], \[Development LEA\], \[GEO Code\], \[Functional Area\], \[Num Houses\/Units\], \[Applicant name\], \[Applicant Address\], \[Applicant Phone Number\], \[Applicant Corresp\. Address\], \[Applicant Fax Number\], \[Development Description\], \[Development Address\], \[Architect Name\], \[Location Key\], \[Electoral Division\], Planner, \[Social Housing Exempt\], \[Plan Enforcement1\], \[IPC Licence Required\], \[Waste Licence Required\], \[Protected Structure\], \[Protected Structure1\], \[Development Name\], \[Decision_DecisionDate\], \[Manager Order\], \[Decision_Decision Type\], \[Number of Conditions\], \[Grant Date\], \[Grant Managers Order1\], \[Section 47 Apply\?\], \[Part 5 Apply\?\], \[Expiry Date\], \[Decision Description\], \[Appeal Notification Date\], \[Appeal BP Reference1\], \[Appeal Type\], \[Appeal File Forward Date\], \[Appeal Submission Due Date\], \[Appeal Submission Sent Date\], \[Appeal Decision\], \[Appeal Decision Date\], \[Appeal Withdrawn Date\], \[Appeal Dismissed Date\], \[Appeal Reason\], \[Comments Significant Case Flag\], Comments, \[Agent Name\], \[Agent Address\], \[Agent Fax\], \[Agent Phone\], \[Agent Email\], \[Document URL\]) values ";
	my $insertQuery = "insert into Ireland_Council_Data values ";
	my $bulkValuesForQuery='';
	my $applicationCount=0;
	my $number=0;
	foreach my $appPageURL (@applicationLink)
    {
		my $Recon = 1;
		
		ReTry:		
		$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
		$mech->add_header( 'Accept-Encoding' => 'gzip, deflate');
		$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
		$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded');
		$mech->add_header( 'Host' => $host);
		$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');
		
		if($Recon >= 2)
		{
			$mech->get($Config->{$councilCode}->{'URL'});
		}
		$mech->get($appPageURL);
		my $appPageContent = $mech->content;
		# if($Recon<=3)
		# {
			# open(DC,">appPageContent.html");
			# print DC $appPageContent;
			# close DC;
			# exit;
		# }
		
		my $File_Number = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FILE_NUMBER'}/is);
		my $File_Number_New = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FILE_NUMBER_NEW'}/is);
		my $Application_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICATION_TYPE'}/is);
		my $Planning_Status = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PLANNING_STATUS'}/is);
		my $Received_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'RECEIVED_DATE'}/is);
		my $Decision_Due_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DUE_DATE'}/is);
		my $Validated_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'VALIDATED_DATE'}/is);
		my $Invalidated_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'INVALIDATED_DATE'}/is);
		my $Further_Info_Requested = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FURTHER_INFO_REQUESTED'}/is);
		my $Further_Info_Received = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FURTHER_INFO_RECEIVED'}/is);
		my $Withdrawn_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'WITHDRAWN_DATE'}/is);
		my $Extend_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'EXTEND_DATE'}/is);
		my $Decision_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_TYPE'}/is);
		my $Decision_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DATE'}/is);
		my $Leave_to_Appeal = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'LEAVE_TO_APPEAL'}/is);
		my $Appeal_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_DATE'}/is);
		my $Commenced_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'COMMENCED_DATE'}/is);
		my $Submissions_By = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'SUBMISSIONS_BY'}/is);
		my $Grid_Northings = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'GRID_NORTHINGS'}/is);
		my $Grid_Eastings = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'GRID_EASTINGS'}/is);
		my $Site_Area = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'SITE_AREA'}/is);
		my $Floor_Area = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FLOOR_AREA'}/is);
		my $Number_of_Units_Developed = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'NUMBER_OF_UNITS_DEVELOPED'}/is);
		my $Number_of_Floors = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'NUMBER_OF_FLOORS'}/is);
		my $Sewerage_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'SEWERAGE_TYPE'}/is);
		my $Water_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'WATER_TYPE'}/is);
		my $National_Grid_Reference = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'NATIONAL_GRID_REFERENCE'}/is);
		my $Road_Number = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'ROAD_NUMBER'}/is);
		my $OS_Map_Ref_Number = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'OS_MAP_REF_NUMBER'}/is);
		my $Control_Officer = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'CONTROL_OFFICER'}/is);
		my $Advertisement_Size = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'ADVERTISEMENT_SIZE'}/is);
		my $ESB_Cable_Length = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'ESB_CABLE_LENGTH'}/is);
		my $Development_LEA = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DEVELOPMENT_LEA'}/is);
		my $GEO_Code = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'GEO_CODE'}/is);
		my $Functional_Area = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'FUNCTIONAL_AREA'}/is);
		my $Num_Houses_Units = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'NUM_HOUSES_UNITS'}/is);
		my $Applicant_name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_NAME'}/is);
		my $Applicant_Address = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
		my $Applicant_Phone_Number = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_PHONE_NUMBER'}/is);
		my $Applicant_Corresp_Address = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_CORRESP_ADDRESS'}/is);
		my $Applicant_Fax_Number = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPLICANT_FAX_NUMBER'}/is);
		my $Development_Description = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DEVELOPMENT_DESCRIPTION'}/is);
		my $Development_Address = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DEVELOPMENT_ADDRESS'}/is);
		my $Architect_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'ARCHITECT_NAME'}/is);
		my $Location_Key = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'LOCATION_KEY'}/is);
		my $Electoral_Division = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'ELECTORAL_DIVISION'}/is);
		my $Planner = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PLANNER'}/is);
		my $Social_Housing_Exempt = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'SOCIAL_HOUSING_EXEMPT'}/is);
		my $Plan_Enforcement1 = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PLAN_ENFORCEMENT1'}/is);
		my $IPC_Licence_Required = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'IPC_LICENCE_REQUIRED'}/is);
		my $Waste_Licence_Required = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'WASTE_LICENCE_REQUIRED'}/is);
		my $Protected_Structure = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PROTECTED_STRUCTURE'}/is);
		my $Protected_Structure1 = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PROTECTED_STRUCTURE1'}/is);
		my $Development_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DEVELOPMENT_NAME'}/is);
		my $Decision_DecisionDate = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DECISIONDATE'}/is);
		my $Manager_Order = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'MANAGER_ORDER'}/is);
		my $Decision_Decision_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DECISION_TYPE'}/is);
		my $Number_of_Conditions = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'NUMBER_OF_CONDITIONS'}/is);
		my $Grant_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'GRANT_DATE'}/is);
		my $Grant_Managers_Order1 = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'GRANT_MANAGERS_ORDER1'}/is);
		my $Section_47_Apply = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'SECTION_47_APPLY'}/is);
		my $Part_5_Apply = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'PART_5_APPLY'}/is);
		my $Expiry_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'EXPIRY_DATE'}/is);
		my $Decision_Description = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DECISION_DESCRIPTION'}/is);
		my $Appeal_Notification_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_NOTIFICATION_DATE'}/is);
		my $Appeal_BP_Reference1 = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_BP_REFERENCE1'}/is);
		my $Appeal_Type = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_TYPE'}/is);
		my $Appeal_File_Forward_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_FILE_FORWARD_DATE'}/is);
		my $Appeal_Submission_Due_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_SUBMISSION_DUE_DATE'}/is);
		my $Appeal_Submission_Sent_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_SUBMISSION_SENT_DATE'}/is);
		my $Appeal_Decision = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_DECISION'}/is);
		my $Appeal_Decision_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_DECISION_DATE'}/is);
		my $Appeal_Withdrawn_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_WITHDRAWN_DATE'}/is);
		my $Appeal_Dismissed_Date = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_DISMISSED_DATE'}/is);
		my $Appeal_Reason = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'APPEAL_REASON'}/is);
		my $Comments_Significant_Case_Flag = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'COMMENTS_SIGNIFICANT_CASE_FLAG'}/is);
		my $Comments = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'COMMENTS'}/is);
		my $Agent_Name = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_NAME'}/is);
		my $Agent_Address = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_ADDRESS'}/is);
		my $Agent_Phone = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_PHONE'}/is);
		my $Agent_Fax = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_FAX'}/is);
		my $Agent_Email = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'AGENT_EMAIL'}/is);
		my $Document_Url = &clean($1) if($appPageContent=~m/$ConfigRegex->{$councilCode}->{'DOCUMENT_URL'}/is);
		
		if($Document_Url!~m/^https?/is)
		{
			$Document_Url = URI::URL->new($Document_Url)->abs( $Config->{$councilCode}->{'URL'}, 1 );
		}
		
		if($File_Number eq "")
		{
			if($File_Number_New ne "")
			{
				$File_Number = $File_Number_New;
			}
		}
		if(($File_Number eq "") && ($Recon<=3))
		{	
			print "Reconnecting..\n";
			$Recon++;
			goto ReTry;
		}		
		
		print "File_Number==>$File_Number\n";
			
		my $joinValues="(\'$councilCode\', \'$councilName\', \'$appPageURL\', \'$scheduleDate\', \'$File_Number\', \'$Application_Type\', \'$Planning_Status\', \'$Received_Date\', \'$Decision_Due_Date\', \'$Validated_Date\', \'$Invalidated_Date\', \'$Further_Info_Requested\', \'$Further_Info_Received\', \'$Withdrawn_Date\', \'$Extend_Date\', \'$Decision_Type\', \'$Decision_Date\', \'$Leave_to_Appeal\', \'$Appeal_Date\', \'$Commenced_Date\', \'$Submissions_By\', \'$Grid_Northings\', \'$Grid_Eastings\', \'$Site_Area\', \'$Floor_Area\', \'$Number_of_Units_Developed\', \'$Number_of_Floors\', \'$Sewerage_Type\', \'$Water_Type\', \'$National_Grid_Reference\', \'$Road_Number\', \'$OS_Map_Ref_Number\', \'$Control_Officer\', \'$Advertisement_Size\', \'$ESB_Cable_Length\', \'$Development_LEA\', \'$GEO_Code\', \'$Functional_Area\', \'$Num_Houses_Units\', \'$Applicant_name\', \'$Applicant_Address\', \'$Applicant_Phone_Number\', \'$Applicant_Corresp_Address\', \'$Applicant_Fax_Number\', \'$Development_Description\', \'$Development_Address\', \'$Architect_Name\', \'$Location_Key\', \'$Electoral_Division\', \'$Planner\', \'$Social_Housing_Exempt\', \'$Plan_Enforcement1\', \'$IPC_Licence_Required\', \'$Waste_Licence_Required\', \'$Protected_Structure\', \'$Protected_Structure1\', \'$Development_Name\', \'$Decision_DecisionDate\', \'$Manager_Order\', \'$Decision_Decision_Type\', \'$Number_of_Conditions\', \'$Grant_Date\', \'$Grant_Managers_Order1\', \'$Section_47_Apply\', \'$Part_5_Apply\', \'$Expiry_Date\', \'$Decision_Description\', \'$Appeal_Notification_Date\', \'$Appeal_BP_Reference1\', \'$Appeal_Type\', \'$Appeal_File_Forward_Date\', \'$Appeal_Submission_Due_Date\', \'$Appeal_Submission_Sent_Date\', \'$Appeal_Decision\', \'$Appeal_Decision_Date\', \'$Appeal_Withdrawn_Date\', \'$Appeal_Dismissed_Date\', \'$Appeal_Reason\', \'$Comments_Significant_Case_Flag\', \'$Comments\', \'$Agent_Name\', \'$Agent_Address\', \'$Agent_Phone\', \'$Agent_Fax\', \'$Agent_Email\', \'$Document_Url\'),";
		print "$joinValues\n";
		if($applicationCount == 0)
		{
			$bulkValuesForQuery = $insertQuery.$joinValues;
			$applicationCount++;
		}
		elsif($applicationCount == 200)
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			
			if($bulkValuesForQuery!~m/values\s*$/is)
			{
				$bulkValuesForQuery=~s/\,\s*$//is;
				$number++;
				&DB_Insert($dbh,$bulkValuesForQuery,$number);
				$applicationCount = 0;
				$bulkValuesForQuery="";
			}
		}
		else
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			$applicationCount++;
		}
	}
	
	$bulkValuesForQuery=~s/\,\s*$//is;
	
	return ($bulkValuesForQuery);
}