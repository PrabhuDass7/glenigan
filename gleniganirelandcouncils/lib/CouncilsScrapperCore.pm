package CouncilsScrapperCore;
use strict;
use DateTime;
use DateTime::Duration;
use utf8;
use POSIX;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use HTTP::CookieMonster;
use IO::Socket::SSL qw();
use URI::URL;
use File::Path qw/make_path/;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "BasePath==>$basePath\n"; <STDIN>;

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $libDirectory = ($basePath.'/lib');

# print "libDirectory==>$libDirectory\n"; 
# print "ConfigDirectory==>$ConfigDirectory\n";


####
# Create new object of class Config::Tiny
####

my ($controlFile, $Config, $regexFilePlanning) = Config::Tiny->new();

####
# Read INI files from the objects "$controlFile"
# These files contains - Details of councils and control properties for date range clculation repsectively
####
$controlFile = Config::Tiny->read($ConfigDirectory.'/Core_Config.ini' );
$Config = Config::Tiny->read($ConfigDirectory.'/ePlan_Alpha_Council_Details.ini' );
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/ePlan_Regex_Heap.ini');

my $COVERAGE 		    = $controlFile->{'gcsControl'}->{'COVERAGE'};
my $DAYS_IN_A_WEEK	 	= $controlFile->{'gcsControl'}->{'DAYS_IN_A_WEEK'};
my $scrapeStatusTable	= $controlFile->{'databaseTableName'}->{'scrapeStatus'};
my $scrapeDeDupStatus	= $controlFile->{'databaseTableName'}->{'scrapeDeDupStatus'};
my $scheduleStatusTable	= $controlFile->{'databaseTableName'}->{'scheduleStatus'};
my $planningStagingTable= $controlFile->{'databaseTableName'}->{'planningStaging'};
my $planningLiveTable	= $controlFile->{'databaseTableName'}->{'planningLive'};
my $decisionStagingTable= $controlFile->{'databaseTableName'}->{'decisionStaging'};
my $decisionLiveTable	= $controlFile->{'databaseTableName'}->{'decisionLive'};


####
# Calculates the default GCS schedule for the current Date
####

sub defaultGcsRange
{
	my ($currentToDate) = @_;
	my %defaultRangeCalc = &gcsRangeCalc($DAYS_IN_A_WEEK, $COVERAGE/$DAYS_IN_A_WEEK, $currentToDate);
	return(%defaultRangeCalc);
}

####
# Calculates and provides gcs schedule for the given $weekDaysCount and $gcsSchedulesCount
####

sub gcsRangeCalc
{
	my ($weekDaysCount, $gcsSchedulesCount, $startDate) = @_;
	my %gcsRanges;
	print "weekDaysCount::$weekDaysCount<=>gcsSchedulesCount::$gcsSchedulesCount<=>StartDate::$startDate\n";
	my ($calcFromDate, $calcToDate, $gcsRangeName);
	for(my $k = 1; $k <= $gcsSchedulesCount; $k++)
	{
		$k=sprintf("%03d",$k);
		$gcsRangeName="GCS".$k;
				
		if($k == 1)
		{
			$calcToDate = $startDate;
			$calcFromDate = $calcToDate - DateTime::Duration->new( days => $weekDaysCount );
			print "GCS::$gcsRangeName=>>FromDate::$calcFromDate<=>ToDate::$calcToDate\n";
		}
		else
		{
			$calcToDate = $calcFromDate;
			$calcFromDate = $calcToDate - DateTime::Duration->new( days => $weekDaysCount );
			print "GCS::$gcsRangeName=>>FromDate::$calcFromDate<=>ToDate::$calcToDate\n";
		}
        push ( @{ $gcsRanges{$gcsRangeName}}, $calcFromDate, $calcToDate);
	}
	
	return(%gcsRanges);
}


###
# Calculates the "Divider value" for the given search result
# and returns the calculated value
###

sub dividerCalc
{
	my ($divCount) = @_;
	my $dividerVal;
	
	# print "divCount==>$divCount\n";
	
	if($divCount == 1)
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'};
	}
	elsif ($divCount == 2)
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'}*$controlFile->{'gcsControl'}->{'INCREMENTAL'};
	}
	elsif ($divCount == 3)
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'}*$controlFile->{'gcsControl'}->{'INCREMENTAL'}+2;
	}
	else
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'}*$controlFile->{'gcsControl'}->{'INCREMENTAL'}*2;
	}
	# print "dividerVal==>$dividerVal\n";
	
	return ($dividerVal);
}


####
# Calculates the "No. of days for week range" from the obtained divider value
# and returns the calculated value
####

sub weekDaysCalc 
{
	my ($divCal) = @_;
	my $weekDaysVal;
	
	if($DAYS_IN_A_WEEK/$divCal>=$controlFile->{'gcsControl'}->{'MIN_NO_WEEK_DAYS'})
	{
		# $weekDaysVal = ceil($DAYS_IN_A_WEEK/$divCal);
		if($divCal == 4)
		{
			$weekDaysVal = floor($DAYS_IN_A_WEEK/2.5);
		}
		elsif($divCal == 6)
		{
			$weekDaysVal = ($DAYS_IN_A_WEEK/4);
		}
		else
		{
			$weekDaysVal = floor($DAYS_IN_A_WEEK/$divCal);
		}
	}
	else
	{
		$weekDaysVal = $controlFile->{'gcsControl'}->{'MIN_NO_WEEK_DAYS'};
	}
	print "WeekDaysVal==>$weekDaysVal\n";
	return ($weekDaysVal);
}

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber);
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	
	
	if($applicationDetailsPageContent=~m/$regexFilePlanning->{$councilCode}->{'PLANNING_APPLICATION_REF'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
	
	print "Current Application number::$applicationNumber\n";

	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $pageContent) = @_;
	my ($nextPageLink,$commonContent);
		
	$commonContent .= $pageContent;
		
	if($pageContent=~m/<[^>]*?>\s*Pages\s*\:?\s*([\w\W]*?)\s*(?:<\/p>|\s*<div\s*id\=\"content2\">\s*<div\s*id\=\"contentPadding2\">)/is)
	{
		my $content=$1;
		while($content=~m/<a[^>]*?href\=\'([\w\W]*?)\'[^>]*?>\s*([\d]{1,2})\s*</igs)
		{
			my $link=$1;
			my $pageNo=$2;
			
			my $nextPageLink;
			if($link!~m/https?\:/is)
			{
				$nextPageLink = URI::URL->new($link)->abs( $Config->{$receivedCouncilCode}->{'URL'}, 1 );
			}	
			else
			{
				$nextPageLink = $link;
			}	
			$nextPageLink =~ s/\&amp\;/\&/g;
			$nextPageLink =~s/\'/\'\'/igs;
						
			$nextPageLink=~s/amp\;//igs;
			
			$receivedCouncilApp->get($nextPageLink);
			my $responseContent = $receivedCouncilApp->content;		

			$commonContent .= $responseContent;
		}
	}

	return($commonContent);
}

####
# Subroutine to set date ranges to GCS schedule
####

sub setDateRanges()
{
    my ($receivedFromDate, $receivedToDate) = @_;
    my ($setFromDate, $setToDate);
    $setFromDate = $receivedFromDate;
    $setToDate = $receivedToDate;
	return($setFromDate, $setToDate);
}

####
# Subroutine to get the Sub Ranges calualted for an GCS scheduled date range
####

sub subRangesCalcMethod
{
    my ($receivedDivCounter, $receivedToDate) = @_;

	my $divCalulator = &dividerCalc($receivedDivCounter);
    my $weekDaysCalculator = &weekDaysCalc($divCalulator);
	# my %subRanges = &gcsRangeCalc($weekDaysCalculator, ceil($DAYS_IN_A_WEEK/$weekDaysCalculator), $receivedToDate);
	my %subRanges = &gcsRangeCalc($weekDaysCalculator, floor($DAYS_IN_A_WEEK/$weekDaysCalculator), $receivedToDate);
	
	return(%subRanges);
}


####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $mech) = @_;	
		
	my ($Application_URL, $Planning_Application_Ref, $Application_Date, $Registration_Date, $Decision_Date, $Application_Type, $Extension_of_time_to, $Main_Location, $Proposal, $Full_Description, $Status, $Status_Description, $Comment, $Applicant_Title, $Applicant_Forename_Initial, $Applicant_Surname_Company_Name, $Applicant_Company_Contact_Name, $Case_Officer, $Agent_Title, $Agent_Forename_Initial, $Agent_Surname_Company_Name, $Agent_Company_Contact_Name, $Agent_Address, $Decision, $Decision_Decision_Date, $Publicity_Start_Date, $Publicity_End_Date, $Type_of_Appeal, $Appeal_Lodged_Date, $Appeal_Decision, $Appeal_Decision_Date, $Document_URL, $Web_Reference, $Last_date_for_Observations, $Final_Grant_Date, $Submission_Observation);
	
	$Planning_Application_Ref = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'PLANNING_APPLICATION_REF'}/is);
	$Application_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPLICATION_DATE'}/is);
	$Registration_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'REGISTRATION_DATE'}/is);
	$Decision_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'DECISION_DATE'}/is);
	$Application_Type = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$Extension_of_time_to = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'EXTENSION_OF_TIME_TO'}/is);
	$Main_Location = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'MAIN_LOCATION'}/is);
	if($councilCode=~m/^(1123|1124)$/is)
	{
		my $newProposal;
		if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'PROPOSAL'}/is)
		{
			$newProposal = $1;
			$Proposal = &htmlTagClean($newProposal);
		}
		if($newProposal=~m/<a\s*href=\"\#\"\s*onClick=\"window\.open\(\'([^\']*?)\'/is)
		{
			my $url=$1;
			if($url!~m/^https?/is)
			{
				$url = URI::URL->new($url)->abs( $Config->{$councilCode}->{'URL'}, 1 );
			}
			
			my ($CNT,$CNT_STATUS) = &getMechContent($url,$mech,$councilCode);	
			
			$Full_Description = &htmlTagClean($1) if($CNT=~m/$regexFilePlanning->{$councilCode}->{'FULL_DESCRIPTION'}/is);			
		}
	}
	else
	{
		$Proposal = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'PROPOSAL'}/is);
		$Full_Description = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'FULL_DESCRIPTION'}/is);
	}
	$Status = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'STATUS'}/is);
	$Status_Description = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'STATUS_DESCRIPTION'}/is);
	$Comment = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'COMMENT'}/is);
	$Applicant_Title = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPLICANT_TITLE'}/is);
	$Applicant_Forename_Initial = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPLICANT_FORENAME_INITIAL'}/is);
	$Applicant_Surname_Company_Name = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPLICANT_SURNAME_COMPANY_NAME'}/is);
	$Applicant_Company_Contact_Name = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPLICANT_COMPANY_CONTACT_NAME'}/is);
	$Case_Officer = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'CASE_OFFICER'}/is);
	$Agent_Title = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'AGENT_TITLE'}/is);
	$Agent_Forename_Initial = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'AGENT_FORENAME_INITIAL'}/is);
	$Agent_Surname_Company_Name = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'AGENT_SURNAME_COMPANY_NAME'}/is);
	$Agent_Company_Contact_Name = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'AGENT_COMPANY_CONTACT_NAME'}/is);
	$Agent_Address = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'AGENT_ADDRESS'}/is);
	
	if($councilCode=~m/^(1122|1123|1124)$/is)
	{
		my $DECISION_LINK = $1 if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'DECISION_LINK'}/is);
		my $APPEAL_LINK = $1 if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_LINK'}/is);
		if($DECISION_LINK eq "")
		{
			$DECISION_LINK = $1 if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'DECISION_LINK_NEW'}/is);
		}

		if($APPEAL_LINK!~m/^https?/is)
		{
			$APPEAL_LINK = URI::URL->new($APPEAL_LINK)->abs( $Config->{$councilCode}->{'URL'}, 1 );
		}
		if($DECISION_LINK!~m/^https?/is)
		{
			$DECISION_LINK = URI::URL->new($DECISION_LINK)->abs( $Config->{$councilCode}->{'URL'}, 1 );
		}
		# print "DecisionLink==>$DECISION_LINK\n";
					
		my ($DECISION_CNT,$DECISION_CNT_STATUS) = &getMechContent($DECISION_LINK,$mech,$councilCode) if($DECISION_LINK ne "");	
		my ($APPEAL_CNT,$APPEAL_CNT_STATUS) = &getMechContent($APPEAL_LINK,$mech,$councilCode) if($APPEAL_LINK ne "");	
		
		$Decision = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFilePlanning->{$councilCode}->{'DECISION'}/is);
		$Decision_Decision_Date = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFilePlanning->{$councilCode}->{'DECISION_DECISION_DATE'}/is);
		$Publicity_Start_Date = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFilePlanning->{$councilCode}->{'PUBLICITY_START_DATE'}/is);
		$Publicity_End_Date = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFilePlanning->{$councilCode}->{'PUBLICITY_END_DATE'}/is);
		
		
		$Type_of_Appeal = &htmlTagClean($1) if($APPEAL_CNT=~m/$regexFilePlanning->{$councilCode}->{'TYPE_OF_APPEAL'}/is);
		$Appeal_Lodged_Date = &htmlTagClean($1) if($APPEAL_CNT=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_LODGED_DATE'}/is);
		$Appeal_Decision = &htmlTagClean($1) if($APPEAL_CNT=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_DECISION'}/is);
		$Appeal_Decision_Date = &htmlTagClean($1) if($APPEAL_CNT=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_DECISION_DATE'}/is);
	}
	else
	{
		$Decision = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'DECISION'}/is);
		$Decision_Decision_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'DECISION_DECISION_DATE'}/is);
		$Publicity_Start_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'PUBLICITY_START_DATE'}/is);
		$Publicity_End_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'PUBLICITY_END_DATE'}/is);
	
		$Type_of_Appeal = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'TYPE_OF_APPEAL'}/is);
		$Appeal_Lodged_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_LODGED_DATE'}/is);
		$Appeal_Decision = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_DECISION'}/is);
		$Appeal_Decision_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'APPEAL_DECISION_DATE'}/is);
	}
	$Document_URL = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'DOCUMENT_URL'}/is);
	$Last_date_for_Observations = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'LAST_DATE_FOR_OBSERVATIONS'}/is);
	$Web_Reference = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'WEB_REFERENCE'}/is);
	$Final_Grant_Date = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'FINAL_GRANT_DATE'}/is);
	$Submission_Observation = &htmlTagClean($1) if($TabContent=~m/$regexFilePlanning->{$councilCode}->{'SUBMISSION_OBSERVATION'}/is);
		
			
	
	$Main_Location=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
	$Main_Location=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
	$Main_Location=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
		

	
	if($Document_URL ne "")
	{
		if($Document_URL!~m/^https?/is)
		{
			$Document_URL = URI::URL->new($Document_URL)->abs( $Config->{$councilCode}->{'URL'}, 1 );
		}
		$Document_URL = $Document_URL;
	}
	else
	{
		$Document_URL = $applicationLink;
	}
	$Document_URL =~s/\&amp\;/\&/igs;
	$Document_URL =~s/\'/\'\'/igs;
	
	$Proposal=~s/\s*View\s*full\s*text$//gsi;	
	$Proposal=~s/\s*View\s*full\s*text\s*\|\s*View\s*Details\s*without\s*Javascript\s*$//gsi;	
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
	$Full_Description=~s/\s*View\s*full\s*text$//gsi;	
	$Full_Description=~s/\s*Show\s*full\s*description$//gsi;
	$Full_Description=~s/\s*View\s*full\s*text\s*\|\s*View\s*Details\s*without\s*Javascript\s*$//gsi;
	$Application_URL = $applicationLink;	
	
	$Main_Location=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$Full_Description=~s/\n+/ /gsi;
	$Status=~s/\n+/ /gsi;
	$Status_Description=~s/\n+/ /gsi;
	$Agent_Address=~s/\n+/ /gsi;
	$Applicant_Company_Contact_Name=~s/\n+/ /gsi;
	$Applicant_Surname_Company_Name=~s/\n+/ /gsi;
	$Agent_Company_Contact_Name=~s/\n+/ /gsi;
	$Agent_Surname_Company_Name=~s/\n+/ /gsi;
	$Case_Officer=~s/\n+/ /gsi;
	$Application_Type=~s/\n+/ /gsi;
	
	$Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	$Status_Description=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Status_Description=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Status_Description=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Application_URL\', \'$scheduleDate\', \'$Planning_Application_Ref\', \'$Application_Date\', \'$Registration_Date\', \'$Decision_Date\', \'$Application_Type\', \'$Extension_of_time_to\', \'$Main_Location\', \'$Proposal\', \'$Full_Description\', \'$Status\', \'$Status_Description\', \'$Comment\', \'$Applicant_Title\', \'$Applicant_Forename_Initial\', \'$Applicant_Surname_Company_Name\', \'$Applicant_Company_Contact_Name\', \'$Case_Officer\', \'$Agent_Title\', \'$Agent_Forename_Initial\', \'$Agent_Surname_Company_Name\', \'$Agent_Company_Contact_Name\', \'$Agent_Address\', \'$Decision\', \'$Decision_Decision_Date\', \'$Publicity_Start_Date\', \'$Publicity_End_Date\', \'$Type_of_Appeal\', \'$Appeal_Lodged_Date\', \'$Appeal_Decision\', \'$Appeal_Decision_Date\', \'$Document_URL\', \'$Web_Reference\', \'$Final_Grant_Date\', \'$Last_date_for_Observations\', \'$Submission_Observation\'),";
	
	undef $Application_URL; undef $Planning_Application_Ref; undef $Application_Date; undef $Registration_Date; undef $Decision_Date; undef $Application_Type; undef $Extension_of_time_to; undef $Main_Location; undef $Proposal; undef $Full_Description; undef $Status; undef $Status_Description; undef $Comment; undef $Applicant_Title; undef $Applicant_Forename_Initial; undef $Applicant_Surname_Company_Name; undef $Applicant_Company_Contact_Name; undef $Case_Officer; undef $Agent_Title; undef $Agent_Forename_Initial; undef $Agent_Surname_Company_Name; undef $Agent_Company_Contact_Name; undef $Agent_Address; undef $Decision; undef $Decision_Decision_Date; undef $Publicity_Start_Date; undef $Publicity_End_Date; undef $Type_of_Appeal; undef $Appeal_Lodged_Date; undef $Appeal_Decision; undef $Appeal_Decision_Date; undef $Document_URL; undef $Last_date_for_Observations; undef $Web_Reference; undef $Final_Grant_Date; undef $Submission_Observation; 
	
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	$data2Clean=~s/^\s+//igs;
	$data2Clean=~s/\s+$//igs;
	
	return($data2Clean);
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
    my $currentCouncilCode = shift;
   

	$URL=~s/amp\;//igs;
		
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
    return($con,$code);
}

1;