use strict;
use LWP::Simple;
use HTTP::Cookies;
use HTML::Entities;
use Time::HiRes qw(time);
use DateTime;
use POSIX qw(strftime);
use DateTime::Format::Strptime;
use Time::Piece;
use Cwd;
use Spreadsheet::XLSX;
use Excel::Writer::XLSX;
use DateTime::Format::Excel;
use Spreadsheet::WriteExcel;
use Encode;

my $sour=$ARGV[0];

my $time_now = DateTime->now( time_zone => "Asia/Kolkata" );
my $current_year=$time_now->year; 

open FH,"<Genre_Translation.txt";
my @kyts = <FH>;

open KL,"<Genre_List.txt";
my @kylist=<KL>;

open CL,"<Country_List.txt";
my @ctry_list=<CL>;

open JJ,"<Language_List.txt";
my @Lang_list=<JJ>;	


# my $s = "@kyts";

# open HH,">Output.txt";
# print HH "Source ID\tStation ID\tProgram Start Date\tProgram Start Time\tProgram End Time\tTime Zone\tDuration\tProgram Type\tAiring Type\tShowing Type\tCaptioning\tHD Level\tAudio Level\tColor Type\t3-D\tAspect Ratio\tSAP\tProgram Title\tPrimary Language\tEpisode Title\tEpisode Number\tPart Number\tPart Total\tRating (Movie)\tRating (TV)\tYear of Release\tProgram Credits\tProgram Description (optional)\tSports Team1\tSports Team2\tSports Conjunction\tSports Event Date\tProgram Information\tOriginal Air Dates\tProgram Credits - Directors\tProgram Credits - Actors\tSeason Number\tScheduleActualStartDate\tScheduleDesiredStartDate\tScheduleDesiredEndDate\tProgramEndDate\tCustom01\tCustom02\tCustom03\tCustom04\tCustom05\tCustom06\tCustom07\tCustom08\tCustom09\tCustom10\tTV Rating\tOriginalTitle\tOriginalTitle\tRuntime\tCountryOfOrigin\tGenre\tCategory\tEventDescription\tFeedBack\n";
# close HH;

my $workbook = Excel::Writer::XLSX->new("$sour-Website.xlsx");

my $worksheet = $workbook->add_worksheet('Output');
my $worksheet2=$workbook->add_worksheet('Error_Log');

my $rc = 0;

my $tit_xl = "Source ID\tStation ID\tProgram Start Date\tProgram Start Time\tProgram End Time\tTime Zone\tDuration\tProgram Type\tAiring Type\tShowing Type\tCaptioning\tHD Level\tAudio Level\tColor Type\t3-D\tAspect Ratio\tSAP\tProgram Title\tPrimary Language\tEpisode Title\tEpisode Number\tPart Number\tPart Total\tRating (Movie)\tRating (TV)\tYear of Release\tProgram Credits\tProgram Description (optional)\tSports Team1\tSports Team2\tSports Conjunction\tSports Event Date\tProgram Information\tOriginal Air Dates\tProgram Credits - Directors\tProgram Credits - Actors\tSeason Number\tScheduleActualStartDate\tScheduleDesiredStartDate\tScheduleDesiredEndDate\tProgramEndDate\tCustom01\tCustom02\tCustom03\tCustom04\tCustom05\tCustom06\tCustom07\tCustom08\tCustom09\tCustom10\tTV Rating\tOriginalTitle\tOriginalTitle\tRuntime\tCountryOfOrigin\tGenre\tCategory\tEventDescription\tFeedBack";


my @j = split("\t",$tit_xl);
my $cv = -1;
foreach my $j(@j){
	$cv++;
	$worksheet->write( 0, $cv, $j);
}

my $er_fl="Error Log\nColumn\tErrors\tCount\n";
my @j = split("\t",$er_fl);

my $cv = -1;
foreach my $j(@j){
	$cv++;
	$worksheet2->write( 0, $cv, $j);
}

my $cwd = getcwd();

# open FF,"<Document_list.txt";
# my @fil=<FF>;
my $cwd = getcwd();
my @fil="$cwd/$sour\_Output\.xlsx";

foreach my $file_path(@fil)
{

	my $count=1;	
	my($yr_c,$dur_c,$pg_nmc,$pg_ty_c,$key_c,$ctry_c,$lag_c,$pg_nm_c)=0;
	my($yrp,$durp,$pg_nm_p,$pg_typ,$key_p,$ctry_p,$lan_p);
	my $excel = Spreadsheet::XLSX->new("$file_path");

	my $sheet = ${ $excel->{Worksheet} }[0];

	my $k=0;
	my $rw_cnt=0;
	foreach my $row ($sheet -> {MinRow} .. $sheet -> {MaxRow}+1) 
	{
		$rw_cnt++;
		
		if($rw_cnt > 1)
		{
		
			my $row_data;
			my $c = 0;
			my $va;
			foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol})
			{
				$c++;			   
				my $cell = $sheet -> {Cells} [$row] [$col];
				if (($c == 2) || ($c == 3)){
					$va = $cell -> {Val};
					# print "VA:: $va\n";
					if ($va != "")
					{
						
						if(($va!~m/\-/is)&&($c == 2))
						{
							my $datetime = DateTime::Format::Excel->parse_datetime( $va );
							$va = $datetime->iso8601();
							print "VA:: $va\n";
							if($va=~m/T/is)
							{
								if(($c == 2))
								{
									my @j = split("T",$va);
									# $va = substr $j[0],0,10;
									$va =$j[0];
									# print "VA1:: $va\n";
								}else{
								
									my @j = split("T",$va);
									$va = substr $j[1],0,5;
									# print "VA2:: $va\n";
								
								}
							}
						}
						
						if(($va!~m/\:/is)&&($c == 3))
						{
							my $datetime = DateTime::Format::Excel->parse_datetime( $va );
							$va = $datetime->iso8601();
							# print "VA:: $va\n";
							if($va=~m/T/is)
							{
								if(($c == 2))
								{
									my @j = split("T",$va);
									# $va = substr $j[0],0,10;
									$va =$j[0];
									# print "VA1:: $va\n";
								}else{
								
									my @j = split("T",$va);
									$va = substr $j[1],0,5;
									# print "VA2:: $va\n";
								
								}
							}
						}
						
						
					}
				}else{
					$va = $cell -> {Val};
				}
				
				
				$row_data=$row_data."\t".$va;
				# $row_data=$row_data."\t".$cell -> value();

			}
			
			$row_data=~s/^\t//igs;
			# print "$row_data\n";
			# print length($row_data);
			
			# my($Lib_key,$Station,$Date,$Start_tm,$Duration,$Program_name,$General_description,$Season_LibKey,$Episode_title,$Episode_description,$Short_description,$Crew_cast,$Genre,$Country,$Year,$Nicam,$Website,$Social_media,$Hashtag,$SD_HD,$SA,$EP,$Teletext_subtitling,$Restart_right,$Restart_value)=split("\t",$row_data);
			
			my($Station,$Date,$Start_tm,$Duration,$Program_name,$Episode_title,$season_number,$EP,$sea_epis,$General_description,$rating,$airingtype,$Year,$Country,$programtype,$Crew_cast,$DIRECTOR)=split("\t",$row_data);
			
			my $General_description=trim($General_description);
			# open FG,">>ddd.txt";
			# print FG "$General_description";
			# close FG;
			
			# print "Duration:: $Duration\n";
			
			
			# print "Crew_cast:: $Crew_cast\n";
			
			
			
			$Crew_cast=~s/\,/; Actor:/igs;
			$Crew_cast=~s/^\;//igs;

			$DIRECTOR=~s/\,/Director:/igs;
			$DIRECTOR=trim($DIRECTOR);
			
			
			
			my $act="Actor:".$Crew_cast if($Crew_cast ne "");
			my $dirc="Director:".$DIRECTOR if($DIRECTOR ne "");
			
			my $cast=$act.";".$dirc;
			$cast=~s/^\;//igs;
			$cast=trim($cast);
			
			# print "cast:: $cast\n";
			
			
			
			# my @arr_act=split('\n',$Crew_cast);
			# my $temp=0;
			# my $pc=0;
			# my ($prr,$aro,$aro2);
			# my $kyw;
			my($yr_clr,$dur_clr,$pg_nm_clr,$lang_clr,$key_clr,$cntry_clr);
			# foreach my $ar_g(@arr_act)
			# {
				# $ar_g=trim($ar_g);
				# if (length($ar_g) != 0)
				# {

					# $kyw=$ar_g if ($ar_g=~m/([^>]*?\:$)/is);
					# if(lc($ar_g) eq lc($kyw))
					# {
						# $temp=1;
						# $aro=$kyw;
						
					# }
					# if($temp==1)
					# {
						# $pc++;
						# if ($pc>1)
						# {
							# $aro2=$aro2.$aro."$ar_g"."; ";
						# }
					# }

				# }else{
					# $temp=0;
					# $pc=0;
				# }
			# }
			
			# $aro2=trim($aro2);
			
			my $Genre=~s/SubGenre\://igs;
			$Genre=~s/Genre\://igs;
			
			my ($ggo);
			
			# $Genre=key_trans($Genre);
			if($Genre ne "")
			{
				my @Gen=split('\n',$Genre);
				foreach my $gg(@Gen)
				{
					$gg=trim($gg);
					if($gg ne "")
					{
						# print "GG::$gg\n";
						# my @kar=split('\,',$gg);
						# foreach my $ggg(@kar)
						# {
							foreach my $s(@kyts){
								chomp($s);
								# print $s;
								my ($f,$r) = split '\t',$s;
								$f=trim($f);
								
								$gg=trim($gg);
								$f=lc($f);
								$gg=lc($gg);
								# print "fff:: $f\n";
								# print "gg:: $gg\n";
								if ($gg eq $f)
								{
									$gg=replace($gg,$f,$r);
									# goto ncc;
								}
								
								# $ggo=$ggo.",".ucfirst($ggg);
								
							}
							# ncc:
							if($gg ne '')
							{
								$ggo=$ggo.",".ucfirst($gg);
							}
						# }
					}
				
				}
				$Genre=$ggo;
			}

			$Genre=trim($Genre);
			# my $val="Comedy,Romantic,Romantic,Comedy";
			my @rrr=split /\,/,$Genre;
			my @filtered = uniq(@rrr);
			$Genre= join(",",@filtered);
			
			# print "Genre:: $Genre\n";
			
			my $cell2 = $sheet -> {Cells} [$row+1] [1];
			my $end_dat = $cell2 -> {Val};
			
			# my ($sdate,$edate,$Tim_dif,$end_tm,$Duration);
			my ($sdate,$edate,$Tim_dif,$end_tm);
			$Start_tm=timeformat($Start_tm);
			# print "Start_Date:: $Date";
			# print "Start_Time:: $Date";
			# print "end_dat:: $end_dat\n";
			if ($end_dat ne '')
			{
				#--------------------------------
				
				if($end_dat!~m/\-/is)
				{
				
					my $datetime2 = DateTime::Format::Excel->parse_datetime($end_dat);
					$end_dat = $datetime2->iso8601();
					# print "end_dat:: $end_dat\n";
					if($end_dat=~m/T/is)
					{
						
						my @j = split("T",$end_dat);
						# $va = substr $j[0],0,10;
						$end_dat =$j[0];
						# print "VA1:: $end_dat\n";
						
					}
				
				}
				
				
				# my @j = split("T",$end_dat);
				# $end_dat = substr $j[0],0,10;
				
				# print "end_dat:: $end_dat\n";
				
				my $cell2 = $sheet -> {Cells} [$row+1] [2];
				$end_tm = $cell2 -> {Val};
				# print "end_tm1:: $end_tm\n";
				
				#--------------------------------
				
				if($end_tm!~m/\:/is)
				{
					my $datetime2 = DateTime::Format::Excel->parse_datetime($end_tm);
					$end_tm = $datetime2->iso8601();
					# print "end_tm2:: $end_tm\n";
					
					# my @j = split("T",$end_tm);
					# $end_tm = $j[1];
					
					
					
					if($end_tm=~m/T/is)
					{
						
						my @j = split("T",$end_tm);
						# $va = substr $j[0],0,10;
						$end_tm =substr $j[1],0,10;
						# print "VA1:: $end_tm\n";
						
					}
				}
				
				# print "end_tm3:: $end_tm\n";
				
				$end_tm=timeformat($end_tm);
				$sdate="$Date"." "."$Start_tm";
				$edate="$end_dat"." "."$end_tm";
				
				# print "sdate:: $sdate\n";
				# print "edate:: $edate\n";

				# $Tim_dif=Time_Diff($sdate,$edate);
				# $Duration=Time_Diff($sdate,$edate);
				
				# print "Tim_dif:: $Duration\n";
			}	
			
			$Country=&contry_trans($Country) if(length($Country) == 2);
			my $prev_contry;
			if($Country=~m/^([\w\W]*?)\|/is)
			{
				$prev_contry=$1;
			}
			else{
				$prev_contry=$Country;
			}
			
			
			# my $lang = &contry_trans("NL")
			
			# $langs{'England'}
			my $val_cntry=counrty_list($prev_contry);
			$val_cntry=trim($val_cntry);
			# print length($val_cntry);
			# $ctry_c++; $cntry_clr = "Yes" if($val_cntry ne "");
			if($val_cntry ne "")
			{
				$ctry_c++; 
				$cntry_clr="Yes"; 
			}
			# print "Coutry_val:: $val_cntry\n";
			# print "ctry_c:: $ctry_c\n";
			
			my $prg_type;
			
			my $ser_key=$Genre;
		
			my $prev_lag;
			my $val_lang=Laguage_list($prev_lag);
			my ($val_ser_ky,$val_mv_ky,$val_temp);
			if($ser_key =~m/\,/is)
			{
				my @fg=split('\,',$Genre);
				foreach my $ff(@fg)
				{
					$val_ser_ky=Genre_list($ff);
					$val_temp=1 if($val_ser_ky eq "Verify KeyWord");
				}
			}else{
				$val_ser_ky=Genre_list($ser_key);
				$val_temp=1 if($val_ser_ky eq "Verify KeyWord");
			}
			
			if ($val_temp !=1){
				$val_ser_ky="";
			}else{
				$key_c++;
				$key_clr="Yes";
			}

			my ($feedback,$fd_c,$fd_l,$fd_s,$fd_m);
			$fd_c=$val_cntry if($val_cntry ne "");
			$fd_l=", ".$val_lang if($val_lang ne "");
			$fd_s=", ".$val_ser_ky if($val_ser_ky ne "");
			$fd_m=", ".$val_mv_ky if($val_mv_ky ne "");
			
	#----- XML Valied Data or not ---------------	
		
			# $prev_contry="" if($val_cntry ne "");
			# $prev_lag="" if($val_lang ne "");
			# $genre1="" if($val_ser_ky ne "");
			# $genre1="" if($val_mv_ky ne "");
			
			$feedback="$fd_c"."$fd_l"."$fd_s"."$fd_m";
			$feedback=~s/^\,\s*//igs;
			$feedback=trim($feedback);
			# print "FEED_BACK:: $feedback\n";
			
	#---------------------------------
	#---------- Error_Logs -------------------
			
			# print "Duration:: $Duration\n";
	
			$rc++;
			# $yr_c++; $yr_clr="Yes" 	if(($current_year ne $Year)&& ($Year ne ""));
			# $dur_c++; $dur_clr="Yes" if($Duration >= 300);
			# print "dur_c:: $dur_c\n";
			
			my $dat_yr = cond($Date,'([\d]{4})');
			
			if(($current_year ne $dat_yr)&& ($dat_yr ne ""))
			{
				 $yr_c++; 
				 $yr_clr="Yes";
			}
			if($Duration >= 300)
			{
				$dur_c++; 
				$dur_clr="Yes";
			}
			
			
			my $pt=$Program_name;
			
			# $pt=trim($pt);
			$pt=~s/\s+//igs;
			$pt=~s/\s*//igs;
			
			# $pg_nm_c++; $pg_nm_clr="Yes" if($pt=~m/^[\d]+$/is);
			# $lag_c++; $lang_clr="Yes"if($val_lang ne "");
			if($pt=~m/^[\d]+$/is)
			{
				$pg_nm_c++; 
				$pg_nm_clr="Yes";
			}
			
			# print "pt::1 $Program_name\n";
			# print "Episode_title::1 $Episode_title\n";
			$Episode_title=~s/$Program_name//igs;
			$Episode_title=trim($Episode_title);
			$Episode_title=~s/^-//igs;
			$Episode_title=~s/^\.$//igs;
			
			# print "Episode_title2:: $Episode_title\n";
			
			my $ep_nm;
			
			if($Episode_title=~m/Aflevering/igs)
			{
				$ep_nm=$Episode_title;
				$ep_nm=~s/Aflevering//igs;
			
				$Episode_title=~s/Aflevering//igs;
				$Episode_title=~s/$ep_nm//igs;
			}
			
			
			$Episode_title=trim($Episode_title);
			
			if ($EP == '')
			{
				$ep_nm=$ep_nm;
			}else
			{
				$ep_nm=$EP;
			}
			
			$ep_nm=trim($ep_nm);
			$season_number=trim($season_number);
			
			
			
			# $pg_ty_c++ if(($Program_type eq "Movie") && ($Episode_title eq ""));
			
			
			$yrp="Date"."\t"."Yes"."\t"."$yr_c"	if($yr_c > 0 );
			$yrp="Date"."\t"."No"."\t"."$yr_c"	if($yr_c == 0 );
			$durp="Duration"."\t"."Yes"."\t"."$dur_c" if($dur_c > 0 );
			$durp="Duration"."\t"."No"."\t"."$dur_c" if($dur_c == 0 );
			$pg_nm_p="Program Title"."\t"."Yes"."\t"."$pg_nm_c" if($pg_nm_c > 0 );
			$pg_nm_p="Program Title"."\t"."No"."\t"."$pg_nm_c" if($pg_nm_c == 0 );
			# my $pg_typ="Program Type"."\t"."Yes"."\t"."$pg_ty_c" if($pg_ty_c > 0 );
			$pg_typ="Program Type"."\t"."No"."\t"."";
			
			$key_p="Key Word"."\t"."Yes"."\t"."$key_c" if($key_c > 0 );
			$key_p="Key Word"."\t"."No"."\t"."$key_c" if($key_c == 0 );
			$ctry_p="Country"."\t"."Yes"."\t"."$ctry_c" if($ctry_c > 0 );
			$ctry_p="Country"."\t"."No"."\t"."$ctry_c" if($ctry_c == 0 );
			$lan_p="Language"."\t"."Yes"."\t"."$ctry_c" if($lag_c > 0 );
			$lan_p="Language"."\t"."No"."\t"."$ctry_c" if($lag_c == 0 );
			
			#----------------------
			
			# $genre_xl=ucfirst($genre_xl);
			# $contry=ucfirst($contry);
			# $lang=ucfirst($lang);

			# open HH,">>Output.txt";
			# print HH "\t$Station\t$Date\t$Start_tm\t$end_tm\t\t$Duration\t\t\t\t\t\t\t\t\t\t\t$Program_name\t\t$Episode_title\t$ep_nm\t\t\t\t\t$Year\t$aro2\t$General_description\t\t\t\t\t\t\t\t\t$SA\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t$Country\t$Genre\t\t\t$feedback\n";
			
			# print HH "Source ID\t$Station\t$Date\t$Start_tm\tProgram End Time\tTime Zone\t$Duration\tProgram Type\tAiring Type\tShowing Type\tCaptioning\tHD Level\tAudio Level\tColor Type\t3-D\tAspect Ratio\tSAP\t$Program_name\tPrimary Language\t$Episode_title\tEpisode Number\tPart Number\tPart Total\tRating (Movie)\tRating (TV)\t$Year\tProgram Credits\t$General_description\tSports Team1\tSports Team2\tSports Conjunction\tSports Event Date\tProgram Information\tOriginal Air Dates\tProgram Credits - Directors\t$Crew_cast\tSeason Number\tScheduleActualStartDate\tScheduleDesiredStartDate\tScheduleDesiredEndDate\tProgramEndDate\tCustom01\tCustom02\tCustom03\tCustom04\tCustom05\tCustom06\tCustom07\tCustom08\tCustom09\tCustom10\tTV Rating\tOriginalTitle\tOriginalTitle\tRuntime\t$Country\t$Genre\tCategory\tEventDescription\n";
			# close HH;
			
			
			
			
			
			my $j1 = "\t$Station\t$Date\t$Start_tm\t$end_tm\t\t$Duration\t$programtype\t$airingtype\t\t\t\t\t\t\t\t\t$Program_name\t\t$Episode_title\t$ep_nm\t\t\t\t\t$Year\t$cast\t$General_description\t\t\t\t\t\t\t\t\t$season_number\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t$rating\t\t\t\t$Country\t$Genre\t\t\t$feedback\n";
			my @ja = split("\t",$j1);
			my $cc = -1;
			
			
			foreach my $ja(@ja){
				$cc++;
				my $format = $workbook->add_format();
				$format->set_bg_color( 'None' );
				if($yr_clr eq "Yes" && $cc == 2){
					$format->set_bg_color( 'orange' );
				}
				if(($dur_clr eq "Yes") && ($cc == 6))
				{
					$format->set_bg_color( 'orange' );
				}
				if($pg_nm_clr eq "Yes" && $cc == 17){
					$format->set_bg_color( 'orange' );
				}
				if($lang_clr eq "Yes" && $cc == 18){
					$format->set_bg_color( 'orange' );
				}
				if($key_clr eq "Yes" && $cc == 56){
					$format->set_bg_color( 'orange' );
				}
				if($cntry_clr eq "Yes" && $cc == 55){
					$format->set_bg_color( 'orange' );
				}
				
				# $worksheet->write($rc, $cc, $ja, $format);
				$worksheet->write($rc, $cc, decode('utf8',$ja), $format);
			}		
		}
	}
	
	# open FF,">>Error_log.txt";
	# print FF "$yrp\n$durp\n$pg_nm_p\n$pg_typ\n$key_p\n$ctry_p\n$lan_p\n";
	# close FF;
	
	my $rcc=0;
	my $j1 = "$yrp\n$durp\n$pg_nm_p\n$pg_typ\n$key_p\n$ctry_p\n$lan_p\n";
	my @ja = split("\n",$j1);
	
	foreach my $ja(@ja)
	{
		$rcc++;
		my $cc = 0;
		# print $ja;
		my $format = $workbook->add_format();
		$format->set_bg_color( 'None' );
		my @ss=split("\t",$ja);
		foreach my $gh(@ss)
		{
			$worksheet2->write($rcc, $cc, $gh, $format);
			$cc++;
		}
		
	}
	
	
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

sub Time_Diff()
{
	my ($from,$to)=@_;
	chomp($from);chomp($to);
	# print "From :$from---- To :$to\n";
	my @fr=split '\-',$from;
	# print length($fr[0]);
	# print length($fr[2]);
	my $dd1;
	if(length($fr[0]) != 4)
	{
	
	
	my $before = Time::Piece->strptime($from, "%d-%m-%Y %H:%M:%S");
	my $now = Time::Piece->strptime($to, "%d-%m-%Y %H:%M:%S");
	
	
	my $diff = $before - $now;
	
	$dd1=int($diff->minutes);
	$dd1=~s/\-//igs;
	}else{
	
		my $before = Time::Piece->strptime($from, "%Y-%m-%d %H:%M:%S");
		my $now = Time::Piece->strptime($to, "%Y-%m-%d %H:%M:%S");
		my $diff = $before - $now;
		 $dd1=int($diff->minutes);
		$dd1=~s/\-//igs;
	}
	
	print "DDD::$dd1\n";
	
	return $dd1;

}

sub replace
{
my $str=shift;
my $find=shift;
my $replace=shift;
my $pos=index($str,$find);
	while($pos != -1)
	{
	  substr($str,$pos,length($find),$replace);
	  $pos=index($str,$find,$pos+length($replace));
	}
	return $str;
}


sub timeformat()
{
	my $ip=shift;
	my $end_tm;
	if($ip=~m/([\d]+)\:([\d]+)/is)
	{
		my $hr=$1;
		my $mn=$2;
		if (length($hr) == 1)
		{
			$hr='0'.$hr;
		}
		$end_tm=$hr.':'.$mn;
	}
	return $end_tm;
}

sub cond()
{
	my $value=shift;
	my $regex=shift;
	if($value=~m/$regex/is)
	{
		my $data=$1;
		return $data;
	}
}	

sub trim()
{
	my $triming=shift;
	$triming=~s/\s+/ /igs;
	$triming=~s/\s*$//igs;
	$triming=~s/^\s*//igs;
	$triming=~s/^\,//igs;
	$triming=~s/^\,//igs;
	$triming=~s/$\,\,\,//igs;
	$triming=~s/^\,\,\,//igs;
	$triming=~s/^\,\,//igs;
	$triming=~s/$\,\,//igs;
	$triming=~s/\,$//igs;
	$triming=~s/\;$//igs;
	return $triming;
}


sub contry_trans()
{
	my $cnty=shift();
	my %contry=( AF=> 'Afghanistan',AX=> 'Åland Islands',	AL=> 'Albania',DZ=> 'Algeria',AS=> 'American Samoa',AD=> 'Andorra',AO=> 'Angola',AI=> 'Anguilla',AQ=> 'Antarctica',AG=> 'Antigua and Barbuda',AR=> 'Argentina',AM=> 'Armenia',AW=> 'Aruba',AU=> 'Australia',AT=> 'Austria',AZ=> 'Azerbaijan',BS=> 'Bahamas',BH=> 'Bahrain',BD=> 'Bangladesh',BB=> 'Barbados',BY=> 'Belarus',BE=> 'Belgium',BZ=> 'Belize',BJ=> 'Benin',BM=> 'Bermuda',BT=> 'Bhutan',BO=> 'Bolivia, Plurinational State of',BQ=> 'Bonaire, Sint Eustatius and Saba',BA=> 'Bosnia and Herzegovina',BW=> 'Botswana',BV=> 'Bouvet Island',BR=> 'Brazil',IO=> 'British Indian Ocean Territory',BN=> 'Brunei Darussalam',BG=> 'Bulgaria',BF=> 'Burkina Faso',BI=> 'Burundi',KH=> 'Cambodia',CM=> 'Cameroon',CA=> 'Canada',CV=> 'Cape Verde',KY=> 'Cayman Islands',CF=> 'Central African Republic',TD=> 'Chad',CL=> 'Chile',CN=> 'China',CX=> 'Christmas Island',CC=> 'Cocos (Keeling) Islands',CO=> 'Colombia',KM=> 'Comoros',CG=> 'Congo',CD=> 'Congo, the Democratic Republic of the',CK=> 'Cook Islands',CR=> 'Costa Rica',CI=> "CÃ´te d'Ivoire",HR=> 'Croatia',CU=> 'Cuba',CW=> 'CuraÃ§ao',CY=> 'Cyprus',CZ=> 'Czech Republic',DK=> 'Denmark',DJ=> 'Djibouti',DM=> 'Dominica',DO=> 'Dominican Republic',EC=> 'Ecuador',EG=> 'Egypt',SV=> 'El Salvador',GQ=> 'Equatorial Guinea',ER=> 'Eritrea',EE=> 'Estonia',ET=> 'Ethiopia',FK=> 'Falkland Islands (Malvinas)',FO=> 'Faroe Islands',FJ=> 'Fiji',FI=> 'Finland',FR=> 'France',GF=> 'French Guiana',PF=> 'French Polynesia',TF=> 'French Southern Territories',GA=> 'Gabon',GM=> 'Gambia',GE=> 'Georgia',DE=> 'Germany',GH=> 'Ghana',GI=> 'Gibraltar',GR=> 'Greece',GL=> 'Greenland',GD=> 'Grenada',GP=> 'Guadeloupe',GU=> 'Guam',GT=> 'Guatemala',GG=> 'Guernsey',GN=> 'Guinea',GW=> 'Guinea-Bissau',GY=> 'Guyana',HT=> 'Haiti',HM=> 'Heard Island and McDonald Islands',VA=> 'Holy See (Vatican City State)',HN=> 'Honduras',HK=> 'Hong Kong',HU=> 'Hungary',IS=> 'Iceland',IN=> 'India',ID=> 'Indonesia',IR=> 'Iran, Islamic Republic of',IQ=> 'Iraq',IE=> 'Ireland',IM=> 'Isle of Man',IL=> 'Israel',IT=> 'Italy',JM=> 'Jamaica',JP=> 'Japan',JE=> 'Jersey',JO=> 'Jordan',KZ=> 'Kazakhstan',KE=> 'Kenya',KI=> 'Kiribati',KP=> "Korea, Democratic People's Republic of",KR=> 'Korea, Republic of',KW=> 'Kuwait',KG=> 'Kyrgyzstan',LA=> "Lao People's Democratic Republic",LV=> 'Latvia',LB=> 'Lebanon',LS=> 'Lesotho',LR=> 'Liberia',LY=> 'Libya',LI=> 'Liechtenstein',LT=> 'Lithuania',LU=> 'Luxembourg',MO=> 'Macao',MK=> 'Macedonia, the Former Yugoslav Republic of',MG=> 'Madagascar',MW=> 'Malawi',MY=> 'Malaysia',MV=> 'Maldives',ML=> 'Mali',MT=> 'Malta',MH=> 'Marshall Islands',MQ=> 'Martinique',MR=> 'Mauritania',MU=> 'Mauritius',YT=> 'Mayotte',MX=> 'Mexico',FM=> 'Micronesia, Federated States of',MD=> 'Moldova, Republic of',MC=> 'Monaco',MN=> 'Mongolia',ME=> 'Montenegro',MS=> 'Montserrat',MA=> 'Morocco',MZ=> 'Mozambique',MM=> 'Myanmar',NA=> 'Namibia',NR=> 'Nauru',NP=> 'Nepal',NL=> 'Netherlands',NC=> 'New Caledonia',NZ=> 'New Zealand',NI=> 'Nicaragua',NE=> 'Niger',NG=> 'Nigeria',NU=> 'Niue',NF=> 'Norfolk Island',MP=> 'Northern Mariana Islands',NO=> 'Norway',OM=> 'Oman',PK=> 'Pakistan',PW=> 'Palau',PS=> 'Palestine, State of',PA=> 'Panama',PG=> 'Papua New Guinea',PY=> 'Paraguay',PE=> 'Peru',PH=> 'Philippines',PN=> 'Pitcairn',PL=> 'Poland',PT=> 'Portugal',PR=> 'Puerto Rico',QA=> 'Qatar',RE=> 'RÃ©union',RO=> 'Romania',RU=> 'Russian Federation',RW=> 'Rwanda',BL=> 'Saint BarthÃ©lemy',SH=> 'Saint Helena, Ascension and Tristan da Cunha',KN=> 'Saint Kitts and Nevis',LC=> 'Saint Lucia',MF=> 'Saint Martin (French part)',PM=> 'Saint Pierre and Miquelon',VC=> 'Saint Vincent and the Grenadines',WS=> 'Samoa',SM=> 'San Marino',ST=> 'Sao Tome and Principe',SA=> 'Saudi Arabia',SN=> 'Senegal',RS=> 'Serbia',SC=> 'Seychelles',SL=> 'Sierra Leone',SG=> 'Singapore',SX=> 'Sint Maarten (Dutch part)',SK=> 'Slovakia',SI=> 'Slovenia',SB=> 'Solomon Islands',SO=> 'Somalia',ZA=> 'South Africa',GS=> 'South Georgia and the South Sandwich Islands',SS=> 'South Sudan',ES=> 'Spain',LK=> 'Sri Lanka',SD=> 'Sudan',SR=> 'Suriname',SJ=> 'Svalbard and Jan Mayen',SZ=> 'Swaziland',SE=> 'Sweden',CH=> 'Switzerland',SY=> 'Syrian Arab Republic',TW=> 'Taiwan, Province of China',TJ=> 'Tajikistan',TZ=> 'Tanzania, United Republic of',TH=> 'Thailand',TL=> 'Timor-Leste',TG=> 'Togo',TK=> 'Tokelau',TO=> 'Tonga',TT=> 'Trinidad and Tobago',TN=> 'Tunisia',TR=> 'Turkey',TM=> 'Turkmenistan',TC=> 'Turks and Caicos Islands',TV=> 'Tuvalu',UG=> 'Uganda',UA=> 'Ukraine',AE=> 'United Arab Emirates',GB=> 'United Kingdom',US=> 'United States',UM=> 'United States Minor Outlying Islands',UY=> 'Uruguay',UZ=> 'Uzbekistan',VU=> 'Vanuatu',VE=> 'Venezuela, Bolivarian Republic of',VN=> 'Viet Nam',VG=> 'Virgin Islands, British',VI=> 'Virgin Islands, U.S.',WF=> 'Wallis and Futuna',EH=> 'Western Sahara',YE=> 'Yemen',ZM=> 'Zambia',ZW=> 'Zimbabwe',VS=> 'United States');

	my $Country=$contry{$cnty};

	return $Country;

}

sub key_trans()
{
	my $ss=shift();
	$ss=~s/Romántico/Romantic/igs;
	$ss=~s/Histórico/Historical/igs;
	$ss=~s/Acción/action/igs;
	$ss=~s/crimen/crime/igs;
	$ss=~s/Espionaje/Espionage/igs;
	$ss=~s/Animación/Animation/igs;
	$ss=~s/Familia/Family/igs;
	$ss=~s/aventuras/adventure/igs;
	$ss=~s/Aventura/adventure/igs;
	$ss=~s/Acción\/aventuras/action\/adventure/igs;
	$ss=~s/adulto/adult/igs;
	$ss=~s/arte y literatura/arts & literature/igs;
	$ss=~s/letras & literatura/arts & literature/igs;
	$ss=~s/biografía/biography/igs;
	$ss=~s/comedia/comedy/igs;
	$ss=~s/comedia dramática/comedy-drama/igs;
	$ss=~s/drama criminal/crime drama/igs;
	$ss=~s/documental /documentary/igs;
	$ss=~s/drama/drama/igs;
	$ss=~s/fantasía/fantasy/igs;
	$ss=~s/horror/horror/igs;
	$ss=~s/Artes marciales/martial arts/igs;
	$ss=~s/música/music/igs;
	$ss=~s/musical/musical/igs;
	$ss=~s/misterio y suspense/mystery & suspense/igs;
	$ss=~s/paranormal/paranormal/igs;
	$ss=~s/ciencia ficción/sci-fi/igs;
	$ss=~s/occidental/western/igs;
	$ss=~s/ação e aventura/action\/adventure/igs;
	$ss=~s/adulto/adult/igs;
	$ss=~s/artes e literatura/arts & literature/igs;
	$ss=~s/biografia/biography/igs;
	$ss=~s/comédia/comedy/igs;
	$ss=~s/comédia-drama/comedy-drama/igs;
	$ss=~s/drama do crime/crime drama/igs;
	$ss=~s/documentário/documentary/igs;
	$ss=~s/drama/drama/igs;
	$ss=~s/fantasia/fantasy/igs;
	$ss=~s/Horror/horror/igs;
	$ss=~s/Artes marciais/martial arts/igs;
	$ss=~s/música/music/igs;
	$ss=~s/musical/musical/igs;
	$ss=~s/mistério e suspense/mystery & suspense/igs;
	$ss=~s/paranormal/paranormal/igs;
	$ss=~s/ficção científica/sci-fi/igs;
	$ss=~s/ocidental/western/igs;
	$ss=~s/3D/3D/igs;
	$ss=~s/esportes de ação/action sports/igs;
	$ss=~s/ação e aventura/action\/adventure/igs;
	$ss=~s/adaptação/adaptation/igs;
	$ss=~s/adulto/adult/igs;
	$ss=~s/alternativa/alternative/igs;
	$ss=~s/animais/animals/igs;
	$ss=~s/animado/animated/igs;
	$ss=~s/anime/anime/igs;
	$ss=~s/antologia/anthology/igs;
	$ss=~s/arquitetura/architecture/igs;
	$ss=~s/arte/art/igs;
	$ss=~s/artes e literatura/arts & literature/igs;
	$ss=~s/automobilismo/auto racing/igs;
	$ss=~s/aviação/aviation/igs;
	$ss=~s/balé/ballet/igs;
	$ss=~s/beisebol/baseball/igs;
	$ss=~s/basquetebol/basketball/igs;
	$ss=~s/Por trás das cenas/behind-the-scenes/igs;
	$ss=~s/motocross de bicicleta/bicycle motocross/igs;
	$ss=~s/big band & swing/big band & swing/igs;
	$ss=~s/biografia/biography/igs;
	$ss=~s/comédia negra/black comedy/igs;
	$ss=~s/blues/blues/igs;
	$ss=~s/bobsledding/bobsledding/igs;
	$ss=~s/fisiculturismo/bodybuilding/igs;
	$ss=~s/Bollywood/Bollywood/igs;
	$ss=~s/boliche/bowling/igs;
	$ss=~s/boxe/boxing/igs;
	$ss=~s/jogo de cartas/card game/igs;
	$ss=~s/desenho animado/cartoon/igs;
	$ss=~s/cheerleading/cheerleading/igs;
	$ss=~s/filme de garota/chick flick/igs;
	$ss=~s/crianças/children/igs;
	$ss=~s/Natal/christmas/igs;
	$ss=~s/cinco de maio/cinco de mayo/igs;
	$ss=~s/circo/circus/igs;
	$ss=~s/música clássica/classical music/igs;
	$ss=~s/comédia/comedy/igs;
	$ss=~s/comédia-drama/comedy-drama/igs;
	$ss=~s/vinda de idade/coming of age/igs;
	$ss=~s/compilação/compilation/igs;
	$ss=~s/show/concert/igs;
	$ss=~s/construção/construction/igs;
	$ss=~s/costureiro/costumer/igs;
	$ss=~s/música country/country music/igs;
	$ss=~s/sala de audiências/courtroom/igs;
	$ss=~s/Grilo/cricket/igs;
	$ss=~s/crime/crime/igs;
	$ss=~s/drama do crime/crime drama/igs;
	$ss=~s/clássico cult/cult classic/igs;
	$ss=~s/ondulação/curling/igs;
	$ss=~s/ciclismo/cycling/igs;
	$ss=~s/dança/dance/igs;
	$ss=~s/dardos/darts/igs;
	$ss=~s/namoro/dating/igs;
	$ss=~s/docudrama/docudrama/igs;
	$ss=~s/documentário/documentary/igs;
	$ss=~s/exposição de cães/dog show/igs;
	$ss=~s/corrida de arrancada/drag racing/igs;
	$ss=~s/drama/drama/igs;
	$ss=~s/Páscoa/easter/igs;
	$ss=~s/emo/emo/igs;
	$ss=~s/Engenharia/engineering/igs;
	$ss=~s/entretenimento/entertainment/igs;
	$ss=~s/meio Ambiente/environment/igs;
	$ss=~s/espionagem/espionage/igs;
	$ss=~s/Esportes extremos/extreme sports/igs;
	$ss=~s/família/family/igs;
	$ss=~s/fantasia/fantasy/igs;
	$ss=~s/moda/fashion/igs;
	$ss=~s/esgrima/fencing/igs;
	$ss=~s/patinação artística/figure skating/igs;
	$ss=~s/filmado no local/filmed on location/igs;
	$ss=~s/música folclórica/folk music/igs;
	$ss=~s/futebol/football/igs;
	$ss=~s/gay e lésbica/gay and lesbian/igs;
	$ss=~s/golfe/golf/igs;
	$ss=~s/música gospel/gospel music/igs;
	$ss=~s/gótico/goth/igs;
	$ss=~s/verde/green/igs;
	$ss=~s/filme de cara/guy flick/igs;
	$ss=~s/ginástica/gymnastics/igs;
	$ss=~s/dia das Bruxas/halloween/igs;
	$ss=~s/hanukkah/hanukkah/igs;
	$ss=~s/metal pesado/heavy metal/igs;
	$ss=~s/hip-hop e rap/hip-hop & rap/igs;
	$ss=~s/história/history/igs;
	$ss=~s/hóquei/hockey/igs;
	$ss=~s/Horror/horror/igs;
	$ss=~s/corrida de cavalo/horse racing/igs;
	$ss=~s/hospital/hospital/igs;
	$ss=~s/imax/imax/igs;
	$ss=~s/Internet/internet/igs;
	$ss=~s/jazz/jazz/igs;
	$ss=~s/andar de jet ski/jet skiing/igs;
	$ss=~s/kwanzaa/kwanzaa/igs;
	$ss=~s/lacrosse/lacrosse/igs;
	$ss=~s/latino/latin/igs;
	$ss=~s/legal/legal/igs;
	$ss=~s/literatura/literature/igs;
	$ss=~s/local/local/igs;
	$ss=~s/Magia/magic/igs;
	$ss=~s/Artes marciais/martial arts/igs;
	$ss=~s/remédio/medicine/igs;
	$ss=~s/militares/military/igs;
	$ss=~s/ml dia do rei/ml king day/igs;
	$ss=~s/mockumentary/mockumentary/igs;
	$ss=~s/esportes a motor/motor sports/igs;
	$ss=~s/corridas de moto/motorcycle racing/igs;
	$ss=~s/música/music/igs;
	$ss=~s/musical/musical/igs;
	$ss=~s/mistério/mystery/igs;
	$ss=~s/mistério e suspense/mystery & suspense/igs;
	$ss=~s/natureza/nature/igs;
	$ss=~s/nova onda e punk/new wave & punk/igs;
	$ss=~s/ópera/opera/igs;
	$ss=~s/de outros/other/igs;
	$ss=~s/ao ar livre/outdoors/igs;
	$ss=~s/paranormal/paranormal/igs;
	$ss=~s/informação da peça na cópia/part info in copy/igs;
	$ss=~s/páscoa/passover/igs;
	$ss=~s/pets/pets/igs;
	$ss=~s/fotografia/photography/igs;
	$ss=~s/piloto/pilot/igs;
	$ss=~s/pôquer/poker/igs;
	$ss=~s/polícia/police/igs;
	$ss=~s/política/politics/igs;
	$ss=~s/piscina/pool/igs;
	$ss=~s/pop/pop/igs;
	$ss=~s/cultura pop clássico/pop culture classic/igs;
	$ss=~s/prequel/prequel/igs;
	$ss=~s/pré escola/preschool/igs;
	$ss=~s/pro wrestling/pro wrestling/igs;
	$ss=~s/perfil/profile/igs;
	$ss=~s/fantoches/puppets/igs;
	$ss=~s/r & b/r&b/igs;
	$ss=~s/Ramadã/ramadan/igs;
	$ss=~s/reggae/reggae/igs;
	$ss=~s/religião/religion/igs;
	$ss=~s/refazer/remake/igs;
	$ss=~s/retrospectivo/retrospective/igs;
	$ss=~s/Rocha/rock/igs;
	$ss=~s/rodeio/rodeo/igs;
	$ss=~s/esportes de rolo/roller sports/igs;
	$ss=~s/romance/romance/igs;
	$ss=~s/remo/rowing/igs;
	$ss=~s/rúgbi/rugby/igs;
	$ss=~s/corrida/running/igs;
	$ss=~s/sátira/satire/igs;
	$ss=~s/Ciência/science/igs;
	$ss=~s/ficção científica/sci-fi/igs;
	$ss=~s/sequela/sequel/igs;
	$ss=~s/assunto curto/short subject/igs;
	$ss=~s/mostrar músicas/show tunes/igs;
	$ss=~s/silencioso/silent/igs;
	$ss=~s/skate/skateboarding/igs;
	$ss=~s/esquiar/skiing/igs;
	$ss=~s/snowboard/snowboarding/igs;
	$ss=~s/snowmobile/snowmobiling/igs;
	$ss=~s/futebol/soccer/igs;
	$ss=~s/softball/softball/igs;
	$ss=~s/alma/soul/igs;
	$ss=~s/espaço/space/igs;
	$ss=~s/speedskating/speedskating/igs;
	$ss=~s/dia de São Patricio/st patricks day/igs;
	$ss=~s/comédia em Pé/stand-up comedy/igs;
	$ss=~s/Olimpíadas de verão/summer olympics/igs;
	$ss=~s/luta de sumô/sumo wrestling/igs;
	$ss=~s/surfe/surfing/igs;
	$ss=~s/suspense \/ suspense/suspense\/thriller/igs;
	$ss=~s/natação/swimming/igs;
	$ss=~s/tecnologia/technology/igs;
	$ss=~s/adolescentes/teens/igs;
	$ss=~s/tênis/tennis/igs;
	$ss=~s/Ação de graças/thanksgiving/igs;
	$ss=~s/oportuno/timely/igs;
	$ss=~s/a ser anunciado/to be announced/igs;
	$ss=~s/trens/trains/igs;
	$ss=~s/viagem/travel/igs;
	$ss=~s/tributo/tribute/igs;
	$ss=~s/tvg classic/tvg classic/igs;
	$ss=~s/Dia dos namorados/valentines day/igs;
	$ss=~s/voleibol/volleyball/igs;
	$ss=~s/guerra/war/igs;
	$ss=~s/Casamento/wedding/igs;
	$ss=~s/ocidental/western/igs;
	$ss=~s/Olimpíadas de Inverno/winter olympics/igs;
	$ss=~s/luta livre/wrestling/igs;
	$ss=~s/yom kippur/yom kippur/igs;
	
	$ss=~s/Actualiteit/Actuality/igs;
	$ss=~s/Nieuws/News/igs;
	$ss=~s/Amusement/Entertainment/igs;
	$ss=~s/Talkshow/Talk show/igs;
	$ss=~s/Soap/Soap/igs;
	$ss=~s/Spelprogramma/Game program/igs;
	$ss=~s/Muziek en kunst/Music and art/igs;
	$ss=~s/Film/Movie/igs;
	$ss=~s/Drama/Drama/igs;
	$ss=~s/Komedie/Comedy/igs;
	$ss=~s/Romantische film/Romantic movie/igs;
	$ss=~s/Consument/Consumer/igs;
	$ss=~s/Vrije Tijd/Leisure/igs;
	$ss=~s/Autoprogramma/Car program/igs;
	$ss=~s/Reizen/To travel/igs;
	$ss=~s/Documentaire/Documentary/igs;
	$ss=~s/Ontspanning/Relaxation/igs;
	$ss=~s/Koken/Cook/igs;
	$ss=~s/Gezondheid/Health/igs;
	$ss=~s/Educatie/Education/igs;
	$ss=~s/Tuinieren/Gardening/igs;
	$ss=~s/Reality TV/Reality TV/igs;
	$ss=~s/Muzikale Biografie/Musical Biography/igs;
	$ss=~s/Filmmagazine/Filmmagazine/igs;
	$ss=~s/Fantasie/Fantasy/igs;
	$ss=~s/Erotiek/Eroticism/igs;
	$ss=~s/Romantische Komedie/Romantic comedy/igs;
	$ss=~s/Bijbels epos/Biblical epic/igs;
	$ss=~s/Historische drama/Historical drama/igs;
	$ss=~s/Actie/Action/igs;
	$ss=~s/Biografie/Biography/igs;
	$ss=~s/Sport/Sport/igs;
	$ss=~s/Avontuur/Adventure/igs;
	
	# $ss=~s/Action/action\/adventure/igs;
	# $ss=~s/Actuality/reality/igs;
	# $ss=~s/Adventure/action\/adventure/igs;
	# $ss=~s/Biblical epic//igs;
	# $ss=~s/Biography/biography/igs;
	# $ss=~s/Comedy/comedy/igs;
	# $ss=~s/Cook/cooking/igs;
	# $ss=~s/Documentary/documentary/igs;
	# $ss=~s/Drama/drama/igs;
	# $ss=~s/Education/educational/igs;
	# $ss=~s/Entertainment/entertainment/igs;
	# $ss=~s/Fantasy/fantasy/igs;
	# $ss=~s/Gardening/gardening/igs;
	# $ss=~s/Health/health/igs;
	# $ss=~s/Historical drama/drama/igs;
	# $ss=~s/Moviemagazine/magazine/igs;
	# $ss=~s/Reality TV/reality/igs;
	# $ss=~s/Romantische Comedy/romance/igs;
	# $ss=~s/Romantische Movie/romance/igs;
	# $ss=~s/Soap/soap opera/igs;
	# $ss=~s/Talkconcert/talk/igs;
	# $ss=~s/To travel/travel/igs;
	
	return $ss;
}

sub Genre_list()
{		
	my $genre1=shift;	
	my $cmp_se_key=0;
	if($genre1 ne "")
	{
		foreach my $c_l(@kylist)
		{   
			my ($type,$c_l2,$catg)=split('\|',$c_l);
			$c_l2= &trim($c_l2);
			$genre1= &trim($genre1);
			
			# print $c_l2;
			
			if (lc($c_l2) eq lc($genre1))
			{
				$cmp_se_key=1;
				last;
			}
		}
		
		$cmp_se_key="Verify KeyWord" if($cmp_se_key == 0);
		$cmp_se_key="" if($cmp_se_key == 1);
	}
	return $cmp_se_key;
}


sub counrty_list()
{
	my $prev_contry	=shift;
	my $cmp_ctry;
	
	print "prev_contry:: $prev_contry\n";
	print length($prev_contry);
	if($prev_contry ne "")
	{
	# my @ctry_list=("Russian Federation","Afghanistan","Africa","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua And Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Austria-Hungary","Azerbaijan","Bahamas","Bahrain","Bali","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia","Bosnia And Herzegovina","Botswana","Bouvet Island","Brazil","Britain","British Indian Ocean Territory","British Virgin Islands","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Byelarus","Cambodia","Cameroon","Canada","Cape Verde","Caribs","Cayman Islands","Central African Republic","Chad","Channel Islands","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Congo (Brazzaville)","Cook Islands","Costa Rica","Croatia","Cuba","Curaçao","Cyprus","Czech Republic","Czechoslovakia","Denmark","Djibouti","Dominica","Dominican Republic","Dubai","East Germany","East Timor","Ecuador","Egypt","El Salvador","England","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Europe","Falkland Islands (Malvinas)","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia (Republic)","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island And Mcdonald Islands","Holy See (Vatican City State)","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Ivory Coast","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","Korea","Kosovo","Kuwait","Kyrgyz Republic","Lao Peoples Democratic Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Libyan Arab Jamahiriya","Liechtenstein","Lithuania","Luxembourg","Macao","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Middle East","Moldova","Moldova; Republic Of","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea (Korean Peoples Democratic Republic)","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestine","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Réunion","Romania","Russia","Russian Federation","Rwanda","Saint Helena","Saint Kitts And Nevis","Saint Pierre And Miquelon","Saint Vincent And The Grenadines","Samoa","San Marino","Sao Tome And Principe","Saudi Arabia","Scotland","Senegal","Serbia","Serbia and Montenegro","Seychelles","Sierra Leone","Singapore","Slovak Republic","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia And S. Sandwich Islands","South Korea","Soviet Union","Spain","Sri Lanka","St. Lucia","Sudan","Suriname","Svalbard And Jan Mayen","Swaziland","Sweden","Switzerland","Syria","Syrian Arab Republic","Tahiti","Taiwan","Tajikistan","Tanzania","Thailand","Tibet","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks And Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Arab Republic","United Kingdom","United States","United States Minor Outlying Islands","Upper Volta","Uruguay","US Virgin Islands","Uzbekistan","Vanuatu","Venezuela","Vietnam","Wales","Wallis And Futuna","West Germany","Western Sahara","Yemen","Yugoslavia","Zaire","Zambia","Zimbabwe");
	
	
	foreach my $c_l(@ctry_list)
	{   

		$c_l= &trim($c_l);
		$prev_contry= &trim($prev_contry);
		# print "c_l:: ".length($c_l)."\n";
		# print "prev_contry:: ".length($prev_contry)."\n";
		if (lc($c_l) eq lc($prev_contry))
		{
			$cmp_ctry=1;
			print "cmp_ctry:: $cmp_ctry\n";
			last;
		}
	}
	
	$cmp_ctry="Verify Country" if($cmp_ctry != 1);
	$cmp_ctry="" if($cmp_ctry == 1);
	}
	print "cmp_ctry:: $cmp_ctry\n";
	return $cmp_ctry;
}	

sub movie_key_list()
{
	my $genre1=shift;
	my $cmp_mv_key;
	if($genre1 ne "")
	{
	my @mv_key_list=("Movie|3D|Additional","Movie|action sports|Additional","Movie|action/adventure|PrimaryAdditional","Movie|adaptation|Additional","Movie|adult|PrimaryAdditional","Movie|alternate voice-over|Additional","Movie|alternative|Additional","Movie|animals|Additional","Movie|animated|Additional","Movie|anime|Additional","Movie|anthology|Additional","Movie|architecture|Additional","Movie|art|Additional","Movie|arts & literature|Primary","Movie|auto racing|Additional","Movie|aviation|Additional","Movie|baby|Additional","Movie|ballet|Additional","Movie|baseball|Additional","Movie|basketball|Additional","Movie|behind-the-scenes|Additional","Movie|belgian national day|Additional","Movie|belgium german-speaking community|Additional","Movie|bicycle motocross|Additional","Movie|big band & swing|Additional","Movie|biography|Primary","Movie|black comedy|Additional","Movie|bloopers|Additional","Movie|blues|Additional","Movie|bobsledding|Additional","Movie|bodybuilding|Additional","Movie|Bollywood|Additional","Movie|bonus content|Additional","Movie|bowling|Additional","Movie|boxing|Additional","Movie|brussels community|Additional","Movie|camping|Additional","Movie|candid camera/spy cam|Additional","Movie|card game|Additional","Movie|cartoon|Additional","Movie|celebrities|Additional","Movie|cheerleading|Additional","Movie|chick flick|Additional","Movie|children|Additional","Movie|christmas|Additional","Movie|cinco de mayo|Additional","Movie|circus|Additional","Movie|civil rights|Additional","Movie|Civil War|Additional","Movie|classical music|Additional","Movie|colorized|Additional","Movie|comedy|PrimaryAdditional","Movie|comedy-drama|PrimaryAdditional","Movie|comic books|Additional","Movie|coming of age|Additional","Movie|community|Additional","Movie|compilation|Additional","Movie|concert|Additional","Movie|construction|Additional","Movie|costumer|Additional","Movie|country music|Additional","Movie|courtroom|Additional","Movie|cricket|Additional","Movie|crime|Additional","Movie|crime drama|PrimaryAdditional","Movie|cult classic|Additional","Movie|curling|Additional","Movie|cycling|Additional","Movie|dance|Additional","Movie|darts|Additional","Movie|dating|Additional","Movie|day of the flemish community|Additional","Movie|day of the french community|Additional","Movie|day of the king|Additional","Movie|director's cut|Additional","Movie|disaster|Additional","Movie|divorce|Additional","Movie|docudrama|Additional","Movie|documentary|PrimaryAdditional","Movie|dog show|Additional","Movie|drag racing|Additional","Movie|drama|PrimaryAdditional","Movie|drugs|Additional","Movie|dubbed|Additional","Movie|easter|Additional","Movie|economy|Additional","Movie|emo|Additional","Movie|engineering|Additional","Movie|entertainment|Additional","Movie|environment|Additional","Movie|erotic drama|Additional","Movie|espionage|Additional","Movie|extended version|Additional","Movie|extreme sports|Additional","Movie|family|Additional","Movie|family issues|Additional","Movie|fantasy|PrimaryAdditional","Movie|fashion|Additional","Movie|feminism|Additional","Movie|fencing|Additional","Movie|figure skating|Additional","Movie|filmed on location|Additional","Movie|final cut|Additional","Movie|flemish community|Additional","Movie|folk music|Additional","Movie|food|Additional","Movie|football|Additional","Movie|france national day|Additional","Movie|golf|Additional","Movie|Gongdou|Additional","Movie|gospel music|Additional","Movie|goth|Additional","Movie|green|Additional","Movie|guy flick|Additional","Movie|gymnastics|Additional","Movie|halloween|Additional","Movie|hanukkah|Additional","Movie|hardcore content|Additional","Movie|heavy metal|Additional","Movie|hip-hop & rap|Additional","Movie|history|Additional","Movie|hockey|Additional","Movie|homage|Additional","Movie|horror|PrimaryAdditional","Movie|horse racing|Additional","Movie|hospital|Additional","Movie|imax|Additional","Movie|internet|Additional","Movie|investigative journalism|Additional","Movie|investment|Additional","Movie|jazz|Additional","Movie|jet skiing|Additional","Movie|jidaigeki|Additional","Movie|july 4th|Additional","Movie|Kollywood|Additional","Movie|kwanzaa|Additional","Movie|lacrosse|Additional","Movie|latin|Additional","Movie|legal|Additional","Movie|LGBTQ|Additional","Movie|literature|Additional","Movie|local|Additional","Movie|local cultures|Additional","Movie|magic|Additional","Movie|martial arts|PrimaryAdditional","Movie|medicine|Additional","Movie|military|Additional","Movie|ml king day|Additional","Movie|mockumentary|Additional","Movie|modernized copy|Additional","Movie|Mollywood|Additional","Movie|motor sports|Additional","Movie|motorcycle racing|Additional","Movie|mountaineering|Additional","Movie|music|PrimaryAdditional","Movie|musical|PrimaryAdditional","Movie|mystery|Additional","Movie|mystery & suspense|Primary","Movie|nature|Additional","Movie|new wave & punk|Additional","Movie|opera|Additional","Movie|organized crime|Additional","Movie|other|PrimaryAdditional","Movie|outdoors|Additional","Movie|paranormal|PrimaryAdditional","Movie|parody/spoof|Additional","Movie|part info in copy|Additional","Movie|passover|Additional","Movie|personal finance|Additional","Movie|pets|Additional","Movie|photography|Additional","Movie|pilot|Additional","Movie|podcast|Additional","Movie|poker|Additional","Movie|police|Additional","Movie|politics|Additional","Movie|pool|Additional","Movie|pop|Additional","Movie|pop culture classic|Additional","Movie|prequel|Additional","Movie|preschool|Additional","Movie|preteen|Additional","Movie|pro wrestling|Additional","Movie|profile|Additional","Movie|puppets|Additional","Movie|r&b|Additional","Movie|radio all news|Additional","Movie|radio classical|Additional","Movie|radio pop rock|Additional","Movie|radio talk|Additional","Movie|ramadan|Additional","Movie|reboot/reimagining|Additional","Movie|reggae|Additional","Movie|religion|Additional","Movie|remake|Additional","Movie|rescue|Additional","Movie|restored|Additional","Movie|retrospective|Additional","Movie|rock|Additional","Movie|rodeo|Additional","Movie|roller sports|Additional","Movie|romance|Additional","Movie|rowing|Additional","Movie|royals & gotha|Additional","Movie|rugby|Additional","Movie|running|Additional","Movie|Sandalwood|Additional","Movie|satire|Additional","Movie|sci-fi|PrimaryAdditional","Movie|science|Additional","Movie|September 11|Additional","Movie|sequel|Additional","Movie|short subject|Additional","Movie|short version|Additional","Movie|show tunes|Additional","Movie|silent|Additional","Movie|sing-a-long|Additional","Movie|sinterklaas/saint nicolas|Additional","Movie|skateboarding|Additional","Movie|sketch comedy|Additional","Movie|skiing|Additional","Movie|snowboarding|Additional","Movie|snowmobiling|Additional","Movie|soccer|Additional","Movie|social issues|Additional","Movie|softball|Additional","Movie|soul|Additional","Movie|space|Additional","Movie|special edition|Additional","Movie|speedskating|Additional","Movie|spin-off|Additional","Movie|st patricks day|Additional","Movie|stand-up comedy|Additional","Movie|stock markets|Additional","Movie|summer olympics|Additional","Movie|sumo wrestling|Additional","Movie|superheroes|Additional","Movie|surfing|Additional","Movie|suspense/thriller|Additional","Movie|swimming|Additional","Movie|technology|Additional","Movie|teens|Additional","Movie|tennis|Additional","Movie|terrorism|Additional","Movie|thanksgiving|Additional","Movie|timely|Additional","Movie|to be announced|Additional","Movie|Tollywood|Additional","Movie|trading|Additional","Movie|trains|Additional","Movie|travel|Additional","Movie|tribute|Additional","Movie|troubled relationships|Additional","Movie|tvg classic|Additional","Movie|uncut|Additional","Movie|unrated/not rated|Additional","Movie|valentines day|Additional","Movie|video games|Additional","Movie|volleyball|Additional","Movie|walloon community|Additional","Movie|war|Additional","Movie|wealth|Additional","Movie|web content|Additional","Movie|wedding|Additional","Movie|western|PrimaryAdditional","Movie|wildlife|Additional","Movie|winter olympics|Additional","Movie|workplace|Additional","Movie|world cup soccer|Additional","Movie|wrestling|Additional","Movie|Wuxia|Additional","Movie|yom kippur|Additional","Movie|action/adventure|Primary Keyword","Movie|adult|Primary Keyword","Movie|arts & literature|Primary Keyword","Movie|biography|Primary Keyword","Movie|comedy|Primary Keyword","Movie|comedy-drama|Primary Keyword","Movie|crime drama|Primary Keyword","Movie|documentary|Primary Keyword","Movie|drama|Primary Keyword","Movie|fantasy|Primary Keyword","Movie|horror|Primary Keyword","Movie|martial arts|Primary Keyword","Movie|music|Primary Keyword","Movie|musical|Primary Keyword","Movie|mystery & suspense|Primary Keyword","Movie|paranormal|Primary Keyword","Movie|sci-fi|Primary Keyword","Movie|western|Primary Keyword","Movie|adult|Primary Keyword","Movie|horror|Primary Keyword","Movie|drama|Primary Keyword","Movie|drama|Primary Keyword","Movie|drama|Primary Keyword","Movie|mystery & suspense|Primary Keyword","Movie|mystery & suspense|Primary Keyword","Movie|mystery & suspense|Primary Keyword","Movie|action/adventure|Primary Keyword","Movie|action/adventure|Primary Keyword","Movie|romance|Additional Keyword","Movie|art|Additional Keyword","Movie|ballet|Additional Keyword","Movie|opera|Additional Keyword","Movie|suspense/thriller|Additional Keyword","Movie|suspense/thriller|Additional Keyword","Movie|mystery|Additional Keyword","Movie|war|Additional Keyword");
	
	foreach my $c_l(@mv_key_list)
	{   
		my ($type,$c_l2,$catg)=split('\|',$c_l);
		$c_l2= &trim($c_l2);
		$genre1= &trim($genre1);
		if (lc($c_l2) eq lc($genre1))
		{
			$cmp_mv_key=1;
			last;
		}
	}
	
	$cmp_mv_key="Verify Movie KeyWord" if($cmp_mv_key eq "");
	$cmp_mv_key="" if($cmp_mv_key == 1);
	}
	return $cmp_mv_key;
}


sub Laguage_list()
{
	my $genre=shift;
	my $cmp_lan_key;
	if($genre ne "")
	{
	foreach my $c_l(@Lang_list)
	{   

		$c_l= &trim($c_l);
		$genre= &trim($genre);
		if (lc($c_l) eq lc($genre))
		{
			$cmp_lan_key=1;
			last;
		}
	}
	
	$cmp_lan_key="Verify Program Language" if($cmp_lan_key eq "");
	$cmp_lan_key="" if($cmp_lan_key == 1);
	}
	
	return $cmp_lan_key;
}


sub get_max_row
{
	my $fname = shift;
	my $max_row;
	my $workbook = Spreadsheet::XLSX -> new ($fname);
	my $c = 0;
	foreach my $sheet (@{$workbook -> {Worksheet}}) 
	{
		$c++;
		if ($c == 1){
			$max_row = $sheet -> {MaxRow}; # Returns row of last cell 
			return $max_row;
		}		
	}
}


sub write_to_excel_with_row_value
{
	my $fname = shift;
	my $sname = shift;	
	my $rv = shift;
	my $data = shift;
	my @j = split("\t",$data);
	my $workbook = Excel::Writer::XLSX->new($fname);    
	# my $worksheet = $workbook->sheets($snum);
	my $worksheet = $workbook->get_worksheet_by_name($sname);
	my $cv = -1;
	foreach my $j(@j){
		$cv++;
		$worksheet->write( $rv, $cv, $j);
	}
}



sub write_title_to_excel
{
	my $fname = shift;
	my $title_list = shift;
	my @j = split("\t",$title_list);
	my $workbook = Excel::Writer::XLSX->new($fname);
	my $worksheet = $workbook->add_worksheet();                
	# my $format = $workbook->add_format();
	# $format->set_bg_color( 'red' );
	my $cv = -1;
	foreach my $j(@j){
		$cv++;
		$worksheet->write( 0, $cv, $j);
	}
}