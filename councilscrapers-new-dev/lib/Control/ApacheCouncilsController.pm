package ApacheCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use File::Path qw/make_path/;
use DateTime;
use URI::Escape;
use WWW::Mechanize;
use HTTP::Cookies;
use IO::Socket::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Apache');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');
my $dataLogsDirectoryPath = ($basePath.'/data');


require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Apache_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Apache_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Apache_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Apache_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');

###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType, $session_id);
				
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Apache_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Apache_Decision";
	}	
	
	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	  # if($currentCouncilCode=~m/^172$/is)
	  # {
		# exit;
	  # }
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
	
		exit;
	}
		
	my $rerunCount=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
	if($currentCouncilCode=~m/^339$/is)
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# New Proxy
	}
	elsif($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working via Old proxy..\n";
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy
	}
	
	my $searchURL = $commonFileDetails->{$currentCouncilCode}->{'URL'};
	
	print "Search Page URL==>$searchURL\n";
	
	($session_id) = &CouncilsScrapperCore::get_cookie_session_details($searchURL,$councilApp,'JSESSIONID');
	print "session_id=>$session_id\n";
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$councilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	
	$responseContent = $councilApp->get($searchURL);
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}
	
    return($councilApp, $pingStatus, $responseContent);
}

####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $redisConnection, $Schedule_no, $CouncilApp) = @_;	
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}	
	
	
	my $csrToken = $commonFileDetails->{$councilCode}->{'CSRFTOKEN_REGEX'};
	my $nextToken = $commonFileDetails->{$councilCode}->{'TOKEN'};
	my $nextPageViewstate = $commonFileDetails->{$councilCode}->{'NEXT_PAGE_VIEWSTATE'};
	
		
	my ($ViewState,$CSRFToken,$Token);			
	if($TabContent=~m/$nextPageViewstate/is)
	{
		$ViewState=uri_escape($2);
	}
	if($TabContent=~m/$csrToken/is)
	{
		$CSRFToken=uri_escape($1);
	}
	if($TabContent=~m/$nextToken/is)
	{
		$Token=uri_escape($1);
	}
	
		
	my $actualScrappedCount = 0;
	my ($deDup_Query,$insertQuery,$commonContent,$totalAppCount);
	
	$commonContent .= $TabContent;
	
	my $pageCount;
	if($councilCode=~m/^(132|172|323|339|450)$/is)
	{
		my $pCount=0;
		while($TabContent =~ m/$councilDetailsFile->{$councilCode}->{'TOTAL_PAGES_REGEX'}/isg)
		{
			$pCount++;
		}
		$pageCount = $pCount;
	}
	else
	{
		$pageCount = $1 if($TabContent =~ m/$councilDetailsFile->{$councilCode}->{'TOTAL_PAGES_REGEX'}/is);
	}
	
	my $RESULT_URL = $commonFileDetails->{$councilCode}->{'RESULT_URL'};
	
	
	my $pageNumber=2;
	NextPage:
	while($TabContent =~ m/$commonFileDetails->{$councilCode}->{'APPLICATION_REGEX'}/igs)
	{
		my $ID_Reference=$1;
		my $Application_Reference=$2;
		
		my ($Application,$idReference);		
		if($councilCode=~m/^21$/is)
		{
			$idReference = uri_escape($Application_Reference);
			$Application = $ID_Reference;
		}
		else
		{
			$idReference = uri_escape($ID_Reference);
			$Application = $Application_Reference;
		}
		
	
		my ($Address,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing,$pkCode);
		
		my $fullAppPageLink=$commonFileDetails->{$councilCode}->{'URL'};
		my $applicationPostContent=$commonFileDetails->{$councilCode}->{'APPLICATION_POST_CONTENT'};
				
		$fullAppPageLink =~ s/\&amp\;/\&/g;
		$fullAppPageLink =~s/\'/\'\'/igs;
		
		
		if($redisConnection->exists($councilCode."_".$Application))	
		{
			print "Exists : $Application\n";	
			
			my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$Application\', \'$fullAppPageLink\', \'$category\', \'$Source\',\'$councilName\'),";
			$deDup_Query.=$deDupQuery;
			
			next;
		}
		
		$applicationPostContent =~s/<Viewstate>/$ViewState/igs;
		$applicationPostContent =~s/<Token>/$Token/igs;
		$applicationPostContent =~s/<Reference_ID>/$idReference/igs;
		$applicationPostContent =~s/<CSRFToken>/$CSRFToken/igs;
		
		
		my $AppContent = $CouncilApp->post( $RESULT_URL, Content => "$applicationPostContent");
		$AppContent = $CouncilApp->content;
		my $AppContentStatus = $CouncilApp->status;
		
		sleep(2);
		
		if(($Application ne "") && ($AppContentStatus=~m/^\s*200\s*$/is))
		{			
			$actualScrappedCount++;
			
			## Insert new application in redis server			
			$redisConnection->set($councilCode."_".$Application => $Application);
			print "Not Exists : $Application\n";
		}
		
		
		my ($SITE_LOCATION, $PROPOSAL, $REGISTRATION_DATE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICANT_NAME, $DOC_URL, $EASTING, $NORTHING, $GRIDREFERENCE, $AGENT_ADDRESS, $AGENT_NAME, $APPLICANT_ADDRESS, $DATE_TARGET, $PLANNING_APPLICATION);
		
		$PLANNING_APPLICATION = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
		if($councilCode=~m/^(21|172|450)$/is)
		{
			$SITE_LOCATION = $1 if($AppContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
		}
		else
		{
			$SITE_LOCATION = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
		}
		$PROPOSAL = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
		$REGISTRATION_DATE = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
		$RECEIVED_DATE = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
		$VALID_DATE = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
		$APPLICATION_TYPE = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
		$APPLICATION_STATUS = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
		$APPLICANT_NAME = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
		$APPLICANT_ADDRESS = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
		$AGENT_NAME = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
		$AGENT_ADDRESS = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
		$DATE_TARGET = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
		
		
		if($Application eq "")
		{
			if($PLANNING_APPLICATION ne "")
			{
				$Application = $PLANNING_APPLICATION;
			}
		}	
		if($Proposal eq "")
		{
			if($PROPOSAL ne "")
			{
				$Proposal = $PROPOSAL;
			}
		}	
		if($dateApplicationRegistered eq "")
		{
			if($REGISTRATION_DATE ne "")
			{
				$dateApplicationRegistered = $REGISTRATION_DATE;
			}
		}
		if($dateApplicationReceived eq "")
		{
			if($RECEIVED_DATE ne "")
			{
				$dateApplicationReceived = $RECEIVED_DATE;
			}
		}
		if($dateApplicationvalidated eq "")
		{
			if($VALID_DATE ne "")
			{
				$dateApplicationvalidated = $VALID_DATE;
			}
		}	
		if($targetDecdt eq "")
		{
			if($DATE_TARGET ne "")
			{
				$targetDecdt = $DATE_TARGET;
			}
		}	
		if($applicationStatus eq "")
		{
			if($APPLICATION_STATUS ne "")
			{
				$applicationStatus = $APPLICATION_STATUS;
			}
		}		
		if($applicationType eq "")
		{
			if($APPLICATION_TYPE ne "")
			{
				$applicationType = $APPLICATION_TYPE;
			}
		}		
		if($applicantName eq "")
		{
			if($APPLICANT_NAME ne "")
			{
				$applicantName = $APPLICANT_NAME;
			}
		}		
		if($applicantAddress eq "")
		{
			if($APPLICANT_ADDRESS ne "")
			{
				$applicantAddress = $APPLICANT_ADDRESS;
			}
		}				
		if($agentName eq "")
		{
			if($AGENT_NAME ne "")
			{
				$agentName = $AGENT_NAME;
			}
		}			
		if($agentAddress eq "")
		{
			if($AGENT_ADDRESS ne "")
			{
				$agentAddress = $AGENT_ADDRESS;
			}
		}
		
		if($councilCode=~m/^21$/is)
		{	
			if($SITE_LOCATION ne "")
			{
				my $address;
				while($SITE_LOCATION=~m/<td\s*class=\"data\">\s*([^<]*?)\s*<\/td>/isg)
				{
					my $value = $1;
					$address .= $value. ", ";
				}
				$address=~s/\,\s*\,\s*/\,/igs;
				$Address = $address;
				$Address=~s/\,\s*\,/\,/igs;
				$Address=~s/\,\s*$//is;
				$Address = &htmlTagClean($Address);
			}	
		}
		elsif($councilCode=~m/^(172|450)$/is)
		{	
			if($SITE_LOCATION ne "")
			{
				my $address;
				while($SITE_LOCATION=~m/<td\s*class=\"planSResultcol2\">\s*([^<]*?)\s*<\/td>/isg)
				{
					my $value = $1;
					$address .= $value. ", ";
				}
				$address=~s/\,\s*\,\s*/\,/igs;
				$Address = $address;
				$Address=~s/\,\s*\,/\,/igs;
				$Address=~s/\,\s*$//is;
				$Address = &htmlTagClean($Address);
			}	
		}
		else
		{
			if($Address eq "")
			{
				if($SITE_LOCATION ne "")
				{
					$Address = $SITE_LOCATION;
				}
			}
		}		
			
		
		if($documentURL eq "")
		{
			$documentURL = $fullAppPageLink;
		}
			
		$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
		$Address=~s/\n+/ /gsi;
		$Proposal=~s/\n+/ /gsi;
		$applicationStatus=~s/\n+/ /gsi;
		$agentAddress=~s/\n+/ /gsi;
		$agentName=~s/\n+/ /gsi;
		$applicantAddress=~s/\n+/ /gsi;
		$applicantName=~s/\n+/ /gsi;
		$applicationType=~s/\n+/ /gsi;
		$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
		$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
		$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
			
		$insertQuery.="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$fullAppPageLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
		
		undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $fullAppPageLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
		
	}
	
	if($commonFileDetails->{$councilCode}->{'NEXT_PAGE_REGEX'} ne "")
	{
		if($TabContent=~m/$commonFileDetails->{$councilCode}->{'NEXT_PAGE_REGEX'}/is)
		{
			my $Page_Index=uri_escape($1);
			my $Page_Index_ID=uri_escape($2);
			
			my $nextPostContent = $commonFileDetails->{$councilCode}->{'NEXT_POST_CONTENT'};
						
			$nextPostContent =~s/<ViewState>/$ViewState/igs;
			$nextPostContent =~s/<Token>/$Token/igs;
			$nextPostContent =~s/<Page_Index>/$Page_Index/igs;
			$nextPostContent =~s/<Page_Index_ID>/$Page_Index_ID/igs;
			$nextPostContent =~s/<CSRFToken>/$CSRFToken/igs;
			
			my $councilAppResponse = $CouncilApp->post( $RESULT_URL, Content => "$nextPostContent");
			$councilAppResponse = $CouncilApp->content;			
			$TabContent = $councilAppResponse;
			
			if($pageCount >= $pageNumber)
			{			
				print "TotalPage::$pageCount <==> SearchNextPage::$pageNumber\n";
				
				$commonContent .= $TabContent;
				
				$nextPostContent =~s/$Page_Index/<Page_Index>/igs;
				$nextPostContent =~s/$Page_Index_ID/<Page_Index_ID>/igs;
				$nextPostContent =~s/$ViewState/<ViewState>/igs;
				
				if($TabContent=~m/$nextPageViewstate/is)
				{
					$ViewState=uri_escape($2);
				}		
				
				$pageNumber++;
				goto NextPage;
			}
		}
	}
	
	
    # Get number of results
	if($totalAppCount eq "")
	{
		my $resultcnt=0;
		while($commonContent =~ m/$councilDetailsFile->{$councilCode}->{'APPLICATION_REGEX'}/igs)
		{
			$resultcnt++;
		}
		$totalAppCount = $resultcnt;
	}
	
	if($totalAppCount eq "")
	{
		$totalAppCount = $1 if($commonContent =~ m/$regexFile->{'MainRegexControl'}->{'SEARCH_RESULTS_COUNT_RX_NEW'}/is);
	}
	
    print "TotalApplicationSearchResult: $totalAppCount\n";
	
	
	my $rawFileName=&rawFileName($scheduleDateTime,$councilCode,$Source,$category);	
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$commonContent,$searchLogsDirectoryPath,$category,'Success');	
	
	
	print "ActualScrappedCount==>${actualScrappedCount}::::TotalAppliationCount==>$totalAppCount\n"; 
		
	return($insertQuery,$actualScrappedCount,$totalAppCount,$deDup_Query);		
}

####
# URL Clean
####
sub URLclean()
{
	my $url=shift;
	
	$url=~s/amp\;//igs;
	$url=~s/\&nbsp\;/ /igs;
	$url=~s/\s\s+/ /igs;
	$url=~s/\'/\'\'/igs;
	$url=~s/\&\#xD\;\&\#xA\;//igs;	
	$url=~s/\s+/ /igs;
	$url=~s/\&\s*$//igs;
	$url=~s/^\.\.\/\.\.\/\.\.\//\//igs;
	$url=~s/^\s+|\s+$//igs;

	return $url; 
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $redisConnection, $Schedule_no, $CouncilApp) = @_;	

	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	
	my $csrToken = $commonFileDetails->{$councilCode}->{'CSRFTOKEN_REGEX'};
	my $nextToken = $commonFileDetails->{$councilCode}->{'TOKEN'};
	my $nextPageViewstate = $commonFileDetails->{$councilCode}->{'NEXT_PAGE_VIEWSTATE'};
	
		
	my ($ViewState,$CSRFToken,$Token);			
	if($TabContent=~m/$nextPageViewstate/is)
	{
		$ViewState=uri_escape($2);
	}
	if($TabContent=~m/$csrToken/is)
	{
		$CSRFToken=uri_escape($1);
	}
	if($TabContent=~m/$nextToken/is)
	{
		$Token=uri_escape($1);
	}
	
		
	my $actualScrappedCount = 0;
	my ($deDup_Query,$insertQuery,$commonContent,$totalAppCount);
	
	$commonContent .= $TabContent;
	
	my $pageCount;
	if($councilCode=~m/^(132|172|323|339|450)$/is)
	{
		my $pCount=0;
		while($TabContent =~ m/$councilDetailsFile->{$councilCode}->{'TOTAL_PAGES_REGEX'}/isg)
		{
			$pCount++;
		}
		$pageCount = $pCount;
	}
	else
	{
		$pageCount = $1 if($TabContent =~ m/$councilDetailsFile->{$councilCode}->{'TOTAL_PAGES_REGEX'}/is);
	}
	my $RESULT_URL = $commonFileDetails->{$councilCode}->{'RESULT_URL'};
	
	my $pageNumber=2;
	NextPage:
	while($TabContent=~m/$commonFileDetails->{$councilCode}->{'APPLICATION_REGEX'}/igs)
	{
		my $ID_Reference=$1;
		my $Application_Reference=$2;
		
		my ($Application,$idReference);		
		if($councilCode=~m/^21$/is)
		{
			$idReference = uri_escape($Application_Reference);
			$Application = $ID_Reference;
		}
		else
		{
			$idReference = uri_escape($ID_Reference);
			$Application = $Application_Reference;
		}
		
		my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application_Status, $sourceWithTime, $pkCode, $applicationLink);
		
		
		my $fullAppPageLink=$commonFileDetails->{$councilCode}->{'URL'};
		my $applicationPostContent=$commonFileDetails->{$councilCode}->{'APPLICATION_POST_CONTENT'};
				
		$fullAppPageLink =~ s/\&amp\;/\&/g;
		$fullAppPageLink =~s/\'/\'\'/igs;
		
		
		if($redisConnection->exists("D_".$councilCode."_".$Application))	
		{
			print "Exists : $Application\n";	
			
			my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$Application\', \'$fullAppPageLink\', \'$category\', \'$Source\',\'$councilName\'),";
			$deDup_Query.=$deDupQuery;
			
			next;
		}
		
		$applicationPostContent =~s/<Viewstate>/$ViewState/igs;
		$applicationPostContent =~s/<Token>/$Token/igs;
		$applicationPostContent =~s/<Reference_ID>/$idReference/igs;
		$applicationPostContent =~s/<CSRFToken>/$CSRFToken/igs;
		
		
		my $AppContent = $CouncilApp->post( $RESULT_URL, Content => "$applicationPostContent");
		$AppContent = $CouncilApp->content;
		my $AppContentStatus = $CouncilApp->status;
		sleep(2);				
		if(($Application ne "") && ($AppContentStatus=~m/^\s*200\s*$/is))
		{			
			$actualScrappedCount++;
			
			## Insert new application in redis server			
			$redisConnection->set("D_".$councilCode."_".$Application => $Application);
			print "Not Exists : $Application\n";
		}
		
		
		my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_STATUS_NEW);
		
		$PLANNING_APPLICATION = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
		$PROPOSAL = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
		$DECISION_DATE = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
		$DECISION_STATUS = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
		$APPLICATION_STATUS = &htmlTagClean($1) if($AppContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
		
		
		if($Application eq "")
		{
			if($PLANNING_APPLICATION ne "")
			{
				$Application = $PLANNING_APPLICATION;
			}
		}	
		if($Proposal eq "")
		{
			if($PROPOSAL ne "")
			{
				$Proposal = $PROPOSAL;
			}
		}	
		if($Decision_Issued_Date eq "")
		{
			if($DECISION_DATE ne "")
			{
				$Decision_Issued_Date = $DECISION_DATE;
			}
		}
		if($Application_Status eq "")
		{
			if($APPLICATION_STATUS ne "")
			{
				$Application_Status = $APPLICATION_STATUS;
			}
		}	
		if($Decision_Status eq "")
		{
			if($DECISION_STATUS ne "")
			{
				$Decision_Status = $DECISION_STATUS;
			}
		}
		
		
		if($councilCode=~m/^450$/is)
		{
			if(($Date_Decision_Made=~m/^\s*$/is) || ($Date_Decision_Made eq ""))
			{
				
				my ($ViewState,$CSRFToken);			
				if($AppContent=~m/$nextPageViewstate/is)
				{
					$ViewState=uri_escape($2);
				}
				if($AppContent=~m/$csrToken/is)
				{
					$CSRFToken=uri_escape($1);
				}	
				
				my $decDatePostCnt = "javax.faces.ViewState=$ViewState&_id413%3A_id441=Documents&_id413_SUBMIT=1&Civica.CSRFToken=$CSRFToken&_id413%3A_idcl=&_id413%3A_link_hidden_=";
				
				my $AppDocContent = $CouncilApp->post( 'http://planning.wrexham.gov.uk/Planning/lg/GFPlanningSingleResult.page', Content => "$decDatePostCnt");
				$AppDocContent = $CouncilApp->content;
				# my $AppContentStatus = $CouncilApp->status;
				$Date_Decision_Made = &htmlTagClean($1) if($AppDocContent=~m/<td[^>]*?>\s*PLN\s*Decision\s*\-\s*Web\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
				print "450_DecisionDate==>$Date_Decision_Made\n";
			}
		}	
		
		
		$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
		$Proposal=~s/\n+/ /gsi;
		$Application_Status=~s/\n+/ /gsi;
		$Decision_Status=~s/\n+/ /gsi;
		
		$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
		$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
		$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
		$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
		$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
		
		
		$insertQuery.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$fullAppPageLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
		
		undef $Application;  undef $Proposal;  undef $Application_Status;  undef $Decision_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $fullAppPageLink;
	}
	
	if($commonFileDetails->{$councilCode}->{'NEXT_PAGE_REGEX'} ne "")
	{
		if($TabContent=~m/$commonFileDetails->{$councilCode}->{'NEXT_PAGE_REGEX'}/is)
		{
			my $Page_Index=uri_escape($1);
			my $Page_Index_ID=uri_escape($2);
			
			my $nextPostContent = $commonFileDetails->{$councilCode}->{'NEXT_POST_CONTENT'};
			
			$nextPostContent =~s/<ViewState>/$ViewState/igs;
			$nextPostContent =~s/<Token>/$Token/igs;
			$nextPostContent =~s/<Page_Index>/$Page_Index/igs;
			$nextPostContent =~s/<Page_Index_ID>/$Page_Index_ID/igs;
			$nextPostContent =~s/<CSRFToken>/$CSRFToken/igs;
			
			my $councilAppResponse = $CouncilApp->post( $RESULT_URL, Content => "$nextPostContent");
			$councilAppResponse = $CouncilApp->content;			
			$TabContent = $councilAppResponse;
			
			if($pageCount >= $pageNumber)
			{			
				print "TotalPage::$pageCount <==> SearchNextPage::$pageNumber\n";
				
				$commonContent .= $TabContent;
				
				$nextPostContent =~s/$Page_Index/<Page_Index>/igs;
				$nextPostContent =~s/$Page_Index_ID/<Page_Index_ID>/igs;
				$nextPostContent =~s/$ViewState/<ViewState>/igs;
				
				if($TabContent=~m/$nextPageViewstate/is)
				{
					$ViewState=uri_escape($2);
				}		
				
				$pageNumber++;
				goto NextPage;
			}
		}
	}
	
	
    # Get number of results
	if($totalAppCount eq "")
	{
		my $resultcnt=0;
		while($commonContent =~ m/$councilDetailsFile->{$councilCode}->{'APPLICATION_REGEX'}/igs)
		{
			$resultcnt++;
		}
		$totalAppCount = $resultcnt;
	}
	
	if($totalAppCount eq "")
	{
		$totalAppCount = $1 if($commonContent =~ m/$regexFile->{'MainRegexControl'}->{'SEARCH_RESULTS_COUNT_RX_NEW'}/is);
	}
	
    print "TotalApplicationSearchResult: $totalAppCount\n";
	
	
	my $rawFileName=&rawFileName($scheduleDateTime,$councilCode,$Source,$category);	
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$commonContent,$searchLogsDirectoryPath,$category,'Success');	
	
	
	print "ActualScrappedCount==>${actualScrappedCount}::::TotalAppliationCount==>$totalAppCount\n";
		
	return($insertQuery,$actualScrappedCount,$totalAppCount,$deDup_Query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $councilApp = shift;
    my $currentCouncilCode = shift;
   

	$URL=~s/amp\;//igs;
	
	# $councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy

	# $councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy
	
	
	$councilApp->get($URL);
	
	my $appContent = $councilApp->content;
    my $appContentCode = $councilApp->status;
	
    return($appContent,$appContentCode);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;

    # Get search results using date ranges	
	print "\nFromDate==>$receivedFromDate\tToDate==>$receivedToDate\n";
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $commonFileDetails, $scriptStatus);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	eval
	{
		if($receivedCouncilCode=~m/^172$/is)
		{
			my $formNumber = $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'};
			my $searchFromDate = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_FROMDATE'};
			my $searchToDate = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_TODATE'};
			
			my $SEARCH_URL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
			my $searchPostContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
			my $searchViewstate = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATE_REGEX'};
			
			my $homeURL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
			$receivedCouncilApp->get($homeURL);	
			my $councilAppContent = $receivedCouncilApp->content;	
			
										
			# $receivedCouncilApp->form_number($formNumber);
			# $receivedCouncilApp->set_fields($searchFromDate => $receivedFromDate, $searchToDate => $receivedToDate );
			# $receivedCouncilApp->click();
			
			my $stringCookie =  $receivedCouncilApp->cookie_jar->as_string;
						
			my ($ViewState,$CSRFToken);			
			if($councilAppContent=~m/$searchViewstate/is)
			{
				$ViewState=uri_escape($2);
			}

			$receivedFromDate=~s/\//%2F/igs;
			$receivedToDate=~s/\//%2F/igs;
			
			
			$searchPostContent =~s/<ViewState>/$ViewState/igs;
			$searchPostContent =~s/<FromDate>/$receivedFromDate/igs;
			$searchPostContent =~s/<ToDate>/$receivedToDate/igs;
		
			my $cookie = $1 if($stringCookie=~m/Set-Cookie3\:\s*(JSESSIONID=[^\;]*?\;)\s*/is);
			
			$receivedCouncilApp->add_header(
							'Host'=>'msp.darlington.gov.uk',
							'User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0',
							'Accept'=>'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept Language'=>'en-us,en;q=0.5',
							'Accept Encoding'=>'gzip, deflate',
							'Connection'=>'keep-alive',
							'Content-type'=>'application/x-www-form-urlencoded',
							'Upgrade-Insecure-Requests'=>'1',
							'Referer'=>$homeURL,
							'Cookie'=>$cookie,
							);
						
			$councilAppResponse = $receivedCouncilApp->post( $SEARCH_URL, [Content => "$searchPostContent"]);
		}		
		else
		{
			$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
			my $formNumber = $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'};
			my $searchFromDate = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_FROMDATE'};
			my $searchToDate = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_TODATE'};
			
			$receivedCouncilApp->form_number($formNumber);
			$receivedCouncilApp->set_fields($searchFromDate => $receivedFromDate, $searchToDate => $receivedToDate);
			$receivedCouncilApp->click();
		}		
		
		$councilAppResponse = $receivedCouncilApp->content;
		$scriptStatus = $receivedCouncilApp->status;

				
	};
		
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=3))
	{
		$rerunCount++;
		goto Loop;
	}
		
    return ($councilAppResponse,$scriptStatus);
}


####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;	
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/^\s*\,\s*//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	
	return($data2Clean);
}
1;