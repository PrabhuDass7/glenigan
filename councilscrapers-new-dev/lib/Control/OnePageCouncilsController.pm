package OnePageCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use IO::Socket::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/OnePage');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/OnePage_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/OnePage_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/OnePage_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/OnePage_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');



###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType, $session_id);
			
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "OnePage_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "OnePage_Decision";
	}	
		
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
		
		exit;
	}
		
	my $rerunCount=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
	if($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working via Old proxy..\n";
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	}
		
	
	
	my $searchURL = $commonFileDetails->{$currentCouncilCode}->{'URL'};
	
	print "Search Page URL==>$searchURL\n";
	Loop2:
	($session_id) = &CouncilsScrapperCore::get_cookie_session_details($searchURL,$councilApp,'JSESSIONID');
	print "session_id=>$session_id\n";
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$councilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	
	$responseContent = $councilApp->get($searchURL);
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;
	
	print "Home Page ping Status==>$pingStatus\n";
	
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}elsif(($currentCouncilCode=~m/^(424)$/is) && ($pingStatus=~m/^5\d{2}$/is) && ($rerunCount<=2))
	{
		$councilApp->proxy(['http','https'], 'http://172.27.140.48:3128');	# New 2021 Proxy
		$rerunCount++;
		print "Working via 48 new Proxy\n";
		goto Loop2;
	}
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp, $paginationContents) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $AGENT_PHONE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $AGENT_NAME, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL, $TARGET_DATE, $GRID_REFERENCE,$AGENT_EMAIL,$APPLICATION_TYPE_NEW, $EASTING, $NORTHING);
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	if($councilCode=~m/^(197|329)$/is)
	{
		if($TabContent=~m/<ul\s*id\=\"navlist\">\s*([\w\W]*?)\s*<\/ul>/is)
		{
			my $menuBlock=$1;
			my ($contactsURL,$docURL,$dateURL);
			
			while($menuBlock=~m/<li>([\w\W]*?)<\/li>/igs)
			{
				my $block=$1;
						
				$contactsURL 		= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*Contacts\s*<\/a>/is);
				$dateURL 			= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*Key\s*Dates\s*<\/a>/is);
				$docURL 			= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*Plans\s*\&\s*Documents\s*<\/a>/is);
				$contactsURL		=~s/\'\'/\'/igs;
				$dateURL			=~s/\'\'/\'/igs;
				$docURL				=~s/\'\'/\'/igs;
			}
			
			my ($contactContent, $contactCode) = &getMechContent($contactsURL, $councilApp);
			my ($dateContent, $dateCode) = &getMechContent($dateURL, $councilApp);
	
			$REGISTRATION_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
			$RECEIVED_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
			$VALID_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
			$TARGET_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
			
			$APPLICANT_NAME = &htmlTagClean($1) if($contactContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
			$APPLICANT_ADDRESS = &htmlTagClean($1) if($contactContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);	
			$AGENT_NAME = &htmlTagClean($1) if($contactContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
			$AGENT_ADDRESS = &htmlTagClean($1) if($contactContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
			$AGENT_PHONE = &htmlTagClean($1) if($contactContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
	
			$DOC_URL = $docURL;
		}
		elsif($TabContent=~m/<ul\s*class\=\"tabs\s*sub\-tabs\">\s*([\w\W]*?)\s*<\/ul>/is)
		{
			my $menuBlock=$1;
			my ($ApplicantURL,$AgentURL,$docURL,$dateURL);
			
			while($menuBlock=~m/<li>([\w\W]*?)<\/li>/igs)
			{
				my $block=$1;
						
				$ApplicantURL 	= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Applicant\s*<\/span>\s*<\/a>/is);
				$AgentURL 		= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Agent\s*<\/span>\s*<\/a>/is);
				$dateURL 		= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Important\s*Dates\s*<\/span>\s*<\/a>/is);
				$docURL 		= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Plans\s*and\s*Comments\s*<\/span>\s*<\/a>/is);
				$ApplicantURL	=~s/\'\'/\'/igs;
				$AgentURL		=~s/\'\'/\'/igs;
				$dateURL		=~s/\'\'/\'/igs;
				$docURL			=~s/\'\'/\'/igs;
				
				if($ApplicantURL!~m/https?\:/is)
				{
					$ApplicantURL = URI::URL->new($ApplicantURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				}	
				elsif($AgentURL!~m/https?\:/is)
				{
					$AgentURL = URI::URL->new($AgentURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				}	
				elsif($dateURL!~m/https?\:/is)
				{
					$dateURL = URI::URL->new($dateURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				}	
				elsif($docURL!~m/https?\:/is)
				{
					$docURL = URI::URL->new($docURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				}	
			
			}
			
			my ($ApplicantContent, $contactCode) = &getMechContent($ApplicantURL, $councilApp);
			my ($AgentContent, $contactsCode) = &getMechContent($AgentURL, $councilApp);
			my ($dateContent, $dateCode) = &getMechContent($dateURL, $councilApp);
			
			
			$REGISTRATION_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
			$RECEIVED_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
			$VALID_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
			$TARGET_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
			
			$APPLICANT_NAME = &htmlTagClean($1) if($ApplicantContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
			$APPLICANT_ADDRESS = &htmlTagClean($1) if($ApplicantContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);	
			$AGENT_NAME = &htmlTagClean($1) if($AgentContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
			$AGENT_ADDRESS = &htmlTagClean($1) if($AgentContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
			$AGENT_PHONE = &htmlTagClean($1) if($AgentContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
	
			$DOC_URL = $docURL;
			
		}
	}
	else
	{
		$REGISTRATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
		$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
		$VALID_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);			
		
		$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
		$APPLICANT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);	
		$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
		$AGENT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);	
		
		$AGENT_PHONE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
		$TARGET_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
		$AGENT_EMAIL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_EMAIL'}/is);
	}
	
	$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$APPLICATION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS_NEW'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);	
	$APPLICATION_TYPE_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE_NEW'}/is);	
	$EASTING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'EASTING'}/is);	
	$NORTHING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'NORTHING'}/is);	
	
	
	if($councilCode=~m/^(185|285|339|424)$/is)
	{
		if($TabContent=~m/$regexFile->{$councilCode}->{'GRID_REFERENCE'}/is)
		{
			$Easting = $1;
			$Northing = $2;
			
			$gridReference=$Easting."/".$Northing;
			
			# print "$gridReference\n"; <STDIN>;
		}
	}
	else
	{
		$GRID_REFERENCE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'GRID_REFERENCE'}/is);
	}
	
	
	if($councilCode=~m/^(27|31|107)$/is)
	{	
		$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
		
		if($documentURL eq "")
		{
			if($DOC_URL ne "")
			{
				$documentURL = $applicationLink.$DOC_URL;
			}
			else
			{
				my $dURL = $applicationLink;
				$documentURL = $dURL;
			}
		
			$documentURL =~s/\'/\'\'/igs;
		}		
	}
	elsif($councilCode!~m/^(197|329)$/is)
	{
		$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
	}
		
	
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}	
	
	if($agentEmail eq "")
	{
		if($AGENT_EMAIL ne "")
		{
			$agentEmail = $AGENT_EMAIL;
		}
	}		
	
	if($Northing eq "")
	{
		if($NORTHING ne "")
		{
			$Northing = $NORTHING;
		}
	}	
	
	if($Easting eq "")
	{
		if($EASTING ne "")
		{
			$Easting = $EASTING;
		}
	}	
	
	if($gridReference eq "")
	{
		if($GRID_REFERENCE ne "")
		{
			if($GRID_REFERENCE=~m/\s*([0-9]+)\s([0-9]+)\s*/is)
			{
				$Easting=&htmlTagClean($1);
				$Northing=&htmlTagClean($2);
			}
			elsif($GRID_REFERENCE=~m/\s*([^>]*?)\s*\/\s*([^>]*)\s*/is)
			{
				$Easting=&htmlTagClean($1);
				$Northing=&htmlTagClean($2);
			}
			
			$gridReference = $GRID_REFERENCE;
			
			if($gridReference=~m/^\s*\/\s*$/is)
			{
				$gridReference="";
			}
		}
	}		
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
	}	
		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
	}		
	
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}
	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
	}
	
	if($agentName eq "")
	{
		if($AGENT_NAME ne "")
		{
			$agentName = $AGENT_NAME;
		}
	}			
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}		
	
	if($agentTelephone1 eq "")
	{
		if($AGENT_PHONE ne "")
		{
			$agentTelephone1 = $AGENT_PHONE;
		}
	}
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			my $dURL = $applicationLink;
			$dURL=~s/loadFullDetails\.do\?/tabPage3\.jsp\?/is;
			$documentURL = $dURL;
		}
	
		$documentURL =~s/\&amp\;/\&/igs;
		$documentURL =~s/\'/\'\'/igs;
	}
			
	if($applicantName eq "")
	{
		if($APPLICANT_NAME ne "")
		{
			$applicantName = $APPLICANT_NAME;
		}
	}				
		
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}					
		
	if($applicationStatus eq "")
	{
		if($councilCode=~m/^(185|285|339|424|21|323|450|98)$/is)
		{
			if($APPLICATION_STATUS_NEW ne "")
			{
				$applicationStatus = $APPLICATION_STATUS_NEW;
			}
			elsif($APPLICATION_STATUS ne "")
			{
				$applicationStatus = $APPLICATION_STATUS;
			}		
		}
		else
		{
			if($APPLICATION_STATUS ne "")
			{
				$applicationStatus = $APPLICATION_STATUS;
			}
			elsif($APPLICATION_STATUS_NEW ne "")
			{
				$applicationStatus = $APPLICATION_STATUS_NEW;
			}
		}
	}	
	
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE_NEW ne "")
		{
			$applicationType = $APPLICATION_TYPE_NEW;
		}
		elsif($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}	
	
	if($targetDecdt eq "")
	{
		if($TARGET_DATE ne "")
		{
			$targetDecdt = $TARGET_DATE;
		}
	}	
	
	$applicationLink =~s/\'/\'\'/igs;
	
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;		
	
	if(($councilCode=~m/^(235|276)$/is) && ($applicationStatus eq ""))
	{
		if($paginationContents=~m/<td\s*class\=\"row[^>]*?\">\s*<a\s*href\=\"[^\"]*?\"[^>]*?>\s*$Application\s*<\/a>\s*<\/td>\s*<td[^>]*?>[^<]*?<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is)
		{
			$applicationStatus = &htmlTagClean($1);
		}
	}
	
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp, $paginationContents) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
		
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_ISSUED_DATE,$DECISION_STATUS_NEW);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);	
	$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
	$DECISION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS_NEW'}/is);
	
	if($councilCode=~m/^(197|329)$/is)
	{
		if($TabContent=~m/<ul\s*id\=\"navlist\">\s*([\w\W]*?)\s*<\/ul>/is)
		{
			my $menuBlock=$1;
			my ($dateURL);
			
			while($menuBlock=~m/<li>([\w\W]*?)<\/li>/igs)
			{
				my $block=$1;
						
				$dateURL 				= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^>]*?)\">\s*Key\s*Dates\s*<\/a>/is);
				
				$dateURL				=~s/\'\'/\'/igs;
			}
			
			my ($dateContent, $dateCode) = &getMechContent($dateURL, $councilApp);
	
			$DECISION_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
			$DECISION_ISSUED_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
		}
		elsif($TabContent=~m/<ul\s*class\=\"tabs\s*sub\-tabs\">\s*([\w\W]*?)\s*<\/ul>/is)
		{
			my $menuBlock=$1;
			my ($dateURL);
			
			while($menuBlock=~m/<li>([\w\W]*?)<\/li>/igs)
			{
				my $block=$1;
						
				$dateURL 		= &htmlTagClean($1) if ( $block =~ m/<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Important\s*Dates\s*<\/span>\s*<\/a>/is);
			
				$dateURL		=~s/\'\'/\'/igs;
				
				if($dateURL!~m/https?\:/is)
				{
					$dateURL = URI::URL->new($dateURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				}	
			}
			
			my ($dateContent, $dateCode) = &getMechContent($dateURL, $councilApp);
			
			$DECISION_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
			$DECISION_ISSUED_DATE = &htmlTagClean($1) if($dateContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);			
		}
	}
	else
	{
		$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
		$DECISION_ISSUED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
	}
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}		
			
	if($Decision_Issued_Date eq "")
	{
		if($DECISION_ISSUED_DATE ne "")
		{
			$Decision_Issued_Date=$DECISION_ISSUED_DATE;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
		}
	}		
		
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
	}		
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
	}		
	
	if($Decision_Status eq "")
	{
		if($DECISION_STATUS_NEW ne "")
		{
			$Decision_Status = $DECISION_STATUS_NEW;
		}
		elsif($DECISION_STATUS ne "")
		{
			$Decision_Status = $DECISION_STATUS;
		}
	}	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Proposal=~s/\s+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	
	$applicationLink =~s/\'/\'\'/igs;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;	
	
	if(($councilCode=~m/^(235|276)$/is) && ($Decision_Status eq "") && ($Application_Status eq ""))
	{
		if($paginationContents=~m/<td\s*class\=\"row[^>]*?\">\s*<a\s*href\=\"[^\"]*?\"[^>]*?>\s*$Application\s*<\/a>\s*<\/td>\s*<td[^>]*?>[^<]*?<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is)
		{
			$Decision_Status = &htmlTagClean($1);
			$Application_Status = &htmlTagClean($1);			
		}
	}
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $PLANNING_APPLICATION;  undef $PROPOSAL;  undef $DECISION_STATUS;  undef $APPLICATION_STATUS;  undef $DECISION_DATE;  undef $DECISION_ISSUED_DATE;  undef $applicationLink;
	undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Application_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
   

	$URL=~s/amp\;//igs;
	
	
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy

	# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy

	my $count = 0;
	Loop:
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
	if(($code!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($con=~m/A\s*problem\s*has\s*ocurred\s*in\s*the\s*repeater\s*process/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($con=~m/<h2>\s*404\s*\-\s*File\s*or\s*directory\s*not\s*found\.\s*<\/h2>/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($con=~m/temporarily\s*unavailable/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
    return($con,$code);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;
	
	my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear,$dumStartDate,$dumEndDate);
	
	my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
	my %monthFull = ('01' =>'January', '02' => 'February', '03' => 'March','04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
	
	if($receivedCouncilCode=~m/^27$/is)
	{
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $month{$2};
			$sYear = $3;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $month{$2};
			$eYear = $3;
		}
	
		$dumStartDate="$sDay-$sMonth-$sYear";
		$dumEndDate="$eDay-$eMonth-$eYear";
	}
	elsif($receivedCouncilCode=~m/^(107|333|482)$/is)
	{
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
		}
	
		$dumStartDate="$sYear-$sMonth-$sDay";
		$dumEndDate="$eYear-$eMonth-$eDay";
	}
	elsif($receivedCouncilCode=~m/^(32)$/is)
	{
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $month{$2};
			$sYear = $3;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $month{$2};
			$eYear = $3;
		}
	
		$dumStartDate="$sDay/$sMonth/$sYear";
		$dumEndDate="$eDay/$eMonth/$eYear";
	}
	elsif($receivedCouncilCode=~m/^(370|414)$/is)
	{
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $2;
			$sYear = $3;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $2;
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$sMonth=~s/^0//is;
		$eDay=~s/^0//is;
		$eMonth=~s/^0//is;
	
	}
	elsif($receivedCouncilCode=~m/^(475|477)$/is)
	{
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $monthFull{$2};
			$sYear = $3;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $monthFull{$2};
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$sMonth=~s/^0//is;
		$eDay=~s/^0//is;
		$eMonth=~s/^0//is;
	
	}
	elsif($receivedCouncilCode=~m/^(20)$/is)
	{
		$dumStartDate=$receivedFromDate;
		$dumEndDate=$receivedToDate;
		
		$receivedCouncilApp->form_number(1);
		$receivedCouncilApp->set_fields( 'CheckBox1' => '0' );
		$receivedCouncilApp->click();		
	}
	elsif($receivedCouncilCode=~m/^(348)$/is)
	{
		$dumStartDate=$receivedFromDate;
		$dumEndDate=$receivedToDate;	
	}
	else
	{
		$dumStartDate=$receivedFromDate;
		$dumEndDate=$receivedToDate;
		$dumStartDate=~s/\//%2F/igs;
		$dumEndDate=~s/\//%2F/igs;
	}
	
    # Get search results using date ranges	
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $commonFileDetails, $scriptStatus,$SEARCH_URL,$currentPageURL);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	$SEARCH_URL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
	
	
	if($receivedCouncilCode=~m/^(370|414)$/is)
	{
		$SEARCH_URL=~s/<SDAY>/$sDay/si;
		$SEARCH_URL=~s/<EDAY>/$eDay/si;
		$SEARCH_URL=~s/<SMONTH>/$sMonth/si;
		$SEARCH_URL=~s/<EMONTH>/$eMonth/si;
		$SEARCH_URL=~s/<SYEAR>/$sYear/si;
		$SEARCH_URL=~s/<EYEAR>/$eYear/si;
	}
	else
	{
		$SEARCH_URL=~s/<FromDate>/$dumStartDate/gsi;
		$SEARCH_URL=~s/<ToDate>/$dumEndDate/gsi;
		print "$SEARCH_URL\n";
		# <STDIN>;
	}
	

	
	if($receivedCouncilCode=~m/^437$/is)
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		
		if($category eq "Planning")
		{
			$receivedCouncilApp-> submit_form(
				form_number => "1",
				fields      => {
					"txtRegFrom"=> $receivedFromDate,
					"txtRegTo"	=> $receivedToDate,
					"btnSearch"	=>"Search",
					},
					button    	=> "btnSearch"
				);
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp-> submit_form(
				form_number => "1",
				fields      => {
					"txtDecFrom"=> $receivedFromDate,
					"txtDecTo"	=> $receivedToDate,
					"btnSearch"	=>"Search",
					},
					button    	=> "btnSearch"
				);
		}
	}
	elsif($receivedCouncilCode=~m/^(96|147|29|282|22|16)$/is)
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		
		my $dumFrom_Date = $receivedFromDate;
		my $dumTo_Date = $receivedToDate;
		$dumFrom_Date=~s/\//\-/gsi;
		$dumTo_Date=~s/\//\-/gsi;
		if($category eq "Planning")
		{
			$receivedCouncilApp->form_number(1);
			$receivedCouncilApp->set_fields( 'valid_date_from' => $dumFrom_Date );
			$receivedCouncilApp->set_fields( 'valid_date_to' => $dumTo_Date );
			$receivedCouncilApp->click();
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->form_number(1);
			$receivedCouncilApp->set_fields( 'decision_issued_date_from' => $dumFrom_Date );
			$receivedCouncilApp->set_fields( 'decision_issued_date_to' => $dumTo_Date );
			$receivedCouncilApp->click();
		}
	}
	elsif($receivedCouncilCode=~m/^(102)$/is)
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		
		if($category eq "Planning")
		{			
			$receivedCouncilApp->form_number(1);
			$receivedCouncilApp->set_fields( 'dateReceivedFrom' => $receivedFromDate );
			$receivedCouncilApp->set_fields( 'dateReceivedTo' => $receivedToDate );
			$receivedCouncilApp->click();
		}
		elsif($category eq "Decision")
		{			
			$receivedCouncilApp->form_number(1);
			$receivedCouncilApp->set_fields( 'dateDecisionFrom' => $receivedFromDate );
			$receivedCouncilApp->set_fields( 'dateDecisionTo' => $receivedToDate );
			$receivedCouncilApp->click();
		}
	}
	elsif($receivedCouncilCode=~m/^(475)$/is)
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		
		if($category eq "Planning")
		{			
			$receivedCouncilApp->form_number(2);
			$receivedCouncilApp->set_fields( 'q359436:q3' => 'validdate' );
			$receivedCouncilApp->set_fields( 'q359436:q4' => $sDay, 'q359436:q5' => $sMonth, 'q359436:q6' => $sYear );
			$receivedCouncilApp->set_fields( 'q359436:q7' => $eDay, 'q359436:q8' => $eMonth, 'q359436:q9' => $eYear );
			$receivedCouncilApp->set_fields( 'q359436:q10[]' => '0' );
			$receivedCouncilApp->set_fields( 'q359436:q11' => '6' );
			$receivedCouncilApp->click();
		}
		elsif($category eq "Decision")
		{			
			$receivedCouncilApp->form_number(2);
			$receivedCouncilApp->set_fields( 'q359436:q3' => 'decision_date' );
			$receivedCouncilApp->set_fields( 'q359436:q4' => $sDay, 'q359436:q5' => $sMonth, 'q359436:q6' => $sYear );
			$receivedCouncilApp->set_fields( 'q359436:q7' => $eDay, 'q359436:q8' => $eMonth, 'q359436:q9' => $eYear );
			$receivedCouncilApp->set_fields( 'q359436:q10[]' => '0' );
			$receivedCouncilApp->set_fields( 'q359436:q11' => '6' );
			$receivedCouncilApp->click();
		}
	}
	elsif($receivedCouncilCode=~m/^(477)$/is)
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		
		if($category eq "Planning")
		{			
			$receivedCouncilApp->form_number(2);
			$receivedCouncilApp->set_fields( 'q457814:q14' => 'ValidDate' );
			$receivedCouncilApp->set_fields( 'q457814:q7' => $sDay, 'q457814:q8' => $sMonth, 'q457814:q9' => $sYear );
			$receivedCouncilApp->set_fields( 'q457814:q10' => $eDay, 'q457814:q11' => $eMonth, 'q457814:q12' => $eYear );
			$receivedCouncilApp->set_fields( 'q457814:q6[]' => '0' );
			$receivedCouncilApp->click();
		}
		elsif($category eq "Decision")
		{			
			$receivedCouncilApp->form_number(2);
			$receivedCouncilApp->set_fields( 'q868954:q3' => 'decision_date' );
			$receivedCouncilApp->set_fields( 'q457814:q7' => $sDay, 'q457814:q8' => $sMonth, 'q457814:q9' => $sYear );
			$receivedCouncilApp->set_fields( 'q457814:q10' => $eDay, 'q457814:q11' => $eMonth, 'q457814:q12' => $eYear );
			$receivedCouncilApp->set_fields( 'q457814:q6[]' => '0' );
			$receivedCouncilApp->click();
		}
	}
	elsif($receivedCouncilCode=~m/^(433)$/is)
	{		
		my $URL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		my $postURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
		my $postCNT = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CNT'};
		if($receivedCouncilCode=~m/^(433)$/is)
		{
			if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
			{
				$sDay = $1;
				# $sMonth = $month{$2};
				$sMonth = $2;
				$sYear = $3;
			}
			if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
			{
				$eDay = $1;
				# $eMonth = $month{$2};
				$eMonth = $2;
				$eYear = $3;
			}
		
			$dumStartDate="$sYear-$sMonth-$sDay";
			$dumEndDate="$eYear-$eMonth-$eDay";
			
			$postCNT=~s/<FromDate>/$dumStartDate/is;
			$postCNT=~s/<ToDate>/$dumEndDate/is;
						
		}
		
		$receivedCouncilApp->get($URL);
		
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Referer" => "$URL" );

		$councilAppResponse = $receivedCouncilApp->post( $postURL, Content => "$postCNT");
		
		$currentPageURL = $URL;
	}
	elsif($receivedCouncilCode=~m/^482$/is)
	{
		$receivedCouncilApp->add_header( "Accept" => 'application/json, text/plain, */*' );
		$receivedCouncilApp->add_header( "x-client" => 'EXMOOR' );
		$receivedCouncilApp->add_header( "x-product" => 'CITIZENPORTAL' );
		$receivedCouncilApp->add_header( "x-service" => 'PA' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Origin" => 'https://planning.agileapplications.co.uk' );
		
		my $referer;
		if($category eq "Planning")
		{
			my $fDate = $1 if($SEARCH_URL=~m/registrationDateFrom=(\d{4}\-\d+\-\d+)/is);
			my $tDate = $1 if($SEARCH_URL=~m/registrationDateTo=(\d{4}\-\d+\-\d+)/is);
			$referer = "https://planning.agileapplications.co.uk/exmoor/search-applications/results?criteria=%7B%22registrationDateFrom%22:%22$fDate%22,%22registrationDateTo%22:%22$tDate%22%7D";
		}
		elsif($category eq "Decision")
		{
			my $fDate = $1 if($SEARCH_URL=~m/decisionDateFrom=(\d{4}\-\d+\-\d+)/is);
			my $tDate = $1 if($SEARCH_URL=~m/decisionDateTo=(\d{4}\-\d+\-\d+)/is);
			$referer = "https://planning.agileapplications.co.uk/exmoor/search-applications/results?criteria=%7B%22decisionDateFrom%22:%22$fDate%22,%22decisionDateTo%22:%22$tDate%22%7D";
		}
		$receivedCouncilApp->add_header( "Referer" => $referer );
		$receivedCouncilApp->get($SEARCH_URL);
	}
	elsif($receivedCouncilCode=~m/^333$/is)
	{
		$receivedCouncilApp->add_header( "Accept" => 'application/json, text/plain, */*' );
		$receivedCouncilApp->add_header( "x-client" => 'RUGBY' );
		$receivedCouncilApp->add_header( "x-product" => 'CITIZENPORTAL' );
		$receivedCouncilApp->add_header( "x-service" => 'PA' );	
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Origin" => 'https://planning.agileapplications.co.uk' );	
		my $referer;
		if($category eq "Planning")
		{
			my $fDate = $1 if($SEARCH_URL=~m/registrationDateFrom=(\d{4}\-\d+\-\d+)/is);
			my $tDate = $1 if($SEARCH_URL=~m/registrationDateTo=(\d{4}\-\d+\-\d+)/is);
			$referer = "https://planning.agileapplications.co.uk/rugby/search-applications/results?criteria=%7B%22status%22:0,%22registrationDateFrom%22:%22$fDate%22,%22registrationDateTo%22:%22$tDate%22%7D";
		}
		elsif($category eq "Decision")
		{
			my $fDate = $1 if($SEARCH_URL=~m/decisionDateFrom=(\d{4}\-\d+\-\d+)/is);
			my $tDate = $1 if($SEARCH_URL=~m/decisionDateTo=(\d{4}\-\d+\-\d+)/is);
			$referer = "https://planning.agileapplications.co.uk/rugby/search-applications/results?criteria=%7B%22status%22:0,%22decisionDateFrom%22:%22$fDate%22,%22decisionDateTo%22:%22$tDate%22%7D";
		}
		$receivedCouncilApp->add_header( "Referer" => $referer );
		
		$receivedCouncilApp->get($SEARCH_URL);
	}
	elsif($receivedCouncilCode=~m/^397$/is)
	{		
		my $URL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		my $postURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
		my $postCNT = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CNT'};
		
		$postCNT=~s/<FromDate>/$dumStartDate/is;
		$postCNT=~s/<ToDate>/$dumEndDate/is;
		
		$councilAppResponse = $receivedCouncilApp->post( $postURL, Content => "$postCNT");
		
	}
	elsif($receivedCouncilCode=~m/^(107|395|438)$/is)
	{		
		my $URL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		my $postURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
		my $postCNT = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CNT'};
		
		$receivedCouncilApp->get($URL);
		my $tempCon=$receivedCouncilApp->content;
		my $QC=$1 if($tempCon=~m/name\=\"QC\"\s*value\=\"([^>]*?)\"/is);
		
		$postCNT=~s/<FromDate>/$dumStartDate/is;
		$postCNT=~s/<ToDate>/$dumEndDate/is;
		$postCNT=~s/<QC>/$QC/is;

		if($receivedCouncilCode=~m/^(107)$/is)
		{
			$receivedCouncilApp->add_header( "Accept" => 'application/json, text/javascript, */*; q=0.01' );
			$receivedCouncilApp->add_header( "Content-Type" => 'application/json; charset=utf-8' );
		}
		
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Referer" => "$URL" );

		$councilAppResponse = $receivedCouncilApp->post( $postURL, Content => "$postCNT");
		if ($receivedCouncilCode=~m/^395$/is)
		{
		
			$councilAppResponse = $receivedCouncilApp->content;
		
			my $tempContent="$councilAppResponse";
			while($tempContent=~m/<input[^>]*?value\=\"Next\s*page\"[^>]*?>/igs)
			{
				my $QC=$1 if($tempContent=~m/name\=\"QC\"\s*value\=\"([^>]*?)\"/is);
				my $regdate1=$1 if($tempContent=~m/name\=\"regdate1\"\s*value\=\"([^>]*?)\"/is);
				my $regdate2=$1 if($tempContent=~m/name\=\"regdate2\"\s*value\=\"([^>]*?)\"/is);
				my $PageNo=$1 if($tempContent=~m/\"Nextform[\w\W]*?name\=\"PageNo\"\s*value\=\"([^>]*?)\"/is);
				my $postCNT="QC=$QC&regdate1=$regdate1&regdate2=$regdate2&parish=&casefullref=&locaddress1=&proposal=&appname=&agtname=&ViewAll=Paged&PageType=Search&PageNo=$PageNo&weeklylisttype=&WeeklyListDate=";
				$receivedCouncilApp->post( $postURL, Content => "$postCNT");
				$tempContent = $receivedCouncilApp->content;
				$councilAppResponse.=$tempContent;
			}
		}
		$currentPageURL = $URL;
	}
	elsif($receivedCouncilCode=~m/^(185|285|339|424|21|323|450|98)$/is)
	{		
		my $URL = $commonFileDetails->{$receivedCouncilCode}->{'URL'};
		my $postURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
		my $postCNT = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CNT'};
		$URL=~s/<FromDate>/$dumStartDate/is;
		$URL=~s/<ToDate>/$dumEndDate/is;
		
		$postCNT=~s/<FromDate>/$receivedFromDate/is;
		$postCNT=~s/<ToDate>/$receivedToDate/is;
		
		
		$receivedCouncilApp->get($URL);
		
		$receivedCouncilApp->add_header( "Accept" => 'application/json, text/javascript, */*; q=0.01' );
		$receivedCouncilApp->add_header( "Content-Type" => 'application/json; charset=utf-8' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Referer" => "$URL" );

		$councilAppResponse = $receivedCouncilApp->post( $postURL, Content => "$postCNT");
		
		$currentPageURL = $URL;
	}
	else
	{	
		if($receivedCouncilCode=~m/^(370|414)$/is)
		{
			$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
			$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		}
		# print "SEARCH_URL==>$SEARCH_URL\n"; <STDIN>;
		$councilAppResponse = $receivedCouncilApp->get($SEARCH_URL);
	}
	
	eval{
		$councilAppResponse = $receivedCouncilApp->content if($receivedCouncilCode!~m/^395$/is);
	};
	if($@)
	{
		my $Er=$@;
	}
	
	if($receivedCouncilCode!~m/^(185|285|339|424|21|323|450|98)$/is)
	{
		$currentPageURL = $receivedCouncilApp->uri();
	}
	
	$scriptStatus = $receivedCouncilApp->status;		
	print "councilAppStatus==>$scriptStatus\n";
	
	# open(PP,">a.html");
	# print PP "$councilAppResponse";
	# close(PP);
	# exit;
	
    return ($councilAppResponse,$scriptStatus,$currentPageURL);
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile,$commonFileDetails);
	
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	if(($councilCode=~m/^27$/is) && ($category eq "Planning"))
	{	
		my $applicantLink=$receivedApplicationUrl;
		$applicantLink=~s/CaseNo\.aspx\?/CASENO_Noindex\.aspx\?/igs;
		print "Application URL==>$applicantLink\n";
				
		$applicationDetailsPageContent = $receivedCouncilApp->get($applicantLink);
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		$applicationDetailsPageContent = $receivedCouncilApp->content;		
		
	}
	elsif($councilCode=~m/^(185|285|339|424|21|323|450|98)$/is)
	{	
		print "Application URL==>$receivedApplicationUrl\n";
		
		my $appDetailURL = $commonFileDetails->{$councilCode}->{'APP_DETAIL_URL'};		
		my $appPostCNT = $commonFileDetails->{$councilCode}->{'APP_DETAIL_CNT'};
		
		my $keyTest;
		
		if($receivedApplicationUrl=~m/KeyNo=([^<]*?)\&KeyText=Subject$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/KeyText=([^<]*?)$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/KeyNo=([^<]*?)$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/CASENO=([^<]*?)$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/refno=([^<]*?)$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/ref=([^<]*?)\&/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/id=([^<]*?)\&/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/planningApplicationNumber=([^<]*?)$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/ApplicationNumber=([^<]*?)$/is)
		{
			$keyTest=$1;
		}
		elsif($receivedApplicationUrl=~m/\&app=([^\&]*?)\&/is)
		{
			$keyTest=$1;
		}
		$appPostCNT =~s/<KeyText>/$keyTest/is;
				
		$applicationDetailsPageContent = $receivedCouncilApp->post($appDetailURL, Content => "$appPostCNT");
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		# print "Application URL Response==>$applicationDetailsPageResponse\n";
		$applicationDetailsPageContent = $receivedCouncilApp->content;
		
	}
	elsif($councilCode=~m/^(333)$/is)
	{	
		my $id = $1 if($receivedApplicationUrl=~m/rugby\/application-details\/([^<]*?)\s*$/is);
		my $appURL = "https://planningapi.agileapplications.co.uk/api/application/$id/";
		
		$receivedCouncilApp->add_header( 'Host' => 'planningapi.agileapplications.co.uk');
		$receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
		$receivedCouncilApp->add_header( 'Referer' => $receivedApplicationUrl);
		$receivedCouncilApp->add_header( 'Accept' => 'application/json, text/plain, */*');
		$receivedCouncilApp->add_header( "x-client" => 'RUGBY' );
		$receivedCouncilApp->add_header( "x-product" => 'CITIZENPORTAL' );
		$receivedCouncilApp->add_header( "x-service" => 'PA' );	
		$applicationDetailsPageContent = $receivedCouncilApp->get($appURL);
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		# print "Application URL Response==>$applicationDetailsPageResponse\n";
		$applicationDetailsPageContent = $receivedCouncilApp->content;
	}
	elsif($councilCode=~m/^(482)$/is)
	{	
		my $id = $1 if($receivedApplicationUrl=~m/exmoor\/application-details\/([^<]*?)\s*$/is);
		my $appURL = "https://planningapi.agileapplications.co.uk/api/application/$id/";		
		
		$receivedCouncilApp->add_header( 'Host' => 'planningapi.agileapplications.co.uk');
		$receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
		$receivedCouncilApp->add_header( 'Referer' => $receivedApplicationUrl);
		$receivedCouncilApp->add_header( 'Accept' => 'application/json, text/plain, */*');
		$receivedCouncilApp->add_header( "x-client" => 'EXMOOR' );
		$receivedCouncilApp->add_header( "x-product" => 'CITIZENPORTAL' );
		$receivedCouncilApp->add_header( "x-service" => 'PA' );	
		$applicationDetailsPageContent = $receivedCouncilApp->get($appURL);
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		# print "Application URL Response==>$applicationDetailsPageResponse\n";
		$applicationDetailsPageContent = $receivedCouncilApp->content;
	}
	elsif($councilCode=~m/^(107)$/is)
	{	
		my $id = '"'.$1.'"' if($receivedApplicationUrl=~m/refval\=([^<]*?)\s*$/is);
		my $appURL = "https://www.bathnes.gov.uk/webapi/api/PlanningAPI/v2/planningdata/";		
		
		$receivedCouncilApp->add_header( 'Host' => 'www.bathnes.gov.uk');
		$receivedCouncilApp->add_header( 'Accept' => 'application/json, text/javascript, */*; q=0.01');
		$receivedCouncilApp->add_header( 'Content-Type' => 'application/json; charset=UTF-8');
		$receivedCouncilApp->add_header( 'X-Requested-With' => 'XMLHttpRequest');
		$receivedCouncilApp->add_header( 'Referer' => $receivedApplicationUrl);
		# print "id:::$id\n\n";
		# print "appURL:::$appURL\n\n";
		# print "receivedApplicationUrl:::$receivedApplicationUrl\n\n";
		$applicationDetailsPageContent = $receivedCouncilApp->post($appURL, Content => "$id");
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		# print "Application URL Response==>$applicationDetailsPageResponse\n";
		$applicationDetailsPageContent = $receivedCouncilApp->content;
	}
	else
	{	
		# print "Application URL==>$receivedApplicationUrl\n";
				
		$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		# print "Application URL Response==>$applicationDetailsPageResponse\n";
		$applicationDetailsPageContent = $receivedCouncilApp->content;
		
	}
	
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
	
	# print "Current Application number::$applicationNumber\n"; <STDIN>;
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		if($receivedApplicationUrl=~m/refval\^='([^\']*?)\'\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/KeyNo=([^<]*?)$/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/CASENO=([^<]*?)$/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/refno=([^<]*?)$/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/ref=([^<]*?)\&/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/casefullref=([^<]*?)\&/is)
		{
			$logName=$1;
		}
		elsif(($councilCode=~m/^(147|96|29|282|22|16)$/is) && ($receivedApplicationUrl=~m/id=([^<]*?)$/is))
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/\&app=([^\&]*?)\&/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/id=([^<]*?)\&/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/planningApplicationNumber=([^<]*?)$/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/ApplicationNumber=([^<]*?)$/is)
		{
			$logName=$1;
		}
		elsif($receivedApplicationUrl=~m/application\/([^<]*?)\/$/is)
		{
			$logName=$1;
		}
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($applicationDetailsPageContent=~m/A\s*problem\s*has\s*ocurred\s*in\s*the\s*repeater\s*process/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($applicationDetailsPageContent=~m/<h2>\s*404\s*\-\s*File\s*or\s*directory\s*not\s*found\.\s*<\/h2>/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	elsif(($applicationDetailsPageContent=~m/temporarily\s*unavailable/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
		
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}

####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\\r|\\n/ /igs;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\n+/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	$data2Clean=~s/\\u0010//igs;
	
	return($data2Clean);
}
1;