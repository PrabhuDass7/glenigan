package WeeklyCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use URI::Escape;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use IO::Socket::SSL;
# use Net::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}



my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Weekly');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Weekly_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Weekly_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Weekly_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Weekly_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');



###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType, $session_id);
	
			
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Weekly_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Weekly_Decision";
	}	

	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
		exit;
	}
		
	my $rerunCount=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
		
	if($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working via Old proxy..\n";
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	}
	
	my $searchURL = $commonFileDetails->{$currentCouncilCode}->{'URL'};
	
	print "Search Page URL==>$searchURL\n";
	
	($session_id) = &CouncilsScrapperCore::get_cookie_session_details($searchURL,$councilApp,'JSESSIONID');
	print "session_id=>$session_id\n";
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$councilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	
	$responseContent = $councilApp->get($searchURL);
	if($currentCouncilCode=~m/^(606)$/is)
	{
		my $acceptURL = "https://planning.cumbria.gov.uk/Disclaimer/Accept?returnUrl=/Search/Standard?searchType=Decided&days=28";
		$councilApp->post( $acceptURL, Content => "returnUrl=%2FSearch%2FStandard%3FsearchType%3DDecided%26days%3D28");
	}
	if($currentCouncilCode=~m/^(20)$/is)
	{
		$councilApp->form_number(1); 
		$councilApp->field( 'ctl00$MainContent$btn_AcceptTnC', 'Accept the Terms and Conditions' );
		$councilApp->click();
	}
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}
	
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $AGENT_PHONE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $AGENT_NAME, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL, $TARGET_DATE,$AGENT_COMPANY,$NORTHING,$EASTING,$GRIDREFERENCE);
	
	if($councilCode=~m/^333$/is)
	{
		$TabContent=~s/<\!\-\-[\w\W]*?\-\->//gsi;
	}
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$REGISTRATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
	$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	$VALID_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	
	
	if($councilCode=~m/^(94)$/is)
	{
		if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is)
		{
			my $AppAddr = $1;
			$AppAddr=~s/<dt>\s*[^<]*?\s*<\/dt>//gsi;
			$AppAddr=~s/<dd>\s*([^<]*?)\s*<\/dd>\s*/$1 /gsi;
			$AppAddr=~s/\s+$//gsi;
			$AppAddr=&htmlTagClean($AppAddr);
			$APPLICANT_ADDRESS=$AppAddr;
		}
		$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);	
		
		if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is)
		{
			my $AgntAddr = $1;
			$AgntAddr=~s/<dt>\s*[^<]*?\s*<\/dt>//gsi;
			$AgntAddr=~s/<dd>\s*([^<]*?)\s*<\/dd>\s*/$1 /gsi;
			$AgntAddr=~s/\s+$//gsi;
			$AgntAddr=&htmlTagClean($AgntAddr);
			$AGENT_ADDRESS=$AgntAddr;
		}
		$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
		
		if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is)
		{
			my $address = $1;
			$address=~s/<dt>\s*[^<]*?\s*<\/dt>//gsi;
			$address=~s/<dd>\s*([^<]*?)\s*<\/dd>\s*/$1 /gsi;
			$address=~s/\s+$//gsi;
			$address=&htmlTagClean($address);
			$SITE_LOCATION=$address;
		}
		
	}
	else
	{
		$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
		$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
		$APPLICANT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);	
		$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
		$AGENT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
	}
		
	$AGENT_PHONE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
	$TARGET_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$EASTING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'EASTING'}/is);
	$NORTHING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'NORTHING'}/is);
	$GRIDREFERENCE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'GRIDREFERENCE'}/is);
	$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
		else
		{
			$Application = $1 if($applicationLink=~m/Ref(?:Val)?\=([^<]*?)$/is);
		}
	}	
			
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}		
			
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}	
		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
	}		
	
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}
	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
	}
	
	if($agentName eq "")
	{
		if($AGENT_NAME ne "")
		{
			$agentName = $AGENT_NAME;
		}
	}				
			
	if($agentTelephone1 eq "")
	{
		if($AGENT_PHONE ne "")
		{
			$agentTelephone1 = $AGENT_PHONE;
		}
	}				
			
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}				
					
		
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}					
		
	if($applicationStatus eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$applicationStatus = $APPLICATION_STATUS;
		}
	}	
	
	if($Easting eq "")
	{
		if($EASTING ne "")
		{
			$Easting = $EASTING;
		}
	}	
	if($Northing eq "")
	{
		if($NORTHING ne "")
		{
			$Northing = $NORTHING;
		}
	}		
	
	if($gridReference eq "")
	{
		if($GRIDREFERENCE ne "")
		{
			$gridReference = $GRIDREFERENCE;
		}
	}	
	
	if($applicantName eq "")
	{
		if($APPLICANT_NAME ne "")
		{
			$applicantName = $APPLICANT_NAME;
		}
	}	
	
	if($targetDecdt eq "")
	{
		if($TARGET_DATE ne "")
		{
			$targetDecdt = $TARGET_DATE;
		}
	}	
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			$documentURL = $applicationLink;
		}
		$documentURL=~s/\&amp\;/\&/gsi;
	
		$documentURL =~s/\'/\'\'/igs;
	}
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
		
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_STATUS_NEW,$DECISION_ISSUED_DATE);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
	$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
	$DECISION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS_NEW'}/is);
	$DECISION_ISSUED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
		else
		{
			$Application = $1 if($applicationLink=~m/Ref(?:Val)?\=([^<]*?)$/is);
		}
	}		
			
	if($Decision_Issued_Date eq "")
	{
		if($DECISION_ISSUED_DATE ne "")
		{
			$Decision_Issued_Date=$DECISION_ISSUED_DATE;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
		}
		elsif(($councilCode=~m/^461$/is) && ($DECISION_DATE eq ""))
		{
			$Date_Decision_Made = &htmlTagClean($1) if($TabContent=~m/>\s*Date\s*the\s*decision\s*was\s*issued\s*\:\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		}
		
		if($councilCode=~m/^482$/is)
		{
			$Date_Decision_Made=~s/^1\/1\/1970$//is;
		}
		
		
	}		
		
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}		
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
	}		
	
	if($Decision_Status eq "")
	{
		if($councilCode=~m/^346$/is)
		{
			if($DECISION_STATUS_NEW ne "")
			{
				$Decision_Status = $DECISION_STATUS_NEW;
			}
			elsif($DECISION_STATUS ne "")
			{
				$Decision_Status = $DECISION_STATUS;
			}
		}
		else
		{
			if($DECISION_STATUS ne "")
			{
				$Decision_Status = $DECISION_STATUS;
			}
			elsif($DECISION_STATUS_NEW ne "")
			{
				$Decision_Status = $DECISION_STATUS_NEW;
			}
		}
	}	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $PLANNING_APPLICATION;  undef $PROPOSAL;  undef $DECISION_STATUS;  undef $APPLICATION_STATUS;  undef $DECISION_DATE;  undef $DECISION_ISSUED_DATE;  undef $applicationLink;
	undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Application_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
    my $cCode = shift;
   
	$URL=~s/amp\;//igs;
	
	# if($cCode!~m/^478$/is)
	# {
		# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy
	# }
	# else
	# {
		# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy
	# }
	 

	
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
    return($con,$code);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;
	
    # Get search results using date ranges	
	my $tempFromDate=$receivedFromDate;
	my $tempToDate=$receivedToDate;
	$tempFromDate=~s/\//\-/gsi;
	$tempToDate=~s/\//\-/gsi;
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $commonFileDetails, $scriptStatus,$postContent);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	$postContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
	$postContent =~s/<FromDate>/$receivedFromDate/igs;
	$postContent =~s/<ToDate>/$receivedToDate/igs;
	
	$councilAppResponse = $receivedCouncilApp->post($commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'}, Content => "$postContent");
	$councilAppResponse = $receivedCouncilApp->content;
	
	$scriptStatus = $receivedCouncilApp->status;		
	print "councilAppStatus==>$scriptStatus\n";
	
	
    return ($councilAppResponse,$scriptStatus);
}

####
# Method to POST the pagenation settings for Online Councils
####

sub searchNextWeeklyRange()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $weeklyURLs, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent, $rawFileName) = @_;
	
	
	my (@appURLs, $appLinkDup, $appLinkCount, $responseContent, $newAppLinkcheck,$dumFlagCheck, $commonFileDetails);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	if($receivedCouncilCode == 606)
	{
		my $page = 2;
		my $nextPageRegex=$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}; 
		my $nextPageRegex=~s/<PAGENUM>/$page/is; 
		my $postContentnew=$postContent;
		if($postContentnew=~m/<li[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*$page\s*<\/a>\s*<\/li>/is)
		{
			while($postContentnew=~m/<li[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*$page\s*<\/a>\s*<\/li>/gsi)
			{
				my $nextpagelink = $1;
				if($nextpagelink!~m/^https?/is)
				{
					$nextpagelink = URI::URL->new($nextpagelink)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				}
				my ($nextListCnt,$responseCode) = &getMechContent($nextpagelink,$receivedCouncilApp,$receivedCouncilCode);
				$postContentnew=$nextListCnt;
				my ($totalAppURL, $redisDup, $totalCount, $newAppcheck) = &collectApplicationDetails($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $nextListCnt,$receivedCouncilApp);
				$page++;
				@appURLs = @{$totalAppURL};
				$appLinkDup = $redisDup;
				$appLinkCount = $totalCount;
				$newAppLinkcheck = $newAppcheck;
				
				$responseContent .= $postContentnew;
			}
		}
		else
		{
			my ($totalAppURL, $redisDup, $totalCount, $newAppcheck) = &collectApplicationDetails($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent,$receivedCouncilApp);
			
			@appURLs = @{$totalAppURL};
			$appLinkDup = $redisDup;
			$appLinkCount = $totalCount;
			$newAppLinkcheck = $newAppcheck;
			
			$responseContent = $postContent;
		}
	}
	elsif($postContent ne "")
	{
		my ($totalAppURL, $redisDup, $totalCount, $newAppcheck) = &collectApplicationDetails($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent,$receivedCouncilApp);
		@appURLs = @{$totalAppURL};
		$appLinkDup = $redisDup;
		$appLinkCount = $totalCount;
		$newAppLinkcheck = $newAppcheck;
		
		$responseContent = $postContent;
	}
	else
	{
		my @weeklyURL = @{$weeklyURLs};
		my $weekCount = 1;
		for my $WeekUrl(@weeklyURL)
		{
			# print "WeekUrl=>$WeekUrl\n"; <STDIN>;		
			print "\nweekCount=>$weekCount\n\n";
			$weekCount++;			
			
			my ($weeklyListCnt,$responseCode) = &getMechContent($WeekUrl,$receivedCouncilApp,$receivedCouncilCode);
			
			# open(TIME,">fileName.html");
			# print TIME "$weeklyListCnt\n";
			# close TIME;
			# exit;
			
			if($receivedCouncilCode == 20)
			{
				my $page = 2;
				my $page1 = 1;
				my $nextPageRegex=$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}; 
				my $nextPageRegex=~s/<PAGENUM>/$page/is; 
				my $postContentnew=$weeklyListCnt;
				if($postContentnew=~m/<[^>]*?selected=\"selected\"[^>]*?>[^>]*?<[^>]*?>\s*<[^>]*?>\s*$page\s*<[^>]*?>/is)
				{
					while($postContentnew=~m/<[^>]*?selected=\"selected\"[^>]*?>[^>]*?<[^>]*?>\s*<[^>]*?>\s*$page\s*<[^>]*?>/gsi)
					{
						my $nextpagelink = $WeekUrl;
						my ($VIEWSTATE,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$postContent);
						if($postContentnew=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATE_REGEX'}/is)
						{
							$VIEWSTATE = uri_escape($1);
						}
						if($postContentnew=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_VIEWSTATEGENERATOR'}/is)
						{
							$VIEWSTATEGENERATOR = uri_escape($1);
						}
						if($postContentnew=~m/$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_EVENTVALIDATION'}/is)
						{
							$EVENTVALIDATION = uri_escape($1);
						}
						$postContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
						$postContent=~s/<VIEWSTATE>/$VIEWSTATE/igs;
						$postContent=~s/<VIEWSTATEGENERATOR>/$VIEWSTATEGENERATOR/igs;
						$postContent=~s/<EVENTVALIDATION>/$EVENTVALIDATION/igs;
						$postContent=~s/<PAGENUM>/$page1/igs;
						$nextpagelink=~s/([\d]{1,2})\/([\d]{1,2})\/([\d]{4})/$1\%2f$2\%2f$3/igs;
						
						$receivedCouncilApp->post( $nextpagelink, Content => "$postContent");
						my $scriptStatus = $receivedCouncilApp->status;		
						my $nextListCnt = $receivedCouncilApp->content;
						$postContentnew=$nextListCnt;						
						$page++;
						$page1++;						
						$weeklyListCnt .= $postContentnew;
					}
				}
				else
				{
					$weeklyListCnt = $weeklyListCnt;
				}
			}
			if($receivedCouncilCode=~m/^103$/is)
			{	
				next if($weeklyListCnt=~m/Red\;\">\s*No\s*records\s*found/is);
			}
			elsif($receivedCouncilCode=~m/^133$/is)
			{
				next if($weeklyListCnt=~m/\{\"SearchResults\"\:\[\]\,\"ResultCount\"\:0\}/is);
			}
			my ($totalAppURL, $redisDup, $totalCount, $newAppcheck) = &collectApplicationDetails($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $weeklyListCnt,$receivedCouncilApp);
			
			$responseContent .= $weeklyListCnt;
			
			if($newAppcheck=~m/^Yes$/is)
			{	
				$newAppLinkcheck = "Yes";
				$dumFlagCheck = "1";
			}
			else
			{
				$newAppLinkcheck = "No";
			}
			
			
			push(@appURLs, @{$totalAppURL});
			$appLinkDup .= $redisDup;
			$appLinkCount = $totalCount + $appLinkCount;	
		}
		
		if($dumFlagCheck=~m/^1$/is)
		{	
			$newAppLinkcheck = "Yes";
		}
	}
		
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$responseContent,$searchLogsDirectoryPath,$category,'Success');	
		
	return(\@appURLs, $appLinkDup, $appLinkCount, $newAppLinkcheck);
}


sub collectApplicationDetails
{
	my ($receivedCouncilCode, $category, $source, $councilName, $scheduleNo, $scheduleDate, $redis, $postContent, $receivedCouncilApp) = @_;
	
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my (@appLinks, $deDup_Query);
	my $pageNumber=1;
	my $actualSearchCount=0;
	my ($newAppLinkcheck,$dumFlagCheck);
	
	
	
	NextPage:
	while($postContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'APPLICATION_REGEX'}/igs)
	{
		my $firstMatch=$1;
		my $secondMatch=$2;
				
		my ($applicationURL,$applicationReference);
		if($receivedCouncilCode=~m/^133$/is)
		{
			my $AppURLDomin = $commonFileDetails->{$receivedCouncilCode}->{'APPLICATION_URL_DOMAIN'};
			$AppURLDomin=~s/<AppNUM>/$firstMatch/si;
			$applicationURL = $AppURLDomin;		
			$applicationReference = $firstMatch;		
		}
		elsif($receivedCouncilCode=~m/^353$/is)
		{
			$applicationReference = $firstMatch;
			$applicationURL = $secondMatch;
		}
		else
		{
			$applicationURL = $firstMatch;
			$applicationReference = $secondMatch;
		}
		
		if($receivedCouncilCode=~m/^353$/is)
		{
			my $ApplicationNumber2=$applicationReference;
			my $Applink=$applicationURL;
			$ApplicationNumber2=~s/\///igs;
			$applicationURL="https://folkestonehythedc.force.com/pr/s/planning-application/$Applink/$ApplicationNumber2";
		}
		
		if($applicationURL!~m/^https?/is)
		{
			$applicationURL = URI::URL->new($applicationURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		}
		
		$applicationURL=~s/\&amp\;/\&/gsi;
		
		if($receivedCouncilCode=~m/^94$/is)
		{
			$applicationURL=~s/^\s*(https\:\/\/digital\.flintshire\.gov\.uk)\./$1/gsi;
		}
		
		# print "applicationURL=>$applicationURL\n"; <STDIN>;
		# print "applicationReference=>$applicationReference\n"; <STDIN>;
		
		
		if($applicationReference!~m/^\s*$/is)
		{
			my $checkRedis;
			if($category eq "Planning")
			{
				$checkRedis = $receivedCouncilCode."_".$applicationReference;
			}
			elsif($category eq "Decision")
			{
				$checkRedis = "D_".$receivedCouncilCode."_".$applicationReference;				
			}
			
			if($redis->exists($checkRedis))	
			{
				print "Exists : $applicationReference\n";
		
				$actualSearchCount++;
				my $deDupQuery="($scheduleNo, \'$scheduleDate\', $receivedCouncilCode, \'$applicationReference\', \'$applicationURL\', \'$category\', \'$source\', \'$councilName\'),";
				$deDup_Query.=$deDupQuery;
				
				$newAppLinkcheck = "No";
				
				next;
			}
			else
			{	
				$actualSearchCount++;
				$dumFlagCheck = "1";
				$newAppLinkcheck = "Yes";
				
				push ( @appLinks, $applicationURL);	
			}
		}
	}	
	
	
		
	if($dumFlagCheck=~m/^1$/is)
	{	
		$newAppLinkcheck = "Yes";
	}
	
	print "Page Number::$pageNumber\n";
	
	
	
	if(($postContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}/is) && ($receivedCouncilCode != 606))
	{
		my $nextPageURL=$1;
		
		if($nextPageURL eq "")
		{
			goto NEXTEND;
		}
		
		
		$nextPageURL=~s/\s*\&\#xD\;\&\#xA\;\s*//gsi;
		
		
		if($nextPageURL!~m/^https?/is)
		{
			$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		}
		
		$nextPageURL=~s/\&amp\;/\&/gsi;
		# print "nextPageURL=>$nextPageURL\n"; <STDIN>;
		
		if($nextPageURL ne "")
		{
			my ($weeklyListCnt,$responseCode) = &getMechContent($nextPageURL,$receivedCouncilApp,$receivedCouncilCode);
			$postContent = $weeklyListCnt;
			
			$pageNumber++;			
			
			my $count478 = 30;
			if(($count478 > $pageNumber) && ($receivedCouncilCode=~m/^478$/is))
			{
				goto NextPage;	
			}
			elsif($receivedCouncilCode!~m/^478$/is)
			{
				goto NextPage;	
			}
		}
		
	}
	
	NEXTEND:
				
	# print "$newAppLinkcheck\n"; <STDIN>;
	
	
	return (\@appLinks, $deDup_Query, $actualSearchCount, $newAppLinkcheck);
			
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	
	
	print "receivedApplicationUrl==>$receivedApplicationUrl\n";
	if($councilCode=~m/^353$/is)
	{
		my ($application_id,$application_name);
		if($receivedApplicationUrl=~m/application\/([^>]*?)\/([^>]*?)$/is)
		{
			$application_id=$1;
			$application_name=$2;
		}
		my $receivedCouncilApptemp=$receivedCouncilApp;
		# my $pURL = "$receivedApplicationUrl?tabset-185b1=2";
		
		my $pURL = "https://folkestonehythedc.force.com/pr/s/sfsites/aura?r=3&ui-force-components-controllers-recordGlobalValueProvider.RecordGvp.getRecord=1";
		my ($commonFileDetails,$postCnt);
		if($category eq "Decision")
		{
			$commonFileDetails=$councilDetailsDecision;
			
			$receivedCouncilApptemp->get($commonFileDetails->{$councilCode}->{'URL'});
		
			my $responseCont = $receivedCouncilApptemp->content;

			my $fwuid=$1 if($responseCont=~m/fwuid\%22\%3A\%22([^>]*?)\%22/is);
			my $loaded=$1 if($responseCont=~m/loaded\%22\%3A\%7B\%22([^>]*?)\%22\%7D\%2C\%22/is);
			
			$fwuid=~s/\\\"//igs;
			$fwuid=~s/\///igs;
			$fwuid=~s/\\//igs;
			
			$loaded=~s/\\\"//igs;
			$loaded=~s/\///igs;
			$loaded=~s/\\//igs;
			
			$postCnt="message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22123%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.force.components.controllers.recordGlobalValueProvider.RecordGvpController%2FACTION%24getRecord%22%2C%22callingDescriptor%22%3A%22UNKNOWN%22%2C%22params%22%3A%7B%22recordDescriptor%22%3A%22$application_id.undefined.FULL.null.null.null.VIEW.true.null.null.null%22%7D%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22$loaded%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AinputDate%22%3A%22z-qp3GvEQ1EfL_McUqcz6Q%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputURL%22%3A%22OLB65JHY2G3Wcv7H_yUspQ%22%2C%22COMPONENT%40markup%3A%2F%2Fui%3AoutputDate%22%3A%22MYbs4tpQL_tXyVnDCgNKCQ%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArecordDetail%22%3A%22vIrcwOEdKZm7pHxSmXJJSA%22%2C%22COMPONENT%40markup%3A%2F%2FforceCommunity%3ArelatedRecords%22%3A%22CscY-pMeaNQlltytn0ADZQ%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fpr%2Fs%2Fplanning-application%2F$application_id&aura.token=undefined";
			
		}
		
		# print "pURL:: $pURL\n";
		# print "postCnt:: $postCnt\n";
		
		$applicationDetailsPageContent = $receivedCouncilApp->post($pURL, Content => $postCnt);
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		$applicationDetailsPageContent = $receivedCouncilApp->content;
	}
	else
	{
		$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
		$applicationDetailsPageResponse = $receivedCouncilApp->status();	
		$applicationDetailsPageContent = $receivedCouncilApp->content;
		
	}
	# open(TIME,">fileName.html");
	# print TIME "$applicationDetailsPageContent\n";
	# close TIME;
	# exit;
	
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
	else
	{
		$applicationNumber = $1 if($receivedApplicationUrl=~m/Ref(?:Val)?\=([^<]*?)$/is);
	}
	
	print "Current Application number::$applicationNumber\n";
	# print "applicationDetailsPageResponse::$applicationDetailsPageResponse\n";
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		if($receivedApplicationUrl=~m/Ref(?:Val)?\=([^<]*?)$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/articleid=([^<]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/frmId\=([^<]*?)$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/APASID\=([^<]*?)\#planapps$/is)
		{
			$logName = $1;
		}
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}

####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/\s*\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\\r\\n/ /igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	$data2Clean=~s/\n/ /igs;
	$data2Clean=~s/\&quot\;/\"/igs;#########new
	
	return($data2Clean);
}
1;