package OnlineCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
# use Mozilla::CA;
use IO::Socket::SSL;
# use Net::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		# $ENV{PERL_NET_HTTPS_SSL_SOCKET_CLASS}= 'Net::SSL';
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

print "basePath==>$basePath\n";


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Online');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile,$councilDetailsDecision,$regexFilePlanning,$regexFileDecision" 
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Online_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Online_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');

###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType);
	
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Online_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Online_Decision";
	}	
		
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
		exit;
	}
	
	my $rerunCount=0;
	my $rerunCount2=0;
	Loop:
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		if($currentCouncilCode=~m/^(602)$/is)
		{
			$councilApp = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
										# SSL_hostname => '',
									}
									, autocheck => 0
						 );
		}
		else
		{
			$councilApp = WWW::Mechanize->new(autocheck => 0);
		}
		# $councilApp = WWW::Mechanize->new(ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( 'ssl_opts' => {'verify_hostname' => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
										# SSL_hostname => '',
									}
									, autocheck => 0
		);
	}
	
	if($currentCouncilCode=~m/^(3|7|15|18|28|35|63|75|100|149|159|192|223|246|317|385|283|378|292)$/is)
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	elsif($pingStatus=~m/^5\d{2}$/is)
	{
		print "Working 192 via Old proxy..\n";
		$councilApp->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy	
	}
	else
	{
		$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	}
	
	my $searchURL = $commonFileDetails->{$currentCouncilCode}->{'URL'};
	
	print "Search Page URL==>$searchURL\n";
	Loop2:
	my $session_id = &CouncilsScrapperCore::get_cookie_session_details($searchURL,$councilApp,'JSESSIONID');
	print "session_id=>$session_id\n";
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$councilApp->add_header( 'Cookie' => 'JSESSIONID='.$session_id) if($session_id!~m/^JSESSION_ISSUE$/is);
	
	$responseContent = $councilApp->get($searchURL);
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";

	if(($currentCouncilCode=~m/^(283)$/is) && ($pingStatus=~m/^5\d{2}$/is) && ($rerunCount2<=1))
	{
		$councilApp->proxy(['http','https'], 'http://172.27.140.48:3128');	# New 2021 Proxy
		print "Working 48 via new proxy..\n";
		$rerunCount2++;
		goto Loop2;
	}
	
	if(($pingStatus!~m/^\s*200\s*$/is) && ($rerunCount<=1))
	{
		$rerunCount++;
		goto Loop;
	}
	
    return($councilApp, $pingStatus, $responseContent);
}

####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
		
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
		
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime) = @_;	
	
	my %activeTabContent = %{$TabContent};
	
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
		
	foreach my $AppDataKey (keys %activeTabContent)
	{		
		my ($ACTUAL_COMMITTEE_DATE, $ACTUAL_COMMITTEE_OR_PANEL_DATE, $ADVERTISEMENT_EXPIRY_DATE, $AGREED_EXPIRY_DATE, $APPLICATION_EXPIRY_DEADLINE, $APPLICATION_VALID_DATE, $APPLICATION_VALIDATED_DATE, $CLOSING_DATE_FOR_COMMENTS, $COMMENTS_TO_BE_SUBMITTED_BY, $COMMITTEE_DATE, $COMMITTEE_DELEGATED_LIST_DATE, $CONSULTATION_END_DATE, $CONSULTATION_EXPIRY_DATE, $CONSULTATION_PERIOD_EXPIRES, $DATE_APPLICATION_VALID, $DECISION_DATE, $DECISION_ISSUED_DATE1, $DECISION_MADE_DATE, $DECISION_NOTICE_DATE, $DECISION_PRINTED_DATE, $DETERMINATION_DEADLINE, $EARLIEST_DECISION_DATE, $ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED, $EXPIRY_DATE, $EXPIRY_DATE_FOR_COMMENT, $LAST_ADVERTISED_IN_PRESS_DATE, $LAST_DATE_FOR_COMMENTS, $LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE, $LAST_DATE_FOR_NEIGHBOUR_COMMENTS, $LAST_DATE_FOR_NEIGHBOURS_RESPONSES, $LAST_SITE_NOTICE_POSTED_DATE, $LATEST_ADVERTISEMENT_EXPIRY_DATE, $LATEST_NEIGHBOUR_CONSULTATION_DATE, $LATEST_SITE_NOTICE_EXPIRY_DATE, $NEIGHBOUR_CONSULTATION_EXPIRY_DATE, $NEIGHBOURS_LAST_NOTIFIED, $OVERALL_CONSULTATION_EXPIRY_DATE, $OVERALL_DATE_OF_CONSULTATION_EXPIRY, $PERMISSION_EXPIRY_DATE, $PUBLIC_CONSULTATION_EXPIRY_DATE, $SITE_NOTICE_EXPIRY_DATE, $STANDARD_CONSULTATION_DATE, $STANDARD_CONSULTATION_EXPIRY_DATE, $STATUTORY_DETERMINATION_DATE, $STATUTORY_DETERMINATION_DEADLINE, $STATUTORY_EXPIRY_DATE, $TARGET_DATE, $TARGET_DETERMINATION_DATE, $TEMPORARY_PERMISSION_EXPIRY_DATE, $VALID_DATE, $WEEKLY_LIST_EXPIRY_DATE, $AGENT_EMAIL, $AGENT_TELEPHONE1, $AGENT_MOBILE, $HOME_PHONE, $AGENT_FAX, $MOBILE_NUMBER, $PHONE_NUMBER, $TELEPHONE_NUMBER, $HOME_PHONE_NUMBER, $AGENT_ADDRESS1, $PERSONAL_EMAIL, $PERSONAL_PHONE, $COMPANY_PHONE, $WORK_PHONE, $EMAIL_ADDRESSE, $FAX_NO, $COMPANY_FAX, $EMAIL_ADDRESS, $TELEPHONE, $PERSONAL_MOBILE, $ELECTRONIC_MAIL, $WORK_PHONE_NUMBER, $PHONE_NO, $OFFICE_PHONE_NUMBER, $MAIN_PHONE, $PHONE_USED_BY_ALL_TEMPLATES, $PHONE_CONTACT_NUMBER, $AGENT_CONTACT_DETAILS, $AGENT_CONTACT_NAME, $ACKNOWLEDGEMENT_EMAIL, $EMAIL, $MOBILE, $E_MAIL_ADDRESS, $ACTUAL_DECISION_LEVEL, $AGENT_ADDRESS, $AGENT_COMPANY_NAME, $AGENT_NAME, $AGENT_TELEPHONE, $AMENITY_SOCIETY, $APPLICANT_ADDRESS, $APPLICANT_NAME, $APPLICATION_TYPE, $CASE_OFFICER, $COMMUNITY_COUNCIL, $DECISION_STATUS, $DISTRICT_REFERENCE, $ENVIRONMENTAL_ASSESSMENT_REQUESTED, $ENVIRONMENTAL_ASSESSMENT_REQUIRED, $EXPECTED_DECISION_LEVEL, $NEIGHBOURHOOD_PARTNERSHIP_AREA, $PARISH, $WARD, $ADDRESS, $ALTERNATIVE_REFERENCE, $APPEAL_DECISION, $DATE_APPLICATION_RECEIVED, $DATE_APPLICATION_VALIDATED, $DATE_APPLICATION_REGISTERED, $APPLICATION_REFERENCE, $APPLICATION_RECEIVED_DATE, $CASE_REFERENCE, $APPLICATION_REGISTERED_DATE, $DECISION_ISSUED_DATE, $LOCATION, $PLANNING_PORTAL_REFERENCE, $PROPOSAL, $APPLICATION, $APPLICATION_STATUS, $NO_DOCUMENT, $NO_CASE, $NO_PROPERTY, $SUMMARY_ADDRESS, $SUMMARY_PROPOSAL, $SUMMARY_CASENUMBER, $DOC_URL, $DOC_URL1, $DOC_URL2, $DOC_URL3);
		
		### Summary ###
		$ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ADDRESS$/is);
		$ALTERNATIVE_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ALTERNATIVE_REFERENCE$/is);
		$APPEAL_DECISION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPEAL_DECISION$/is);
		$DATE_APPLICATION_RECEIVED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_RECEIVED$/is);
		$DATE_APPLICATION_VALIDATED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_VALIDATED$/is);
		$DATE_APPLICATION_REGISTERED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_REGISTERED$/is);
		$APPLICATION_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_REFERENCE$/is);
		$APPLICATION_RECEIVED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_RECEIVED_DATE$/is);
		$CASE_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CASE_REFERENCE$/is);
		$APPLICATION_REGISTERED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_REGISTERED_DATE$/is);
		$DECISION_ISSUED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATE$/is);
		$LOCATION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LOCATION$/is);
		$PLANNING_PORTAL_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PLANNING_PORTAL_REFERENCE$/is);
		$PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PROPOSAL$/is);
		$APPLICATION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION$/is);
		$APPLICATION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_STATUS$/is);
		$NO_DOCUMENT = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NO_DOCUMENT$/is);
		$NO_CASE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NO_CASE$/is);
		$NO_PROPERTY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NO_PROPERTY$/is);
		$SUMMARY_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_ADDRESS$/is);
		$SUMMARY_PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_PROPOSAL$/is);
		$SUMMARY_CASENUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_CASENUMBER$/is);
		$DOC_URL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL$/is);
		$DOC_URL1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL1$/is);
		$DOC_URL2 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL2$/is);
		$DOC_URL3 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL3$/is);
		
		### Details ###
		$ACTUAL_DECISION_LEVEL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACTUAL_DECISION_LEVEL$/is);
		$AGENT_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_ADDRESS$/is);
		$AGENT_COMPANY_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_COMPANY_NAME$/is);
		$AGENT_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_NAME$/is);
		$AGENT_TELEPHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_TELEPHONE$/is);
		$AMENITY_SOCIETY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AMENITY_SOCIETY$/is);
		$APPLICANT_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICANT_ADDRESS$/is);
		$APPLICANT_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICANT_NAME$/is);
		$APPLICATION_TYPE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_TYPE$/is);
		$CASE_OFFICER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CASE_OFFICER$/is);
		$COMMUNITY_COUNCIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMUNITY_COUNCIL$/is);
		$DECISION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_STATUS$/is);
		$DISTRICT_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DISTRICT_REFERENCE$/is);
		$ENVIRONMENTAL_ASSESSMENT_REQUESTED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ENVIRONMENTAL_ASSESSMENT_REQUESTED$/is);
		$ENVIRONMENTAL_ASSESSMENT_REQUIRED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ENVIRONMENTAL_ASSESSMENT_REQUIRED$/is);
		$EXPECTED_DECISION_LEVEL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EXPECTED_DECISION_LEVEL$/is);
		$NEIGHBOURHOOD_PARTNERSHIP_AREA = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NEIGHBOURHOOD_PARTNERSHIP_AREA$/is);
		$PARISH = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PARISH$/is);
		$WARD = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WARD$/is);
		
		### Contact ###
		$AGENT_EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_EMAIL$/is);
		$AGENT_TELEPHONE1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_TELEPHONE1$/is);
		$AGENT_MOBILE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_MOBILE$/is);
		$HOME_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^HOME_PHONE$/is);
		$AGENT_FAX = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_FAX$/is);
		$MOBILE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^MOBILE_NUMBER$/is);
		$PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_NUMBER$/is);
		$TELEPHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TELEPHONE_NUMBER$/is);
		$HOME_PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^HOME_PHONE_NUMBER$/is);
		$AGENT_ADDRESS1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_ADDRESS1$/is);
		$PERSONAL_EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERSONAL_EMAIL$/is);
		$PERSONAL_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERSONAL_PHONE$/is);
		$COMPANY_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMPANY_PHONE$/is);
		$WORK_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WORK_PHONE$/is);
		$EMAIL_ADDRESSE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EMAIL_ADDRESSE$/is);
		$FAX_NO = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^FAX_NO$/is);
		$COMPANY_FAX = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMPANY_FAX$/is);
		$EMAIL_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EMAIL_ADDRESS$/is);
		$TELEPHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TELEPHONE$/is);
		$PERSONAL_MOBILE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERSONAL_MOBILE$/is);
		$ELECTRONIC_MAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ELECTRONIC_MAIL$/is);
		$WORK_PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WORK_PHONE_NUMBER$/is);
		$PHONE_NO = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_NO$/is);
		$OFFICE_PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^OFFICE_PHONE_NUMBER$/is);
		$MAIN_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^MAIN_PHONE$/is);
		$PHONE_USED_BY_ALL_TEMPLATES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_USED_BY_ALL_TEMPLATES$/is);
		$PHONE_CONTACT_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_CONTACT_NUMBER$/is);
		$AGENT_CONTACT_DETAILS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_CONTACT_DETAILS$/is);
		$AGENT_CONTACT_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_CONTACT_NAME$/is);
		$ACKNOWLEDGEMENT_EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACKNOWLEDGEMENT_EMAIL$/is);
		$EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EMAIL$/is);
		$MOBILE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^MOBILE$/is);
		$E_MAIL_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^E_MAIL_ADDRESS$/is);
		
		### Date ###
		$ACTUAL_COMMITTEE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACTUAL_COMMITTEE_DATE$/is);
		$ACTUAL_COMMITTEE_OR_PANEL_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACTUAL_COMMITTEE_OR_PANEL_DATE$/is);
		$ADVERTISEMENT_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ADVERTISEMENT_EXPIRY_DATE$/is);
		$AGREED_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGREED_EXPIRY_DATE$/is);
		$APPLICATION_EXPIRY_DEADLINE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_EXPIRY_DEADLINE$/is);
		$APPLICATION_VALID_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_VALID_DATE$/is);
		$APPLICATION_VALIDATED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_VALIDATED_DATE$/is);
		$CLOSING_DATE_FOR_COMMENTS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CLOSING_DATE_FOR_COMMENTS$/is);
		$COMMENTS_TO_BE_SUBMITTED_BY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMENTS_TO_BE_SUBMITTED_BY$/is);
		$COMMITTEE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMITTEE_DATE$/is);
		$COMMITTEE_DELEGATED_LIST_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMITTEE_DELEGATED_LIST_DATE$/is);
		$CONSULTATION_END_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CONSULTATION_END_DATE$/is);
		$CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CONSULTATION_EXPIRY_DATE$/is);
		$CONSULTATION_PERIOD_EXPIRES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CONSULTATION_PERIOD_EXPIRES$/is);
		$DATE_APPLICATION_VALID = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_VALID$/is);
		$DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_DATE$/is);
		$DECISION_ISSUED_DATE1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATE$/is);
		$DECISION_MADE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_MADE_DATE$/is);
		$DECISION_NOTICE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_NOTICE_DATE$/is);
		$DECISION_PRINTED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_PRINTED_DATE$/is);
		$DETERMINATION_DEADLINE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DETERMINATION_DEADLINE$/is);
		$EARLIEST_DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EARLIEST_DECISION_DATE$/is);
		$ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED$/is);
		$EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EXPIRY_DATE$/is);
		$EXPIRY_DATE_FOR_COMMENT = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EXPIRY_DATE_FOR_COMMENT$/is);
		$LAST_ADVERTISED_IN_PRESS_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_ADVERTISED_IN_PRESS_DATE$/is);
		$LAST_DATE_FOR_COMMENTS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_COMMENTS$/is);
		$LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE$/is);
		$LAST_DATE_FOR_NEIGHBOUR_COMMENTS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_NEIGHBOUR_COMMENTS$/is);
		$LAST_DATE_FOR_NEIGHBOURS_RESPONSES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_NEIGHBOURS_RESPONSES$/is);
		$LAST_SITE_NOTICE_POSTED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_SITE_NOTICE_POSTED_DATE$/is);
		$LATEST_ADVERTISEMENT_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LATEST_ADVERTISEMENT_EXPIRY_DATE$/is);
		$LATEST_NEIGHBOUR_CONSULTATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LATEST_NEIGHBOUR_CONSULTATION_DATE$/is);
		$LATEST_SITE_NOTICE_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LATEST_SITE_NOTICE_EXPIRY_DATE$/is);
		$NEIGHBOUR_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NEIGHBOUR_CONSULTATION_EXPIRY_DATE$/is);
		$NEIGHBOURS_LAST_NOTIFIED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NEIGHBOURS_LAST_NOTIFIED$/is);
		$OVERALL_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^OVERALL_CONSULTATION_EXPIRY_DATE$/is);
		$OVERALL_DATE_OF_CONSULTATION_EXPIRY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^OVERALL_DATE_OF_CONSULTATION_EXPIRY$/is);
		$PERMISSION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERMISSION_EXPIRY_DATE$/is);
		$PUBLIC_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PUBLIC_CONSULTATION_EXPIRY_DATE$/is);
		$SITE_NOTICE_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SITE_NOTICE_EXPIRY_DATE$/is);
		$STANDARD_CONSULTATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STANDARD_CONSULTATION_DATE$/is);
		$STANDARD_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STANDARD_CONSULTATION_EXPIRY_DATE$/is);
		$STATUTORY_DETERMINATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STATUTORY_DETERMINATION_DATE$/is);
		$STATUTORY_DETERMINATION_DEADLINE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STATUTORY_DETERMINATION_DEADLINE$/is);
		$STATUTORY_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STATUTORY_EXPIRY_DATE$/is);
		$TARGET_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TARGET_DATE$/is);
		$TARGET_DETERMINATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TARGET_DETERMINATION_DATE$/is);
		$TEMPORARY_PERMISSION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TEMPORARY_PERMISSION_EXPIRY_DATE$/is);
		$VALID_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^VALID_DATE$/is);
		$WEEKLY_LIST_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WEEKLY_LIST_EXPIRY_DATE$/is);
			
		if($DOC_URL ne "")
		{
			$documentURL = $DOC_URL;
		}
		if($documentURL=~m/^\s*$/is)
		{
			$documentURL=$DOC_URL1;
		}
		if($documentURL=~m/^\s*$/is)
		{
			$documentURL=$DOC_URL2;
		}
		if($documentURL=~m/^\s*$/is)
		{
			$documentURL=$DOC_URL3;
		}	
		
		if(($documentURL!~m/^http/is) && ($documentURL ne ""))
		{
			my $applicationLinkUri = URI::URL->new($documentURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
			$documentURL=$applicationLinkUri;
		}
		
		if($documentURL eq "")
		{
			my $dumDocURL = $applicationLink;
			$dumDocURL=~s/activeTab=summary/activeTab=documents/is;
			$documentURL=$dumDocURL;
		}

		if($councilCode=~m/^(105|149|232|245|246|273|292|15|179|171|320|533|211|268|350|406|429|455|481)$/is)
		{
			if($documentURL eq "")
			{
				my $dumDocURL = $applicationLink;
				$dumDocURL=~s/activeTab=summary/activeTab=externalDocuments/is;
				$documentURL=$dumDocURL;
			}
			else
			{
				$documentURL=~s/activeTab=documents/activeTab=externalDocuments/is;
			}
		}		
		
		if($APPLICATION eq '')
		{
			if($CASE_REFERENCE!~m/^\s*$/is)
			{
				$Application=$CASE_REFERENCE;
			}
			elsif($SUMMARY_CASENUMBER!~m/^\s*$/is)
			{
				$Application=$SUMMARY_CASENUMBER;
			}
			elsif($APPLICATION_REFERENCE!~m/^\s*$/is)
			{
				$Application=$APPLICATION_REFERENCE;
			}
		}
		else
		{
			$Application = $APPLICATION;
		}
		
		if($PROPOSAL=~m/^\s*$/is)
		{
			if($SUMMARY_PROPOSAL!~m/^\s*$/is)
			{
				$Proposal=$SUMMARY_PROPOSAL;
			}
		}	
		else
		{
			$Proposal = $PROPOSAL;
		}
		
		if($ADDRESS eq '')
		{
			if($LOCATION!~m/^\s*$/is)
			{
				$Address=$LOCATION;
			}
			elsif($SUMMARY_ADDRESS!~m/^\s*$/is)
			{
				$Address=$SUMMARY_ADDRESS;
			}
		}	
		else
		{
			$Address = $ADDRESS;
		}
		
		if($AGENT_EMAIL=~m/^\s*$/is)
		{
			if($PERSONAL_EMAIL!~m/^\s*$/is)
			{
				$agentEmail = $PERSONAL_EMAIL;
			}
			elsif($EMAIL_ADDRESSE!~m/^\s*$/is)
			{
				$agentEmail = $EMAIL_ADDRESSE;
			}
			elsif($EMAIL_ADDRESS!~m/^\s*$/is)
			{
				$agentEmail = $EMAIL_ADDRESS;
			}
			elsif($ACKNOWLEDGEMENT_EMAIL!~m/^\s*$/is)
			{
				$agentEmail = $ACKNOWLEDGEMENT_EMAIL;
			}
			elsif($EMAIL!~m/^\s*$/is)
			{
				$agentEmail = $EMAIL;
			}
			elsif($E_MAIL_ADDRESS!~m/^\s*$/is)
			{
				$agentEmail = $E_MAIL_ADDRESS;
			}
			elsif($ELECTRONIC_MAIL!~m/^\s*$/is)
			{
				$agentEmail = $ELECTRONIC_MAIL;
			}
		}
		else
		{
			$agentEmail = $AGENT_EMAIL;
		}
		
		if($AGENT_FAX=~m/^\s*$/is)
		{
			if($FAX_NO!~m/^\s*$/is)
			{
				$agentFax = $FAX_NO;
			}
			elsif($COMPANY_FAX!~m/^\s*$/is)
			{
				$agentFax = $COMPANY_FAX;
			}
		}
		else
		{
			$agentFax = $AGENT_FAX;
		}

		if($AGENT_MOBILE=~m/^\s*$/is)
		{
			if($MOBILE_NUMBER!~m/^\s*$/is)
			{
				$agentMobile = $MOBILE_NUMBER;
			}
			elsif($MOBILE!~m/^\s*$/is)
			{
				$agentMobile = $MOBILE;
			}
		}	
		else
		{
			$agentMobile = $AGENT_MOBILE;
		}
			
		if($AGENT_TELEPHONE1 ne "")
		{
			$agentTelephone2 = $AGENT_TELEPHONE1;
		}
		elsif($AGENT_TELEPHONE1=~m/^\s*$/is)
		{
			if($HOME_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $HOME_PHONE;
			}
			elsif($PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_NUMBER;
			}
			elsif($TELEPHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $TELEPHONE_NUMBER;
			}
			elsif($HOME_PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $HOME_PHONE_NUMBER;
			}
			elsif($PERSONAL_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $PERSONAL_PHONE;
			}
			elsif($COMPANY_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $COMPANY_PHONE;
			}
			elsif($WORK_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $WORK_PHONE;
			}
			elsif($TELEPHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $TELEPHONE;
			}
			elsif($WORK_PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $WORK_PHONE_NUMBER;
			}
			elsif($PHONE_NO!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_NO;
			}
			elsif($OFFICE_PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $OFFICE_PHONE_NUMBER;
			}
			elsif($MAIN_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $MAIN_PHONE;
			}
			elsif($PHONE_CONTACT_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_CONTACT_NUMBER;
			}
			elsif($PHONE_USED_BY_ALL_TEMPLATES!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_USED_BY_ALL_TEMPLATES;
			}
			elsif($PERSONAL_MOBILE!~m/^\s*$/is)
			{
				$agentTelephone2 = $PERSONAL_MOBILE;
			}
		}	
		
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
		if($agentAddress=~m/^\s*$/is)
		{
			if($AGENT_ADDRESS1!~m/^\s*$/is)
			{
				$agentAddress = $AGENT_ADDRESS1;
			}
		}

		if($AGENT_CONTACT_DETAILS=~m/^\s*$/is)
		{
			if($AGENT_CONTACT_NAME!~m/^\s*$/is)
			{
				$agentContactDetails = $AGENT_CONTACT_NAME;
			}
		}
		else
		{
			$agentContactDetails = $AGENT_CONTACT_DETAILS;
		}
		
		if($DATE_APPLICATION_RECEIVED=~m/^\s*$/is)
		{
			if($APPLICATION_RECEIVED_DATE!~m/^\s*$/is)
			{
				$dateApplicationReceived=$APPLICATION_RECEIVED_DATE;
			}
		}
		else
		{
			$dateApplicationReceived = $DATE_APPLICATION_RECEIVED;
		}
		
		if($DATE_APPLICATION_REGISTERED=~m/^\s*$/is)
		{
			if($APPLICATION_REGISTERED_DATE!~m/^\s*$/is)
			{
				$dateApplicationRegistered = $APPLICATION_REGISTERED_DATE;
			}
		}
		else
		{
			$dateApplicationRegistered = $DATE_APPLICATION_REGISTERED;
		}
		
		if($DATE_APPLICATION_VALIDATED=~m/^\s*$/is)
		{
			if($APPLICATION_VALID_DATE!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $APPLICATION_VALID_DATE;
			}
			elsif($APPLICATION_VALIDATED_DATE!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $APPLICATION_VALIDATED_DATE;
			}
			elsif($DATE_APPLICATION_VALID!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $DATE_APPLICATION_VALID;
			}
			elsif($VALID_DATE!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $VALID_DATE;
			}
		}
		else
		{
			$dateApplicationvalidated = $DATE_APPLICATION_VALIDATED;
		}
		
		if($targetDecdt=~m/^\s*$/is)
		{
			if($DETERMINATION_DEADLINE!~m/^\s*$/is)
			{
				$targetDecdt = $DETERMINATION_DEADLINE;
			}
			elsif($EARLIEST_DECISION_DATE!~m/^\s*$/is)
			{
				$targetDecdt = $EARLIEST_DECISION_DATE;
			}
			elsif($TARGET_DATE!~m/^\s*$/is)
			{
				$targetDecdt = $TARGET_DATE;
			}
			elsif($TARGET_DETERMINATION_DATE!~m/^\s*$/is)
			{
				$targetDecdt = $TARGET_DETERMINATION_DATE;
			}
			# elsif($STATUTORY_EXPIRY_DATE!~m/^\s*$/is) # Gayathri Request on 19/09/2018
			# {
				# $targetDecdt = $STATUTORY_EXPIRY_DATE;
			# }
		}
				
		$agentName = $AGENT_NAME if($agentName eq "");	
		$applicationStatus = $APPLICATION_STATUS if($applicationStatus eq "");
		$actualDecisionLevel = $ACTUAL_DECISION_LEVEL if($actualDecisionLevel eq "");
		$agentCompanyName = $AGENT_COMPANY_NAME if($agentCompanyName eq "");
		$applicantAddress = $APPLICANT_ADDRESS if($applicantAddress eq "");
		$applicantName = $APPLICANT_NAME if($applicantName eq "");
		$applicationType = $APPLICATION_TYPE if($applicationType eq "");
		$agentTelephone1 = $AGENT_TELEPHONE if($agentTelephone1 eq "");
		$agentTelephone2 = $AGENT_TELEPHONE1 if($agentTelephone2 eq "");
		$actualCommitteeDate = $ACTUAL_COMMITTEE_DATE if($actualCommitteeDate eq "");
		$actualCommitteeorPanelDate = $ACTUAL_COMMITTEE_OR_PANEL_DATE if($actualCommitteeorPanelDate eq "");
		$advertisementExpiryDate = $ADVERTISEMENT_EXPIRY_DATE if($advertisementExpiryDate eq "");
		$agreedExpiryDate = $AGREED_EXPIRY_DATE if($agreedExpiryDate eq "");
		$applicationExpiryDeadline = $APPLICATION_EXPIRY_DEADLINE if($applicationExpiryDeadline eq "");
		$temporaryPermissionExpiryDate = $TEMPORARY_PERMISSION_EXPIRY_DATE if($temporaryPermissionExpiryDate eq "");
		$noDocument = $NO_DOCUMENT if($noDocument eq "");
		
	}
	
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Pending\sConsideration)[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Pending\sDecision)[\w\W]*?$/$1/msi;
	$actualDecisionLevel=~s/\s*Expected\s*Decision\s*Level\s*Case\s*Officer\s*Parish\s*Ward\s*District\s*Reference\s*Applicant\s*Name\s*//msi;
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
		
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime) = @_;	
	
	my %activeTabContent = %{$TabContent};
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
		
	foreach my $AppDataKey (keys %activeTabContent)
	{				
				
		### Summary ###
		my ($DATE_DECISION_MADE,$APPLICATION_REFERENCE,$CASE_REFERENCE,$DECISION_STATUS,$DECISION_ISSUED_DATE,$PROPOSAL,$APPLICATION,$APPLICATION_STATUS,$SUMMARY_CASENUMBER,$SUMMARY_PROPOSAL,$DECISION_ISSUED_DATES,$DECISION_MADE_DATE,$DECISION_DATE,$DECISION_PRINTED_DATE,$DECISION,$AUTHORITY_DECISION_STATUS,$AUTHORITY_DECISION_DATE);
		$DATE_DECISION_MADE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_DECISION_MADE$/is);
		$APPLICATION_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_REFERENCE$/is);
		$CASE_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CASE_REFERENCE$/is);
		$DECISION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_STATUS$/is);
		$DECISION_ISSUED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATE$/is);
		$PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PROPOSAL$/is);
		$APPLICATION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION$/is);
		$APPLICATION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_STATUS$/is);
		$SUMMARY_CASENUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_CASENUMBER$/is);
		$SUMMARY_PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_PROPOSAL$/is);
		$AUTHORITY_DECISION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AUTHORITY_DECISION_STATUS$/is);
		$AUTHORITY_DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AUTHORITY_DECISION_DATE$/is);
		
		
		### Dates ###
		$DECISION_ISSUED_DATES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATES$/is);
		$DECISION_MADE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_MADE_DATE$/is);
		$DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_DATE$/is);
		$DECISION_PRINTED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_PRINTED_DATE$/is);
	
		### Details ###
		$DECISION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION$/is);
		
		
		if($Application eq '')
		{
			if($APPLICATION!~m/^\s*$/is)
			{
				$Application=$APPLICATION;
			}
			elsif($CASE_REFERENCE!~m/^\s*$/is)
			{
				$Application=$CASE_REFERENCE;
			}
			elsif($APPLICATION_REFERENCE!~m/^\s*$/is)
			{
				$Application=$APPLICATION_REFERENCE;
			}
			elsif($SUMMARY_CASENUMBER!~m/^\s*$/is)
			{
				$Application=$SUMMARY_CASENUMBER;
			}
		}
		
		if($Proposal=~m/^\s*$/is)
		{
			if($PROPOSAL!~m/^\s*$/is)
			{
				$Proposal=$PROPOSAL;
			}
			elsif($SUMMARY_PROPOSAL!~m/^\s*$/is)
			{
				$Proposal=$SUMMARY_PROPOSAL;
			}
		}
		
		if($Decision_Issued_Date=~m/^\s*$/is)
		{
			if($DECISION_ISSUED_DATE!~m/^\s*$/is)
			{
				$Decision_Issued_Date=$DECISION_ISSUED_DATE;
			}
			elsif($DECISION_ISSUED_DATES!~m/^\s*$/is)
			{
				$Decision_Issued_Date=$DECISION_ISSUED_DATES;
			}
		}	
		
		if($Decision_Status=~m/^\s*$/is)
		{
			if($DECISION_STATUS!~m/^\s*$/is)
			{
				$Decision_Status=$DECISION_STATUS;
			}
			elsif($DECISION!~m/^\s*$/is)
			{
				$Decision_Status=$DECISION;
			}
			elsif($AUTHORITY_DECISION_STATUS!~m/^\s*$/is)
			{
				$Decision_Status=$AUTHORITY_DECISION_STATUS;
			}
		}	
		
		if($Date_Decision_Made=~m/^\s*$/is)
		{
			if($DECISION_MADE_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DECISION_MADE_DATE;
			}
			elsif($DATE_DECISION_MADE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DATE_DECISION_MADE;
			}			
			elsif($DECISION_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DECISION_DATE;
			}
			elsif($DECISION_PRINTED_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DECISION_PRINTED_DATE;
			}
			elsif($AUTHORITY_DECISION_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$AUTHORITY_DECISION_DATE;
			}
		}

		if($Application_Status=~m/^\s*$/is)
		{
			if($APPLICATION_STATUS!~m/^\s*$/is)
			{
				$Application_Status=$APPLICATION_STATUS;
			}
		}	
	
	}
	
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	my $dateLength = length( $Date_Decision_Made );
	
	if($dateLength>50)
	{
		$Date_Decision_Made = '';
	}
	
	$Date_Decision_Made=~s/Not\s*Available[^>]*?//msi;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $Application;  undef $Proposal;  undef $Application_Status;   undef $Decision_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	return($insert_query);	
}


####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;
	return($data2Clean);
}


####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;

    # Get search results using date ranges	
	print "\nFromDate==>$receivedFromDate\tToDate==>$receivedToDate\n";
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $cookie_jar, $commonFileDetails, $scriptStatus);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	eval
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
		$cookie_jar = $receivedCouncilApp->cookie_jar;
		$receivedCouncilApp-> submit_form
		(
			form_number => $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'},
			fields      => 
			{
				$commonFileDetails->{$receivedCouncilCode}->{'FORM_START_ID'}  => $receivedFromDate,
				$commonFileDetails->{$receivedCouncilCode}->{'FORM_END_ID'}    => $receivedToDate,
			}
		);
	};
	
    $councilAppResponse = $receivedCouncilApp->content;
    my $currentPageURL = $receivedCouncilApp->uri();
	
    $scriptStatus = $receivedCouncilApp->status;
	
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=3))
	{
		$rerunCount++;
		goto Loop;
	}
		
    return ($councilAppResponse,$scriptStatus,$currentPageURL);
}


####
# Method to POST the pagenation settings for Online Councils
####

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $inputFromDate, $inputToDate, $responseContents) = @_;
	my ($nextPageLink,$commonFileDetails,$regexFile,$councilAppResponse,$councilAppStatus, $totalCont);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	if($responseContents=~m/<input[^>]*?name=\"_csrf\"\s*value=\"([^\"]*?)\"[^>]*?>/is)
	{
		my $csrf = $1;
		$nextPageLink = URI::URL->new($regexFile->{'MainRegexControl'}->{'SEARCH_NEXTPAGE_NA_LINK'})->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		$nextPageLink=$commonFileDetails->{$receivedCouncilCode}->{'FILTER_URL'} if($receivedCouncilCode=~m/^(48|49|70|87|164|194|217|242|298|342|460|203|381|72|338|58|237|232|422)$/is);
		my $refer = $nextPageLink."?action=firstPage";
		
		$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
		$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
		$receivedCouncilApp->add_header( "Referer" => "$refer" );
		my $post_cnt = "_csrf=$csrf&searchCriteria.page=1&action=page&orderBy=DateReceived&orderByDirection=Descending&searchCriteria.resultsPerPage=100";
		$receivedCouncilApp->post($nextPageLink, Content => "$post_cnt");
		$councilAppResponse = $receivedCouncilApp->content;
		$councilAppStatus = $receivedCouncilApp->status;
		$totalCont.= $councilAppResponse;
		
		if($councilAppResponse=~m/>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/is)
		{	
			my $page = 2;
			while($councilAppResponse=~m/<a[^>]*?href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/gsi)
			{
				my $nextHrefLink = $1;
				my $nextPageURL = URI::URL->new($nextHrefLink)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				$nextPageURL=~s/\&amp\;/\&/gsi;
				print "nextPageURL==>$nextPageURL\n";
				
				$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
				$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
				$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
				$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
				$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
				if($page == 2)
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageLink" );
				}
				else
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageURL" );
				}
				
				$receivedCouncilApp->get($nextPageURL);
				$councilAppResponse = $receivedCouncilApp->content;
				$totalCont.=$councilAppResponse;
				$page++;
			}
		}
	}
	elsif($receivedCouncilCode=~m/^(440)$/is)
	{		
		my $apacheToken = $1 if($responseContents=~m/<input[^>]*?name=\"org\.apache\.struts\.taglib\.html\.TOKEN\"\s*value=\"([^\"]*?)\"[^>]*?>/is);
		
		my $csrf = $1 if($responseContents=~m/<input[^>]*?name=\"_csrf\"\s*value=\"([^\"]*?)\"[^>]*?>/is);
		$nextPageLink = URI::URL->new($regexFile->{'MainRegexControl'}->{'SEARCH_NEXTPAGE_NA_LINK'})->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		my $refer = $nextPageLink."?action=firstPage";
		
		$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
		$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
		$receivedCouncilApp->add_header( "Referer" => "$refer" );
		my $post_cnt = "org.apache.struts.taglib.html.TOKEN=$apacheToken&_csrf=$csrf&searchCriteria.page=1&action=page&orderBy=DateReceived&orderByDirection=Descending&searchCriteria.resultsPerPage=100";
		$receivedCouncilApp->post($nextPageLink, Content => "$post_cnt");
		$councilAppResponse = $receivedCouncilApp->content;
		$councilAppStatus = $receivedCouncilApp->status;
		$totalCont.= $councilAppResponse;
		
		if($councilAppResponse=~m/>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/is)
		{	
			my $page = 2;
			while($councilAppResponse=~m/<a[^>]*?href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/gsi)
			{
				my $nextHrefLink = $1;
				my $nextPageURL = URI::URL->new($nextHrefLink)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				$nextPageURL=~s/\&amp\;/\&/gsi;
				print "nextPageURL==>$nextPageURL\n";
				
				$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
				$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
				$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
				$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
				$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
				if($page == 2)
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageLink" );
				}
				else
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageURL" );
				}
				
				$receivedCouncilApp->get($nextPageURL);
				$councilAppResponse = $receivedCouncilApp->content;
				$totalCont.=$councilAppResponse;
				$page++;
			}
		}
		
	}
	elsif($councilDetailsFile->{$receivedCouncilCode}->{'FILTER_URL'}=~m/N\/A/is)
	{
		$nextPageLink = URI::URL->new($regexFile->{'MainRegexControl'}->{'SEARCH_NEXTPAGE_NA_LINK'})->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
		my $refer = $nextPageLink."?action=firstPage";
		
		$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.9' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
		$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
		$receivedCouncilApp->add_header( "Referer" => "$refer" );
		
		$councilAppResponse = $receivedCouncilApp->post($nextPageLink, ['searchCriteria.page' => '1', 'action' => 'page', 'orderBy' => 'DateReceived', 'orderByDirection' => 'Descending', 'searchCriteria.resultsPerPage' => '100']);
		$councilAppResponse = $receivedCouncilApp->content;
		$councilAppStatus = $receivedCouncilApp->status;
		$totalCont.=$councilAppResponse;
		
		if($councilAppResponse=~m/>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/is)
		{
			my $page = 2;
			while($councilAppResponse=~m/<a[^>]*?href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/gsi)
			{
				my $nextHrefLink = $1;
				my $nextPageURL = URI::URL->new($nextHrefLink)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				$nextPageURL=~s/\&amp\;/\&/gsi;
				print "nextPageURL==>$nextPageURL\n";
				
				$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
				$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.9' );
				$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
				$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
				$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
				if($page == 2)
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageLink" );
				}
				else
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageURL" );
				}
				
				$receivedCouncilApp->get($nextPageURL);
				$councilAppResponse = $receivedCouncilApp->content;
				$totalCont.=$councilAppResponse;
				$page++;
			}
		}
	}
	else
	{	
		$nextPageLink = $councilDetailsFile->{$receivedCouncilCode}->{'FILTER_URL'};
		my $refer = $nextPageLink."?action=firstPage";
			
		$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
		$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
		$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
		$receivedCouncilApp->add_header( "Referer" => "$refer" );
		my $append = "?searchCriteria.page=1&action=page&orderBy=DateReceived&orderByDirection=Descending&searchCriteria.resultsPerPage=100";
		
		$nextPageLink=$nextPageLink.$append;
		print "nextPageLink==>$nextPageLink\n";
		
		$receivedCouncilApp->get($nextPageLink);
		$councilAppResponse = $receivedCouncilApp->content;
		$councilAppStatus = $receivedCouncilApp->status;
		$totalCont.= $councilAppResponse;
		if($councilAppResponse=~m/>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/is)
		{
			my $page = 2;
			while($councilAppResponse=~m/<a[^>]*?href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*Next\s*<img[^>]*?title\s*=\s*\"Display\s*the\s*next\s*page\s*of\s*search\s*results\"[^>]*?>/gsi)
			{
				my $nextHrefLink = $1;
				my $nextPageURL = URI::URL->new($nextHrefLink)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				$nextPageURL=~s/\&amp\;/\&/gsi;
				print "nextPageURL==>$nextPageURL\n";
				
				$receivedCouncilApp->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' );
				$receivedCouncilApp->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
				$receivedCouncilApp->add_header( "Accept-Encoding" => 'gzip, deflate' );
				$receivedCouncilApp->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
				$receivedCouncilApp->add_header( "Host" => $commonFileDetails->{$receivedCouncilCode}->{'HOST'} );
				if($page == 2)
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageLink" );
				}
				else
				{
					$receivedCouncilApp->add_header( "Referer" => "$nextPageURL" );
				}
				
				$receivedCouncilApp->get($nextPageURL);
				$councilAppResponse = $receivedCouncilApp->content;
				$totalCont.=$councilAppResponse;
				$page++;
			}
		}
	}
	
	if(($nextPageLink=~m/^http\:/is) && ($councilAppStatus!~m/^200$/is))
	{
		$nextPageLink=~s/^http\:/https\:/is;
		
		$receivedCouncilApp->get($nextPageLink);
		$councilAppResponse = $receivedCouncilApp->content;
		$councilAppStatus = $receivedCouncilApp->status;
		$totalCont = $councilAppResponse;
	}
	
	print "Overall page ping status==>$councilAppStatus\n";
	return($totalCont);
}

####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $category,$councilCode) = @_;
	
	my $tempURL = $receivedApplicationUrl;
	my $count = 0;
	Loop:
	sleep(rand(5));
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'CASE_REFERENCE'}/is)
	{
		$applicationNumber=$1;
	}
	elsif($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'APPLICATION_REFERENCE'}/is)
	{
		$applicationNumber=$1;
	}
	elsif($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'SUMMARY_CASENUMBER'}/is)
	{
		$applicationNumber=$1;
	}
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		$logName = $1 if($receivedApplicationUrl=~m/\&KeyVal\=([^\n]*?)$/is);
		
		if($logName eq "")
		{
			$logName = $1 if($receivedApplicationUrl=~m/KeyVal\=([^\&]*?)\&/is);
		}
		
		my $fileName= $category."_".$councilCode."_".$logName;
		
		
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	my %dataDetails;
	if($tempURL=~m/activeTab=summary/is)
	{
		print "ActiveTab ==> Summary\n";
		my %activeTabContents = &tabDetailsCollection($applicationDetailsPageContent, "summary", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	
	
	if(($councilCode!~m/^149$/is) && ($tempURL=~s/activeTab=summary/activeTab=details/is))
	{
		print "ActiveTab ==> Details\n";
		# print "ActiveTabURL ==> $tempURL\n";
		sleep(rand(10));
		$receivedCouncilApp->get($tempURL);
		my $detailsPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($detailsPageContent, "details", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	
	if(($category ne "Decision") && ($tempURL=~s/activeTab=details/activeTab=contacts/is))
	{
		print "ActiveTab ==> Contacts\n";
		# print "ActiveTabURL ==> $tempURL\n";
		sleep(rand(10));
		$receivedCouncilApp->get($tempURL);
		my $contactsPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($contactsPageContent, "contacts", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	elsif(($category eq "Decision") && ($tempURL=~s/activeTab=details/activeTab=dates/is))
	{
		print "ActiveTab ==> Dates\n";
		# print "ActiveTabURL ==> $tempURL\n";
		my $keyVal=$1 if($tempURL=~m/keyVal=([^>]*?)\&activeTab=dates\s*$/is);
		$tempURL=~s/^([^>]*?)(keyVal=[^>]*?)\&(activeTab=dates)\s*$/$1$3\&$2/igs;
		print "ActiveTabURL ==> $tempURL\n";
		sleep(rand(10));
		$receivedCouncilApp->get($tempURL);
		my $datesPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($datesPageContent, "dates", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	
	if(($category eq "Planning") && ($tempURL=~s/activeTab=contacts/activeTab=dates/is))
	{
		print "ActiveTab ==> Dates\n";
		# print "ActiveTabURL ==> $tempURL\n";
		sleep(rand(10));
		$receivedCouncilApp->get($tempURL);
		my $datesPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($datesPageContent, "dates", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	
	
	return(\%dataDetails, $applicationDetailsPageResponse, $applicationNumber);
	
}


####
# Scraping of details for an given Tab and Application
####

sub tabDetailsCollection()
{
	my ($tabContent, $blockSection, $category) = @_;
	my %tabDetails;
	my $regexFile;
	
    if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	foreach my $detailsKey (keys %{$regexFile->{$blockSection}}) {
		if($tabContent=~/$regexFile->{$blockSection}->{$detailsKey}/is)
		{
			push ( @{ $tabDetails{$detailsKey}}, $1);
		}
	}
	
	return(%tabDetails);
}
1;