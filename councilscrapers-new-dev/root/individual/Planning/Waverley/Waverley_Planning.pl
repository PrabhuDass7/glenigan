use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use Win32; 
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use JSON;
use Glenigan_DB_Windows;


my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"424\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Waverley/Waverley_Planning.ini" );

my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $POST_CNT = $Config->{$Council_Code}->{'POST_CNT'};
my $POST_DETAIL_URL = $Config->{$Council_Code}->{'POST_DETAIL_URL'};
my $POST_DETAIL_CNT = $Config->{$Council_Code}->{'POST_DETAIL_CNT'};
my $POST_NPAGE_CNT = $Config->{$Council_Code}->{'POST_NPAGE_CNT'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";

	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
}	


## Proxy settings ###
# if($Council_Code=~m/^(16)$/is)
# {
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
# }


my ($fDate,$tDate) = ($From_Date,$To_Date);

$fDate=~s/\//%2F/gsi;
$tDate=~s/\//%2F/gsi;

$HOME_URL=~s/<FromDate>/$fDate/gsi;
$HOME_URL=~s/<ToDate>/$tDate/gsi;

# Get search results using date ranges
my ($Responce, $cookie_jar, $Ping_Status1);
eval{
$Responce = $mech->get($HOME_URL);
$cookie_jar = $mech->cookie_jar;		
};

if ($@) {
	$Ping_Status1 = $@;
};	

# my $content = $mech->content;
# my $code = $mech->status;
# my $Search_Content=$content;

$POST_CNT=~s/<FromDate>/$From_Date/gsi;
$POST_CNT=~s/<ToDate>/$To_Date/gsi;

# print "POST_CNT==>$POST_CNT\n";

$mech->add_header( "Accept" => 'application/json, text/javascript, */*; q=0.01' );
$mech->add_header( "Host" => "$HOST" );
$mech->add_header( "Content-Type" => 'application/json; charset=utf-8' );
$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
$mech->add_header( "Accept-Encoding" => 'gzip, deflate' );
$mech->add_header( "Referer" => "$HOME_URL" );

$mech->post( $POST_URL, Content => "$POST_CNT");
sleep(10);				
my $Code_status = $mech->status;
my $Content = $mech->content;

# open(PP, ">app_cont.html");
# print PP "$Content\n";
# close(PP); 
# exit;

&scrape_details($Content);


sub scrape_details()
{
	my $dtlCont = shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $json = JSON->new;
	my $data = $json->decode($dtlCont);
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';

	my ($Application,$Address,$Proposal,$Case_Officer,$Application_Status,$Date_Application_Received,$Date_Application_validated,$Date_Application_Registered,$Applicant_name,$Applicant_Address,$Agent_Name,$Agent_Address,$Agent_Telephone,$Application_Type,$Doc_Url,$Application_Link,$TargetDec_dt,$Agent_Email);
	
	my @KeyObjects = @{$data->{'KeyObjects'}};
	foreach my $Keys ( @KeyObjects) 
	{							
		my $KeyText = $Keys->{'KeyText'};
		my $keyNumb = $Keys->{'KeyNumber'};
		print "KeyText=>$KeyText\n";
		print "keyNumb=>$keyNumb\n";
		
		$POST_DETAIL_CNT=~s/<KeyText>/$KeyText/gsi;
		$POST_DETAIL_CNT=~s/<keyNumb>/$keyNumb/gsi;
		
		$mech->post( $POST_DETAIL_URL, Content => "$POST_DETAIL_CNT");
						
		my $Code_status = $mech->status;
		my $detailContent = $mech->content;
		
		
		# open(PP, ">app_details_cont.html");
		# print PP "$detailContent\n";
		# close(PP); 
		# exit;

		
		my $detailData = $json->decode($detailContent);
		my $Items = $detailData->[0]->{'Items'};
		my $SummaryDetails = $detailData->[0]->{'SummaryDetails'};
		my @Items = @$Items;
		my @SummaryDetails = @$SummaryDetails;
		
			
		
		$Application_Link=$HOME_URL."#VIEW?RefType=GFPlanning&KeyNo=$keyNumb&KeyText=$KeyText";
		# print "Application_Link===>$Application_Link\n";
		
		$POST_DETAIL_CNT=~s/\"$KeyText\"\}$/\"\<KeyText\>\"\}/gsi;
		$POST_DETAIL_CNT=~s/\"$keyNumb\"\,\"keyText\"/\"\<keyNumb\>\"\,\"keyText\"/gsi;
		
		for my $Each_Time(@Items)
		{
			my $Label = $Each_Time->{'Label'};
			my $Value = $Each_Time->{'Value'};
			
			if($Label eq "Case No")
			{
				$Application=&clean($Value);
			}
			if($Label eq "Proposal")
			{
				$Proposal=&clean($Value);
			}
			if($Label eq "Application Address")
			{
				$Address=&clean($Value);
			}
			if($Label eq "Agent Address")
			{
				$Agent_Address=&clean($Value);
			}
			if($Label eq "Application Status")
			{
				$Application_Status=&clean($Value);
			}
			if($Label eq "Application Type")
			{
				$Application_Type=&clean($Value);
			}
			if($Label eq "Date Received")
			{
				$Date_Application_Received=&clean($Value);
			}
			if($Label eq "Date Registered")
			{
				$Date_Application_Registered=&clean($Value);
			}
			if($Label eq "Applicant Name")
			{
				$Applicant_name=&clean($Value);
			}
			if($Label eq "Agent Name")
			{
				$Agent_Name=&clean($Value);
			}
			if($Label eq "Applicant Address")
			{
				$Applicant_Address=&clean($Value);
			}
			if($Label eq "Case Officer")
			{
				$Case_Officer=&clean($Value);
			}
			if($Label eq "Date Valid")
			{
				$Date_Application_validated=&clean($Value);
			}
			if($Label eq "Target Date")
			{
				$TargetDec_dt=&clean($Value);
			}
			
			
			
			if($Date_Application_Received eq "")
			{
				$Date_Application_Received = $SummaryDetails[0]->{'Value'};
			}
		}
		
		# print "Application==>$Application\n";
		# print "Address==>$Address\n";
		# print "Proposal==>$Proposal\n";
		# print "Application_Status==>$Application_Status\n";
		# print "Date_Application_Received==>$Date_Application_Received\n";
		# print "Date_Application_validated==>$Date_Application_validated\n";
		# print "Date_Application_Registered==>$Date_Application_Registered\n";
		# print "Applicant_name==>$Applicant_name\n";
		# print "Applicant_Address==>$Applicant_Address\n";
		# print "Agent_Name==>$Agent_Name\n";
		# print "Agent_Address==>$Agent_Address\n";
		# print "Agent_Telephone==>$Agent_Telephone\n";
		# print "Application_Type==>$Application_Type\n";
		# print "Doc_Url==>$Doc_Url\n";
		# print "Application_Link==>$Application_Link\n";
		# print "TargetDec_dt==>$TargetDec_dt\n";
		# print "Agent_Email==>$Agent_Email\n";
		# <STDIN>;
		
		if($Doc_Url eq "")
		{
			$Doc_Url = $Application_Link;
		}
		
		my $Source = $Date_Range;	
		# my $Source = "GCS001";	
	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";	
		
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
		
		
		$Application=""; $Address=""; $Proposal=""; $Application_Status=""; $Date_Application_validated=""; $Date_Application_Registered=""; $Applicant_name=""; $Applicant_Address=""; $Agent_Name=""; $Agent_Address=""; $Agent_Telephone=""; $Date_Application_Received=""; $TargetDec_dt=""; $Doc_Url=""; $Application_Link=""; $Agent_Email=""; $Application_Type=""; $Source_With_Time="";			
	}
	$insert_query=~s/\,$//igs;
		
	print "insert_query::$insert_query\n";
	
	
	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
}
		

sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###### DB Connection ####
sub DbConnection()
{
	# my $dsn 						= 'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dsn 						= 'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}