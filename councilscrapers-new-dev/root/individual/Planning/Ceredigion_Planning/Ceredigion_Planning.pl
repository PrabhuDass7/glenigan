use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"147\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

my $dumFrom_Date = $From_Date;
my $dumTo_Date = $To_Date;
$dumFrom_Date=~s/\//\-/gsi;
$dumTo_Date=~s/\//\-/gsi;
print "Council_Code: $Council_Code\n";
print "From_Date: $dumFrom_Date\n";
print "To_Date: $dumTo_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Ceredigion_Planning/Ceredigion_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";


#### UserAgent Declaration ####	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0, autoclose => 1);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
    verify_hostname => 0, 
	autoclose => 1,
});
}	

### Proxy settings ###
if($Council_Code=~m/^(147)$/is)
{
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
}

# Find home page url
my $Home_Url;
if($HOME_URL=~m/^(https?\:\/\/.*?)\//is)
{
	$Home_Url=$1;
}

# Get search results using date ranges

Retry:
my ($Responce, $cookie_jar, $Ping_Status1);
eval{
$Responce = $mech->get($HOME_URL);
$cookie_jar = $mech->cookie_jar;

$mech->form_number($FORM_NUMBER); 
# $mech->set_fields( $RADIO_BTN_NME => $RADIO_BTN_VAL );
# $mech->click();
$mech->set_fields( $FORM_START_DATE => $dumFrom_Date, $FORM_END_DATE => $dumTo_Date );	
$mech->click();
		
};

if ($@) {
	$Ping_Status1 = $@;
};	

my $content = $mech->content;
my $Search_Content=$content;

# open(PP, ">app_cont.html");
# print PP "$Search_Content\n";
# close(PP); 
# exit;

&Search_Content( $Search_Content);

sub Search_Content()
{
	my $appContent = shift;
	
	my $i = 1;
	if($appContent=~m/<table\s*class\s*=\s*\"table\s*table-bordered\s*table-hover\s*table-condensed[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
	{		
		my $appDetails = $1;
		while($appContent=~m/<tr>\s*<td>\s*([^<]*?)\s*<\/td>[\w\W]*?\s*<td>\s*<button[^>]*?data-id=\"([^\"]*?)\">\s*[\w\W]*?\s*<\/button>\s*<\/td>\s*<\/tr>/isg)
		{
			my $Application_number = $1;
			my $APPPAGE = $2;
			my $Page_url = "https://ceredigion-online.tascomi.com/planning/index.html?fa=getApplication&id=$APPPAGE";
			
			
			$mech->get($Page_url);
			
			my $App_content = $mech->content;	
			my $Code = $mech->status;		
			
			# open(PP,">147_$i.html");
			# print PP "$App_content\n";
			# close(PP);
			# exit;
			
			$i++;
			
			&Scrape_Details($Council_Code,$App_content,$Code,$Page_url,$Application_number);
			
		}
	}
}

sub Scrape_Details()
{
	my $Council_Code=shift;
	my $App_Page_Content=shift;
	my $codes=shift;
	my $Page_url=shift;
	my $Application=shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	print "Council_Code: $Council_Code\n";
	
	
	my $Application					= &clean($1) if($App_Page_Content=~m/>\s*Application\s*Reference\s*Number\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Application_Type			= &clean($1) if($App_Page_Content=~m/>\s*Application\s*type\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Address						= &clean($1) if($App_Page_Content=~m/>\s*Location\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);	
	my $Proposal					= &clean($1) if($App_Page_Content=~m/>\s*Proposal\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);	
	my $Date_Application_Received	= &clean($1) if($App_Page_Content=~m/>\s*Received\s*Date\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Date_Application_validated	= &clean($1) if($App_Page_Content=~m/>\s*Valid\s*Date\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/>\s*Registered\s*Date\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);	
	my $Applicant_name				= &clean($1) if($App_Page_Content=~m/>\s*Applicant\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Agent_Name					= &clean($1) if($App_Page_Content=~m/>\s*Agent\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Application_Status					= &clean($1) if($App_Page_Content=~m/>\s*Application\s*Status\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	
	my $Agent_Address;
	my $Applicant_Address;
	my $Agent_Telephone;
	my $TargetDec_dt;
	my $Doc_Url=$Page_url;
	my $Application_Link=$Page_url;
	my $Agent_Email;
	
	$Address=~s/\'/\'\'/gsi;	
	my $Source = $Date_Range;	
	
	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
	my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\')";
	
	undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
	
	
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}