use strict;
use WWW::Mechanize::Firefox;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $inFrom_Date = $ARGV[1];
my $inTo_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"172\" \"01/07/2017\" \"31/07/2017\"\)", 16, "Error message");
    exit();
}


my ($From_Date, $To_Date) = ($inFrom_Date, $inTo_Date);

print "Council_Code: $Council_Code\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();


system('"C:/Program Files/Mozilla Firefox/firefox.exe"');


### Get Council Details from ini file ###
my $Config = Config::Tiny->new();

$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Darlington_Planning/Darlington_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";


#### UserAgent Declaration ####	
my $mech = WWW::Mechanize::Firefox->new(autoclose => 1);

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}


# Get search results using date ranges

$mech->get($HOME_URL);
sleep(5);
$mech->form_number($FORM_NUMBER);
$mech->set_fields('_id113:SDate5From', $From_Date);
$mech->set_fields('_id113:SDate5To', $To_Date);
$mech->click({ xpath => "//*[\@id=\"_id113:_id182\"]" });
sleep(10);


my $content = $mech->content;
my $code = $mech->status;
print "$code\n";
my $datePage_urls = $mech->uri();
my $Home_Content=$content;

# open(PPP,">Search_Content1.html");
# print PPP "$Home_Content\n";
# close(PPP);
# exit;
&Scrape_Details($Home_Content,$datePage_urls);

my $pageCount;
if($Home_Content=~m/<table\s*class=\"scroller\">\s*([\w\W]*?)\s*<\/table>/is)
{
	my $page_num = $1;
	my $count=0;
	while($page_num=~m/<td[^>]*?>\s*<a\s*href=\"#\"\s*onclick=\"return oamSubmitForm[^>]*?>(\d+)<\/a>\s*<\/td>/gsi)
	{
		$count++;
	}
	$pageCount=$count;
}

print "TotalPage==>$pageCount\n";

my $i=2;
next_page:
if($i<=$pageCount)
{	
	print "$i======$pageCount\n";	
	$mech->click({ xpath => "//*[\@id=\"_id116:scroll_2idx$i\"]" });
	sleep(5);
	my $datePage_url = $mech->uri();
	my $nextpagecontent = $mech->content;
	# open(PP,">Search_Content$i.html");
	# print PP "$nextpagecontent\n";
	# close(PP);
	
	&Scrape_Details($nextpagecontent,'http://msp.darlington.gov.uk/Planning/lg/GFPlanningSearchResults.page');
	# &Scrape_Details($nextpagecontent,$datePage_url);
	
	
	$i++;
	goto next_page;
}
else
{
	&Scrape_Detail($Home_Content,$datePage_urls);
}

sub Scrape_Detail
{
	my $App_Page_Content = shift;
	my $App_Link = shift;
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';

		
	my $Application					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Case\s*No\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $Application_Type			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Case\s*Type\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $Address						= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Application\s*Address\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);	
	my $Proposal					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Proposal\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);	
	my $Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Date\s*Received\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Date\s*Valid\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Date\s*Registered\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);	
	my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Applicant\s*Name\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Agent\s*Name\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Case\s*Officer\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	my $TargetDec_dt				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Target\s*Date\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	
	
	my $Application_Status			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Status\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	
	my $Agent_Telephone;
	my $Applicant_Address;
	my $Agent_Address;
	my $Doc_Url=$App_Link;
	my $Application_Link=$App_Link;
	my $Agent_Email;
	
	$Address=~s/\'/\'\'/gsi;	
	my $Source = 'GCS001';	
	
	
	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
	$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
	
	undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
		
		
	
	
	
		
	$insert_query=~s/\,$//igs;
	
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
	
}

# system("taskkill /IM firefox.exe /F");

sub Scrape_Details
{
	my $App_PageContent = shift;
	my $App_Link = shift;
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	# my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';
	while($App_PageContent=~m/<input\s*id=\"[^>]*?results[^>]*?\"\s*name=\"([^\"]*?)\"\s*value=\"([^\"]*?)\"[^>]*?>/gsi)
	{
		my $click=$1;
		my $App_Num=$2;
		
		print "Application_Number==>$App_Num\n";
		
		$mech->click({ xpath => "//*[\@id=\"$click\"]" });
		sleep(7);
		my $App_Links = $mech->uri();
		my $App_Page_Content = $mech->content;
		# open(PP,">App_Content.html");
		# print PP "$App_Page_Content\n";
		# close(PP);
		# exit;
		
		my $Application					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Case\s*No\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Application_Type			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Case\s*Type\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Address						= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Application\s*Address\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);	
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Proposal\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);	
		my $Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Date\s*Received\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Date\s*Valid\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Date\s*Registered\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);	
		my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Applicant\s*Name\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Agent\s*Name\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Case\s*Officer\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $TargetDec_dt			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Target\s*Date\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		
		
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Status\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		
		my $Agent_Telephone;
		my $Applicant_Address;
		my $Agent_Address;
		my $Doc_Url=$App_Link;
		my $Application_Link=$App_Link;
		my $Agent_Email;
		
		$Address=~s/\'/\'\'/gsi;	
		my $Source = 'GCS001';	
		
		if($Application=~m/^\s*$/is)
		{
			$Application=$App_Num;
		}
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		# $insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
		my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\')";
		print "insert_query::$insert_query\n";

		if($insert_query!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
		}
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
		
		my $back = $1 if($App_Page_Content=~m/<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"Back\"[^>]*?>/is);
		$mech->click({ xpath => "//*[\@id=\"$back\"]" });
		sleep(5);
	}
	
	
		
	# $insert_query=~s/\,$//igs;
	
	# print "insert_query::$insert_query\n";

	# if($insert_query!~m/values\s*$/is)
	# {
		# &Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	# }
	
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}


######  Clean  ######
sub clean()
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	
	return($Data);
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}