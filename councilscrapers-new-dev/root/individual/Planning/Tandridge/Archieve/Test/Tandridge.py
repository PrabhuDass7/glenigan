import requests
from bs4 import BeautifulSoup
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

url = "https://tdcplanningsearch.tandridge.gov.uk/"
# captcha_api_key = "b31ffb49e380cea644c476de3931ba78"
captcha_api_key = "18c788d08a948697708ea27a0207871f"

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

proxies2 = {
  'http': 'http://172.27.137.199:3128',
  'https': 'http://172.27.137.199:3128',
}

# To collect Application URLs here
def collectAppURL(content, home_url):	
	try:		 
		regionURLs=[]
		if re.findall(r'<tr>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*([^<]*?)\s*<\/a>\s*<\/td>', str(content), re.IGNORECASE):
			regexMatched=re.findall(r'<tr>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*([^<]*?)\s*<\/a>\s*<\/td>', str(content), re.IGNORECASE)       
			
			for appDetails in regexMatched:
				appURL,appNum = appDetails
				if not re.findall(r'^http', str(appURL)):
					appURL = urljoin(home_url,appURL)
				# appNumber = re.sub(r'\/', "", str(appNum))
				# print("AfterappURL::",appURL)
				regionURLs.append(appURL)	
		else:
			print("App Page not found. Please check!!!!!")
			
		return regionURLs	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 



# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)
	
# Proxy Section
def proxiesGenerator(proxies1,proxies2):    
	i = 0
	while i <= 1:
		try:
			print("Main URL is: ", url)
			res = requests.get(url, proxies=proxies1)			
			if res.status_code == 200:				
				return proxies1
				
		except Exception as ex:
			print("Error is: ", str(type(ex).__name__))
			
			if str(type(ex).__name__) == "ProxyError":
				while i <= 2:
					print("Now trying in second Proxy", proxies2)
					res = requests.get(url, proxies=proxies2)
					print("second proxy URL status is: ", res.status_code)
					if res.status_code == 200:
						return proxies2
					else:
						i = i + 1		
						

# Clean function
def clean(cleanValue):
    try:
		clean=''
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print(ex,sys.exc_traceback.tb_lineno)
						

# To collect Application URLs here
def collectAppDetails(fullAppURLs, home_url, CouncilCode, Source, conn, cursor, proxy):	
	try:		 
		bulkValuesForQuery=''
		
		appCount = 0
		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date,Easting,Northing) values '
		
		for appURL in fullAppURLs:
			print("Now processing this URL::",appURL)
			res = requests.get(appURL, proxies=proxy)
			Appcontent = res.content
			
			print("Appcontent Response::",res.status_code)
			
			with open("Appcontent.html", "wb") as f:
				f.write(Appcontent)
			
			print ("Appcontent Compledted**")
			raw_input("STT")
			
			Application_Number=re.findall(r'<h[\d]+>\s*Application\s+([^<]*?)\s*<\/h[\d]+>', str(Appcontent), re.IGNORECASE)
			Proposal=re.findall(r'>\s*Summary\s*<\/h[\d]+>\s*<p>\s*([^<]*?)\s*<\/p>', str(Appcontent), re.IGNORECASE)
			Application_Type=re.findall(r'>\s*Application\s*Type\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>', str(Appcontent), re.IGNORECASE)
			Address=re.findall(r'>\s*Address\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>', str(Appcontent), re.IGNORECASE)
			Application_Status=re.findall(r'>\s*Decision\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>', str(Appcontent), re.IGNORECASE)
			DtAppReg=re.findall(r'<li>\s*Application\s*registered\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>', str(Appcontent), re.IGNORECASE)
			DtAppValid=re.findall(r'<li>\s*Application\s*validated\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>', str(Appcontent), re.IGNORECASE)
			DtAppReceived=re.findall(r'<li>\s*Application\s*received\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>', str(Appcontent), re.IGNORECASE)
			docURL=re.findall(r'<a\s*href\=\"([^\"]*?)\"[^>]*?>\s*View\s*all\s*documents\s*<\/a>\s*<\/li>', str(Appcontent), re.IGNORECASE)
			
						
			(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url,east,north) = ('','','','','','','','','','','','','','','','','','')

			if len(Application_Number) > 0:
				Application=clean(Application_Number[0])
			
			if len(Proposal) > 0:
				ProposalCnt=clean(Proposal[0])

			if len(Address) > 0:
				AppAddress=clean(Address[0])

			if len(Application_Status) > 0:
				ApplicationStatus=clean(Application_Status[0])

			if len(Application_Type) > 0:
				ApplicationType=clean(Application_Type[0])

			if len(docURL) > 0:
				if not re.findall(r'^http', str(docURL[0])):
					Document_Url = urljoin(appURL,docURL[0])
				else:
					Document_Url=clean(docURL[0])
							
			if len(DtAppReg) > 0:
				DateApplicationRegistered=DtAppReg[0]
			
			if len(DtAppReceived) > 0:
				DateApplicationReceived=DtAppReceived[0]				
				
			if len(DtAppValid) > 0:
				DateApplicationvalidated=DtAppValid[0]
				
			print("Application::",Application)
			# print("ProposalCnt::",ProposalCnt)
			# print("ApplicationStatus::",ApplicationStatus)
			# print("AppAddress::",AppAddress)
			# print("ApplicationType::",ApplicationType)
			# print("Document_Url::",Document_Url)
			# print("DateApplicationRegistered::",DateApplicationRegistered[0])
			# print("DateApplicationReceived::",DateApplicationReceived[0])
			# print("DateApplicationvalidated::",DateApplicationvalidated[0])
			raw_input("STOP***")
								
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			
			Council_Name = "Tandridge"
			
			joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived)+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated)+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"','"+str(east)+"','"+str(north)+"')"
			Application='test'
			print("ApplicationValues: ", joinValues)
			print("Application LEN: ", len(Application))
			print("bulkValuesForQuery::",bulkValuesForQuery)
			raw_input("FINAL*****")
			
			if len(Application) != 0:
				print ("HIIII")
				if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
				elif appCount == 5:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					appCount=0
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					appCount += 1	
				
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print(e,sys.exc_traceback.tb_lineno)
		
######### Main function ##########
def main(fdate, tdate, conn, cursor, gcs, CouncilCode):
	
	finProxy = proxiesGenerator(proxies1,proxies2)
		
	print("Final working proxy is: ", finProxy)
	
	"""creating request session"""
	headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp"
		",image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
		"Connection": "keep-alive",
		"Upgrade-Insecure-Requests": "1",
		"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36"
		" (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
		"Host": "tdcplanningsearch.tandridge.gov.uk",
	}
	s = requests.session()
	s.headers.update(headers)

	response = s.get(url, proxies= finProxy)
	soup = BeautifulSoup(response.text, "html.parser")
	recaptcha_key = soup.select_one(".g-recaptcha").get(
		"data-sitekey"
	)  # retrieve recaptcha sitekey


	"""send google recaptcha sitekey to captcha solving services and retrieve the g_recaptcha_response"""

	captcha_url = 'https://2captcha.com/in.php?key='+captcha_api_key+'&method=userrecaptcha&googlekey='+recaptcha_key+'&pageurl='+url+'&json=1'
	request_id = requests.get(captcha_url).json().get("request")
	captcha_result_url = 'https://2captcha.com/res.php?key='+captcha_api_key+'&action=get&id='+request_id+'&json=1'
	timeout = 60
	time_taken = 0

	while time_taken <= timeout:
		time.sleep(3)
		time_taken = 3 + time_taken
		g_recaptcha_response = requests.get(captcha_result_url).json().get("request")
		print(g_recaptcha_response)
		if g_recaptcha_response != "CAPCHA_NOT_READY":
			break


	"""send the g_recaptcha_response to the server"""
	search_url = 'https://tdcplanningsearch.tandridge.gov.uk/Home/Search'
	payload = 'searchType=AcknowledgedDate&reference=&address=&parish=Bletchingley&startDate='+fdate+'&endDate='+tdate+'&x=32&y=22&g-recaptcha-response='+g_recaptcha_response
	# payload = 'searchType=AcknowledgedDate&reference=&address=&parish=Bletchingley&startDate=2019-10-01&endDate=2019-11-19&x=32&y=22&g-recaptcha-response='+g_recaptcha_response
	
	s.headers.update(
		{
			"Content-Type": "application/x-www-form-urlencoded",
			"Origin": "https://tdcplanningsearch.tandridge.gov.uk",
			"Referer": "https://tdcplanningsearch.tandridge.gov.uk/",
			"Content-Length": str(len(payload)),
		}
	)
	response = s.post(search_url, data=str(payload), proxies= finProxy)
	searchcontent = response.content
	# print(response.headers)
	# print(s.cookies)
	# print(response.history)
	print("SearchPost Response::",response.status_code)
	with open("response.html", "wb") as f:
		f.write(response.content)
	
	pageNumber = 2
	AppURLs=[]
	if re.findall(r'<a[^>]*?href=\"[^\"]*?\"\s*onclick=\"goTo\('+str(pageNumber)+'\)\;\">\s*'+str(pageNumber)+'\s*<\/a>', str(searchcontent)):
		fullURL = collectAppURL(str(searchcontent), search_url)
		AppURLs.extend(fullURL)
		
		check_next_page = re.findall(r'<a[^>]*?href=\"[^\"]*?\"\s*onclick=\"goTo\('+str(pageNumber)+'\)\;\">\s*('+str(pageNumber)+')\s*<\/a>', str(searchcontent), re.I)
		print("Page::::",check_next_page)
		while (check_next_page[0]):	
			if re.findall(r'<a[^>]*?href=\"[^\"]*?\"\s*onclick=\"goTo\('+str(pageNumber)+'\)\;\">\s*'+str(pageNumber)+'\s*<\/a>', str(searchcontent), re.I):	
				nextPageURL = 'https://tdcplanningsearch.tandridge.gov.uk/Home/GoToPage?page='+check_next_page[0]
				
				# print("Type::::",type(nextPageURL))			
				
				print("NextPage URL::", nextPageURL)
				
				getCookie = s.cookies.get_dict ()
				print ("getCookie:::::", getCookie)			
								
				reqHeaders = {
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
							"Host": "tdcplanningsearch.tandridge.gov.uk",
							"X-Requested-With": "XMLHttpRequest",
							"Content-Type": "text/html; charset=utf-8",
							"Accept-Language": "en-US,en;q=0.9",
							"Accept": "text/html, */*; q=0.01",
							"Accept-Encoding": "gzip, deflate, br",
							"Referer": "https://tdcplanningsearch.tandridge.gov.uk/", 
							"Cookie": "ASP.NET_SessionId="+getCookie["ASP.NET_SessionId"]
				}
							
				response = requests.get(nextPageURL, headers = reqHeaders, proxies= finProxy)
				appNextPagecontent = response.content
				with open("appNextPagecontent_"+str(pageNumber)+".html", "wb") as f:
					f.write(appNextPagecontent)	
					
				fullURL = collectAppURL(str(appNextPagecontent), nextPageURL)		
				AppURLs.extend(fullURL)
			
				pageNumber = pageNumber + 1  
				
				if re.findall(r'<a[^>]*?href=\"[^\"]*?\"\s*onclick=\"goTo\('+str(pageNumber)+'\)\;\">\s*('+str(pageNumber)+')\s*<\/a>', str(appNextPagecontent), re.I):
					check_next_page = re.findall(r'<a[^>]*?href=\"[^\"]*?\"\s*onclick=\"goTo\('+str(pageNumber)+'\)\;\">\s*('+str(pageNumber)+')\s*<\/a>', str(appNextPagecontent), re.I)
			
					print("pageNumber:",pageNumber)          
								 
					if check_next_page != "":
						continue
					else:
						break
			else:
				break
	else:
		fullURL = collectAppURL(str(searchcontent), search_url)		
		AppURLs.extend(fullURL)
	
	finalinsertQuery = collectAppDetails(AppURLs, search_url, CouncilCode, gcs, conn, cursor, finProxy)
	
	if (finalinsertQuery != ''):
		cursor.execute(finalinsertQuery)
		conn.commit()

	
if __name__ == '__main__':
	councilCode = sys.argv[1]
	sourceGCS = sys.argv[2]
	
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	print("gcs::",sourceGCS)
	
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()
	
	gcsdate = thisgcs[sourceGCS]	
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%Y-%m-%d"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	
	print('fromdate  :', fromdate)
	print('todate    :', todate)
	
	main(fromdate, todate, conn, cursor, sourceGCS, councilCode)