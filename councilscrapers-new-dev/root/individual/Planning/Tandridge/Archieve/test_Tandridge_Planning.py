import requests
from bs4 import BeautifulSoup
import time
import os, re, sys
from datetime import datetime, timedelta
import ssl
# from urlparse import urljoin

url = "https://tdcplanningsearch.tandridge.gov.uk/"
# captcha_api_key = "b31ffb49e380cea644c476de3931ba78"
captcha_api_key = "18c788d08a948697708ea27a0207871f"



headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp"
		",image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
		"Connection": "keep-alive",
		"Upgrade-Insecure-Requests": "1",
		"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36"
		" (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
		"Host": "tdcplanningsearch.tandridge.gov.uk",
	}

s = requests.session()
s.headers.update(headers)

response = s.get(url)
soup = BeautifulSoup(response.text, "html.parser")
recaptcha_key = soup.select_one(".g-recaptcha").get(
	"data-sitekey"
)  # retrieve recaptcha sitekey


"""send google recaptcha sitekey to captcha solving services and retrieve the g_recaptcha_response"""

captcha_url = 'https://2captcha.com/in.php?key='+captcha_api_key+'&method=userrecaptcha&googlekey='+recaptcha_key+'&pageurl='+url+'&json=1'
request_id = requests.get(captcha_url).json().get("request")
captcha_result_url = 'https://2captcha.com/res.php?key='+captcha_api_key+'&action=get&id='+request_id+'&json=1'
timeout = 60
time_taken = 0

while time_taken <= timeout:
	time.sleep(3)
	time_taken = 3 + time_taken
	g_recaptcha_response = requests.get(captcha_result_url).json().get("request")
	print(g_recaptcha_response)
	if g_recaptcha_response != "CAPCHA_NOT_READY":
		break


"""send the g_recaptcha_response to the server"""
search_url = 'https://tdcplanningsearch.tandridge.gov.uk/Home/Search'
# payload = 'searchType=AcknowledgedDate&reference=&address=&parish=Bletchingley&startDate='+fdate+'&endDate='+tdate+'&x=32&y=22&g-recaptcha-response='+g_recaptcha_response
payload = 'searchType=AcknowledgedDate&reference=&address=&parish=Bletchingley&startDate=2020-11-17&endDate=2020-11-24&x=32&y=22&g-recaptcha-response='+g_recaptcha_response

# s.headers.update(
	# {
		# "Content-Type": "application/x-www-form-urlencoded",
		# "Origin": "https://tdcplanningsearch.tandridge.gov.uk",
		# "Referer": "https://tdcplanningsearch.tandridge.gov.uk/",
		# "Content-Length": str(len(payload)),
	# }
# )

headers={
		"Content-Type": "application/x-www-form-urlencoded",
		"Origin": "https://tdcplanningsearch.tandridge.gov.uk",
		"Referer": "https://tdcplanningsearch.tandridge.gov.uk/",
		"Content-Length": str(len(payload)),
	}

response = s.post(search_url, data=str(payload),headers=headers)
searchcontent = response.content

print("SearchPost Response::",response.status_code)
with open("response.html", "wb") as f:
	f.write(response.content)
# obj=s.get("https://tdcplanningsearch.tandridge.gov.uk/Planning/Planning/Planning?reference=2020-2057-TCA")
obj=s.get("https://tdcplanningsearch.tandridge.gov.uk/Planning/Planning/Planning?reference=2020-2054-NC")
finalcontent=obj.content
print("SearchPost Response::",obj.status_code)
with open("final.html", "wb") as f:
	f.write(finalcontent)

