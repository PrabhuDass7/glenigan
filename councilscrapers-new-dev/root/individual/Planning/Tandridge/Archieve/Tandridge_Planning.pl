use strict;
use WWW::Mechanize;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use IO::Socket::SSL;

# Establish connection with DB server
my $dbh = &DbConnection();

my $mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
			

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

my $homeURL = "https://tdcplanningsearch.tandridge.gov.uk/";
my $inputpath = "C:/Glenigan/Live_Schedule/Planning/Tandridge/html";



opendir(DIR, $inputpath) or die $!;
my @htmlfiles = grep(/\.html$/, readdir(DIR));
closedir(DIR);

foreach my $htmlfile (@htmlfiles)
{
	print "$htmlfile\n";
	open(HTM, "$inputpath\\$htmlfile");
	undef $/;
	my $htmlCnt = <HTM>;
	close(HTM);
	
	my @appURL;
	my $appFullURL;
	while($htmlCnt=~m/<tr>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*([^<]*?)\s*<\/a>\s*<\/td>/igs)
	{
		my $applicationURL = $1;
		my $application = $2;
		
		if($applicationURL!~m/^http/is)
		{
			my $u1=URI::URL->new($applicationURL,$homeURL);
			my $u2=$u1->abs;
			$appFullURL = $u2;
		}
		else
		{
			$appFullURL = $applicationURL;
		}
		push(@appURL, $appFullURL)
	}
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insertQuery="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values ";
	my $count=1;
	my $bulkValuesForQuery='';
	my $applicationCount=0;
	foreach my $Page_url (@appURL)
	{
		# print "$count==>$Page_url\n";
		$count++;
		
		my $App_Page_Content;
		eval{
			$mech->get($Page_url);
			$App_Page_Content = $mech->content;
		};
		
		
		my $Proposal					= &clean($1) if($App_Page_Content=~m/>\s*Summary\s*<\/h3>\s*<p>\s*([^<]*?)\s*<\/p>/is);	
		my $Application					= &clean($1) if($App_Page_Content=~m/<h2>\s*Application\s+([^<]*?)\s*<\/h2>/is);	
		my $Application_Type			= &clean($1) if($App_Page_Content=~m/>\s*Application\s*Type\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);	
		my $Address						= &clean($1) if($App_Page_Content=~m/>\s*Address\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/>\s*Decision\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Applicant_name;
		my $Applicant_Address;
		my $Agent_Name	;
		my $Agent_Address;
		my $Case_Officer;
	
		my $Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<li>\s*Application\s*registered\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>/is);
		my $Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<li>\s*Application\s*validated\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>/is);
		my $Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<li>\s*Application\s*received\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>/is);
		my $Doc_Url						= &clean($1) if($App_Page_Content=~m/<a\s*href=\"([^\"]*?)\"\s*>\s*View\s*all\s*documents\s*<\/a>\s*<\/li>/is);
		
		my $Agent_Telephone;
		my $TargetDec_dt;
		my $Application_Link=$Page_url;
		my $Agent_Email;
		
		$Proposal=~s/\'/\'\'/gsi;	
		$Address=~s/\'/\'\'/gsi;	
		$Applicant_name=~s/\'/\'\'/gsi;	
		$Agent_Name=~s/\'/\'\'/gsi;	
		$Agent_Address=~s/\'/\'\'/gsi;	
		$Applicant_Address=~s/\'/\'\'/gsi;	
		$Application_Status=~s/\'/\'\'/gsi;	
		my $Source = "GCS001";			
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		my $COUNCIL_NAME = "Tandridge";
		my $Council_Code = "394";
		
		print "ApplicationNo==>$Application\n";
		
		
		next if($Application eq "");
		
		
		my $joinValues = "(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'), ";
		
		if($applicationCount == 0)
		{
			$bulkValuesForQuery = $insertQuery.$joinValues;
			$applicationCount++;
		}
		elsif($applicationCount == 10)
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			
			$bulkValuesForQuery=~s/\,\s*$//igs;
			&DB_Insert($dbh,$bulkValuesForQuery);
			$applicationCount = 0;
			$bulkValuesForQuery="";
		}
		else
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			$applicationCount++;
		}
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;	
	}
	
	$bulkValuesForQuery=~s/\,\s*$//igs;
	if($bulkValuesForQuery ne "")
	{
		&DB_Insert($dbh,$bulkValuesForQuery);
	}
}



###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### DB Connection ####
sub DbConnection()
{
    my $dsn                          =  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
    my $dbh                          =    DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
   
    if(!$dbh)
    {
        &DBIconnect($dsn);
    }
    else
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}