use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
my $From_Date = $ARGV[1];
my $To_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"467\" \"GCS001\"\)", 16, "Error message");
    exit();
}

# chomp($Date_Range);

# my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


# my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";
if($From_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$sDay = $1;
	# $sMonth = $month{$2};
	$sMonth = $2;
	$sYear = $3;
	$sDay=~s/^0//si;
	$sMonth=~s/^0//si;
	print "sDay==>$sDay \n";
	print "sMonth==>$sMonth \n";
	print "sYear==>$sYear \n";
}
if($To_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$eDay = $1;
	# $eMonth = $month{$2};
	$eMonth = $2;
	$eYear = $3;
	
	$eDay=~s/^0//si;
	$eMonth=~s/^0//si;
	print "eDay==>$eDay \n";
	print "eMonth==>$eMonth \n";
	print "eYear==>$eYear \n";
}


my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://www.gov.je/citizen/Planning/Pages/planning.aspx";
$mech->get($searchURL);
my $Content = $mech->content;

my $searchPagePostCont = "__SPSCEditMenu=true&_wpcmWpid=&wpcmVal=&MSOWebPartPage_PostbackSource=&MSOTlPn_SelectedWpId=&MSOTlPn_View=0&MSOTlPn_ShowSettings=False&MSOGallery_SelectedLibrary=&MSOGallery_FilterString=&MSOTlPn_Button=none&__EVENTTARGET=&__EVENTARGUMENT=&__REQUESTDIGEST=<REQUESTDIGEST>&MSOSPWebPartManager_DisplayModeName=Browse&MSOSPWebPartManager_ExitingDesignMode=false&MSOWebPartPage_Shared=&MSOLayout_LayoutChanges=&MSOLayout_InDesignMode=&MSOSPWebPartManager_OldDisplayModeName=Browse&MSOSPWebPartManager_StartWebPartEditingName=false&MSOSPWebPartManager_EndWebPartEditing=false&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlPlanningRegisters=1&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtPlanningApplicationReferenceNumber=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlPlanningApplicationStatus=All&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlFromDay=<FromDay>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlFromMonth=<FromMonth>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlFromYear=<FromYear>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlToDay=<ToDay>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlToMonth=<ToMonth>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlToYear=<ToYear>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtPlanningApplicationAddress=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtPlanningApplicationRoadName=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlPlanningApplicationParish=0&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24ddlPlanningCategory=All&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtPageNumber=1&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtMaxPageNumber=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtCurrentLatitude=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtCurrentLongitude=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtCurrentZoom=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtLastSearch=&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24txtwebsiteaddress=%2Fcitizen%2FPlanning%2FPages%2F&txtShowMap=&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24PlaceHolderMain%24PlanningRegisterSearchForm%24btnPlanningApplicationSearchSubmit=Search";

my $requestDigest = uri_escape($1) if($Content=~m/<input[^>]*?name=\"__REQUESTDIGEST\"[^>]*?value=\"([^\"]*?)"[^>]*?>/is);
my ($ViewState,$ViewstateGenerator,$EventValidation,$Eventtarget);
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__ViewState\s*"\s*value="([^\"]*?)"\s*\/>/is)
{
	$ViewState=uri_escape($2);
}
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^\"]*?)"\s*\/>/is)
{
	$ViewstateGenerator=uri_escape($2);
}
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^\"]*?)"\s*\/>/is)
{
	$EventValidation=uri_escape($2);
}	
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTTARGET\s*"\s*value="([^\"]*?)"\s*\/>/is)
{
	$Eventtarget=uri_escape($2);
}	
$searchPagePostCont=~s/<VIEWSTATE>/$ViewState/igs;
$searchPagePostCont=~s/<VIEWSTATEGENERATOR>/$ViewstateGenerator/igs;
$searchPagePostCont=~s/<EVENTVALIDATION>/$EventValidation/igs;
$searchPagePostCont=~s/<EVENTTARGET>/$Eventtarget/igs;
$searchPagePostCont=~s/<REQUESTDIGEST>/$requestDigest/igs;

# open(DC,">Content.html");
# print DC $Content;
# close DC;
# exit;

$searchPagePostCont=~s/<FromDay>/$sDay/igs;
$searchPagePostCont=~s/<ToDay>/$eDay/igs;
$searchPagePostCont=~s/<FromMonth>/$sMonth/igs;
$searchPagePostCont=~s/<ToMonth>/$eMonth/igs;
$searchPagePostCont=~s/<FromYear>/$sYear/igs;
$searchPagePostCont=~s/<ToYear>/$eYear/igs;
	
$mech->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' );
$mech->add_header( "Host" => 'www.gov.je' );
$mech->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
$mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
$mech->add_header( "Origin" => 'https://www.gov.je' );
$mech->add_header( "Referer" => "$searchURL" );	

$mech->post('https://www.gov.je/citizen/Planning/Pages/planning.aspx', Content => "$searchPagePostCont");
my $SearchResultContent = $mech->content;
print $mech->status;

# open(DC,">SearchResultContent.html");
# print DC $SearchResultContent;
# close DC;
# exit;

my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Application, Address, Date_Application_Registered, Date_Application_validated, Applicant_name, Applicant_Address, Application_type, Proposal, Agent_Name, Agent_Telephone, Agent_Company_Name, Agent_Address, Application_Status, Date_Application_Received, TargetDec_dt, Easting, Northing, PAGE_URL, Document_Url, Source, Schedule_Date) values ';

my $MaxPage=2;
my $Page_Number=2;
NextPage:
while($SearchResultContent=~m/<h2[^>]*?>\s*<a[^>]*?href\s*=\s*(?:\\)?\"([^\"]*?)(?:\\)?\"[^>]*?>\s*([^<]*?)\s*<(?:\\)?\/a>\s*<(?:\\)?\/h2>/igs)
{
	my $Application_URL=$1;
	my $Application_Num=$2;
	
	
	
	$Application_URL=~s/\\\//\//gsi;
	$Application_URL=~s/\&amp;/\&/gsi;
	$Application_Num=~s/\\\//\//gsi;
	
	
	print "Application_Num==>$Application_Num\n";	
	print "Application_URL=>$Application_URL\n";
	
	$mech->get($Application_URL);
	my $App_content = $mech->content;		

	# open(DC,">App_content.html");
	# print DC $App_content;
	# close DC;
	# exit;
	
	my ($Application,$Address,$Agent_Name,$Agent_Telephone,$Agent_Address,$Applicant_Address,$Applicant_name,$Application_Status,$Application_type,$Date_Application_Received,$Date_Application_Registered,$Date_Application_validated,$Easting,$Northing,$Grid_reference,$Proposal,$TargetDec_dt,$Agent_Company_Name,$Doc_Url);
	
	$Application=&clean($1) if($App_content=~m/>\s*Planning\s*application\s*\:\s*([^<]*?)\s*</is);
	$Proposal=&clean($1) if($App_content=~m/>\s*Description\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);
	$Applicant_name=&clean($1) if($App_content=~m/>\s*Applicant\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);
	$Address = &clean($1) if($App_content=~m/>\s*Property\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);	
	$Agent_Name=&clean($1) if($App_content=~m/>\s*Agent\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);
	$Application_type=&clean($1) if($App_content=~m/>\s*Type\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);
	$Application_Status=&clean($1) if($App_content=~m/>\s*Status\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);
	$Date_Application_validated=&clean($1) if($App_content=~m/>\s*Validated\s*date\s*\:?\s*<\/dt>\s*<dd[^>]*?>\s*([\w\W]*?)\s*<\/dd>/is);
	
	$Doc_Url='https://www.gov.je'.$1 if($App_content=~m/<a[^>]*?href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*Plans\s*and\s*documents\s*</is);	
	
	
	
	$Applicant_name=~s/\'/\'\'/igs;	
	$Agent_Name=~s/\'/\'\'/igs;	
	$Address=~s/\'/\'\'/igs;	
	$Agent_Address=~s/\'/\'\'/igs;	
	$Applicant_Address=~s/\'/\'\'/igs;	
	
	my $COUNCIL_NAME = 'Jersey States';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	my $Source = "GCS001";	
	# my $Source = $Date_Range;	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Application\', \'$Address\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Applicant_name\', \'$Applicant_Address\', \'$Application_type\', \'$Proposal\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Agent_Company_Name\', \'$Agent_Address\', \'$Application_Status\', \'$Date_Application_Received\',  \'$TargetDec_dt\',  \'$Easting\',  \'$Northing\', \'$Application_URL\', \'$Doc_Url\', \'$Source_With_Time\',\'$Schedule_Date\'),";
	
	undef($Application); undef($Address); undef($Date_Application_Registered); undef($Date_Application_validated); undef($Applicant_name); undef($Applicant_Address); undef($Application_type); undef($Proposal); undef($Agent_Name); undef($Agent_Telephone); undef($Agent_Company_Name); undef($Agent_Address); undef($Application_Status); undef($Date_Application_Received); undef($TargetDec_dt); undef($Easting); undef($Northing); undef($Doc_Url);
	
}

if($SearchResultContent=~m/<a\s*id[^>]*?NextButtonBottom[^>]*?>[^<]*?<(?:\\)?\/a>/is)
{	
	my $nextPagePostCont = '{"URL":"https://www.gov.je//citizen/Planning/Pages/PlanningApplicationSearch.aspx","CommonParameters":"|05|<Page_number>|<Total_Page>|||","SearchParameters":"|1301||||0|All|All|<FromDay>|<FromMonth>|<FromYear>|<ToDay>|<ToMonth>|<ToYear>"}';
		
	$nextPagePostCont=~s/<Page_number>/$Page_Number/igs;
	$nextPagePostCont=~s/<Total_Page>/$MaxPage/igs;
	print "MaxPage: $MaxPage\n";
	
	$nextPagePostCont=~s/<FromDay>/$sDay/igs;
	$nextPagePostCont=~s/<ToDay>/$eDay/igs;
	$nextPagePostCont=~s/<FromMonth>/$sMonth/igs;
	$nextPagePostCont=~s/<ToMonth>/$eMonth/igs;
	$nextPagePostCont=~s/<FromYear>/$sYear/igs;
	$nextPagePostCont=~s/<ToYear>/$eYear/igs;
	

	
	$mech->add_header( "Accept" => 'text/plain, */*; q=0.01' );
	$mech->add_header( "Host" => 'www.gov.je' );
	$mech->add_header( "Content-Type" => 'application/json; charset=UTF-8' );
	$mech->add_header( "Accept-Language" => 'en-US,en;q=0.9' );
	$mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
	$mech->add_header( "Sec-Fetch-Mode" => 'cors' );
	$mech->add_header( "Sec-Fetch-Site" => 'same-origin' );
	$mech->add_header( "Origin" => 'https://www.gov.je' );
	$mech->add_header( "Referer" => 'https://www.gov.je/citizen/Planning/Pages/PlanningApplicationSearch.aspx' );	
	
	print "nextPagePostCont: $nextPagePostCont\n";

	$mech->post('https://www.gov.je/_layouts/15/PlanningAjaxServices/PlanningSearch.svc/Search', Content => "$nextPagePostCont");
	my $content = $mech->content;
	$SearchResultContent = $content;
	open(DC,">SearchResultContent$Page_Number.html");
	print DC $content;
	close DC;
	# exit;

	
	print "Page_Number: $Page_Number\n";
	
	$Page_Number++;
	$MaxPage++;
	
	
	goto NextPage;
}

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
# print "insert_query: $insert_query\n";


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}