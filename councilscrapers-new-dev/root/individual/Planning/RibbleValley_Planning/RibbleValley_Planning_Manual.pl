use WWW::Mechanize;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use URI::Escape;
use DBI;
use Win32; 
use DBD::ODBC;
use FileHandle;
use File::Copy;
use Time::Piece;

my $Council_Code = $ARGV[0];


if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"325\" \"GCS001\"\)", 16, "Error message");
    exit();
}


# Establish connection with DB server
my $dbh = &DbConnection();

# User Agent
my $mech = WWW::Mechanize->new(autocheck => 0);

my @Appl_Num = ("3/2018/0564","3/2018/0125","3/2018/0436","3/2018/0243","3/2018/0303","3/2018/0322","3/2018/0373");
				
&Scrape_Details(\@Appl_Num,$Council_Code,$pdf_url);	
		

sub Scrape_Details()
{
	my $AppNum=shift;
	my $Council_Code=shift;	
	my $page_url=shift;
	
	my @AppNums=@{$AppNum};
	
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';
	
	foreach my $ApplicationNo (@AppNums)
	{
		print "$ApplicationNo\n";
		my $tempNum = $ApplicationNo;
		$tempNum=~s/\//\%2F/gsi;
		my $simpleSearchURL = "https://www.ribblevalley.gov.uk/site/scripts/planx_details.php?appNumber=$tempNum";
		$mech->get($simpleSearchURL);
		sleep(5);
		my $App_Page_Content = $mech->content;
		my $Page_url = $mech->uri();
		
		# print "$Page_url\n"; <STDIN>;
		
		# open SR, ">$ApplicationNo.html";
		# print SR $App_Page_Content;
		# close SR;
		# exit;
		
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<br\s*\/?>\s*([\w\W]*?)\s*<\/p>\s*<div\s*class=\"download_box\">/is);	
		my $Application					= &clean($1) if($App_Page_Content=~m/<h1>Application\s*([^<]*?)\s*<\/h1>/is);			
		if($Application eq $ApplicationNo)
		{
			$Application;
		}
		else
		{
			$Application=$ApplicationNo;
		}
		my $Application_Type;
		my $Address						= &clean($1) if($App_Page_Content=~m/<td>\s*Development\s*address\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<td>\s*Applicant\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Applicant_Address			= &clean($1) if($App_Page_Content=~m/<td>\s*Applicant\s*<\/td>\s*<td>\s*<strong>\s*[^<]*?\s*<\/strong>\s*<br\s*\/?>\s*([^<]*?)\s*<\/td>/is);
		my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<td>\s*Agent\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Agent_Address				= &clean($1) if($App_Page_Content=~m/<td>\s*Agent\s*<\/td>\s*<td>\s*<strong>\s*[^<]*?\s*<\/strong>\s*<br\s*\/?>\s*([^<]*?)\s*<\/td>/is);
		my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<td>\s*Officer\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*<br\s*\/?>/is);
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/<td>\s*Planning\s*Status\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*/is);
		my $Date_Application_Received;
		my $Date_Application_Registered;
		my $Date_Application_validated;
		if($App_Page_Content=~m/<td>\s*Key\s*dates\s*<\/td>\s*<td>\s*([\w\W]*?)\s*<\/td>/is)
		{
			$Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<strong>\s*Received\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
			$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<strong>\s*Valid\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
			$Date_Application_Registered= &clean($1) if($App_Page_Content=~m/<strong>\s*Registered\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
		}
		my $Agent_Telephone;
		my $TargetDec_dt;
		my $Doc_Url=$Page_url;
		my $Application_Link=$Page_url;
		my $Agent_Email;
		
		$Proposal=~s/\'/\'\'/gsi;	
		$Address=~s/\'/\'\'/gsi;	
		$Applicant_name=~s/\'/\'\'/gsi;	
		$Agent_Name=~s/\'/\'\'/gsi;	
		$Agent_Address=~s/\'/\'\'/gsi;	
		$Applicant_Address=~s/\'/\'\'/gsi;	
		$Application_Status=~s/\'/\'\'/gsi;	
		my $Source = "GCS001";			
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		my $COUNCIL_NAME = "Ribble Valley";
		
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
		
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;		
		
	}
	
	$insert_query=~s/\,$//igs;
    print "insert_query::$insert_query\n";
	
	if($insert_query!~m/values\s*$/is)
	{
		&DB_Insert($dbh,$insert_query);
	}	
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\n+/ /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###### DB Connection ####
sub DbConnection()
{
	# my $dsn 						= 'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dsn 						= 'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}