use WWW::Mechanize;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use URI::Escape;
use DBI;
use Win32;
use DBD::ODBC;
use FileHandle;
use File::Copy;
use Time::Piece;
use Glenigan_DB_Windows;


my $Council_Code = $ARGV[0];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"325\" \"GCS001\"\)", 16, "Error message");
    exit();
}


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();

# User Agent
my $mech = WWW::Mechanize->new(autocheck => 0);

# Location    
my $pgm_path='C:/Glenigan/Live_Schedule/Planning/RibbleValley_Planning/Program';
$pgm_path=~s/\s+$//igs;
my $archive_path=$pgm_path;
$archive_path=~s/Program$/Archive/igs;

print "Archive Path :: $archive_path \n ";


opendir(DIR, $archive_path) or die $!;
my @folder_array;
while (my $file = readdir(DIR))
{
	next if ($file=~m/^\./);
	$file=~s/\.pdf\s*$//igs;
	push(@folder_array,$file);
	# print "$file\n";
}
print "folder_array :: @folder_array \n";
closedir(DIR);

my $URL = 'https://www.ribblevalley.gov.uk/weekly_lists';
$mech->get($URL);
my $list_content = $mech->content;

my $t = Time::Piece->new();
my $currentYear = $t->year;
print "currentYear=>$currentYear\n";

# open SR, ">ribblevalley.html";
# print SR $list_content;
# close SR;
# exit;
	
# if($list_content=~m/<p>\s*<a\s*title=\"Weekly\s*list\s*of\s*planning\s*applications\s*received\"\s*href=\"([^\"]*?)\"[^>]*?>[^<]*?<\/a>\s*<\/p>\s*<h3>\s*Weekly\s*list\s*of\s*decided\s*planning\s*applications\s*<\/h3>/is)
if($list_content=~m/<p>\s*<a\s*href=\"([^\"]*?)\"[^>]*?title=\"Weekly\s*list\s*of\s*planning\s*applications\s*received\"[^>]*?>[^<]*?<\/a>\s*<\/p>\s*<h3>\s*Weekly\s*list\s*of\s*decided\s*planning\s*applications\s*<\/h3>/is)
{
	my $pdfURL=$1;
	if($pdfURL!~m/^http/is)
	{
		$pdfURL="https://www.ribblevalley.gov.uk/".$pdfURL;
		# print "$pdfURL\n"; <STDIN>;
	}
	$mech->get($pdfURL);
	my $pdfList_content = $mech->content;

	# open SR, ">ribblevalley.html";
	# print SR $pdfList_content;
	# close SR;
	# exit;
	
	
	
	my $file_occur_flag=0;
	while($pdfList_content=~m/<li>\s*<h3>\s*<a[^>]*?href=\"([^\"]*?)\">\s*([^<]*?$currentYear)\s*<\/a>\s*<\/h3>/igs)
	{
		my $pdfPageurl=$1;
		my $file_name=$2;
		
		if($pdfPageurl!~m/^http/is)
		{
			$pdfPageurl="https://www.ribblevalley.gov.uk".$pdfPageurl;
			# print "$pdfPageurl\n"; <STDIN>;
		}
		
		$mech->get($pdfPageurl);
		my $pdfPagecontent = $mech->content;

		if($pdfPagecontent=~m/<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*Download\s*now\s*<\/a>/is)
		{	
			my $pdf_url=$1;
			if($pdf_url!~m/^http/is)
			{
				$pdf_url="https://www.ribblevalley.gov.uk/".$pdf_url;
				# print "$pdf_url\n"; <STDIN>;
			}
			
			# print "pdfURL=>$pdf_url\n";
			# print "file_name=>$file_name\n"; 
			# <STDIN>;
			
			$file_name=~s/\s+//igs;
			my $file_exist=0;		
			my @Match_file = grep(/$file_name/, @folder_array);
			if($Match_file[0] ne "")
			{
				my $file_exist=1;
				print "file_exist :: $file_exist \n";
				next;
			}
			else
			{
				$mech->get($pdf_url);
				my $pdf_content = $mech->content;			
				$file_occur_flag=1;
			
				my $temp_file = $pgm_path.'\\'.$file_name;
				$file_name=$pgm_path.'\\'.$file_name;
				
				my $fh = FileHandle->new("$temp_file.pdf", 'w') or die "Cannot open $file_name.pdf for write :$!";
				binmode($fh);
				$fh->print($pdf_content);
				$fh->close();
				
				my $copy_res=copy("$temp_file.pdf",$archive_path);
				print "Download URL :: $file_name.pdf \n";#<STDIN>;
				
				
				my $exe_name="C:/Glenigan/Live_Schedule/Planning/RibbleValley_Planning/pdftotext.exe -raw";
				system("$exe_name $temp_file.pdf");
				
				my $html_file_pdf=$file_name.'.pdf';
				$html_file_pdf=~s/\.pdf\s*$/\.txt/igs;
				
				print "html_file_pdf :: $html_file_pdf\n";
				
				
				undef $/;
				open KV, "$html_file_pdf";
				my $txt_cnt=<KV>;
				close KV;
				
				my @Appl_Num;
				while($txt_cnt=~m/^\s*(\d\/\d{4}\/\d{4})\s*$/mgsi)
				{
					my $num = $1;
					print "App=>$num\n";	
					push(@Appl_Num,$num);
				}
				
				&Scrape_Details(\@Appl_Num,$Council_Code,$pdf_url);	
			}
		}
	}
	if($file_occur_flag != 1)
	{
		print "No new file occur \n";
	}
}

sub Scrape_Details()
{
	my $AppNum=shift;
	my $Council_Code=shift;	
	my $page_url=shift;
	
	my @AppNums=@{$AppNum};
	
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	
	
	foreach my $ApplicationNo (@AppNums)
	{
		# print "$ApplicationNo\n";
		# my $simpleSearchURL = 'https://www.ribblevalley.gov.uk/planningApplication/search';
		
		my $tempNum = $ApplicationNo;
		
		
		$tempNum=~s/\//\%2F/gsi;
		my $simpleSearchURL = "https://www.ribblevalley.gov.uk/site/scripts/planx_details.php?appNumber=$tempNum";
		$mech->get($simpleSearchURL);
		
		sleep(5);
		# $mech->form_number(2); 
		# $mech->set_fields('appNumber' => $ApplicationNo );
		# $mech->click( $SRCH_BTN_NME );
		
		
		my $App_Page_Content = $mech->content;
		my $Page_url = $mech->uri();
		
		# print "$Page_url\n"; <STDIN>;
		
		# open SR, ">$ApplicationNo.html";
		# print SR $App_Page_Content;
		# close SR;
		# exit;
		
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<br\s*\/?>\s*([\w\W]*?)\s*<\/p>\s*<div\s*class=\"download_box\">/is);	
		my $Application					= &clean($1) if($App_Page_Content=~m/<h1>Application\s*([^<]*?)\s*<\/h1>/is);			
		if($Application eq $ApplicationNo)
		{
			$Application;
		}
		else
		{
			$Application=$ApplicationNo;
		}
		my $Application_Type;
		my $Address						= &clean($1) if($App_Page_Content=~m/<td>\s*Development\s*address\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<td>\s*Applicant\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Applicant_Address			= &clean($1) if($App_Page_Content=~m/<td>\s*Applicant\s*<\/td>\s*<td>\s*<strong>\s*[^<]*?\s*<\/strong>\s*<br\s*\/?>\s*([^<]*?)\s*<\/td>/is);
		my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<td>\s*Agent\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Agent_Address				= &clean($1) if($App_Page_Content=~m/<td>\s*Agent\s*<\/td>\s*<td>\s*<strong>\s*[^<]*?\s*<\/strong>\s*<br\s*\/?>\s*([^<]*?)\s*<\/td>/is);
		my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<td>\s*Officer\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*<br\s*\/?>/is);
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/<td>\s*Planning\s*Status\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*/is);
		my $Date_Application_Received;
		my $Date_Application_Registered;
		my $Date_Application_validated;
		if($App_Page_Content=~m/<td>\s*Key\s*dates\s*<\/td>\s*<td>\s*([\w\W]*?)\s*<\/td>/is)
		{
			$Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<strong>\s*Received\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
			$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<strong>\s*Valid\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
			$Date_Application_Registered= &clean($1) if($App_Page_Content=~m/<strong>\s*Registered\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
		}
		my $Agent_Telephone;
		my $TargetDec_dt;
		my $Doc_Url=$Page_url;
		my $Application_Link=$Page_url;
		my $Agent_Email;
		
		$Proposal=~s/\'/\'\'/gsi;	
		$Address=~s/\'/\'\'/gsi;	
		$Applicant_name=~s/\'/\'\'/gsi;	
		$Agent_Name=~s/\'/\'\'/gsi;	
		$Agent_Address=~s/\'/\'\'/gsi;	
		$Applicant_Address=~s/\'/\'\'/gsi;	
		$Application_Status=~s/\'/\'\'/gsi;	
		my $Source = "GCS001";			
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		my $COUNCIL_NAME = "Ribble Valley";
		
		print "ApplicationNo==>$Application\n";
		
		my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\')";
	
		$insert_query=~s/\,$//igs;
		print "insert_query::$insert_query\n";
		
		if($insert_query!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
		}	
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;		
		
	}
	
	
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\n+/ /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###### DB Connection ####
sub DbConnection_old()
{
	# my $dsn 						= 'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dsn 						= 'driver={SQL Server};Server=10.101.53.25;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("DBI:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	# my $dbh							=	DBI->connect("DBI:ODBC:$dsn") || die "Couldn't open database: $DBI::errstr\n";
	
	if(!$dbh)
	{
		&DBI->connect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;

}

sub DbConnection()
{
	# Test DB
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.183;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# Live DB
	my $dsn1 ='driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.184;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}