import requests
import os,sys,re
import json
import ast
from datetime import datetime, timedelta
import pymssql


#Main Url
mainUrl = 'https://www.ambervalley.gov.uk/planning/development-management/view-a-planning-application/'

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

proxies2 = {
  'http': 'http://172.27.137.199:3128',
  'https': 'http://172.27.137.199:3128',
}

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)
	

def proxiesGenerator(proxies1,proxies2):    
	i = 0
	while i <= 1:
		try:
			print "Main URL is: ", mainUrl
			res = requests.get(mainUrl, proxies=proxies1)			
			if res.status_code == 200:				
				return proxies1
				
		except Exception as ex:
			print "Error is: ", str(type(ex).__name__)
			
			if str(type(ex).__name__) == "ProxyError":
				while i <= 2:
					print  "Now trying in second Proxy", proxies2
					res = requests.get(mainUrl, proxies=proxies2)
					print "second proxy URL status is: ", res.status_code
					if res.status_code == 200:
						return proxies2
					else:
						i = i + 1
	
# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, CouncilCode, Source):
	try:	
	
		finProxy = proxiesGenerator(proxies1,proxies2)
		
		print "Final working proxy is: ", finProxy

		sessionByRequest = requests.session()
		req1 = sessionByRequest.get(mainUrl, proxies= finProxy)
		# print "Main URL status is: ", req1.status_code
		
		#Address Url
		addressUrl = 'https://info.ambervalley.gov.uk/WebServices/AVBCFeeds/DevConJSON.asmx/PlanAppsByAddressKeyword'
		req2Headers = {
						'Accept': 'application/json, text/javascript, */*; q=0.01',
						'Accept-Encoding': 'gzip, deflate, br',
						'Accept-Language': 'en-US,en;q=0.9',
						'Connection': 'keep-alive',
						'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
						'Host': 'info.ambervalley.gov.uk',
						'Origin': 'https://www.ambervalley.gov.uk',
						'Referer': 'https://www.ambervalley.gov.uk/planning/development-management/view-a-planning-application/',
						'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36}'
		}

		#Date as PostContent passing through URL
		datePostContent =  'keyWord=&fromDate='+dumFrom_Date+'&toDate='+dumTo_Date

		#Post method to parse the Url, datepostcontent, Headers to generate Output
		req2 = sessionByRequest.post(url= addressUrl, data= datePostContent, headers = req2Headers, proxies= finProxy )
		JsonContent = json.loads(req2.content)
		
		# with open("output.html", 'wb') as handle:
			# handle.write(str(JsonContent))
			

		refValues = []
		for i in range(0,len(JsonContent)):
			refValues.append(str(JsonContent[i]['refVal'].encode('ascii', 'ignore')))
		
		appCount = 0
		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Easting, Northing) values '
		bulkValuesForQuery=''	
		
		try:
			for r in range(0,len(refValues)):
				applicationUrl = 'https://info.ambervalley.gov.uk/WebServices/AVBCFeeds/DevConJSON.asmx/GetPlanAppDetails?refVal=' + str(
					refValues[r].replace('/', '%2F'))
				req = sessionByRequest.get(applicationUrl, proxies= finProxy)
				AppJsonContent = json.loads(req.content)
				
				application = clean(str(AppJsonContent['refVal']))
				location = clean(str(AppJsonContent['applicationAddress'].encode('ascii', 'ignore').replace('\r', ' ')))
				applicantName = clean(str(AppJsonContent['applicantName'])	)			
				applicantAddress = clean(str(AppJsonContent['applicantAddress']).replace('\r', ' '))
				agentName = clean(str(AppJsonContent['agentName']))
				agentAddress = clean(str(AppJsonContent['agentAddress']).replace('\r', ' '))
				dateRegistered = clean(str(AppJsonContent['dateRegistered'].replace('T', ' ')))
				targetdecdt = ''
				if str(AppJsonContent['revisedTargetDate'].replace('T', ' ')) == '0001-01-01 00:00:00':
					AppJsonContent['revisedTargetDate'] = ' '
					targetdecdt = clean(str(AppJsonContent['revisedTargetDate'].replace('T', ' ')))
				else:
					targetdecdt = clean(str(AppJsonContent['revisedTargetDate'].replace('T', ' ')))
				dateReceived = clean(str(AppJsonContent['dateReceived'].replace('T', ' ')))
				dateValid = clean(str(AppJsonContent['dateValid'].replace('T', ' ')))
				applicationStatus = clean(str(AppJsonContent['status']))
				northings = clean(str(AppJsonContent['northings']))
				eastings = clean(str(AppJsonContent['eastings']))
				proposal = clean(str(AppJsonContent['proposal']))
				documentUrl = clean(str(applicationUrl))
				# str(AppJsonContent['dateToBeDecided'].replace('T', ' '))
				dateDecision = ''
				if str(AppJsonContent['dateDecision'].replace('T', ' ')) == '0001-01-01 00:00:00':
					AppJsonContent['dateDecision'] = ''
					dateDecision = clean(str(AppJsonContent['dateDecision'].replace('T', ' ')))
				else:
					dateDecision = clean(str(AppJsonContent['dateDecision'].replace('T', ' ')))
					
				decision = ''	
				if str(AppJsonContent['decision'].replace('T', ' ')) == '0001-01-01 00:00:00':
					AppJsonContent['decision'] = ''
					decision = clean(str(AppJsonContent['decision'].replace('T', ' ')))
				else:
					decision = clean(str(AppJsonContent['decision'].replace('T', ' ')))
					
				Date_Decision_Dispatched = ''
				format1 = "%Y/%m/%d %H:%M"	
				Schedule_Date = datetime.now().strftime(format1)
			
				Source_With_Time = Source+"_"+Schedule_Date+"-perl"
				
				Council_Name = "Amber Valley";
				

				joinValues="("+"'"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(location)+"','"+str(dateReceived)+"','"+str(application)+"','"+str(dateRegistered)+"','"+str(dateValid)+"','"+str(proposal)+"','"+str(applicationStatus)+"','','"+str(agentAddress)+"','','"+str(agentName)+"','','"+str(applicantAddress)+"','"+str(applicantName)+"','','','','','','','','','','','','"+str(targetdecdt)+"','','"+str(applicationUrl)+"','"+str(documentUrl)+"','','"+Source_With_Time+"','"+str(Schedule_Date)+"','"+str(eastings)+"','"+str(northings)+"')"
		

				print "ApplicationValues: ", joinValues

				if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
				elif appCount == 900:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					appCount=0
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					appCount += 1	
					
				
			return bulkValuesForQuery	
				
		except Exception as ex:
			print "Application URL exception is: ", str(type(ex).__name__)
		
		

	except Exception as ex:
		print "Post method response is: ", str(type(ex).__name__)
		

# Clean function
def clean(cleanValue):
    try:
		clean='' 
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno		
				
	
# Main section	
if __name__== "__main__":	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()
	
	# GCS calculation
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "25",
	  "GCS003": "51",
	  "GCS004": "77",
	  "GCS005": "103",	
	  "GCS006": "129",
	  "GCS007": "155",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d-%m-%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=26)
	fromdate = preday.strftime(format)
	
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	
	# application extraction
	finalinsertQuery = inputsection(fromdate, todate, councilcode, gcs)
	
	# print "final: ", finalinsertQuery
	# with open("finalinsertQuery.txt", 'wb') as handle:
		# handle.write(str(finalinsertQuery))
	# raw_input()
	
	# final output query insertion
	if (finalinsertQuery != '') or finalinsertQuery is not None:
		cursor.execute(finalinsertQuery)
		conn.commit()