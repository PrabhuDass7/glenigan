# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Planning/Anglesey_Planning'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(fromdate, todate, driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		driver.maximize_window()
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		with open("456plan.html", 'wb') as FH:
			FH.write(content)
		advfinalCnt=''
		if len(content) > 0:
			advfinalCnt = content.encode("utf-8")

		
		# driver.find_element_by_xpath('//*[@id="32:148;a"]').send_keys('PApplication')
		# driver.find_element_by_xpath('//*[@id="32:152;a"]').send_keys('PApplication')
		driver.find_element_by_xpath('//*[@id="32:152;a"]').send_keys('Planning Applications')
		time.sleep(10)
		advcontent=driver.page_source
		
		fDate = re.findall(">\s*Date\s*Valid\s*From\s*(?:<[^>]*?>)?\s*<\/label>\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advcontent), re.IGNORECASE)  
		fromDate = '//*[@id="'+str(fDate[0])+'"]'
		

		tDate = re.findall(">\s*Date\s*Valid\s*To\s*(?:<[^>]*?>)?\s*<\/label>\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advcontent), re.IGNORECASE) 
		toDate = '//*[@id="'+str(tDate[0])+'"]'	
		
		driver.find_element_by_xpath(fromDate).send_keys(fromdate)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath(toDate).send_keys(todate)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="254:0"]/div/div/div/div/div/div/div/div[2]/button').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
				
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno



# To collect Application URLs here
def collectAppURL(content, home_url):	
	try:		 
		regionURLs=[]
		if re.findall(r'<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?data-aura-class=\"uiOutputURL\"[^>]*?>\s*[^>]*?\s*<\/a>\s*<\/td>', str(content), re.IGNORECASE):
			regexMatched=re.findall(r'<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?data-aura-class=\"uiOutputURL\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/td>', str(content), re.IGNORECASE)       
			
			for appDetails in regexMatched:
				appURL,appNum = appDetails
				if not re.findall(r'^http', str(appURL)):
					appURL = urljoin(home_url,appURL)
				appNumber = re.sub(r'\/', "", str(appNum))
				# print("appNumber::",appNumber)
				appURL=appURL+'/'+appNumber+'?language=en_GB'
				# print("AfterappURL::",appURL)
				regionURLs.append(appURL)
				
				
		else:
			print("ERROR!!!!!")
			
		return regionURLs	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 



# To collect Application URLs here
def collectAppDetails(fullAppURLs, home_url, driver, CouncilCode, Source, conn, cursor):	
	try:		 
		bulkValuesForQuery=''
		
		appCount = 0
		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date,Easting,Northing) values '
		
		for appURL in fullAppURLs:
			print("Now processing this URL::",appURL)			
			driver.get(appURL)
			time.sleep(10)  
			Appcontent=driver.page_source
			
			Application_Number=re.findall(r'>\s*Planning\s*Application\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
			DtAppReceived=re.findall(r'>\s*Received\s*Date\s*[^<]*?<\/span>\s*<div[^>]*?>[\w\W]*?Received[^>]*?<\/span>\s*<\/div>\s*<\/div><div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			DtAppValid=re.findall(r'>\s*Validation\s*Date\s*[^<]*?<\/span>\s*<div[^>]*?>[\w\W]*?valid[^>]*?<\/span>\s*<\/div>\s*<\/div><div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			new_applicant_name=re.findall(r'>\s*Applicant\s*Name\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			new_agent_name=re.findall(r'>\s*Agent\s*Name\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Application_Status=re.findall(r'>\s*Decision\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Address=re.findall(r'Site\s*Address[^<]*?<\/span>\s*<div[^>]*?>[\w\W]*?for\s*Site\s*Address<\/span>\s*<\/div>\s*<\/div><div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Proposal=re.findall(r'>\s*Proposal\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			# Easting=re.findall(r'>\s*Easting\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			# Northing=re.findall(r'>\s*Northing\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			docURL=re.findall(r'>\s*Documents\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>', str(Appcontent), re.IGNORECASE)
			
			(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url,east,north) = ('','','','','','','','','','','','','','','','','','')

			if len(Application_Number) > 0:
				Application=clean(Application_Number[0])
			

			if len(Proposal) > 0:
				ProposalCnt=clean(Proposal[0])

			if len(Address) > 0:
				AppAddress=clean(Address[0])
				
			if len(Application_Status) > 0:
				ApplicationStatus=clean(Application_Status[0])

			if len(new_applicant_name) > 0:
				ApplicantName=clean(new_applicant_name[0])
				
				
			if len(new_agent_name) > 0:
				AgentName=clean(new_agent_name[0])
				
			if len(docURL) > 0:
				Document_Url=clean(docURL[0])
			# print("Document_Url::",Document_Url)
				
			# if len(Easting) > 0:
				# east=clean(Easting[0])
				
			# if len(Northing) > 0:
				# north=clean(Northing[0])
			
			if len(DtAppReceived) > 0:
				DateApplicationReceived=DtAppReceived
				
				
			if len(DtAppValid) > 0:
				DateApplicationvalidated=DtAppValid
				
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			
			Council_Name = "Anglesey"
			joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived[0])+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated[0])+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"','"+str(east)+"','"+str(north)+"')"
			
			print "ApplicationValues: ", joinValues
			bulkValuesForQuery = insertQuery+joinValues
			cursor.execute(bulkValuesForQuery)
			conn.commit()
			bulkValuesForQuery=''

			# if appCount == 0:
				# bulkValuesForQuery = insertQuery+joinValues
				# appCount += 1
			# elif appCount == 5:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# cursor.execute(bulkValuesForQuery)
				# conn.commit()
				# appCount=0
				# bulkValuesForQuery=''
			# else:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# appCount += 1	
				
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 		

# nextPageSection parse section	
def nextPageSection(searchcontent, homeURL, driver):
	try:
		
		pageNumber = 2
		pageNum = 4
		fullQuery=''
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		AppURLs=[]
		if re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*'+str(pageNumber)+'\s*(?:<[^>]*?>)?\s*<\/button>', str(searchcontent)):
			fullURL = collectAppURL(str(searchcontent), homeURL)
			AppURLs.extend(fullURL)
						
			check_next_page = re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(searchcontent), re.I):					
					driver.find_element_by_xpath('//*[@id="PApplication"]/div[2]/div/button['+str(pageNum)+']').click()	
					#-- Wait funtion
					time.sleep(10)  
					appNextPagecontent=driver.page_source
					
					
					fullURL = collectAppURL(str(appNextPagecontent), homeURL)	
					AppURLs.extend(fullURL)
				
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(appNextPagecontent), re.I):
						check_next_page = re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(appNextPagecontent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			fullURL = collectAppURL(str(searchcontent), homeURL)		
			AppURLs.extend(fullURL)
			
		return AppURLs

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	# browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Live_Schedule/Jar/chromedriver.exe')
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
		
	print 'fromdate  :', fromdate
	print 'todate    :', todate
		
	
	homeURL = 'https://ioacc.force.com/s/pr-english?language=en_GB&tabset-8996a=11112'
	
	results = inputsection(fromdate, todate, browser, homeURL)

	fullAppURLs = nextPageSection(results, homeURL, browser)
	
	finalinsertQuery = collectAppDetails(fullAppURLs, homeURL, browser, councilcode, gcs, conn, cursor)
	# print type(finalinsertQuery)
	
	if (finalinsertQuery != ''):
		# print (finalinsertQuery)
		cursor.execute(finalinsertQuery)
		conn.commit()
	
	browser.quit()