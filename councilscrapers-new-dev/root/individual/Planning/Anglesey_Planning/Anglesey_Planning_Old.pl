use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use FileHandle;

# Establish connection with DB server
my $dbh = &DbConnection();


# Cookies decleration
my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);

# User Agent decleration
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect('7');


# Write Cookies
my $file=$0."_cookie.txt";
my $cookie=HTTP::Cookies->new(file=>$file,autosave=>1,);
$ua->cookie_jar($cookie);

# Add Proxy
BEGIN {
 $ENV{HTTPS_PROXY} = 'https://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

# Get Home Page Content
# my $spefication_url="https://www.anglesey.gov.uk/planning-and-waste/planning-control/view-list-of-current-and-previous-planning-applications/";# for 2018
my $spefication_url="https://www.anglesey.gov.uk/en/Residents/Planning-building-control-and-conservation/Planning/View-list-of-current-and-previous-planning-applications.aspx";
# my $spefication_url="http://www.anglesey.gov.uk/planning-and-waste/planning-control/view-list-of-current-and-previous-planning-applications/planning-applications-2017/"; # for 2017
my $content=&Getcontent($spefication_url,"GET","","");

# open(DIR, ">Home_Content.html");
# print DIR "$content\n";
# close(DIR); 
# exit;

# Get current year Planning report
# if($content=~m/<h2>Current\s*planning\s*applications<\/h2>\s*([\w\W]*?)\s*<\/div>\s*<\/div>\s*<\/div>\s*<\/div>/is)# for 2018
if($content=~m/<ul\s*class=\"download-resource\">\s*([\w\W]*?)\s*<\/ul>\s*<\/div>\s*<\/div>/is)
# if($content=~m/<h1>Planning\s*applications\s*2017<\/h1>\s*([\w\W]*?)\s*<\/div>\s*<\/div>\s*<\/div>\s*<\/div>/is) # for 2017
{
	my $sub_content=$1;
	# while($sub_content=~m/<h2>\s*<a\s*href\=\"\s*([^>]*?)\s*\">\s*([^>]*?)\s*<\/a>\s*<\/h2>/igs)# for 2018
	while($sub_content=~m/<span[^>]*?>\s*<a\s*href\=\"\s*([^\"]*?)\s*\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/span>/igs)
	# while($sub_content=~m/<h3>\s*<a\s*href\=\"\s*([^>]*?)\s*\">\s*([^>]*?)\s*<\/a>\s*<\/h3>/igs) # for 2017
	{
		my $url_a=$1;
		my $file_name=$2;
		$file_name=~s/[^A-z\d]+//isg;	
		my $pdm_name1=$file_name.'.pdf';		
		my $somedir="C:/Glenigan/Live_Schedule/Planning/Anglesey_Planning/export";
		
		opendir(DIR, $somedir) || die "can't opendir $somedir: $!";
		my @files = grep { (!/^\./) && -f "$somedir/$_" } 
		readdir(DIR);
		closedir DIR;
		
		next if($pdm_name1~~@files);	
	
		my $second_url="$url_a";
		
		# my $second_url="http://www.anglesey.gov.uk$url_a" if($url_a!~m/^http/is);  # for 2017
		
		my $second_url_content=&Getcontent($second_url,"GET","","");
		
		# open(DIR, ">second_url_content.html");
		# print DIR "$second_url_content\n";
		# close(DIR); 
		# exit;
		if($second_url_content=~m/<h3>\s*<a\s*href\=\"\s*([^>]*?)\s*\">\s*[^>]*?\s*<\/a>\s*<\/h3>/is)
		{
			my $pdf_url=$1;
			
			
			my $pdf_content =&Getcontent("$pdf_url","","","GET");
			print "\n$pdf_url\n";
				
			my $fh = FileHandle->new("C:/Glenigan/Live_Schedule/Planning/Anglesey_Planning/export/$file_name.pdf", 'w') or die "Cannot open $file_name.pdf for write :$!";
			binmode($fh);
			$fh->print($pdf_content);
			$fh->close();
			
			my $filename1 = "C:/Glenigan/Live_Schedule/Planning/Anglesey_Planning/export/$file_name.txt";
			system("C:/Glenigan/Live_Schedule/Planning/Anglesey_Planning/pdftotext.exe \-raw \"C:/Glenigan/Live_Schedule/Planning/Anglesey_Planning/export/$file_name.pdf\"");
			sleep(10);
			undef $/;
			
			open KV, "$filename1";
			my $text=<KV>;
			close KV;
			
			my $front="";
			if($text=~m/^\s*([^<]*?)\s*Rhif\s+Cais\s+/is)
			{
				$front=$1;
			}
			$text=~s/$front//gsi;
			$text=~s///gsi;
			
			open FH, ">$filename1";
			print FH $text;
			close FH;
			
			# Pass text content and council code to sub function
			&Scrape_Details($text,"456",$pdf_url);
				
		}			
	}	
}

sub Scrape_Details()
{
    my $html_content=shift;
    my $Council_Code=shift;  
    my $PDF_URL=shift;  
    my $time = Time::Piece->new;
    my $ScheduleDateTime= $time->strftime('%Y/%m/%d %H:%M');
    my $Source = "GCS001";  
    my $COUNCIL_NAME = "ISLE OF ANGLESEY";  
    my $Source_With_Time=$Source."_".$ScheduleDateTime."-perl";  
   		
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';

    my(@Applicationss,@Case_officerss,@Proposalss,@Applicant_namess,@Addressss);
	
    my($Application,$Case_officer,$Applicant_name,$Proposal,$Address,$Date_Application_Received,$Application_Type,$Date_Application_validated,$Date_Application_Registered,$Application_Status,$Agent_Name,$Applicant_Address,$Agent_Telephone,$TargetDec_dt,$Schedule_Date,$Document_Url,$PAGE_URL,$Agent_Email,$Doc_Url,$Agent_Address,$Application_Link); 
	
    while($html_content=~m/Rhif\s*Cais\s*[\w\W]*?\s*Application\s*No\:\s*([^\n]*?)\s*(?:\n|Cyf)/igs)
    {
        $Application=&clean($1);
		# print "Application=>$Application\n";
        push(@Applicationss,$Application);
    }
    while($html_content=~m/Swyddog\s*[\w\W]*?\s*Officer\:\s*([^\n]*?)\s*(?:\n|Ymgeisydd)/igs)
    {
        $Case_officer=&clean($1);
        push(@Case_officerss,$Case_officer);
    }
    while($html_content=~m/Ymgeisydd\s*[\w\W]*?\s*Applicant\s*\:\s*([^\n]*?)\s*(?:\n|Bwriad)/igs)
    {
        $Applicant_name=&clean($1);
        push(@Applicant_namess,$Applicant_name);
    }
    while($html_content=~m/Bwriad\s*[\w\W]*?\s*Proposal\s*\:\s*[^>]*?([\w\W]*?)\s*Lleoliad\s+\/\s+Location/gs)
    {
        $Proposal=&clean($1);
        push(@Proposalss,$Proposal);
    }
    while($html_content=~m/Lleoliad\s*[\w\W]*?\s*Location\:\s*([^>]*?)(?:Rhif|Diwedd)/igs)
    {
        $Address=&clean($1);
        push(@Addressss,$Address);
    }
	
	$Schedule_Date = $ScheduleDateTime;
	$Application_Link=$PDF_URL;
	
	if($Doc_Url eq "")
	{
		$Doc_Url=$Application_Link;		
	}
	
    for(my $i=0;$i<=$#Applicationss;$i++)
    {
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Addressss[$i]\', \'$Proposalss[$i]\', \'$Date_Application_Received\', \'$Application_Type\', \'$Applicationss[$i]\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_namess[$i]\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
			   
			   
		undef $Applicant_namess[$i]; undef $Agent_Name; undef $Applicationss[$i]; undef $Proposalss[$i]; undef $Application_Type; undef $Addressss[$i]; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
    }
    $insert_query=~s/\,$//igs;
    
    print "insert_query::$insert_query\n";

    if($insert_query!~m/values\s*$/is)
    {
        &DB_Insert($dbh,$insert_query);
    }	 
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}



###### DB Connection ####
sub DbConnection()
{
    my $dsn	=	'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
    my $dbh	=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
   
    if(!$dbh)
    {
        &DBIconnect($dsn);
    }
    else
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}

#####GET Content####
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;	
	my $Content_Disposition=$res->header("Content-Disposition");
	
	my $content;
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if($rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if($rerun_count<= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}	


# SUBROUTINE TO GET THE POST HTML CONTENT
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;	
	my $rerun_count=0;
	
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->decoded_content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}