import requests
import os,sys,re
from bs4 import BeautifulSoup
import ast
from datetime import datetime, timedelta
import pymssql


#Main Url
mainUrl = 'https://planning.southderbyshire.gov.uk/'

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

proxies2 = {
  'http': 'http://172.27.137.199:3128',
  'https': 'http://172.27.137.199:3128',
}

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)
	

def proxiesGenerator(proxies1,proxies2):    
	i = 0
	while i <= 1:
		try:
			print "Main URL is: ", mainUrl
			res = requests.get(mainUrl, proxies=proxies1)			
			if res.status_code == 200:				
				return proxies1
				
		except Exception as ex:
			print "Error is: ", str(type(ex).__name__)
			
			if str(type(ex).__name__) == "ProxyError":
				while i <= 2:
					print  "Now trying in second Proxy", proxies2
					res = requests.get(mainUrl, proxies=proxies2)
					print "second proxy URL status is: ", res.status_code
					if res.status_code == 200:
						return proxies2
					else:
						i = i + 1

def viewState(content):
    soup = BeautifulSoup (content, 'lxml')
    viewStateValue = soup.find ('input', {'id': '__VIEWSTATE'}).get ('value')
    return viewStateValue

def viewStateGenerator(content):
    soup = BeautifulSoup (content, 'lxml')
    viewStateGeneratorValue = soup.find ('input', {'id': '__VIEWSTATEGENERATOR'}).get ('value')
    return viewStateGeneratorValue

def eventValidation(content):
    soup = BeautifulSoup (content, 'lxml')
    eventValidationValue = soup.find ('input', {'id': '__EVENTVALIDATION'}).get ('value')
    return eventValidationValue
	
# daterange search section	
def inputsection(fromDay, fromMonth, fromYear, toDay, toMonth, toYear):
	try:	
	
		finProxy = proxiesGenerator(proxies1,proxies2)
		
		print "Final working proxy is: ", finProxy

		reqHeaders = {
						'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Encoding': 'gzip, deflate, br',
						'Accept-Language': 'en-US,en;q=0.9',
						'Connection': 'keep-alive',
						'Content-Type': 'text/html; charset=utf-8',
						'Host': 'planning.southderbyshire.gov.uk',
						'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
		}
		
		sessionByRequest = requests.session()
		req = sessionByRequest.get(mainUrl, headers = reqHeaders, proxies= finProxy)
		# print "Main URL status is: ", req1.status_code
		Content = req.content
		
		# with open("output.html", 'wb') as handle:
			# handle.write(str(Content))
			
		if req.status_code == 200:
			# For select Catagory here		
			req2Headers = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Connection': 'keep-alive',
							'Content-Type': 'application/x-www-form-urlencoded',
							'Referer': 'https://planning.southderbyshire.gov.uk/',
							'Host': 'planning.southderbyshire.gov.uk',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
			}
			
			fromDateContent=re.findall(r'<strong>\s*From\s*Date\s*\:?\s*<\/strong>\s*<table>\s*([\w\W]*?)\s*<\/table>', str(Content), re.IGNORECASE)
			toDateContent=re.findall(r'<strong>\s*To\s*Date\s*\:?\s*<\/strong>\s*<table>\s*([\w\W]*?)\s*<\/table>', str(Content), re.IGNORECASE)
			
			fromday=re.findall(r'FromDay\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(fromDateContent[0]), re.IGNORECASE)   
			frommonth=re.findall(r'FromMonth\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(fromDateContent[0]), re.IGNORECASE)   
			fromyear=re.findall(r'FromYear\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(fromDateContent[0]), re.IGNORECASE) 
			
			today=re.findall(r'ToDay\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(toDateContent[0]), re.IGNORECASE)   
			tomonth=re.findall(r'ToMonth\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(toDateContent[0]), re.IGNORECASE)   
			toyear=re.findall(r'ToYear\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(toDateContent[0]), re.IGNORECASE)   
			
			#Date as PostContent passing through URL
			catPostContent =  '__EVENTTARGET=ctl00%24Mainpage%24optDecision&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(Content).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (Content)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (Content)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromday[0]) +'&ctl00%24Mainpage%24FromMonth='+ str(frommonth[0]) +'&ctl00%24Mainpage%24FromYear='+ str(fromyear[0]) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(today[0]) +'&ctl00%24Mainpage%24ToMonth='+ str(tomonth[0]) +'&ctl00%24Mainpage%24ToYear='+ str(toyear[0]) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus='
			catPostContent = catPostContent.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')

			#Post method to select Catagory
			req1 = sessionByRequest.post(url= mainUrl, data= catPostContent, headers = req2Headers, proxies= finProxy )
			CatContent = req1.content
			
			datePostContent = '__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(CatContent).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (CatContent)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (CatContent)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromDay) +'&ctl00%24Mainpage%24FromMonth='+ str(fromMonth) +'&ctl00%24Mainpage%24FromYear='+ str(fromYear) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(toDay) +'&ctl00%24Mainpage%24ToMonth='+ str(toMonth) +'&ctl00%24Mainpage%24ToYear='+ str(toYear) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus=&ctl00%24Mainpage%24cmdSearch=Search'
			datePostContent = datePostContent.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
			# print(datePostContent)
			
			#Post method to select date range
			req2 = sessionByRequest.post(url= mainUrl, data= datePostContent, headers = req2Headers, proxies= finProxy )
			dateContent = req2.content
			
			# with open("dateContent.html", 'wb') as handle:
				# handle.write(str(dateContent))

		return dateContent, finProxy
		
	except Exception as ex:
		print "Post method response is: ", str(type(ex).__name__)


# To collect Application URLs here
def collectAppURL(content, CouncilCode, Source, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear, finProxy, ss):	
	try:		 
		bulkValuesForQuery=''
		if re.findall(r'<tr[^>]*?>\s*<td>\s*([^>]*?)\s*<\/td>', str(content), re.IGNORECASE):  
			AppNum=re.findall(r'<tr[^>]*?>\s*<td>\s*([^>]*?)\s*<\/td>', str(content), re.IGNORECASE)       
			# print(AppNum)
			
			insertQuery = 'insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values '
				
			appCount = 0		
			for appNumber in AppNum:
				print("appNumber::::", appNumber)
				applicationNumber = appNumber
				appButtonNum=re.findall(r'<tr[^>]*?>\s*<td>\s*'+ str(appNumber) +'\s*<\/td>[\w\W]*?<input\s*type=\"button\"\s*value=\"view\"\s*onclick=\"javascript\:__doPostBack\([^>]*?\&\#39\;(Select\$\d+)[^>]*?>', str(content), re.IGNORECASE)
				# print(appButtonNum[0])
				
				appPostContent = '__EVENTTARGET=ctl00%24Mainpage%24gridMain&__EVENTARGUMENT='+ str(appButtonNum[0]) +'&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(content).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (content)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (content)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromDay) +'&ctl00%24Mainpage%24FromMonth='+ str(fromMonth) +'&ctl00%24Mainpage%24FromYear='+ str(fromYear) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(toDay) +'&ctl00%24Mainpage%24ToMonth='+ str(toMonth) +'&ctl00%24Mainpage%24ToYear='+ str(toYear) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus='
				appPostContent = appPostContent.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
								
				req3Headers = {
						'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Encoding': 'gzip, deflate, br',
						'Accept-Language': 'en-US,en;q=0.9',
						'Connection': 'keep-alive',
						'Content-Type': 'application/x-www-form-urlencoded',
						'Referer': 'https://planning.southderbyshire.gov.uk/',
						'Host': 'planning.southderbyshire.gov.uk',
						'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
				}
				
				#App Page Post method
				req4 = ss.post(url= mainUrl, data= appPostContent, headers = req3Headers, proxies= finProxy )
				appURL1 = req4.url
				print("appURL1==>",appURL1)
				
				if re.search(r'^https?\:\/\/southderbyshirepr\.force\.com\/', str(appURL1), re.IGNORECASE):
					req4Headers = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Host': 'southderbyshirepr.force.com',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
					}
					
					req5 = requests.get(appURL1, headers = req4Headers, proxies=finProxy)
					appURL = req5.url
					Appcontent1 = req5.content
					print("appURL==>",appURL)
					# with open("Appcontent.html", 'wb') as handle:
						# handle.write(str(Appcontent1))
					
					req5Headers = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Content-Type': 'text/html;charset=UTF-8',
							'Host': 'southderbyshirepr.force.com',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
					}
					req6 = requests.get(appURL, headers = req5Headers, proxies=finProxy)
					Appcon = req6.content
					# with open("Appcon.html", 'wb') as handle:
						# handle.write(str(Appcon))
					
					getURL=re.findall(r'<script\s*src=\"([^\"]*?)\">\s*<\/script>\s*<\/body>', str(Appcon), re.IGNORECASE)   
					
					req6Headers = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Content-Type': 'text/html;charset=UTF-8',
							'Host': 'southderbyshirepr.force.com',
							'Referer': appURL,
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
					}
					
					formURL = "https://southderbyshirepr.force.com" + str(getURL[0])
					# print("formURL==>",formURL)
					
					req6 = requests.get(formURL, headers = req6Headers, proxies=finProxy)
					formAppcontent = req6.content
					
					fwuid=re.findall(r'\,\"fwuid\"\:\"([^\"]*?)\"\,', str(formAppcontent), re.IGNORECASE)
					communityAppid=re.findall(r'\"APPLICATION\@markup\:\/\/siteforce\:communityApp\"\:\"([^\"]*?)\"', str(formAppcontent), re.IGNORECASE)
					Appid=re.findall(r'^[^>]*?(\/s\/planning-application\/[^>]*?)$', str(appURL), re.IGNORECASE)
					print("fwuid==>",fwuid)
					
					
					appPostContent1 = 'message=%7B%22actions%22%3A%5B%7B%22id%22%3A%2228%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.navigationMenu.NavigationMenuDataProviderController%2FACTION%24getNavigationMenu%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3AnavigationMenuBase%22%2C%22params%22%3A%7B%22navigationLinkSetIdOrName%22%3A%22%22%2C%22includeImageUrl%22%3Afalse%2C%22addHomeMenuItem%22%3Atrue%2C%22menuItemTypesToSkip%22%3A%5B%5D%7D%2C%22version%22%3A%2247.0%22%2C%22storable%22%3Atrue%7D%2C%7B%22id%22%3A%2233%3Ba%22%2C%22descriptor%22%3A%22serviceComponent%3A%2F%2Fui.communities.components.aura.components.forceCommunity.languagePicker.LanguagePickerController%2FACTION%24getInitData%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2FforceCommunity%3AlanguageSelector43%22%2C%22params%22%3A%7B%7D%2C%22version%22%3A%2247.0%22%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22'+ str(fwuid[0]) +'%22%2C%22app%22%3A%22siteforce%3AcommunityApp%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fsiteforce%3AcommunityApp%22%3A%22'+ str(communityAppid[0]) +'%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI='+ str(Appid[0]) +'&aura.token=undefined'
					appPostContent1 = appPostContent1.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
					print(appPostContent1)
					
					req7Headers = {
							'Accept': '*/*',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
							'Host': 'southderbyshirepr.force.com',
							'Referer': appURL,
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
					}
					
					#App Page Post method
					req7 = ss.post(url= 'https://southderbyshirepr.force.com/s/sfsites/aura?r=1&ui-communities-components-aura-components-forceCommunity-languagePicker.LanguagePicker.getInitData=1&ui-communities-components-aura-components-forceCommunity-navigationMenu.NavigationMenuDataProvider.getNavigationMenu=1', data= appPostContent1, headers = req7Headers, proxies= finProxy )
					Appcontent7 = req7.content
					with open("Appcontent7.html", 'wb') as handle:
						handle.write(str(Appcontent7))
					
					raw_input()
					
					Application_Number=re.findall(r'>\s*Application\s*Reference\s*<\/p>\s*<h1[^>]*?>\s*([^<]*?)\s*<\/h1>', str(Appcontent), re.IGNORECASE)
					Date_Decision_Made=re.findall(r'>\s*latest\sdecision\sdate\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*latest\s*date[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Application_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Decision_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Proposal=re.findall(r'>\s*Proposal\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Proposal[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					
					(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')
					
					if len(Application_Number) > 0:
						Application=clean(Application_Number[0])
					else:
						Application = appNumber

					if len(Proposal) > 0:
						ProposalCnt=clean(Proposal[0])

					if len(Application_Status) > 0:
						ApplicationStatus=clean(Application_Status[0])

					if len(Decision_Status) > 0:
						DecisionStatus=clean(Decision_Status[0])

					if len(Date_Decision_Made) > 0:
						DateDecisionMade=clean(Date_Decision_Made[0])

					format1 = "%Y/%m/%d %H:%M"					
					Schedule_Date = datetime.now().strftime(format1)

					Source_With_Time = Source+"_"+Schedule_Date+"-perl"
					
					Council_Name = "South Derbyshire"
					joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
					
					
					if appCount == 0:
						bulkValuesForQuery = insertQuery+joinValues
						appCount += 1
					elif appCount == 5:
						bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						cursor.execute(bulkValuesForQuery)
						conn.commit()
						appCount=0
						bulkValuesForQuery=''
					else:
						bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						appCount += 1	
				
				else:
					req4Headers = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Host': 'planning.southderbyshire.gov.uk',
							'Referer': 'https://planning.southderbyshire.gov.uk/Applications.aspx',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'
					}
					
					req5 = requests.get(appURL1, headers = req4Headers, proxies=finProxy)
					appURL = req5.url
					Appcontent = req5.content
					# print("appURL==>",appURL)
					# with open("Appcontent.html", 'wb') as handle:
						# handle.write(str(Appcontent1))
					# raw_input()
					
					Application_Number=re.findall(r'Application\s*Reference\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Date_Decision_Made=re.findall(r'Date\s*of\s*Decision\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					# Application_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Decision_Status=re.findall(r'Decision\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Proposal=re.findall(r'Proposed\s*Development\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([\w\W]*?)\s*(?:<[^>]*?>)?\s*<\/td>\s*<\/tr>', str(Appcontent), re.IGNORECASE)
					
					(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')
					
					if len(Application_Number) > 0:
						Application=clean(Application_Number[0])
					else:
						Application = appNumber

					if len(Proposal) > 0:
						ProposalCnt=clean(Proposal[0])


					if len(Decision_Status) > 0:
						DecisionStatus=clean(Decision_Status[0])

					if len(Date_Decision_Made) > 0:
						DateDecisionMade=clean(Date_Decision_Made[0])

					format1 = "%Y/%m/%d %H:%M"					
					Schedule_Date = datetime.now().strftime(format1)

					Source_With_Time = Source+"_"+Schedule_Date+"-perl"
					
					Council_Name = "South Derbyshire"
					joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
					
					
					if appCount == 0:
						bulkValuesForQuery = insertQuery+joinValues
						appCount += 1
					elif appCount == 5:
						bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						cursor.execute(bulkValuesForQuery)
						conn.commit()
						appCount=0
						bulkValuesForQuery=''
					else:
						bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						appCount += 1	
				
		else:
			print("ERROR!!!!!")
			
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# nextPageSection parse section	
def nextPageSection(searchcontent, CouncilCode, Source, Proxy, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear):
	try:
		
		pageNumber = 2
		pageNum = 2
		fullQuery=''
		
		sessionRequest = requests.session()
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent)):
			fullQuery = collectAppURL(str(searchcontent), CouncilCode, Source, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear, Proxy, sessionRequest)
			cursor.execute(fullQuery)
			conn.commit()
			
			check_next_page = re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?(Page\$'+str(pageNumber)+')[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?(Page\$'+str(pageNumber)+')[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent), re.I):					
					eventArgument=check_next_page[0].replace ('$', '%24')
					nextPagePostCnt = '__EVENTTARGET=ctl00%24Mainpage%24gridMain&__EVENTARGUMENT='+ str(eventArgument) +'&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(searchcontent).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (searchcontent)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (searchcontent)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromDay) +'&ctl00%24Mainpage%24FromMonth='+ str(fromMonth) +'&ctl00%24Mainpage%24FromYear='+ str(fromYear) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(toDay) +'&ctl00%24Mainpage%24ToMonth='+ str(toMonth) +'&ctl00%24Mainpage%24ToYear='+ str(toYear) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus='
					nextPagePostCnt = nextPagePostCnt.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
					
					reqHeaders = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Connection': 'keep-alive',
							'Content-Type': 'application/x-www-form-urlencoded',
							'Referer': 'https://planning.southderbyshire.gov.uk/',
							'Host': 'planning.southderbyshire.gov.uk',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
					}
			
					# Next page Post method
					req3 = sessionRequest.post(url= mainUrl, data= nextPagePostCnt, headers = reqHeaders, proxies= Proxy )
					nextPageContent = req3.content
			
					with open("nextPageContent.html", 'wb') as handle:
						handle.write(str(nextPageContent))
			
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(nextPageContent), re.I):
						check_next_page = re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(nextPageContent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			print("Hello Pari!!")		
			fullQuery = collectAppURL(str(searchcontent), CouncilCode, Source, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear, Proxy, sessionRequest)
			cursor.execute(fullQuery)
			conn.commit()
			
		
		return fullQuery

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno		

# Clean function
def clean(cleanValue):
    try:
		clean='' 
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno		
				
	
# Main section	
if __name__== "__main__":	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()
	
	# GCS calculation
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]	
	todayCalender = datetime.now()

	sdayCalender = todayCalender - timedelta(days=int(gcsdate))
	const_CalenderMonth = sdayCalender.strftime("%b")
	const_CalenderYear = sdayCalender.strftime("%Y")

	currentMonth = sdayCalender.strftime("%b")
	currentYear = sdayCalender.strftime("%Y")
	currentDay = sdayCalender.strftime("%d")
	
	preday = sdayCalender - timedelta(days=6)
	
	fromMonth = preday.strftime("%b")
	fromYear = preday.strftime("%Y")
	fromDay = preday.strftime("%d")

	print fromDay, fromMonth, fromYear
	print currentDay, currentMonth, currentYear
	
	searchCnt, proxy  = inputsection(fromDay, fromMonth, fromYear, currentDay, currentMonth, currentYear)
	
	finalinsertQuery = nextPageSection(searchCnt, councilcode, gcs, proxy, conn, cursor,fromDay, fromMonth, fromYear, currentDay, currentMonth, currentYear )
	
	# if (finalinsertQuery != ''):
		# print (finalinsertQuery)
		# cursor.execute(finalinsertQuery)
		# conn.commit()