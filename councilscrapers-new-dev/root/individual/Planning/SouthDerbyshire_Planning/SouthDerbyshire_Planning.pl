use strict;
use WWW::Mechanize;
use WWW::Mechanize::Firefox;
use HTTP::Cookies;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use HTTP::Cookies;
use LWP::UserAgent;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection1();

my $Council_Code = $ARGV[0];
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];
my $Date_Range = $ARGV[1];


my $ua = LWP::UserAgent->new( ssl_opts =>{ verify_hostname => 0 },keep_alive => 1,show_progress => 1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0");
$ua->timeout(30); 
$ua->max_redirect();

my $cookie = HTTP::Cookies->new(); 
$ua->cookie_jar(); 


if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"361\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

# system('"C:/Program Files/Mozilla Firefox/firefox.exe"');
# my $mech1 = WWW::Mechanize::Firefox->new(autocheck => 0);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";
if($From_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$sDay = $1;
	$sMonth = $month{$2};
	$sYear = $3;
	print "sDay==>$sDay \n";
	print "sMonth==>$sMonth \n";
	print "sYear==>$sYear \n";
}
if($To_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$eDay = $1;
	$eMonth = $month{$2};
	$eYear = $3;
	print "eDay==>$eDay \n";
	print "eMonth==>$eMonth \n";
	print "eYear==>$eYear \n";
}



#### UserAgent Declaration ####	
my $mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});



### Proxy settings ###
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');
				
my $searchURL = "https://planning.southderbyshire.gov.uk/";
$mech->get($searchURL);
my $Content = $mech->content;
my $status = $mech->status;


my ($ViewState,$ViewstateGenerator,$EventValidation,$Eventtarget);
my $searchPagePostCont = "__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optValid&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay=<FDAY>&ctl00%24Mainpage%24FromMonth=<FMON>&ctl00%24Mainpage%24FromYear=<FYEAR>&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay=<TDAY>&ctl00%24Mainpage%24ToMonth=<TMON>&ctl00%24Mainpage%24ToYear=<TYEAR>&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus=&ctl00%24Mainpage%24cmdSearch=Search";

# open(DC,">Content.html");
# print DC $Content;
# close DC;
# exit;

if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewState=uri_escape($2);
}
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewstateGenerator=uri_escape($2);
}
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$EventValidation=uri_escape($2);
}	
if($Content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTTARGET\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$Eventtarget=uri_escape($2);
}	
$searchPagePostCont=~s/<VIEWSTATE>/$ViewState/igs;
$searchPagePostCont=~s/<VIEWSTATEGENERATOR>/$ViewstateGenerator/igs;
$searchPagePostCont=~s/<EVENTVALIDATION>/$EventValidation/igs;
$searchPagePostCont=~s/<EVENTTARGET>/$Eventtarget/igs;
$searchPagePostCont=~s/<FDAY>/$sDay/igs;
$searchPagePostCont=~s/<TDAY>/$eDay/igs;
$searchPagePostCont=~s/<FMON>/$sMonth/igs;
$searchPagePostCont=~s/<TMON>/$eMonth/igs;
$searchPagePostCont=~s/<FYEAR>/$sYear/igs;
$searchPagePostCont=~s/<TYEAR>/$eYear/igs;
	
my $SearchResultContent;
$mech->post($searchURL, Content => "$searchPagePostCont");
my $ResultContent = $mech->content;
$SearchResultContent = $ResultContent;
print $mech->status."\n";

# open(DC,">SearchResultContent.html");
# print DC $SearchResultContent;
# close DC;
# exit;

my $Page_Number=2;
NextPage:
my ($viewState1,$viewstateGenerator1,$eventValidation1);
if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$viewState1=uri_escape($2);
}
if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$viewstateGenerator1=uri_escape($2);
}
if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$eventValidation1=uri_escape($2);
}
while($SearchResultContent=~m/<tr[^>]*?>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/igs)
{
	my $Application_Num=$1;
	print "Application_Num==>$Application_Num\n";
	
	my $Application_URL;
	if($Application_Num=~m/^\d+/is)
	{
		$Application_URL = "\/ApplicationDetail\.aspx\?Ref=".$Application_Num;
	}
	else
	{
		my $eventTarget1=uri_escape($1) if($SearchResultContent=~m/<tr[^>]*?>\s*<td[^>]*?>\s*(?:<[^>]*?>)?\s*$Application_Num\s*(?:<[^>]*?>)?\s*<\/td>[\w\W]*?<input\s*type=\"button\"\s*value=\"view\"\s*onclick=\"javascript\:__doPostBack\(\&\#39\;([^<]*?)\&\#39\;\,\&\#39\;[^<]*?\&\#39\;\)\"\s*\/>/is);
		my $eventArgument1=uri_escape($1) if($SearchResultContent=~m/<tr[^>]*?>\s*<td[^>]*?>\s*(?:<[^>]*?>)?\s*$Application_Num\s*(?:<[^>]*?>)?\s*<\/td>[\w\W]*?<input\s*type=\"button\"\s*value=\"view\"\s*onclick=\"javascript\:__doPostBack\(\&\#39\;[^<]*?\&\#39\;\,\&\#39\;([^<]*?)\&\#39\;\)\"\s*\/>/is);
		
		my $postCnt = "__EVENTTARGET=<EVENTTARGET>&__EVENTARGUMENT=<EVENTARGUMENT>&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optValid&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay=<FDAY>&ctl00%24Mainpage%24FromMonth=<FMON>&ctl00%24Mainpage%24FromYear=<FYEAR>&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay=<TDAY>&ctl00%24Mainpage%24ToMonth=<TMON>&ctl00%24Mainpage%24ToYear=<TYEAR>&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus=";
		
		# print "eventArgument1==>$eventArgument1\n";
		$postCnt=~s/<VIEWSTATE>/$viewState1/igs;
		$postCnt=~s/<VIEWSTATEGENERATOR>/$viewstateGenerator1/igs;
		$postCnt=~s/<EVENTVALIDATION>/$eventValidation1/igs;
		$postCnt=~s/<EVENTTARGET>/$eventTarget1/igs;
		$postCnt=~s/<EVENTARGUMENT>/$eventArgument1/igs;
		$postCnt=~s/<FDAY>/$sDay/igs;
		$postCnt=~s/<TDAY>/$eDay/igs;
		$postCnt=~s/<FMON>/$sMonth/igs;
		$postCnt=~s/<TMON>/$eMonth/igs;
		$postCnt=~s/<FYEAR>/$sYear/igs;
		$postCnt=~s/<TYEAR>/$eYear/igs;
		# print "$postCnt\n"; <STDIN>;

		$mech->add_header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		$mech->add_header("Accept-Encoding"=> "gzip, deflate, br");
		$mech->add_header("Accept-Language"=> "en-US,en;q=0.5");
		$mech->add_header("Content-Type"=> "application/x-www-form-urlencoded");
		$mech->add_header("Host"=> "planning.southderbyshire.gov.uk");
		$mech->add_header("Referer"=> "https://planning.southderbyshire.gov.uk/Applications.aspx");
		
		$mech->max_redirect(0);
		$mech->post($searchURL, Content => "$postCnt");
		my $pContent = $mech->content;	
	
		$Application_URL = $1 if($pContent=~m/<a\s*href=\"([^\"]*?)\">here<\/a>/is);
		print "Application_URL==>$Application_URL\n";
		$Application_Num=~s/\///gsi;
		$Application_Num= lc $Application_Num;	
	}
	
	my $App_content;
	my $Chrome_Status;
	if($Application_URL=~m/^\/ApplicationDetail\.aspx\?Ref/is)
	{
		$Application_URL = URI::URL->new($Application_URL)->abs( 'https://planning.southderbyshire.gov.uk/Applications.aspx', 1 );
		$mech->get($Application_URL);
		$App_content = $mech->content;	
	}
	else
	{
		$Application_URL = $Application_URL."/".$Application_Num;
		
		# $mech1->get($Application_URL);
		# sleep(10);
		# $App_content = $mech1->content;
		
		my $ARGV1='c:\Anaconda2\python.exe GoogleSearch.py';
		my $ARGV2="$Date_Range'|'$Application_URL";
		$Chrome_Status=qx($ARGV1 $ARGV2);
	}
	
	$Chrome_Status=clean($Chrome_Status);
	print "Chrome_Status:: $Chrome_Status ** \n";
	if ($Chrome_Status ne "YES")
	{
		goto nexts;
	}
	
	print "Application_URL==>$Application_URL\n";
	
	open F1, "<Detail_Page_$Date_Range.html";
	my @ccontent=<F1>;
	$App_content="@ccontent";
	close F1;
	
	# open F1, ">southderby-plan.html";
	# print F1 $App_content;
	# close F1;
	# exit;
	my ($Application,$Address,$Agent_Name,$Agent_Telephone,$Agent_Address,$Applicant_Address,$Applicant_name,$Application_Status,$Application_type,$Date_Application_Received,$Date_Application_Registered,$Date_Application_validated,$Easting,$Northing,$Grid_reference,$Proposal,$TargetDec_dt,$Agent_Company_Name,$Doc_Url);
	
	if($Application_URL=~m/^https?\:\/\/southderbyshirepr\.force\.com\//is)
	{
		$Application=&clean($1) if($App_content=~m/>\s*Application\s*Reference\s*<\/p>\s*<h1[^>]*?>\s*([^<]*?)\s*<\/h1>/is);
		$Address=&clean($1) if($App_content=~m/>\s*Site\s*Location\s*<\/span>\s*<div[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/div>\s*<\/div>\s*<\/div>/is);
		$Application_Status=&clean($1) if($App_content=~m/<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		$Date_Application_Received=&clean($1) if($App_content=~m/>\s*Received\s*Date\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Date\s*that\s*the\s*application\s*was[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>/is);
		$Easting=&clean($1) if($App_content=~m/>\s*Easting\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>/is);
		$Northing=&clean($1) if($App_content=~m/>\s*Northing\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>/is);
			
		$Doc_Url=$1 if($App_content=~m/<a[^>]*?title=\"Documents\"[^>]*?href=\"([^\"]*?)\"[^>]*?>/is);
		$Doc_Url=$Application_URL.$Doc_Url if($Doc_Url!~m/^http/is);
		if($App_content=~m/>\s*Valid\s*Date\s*<[\w\W]*?>\s*<[^>]*?class=\"uiOutputDate\"[^>]*?>([\w\W]*?)<\/div>/is)
		{
			$Date_Application_validated=&clean($1);
		}
		elsif($App_content=~m/>\s*Valid\s*Date\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Date\s*that\s*the\s*application\s*was[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>/is)
		{
			$Date_Application_validated=&clean($1);
		}
		
		if($App_content=~m/>\s*Proposal\s*<[\w\W]*?>\s*<[^>]*?class=\"uiOutputTextArea\"[^>]*?>([\w\W]*?)<\/div>/is)
		{
			$Proposal=&clean($1);
		}
		elsif($App_content=~m/>\s*Proposed\s*Development\s*<[\w\W]*?>\s*<[^>]*?class=\"uiOutputText\"[^>]*?>([\w\W]*?)<\/div>/is)
		{
			$Proposal=&clean($1);
		}
		elsif($App_content=~m/>\s*Proposal\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Proposal[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>/is)
		{
			$Proposal=&clean($1);
		}
	}
	else
	{
		$Application=&clean($1) if($App_content=~m/Application\s*Reference\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is);
		$Address=&clean($1) if($App_content=~m/Site\s*Location\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is);
		$Date_Application_Received=&clean($1) if($App_content=~m/Date\s*Received\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is);
		$Easting=&clean($1) if($App_content=~m/Easting\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is);
		$Northing=&clean($1) if($App_content=~m/Northing\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is);		
		$TargetDec_dt=&clean($1) if($App_content=~m/Target\s*Date\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is);
		if($App_content=~m/>\s*Proposed\s*Development\s*<[\w\W]*?>\s*<[^>]*?class=\"uiOutputText\"[^>]*?>([\w\W]*?)<\/div>/is)
		{
			$Proposal=&clean($1);
		}
		elsif($App_content=~m/>\s*Proposal\s*<[\w\W]*?>\s*<[^>]*?class=\"uiOutputTextArea\"[^>]*?>([\w\W]*?)<\/div>/is)
		{
			$Proposal=&clean($1);
		}
		elsif($App_content=~m/Proposed\s*Development\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([\w\W]*?)\s*(?:<[^>]*?>)?\s*<\/td>\s*<\/tr>/is)
		{
			$Proposal=&clean($1);
		}
		if($App_content=~m/>\s*Valid\s*Date\s*<[\w\W]*?>\s*<[^>]*?class=\"uiOutputDate\"[^>]*?>([\w\W]*?)<\/div>/is)
		{
			$Date_Application_validated=&clean($1);
		}
		elsif($App_content=~m/Date\s*Valid\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>/is)
		{
			$Date_Application_validated=&clean($1);
		}
		
		$Doc_Url=$searchURL;
	}
	
	$Applicant_name=~s/\'/\'\'/igs;	
	$Agent_Name=~s/\'/\'\'/igs;	
	$Address=~s/\'/\'\'/igs;	
	$Agent_Address=~s/\'/\'\'/igs;	
	$Applicant_Address=~s/\'/\'\'/igs;	
	my $COUNCIL_NAME = 'South Derbyshire';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	# my $Source = "GCS001";	
	my $Source = $Date_Range;	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	next if($Application=~m/^\s*$/is);
	my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Application, Address, Date_Application_Registered, Date_Application_validated, Applicant_name, Applicant_Address, Application_type, Proposal, Agent_Name, Agent_Telephone, Agent_Company_Name, Agent_Address, Application_Status, Date_Application_Received, TargetDec_dt, Easting, Northing, PAGE_URL, Document_Url, Source, Schedule_Date) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Application\', \'$Address\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Applicant_name\', \'$Applicant_Address\', \'$Application_type\', \'$Proposal\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Agent_Company_Name\', \'$Agent_Address\', \'$Application_Status\', \'$Date_Application_Received\',  \'$TargetDec_dt\',  \'$Easting\',  \'$Northing\', \'$Application_URL\', \'$Doc_Url\', \'$Source_With_Time\',\'$Schedule_Date\')";
	
	# print "$insert_query\n";
	&DB_Insert($dbh,$insert_query);
	
	nexts:
	
	undef($Application); undef($Address); undef($Date_Application_Registered); undef($Date_Application_validated); undef($Applicant_name); undef($Applicant_Address); undef($Application_type); undef($Proposal); undef($Agent_Name); undef($Agent_Telephone); undef($Agent_Company_Name); undef($Agent_Address); undef($Application_Status); undef($Date_Application_Received); undef($TargetDec_dt); undef($Easting); undef($Northing); undef($Doc_Url);
	
}

if($SearchResultContent=~m/<a\s*href[^>]*?Page[^>]*?>\s*(?:<[^>]*?>)?\s*$Page_Number\s*(?:<[^>]*?>)?\s*<\/a>/is)
{	
	my ($ViewState2,$ViewstateGenerator2,$EventValidation2,$Eventtarget2);
	
	my $nextPagePostCont = "__EVENTTARGET=ctl00%24Mainpage%24gridMain&__EVENTARGUMENT=Page%24$Page_Number&__LASTFOCUS=&__VIEWSTATE=<VIEWSTATE>&__VIEWSTATEGENERATOR=<VIEWSTATEGENERATOR>&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=<EVENTVALIDATION>&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optValid&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay=<FDAY>&ctl00%24Mainpage%24FromMonth=<FMON>&ctl00%24Mainpage%24FromYear=<FYEAR>&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay=<TDAY>&ctl00%24Mainpage%24ToMonth=<TMON>&ctl00%24Mainpage%24ToYear=<TYEAR>&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus=";
	
	my $nextPagePostContTemp = $nextPagePostCont;
	
	if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
	{
		$ViewState2=uri_escape($2);
	}
	if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^>]*?)"\s*\/>/is)
	{
		$ViewstateGenerator2=uri_escape($2);
	}
	if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
	{
		$EventValidation2=uri_escape($2);
	}	
	if($SearchResultContent=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTTARGET\s*"\s*value="([^>]*?)"\s*\/>/is)
	{
		$Eventtarget2=uri_escape($2);
	}	
	$nextPagePostContTemp=~s/<VIEWSTATE>/$ViewState2/igs;
	$nextPagePostContTemp=~s/<VIEWSTATEGENERATOR>/$ViewstateGenerator2/igs;
	$nextPagePostContTemp=~s/<EVENTVALIDATION>/$EventValidation2/igs;
	$nextPagePostContTemp=~s/<EVENTTARGET>/$Eventtarget2/igs;
	$nextPagePostContTemp=~s/<FDAY>/$sDay/igs;
	$nextPagePostContTemp=~s/<TDAY>/$eDay/igs;
	$nextPagePostContTemp=~s/<FMON>/$sMonth/igs;
	$nextPagePostContTemp=~s/<TMON>/$eMonth/igs;
	$nextPagePostContTemp=~s/<FYEAR>/$sYear/igs;
	$nextPagePostContTemp=~s/<TYEAR>/$eYear/igs;
	
	
	$mech->post('https://planning.southderbyshire.gov.uk/', Content => "$nextPagePostContTemp");
	my $content = $mech->content;
	$SearchResultContent = $content;
	# open(DC,">SearchResultContent.html");
	# print DC $SearchResultContent;
	# close DC;
	# exit;

	
	print "Page_Number: $Page_Number\n";
	
	$Page_Number++;
	
	
	goto NextPage;
}


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}

sub Lwp_Get()
{
    my $url = shift;
    my $ref = shift;
	my @param=@$ref if($ref ne '');
	$url =~ s/amp\;//igs;	
	re_ping:
    my $req = HTTP::Request->new(GET=>$url);
	$req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	
	$cookie->add_cookie_header($req); 
    $cookie->save; 
	
	foreach my $header(@param)
	{
		if($header=~m/\"([^\"]*?)\"\=\>\"([^\"]*?)\"/is)
		{
			my $head=$1;
			my $tail=$2;

			$req->header("$head"=>"$tail");
		}
	}
    my $res = $ua->request($req); 
	my $res_cont=$res->headers_as_string();
	
	$cookie->extract_cookies($res); 
	my $php_session_id=$1 if($res_cont=~m/PHPSESSID\=([^<]*?)\;/is);
    my $code = $res->code();
	return ($res->decoded_content,$php_session_id);
}




###### DB Connection ####
sub DbConnection1()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}

sub DbConnection()
{
	# Test DB
	# my $dsn1 ='driver={SQL Server};Server=172.27.137.183;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# Live DB
	my $dsn1 ='driver={SQL Server};Server=CH1025BD03;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	# my $dsn1 ='driver={Microsoft ODBC Driver 17 for SQL};Server=172.27.137.184;database=ScreenScrapper;uid=User2;pwd=Merit456;';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn1"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}