﻿import requests
import re
import cookielib
import sys
import itertools
from datetime import timedelta
import time
from datetime import datetime
from titlecase import titlecase


def view_state_content(content1):
    view_state = re.findall('<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']\s*\/>',content1, re.IGNORECASE)
    event = re.findall('<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']\s*\/>',content1, re.IGNORECASE)
    vsg1 = re.findall('<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']\s*\/>',content1, re.IGNORECASE)

    if view_state:
        vs = view_state[0]
        vs = re.sub(r'\/', '%2F', str(vs))
        vs = re.sub(r'\=', '%3D', str(vs))
        vs = re.sub(r'\+', '%2B', str(vs))
    else:
        print '***view_state***EMPTY'
        vs = ''

    if vsg1:
        vsg = vsg1[0]
        vsg = re.sub(r'\/', '%2F', str(vsg))
        vsg = re.sub(r'\=', '%3D', str(vsg))
        vsg = re.sub(r'\+', '%2B', str(vsg))
    else:
        print '***vsg1***EMPTY'
        vsg = ''

    if event:
        ev = event[0]
        ev = re.sub(r'\/', '%2F', str(ev))
        ev = re.sub(r'\=', '%3D', str(ev))
        ev = re.sub(r'\+', '%2B', str(ev))
    else:
        print '***event***EMPTY'
        ev = ''

    return vs,vsg,ev


 	
def view_state_content1(content1):
    view_state = re.findall('VIEWSTATE\|([^<]*?)\|',content1, re.IGNORECASE)
    event = re.findall('EVENTVALIDATION\|([^<]*?)\|',content1, re.IGNORECASE)
    vsg1 = re.findall('VIEWSTATEGENERATOR\|([^<]*?)\|',content1, re.IGNORECASE)

    if view_state:
        vs = view_state[0]
        vs = re.sub(r'\/', '%2F', str(vs))
        vs = re.sub(r'\=', '%3D', str(vs))
        vs = re.sub(r'\+', '%2B', str(vs))
    else:
        print '***view_state***EMPTY'
        vs = ''

    if vsg1:
        vsg = vsg1[0]
        vsg = re.sub(r'\/', '%2F', str(vsg))
        vsg = re.sub(r'\=', '%3D', str(vsg))
        vsg = re.sub(r'\+', '%2B', str(vsg))
    else:
        print '***vsg1***EMPTY'
        vsg = ''

    if event:
        ev = event[0]
        ev = re.sub(r'\/', '%2F', str(ev))
        ev = re.sub(r'\=', '%3D', str(ev))
        ev = re.sub(r'\+', '%2B', str(ev))
    else:
        print '***event***EMPTY'
        ev = ''

    return vs,vsg,ev
	
	
url_list=["http://extranet.net4gas.cz/capacity_ee.aspx","http://extranet.net4gas.cz/capacity_cmp.aspx","http://extranet.net4gas.cz/gas_flow.aspx","http://extranet.net4gas.cz/caloricity.aspx","http://extranet.net4gas.cz/wobbe_index.aspx","http://extranet.net4gas.cz/nomination_ee.aspx","http://extranet.net4gas.cz/allocation_p2p.aspx","http://extranet.net4gas.cz/allocation_ee.aspx"]

# url_list=["http://extranet.net4gas.cz/shutdown_firm.aspx","http://extranet.net4gas.cz/shutdown_br.aspx"]
url_list=["http://extranet.net4gas.cz/storage_actual.aspx","http://extranet.net4gas.cz/linepack.aspx"]
datasourceName=['storage_actual','linepack']
# datasourceName=['capacity_ee','capacity_cmp','gas_flow','caloricity','wobbe_index','nomination_ee','allocation_p2p','allocation_ee']
# datasourceName=['shutdown_firm','shutdown_br']
# datasourceName='test'
tempFilePath='temp/'

	

def get_content():

	try:
		
		month_word_list=['','January','February','March','April','May','June','July','August','September','October','November','December']
		month_list=[1,2,3,4,5,6,7,8,9,10,11,12]
		y = str(datetime.now().strftime("%Y"))
		m = str(datetime.now().strftime("%m"))
		for index,url in enumerate(url_list):
			try:
				print datasourceName[index]
				s=requests.session()
				cookie_file = '/tmp/cookies'
				cj = cookielib.LWPCookieJar(cookie_file)
				header={"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8","Host":"extranet.net4gas.cz","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.3"}
				s.cookies = cj
				obj=s.get(url,headers=header)
				home_page_content=obj.content
				cooki = str(s.cookies)
				header_param_sessionid = re.findall('Cookie\s*ASP\.NET\_SessionId\=([^>]*?)\s+for', cooki, re.IGNORECASE)[0]
				header_param_fgt = re.findall('FGTServer\=([^>]*?)\s+for', cooki, re.IGNORECASE)[0]
				
				vs, vsg, ev = view_state_content(home_page_content)
				
				english_post_con="__EVENTTARGET=ctl00%24lbLang&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)
				
				header={"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8","Content-Type":"application/x-www-form-urlencoded","Host":"extranet.net4gas.cz","Cookie":"FGTServer="+str(header_param_fgt)+"; ASP.NET_SessionId="+str(header_param_sessionid)}

				obj=s.post(url,data=english_post_con,headers=header)
				english_page_content=obj.content

				with open("English_data.html","w") as F:
					F.write(english_page_content)
			
				file_names=[]
				
				
				header__actual_data_net4gas = {
									
									"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
									"Content-Type":"application/x-www-form-urlencoded; charset=UTF-8",
									"Cookie": "ASP.NET_SessionId="+header_param_sessionid+"; FGTServer="+header_param_fgt
									
								}
								
				#  --------------  shutdown_firm and shutdown_br ------------------------------------------#
				
				if re.search(r"<select[^<]*?ddlPoints[^<]*?>",english_page_content,re.I) is not None:
				
					print "shutdown_firm and shutdown_br"
				
					year_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24bRefresh&ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)+"&ctl00%24holder%24ddlPoints=0&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&ctl00%24holder%24bRefresh=%20Refresh%20"
					
					
					obj=s.post(url,data=year_post_con,headers=header__actual_data_net4gas)
					Year_content=obj.content
					
					vs, vsg, ev = view_state_content1(Year_content)
					with open("Year_content.html","w") as F:
						F.write(Year_content)
						
					
					final_post_con="ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)+"&ctl00%24holder%24ddlPoints=0&ctl00%24holder%24XLS1.x=10&ctl00%24holder%24XLS1.y=8&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)
					
					
					obj=s.post(url,data=final_post_con,headers=header__actual_data_net4gas)
					con=obj.content
					
					date_time = str(datetime.now().strftime("%H%M%S"))
					rawFileName_marketName="Operational_Data"
					
					
			
					rawFile_time = str(titlecase(datasourceName[index])) + '_' + str(titlecase(rawFileName_marketName)) + '_' + str(y)+str(m)+"_"+str(date_time)
					rawFileName=tempFilePath + rawFile_time+".xlsx"
					with open(rawFileName,"wb") as F:
						F.write(con)
					
					
				else:
				
					print "Else shutdown_firm and shutdown_br"
					
					#--------------------- Storage_actual, linepack, system_deviation and supply_demand ------------------------------------------------#
				
					if re.search(r"<table[^<]*?id=[\"\']ctl00_holder_Calendar2[^<]*?>",english_page_content,re.I) is not None:
							
						from_year_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlYear1&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlYear1&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"

						obj=s.post(url,data=from_year_post_con,headers=header__actual_data_net4gas)
						from_Year_content=obj.content
						
						vs, vsg, ev = view_state_content1(from_Year_content)
							
						from_month_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlMonth1&ctl00%24holder%24ddlMonth1=8&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlMonth1&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"
						
						
						obj=s.post(url,data=from_month_post_con,headers=header__actual_data_net4gas)
						from_month_content=obj.content
						
						
								
						from_date_argument=re.findall(r"<a[^<]*?Calendar1\s*\'\s*\,\s*[\'\"]([^<]*?)[\'\"][^<]*?title=[\"\']August\s*01[\"\']>",from_month_content,re.I)
						
						# last_date=calendar.monthrange(year,8)[1]
						# last_date=str(last_date)
						last_date='31'
						
						
						if from_date_argument:
						
							from_date_argument=from_date_argument[0]
							
							vs, vsg, ev = view_state_content1(from_month_content)
							
							from_date_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24Calendar1&ctl00%24holder%24ddlMonth1=8&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2=8&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24Calendar1&__EVENTARGUMENT="+str(from_date_argument)+"&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true"
							
							obj=s.post(url,data=from_date_post_con,headers=header__actual_data_net4gas)
							from_date_post_con=obj.content
							
							# with open("bfrom_date_post_con.html","w") as F:
								# F.write(from_date_post_con)
							
								
						vs, vsg, ev = view_state_content1(from_date_post_con)
							
						to_year_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlYear2&ctl00%24holder%24ddlMonth1=8&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlYear2&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"
						
						obj=s.post(url,data=to_year_con,headers=header__actual_data_net4gas)
						to_year_content=obj.content
						
						vs, vsg, ev = view_state_content1(to_year_content)
						
						to_month_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlMonth2&ctl00%24holder%24ddlMonth1=8&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2=8&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlMonth2&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"
						
						
						
						
						obj=s.post(url,data=to_month_post_con,headers=header__actual_data_net4gas)
						to_month_content=obj.content
					
						
						todate_argument=re.findall(r"<a[^<]*?Calendar2\s*\'\s*\,\s*[\'\"]([^<]*?)[\'\"][^<]*?title=[\"\']August\s*"+re.escape(last_date)+"[\"\']>",to_month_content,re.I)
						
						
						
						if todate_argument:
						
							vs, vsg, ev = view_state_content1(to_month_content)
						
							todate_argument=todate_argument[0]
							
							
							to_date_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24Calendar2&ctl00%24holder%24ddlMonth1=10&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2=8&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24Calendar2&__EVENTARGUMENT="+str(todate_argument)+"&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true"
							
							
							
							
							obj=s.post(url,data=to_date_post_con,headers=header__actual_data_net4gas)
							to_date_post_con=obj.content
							# with open("bto_date_post_con.html","w") as F:
								# F.write(to_date_post_con)
							
						
						file_names=[]
						header__actual_data_net4gas = {
											
											"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
											"Content-Type":"application/x-www-form-urlencoded; charset=UTF-8",
											"Cookie": "ASP.NET_SessionId="+header_param_sessionid+"; FGTServer="+header_param_fgt
										}
						
						
						vs, vsg, ev = view_state_content1(to_date_post_con)
						from_year_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlYear1&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlYear1&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"

						obj=s.post(url,data=from_year_post_con,headers=header__actual_data_net4gas)
						from_Year_content=obj.content
						
						# with open("from_year_post_con.html","w") as F:
							# F.write(from_Year_content)
							
						vs, vsg, ev = view_state_content1(from_Year_content)
							
						from_month_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlMonth1&ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlMonth1&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"
						
						
						
						obj=s.post(url,data=from_month_post_con,headers=header__actual_data_net4gas)
						from_month_content=obj.content
						
						# with open("from_month_post_con.html","w") as F:
							# F.write(from_month_content)
							
						vs, vsg, ev = view_state_content1(from_month_content)
							
						to_year_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlYear2&ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlYear2&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"
						
						obj=s.post(url,data=to_year_con,headers=header__actual_data_net4gas)
						to_year_content=obj.content
						
						# with open("to_year_content.html","w") as F:
							# F.write(to_year_content)
						
						vs, vsg, ev = view_state_content1(to_year_content)
						
						to_month_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlMonth2&ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2="+str(m)+"&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP=5&__EVENTTARGET=ctl00%24holder%24ddlMonth2&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&"
						
						obj=s.post(url,data=to_month_post_con,headers=header__actual_data_net4gas)
						to_month_content=obj.content
						
						# with open("to_month_content.html","w") as F:
							# F.write(to_month_content)
							
						
						
						vs, vsg, ev = view_state_content1(to_month_content)
						
						#----------------------------- Storage_actual ---------------------------#
						
						if re.search(r"<select[^<]*?ddlVPZP[^<]*?>",to_month_content,re.I):
							
							id_list=[15,5,103]
							print "%s-%s"%(m,y)
							for id in id_list:
									
								refresh_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24bRefresh&ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2="+str(m)+"&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP="+str(id)+"&__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&ctl00%24holder%24bRefresh=%20Refresh%20"
								
								obj=s.post(url,data=refresh_post_con,headers=header__actual_data_net4gas)
								refresh_post_con=obj.content
								
								vs, vsg, ev = view_state_content1(refresh_post_con)
								
								final_post_con="ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2="+str(m)+"&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24ddlVPZP="+str(id)+"&ctl00%24holder%24XLS.x=13&ctl00%24holder%24XLS.y=15&__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)
								
								obj=s.post(url,data=final_post_con,headers=header__actual_data_net4gas)
								con=obj.content
								
								date_time = str(datetime.now().strftime("%H%M%S"))
								rawFileName_marketName="Operational_Data"
								
								rawFile_time = str(titlecase(datasourceName[index])) + '_' + str(titlecase(rawFileName_marketName)) + '_'+str(id)+'_'+ str(y)+str(m)+"_"+str(date_time)
								rawFileName=tempFilePath + rawFile_time+".xlsx"
								with open(rawFileName,"wb") as F:
									F.write(con)
						#-----------------------------linepack , system_deviation and 	supply_demand  ---------------------------#
						else:
									
							refresh_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24bRefresh&ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2="+str(m)+"&ctl00%24holder%24ddlYear2="+str(y)+"&__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true&ctl00%24holder%24bRefresh=%20Refresh%20"
							
							obj=s.post(url,data=refresh_post_con,headers=header__actual_data_net4gas)
							refresh_post_con=obj.content
							
							with open("refresh_post_con.html","w") as F:
								F.write(refresh_post_con)
						
							vs, vsg, ev = view_state_content1(refresh_post_con)
						
							final_post_con="ctl00%24holder%24ddlMonth1="+str(m)+"&ctl00%24holder%24ddlYear1="+str(y)+"&ctl00%24holder%24ddlMonth2="+str(m)+"&ctl00%24holder%24ddlYear2="+str(y)+"&ctl00%24holder%24XLS.x=13&ctl00%24holder%24XLS.y=15&__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)
							
							obj=s.post(url,data=final_post_con,headers=header__actual_data_net4gas)
							con=obj.content
							
							date_time = str(datetime.now().strftime("%H%M%S"))
							rawFileName_marketName="Operational_Data"
							
							rawFile_time = str(titlecase(datasourceName[index])) + '_' + str(titlecase(rawFileName_marketName))+'_'+ str(y)+str(m)+"_"+str(date_time)
							rawFileName=tempFilePath + rawFile_time+".xlsx"
							with open(rawFileName,"wb") as F:
								F.write(con)
								
						

					#---capacity_ee , capacity_cmp , gas_flow , caloricity , wobbe_index , nomination_ee , allocation_p2p and allocation_ee ----#			
					else:
						
						
					
						year_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlYear&ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)+"&__EVENTTARGET=ctl00%24holder%24ddlYear&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&__ASYNCPOST=true"

						obj=s.post(url,data=year_post_con,headers=header__actual_data_net4gas)
						Year_content=obj.content

						with open("Year_content.html","w") as F:
							F.write(Year_content)
							
						monthly_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24ddlMonth&__EVENTTARGET=ctl00%24holder%24ddlMonth&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)+"&__ASYNCPOST=true"
						
						obj=s.post(url,data=monthly_post_con,headers=header__actual_data_net4gas)
						monthly_content=obj.content
						f_name=str(index)+'.html'
						with open(f_name,"w") as F:
							F.write(monthly_content)
						
						calendar=re.findall(r"<table\s*id=[\"\'][^<]*?(Calendar[^<]*?)[\"\']",monthly_content,re.I)[0]
						
						
						
						date_list=re.findall(r"<a\s*href=\"javascript\:__doPostBack\(\'ctl00\$holder\$"+re.escape(calendar)+"\s*\'\,\s*[\'\"]([\d]+)[\'\"][^<]*?title=[\"\']"+re.escape(month_word_list[int(m)])+"\s*[\d]+[\"\']>\s*([\d]+)\s*<\/a>",monthly_content,re.I)
						
						x="<a\s*href=\"javascript\:__doPostBack\(\'ctl00\$holder\$"+re.escape(calendar)+"\s*\'\,\s*[\'\"]([\d]+)[\'\"][^<]*?title=[\"\']"+re.escape(month_word_list[int(m)])+"\s*[\d]+[\"\']>\s*([\d]+)\s*<\/a>"
						
						
						vs1, vsg1, ev1 = view_state_content1(monthly_content)
						if date_list:
							for d in date_list:
								print "%s-%s-%s"%(d[1],m,y)
								date_post_con="ctl00%24holder%24ScriptManager1=ctl00%24holder%24UpPanel%7Cctl00%24holder%24"+str(calendar)+"&__EVENTTARGET=ctl00%24holder%24"+str(calendar)+"&__EVENTARGUMENT="+str(d[0])+"&__LASTFOCUS=&__VIEWSTATE="+str(vs1)+"&__VIEWSTATEGENERATOR="+str(vsg1)+"&__EVENTVALIDATION="+str(ev1)+"&ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)+"&__ASYNCPOST=true"
								

								obj=s.post(url,data=date_post_con,headers=header__actual_data_net4gas)
								date_content=obj.content

								with open("date.html","w") as F:
									F.write(date_content)
								
								
								final_post_con="ctl00%24holder%24ddlMonth="+str(m)+"&ctl00%24holder%24ddlYear="+str(y)+"&ctl00%24holder%24XLS1.x=11&ctl00%24holder%24XLS1.y=12&__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="+str(vs1)+"&__VIEWSTATEGENERATOR="+str(vsg1)+"&__EVENTVALIDATION="+str(ev1)
							
						
								obj=s.post(url,data=final_post_con,headers=header__actual_data_net4gas)
								final_page_content=obj.content
								date_time = str(datetime.now().strftime("%H%M%S"))
								rawFileName_marketName="Operational_Data"
								
								rawFile_time = str(titlecase(datasourceName[index])) + '_' + str(titlecase(rawFileName_marketName)) + '_' + str(y)+str(m)+str(d[1])+"_"+str(date_time)
								rawFileName=tempFilePath + rawFile_time+".xlsx"
								with open(rawFileName,"wb") as F:
									F.write(final_page_content)
						
				
					
			except Exception as e:
				print "Error",str(e)
				print str(e) + str(sys.exc_traceback.tb_lineno)
				
				

	except Exception as e:
		print str(e) + str(sys.exc_traceback.tb_lineno)
        return 'n/a'
		
	
get_content()