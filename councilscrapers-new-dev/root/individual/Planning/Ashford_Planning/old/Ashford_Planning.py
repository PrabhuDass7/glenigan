# -*- coding: utf-8 -*-

import requests
import re
import cookielib
import sys
import itertools
from datetime import timedelta
import time
from datetime import datetime
from titlecase import titlecase
import pymssql
import ssl

reload(sys)
sys.setdefaultencoding("utf-8")

folder_path = 'C:/Glenigan/Live_Schedule/Planning/Ashford_Planning'


# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)
	
def view_state_content(content):
    view_state = re.findall('<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']\s*\/>',content, re.IGNORECASE)
    event = re.findall('<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']\s*\/>',content, re.IGNORECASE)
    vsg1 = re.findall('<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']\s*\/>',content, re.IGNORECASE)

    if view_state:
        vs = view_state[0]
        vs = re.sub(r'\/', '%2F', str(vs))
        vs = re.sub(r'\=', '%3D', str(vs))
        vs = re.sub(r'\+', '%2B', str(vs))
    else:
        print '***view_state***EMPTY'
        vs = ''

    if vsg1:
        vsg = vsg1[0]
        vsg = re.sub(r'\/', '%2F', str(vsg))
        vsg = re.sub(r'\=', '%3D', str(vsg))
        vsg = re.sub(r'\+', '%2B', str(vsg))
    else:
        print '***vsg1***EMPTY'
        vsg = ''

    if event:
        ev = event[0]
        ev = re.sub(r'\/', '%2F', str(ev))
        ev = re.sub(r'\=', '%3D', str(ev))
        ev = re.sub(r'\+', '%2B', str(ev))
    else:
        print '***event***EMPTY'
        ev = ''

    return vs,vsg,ev
	
def inputsection(fDate, tDate, Url):
	try:
		month_word_list=['','January','February','March','April','May','June','July','August','September','October','November','December']
		month_list=[1,2,3,4,5,6,7,8,9,10,11,12]
		y = str(datetime.now().strftime("%Y"))
		y2 = str(datetime.now().strftime("%y"))
		m = str(datetime.now().strftime("%m"))
		mb = str(datetime.now().strftime("%b"))
		
		# print "Two digit Year", y2
		# print "Current Year", y
		# print "Current Month", m
		# print "Current Month", mb
		# raw_input()
		
		s=requests.session()
		
		cookie_file = folder_path+"/cookies"	
		cj = cookielib.LWPCookieJar(cookie_file)
		
		header={"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8","Host":"planning.ashford.gov.uk","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36"}
		s.cookies = cj
		obj=s.get(Url,headers=header)
		home_page_content=obj.content
		cooki = str(s.cookies)
		
		header_param_sessionid = re.findall('Cookie\s*ASP\.NET\_SessionId\=([^>]*?)\s+for', cooki, re.IGNORECASE)[0]
		print "Cookie:",header_param_sessionid
		
		# with open(folder_path+"/filename.html", 'wb') as fd:
			# fd.write(home_page_content)
		
		vs, vsg, ev = view_state_content(home_page_content)
		
		url = 'http://planning.ashford.gov.uk/terms.aspx'
		
		accept_post_con="ctl00%24ScriptManager1=ctl00%24CPH_Details%24UpdatePanel1%7Cctl00%24CPH_Details%24Submit_Button&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&ctl00%24CPH_Details%24Agree_CheckBox=on&__ASYNCPOST=true&ctl00%24CPH_Details%24Submit_Button=Proceed"
		
		header={"Accept":"*/*","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.5","Host":"planning.ashford.gov.uk","Content-Type":"application/x-www-form-urlencoded; charset=utf-8","User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36","Cookie":"ASP.NET_SessionId="+str(header_param_sessionid)+"; SHADER-SRVNODE=s5","Referer":str(url)}

		obj=s.post(url,data=accept_post_con,headers=header, allow_redirects=True)
		post_page_content=obj.content		
		
		obj=s.get('http://planning.ashford.gov.uk/',headers=header, allow_redirects=True)
		page_content=obj.content
		# with open(folder_path+"/page_content.html", 'wb') as fd:
			# fd.write(page_content)
		# raw_input()
		
		vs, vsg, ev = view_state_content(page_content)

			
		search_post_con="__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&__EVENTVALIDATION="+str(ev)+"&ctl00%24CPH_Details%24Button2=Planning+Applications"
		
		header={"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8","Host":"planning.ashford.gov.uk","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36","Referer":"http://planning.ashford.gov.uk/"}
		
		obj=s.post(Url,data=search_post_con,headers=header, allow_redirects=True)
		searchpage_content=obj.content
		
		obj=s.get(Url, allow_redirects=True)
		search_content=obj.content
		
		with open(folder_path+"/filename.html", 'wb') as fd:
			fd.write(search_content)
		
		vs, vsg, ev = view_state_content(search_content)
		
		header={"Accept":"*/*","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.5","Host":"planning.ashford.gov.uk","Content-Type":"application/x-www-form-urlencoded; charset=utf-8","User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36","Cookie":"ASP.NET_SessionId="+str(header_param_sessionid)+";","Referer":""+str(Url)+""}
				
		datePostCnt = "ctl00%24ScriptManager1=ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24UpdatePanel1%7Cctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24DateSearchBy&__LASTFOCUS=&CPH_Details_Details_TabContainer_ClientState=%7B%22ActiveTabIndex%22%3A2%2C%22TabEnabledState%22%3A%5Btrue%2Ctrue%2Ctrue%2Ctrue%2Ctrue%2Ctrue%2Ctrue%2Ctrue%5D%2C%22TabWasLoadedOnceState%22%3A%5Btrue%2Cfalse%2Cfalse%2Cfalse%2Cfalse%2Cfalse%2Cfalse%2Cfalse%5D%7D&__EVENTTARGET=ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24DateSearchBy&__EVENTARGUMENT=&__VIEWSTATE="+str(vs)+"&__VIEWSTATEGENERATOR="+str(vsg)+"&ctl00%24Header%24MyAppsHeader1%24EmailAddress=&ctl00%24Header%24MyAppsHeader1%24accPassword=&ctl00%24CPH_Details%24Details_TabContainer%24CaseNo_Tab%24searchby_caseNo1%24CaseYear="+str(y2)+"&ctl00%24CPH_Details%24Details_TabContainer%24CaseNo_Tab%24searchby_caseNo1%24CaseNo=&ctl00%24CPH_Details%24Details_TabContainer%24Address_Tab%24searchby_address1%24Address=&ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24DateSearchBy=Decision%20Date&ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24StartDate%24DD_Month="+str(mb)+"&ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24StartDate%24DD_Year="+str(y)+"&ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24EndDate%24DD_Month="+str(mb)+"&ctl00%24CPH_Details%24Details_TabContainer%24Date_Tab%24searchby_date1%24EndDate%24DD_Year="+str(y)+"&ctl00%24CPH_Details%24Details_TabContainer%24App_Tab%24searchby_appType1%24AppType=PLA&ctl00%24CPH_Details%24Details_TabContainer%24App_Tab%24searchby_appType1%24DateSearchBy=Decision%20Date&ctl00%24CPH_Details%24Details_TabContainer%24App_Tab%24searchby_appType1%24StartDate%24DD_Month="+str(mb)+"&ctl00%24CPH_Details%24Details_TabContainer%24App_Tab%24searchby_appType1%24StartDate%24DD_Year="+str(y)+"&ctl00%24CPH_Details%24Details_TabContainer%24App_Tab%24searchby_appType1%24EndDate%24DD_Month="+str(mb)+"&ctl00%24CPH_Details%24Details_TabContainer%24App_Tab%24searchby_appType1%24EndDate%24DD_Year="+str(y)+"&ctl00%24CPH_Details%24Details_TabContainer%24AppAgent_Tab%24searchby_appAgent1%24SearchBy_List=Applicant&ctl00%24CPH_Details%24Details_TabContainer%24AppAgent_Tab%24searchby_appAgent1%24SearchValue=&ctl00%24CPH_Details%24Details_TabContainer%24List_Tab%24searchby_weeklyLists1%24WeekCommencing%24DD_Month="+str(mb)+"&ctl00%24CPH_Details%24Details_TabContainer%24List_Tab%24searchby_weeklyLists1%24WeekCommencing%24DD_Year="+str(y)+"&ctl00%24CPH_Details%24Details_TabContainer%24List_Tab%24searchby_weeklyLists1%24WeeklyListType=New%20Applications%20Published&ctl00%24CPH_Details%24Details_TabContainer%24Summary_Tab%24monthlySummary_1%24DD_Month="+str(mb)+"&ctl00%24CPH_Details%24Details_TabContainer%24Summary_Tab%24monthlySummary_1%24DD_Year="+str(y)+"&__ASYNCPOST=true&"
				
		obj=s.post(Url, data=datePostCnt, headers=header, allow_redirects=True)
		# search_content=obj.content
		
		# obj=s.get(Url)
		search_pgecontent=obj.content
		
		with open(folder_path+"/English_data.html","wb") as F:
			F.write(search_pgecontent)
			
		return search_pgecontent

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno
			
# Main section
if __name__== "__main__":
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	
	url = 'http://planning.ashford.gov.uk/planning/Default.aspx?new=true'
	
	results = inputsection(fromdate, todate, url)

	