# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Merit/Projects/Individual_councils'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, driver):
	try:
		#-- Parse
		driver.get('https://planning.eastleigh.gov.uk/s/public-register')
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		finalCnt=''
		if len(content) > 0:
			finalCnt = content.encode("utf-8")
		
		matchedContent = re.findall("<a[^>]*?id=\"([^\"]*?)\"[^>]*?>\s*Advanced\s*Search\s*<\/a>\s*<\/li>", str(finalCnt), re.IGNORECASE)  
		xpath = '//*[@id="'+str(matchedContent[0])+'"]'

		driver.find_element_by_xpath(xpath).click()
		#-- Wait funtion
		time.sleep(5)  
		advcontent=driver.page_source
		advfinalCnt=''
		if len(advcontent) > 0:
			advfinalCnt = advcontent.encode("utf-8")

		# print advfinalCnt

		fDate = re.findall("<span[^>]*?>\s*Date\s*Received\s*From\s*<\/span>\s*<\!\-\-[^>]*?\-\->\s*<\!\-\-[^>]*?\-\->\s*<\/label>\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advfinalCnt), re.IGNORECASE)  
		fromDate = '//*[@id="'+str(fDate[0])+'"]'

		tDate = re.findall("<span[^>]*?>\s*Date\s*Received\s*To\\s*<\/span>\s*<\!\-\-[^>]*?\-\->\s*<\!\-\-[^>]*?\-\->\s*<\/label>\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advfinalCnt), re.IGNORECASE)  
		toDate = '//*[@id="'+str(tDate[0])+'"]'
		
		driver.find_element_by_xpath(fromDate).send_keys(dumFrom_Date)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath(toDate).send_keys(dumTo_Date)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="expando-unique-id"]/div/div/div/form/div/div/div[2]/button').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		# with open(folder_path+"/190_out.html", 'wb') as fd:
		# 	fd.write(searchfinalCnt)
		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Application parse section	
def parsesection(searchcontent, CouncilCode, Source, conn, cursor, driver):
	try:
		searchcount = re.findall("Planning\s+Applications\s+\((\d+)\)<\/h2>", str(searchcontent), re.IGNORECASE)  
		print "searchcount:", int(searchcount[0])
		totalcount = int(searchcount[0]) + 1
		print "Totalcount", totalcount

		a = 1
		appCount = 0

		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_NEW (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date) values '
		
		bulkValuesForQuery=''

		appcollection = re.findall("<h4[^>]*?>\s*<a[^>]*?>([^>]*?)\s+\-\s*([^<]*?)<\/a>\s*<\/h4>", str(searchcontent), re.IGNORECASE) 
		for appMatch in appcollection:
			app_num = appMatch[0]

			print a ,"<==>", totalcount
			print "Current Application Number is:", app_num

			if a <= totalcount:
				if a == 6:
					driver.find_element_by_xpath('//*[@id="arcusbuilt__PApplication__c"]/div['+str(totalcount)+']/a').click()
					#-- Wait funtion
					time.sleep(5)  
				
				driver.find_element_by_xpath('//*[@id="arcusbuilt__PApplication__c"]/div['+str(a)+']/div/h4/a').click()	
				#-- Wait funtion
				time.sleep(15)  
				appcontent=driver.page_source
				
				appfinalCnt=''
				if len(appcontent) > 0:
					appfinalCnt = appcontent.encode("utf-8")

				appDataCollection = appParseSection(appfinalCnt, app_num)
				print "appDataCollection:  ", appDataCollection

				# with open(folder_path+"/filename.html", 'wb') as fd:
				# 	fd.write(appfinalCnt)
				
				a += 1
                # driver.find_element_by_xpath('//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[2]/div/button[1]').click()
				driver.find_element_by_xpath('//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[2]/div/button[1]/lightning-primitive-icon').click()
				
				#-- Wait funtion
                time.sleep(7)
                
                print "InsertQuery: ", bulkValuesForQuery

                return bulkValuesForQuery

                """
				appDataCollection = appParseSection(appfinalCnt, app_num)
				
				App=appDataCollection[0]
				Prop=appDataCollection[1]
				DtApplicationRegistered=appDataCollection[2]
				DtApplicationReceived=appDataCollection[3]
				DtApplicationvalidated=appDataCollection[4]
				AppStatus=appDataCollection[5]
				AppType=appDataCollection[6]
				Addr=appDataCollection[7]
				AppName=appDataCollection[8]
				AgntName=appDataCollection[9]
				AgntAddress=appDataCollection[10]
				AppAddr=appDataCollection[11]
				AgntEmail=appDataCollection[12]
				AgntTelephone=appDataCollection[13]
				TrgtDecdt=appDataCollection[14]

                print "App",App
                print "Prop",Prop
                print "DtApplicationRegistered",DtApplicationRegistered
                print "DtApplicationReceived",DtApplicationReceived
                print "DtApplicationvalidated",DtApplicationvalidated
                print "AppStatus",AppStatus
                print "AppType",AppType
                print "Addr",Addr
                print "AppName",AppName
                print "AgntName",AgntName
                print "AgntAddress",AgntAddress
                print "AppAddr",AppAddr
                print "AgntEmail",AgntEmail
                print "AgntTelephone",AgntTelephone
                print "TrgtDecdt",TrgtDecdt

                print "appDataCollectionType:  ", type(appDataCollection)
                print "appDataCollection:  ", appDataCollection
                raw_input()

                
                Page_URL='https://planning.eastleigh.gov.uk/s/public-register'
                format1 = "%Y/%m/%d %H:%M"	
				
                Schedule_Date = datetime.now().strftime(format1)

                COUNCIL_NAME = "Eastleigh"
                Source_With_Time = Source+"_"+Schedule_Date+"-perl"

                joinValues="('"+str(CouncilCode)+"','"+str(COUNCIL_NAME)+"','"+str(Addr)+"','"+str(DtApplicationReceived)+"','"+str(App)+"','"+str(DtApplicationRegistered)+"','"+str(DtApplicationvalidated)+"','"+str(Prop)+"','"+str(AppStatus)+"','','"+str(AgntAddress)+"','','"+str(AgntName)+"','"+str(AgntTelephone)+"','"+str(AppAddr)+"','"+str(AppName)+"','"+str(AppType)+"','"+str(AgntEmail)+"','','','','','','','','','','"+str(TrgtDecdt)+"','','"+str(Page_URL)+"','"+str(Page_URL)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"')"

				

                print "joinValues:  ", appDataCollection
                
                if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
                elif appCount == 10:
                    bulkValuesForQuery = bulkValuesForQuery+","+joinValues
                    cursor.execute(bulkValuesForQuery)
                    conn.commit()
                    appCount=0
                    bulkValuesForQuery=''
                else:
                    bulkValuesForQuery = bulkValuesForQuery+","+joinValues
                    appCount += 1	
                    
                a = a + 1
                driver.find_element_by_xpath('//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[2]/div/button[1]').click()	
				#-- Wait funtion
                time.sleep(5)
				"""

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application details parse section	
def appParseSection(Appcontent, AppNum):
    try:
        conDetails=[]
        Application_Number=re.findall(r'<label[^>]*?>\s*Planning\s*Application\s*Number\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Proposal=re.findall(r'<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Proposal1=re.findall(r'<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Proposal2=re.findall(r'<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Proposal3=re.findall(r'<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Application_Status=re.findall(r'<label[^>]*?>\s*Application\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Application_Type=re.findall(r'<label[^>]*?>\s*Application\s*Type\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Address=re.findall(r'<label[^>]*?>\s*Site\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Date_Application_Received=re.findall(r'<label[^>]*?>\s*Date\s*Received\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Date_Application_Registered=re.findall(r'<label[^>]*?>\s*Registration\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Date_Application_Registered1=re.findall(r'<label[^>]*?>\s*Date\s*Registration\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Date_Application_validated=re.findall(r'<label[^>]*?>\s*Date\s*Valid\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Applicant_Name=re.findall(r'<label[^>]*?>\s*Applicant\s*Name\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Agent_Name=re.findall(r'<label[^>]*?>\s*Agent\s*Name\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Agent_Email=re.findall(r'<label[^>]*?>\s*Agent\s*Email\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Agent_Telephone=re.findall(r'<label[^>]*?>\s*Agent\s*Telephone\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Agent_Address=re.findall(r'<label[^>]*?>\s*Agent\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        Applicant_Address=re.findall(r'<label[^>]*?>\s*Applicant\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        TargetDec_dt=re.findall(r'<label[^>]*?>\s*Decision\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
        
        
        (Application, ProposalCnt, ApplicationType, AppAddress, ApplicationStatus, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt) = ('','','','','','','','','','','','','','','')


        if len(Application_Number) > 0:
			Application=clean(Application_Number[0])
        else:
			Application=AppNum

        conDetails.append(Application)


        if len(Proposal) > 0:
			ProposalCnt=clean(Proposal[0])
        elif len(Proposal1) > 0:
			ProposalCnt=clean(Proposal1[0])
        elif len(Proposal2) > 0:
			ProposalCnt=clean(Proposal2[0])
        elif len(Proposal3) > 0:
			ProposalCnt=clean(Proposal2[0])

        conDetails.append(ProposalCnt)

        if len(Date_Application_Registered) > 0:
			DateApplicationRegistered=clean(Date_Application_Registered[0])
        elif len(Date_Application_Registered1) > 0:
			DateApplicationRegistered=clean(Date_Application_Registered1[0])
        else:
			DateApplicationRegistered=''
        conDetails.append(DateApplicationRegistered)

        if len(Date_Application_Received) > 0:
			DateApplicationReceived=clean(Date_Application_Received[0])
        else:
			DateApplicationReceived=''
        conDetails.append(DateApplicationReceived)

        if len(Date_Application_validated) > 0:
			DateApplicationvalidated=clean(Date_Application_validated[0])
        else:
			DateApplicationvalidated=''
        conDetails.append(DateApplicationvalidated)

        if len(Application_Status) > 0:
			ApplicationStatus=clean(Application_Status[0])
        else:
			ApplicationStatus=''
        conDetails.append(ApplicationStatus)

        if len(Application_Type) > 0:
			ApplicationType=clean(Application_Type[0])
        else:
			ApplicationType=''
        conDetails.append(ApplicationType)

        if len(Address) > 0:
			AppAddress=clean(Address[0])
        else:
			AppAddress=''

        AppAddress = re.sub(r'\'', "\'\'", str(AppAddress))
        conDetails.append(AppAddress)


        if len(Applicant_Name) > 0:
			ApplicantName=clean(Applicant_Name[0])
        else:
			ApplicantName=''
			
        ApplicantName = re.sub(r'\'', "\'\'", str(ApplicantName))
        conDetails.append(ApplicantName)


        if len(Agent_Name) > 0:
			AgentName=clean(Agent_Name[0])
        else:
			AgentName=''
        conDetails.append(AgentName)


        if len(Agent_Address) > 0:
			AgentAddress=clean(Agent_Address[0])
        else:
			AgentAddress=''

        AgentAddress = re.sub(r'\'', "\'\'", str(AgentAddress))
        conDetails.append(AgentAddress)


        if len(Applicant_Address) > 0:
			Applicant_Address[0] = re.sub(r'(?:[\,\s])+$', "", str(Applicant_Address[0]))
			ApplicantAddress=clean(Applicant_Address[0])
        else:
			ApplicantAddress=''

        ApplicantAddress = re.sub(r'\'', "\'\'", str(ApplicantAddress))
        conDetails.append(ApplicantAddress)



        if len(Agent_Email) > 0:
			AgentEmail=clean(Agent_Email[0])
        else:
			AgentEmail=''
        conDetails.append(AgentEmail)


        if len(Agent_Telephone) > 0:
			AgentTelephone=clean(Agent_Telephone[0])
        else:
			AgentTelephone=''
        conDetails.append(AgentTelephone)



        if len(TargetDec_dt) > 0:
			TargetDecdt=clean(TargetDec_dt[0])
        else:
			TargetDecdt=''
        conDetails.append(TargetDecdt)

        
        

        return conDetails 

    except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:      
        clean = ''  
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
        return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Merit/Projects/Selenium_Jar/chromedriver.exe')

	# service = service.Service('C:/Glenigan/Merit/Projects/Selenium_Jar/chromedriver.exe')
	# service.start()
	# capabilities = {'chrome.binary': 'C:/Glenigan/Merit/Projects/Selenium_Jar/chromedriver.exe'}
	# browser = webdriver.Remote(service.service_url, capabilities)
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	
	results = inputsection(fromdate, todate, browser)

	finalinsertQuery = parsesection(results, councilcode, gcs, conn, cursor, browser)
	
	if finalinsertQuery:
		cursor.execute(finalinsertQuery)
		conn.commit()
	
	browser.quit()