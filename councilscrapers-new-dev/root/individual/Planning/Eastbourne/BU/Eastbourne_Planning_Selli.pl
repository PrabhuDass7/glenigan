use strict;
use DBI;
use DBD::ODBC;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Config::Tiny;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;

# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $input_from_date = $ARGV[1];
my $input_to_date = $ARGV[2];


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Eastbourne/Eastbourne_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};

#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();


print "HOME_URL==>$HOME_URL\n";
print "Council_Code: $Council_Code\n";

$driver->get($HOME_URL);
sleep(10);
my $model_cont=$driver->get_page_source($driver);


$driver->find_element_by_xpath("//*[\@id=\"ac1\"]")->click;
sleep(5);	
$driver->find_element_by_xpath("//*[\@id=\"faux\"]/div[1]/div/form/fieldset/div/div/div[3]/input")->click;
sleep(5);	
my $temp1=$driver->get_page_source($driver);

# open(PPP,">Search_Content.html");
# print PPP "$model_cont\n";
# close(PPP);
# exit;


$driver->find_element("/html/body/form/div[2]/div/div/div[3]/div/div[2]/div/div/div[13]/div/div[2]/input")->send_keys("$input_from_date");
sleep(5);
print "FromDate: $input_from_date\n";

$driver->find_element("/html/body/form/div[2]/div/div/div[3]/div/div[2]/div/div/div[14]/div/div[2]/input")->send_keys("$input_to_date");
sleep(5);	
print "ToDate: $input_to_date\n";

$driver->find_element_by_xpath("/html/body/form/div[2]/div/div/div[3]/div/div[2]/div/div/button")->click;
sleep(10);	

my $searchContent=$driver->get_page_source($driver);

# open FH, ">Home.html";
# print FH $searchContent;
# close FH;
# exit;



my $id = $1 if($searchContent=~m/<div\s*id=\"(espr_renderHost_PageStructureDisplayRenderer[^\"]*?javavscriptWidget_results)\">/is);

my $page=2;
Next_Page:
my $a=1;
while($searchContent=~m/<a[^>]*?KeyText=[^\"]*?\"\s*>\s*Planning\s*Application\s*([^>]*?)\s*\-[^>]*?<\/a>/gsi)
{
	my $Application = $1;
	
	print "\n\nApplication Number is Processing::$Application\n\n";
	
	$driver->find_element_by_xpath("//*[\@id=\"$id\"]/div/div/div[1]/div[2]/ul/li[$a]/div/div/div/div/div/div/div/div/div/a")->click;
	sleep(5);
	my $current_URL = $driver->get_current_url();
	print "$current_URL\n";
	
	my $content=$driver->get_page_source($driver);
	$content=~s/<\!\-\-[^<]*?\-\->//gsi;
	
	# open FH, ">Output-$a.html";
	# print FH $content;
	# close FH;
	# exit;
	
# =pod		
		
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	
	my ($Actual_Decision_Level,$Agent_Company_Name,$Agent_Telephone,$Agent_Email,$Agent_Telephone_1,$Agent_mobile,$Agent_Fax,$agent_contact_details,$Actual_Committee_Date,$Actual_Committee_or_Panel_Date,$Advertisement_Expiry_Date,$Agreed_Expiry_Date,$Application_Expiry_Deadline,$TargetDec_dt,$Temporary_Permission_Expiry_Date,$Application_Link,$Document_Url,$No_document,$Source_With_Time);
	
	
	my $Application_Number = &clean($1) if($content=~m/>\s*Planning\s*Application\s*([^>]*?)\s*\-/is);
	my $Application_Status = &clean($1) if($content=~m/>\s*Stage\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Application_Type = &clean($1) if($content=~m/>\s*Application\s*Type\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Address = &clean($1) if($content=~m/>\s*Premises\s*address\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Date_Application_Received = &clean($1) if($content=~m/>\s*Date\s*Received\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Date_Application_Registered = &clean($1) if($content=~m/>\s*Date\s*Registere?d?\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Date_Application_validated = &clean($1) if($content=~m/>\s*Date\s*valid\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Applicant_Name = &clean($1) if($content=~m/>\s*Applicant\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Applicant_Address = &clean($1) if($content=~m/>\s*Applicant\s*Address\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Agent_Name = &clean($1) if($content=~m/>\s*Agent\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Agent_Address = &clean($1) if($content=~m/>\s*Agent\s*Address\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Proposal = &clean($1) if($content=~m/>\s*Application\s*Description\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is);
	my $TargetDec_dt = &clean($1) if($content=~m/>\s*Target\s*Determination\s*Date\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is);
	
	$Address=~s/\'/\'\'/gsi;
	$Applicant_Address=~s/\'/\'\'/gsi;
	$Applicant_Name=~s/\'/\'\'/gsi;
	$Agent_Address=~s/\'/\'\'/gsi;	

	if($Application_Number=~m/^\s*$/is)
	{
		if($Application!~m/^\s*$/is)
		{
			$Application_Number = $Application;
		}
	}

	
	my $Application_Link = $current_URL;
	my $Doc_Url = "";
	
	
	my $Source = "GCS001";	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
	$Document_Url=$Application_Link;
	
	# Create insert query for required columns
	my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_NEW (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Date_Application_Received\', \'$Application_Number\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Proposal\', \'$Application_Status\', \'$Actual_Decision_Level\', \'$Agent_Address\', \'$Agent_Company_Name\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Applicant_Address\', \'$Applicant_Name\', \'$Application_Type\', \'$Agent_Email\', \'$Agent_Telephone_1\', \'$Agent_mobile\', \'$Agent_Fax\', \'$agent_contact_details\', \'$Actual_Committee_Date\', \'$Actual_Committee_or_Panel_Date\', \'$Advertisement_Expiry_Date\', \'$Agreed_Expiry_Date\', \'$Application_Expiry_Deadline\', \'$TargetDec_dt\', \'$Temporary_Permission_Expiry_Date\', \'$Application_Link\', \'$Document_Url\', \'$No_document\', \'$Source_With_Time\', \'$Schedule_Date\')";
	
	
	print "insert_query::$insert_query\n";
	
	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
	
	
	undef $Address; undef $Date_Application_Received; undef $Application_Number; undef $Date_Application_Registered; undef $Date_Application_validated; undef $Proposal; undef $Application_Status; undef $Actual_Decision_Level; undef $Agent_Address; undef $Agent_Company_Name; undef $Agent_Name; undef $Agent_Telephone; undef $Applicant_Address; undef $Applicant_Name; undef $Application_Type; undef $Agent_Email; undef $Agent_Telephone_1; undef $Agent_mobile; undef $Agent_Fax; undef $agent_contact_details; undef $Actual_Committee_Date; undef $Actual_Committee_or_Panel_Date; undef $Advertisement_Expiry_Date; undef $Agreed_Expiry_Date; undef $Application_Expiry_Deadline; undef $TargetDec_dt; undef $Temporary_Permission_Expiry_Date; undef $Application_Link; undef $Document_Url; undef $No_document; undef $Schedule_Date; undef $Source_With_Time;
			
		
# =cut	

	$a++;	
	
	$driver->go_back();
	sleep(7);
}


if($searchContent=~m/<div\s*class=\"btn\s*secondary-btn\"[^>]*?>\s*Next\s*<\/div>/is)
{
	$driver->find_element_by_xpath("//*[\@id=\"$id\"]/div/div/div[2]/div/div/div")->click;
	sleep(7);	
	my $content=$driver->get_page_source($driver);
	$searchContent = $content;
	
	print "\#\#\#\#$page page content\#\#\#\#\n";
	
	goto Next_Page;
}
elsif($searchContent=~m/<div\s*class=\"btn\s*secondary-btn\s*disabled-btn\"[^>]*?>\s*Next\s*<\/div>/is)
{
	print "\nNo next page found\n";
}

$driver->quit();
$driver->close();

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}