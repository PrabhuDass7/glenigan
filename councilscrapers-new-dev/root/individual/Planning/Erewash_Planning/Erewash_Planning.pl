use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];


# my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://myservice.erewash.gov.uk/Planning/lg/GFPlanningWelcome.page";
$mech->get($searchURL);
my $SearchResultContent = $mech->content;
my $status = $mech->status;


# open(DC,">Content.html");
# print DC $SearchResultContent;
# close DC;
# exit;


my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Application, Address, Date_Application_Registered, Date_Application_validated, Applicant_name, Applicant_Address, Application_type, Proposal, Agent_Name, Agent_Telephone, Agent_Company_Name, Agent_Address, Application_Status, Date_Application_Received, TargetDec_dt, Easting, Northing, PAGE_URL, Document_Url, Source, Schedule_Date) values';

my $Page_Number=1;
my @Weekly_List_Urls;

while($SearchResultContent=~m/<a[^>]*?href\s*=\s*\"([^>]*?ApplicationValidDateFrom[^>]*?)\"\s*>\s*([^>]*?)\s*<\/a>/igs)
{
	my $Weekly_URL=$1;
	my $Weekly_Range=$2;
	
	
	$Weekly_URL=~s/^\.+//igs;
	$Weekly_URL=~s/\\\//\//gsi;
	$Weekly_URL=~s/\&amp;/\&/gsi;
	
	my $Weekly_List_URL='https://myservice.erewash.gov.uk/Planning'.$Weekly_URL;
	
		
	# print "Weekly_URL==>$Weekly_List_URL\n";	
	# print "Weekly_Range=>$Weekly_Range\n";
	
	push(@Weekly_List_Urls,$Weekly_List_URL);
	
}


foreach my $Week_Url(@Weekly_List_Urls)
{
	print "\nWeek_Url:$Week_Url\n";
	
	$mech->get($Week_Url);
	my $content = $mech->content;
	my $status = $mech->status;
	
	my $Number_Pages=0;
	my ($Token,$ViewState,$CSRFToken);
	
	NextPage:
	if($content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"javax\.faces\.ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
	{
		my $ViewState_key=uri_escape($1);
		$ViewState=uri_escape($2);
	}
	if($content=~m/<input\s*id[^>]*?token[^>]*?value\s*=\s*\"([^>]*?)\"\s\/\s*>/is)
	{
		$Token=uri_escape($1);
	}		
	if($content=~m/<input\s*type[^>]*?Civica\.CSRFToken\s*\"\s*value\s*=\s*\"([^>]*?)\"\s*\/>/is)
	{
		$CSRFToken=uri_escape($1);
	}	
	
	while($content=~m/<a\s*href[^>]*?scroll_1idx[^>]*?>\s*([\d]+)\s*<\s*\/a>/gsi)
	{
		$Number_Pages=$1;
	}	
	
	print "Number of Pages: $Number_Pages\n";	
	while($content=~m/<input\s*id\s*=\s*\"([^>]*?)\"\s*name[^>]*?value\s*=\s*\"([\d\/]*?)\"\s*[^>]*?>/igs)
	{
		my $ID_Reference=uri_escape($1);
		my $Application_Reference=$2;
		# print "ID_Reference==>$ID_Reference\n";	
		print "Application_Number=>$Application_Reference\n";
		
		my $appPostContent = 'javax.faces.ViewState=<Viewstate>&_id98%3Atoken=<Token>&<Reference_ID>=<Reference>&_id98_SUBMIT=1&Civica.CSRFToken=<CSRFToken>&_id98%3Ascroll_2=&_id98%3Ascroll_1=&_id98_SUBMIT=&_id98%3A_idcl=&_id98%3A_link_hidden_=';
		
		$appPostContent =~s/<Viewstate>/$ViewState/igs;
		$appPostContent =~s/<Token>/$Token/igs;
		$appPostContent =~s/<CSRFToken>/$CSRFToken/igs;
		$appPostContent =~s/<Reference_ID>/$ID_Reference/igs;
		$appPostContent =~s/<Reference>/$Application_Reference/igs;
		
		$mech->post('https://myservice.erewash.gov.uk/Planning/lg/GFPlanningSearchResults.page', Content => "$appPostContent");
		my $App_content = $mech->content;
		
		my ($insert_query1)=&Scrape_Details($Council_Code,$App_content,$Week_Url);
		$insert_query.=$insert_query1;
		# open(DC,">content.html");
		# print DC $content;
		# close DC;
		# exit;
			
	}
	
	if($content=~m/<a\s*href\s*\=\"[^>]*?scroll_1next\"\s*>/is)
	{		
		my $nextPagePostContent = 'javax.faces.ViewState=<ViewState>&_id98%3Atoken=<Token>&_id98_SUBMIT=1&Civica.CSRFToken=<CSRFToken>&_id98%3Ascroll_2=next&_id98%3Ascroll_1=&_id98_SUBMIT=&_id98%3A_idcl=_id98%3Ascroll_2next&_id98%3A_link_hidden_=';
		
		print "Page_Number: $Page_Number\n";
		
		$nextPagePostContent =~s/<ViewState>/$ViewState/igs;
		$nextPagePostContent =~s/<Token>/$Token/igs;
		$nextPagePostContent =~s/<CSRFToken>/$CSRFToken/igs;
		
		$mech->post('https://myservice.erewash.gov.uk/Planning/lg/GFPlanningSearchResults.page', Content => "$nextPagePostContent");
		my $next_content = $mech->content;
		$content = $next_content;
		
		if($Number_Pages > $Page_Number)
		{
			$Page_Number++;
			
			goto NextPage;
		}
	}	
}

$insert_query=~s/\,$//igs;
if($insert_query!~m/values\s*$/is)
{
	print "insert_query::$insert_query\n";
	&DB_Insert($dbh,$insert_query);
}

sub Scrape_Details()
{
	my $Council_Code=shift;
	my $App_Page_Content=shift;
	my $Page_url=shift;
	
	my ($Application,$Address,$Agent_Name,$Agent_Telephone,$Agent_Address,$Applicant_Address,$Applicant_name,$Application_Status,$Application_type,$Date_Application_Received,$Date_Application_Registered,$Date_Application_validated,$Easting,$Northing,$Grid_reference,$Proposal,$TargetDec_dt,$Agent_Company_Name,$Doc_Url);
	
	$Application = &clean($1) if($App_Page_Content=~m/<td[^>]*?>\s*Application\s*Reference\s*ERE\s*\/?\s*<\/td>[^>]*?<td\s*class\=\"[^>]*?\>\s*([^>]*?)\s*<\/td><\/tr>/is);
	$Proposal = &clean($1)if( $App_Page_Content =~ m/Application\s*Description<\/td><td\s*[^>]*?\>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	$Date_Application_Received = &clean($1)if( $App_Page_Content =~ m/Received\s*Date<\/td>[^>]*?\>\s*([^>]*?)\s*<\/td>\s*<\/tr>/is);
	$Application_type = &clean($1)if( $App_Page_Content =~ m/Application\s*Type<\/td>[^>]*?\>\s*([^>]*?)\s*<\/td>\s*<\/tr>/is);
	$Address = &clean("$1$2$3$4$5")if( $App_Page_Content =~ m/House\s*No<\/td>([\w\W]*?)Address\s*Line\s*1<\/td>([\w\W]*?)Address\s*Line\s*2<\/td>([\w\W]*?)Address\s*Line\s*3<\/td>([\w\W]*?)Postcode<\/td>([\w\W]*?)Address\s*Status<\/td>/is);
	
	$Address=~s/\'/\'\'/igs;	
	$Proposal=~s/\'/\'\'/igs;	
	
	my $COUNCIL_NAME = 'Erewash';
	
	my $Application_URL = $Page_url;
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	my $Source = "GCS001";	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	my $query ="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Application\', \'$Address\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Applicant_name\', \'$Applicant_Address\', \'$Application_type\', \'$Proposal\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Agent_Company_Name\', \'$Agent_Address\', \'$Application_Status\', \'$Date_Application_Received\',  \'$TargetDec_dt\',  \'$Easting\',  \'$Northing\', \'$Application_URL\', \'$Doc_Url\', \'$Source_With_Time\',\'$Schedule_Date\'),";
	
	undef($Application); undef($Address); undef($Date_Application_Registered); undef($Date_Application_validated); undef($Applicant_name); undef($Applicant_Address); undef($Application_type); undef($Proposal); undef($Agent_Name); undef($Agent_Telephone); undef($Agent_Company_Name); undef($Agent_Address); undef($Application_Status); undef($Date_Application_Received); undef($TargetDec_dt); undef($Easting); undef($Northing); undef($Doc_Url);
	
	return $query;
}


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}