use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use File::Copy;
use FileHandle;

# Establish connection with DB server
my $dbh = &DbConnection();
my $Council_Code = '114';




# Location    
my $pgm_path="C:/Glenigan/Live_Schedule/Planning/Blaenau_Gwent/Program";
$pgm_path=~s/\s+$//igs;
my $archive_path=$pgm_path;
$archive_path=~s/Program$/Archive/igs;

# Cookies decleration
my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);

# User Agent decleration
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect('7');


# Write Cookies
my $file=$0."_cookie.txt";
my $cookie=HTTP::Cookies->new(file=>$file,autosave=>1,);
$ua->cookie_jar($cookie);

# Add Proxy
BEGIN {
 $ENV{HTTPS_PROXY} = 'https://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

my $file_name = "2018-Week45-PlanningApplications";
		
my $temp_file = $pgm_path.'\\'.$file_name;
$file_name=$pgm_path.'\\'.$file_name;
	

my $copy_res=copy("$temp_file.pdf",$archive_path);

if($copy_res ne "")
{
	# print "$copy_res\n";
	# <STDIN>;
	print "Downloaded file copied into Archive folder\n";#<STDIN>;
}

print "Download URL :: $file_name.pdf \n";#<STDIN>;


my $deillustrate_name="C:/Glenigan/Live_Schedule/Planning/Blaenau_Gwent/deillustrate.pl";
system("perl $deillustrate_name $temp_file.pdf $file_name.pdf ");

my $exe_name="C:/Glenigan/Live_Schedule/Planning/Blaenau_Gwent/pdftotext.exe -raw";
system("$exe_name $file_name.pdf");

my $html_file_pdf=$file_name.'.pdf';
$html_file_pdf=~s/\.pdf\s*$/\.txt/igs;

print "html_file_pdf :: $html_file_pdf\n";


&Scrape_Details($html_file_pdf,$Council_Code,'https://www.blaenau-gwent.gov.uk/fileadmin/documents/Resident/Planning/RecentApps/2018_planning_app/week_45.pdf');
		

sub Scrape_Details()
{
    my $html_content=shift;
    my $Council_Code=shift;  
    my $PDF_URL=shift;  
	
    my $time = Time::Piece->new;
    my $ScheduleDateTime= $time->strftime('%Y/%m/%d %H:%M');
    my $Source = "GCS001";  
    my $COUNCIL_NAME = "Blaenau Gwent";  
    my $Source_With_Time=$Source."_".$ScheduleDateTime."-perl";  
   		
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application, Date_Application_Registered, Applicant_name,Grid_Reference, Easting, Northing, Document_Url,PAGE_URL, Source, Schedule_Date) values ';

    open IN,"$html_content";
	my $App_content;
	while(<IN>)
	{
		$App_content.=$_;
	}
	
	my ($East_North,$Easting,$Northing,$Application_Number,$Date_Reg,$Location,$Proposal,$Applicant_name,$Received_Dt,$Application_Link,$Doc_Url);
	 
    while($App_content=~m/(C\/201\d+\/[\d\*]+[\w\W]+?E\:+\s*[\d\,\.]*\s*N\:+\s*[\d\,\.]*)/igs)
	{
		my $single_app=$1;
		my $single_app_temp=$single_app;
		
		if($single_app=~m/(C\/201\d+\/[\d\*]+)/is)
		{
			$Application_Number=$1;
			$Application_Number=trim($Application_Number);
		}
			
		if($single_app=~m/C\/201\d+\/[\d\*]+\s*(\d+\W\d+\W\d+)/is)
		{
			$Date_Reg=$1;
			$Date_Reg=trim($Date_Reg);
		}
		if($single_app_temp=~m/(?:Remove\/Vary\s*a\s*Condition|Temporary\s*Consent|Hazardous\s*Substances\s*Cons\.|Outline\s*Application|Full\s*Application|Retention\s*Application|Discharge\s*of\s*Conditions*|Advertisement\s*Consent|Non\s*Material\s*Amendment|Reserved\s*matters|Cons\.\s*from\s*Other\s*Authority|Lawful\s*Dev\.\s*Cert.\s*App|Resubmission|App\.\s*for\s*Demolition\s*only|Listed\s*Building\s*Consent)\s*\n+([\w\W]*?)E\:+\s*[\d\,\.]*\s*N\:+\s*[\d\,\.]*/is)
		{
			$Location=$1;
			$Location=trim($Location);
		}
		if($single_app_temp=~m/C\/201\d+\/[\d\*]+\s*\d+\W\d+\W\d+\s*[^<]*?\s*[^<]*?\s*\d+\W\d+\W\d+([\w\W]*?)\s*(?:Full\s*Application|Outline\s*Application|Retention\s*Application|Discharge\s*of\s*Conditions*|Advertisement\s*Consent|Temporary\s*Consent|Non\s*Material\s*Amendment|Reserved\s*matters|Cons\.\s*from\s*Other\s*Authority|Lawful\s*Dev\.\s*Cert.\s*App|Remove\/Vary\s*a\s*Condition|Resubmission|Listed\s*Building\s*Consent|App\.\s*for\s*Demolition\s*only|Hazardous\s*Substances\s*Cons\.)\s*\n+/is)
		{
			$Proposal=$1;
			$Proposal=trim($Proposal);
		}		
		elsif($single_app=~m/C\/201\d+\/[\d\*]+\s*\d+\W\d+\W\d+\s*[^<]*?\n*\s*([^<]+?)\n*\s*\d+\W\d+\W\d+/is)
		{
			$Applicant_name=$1;
			$Applicant_name=trim($Applicant_name);
		}
		if($single_app=~m/(E\:+\s*[\d\,\.]*\s*N\:+\s*[\d\,\.]*)/is)
		{
			$East_North=$1;
			if($East_North=~m/E\s*\:\s*?([^>]*?)\s+N\s*\:\s*?([^>]*)/is)
			{
				$Easting  = $1;
				$Northing = $2;
			}
		}
		
		$Received_Dt=$Date_Reg; # Please refer this mail on 30/01/2018 "Sub:Incorrect date of Application" [Request from data team - Gayathri Vadivel ]
		
		
		my $time 				= Time::Piece->new;
		my $Schedule_Date 		= $time->strftime('%m/%d/%Y %H:%M');
		

		$Location			=~s/\'/\'\'/igs;
		$Proposal			=~s/\'/\'\'/igs;
		$Application_Number	=~s/\'/\'\'/igs;
		$Received_Dt		=~s/\'/\'\'/igs;
		$Date_Reg			=~s/\'/\'\'/igs;
		$Applicant_name		=~s/\'/\'\'/igs;
		$East_North			=~s/\'/\'\'/igs;
		$Easting			=~s/\'/\'\'/igs;
		$Northing			=~s/\'/\'\'/igs;
		$PDF_URL			=~s/\'/\'\'/igs;
		
		
		$Application_Link=$PDF_URL;
		$Doc_Url=$PDF_URL;
		
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Location\', \'$Proposal\', \'$Received_Dt\', \'$Application_Number\', \'$Date_Reg\', \'$Applicant_name\', \'$East_North\',\'$Easting\',\'$Northing\', \'$Doc_Url\',\'$Application_Link\', \'$Source_With_Time\', \'$Schedule_Date\'),";
	
		undef $Application_Number;  undef $Location;  undef $Proposal; undef $Applicant_name;  undef $Received_Dt;	undef $Date_Reg;	undef $East_North;	undef $Easting;	undef $Northing; undef $Doc_Url;	undef $Application_Link; 
		
	}
	
	close(IN);
	
    $insert_query=~s/\,$//igs;
    
    print "insert_query::$insert_query\n";

    if($insert_query!~m/values\s*$/is)
    {
        &DB_Insert($dbh,$insert_query);
    }	 
}


sub trim()
{
	my $value=shift;
	
	$value=~s/<[^>]*?>//igs;
	$value=~s/\s+/ /igs;
	$value=~s/^\s*|\s*$//igs;
	
	return($value);
}

###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}



###### DB Connection ####
sub DbConnection()
{
    my $dsn	=	'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
    my $dbh	=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
   
    if(!$dbh)
    {
        &DBIconnect($dsn);
    }
    else
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}

#####GET Content####
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;	
	my $Content_Disposition=$res->header("Content-Disposition");
	
	my $content;
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if($rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if($rerun_count<= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}	


# SUBROUTINE TO GET THE POST HTML CONTENT
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;	
	my $rerun_count=0;
	
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->decoded_content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}