use strict;
use LWP::UserAgent;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use HTTP::Cookies;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' \(Eg: \"94\"\)", 16, "Error message");
    exit();
}

print "Council_Code: $Council_Code\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Flintshire_Planning/Flintshire_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";


#### UserAgent Declaration ####	
my $ua=LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 }, show_progress=>0);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect(0);
$ua->cookie_jar({});

my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile, autosave=>1); 
$ua->cookie_jar($cookie);


### Proxy settings ###
# if($Council_Code=~m/^(94)$/is)
# {
	# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');
# }


my $Content=&Getcontent($HOME_URL);


# open(PP, ">app_cont.html");
# print PP "$Content\n";
# close(PP); 
# exit;

&Search_Content( $Content);

sub Search_Content()
{
	my $Content = shift;
	
	my $i = 1;
	if($Content=~m/<table[^>]*?id=\"planning\">\s*<thead>\s*([\w\W]*?)\s*<\/table>/is)
	{		
		my $appContent = $1;
		
		my $time = Time::Piece->new;
	    my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
		
		my $insert_query= 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values ';
	
		while($appContent=~m/<td>\s*<a\s*href=\"([^\"]*?)\">\s*([^<]*?)\s*<\/a>\s*<\/td>/isg)
		{
			my $APPPAGE = $1;
			my $Application_number = $2;
			
			
			my $Page_url;
			if($APPPAGE!~m/^http/is)
			{
				$Page_url = "$FILTER_URL.$APPPAGE";
				# print "$Page_url\n"; <STDIN>;
			}
			
			my $App_content=&Getcontent($Page_url);
			
			# open(PP,">94_$i.html");
			# print PP "$App_content\n";
			# close(PP);
			# exit;
			
			$i++;
			
			print "Application_number: $Application_number\n";
			
			my ($Application,$Proposal,$Address,$Date_Application_Received,$Date_Application_validated,$Date_Application_Registered,$Agent_Address,$Applicant_Address,$Agent_Telephone,$TargetDec_dt,$Doc_Url,$Application_Link,$Agent_Email,$Application_Type,$Application_Status,$Agent_Name,$Applicant_name);
			
			$Application					= &clean($1) if ( $App_content =~ m/<dt>\s*Reference\s*Number\s*<\/dt>\s*<dd>\s*([^<]*?)\s*<\/dd>/is);
			$Proposal						= &clean($1) if ( $App_content =~ m/<dt>\s*Description\s*of\s*proposal\s*<\/dt>\s*<dd>\s*(.*?)\s*<\/dd>/is);
			
			if($App_content=~m/<dt>\s*Location\s*Address\s*2\s*<\/dt>\s*([\w\W]*?)\s*<dt>\s*Community\s*\/\s*Town\s*Council/is)
			{
				my $addr = $1;
				$addr=~s/<dt>\s*[^<]*?\s*<\/dt>//gsi;
				$addr=~s/<dd>\s*([^<]*?)\s*<\/dd>\s*/$1 /gsi;
				$addr=~s/\s+$//gsi;
				$addr=&clean($addr);
				$Address=$addr;
			}
			if($App_content=~m/<dt>\s*Applicant\s*Address\s*1\s*<\/dt>\s*([\w\W]*?)\s*<dt>\s*(Agent\s*Address\s*1\s*|Case\s*Officers\s*Name)/is)
			{
				my $AppAddr = $1;
				$AppAddr=~s/<dt>\s*[^<]*?\s*<\/dt>//gsi;
				$AppAddr=~s/<dd>\s*([^<]*?)\s*<\/dd>\s*/$1 /gsi;
				$AppAddr=~s/\s+$//gsi;
				$AppAddr=&clean($AppAddr);
				$Applicant_Address=$AppAddr;
			}
			if($App_content=~m/<dt>\s*Agent\s*Address\s*1\s*<\/dt>\s*([\w\W]*?)\s*<dt>\s*(Date\s*Valid|Case\s*Officers\s*Name)/is)
			{
				my $AgentAddr = $1;
				$AgentAddr=~s/<dt>\s*[^<]*?\s*<\/dt>//gsi;
				$AgentAddr=~s/<dd>\s*([^<]*?)\s*<\/dd>\s*/$1 /gsi;
				$AgentAddr=~s/\s+$//gsi;
				$AgentAddr=&clean($AgentAddr);
				$Agent_Address=$AgentAddr;
			}
			$Application_Status 			= &clean($1) if ( $App_content =~ m/<dt>\s*Status\s*<\/dt>\s*<dd>\s*([^<]*?)\s*</is);
			$Application_Type 				= &clean($1) if ( $App_content =~ m/<dt>\s*Application\s*Type\s*<\/dt>\s*<dd>\s*([^<]*?)\s*<\/dd>/is);
			$Date_Application_validated		= &clean($1) if ( $App_content =~ m/<dt>\s*Date\s*Valid\s*<\/dt>\s*<dd>\s*([^<]*?)\s*<\/dd>/is);
			$TargetDec_dt					= &clean($1) if ( $App_content =~ m/<dt>\s*Decision\s*Target\s*Date\s*<\/dt>\s*<dd>\s*([^<]*?)\s*<\/dd>/is);
			
			
			$Doc_Url=$Page_url;
			$Application_Link=$Page_url;
			
			$Address=~s/\'/\'\'/gsi;	
			$Proposal=~s/\'/\'\'/gsi;	
			my $Source = 'GCS001';	
			
			
			my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
			
			$insert_query .= "(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'), ";
			
			undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
			
		}
		
		$insert_query=~s/\,\s*$//igs;
			
		print "insert_query::$insert_query\n";

		if($insert_query!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
		}
	}
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}


sub Getcontent
{
	my $url = shift;
	
	my $rerun_count=0;
	my $redir_url;
	
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	$req->header("Accept-Encoding"=> "gzip, deflate, br");
	$req->header("Accept-Language"=> "en-US,en;q=0.5");
	$req->header("Connection"=> "keep-alive");
	$req->header("Host"=> $HOST);
	$req->header("Upgrade-Insecure-Requests"=> "1");
	
	my $res = $ua->request($req);
	my %res=%$res;
	
	my $headers=$res{'_headers'};
	my %headers=%$headers;

	my $Cookie=$headers{'set-cookie'};
	print "$Cookie";

	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	
	print "CODE :: $code\n";
	
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	
	return ($content);
}


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}