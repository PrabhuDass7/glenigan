#!/usr/bin/perl
use strict;
use WWW::Mechanize;
use URI::URL;
use URI::Escape;
use IO::Socket::SSL;
use Net::Ping;
use JSON;
use Time::Piece;
use DBI;
use DBD::ODBC;
use Win32; 
use Glenigan_DB_Windows;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}

my $time = Time::Piece->new;
my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');


my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"190\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();


my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
		);
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');
$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');

my $geturl = 'https://planning.eastleigh.gov.uk/s/public-register';
$mech->get($geturl);
my $getcontents = $mech->content;

# open(PP, ">getcontents.html");
# print PP "$getcontents";
# close(PP);

my $fwuid = $1 if($getcontents=~m/fwuid\%22\%3A\%22([^<]*?)\%22/is); 
my $siteforce = $1 if($getcontents=~m/siteforce\%3A([^<]*?)\%22/is); 
my $markup = $1 if($getcontents=~m/markup\%3A\%2F\%2Fsiteforce\%3A$siteforce\%22\%3A\%22([^<]*?)\%22/is); 

my $searchurl = "https://planning.eastleigh.gov.uk/s/sfsites/aura?r=4&other.LCPublicRegCont.advancedSearch=1";

my $dupFrom_Date;
my $dupTo_Date;
if($From_Date=~m/^(\d+)\/(\d+)\/(\d+)$/is)
{
	$dupFrom_Date = $3."-".$2."-".$1;
}
if($To_Date=~m/^(\d+)\/(\d+)\/(\d+)$/is)
{
	$dupTo_Date = $3."-".$2."-".$1;
}

print "dupFrom_Date: $dupFrom_Date\n";
print "dupTo_Date: $dupTo_Date\n";

my $searchpostcnt = "message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22240%3Ba%22%2C%22descriptor%22%3A%22apex%3A%2F%2FLCPublicRegCont%2FACTION%24advancedSearch%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2Fc%3APubRegAdvSearch%22%2C%22params%22%3A%7B%22keywords%22%3A%22%22%2C%22ward%22%3A%22%22%2C%22parish%22%3A%22%22%2C%22determination%22%3A%22%22%2C%22undecidedOnly%22%3A%22%22%2C%22recordTypeName%22%3A%22%22%2C%22dateRecFrom%22%3A%22$dupFrom_Date%22%2C%22dateRecTo%22%3A%22$dupTo_Date%22%2C%22dateDecFrom%22%3A%22%22%2C%22dateDecTo%22%3A%22%22%7D%2C%22version%22%3Anull%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwuid%22%2C%22app%22%3A%22siteforce%3A$siteforce%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fsiteforce%3A$siteforce%22%3A%22$markup%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fs%2Fpublic-register&aura.token=undefined";


$mech->post($searchurl, content => $searchpostcnt);
my $responseCode = $mech->status;
my $searchresult = $mech->content;

print "ResponseCode::$responseCode\n";

# open(PP, ">contents.html");
# print PP "$searchresult";
# close(PP);
# exit;

my $json = JSON->new;
my $data = $json->decode($searchresult);


my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values ';


my $actions= $data->{'actions'}->[0]->{'returnValue'}->{'arcusbuilt__PApplication__c'};
my $fwid= $data->{'context'}->{'fwuid'};
my $napiliApp= $data->{'context'}->{'loaded'}->{'APPLICATION@markup://siteforce:napiliApp'};


foreach my $val ( @$actions ) 
{
	my $Id = $val->{'Id'};
	
	my $POST_DETAIL_URL = "https://planning.eastleigh.gov.uk/s/sfsites/aura?r=7&other.LC_BE_Lightning_.getPlanAppFromId=1&other.LC_BE_Lightning_.getPlanningConsultees=1";
	my $POST_DETAIL_CNT = "message=%7B%22actions%22%3A%5B%7B%22id%22%3A%22783%3Ba%22%2C%22descriptor%22%3A%22apex%3A%2F%2FLC_BE_Lightning_Controller%2FACTION%24getPlanningConsultees%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2Fc%3ABE_PL_Pub_Reg_Detail%22%2C%22params%22%3A%7B%22planningAppId%22%3A%22$Id%22%7D%2C%22version%22%3Anull%7D%2C%7B%22id%22%3A%22784%3Ba%22%2C%22descriptor%22%3A%22apex%3A%2F%2FLC_BE_Lightning_Controller%2FACTION%24getPlanAppFromId%22%2C%22callingDescriptor%22%3A%22markup%3A%2F%2Fc%3ABE_PL_Pub_Reg_Detail%22%2C%22params%22%3A%7B%22recordID%22%3A%22$Id%22%7D%2C%22version%22%3Anull%7D%5D%7D&aura.context=%7B%22mode%22%3A%22PROD%22%2C%22fwuid%22%3A%22$fwid%22%2C%22app%22%3A%22siteforce%3AnapiliApp%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fsiteforce%3AnapiliApp%22%3A%22$napiliApp%22%7D%2C%22dn%22%3A%5B%5D%2C%22globals%22%3A%7B%7D%2C%22uad%22%3Afalse%7D&aura.pageURI=%2Fs%2Fpublic-register&aura.token=undefined";

	
	$mech->post( $POST_DETAIL_URL, Content => "$POST_DETAIL_CNT");
						
	my $Code_status = $mech->status;
	my $detailContent = $mech->content;
	
	# open(PP, ">app_details_cont.html");
	# print PP "$detailContent\n";
	# close(PP); 
	# exit;
	
	my ($Application,$Address,$Proposal,$Case_Officer,$Application_Status,$Date_Application_Received,$Date_Application_validated,$Date_Application_Registered1,$Date_Application_Registered,$Applicant_name,$Applicant_Address,$Agent_Name,$Agent_Address,$Agent_Telephone,$Application_Type,$Doc_Url,$Application_Link,$TargetDec_dt,$Agent_Email);

	my $detailData = $json->decode($detailContent);
	my $application= $detailData->{'actions'}->[1]->{'returnValue'}->{'application'};
	
	foreach my $appdetail ( @$application ) 
	{
		$Application = &clean($appdetail->{'Name'});
		$Proposal = &clean($appdetail->{'arcusbuilt__Proposal__c'});
		$Date_Application_Received = &clean($appdetail->{'arcusbuilt__ReceivedDate__c'});
		$Date_Application_Registered = &clean($appdetail->{'arcusbuilt__Registration_Complete_Date__c'});
		$Date_Application_Registered1 = &clean($appdetail->{'arcusbuilt__Registration_Date__c'});
		$Date_Application_validated = &clean($appdetail->{'arcusbuilt__Validation_Date__c'});
		$Address = &clean($appdetail->{'arcusbuilt__SiteAddress__c'});
		$Application_Status = &clean($appdetail->{'Portal_Status__c'});
		$Application_Type = &clean($appdetail->{'arcusbuilt__PS_Type__c'});
		$Agent_Name = &clean($appdetail->{'Agent_Name__c'});
		$Applicant_name = &clean($appdetail->{'Applicant_Name__c'});
		$Agent_Address = &clean($appdetail->{'Agent_Address__c'});
		$Applicant_Address = &clean($appdetail->{'Applicant_Address__c'});
		
		if($Date_Application_Registered=~m/^\s*$/is)
		{
			if($Date_Application_Registered1!~m/^\s*$/is)
			{
				$Date_Application_Registered = $Date_Application_Registered1;
			}
		}
		
		$Application_Link = 'https://planning.eastleigh.gov.uk/s/public-register';
		
		print "Application=>$Application\n";
		
		my $COUNCIL_NAME = "Eastleigh";	
		my $Source = $Date_Range;	
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";	
		
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
		
		# print "$insert_query\n";
		# <STDIN>;
				
		$Application=""; $Address=""; $Proposal=""; $Application_Status=""; $Date_Application_validated=""; $Date_Application_Registered=""; $Applicant_name=""; $Applicant_Address=""; $Agent_Name=""; $Agent_Address=""; $Agent_Telephone=""; $Date_Application_Received=""; $TargetDec_dt=""; $Doc_Url=""; $Application_Link=""; $Agent_Email=""; $Application_Type=""; $Source_With_Time="";
		
	}

}
$insert_query=~s/\,\s*$//igs;
	

if($insert_query!~m/values\s*$/is)
{
	print "insert_query::$insert_query\n";
	# &DB_Insert($dbh,$insert_query);
	&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}