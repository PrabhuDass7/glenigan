use strict;
use DBI;
use DBD::ODBC;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;



# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $input_from_date = $ARGV[1];
my $input_to_date = $ARGV[2];
my $COUNCIL_NAME = "Eastleigh";


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


############
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");

#### COOKIE SETTING
my $cookie_jar=HTTP::Cookies->new(file=>$0."_Cookie.txt",autosave=>1);                          
$ua->cookie_jar($cookie_jar);


my $Applicant_Flag = 'Y';


#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();


my $home_url = "https://planning.eastleigh.gov.uk/s/";
$driver->get("$home_url");
sleep(10);
my $model_cont=$driver->get_page_source($driver);

$driver->find_element_by_xpath("//*[\@id=\"NapiliCommunityTemplate\"]/div[2]/div/div[2]/div[1]/div/div/div/div[1]/p[1]/strong/a")->click;
sleep(10);	
my $temp1=$driver->get_page_source($driver);

my $advSearch = $1 if($temp1=~m/<a[^>]*?id=\"([^\"]*?)\"[^>]*?>\s*Advanced\s*Search\s*<\/a>\s*<\/li>/is);

$driver->find_element_by_xpath("//*[\@id=\"$advSearch\"]")->click;
sleep(5);	
my $temp2=$driver->get_page_source($driver);

my $fromDate = $1 if($temp2=~m/<span[^>]*?>\s*Date\s*Received\s*From\s*<\/span>\s*<\!\-\-[^>]*?\-\->\s*<\!\-\-[^>]*?\-\->\s*<\/label>\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>/is);
my $toDate = $1 if($temp2=~m/<span[^>]*?>\s*Date\s*Received\s*To\s*<\/span>\s*<\!\-\-[^>]*?\-\->\s*<\!\-\-[^>]*?\-\->\s*<\/label>\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>/is);

$driver->find_element("//*[\@id=\"$fromDate\"]")->send_keys("$input_from_date");
sleep(5);

$driver->find_element("//*[\@id=\"$toDate\"]")->send_keys("$input_to_date");
sleep(5);	

$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
sleep(10);	

my $model_content=$driver->get_page_source($driver);

# open FH, ">Home.html";
# print FH $model_content;
# close FH;
# exit;

# Create insert query for required columns
my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_NEW (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date) values';

my $total_count = $1 if($model_content=~m/Planning\s+Applications\s+\((\d+)\)<\/h2>/si);
my $click_count = $total_count + 1;
my $a=1;
while($model_content=~m/<h4[^>]*?>\s*<a[^>]*?>([^>]*?)\s+\-\s*([^<]*?)<\/a>\s*<\/h4>/gsi)
{
	my $Application = $1;
	print "${a}::::$total_count\n";
	
	print "\n\nApplication Number is Processing::$Application\n\n";
	
	if($a<=$total_count)
	{		
		if($a==6)
		{			
			$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$click_count]/a")->click;
			sleep(5);				
		}
		
		$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$a]/div/h4/a")->click;
		sleep(10);	
		
		my $current_URL = $driver->get_current_url();
		# print "$current_URL\n";
		
		my $content=$driver->get_page_source($driver);
				
		# open FH, ">Output-$a.html";
		# print FH $content;
		# close FH;
		# exit;
		
		my $time = Time::Piece->new;
		my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
		
		
		# print "Council_Code: $Council_Code\n";
		
		my ($Actual_Decision_Level,$Agent_Company_Name,$Agent_Telephone,$Agent_Email,$Agent_Telephone_1,$Agent_mobile,$Agent_Fax,$agent_contact_details,$Actual_Committee_Date,$Actual_Committee_or_Panel_Date,$Advertisement_Expiry_Date,$Agreed_Expiry_Date,$Application_Expiry_Deadline,$TargetDec_dt,$Temporary_Permission_Expiry_Date,$Application_Link,$Document_Url,$No_document,$Source_With_Time);
		
		
		my $Application_Number = &clean($1) if($content=~m/<label[^>]*?>\s*Planning\s*Application\s*Number\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Application_Status = &clean($1) if($content=~m/<label[^>]*?>\s*Application\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Application_Type = &clean($1) if($content=~m/<label[^>]*?>\s*Application\s*Type\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Address = &clean($1) if($content=~m/<label[^>]*?>\s*Site\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_Received = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Received\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_Registered = &clean($1) if($content=~m/<label[^>]*?>\s*Registration\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_Registered1 = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Registration\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_validated = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Valid\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Applicant_Name = &clean($1) if($content=~m/<label[^>]*?>\s*Applicant\s*Name\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Name = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Name\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Email = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Email\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Telephone = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Telephone\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Address = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Applicant_Address = &clean($1) if($content=~m/<label[^>]*?>\s*Applicant\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal = &clean($1) if($content=~m/<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal1 = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal2 = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
		my $Proposal3 = &clean($1) if($content=~m/<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
		my $TargetDec_dt = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		
		$Address=~s/\'/\'\'/gsi;
		$Applicant_Address=~s/\'/\'\'/gsi;
		$Applicant_Name=~s/\'/\'\'/gsi;
		$Agent_Address=~s/\'/\'\'/gsi;	
	
		if($Application_Number=~m/^\s*$/is)
		{
			if($Application!~m/^\s*$/is)
			{
				$Application_Number = $Application;
			}
		}
		if($Date_Application_Registered=~m/^\s*$/is)
		{
			if($Date_Application_Registered1!~m/^\s*$/is)
			{
				$Date_Application_Registered = $Date_Application_Registered1;
			}
		}
		if($Proposal=~m/^\s*$/is)
		{
			if($Proposal1!~m/^\s*$/is)
			{
				$Proposal = $Proposal1;
			}
			elsif($Proposal2!~m/^\s*$/is)
			{	
				$Proposal = $Proposal2;
			}
			elsif($Proposal3!~m/^\s*$/is)
			{	
				$Proposal = $Proposal3;
			}
		}
		
		my $Council_Code = "190";
		my $COUNCIL_NAME = "Eastleigh";
		my $Application_Link = "";
		my $Doc_Url = "";
		my $Page_url = $current_URL;
		
		
		my $Source = "GCS001";	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$Application_Link = $Page_url;
		$Document_Url=$Application_Link;
		
		# Create insert query for required columns
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Date_Application_Received\', \'$Application_Number\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Proposal\', \'$Application_Status\', \'$Actual_Decision_Level\', \'$Agent_Address\', \'$Agent_Company_Name\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Applicant_Address\', \'$Applicant_Name\', \'$Application_Type\', \'$Agent_Email\', \'$Agent_Telephone_1\', \'$Agent_mobile\', \'$Agent_Fax\', \'$agent_contact_details\', \'$Actual_Committee_Date\', \'$Actual_Committee_or_Panel_Date\', \'$Advertisement_Expiry_Date\', \'$Agreed_Expiry_Date\', \'$Application_Expiry_Deadline\', \'$TargetDec_dt\', \'$Temporary_Permission_Expiry_Date\', \'$Application_Link\', \'$Document_Url\', \'$No_document\', \'$Source_With_Time\', \'$Schedule_Date\'),";
		
		undef $Address; undef $Date_Application_Received; undef $Application_Number; undef $Date_Application_Registered; undef $Date_Application_validated; undef $Proposal; undef $Application_Status; undef $Actual_Decision_Level; undef $Agent_Address; undef $Agent_Company_Name; undef $Agent_Name; undef $Agent_Telephone; undef $Applicant_Address; undef $Applicant_Name; undef $Application_Type; undef $Agent_Email; undef $Agent_Telephone_1; undef $Agent_mobile; undef $Agent_Fax; undef $agent_contact_details; undef $Actual_Committee_Date; undef $Actual_Committee_or_Panel_Date; undef $Advertisement_Expiry_Date; undef $Agreed_Expiry_Date; undef $Application_Expiry_Deadline; undef $TargetDec_dt; undef $Temporary_Permission_Expiry_Date; undef $Application_Link; undef $Document_Url; undef $No_document; undef $Schedule_Date; undef $Source_With_Time;
				
		
		$a++;
		
		$driver->find_element_by_xpath("//*[\@id=\"NapiliCommunityTemplate\"]/div[2]/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[2]/div/button[1]")->click;
		sleep(7);	
		
	}
	else
	{
		print "\nEastleigh Completed\n\n";
	}
}
$insert_query=~s/\,$//igs;

print "insert_query::$insert_query\n";

if($insert_query!~m/values\s*$/is)
{
	&DB_Insert($dbh,$insert_query);
}


sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}