use strict;
use DBI;
use DBD::ODBC;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Text::CSV;
use XML::CSV;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;



# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $input_from_date = $ARGV[1];
my $input_to_date = $ARGV[2];
my $COUNCIL_NAME = "Eastleigh";

# my $Status_query="insert into TBL_SCRAP_STATUS (SCHEDULE_NO, SCHEDULE_DATE, GCS_NAME, COUNCIL_CODE, STATUS, PLANNING_OR_DECISION, COUNCIL_NAME) values (\'$Schedule_no\',GETDATE(),\'\',\'$Council_Code\',\'Running\',\'Planning\',\'$COUNCIL_NAME\')";

# &DB_Insert($dbh,$Status_query);


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


############
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");

#### COOKIE SETTING
my $cookie_jar=HTTP::Cookies->new(file=>$0."_Cookie.txt",autosave=>1);                          
$ua->cookie_jar($cookie_jar);

my $csv_filename="Eastleigh_Output.csv";
my $xml_filename="Eastleigh_Output.xml";

my $Applicant_Flag = 'Y';
my $ID = 1;


#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();

open my $fh, ">", "$csv_filename" or die "Failed to open file: $!";
$csv->print($fh,["ID","Application","PAGE_URL","Council_Code","Council_Name","Proposal","Address","Date_Application_Received","Date_Application_validated","Date_Application_Registered","Applicant_name","Applicant_Address","Application_Type","Application_Status","Applicant_Flag","Application_Link","Agent_Name","Agent_Address","Agent_Email","Agent_Telephone","Document_Url","TargetDec_dt"]);
close $fh;


my $home_url = "https://planning.eastleigh.gov.uk/s/";
$driver->get("$home_url");
sleep(10);
my $model_cont=$driver->get_page_source($driver);

# open (CO,">Output.txt");
# print CO "Council_Code\tCOUNCIL_NAME\tAddress\tPage_url\tProposal\tDate_Application_Received\tApplication_Type\tApplication\tDate_Application_validated\tDate_Application_Registered\tApplication_Status\tAgent_Telephone\tAgent_Email\tAgent_Name\tAgent_Address\tApplicant_name\tApplicant_Address\tTargetDec_dt\tApplication_Link\tDoc_Url\n";
# close CO;

# open FH, ">Eastleigh.html";
# print FH $model_cont;
# close FH;
# exit;
# //*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div[1]/div/div/div/div[1]/p[1]/strong/a
$driver->find_element_by_xpath("//*[\@id=\"NapiliCommunityTemplate\"]/div[2]/div/div[2]/div[1]/div/div/div/div[1]/p[1]/strong/a")->click;
sleep(10);	
# //*[@id="362:0__item"]
$driver->find_element_by_xpath("//*[\@id=\"362:0__item\"]")->click;
sleep(5);	

# $driver->find_element("//*[\@id=\"735:0\"]")->send_keys("$input_from_date");
$driver->find_element("//*[\@id=\"749:0\"]")->send_keys("$input_from_date");
sleep(5);
# $driver->find_element("//*[\@id=\"751:0\"]")->send_keys("$input_to_date");
$driver->find_element("//*[\@id=\"765:0\"]")->send_keys("$input_to_date");
sleep(5);	

my $contents;
$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
# $driver->find_element("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->send_keys('submit');
sleep(10);	

my $model_content=$driver->get_page_source($driver);

# open FH, ">Home.html";
# print FH $model_content;
# close FH;
# exit;

# Create insert query for required columns
my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_NEW_Staging (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date) values';

my $total_count = $1 if($model_content=~m/Planning\s+Applications\s+\((\d+)\)<\/h2>/si);
my $click_count = $total_count + 1;
my $a=1;
while($model_content=~m/<h4[^>]*?>\s*<a[^>]*?>([^>]*?)\s+\-\s*([^<]*?)<\/a>\s*<\/h4>/gsi)
{
	my $Application = $1;
	print "${a}::::$total_count\n";
	
	print "\n\nApplication Number is Processing::$Application\n\n";
	
	if($a<=$total_count)
	{
		if($a >= 6)
		{
			$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$click_count]/a")->click;
		}
		
		# my $Ping_Status1;
		# if ($@) 
		# {
			# $Ping_Status1 = $@;
		# };	
		
		# print "Ping_Status::$Ping_Status1\n"; <STDIN>;
		# if($Ping_Status1!~m/^\s*(?:200|ok)\s*$/is)
		# {
			# my $Search_Results_Count = $total_count;
			
			# my $Status_Update_Query="update TBL_SCRAP_STATUS set Status='Terminated', Searched_Count=$Search_Results_Count where COUNCIL_CODE=$Council_Code and Schedule_no=$Schedule_no and convert(nvarchar, schedule_date, 101)=convert(nvarchar, getdate(), 101) and Status='Running'";
			
			# &DB_Insert($dbh,$Status_Update_Query);
		# }
		
		$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$a]/div/h4/a")->click;
		sleep(10);	
		
		my $current_URL = $driver->get_current_url();
		print "$current_URL\n";
		
		my $content=$driver->get_page_source($driver);
				
		# open FH, ">Output-$a.html";
		# print FH $content;
		# close FH;
		# exit;
		my $time = Time::Piece->new;
		my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
		
		
		print "Council_Code: $Council_Code\n";
		
		my ($Actual_Decision_Level,$Agent_Company_Name,$Agent_Telephone,$Agent_Email,$Agent_Telephone_1,$Agent_mobile,$Agent_Fax,$agent_contact_details,$Actual_Committee_Date,$Actual_Committee_or_Panel_Date,$Advertisement_Expiry_Date,$Agreed_Expiry_Date,$Application_Expiry_Deadline,$TargetDec_dt,$Temporary_Permission_Expiry_Date,$Application_Link,$Document_Url,$No_document,$Source_With_Time);
		
		
		my $Application_Number = &clean($1) if($content=~m/<label[^>]*?>\s*Planning\s*Application\s*Number\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Application_Status = &clean($1) if($content=~m/<label[^>]*?>\s*Application\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Application_Type = &clean($1) if($content=~m/<label[^>]*?>\s*Application\s*Type\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Address = &clean($1) if($content=~m/<label[^>]*?>\s*Site\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_Received = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Received\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_Registered = &clean($1) if($content=~m/<label[^>]*?>\s*Registration\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_Registered1 = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Registration\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Application_validated = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Valid\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Applicant_Name = &clean($1) if($content=~m/<label[^>]*?>\s*Applicant\s*Name\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Name = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Name\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Email = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Email\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Telephone = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Telephone\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Agent_Address = &clean($1) if($content=~m/<label[^>]*?>\s*Agent\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Applicant_Address = &clean($1) if($content=~m/<label[^>]*?>\s*Applicant\s*Address\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal = &clean($1) if($content=~m/<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal1 = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal2 = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
		my $Proposal3 = &clean($1) if($content=~m/<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
		my $TargetDec_dt = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		
		$Address=~s/\'/\'\'/gsi;
		$Applicant_Address=~s/\'/\'\'/gsi;
		$Applicant_Name=~s/\'/\'\'/gsi;
		$Agent_Address=~s/\'/\'\'/gsi;	
	
		if($Application_Number=~m/^\s*$/is)
		{
			if($Application!~m/^\s*$/is)
			{
				$Application_Number = $Application;
			}
		}
		if($Date_Application_Registered=~m/^\s*$/is)
		{
			if($Date_Application_Registered1!~m/^\s*$/is)
			{
				$Date_Application_Registered = $Date_Application_Registered1;
			}
		}
		if($Proposal=~m/^\s*$/is)
		{
			if($Proposal1!~m/^\s*$/is)
			{
				$Proposal = $Proposal1;
			}
			elsif($Proposal2!~m/^\s*$/is)
			{	
				$Proposal = $Proposal2;
			}
			elsif($Proposal3!~m/^\s*$/is)
			{	
				$Proposal = $Proposal3;
			}
		}
		
		my $Council_Code = "190";
		my $COUNCIL_NAME = "Eastleigh";
		my $Application_Link = "";
		my $Doc_Url = "";
		my $Page_url = $current_URL;
		
		# open (CO,">>Output.txt");
		# print CO "$Council_Code\t$COUNCIL_NAME\t$Address\t$Page_url\t$Proposal\t$Date_Application_Received\t$Application_Type\t$Application\t$Date_Application_validated\t$Date_Application_Registered\t$Application_Status\t$Agent_Telephone\t$Agent_Email\t$Agent_Name\t$Agent_Address\t$Applicant_name\t$Applicant_Address\t$TargetDec_dt\t$Application_Link\t$Doc_Url\n";
		# close CO;
		
		
		open my $fh, ">>", "$csv_filename" or die "Failed to open file: $!";
		$csv->print($fh,["$ID","$Application","$Page_url","$Council_Code","$COUNCIL_NAME","$Proposal","$Address","$Date_Application_Received","$Date_Application_validated","$Date_Application_Registered","$Applicant_Name","$Applicant_Address","$Application_Type","$Application_Status","$Applicant_Flag","$Application_Link","$Agent_Name","$Agent_Address","$Agent_Email","$Agent_Telephone","$Doc_Url","$TargetDec_dt"]);
		close $fh;	
		
		my $Source = "GCS001";	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$Application_Link = $Page_url;
		$Document_Url=$Application_Link;
		
		# Create insert query for required columns
		my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_NEW (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Date_Application_Received\', \'$Application_Number\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Proposal\', \'$Application_Status\', \'$Actual_Decision_Level\', \'$Agent_Address\', \'$Agent_Company_Name\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Applicant_Address\', \'$Applicant_Name\', \'$Application_Type\', \'$Agent_Email\', \'$Agent_Telephone_1\', \'$Agent_mobile\', \'$Agent_Fax\', \'$agent_contact_details\', \'$Actual_Committee_Date\', \'$Actual_Committee_or_Panel_Date\', \'$Advertisement_Expiry_Date\', \'$Agreed_Expiry_Date\', \'$Application_Expiry_Deadline\', \'$TargetDec_dt\', \'$Temporary_Permission_Expiry_Date\', \'$Application_Link\', \'$Document_Url\', \'$No_document\', \'$Source_With_Time\', \'$Schedule_Date\')";
		
		undef $Address; undef $Date_Application_Received; undef $Application_Number; undef $Date_Application_Registered; undef $Date_Application_validated; undef $Proposal; undef $Application_Status; undef $Actual_Decision_Level; undef $Agent_Address; undef $Agent_Company_Name; undef $Agent_Name; undef $Agent_Telephone; undef $Applicant_Address; undef $Applicant_Name; undef $Application_Type; undef $Agent_Email; undef $Agent_Telephone_1; undef $Agent_mobile; undef $Agent_Fax; undef $agent_contact_details; undef $Actual_Committee_Date; undef $Actual_Committee_or_Panel_Date; undef $Advertisement_Expiry_Date; undef $Agreed_Expiry_Date; undef $Application_Expiry_Deadline; undef $TargetDec_dt; undef $Temporary_Permission_Expiry_Date; undef $Application_Link; undef $Document_Url; undef $No_document; undef $Schedule_Date; undef $Source_With_Time;
		
		
		print "insert_query::$insert_query\n";

		if($insert_query!~m/values\s*$/is)
		{
			&DB_Insert($dbh,$insert_query);
		}

		
		$driver->get("https://planning.eastleigh.gov.uk/s/public-register");
		sleep(15);
		
		eval{$driver->find_element_by_xpath("//*[\@id=\"352:0__item\"]")->click;};
		sleep(5);	
		
		my $model_content=$driver->get_page_source($driver);

		if($model_content eq "")
		{
			$driver->find_element_by_xpath("//*[\@id=\"346:0__item\"]")->click;
			sleep(5);
			
			# $driver->find_element("//*[\@id=\"735:0\"]")->send_keys("$input_from_date");
			$driver->find_element("//*[\@id=\"749:0\"]")->send_keys("$input_from_date");
			sleep(5);
			# $driver->find_element("//*[\@id=\"751:0\"]")->send_keys("$input_to_date");
			$driver->find_element("//*[\@id=\"765:0\"]")->send_keys("$input_to_date");
			sleep(5);	

			$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
			sleep(5);
			
		}
		else
		{
			# $driver->find_element("//*[\@id=\"735:0\"]")->send_keys("$input_from_date");
			$driver->find_element("//*[\@id=\"749:0\"]")->send_keys("$input_from_date");
			sleep(5);
			# $driver->find_element("//*[\@id=\"751:0\"]")->send_keys("$input_to_date");
			$driver->find_element("//*[\@id=\"765:0\"]")->send_keys("$input_to_date");
			sleep(5);	

			$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
			sleep(5);
		}
		
		$a++;
		$ID++;
		
	}
	else
	{
		print "\nEastleigh Completed\n\n";
	}
}

my $csv_obj = XML::CSV->new();
$csv_obj->parse_doc("$csv_filename", {headings => 1});
$csv_obj->declare_xml({version => '1.0', encoding => 'Windows-1252'}); 
$csv_obj->print_xml("$xml_filename",
		{file_tag    => 'Items',
		parent_tag  => 'Item'});

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}