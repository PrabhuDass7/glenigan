use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();

my $Council_Code = $ARGV[0];

my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://services.boston.gov.uk/agile/searchResults.aspx";
$mech->add_header("Host"=>"services.boston.gov.uk");
$mech->add_header("Referer"=>"https://services.boston.gov.uk/agile/?q=/planning-application-search/");
$mech->post($searchURL, Content=>'inputSearch=%25');

my $SearchResultContent = $mech->content;

my $tableCnt = $1 if($SearchResultContent=~m/<table[^>]*?id=\"results\"[^>]*?>[\w\W]*?<\/thead>\s*<tbody>\s*([\w\W]*?)\s*<\/table>/is);

# print "$tableCnt";

my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Application, Address, Date_Application_Registered, Date_Application_validated, Applicant_name, Applicant_Address, Application_type, Proposal, Agent_Name, Agent_Telephone, Agent_Company_Name, Agent_Address, Application_Status, Date_Application_Received, TargetDec_dt, Easting, Northing, PAGE_URL, Document_Url, Source, Schedule_Date) values';

my $Page_Number=1;
NextPage:
while($tableCnt=~m/<tr[^>]*?>\s*<td>\s*([^<]*?)\s*<\/td>[\w\W]*?<\/tr>/igs)
{
	my $Application_Num=$1;
	next if($Application_Num=~m/^Ref$/is);
	print "Application_Num==>$Application_Num\n";
	
	my $Application_URL="https://services.boston.gov.uk/agile/planApp.aspx?ref=<APPNUM>";
	$Application_URL=~s/<APPNUM>/$Application_Num/is;
	
	
	$mech->get( $Application_URL );
	my $Code_status = $mech->status;
	my $AppCnt = $mech->content;
	
	my ($Application,$Address,$Agent_Name,$Agent_Telephone,$Agent_Address,$Applicant_Address,$Applicant_name,$Application_Status,$Application_type,$Date_Application_Received,$Date_Application_Registered,$Date_Application_validated,$Easting,$Northing,$Grid_reference,$Proposal,$TargetDec_dt,$Agent_Company_Name,$Doc_Url);
	
	$Application=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Reference\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Address=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Location\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Applicant_name=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Applicant\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Agent_Name=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Agent\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Date_Application_Received=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Received\s*On\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Proposal=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Proposal\s*<\/th>\s*<td>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	$Application_type=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Type\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Doc_Url=$Application_URL;
	
	if($Application_Num ne "")
	{
		$Application = $Application_Num;
	}
	
	$Applicant_name=~s/\'/\'\'/igs;	
	$Agent_Name=~s/\'/\'\'/igs;	
	$Address=~s/\'/\'\'/igs;	
	$Agent_Address=~s/\'/\'\'/igs;	
	$Applicant_Address=~s/\'/\'\'/igs;	
	
	my $COUNCIL_NAME = 'Boston';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	my $Source = "GCS001";	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Application\', \'$Address\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Applicant_name\', \'$Applicant_Address\', \'$Application_type\', \'$Proposal\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Agent_Company_Name\', \'$Agent_Address\', \'$Application_Status\', \'$Date_Application_Received\',  \'$TargetDec_dt\',  \'$Easting\',  \'$Northing\', \'$Application_URL\', \'$Doc_Url\', \'$Source_With_Time\',\'$Schedule_Date\'),";
	
	undef($Application); undef($Address); undef($Date_Application_Registered); undef($Date_Application_validated); undef($Applicant_name); undef($Applicant_Address); undef($Application_type); undef($Proposal); undef($Agent_Name); undef($Agent_Telephone); undef($Agent_Company_Name); undef($Agent_Address); undef($Application_Status); undef($Date_Application_Received); undef($TargetDec_dt); undef($Easting); undef($Northing); undef($Doc_Url);
	
}

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
print "insert_query: $insert_query\n";


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}