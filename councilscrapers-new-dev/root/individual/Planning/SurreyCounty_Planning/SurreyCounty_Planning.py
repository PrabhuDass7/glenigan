# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Merit/Projects/Individual_councils'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, driver):
	try:
		#-- Parse
		driver.get('https://planning.surreycc.gov.uk/planappsearch.aspx')
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		finalCnt=''
		if len(content) > 0:
			finalCnt = content.encode("utf-8")
		
		# print advfinalCnt

		
		fromDate = '//*[@id="ContentPlaceHolder1_txtAppValFrom"]'
 
		toDate = '//*[@id="ContentPlaceHolder1_txtAppValTo"]'
		
		driver.find_element_by_xpath(fromDate).send_keys(dumFrom_Date)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath(toDate).send_keys(dumTo_Date)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnSearch"]').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		# with open("190_out.html", 'wb') as fd:
			# fd.write(searchfinalCnt)
		# raw_input()
		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Application parse section	
def parsesection(searchcontent, CouncilCode, Source, conn, cursor, driver):
	try:
		searchcount = re.findall("Your\s*search\s*has\s*returned\s*(\d+)\s*application", str(searchcontent), re.IGNORECASE)  
		print "searchcount:", int(searchcount[0])
		totalcount = int(searchcount[0])
		print "Totalcount", totalcount
		
		pageNum = re.findall("Currently\s*showing\s*page\s*1\s*of\s*(\d+)", str(searchcontent), re.IGNORECASE)  
		print "pageNumCount:", pageNum

		a = 1
		
		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Northing, Easting) values '
				
		bulkValuesForQuery=''

		if re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>'+str(pageNum)+'<\/a>', str(searchcontent)):
			check_next_page = re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>('+str(pageNum)+')<\/a>', str(searchcontent), re.I)
			print check_next_page
			print type(check_next_page)
			
			pageNumber = 1
			nextPageNumber = 0
			while (check_next_page):

				appCount = 1
				b = 2
				
				if pageNumber <= int(check_next_page[0]):
					appcollection=[]
					if pageNumber >= 2:	
						print("For next page click")		
						nextPageNumber = nextPageNumber + 1	
						print "CurrentPage::",nextPageNumber
						driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_grdResults"]/tbody/tr[42]/td/div/a['+str(nextPageNumber)+']').click()
						time.sleep(4)  
						searchcontent=driver.page_source		
						appcollection = re.findall("<td>[^<]*?<\/td>\s*<td>\s*<a\s*href=\"[^\"]*\"[^<]*>\s*([^<]*)\s*<\/a>\s*<\/td>", str(searchcontent), re.IGNORECASE)
					else:
						print("For first page click")
						appcollection = re.findall("<td>[^<]*?<\/td>\s*<td>\s*<a\s*href=\"[^\"]*\"[^<]*>\s*([^<]*)\s*<\/a>\s*<\/td>", str(searchcontent), re.IGNORECASE)

					# print appcollection
					# print type(appcollection)
					for appMatch in appcollection:
						app_num = appMatch
						print a ,"<==>", appCount ,"<==>", totalcount,"<==>", b
						print "Current Application Number is:", app_num
						
						driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_grdResults"]/tbody/tr['+str(b)+']/td[2]/a').click()	
						app_url=driver.current_url	
						#-- Wait funtion
						time.sleep(4)  
						appcontent=driver.page_source
						
						appfinalCnt=''
						if len(appcontent) > 0:
							appfinalCnt = appcontent.encode("utf-8")

						appDataCollection = appParseSection(appfinalCnt, app_num, app_url, CouncilCode, Source)
						print "appDataCollection:  ", appDataCollection

						# with open(folder_path+"/filename.html", 'wb') as fd:
						# 	fd.write(searchcontent)
						
						b += 4
						a += 1
						
						
						# driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnReturnToResults"]').click()
						driver.execute_script("window.history.go(-1)")
						
						#-- Wait funtion
						time.sleep(7)
						
						
						if appCount == 1:
							bulkValuesForQuery = insertQuery+appDataCollection
							appCount += 1
						elif appCount == 5:
							bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
							cursor.execute(bulkValuesForQuery)
							conn.commit()
							appCount=1
							bulkValuesForQuery=''
						else:
							bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
							appCount += 1
					
					if re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>'+str(pageNum)+'<\/a>', str(searchcontent)):
						check_next_page = re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>('+str(pageNum)+')<\/a>', str(searchcontent), re.I)
						print "pageNumber:",pageNum              					
						if check_next_page != "":
							pageNumber = pageNumber + 1
							continue
						else:
							break
					else:
						break
				else:
					break	
				
		else:
			appCount = 1
			b = 2
					
			appcollection = re.findall("<td>[^<]*?<\/td>\s*<td>\s*<a\s*href=\"[^\"]*\"[^<]*>\s*([^<]*)\s*<\/a>\s*<\/td>", str(searchcontent), re.IGNORECASE)

			for appMatch in appcollection:
				app_num = appMatch
				print a ,"<==>", totalcount
				print "Current Application Number is:", app_num
				
				driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_grdResults"]/tbody/tr['+str(b)+']/td[2]/a').click()	
				app_url=driver.current_url	
				#-- Wait funtion
				time.sleep(8)  
				appcontent=driver.page_source
				
				appfinalCnt=''
				if len(appcontent) > 0:
					appfinalCnt = appcontent.encode("utf-8")

				appDataCollection = appParseSection(appfinalCnt, app_num, app_url, CouncilCode, Source)
				print "appDataCollection:  ", appDataCollection

				# with open(folder_path+"/filename.html", 'wb') as fd:
				# 	fd.write(appfinalCnt)
				
				b += 4
				a += 1
				
				
				driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnReturnToResults"]').click()
				# driver.execute_script("window.history.go(-1)")
				
				#-- Wait funtion
				time.sleep(7)
				
				bulkValuesForQuery = insertQuery+joinValues
				cursor.execute(bulkValuesForQuery)
				conn.commit()
				bulkValuesForQuery=''
				
				
				# if appCount == 1:
					# bulkValuesForQuery = insertQuery+appDataCollection
					# appCount += 1
				# elif appCount == 5:
					# bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					# cursor.execute(bulkValuesForQuery)
					# conn.commit()
					# appCount=1
					# bulkValuesForQuery=''
				# else:
					# bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					# appCount += 1
			
		print "\nbulkValuesForQuery==>",bulkValuesForQuery	
			
		return bulkValuesForQuery
			
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application details parse section	
def appParseSection(Appcontent, AppNum, AppURL, CouncilCode, Source):
    try:
        Application_Number=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Our\s*reference\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Proposal=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Proposal\s*<\/label>\s*<[^>]*?>\s*([\w\W]*?)\s*<\/textarea>\s*', str(Appcontent), re.IGNORECASE)        
        Application_Status=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Status\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        
        Address=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Site\s*address\s*<\/label>\s*<[^>]*?>\s*([\w\W]*?)\s*<\/textarea>', str(Appcontent), re.IGNORECASE)
        Date_Application_Received=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Date\s*received\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Date_Application_validated=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Date\s*valid\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Applicant_Name=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Applicant\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Agent_Name=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Agent\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Agent_Address=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Agent\s*Address\s*<\/label>\s*<[^>]*?>\s*([\w\W]*?)\s*<\/textarea>', str(Appcontent), re.IGNORECASE)
        Applicant_Address=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Applicant\s*Address\s*<\/label>\s*<[^>]*?>\s*([\w\W]*?)\s*<\/textarea>', str(Appcontent), re.IGNORECASE)
        Northing=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Northing\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Easting=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Easting\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
		
        
        (Application, ProposalCnt, ApplicationType, AppAddress, ApplicationStatus, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, EastingData, NorthingData) = ('','','','','','','','','','','','','','','','','')


        if len(Application_Number) > 0:
			Application=clean(Application_Number[0])
        else:
			Application=AppNum


        if len(Proposal) > 0:
			ProposalCnt=clean(Proposal[0])
        else:
			ProposalCnt=''
        ProposalCnt = re.sub(r'\'', "\'\'", str(ProposalCnt))


        if len(Date_Application_Received) > 0:
			DateApplicationReceived=clean(Date_Application_Received[0])
        else:
			DateApplicationReceived=''

        if len(Date_Application_validated) > 0:
			DateApplicationvalidated=clean(Date_Application_validated[0])
        else:
			DateApplicationvalidated=''

        if len(Application_Status) > 0:
			ApplicationStatus=clean(Application_Status[0])
        else:
			ApplicationStatus=''

        if len(Address) > 0:
			AppAddress=clean(Address[0])
        else:
			AppAddress=''

        AppAddress = re.sub(r'\'', "\'\'", str(AppAddress))


        if len(Applicant_Name) > 0:
			ApplicantName=clean(Applicant_Name[0])
        else:
			ApplicantName=''
			
        ApplicantName = re.sub(r'\'', "\'\'", str(ApplicantName))


        if len(Agent_Name) > 0:
			AgentName=clean(Agent_Name[0])
        else:
			AgentName=''


        if len(Agent_Address) > 0:
			AgentAddress=clean(Agent_Address[0])
        else:
			AgentAddress=''

        AgentAddress = re.sub(r'\'', "\'\'", str(AgentAddress))
        AgentAddress = re.sub(r'\\n+', " ", str(AgentAddress))


        if len(Applicant_Address) > 0:
			Applicant_Address[0] = re.sub(r'(?:[\,\s])+$', "", str(Applicant_Address[0]))
			ApplicantAddress=clean(Applicant_Address[0])
        else:
			ApplicantAddress=''

        ApplicantAddress = re.sub(r'\'', "\'\'", str(ApplicantAddress))
        ApplicantAddress = re.sub(r'\\n+', " ", str(ApplicantAddress))



        if len(Easting) > 0:
			EastingData=clean(Easting[0])
        else:
			EastingData=''


        if len(Northing) > 0:
			NorthingData=clean(Northing[0])
        else:
			NorthingData=''

		
        format1 = "%Y/%m/%d %H:%M"	
        Schedule_Date = datetime.now().strftime(format1)
	
        Source_With_Time = Source+"_"+Schedule_Date+"-perl"
		
        Council_Name = "Surrey County Council";
		
        joinValues="("+"'"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived)+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated)+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','','','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(AppURL)+"','"+str(AppURL)+"','','"+Source_With_Time+"','"+str(Schedule_Date)+"','"+str(NorthingData)+"','"+str(EastingData)+"')"


        # print "ApplicationValues: ", joinValues
		
        return joinValues 

    except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:      
        clean = ''  
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
        return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	# browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Users/parielavarasan/Downloads/chromedriver.exe')

	# service = service.Service('C:/Glenigan/Merit/Projects/Selenium_Jar/chromedriver.exe')
	# service.start()
	# capabilities = {'chrome.binary': 'C:/Glenigan/Merit/Projects/Selenium_Jar/chromedriver.exe'}
	# browser = webdriver.Remote(service.service_url, capabilities)
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	
	results = inputsection(fromdate, todate, browser)
	# results = inputsection('01/01/2019', '03/04/2019', browser)

	finalinsertQuery = parsesection(results, councilcode, gcs, conn, cursor, browser)
	
	
	# final output query insertion
	if (finalinsertQuery != '') or finalinsertQuery is not None:
		cursor.execute(finalinsertQuery)
		conn.commit()
		
	
	browser.quit()