# -*- coding: utf-8 -*-
import requests as req
import json
import sys,os,re
import pandas as pd
from datetime import datetime, timedelta
import pymssql
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import ssl
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import imp,math
import configparser
import codecs
from time import sleep
from random import randint

''' Create Log Directory'''
basePath = os.getcwd()
print ("basePath::",basePath)
Config = configparser.ConfigParser()
Config.read(str(basePath)+'\\'+str('Online_Planning.ini'))

todaydate = time.strftime('%Y%m%d')
logDirectory=basePath+"/log/"+todaydate
if os.path.isdir(str(logDirectory)) == False:
	os.makedirs(str(logDirectory))
	
reload(sys)
sys.setdefaultencoding("utf-8")

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	# conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	return (conn)

# Insert Query
def dbInsert(InsertQuery,queryType):
	try:
		cursor.execute(InsertQuery)
		conn.commit()
		print ("*** DB Insertion Succesfully ***")
	except Exception as e:
		QueryTxt=logDirectory+"/"+'Failed_Query_'+councilcode+"_"+gcs+".txt"
		with open(QueryTxt, 'a') as fd:
			fd.write(str(InsertQuery)+"\t"+str(queryType)+"\t"+str(e)+"\t"+"\n")

# Clean function
def clean(cleanValue):
	try:	  
		clean = ''	
		clean = re.sub(r'\&lt\;', "\<", str(cleanValue))
		clean = re.sub(r'\&gt\;', "\>", str(cleanValue))
		clean = re.sub(r'<br\s*\/?\s*>', " ", str(cleanValue))
		clean = re.sub(r'<\/br>', " ", str(cleanValue))
		clean = re.sub(r'<\/br>', " ", str(cleanValue))
		clean = re.sub(r'\n', "", str(cleanValue))
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
		
		return clean	

	except Exception as ex:
		print (ex,sys.exc_traceback.tb_lineno)
		
def tabDetailsCollection(tabContent, blockSection, category):

	''' Ascii Code Conversion '''
	tabContent = tabContent.encode("ascii", "ignore")
	tabContent = tabContent.decode()
				
	tabDetails={}
	for Key in Config[blockSection]:
		Regex=Config.get(blockSection,Key)
		dataPoint=re.findall(Regex, str(tabContent), re.IGNORECASE)
		try:
			tabDetails[Key] = dataPoint[0]
		except:
			tabDetails[Key] = ''
	return(tabDetails)

def getApplicationDetails(receivedApplicationUrl, category, councilCode):
	tempURL=str(receivedApplicationUrl)
	ses = req.session()
	headers = {
				'Host': Config.get(councilCode,"host"),
				'Content-Type': 'text/html;charset=ISO-8859-1',
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36',
			}
			
	ses.headers.update(headers)
	
	count=0
	applicationDetailsPageResponse=500
	applicationDetailsPageContent=''
	dataDetails={}
	applicationNumber=''
	try:
		while(count<=3):
			if not (re.search("^200$",str(applicationDetailsPageResponse))):
				try:
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					sleep(randint(5,10))
					applicationDetailsPageResponse=response1.status_code
					applicationDetailsPageContent=response1.content

					applicationDetailsPageContent = BeautifulSoup(applicationDetailsPageContent, 'html.parser')
					applicationDetailsPageContent=str(applicationDetailsPageContent)
				except:
					print ("*** Error 1 ***")
					pass
				count+=1;
			else:
				break

		if(re.findall(Config.get('summary','case_reference'),str(applicationDetailsPageContent),re.IGNORECASE)):
			applicationNumber=re.findall(Config.get('summary','case_reference'),applicationDetailsPageContent)
			applicationNumber=applicationNumber[0]
		elif(re.findall(Config.get('summary','application_reference'),applicationDetailsPageContent,re.IGNORECASE)):
			applicationNumber=re.findall(Config.get('summary','application_reference'),applicationDetailsPageContent,re.IGNORECASE)
			applicationNumber=applicationNumber[0]
		elif(re.findall(Config.get('summary','summary_casenumber'),applicationDetailsPageContent,re.IGNORECASE)):
			applicationNumber=re.findall(Config.get('summary','summary_casenumber'),applicationDetailsPageContent,re.IGNORECASE)
			applicationNumber=applicationNumber[0]
		
		if(re.search("activeTab=summary",str(tempURL))):
			print ("ActiveTab ==> Summary")
			activeTabContents = tabDetailsCollection(applicationDetailsPageContent, "summary", category)
			dataDetails.update(activeTabContents)
		
		if not re.search("^149$",str(councilCode)):
			count=0
			while(count<=3):
				try:
					tempURL=re.sub(r'activeTab=summary','activeTab=details',str(tempURL))
					print ("ActiveTab ==> Details")
					print ("ActiveTabURL ==> ",tempURL)
					sleep(randint(5,10))
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')
						
					activeTabContents = tabDetailsCollection(detailsPageContent, "details", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Details Error1 ***")
					pass
				count+=1
		if (category != "Decision"):
			tempURL=re.sub(r'activeTab=details','activeTab=contacts',str(tempURL))
			print ("ActiveTab ==> Contacts")
			print ("ActiveTabURL ==> ",tempURL)
			sleep(randint(5,10))
			count=0
			while(count<=3):
				try:
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')

					activeTabContents = tabDetailsCollection(detailsPageContent, "contacts", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Planning Contacts Error ***")
					pass
				count+=1	
					
		elif(category == "Decision"):
			tempURL=re.sub(r'activeTab=details','activeTab=dates',tempURL)
			print ("ActiveTab ==> Dates")
			print ("ActiveTabURL ==> ",tempURL)
			
			keyVal=re.findall(r'keyVal=([^>]*?)\&activeTab=dates\s*$',str(tempURL), re.IGNORECASE)
		  
			count=0
			while(count<=3):
				try:
					tempURL = re.search(r'^([^>]*?)('+str(keyVal[0])+'=[^>]*?)\&(activeTab=dates)\s*$', tempURL)
					tempURL=tempURL.group()+tempURL.group(1)+'\&'+tempURL.group(1)
					print ("Elif ActiveTabURL ==> ",tempURL)
					sleep(randint(5,10))
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')
					activeTabContents = tabDetailsCollection(detailsPageContent, "dates", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Decision Dates Error ***")
					pass
				count+=1
		if(category == "Planning"):
			tempURL=re.sub(r'activeTab=contacts','activeTab=dates',tempURL)
			print ("ActiveTab ==> Dates")
			print ("ActiveTabURL ==> ",tempURL)
			sleep(randint(5,10))
			count=0
			while(count<=3):
				try:
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')

					activeTabContents = tabDetailsCollection(detailsPageContent, "dates", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Planning Dates Error ***")
					pass
				count+=1
	except Exception as e:
		print ("getApplicationDetails Error =>",e)
	
	return(dataDetails, applicationDetailsPageResponse, applicationNumber)


def WeeklyMonthlySearch(DayWise,category,GCS):
	print ("*** Month Wise Application ***")

	if DayWise == "week":
		monthly_URL='https://caps.woking.gov.uk/online-applications/search.do?action=weeklyList'
	else:
		monthly_URL='https://caps.woking.gov.uk/online-applications/search.do?action=monthlyList'
	
	browser.get(monthly_URL)
	sleep(randint(7,10))
	monID=browser.find_element_by_id(str(DayWise))
	selectCount=len(monID.find_elements_by_tag_name("option"))
	# print("SelectCount::", len(monID.find_elements_by_tag_name("option")))
	
	MonthlyURLs=[]
	Count=0
	
	position =	 {
	  "GCS001": 1,
	  "GCS002": 2,
	  "GCS003": 3,
	  "GCS004": 4,
	  "GCS005": 5,	 
	  "GCS090": 6,
	  "GCS180": 7,
	} 
	
	pos=searchfinalCnt=""
	if gcs !="":
		pos = position[gcs]
	
	while(selectCount > Count):
		
		if Count != 0:
			browser.get(monthly_URL)
			sleep(randint(7,10))
		
		if category == "Decision":
			browser.find_element_by_id("dateDecided").click()
		
		if GCS == "GCS":
			Count=int(pos)
			Count=Count-1
	   
		monID=browser.find_element_by_id(str(DayWise))
		monID.find_elements_by_tag_name("option")[Count].click()
		
		Count+=1
		try:
			clickCount=1
			while(1):
				try:
					btn=browser.find_element_by_class_name("buttons")
					btns=btn.find_elements_by_tag_name("input")
					for searchBtn in btns:
						print ("SEARCH :: ",searchBtn.get_attribute("value"))
						if re.search(searchBtn.get_attribute("value"),"Search"):
							searchBtn.click()
							print ("Button Clicked ***")
							sleep(randint(5,8))
							break
				except Exception as e:
					print ("Button Error::",e)
					break
				
				if clickCount == 3:
					break
				
				clickCount+=1
					
			sleep(randint(8,10))
			browser.find_element_by_id('resultsPerPage').send_keys('100')
			browser.find_element_by_xpath('//*[@id="searchResults"]/input[4]').click()
			sleep(randint(10,13))
			
			total_Page=1
			searchcontent=browser.page_source
			try:
				total_Page = re.findall("class\=\"page\">([^>]*?)<\/a>[^.]*?<[^>]*?class\=\"next\">", str(searchcontent), re.IGNORECASE)
				total_Page=total_Page[0]
			except:
				total_Page=1

			print ("total_Page::",total_Page)

			try:
				while(browser.find_elements_by_class_name("next")[0]):
					browser.find_elements_by_class_name("next")[0].click()
					print ("*** Next Page Clicked ***")
					sleep(randint(6,9))
					tempcontent = browser.page_source
					sleep(randint(5,8))
					tempcontent = BeautifulSoup(tempcontent, 'html.parser')
					searchcontent=searchcontent + str(tempcontent)
			except Exception as e:
				print ("No More Next Page::", e)
		 
			# -- Wait funtion
			# sleep(randint(10,15))
			
			if len(searchcontent) > 0:
				searchfinalCnt = searchcontent.encode("utf-8")
			
			filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
			with open(filename, 'wb') as fd:
				fd.write(searchfinalCnt)
			
			searchfinalCnt = BeautifulSoup(searchfinalCnt, 'html.parser')
		except Exception as e:
			print ("Search Button ERROR::",e)
			
		if GCS == "GCS":
			break
	return (searchfinalCnt)

# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, driver, councilcode, gcs):
	try:
		print ("home_url:: ",Config.get(councilcode,'home_url'))
		driver.get(Config.get(councilcode,'home_url'))
		sleep(randint(7,10))
		
		''' Advance Button - Click '''
		try:
			tab=driver.find_element_by_class_name("tabs")
			lis =tab.find_elements_by_tag_name("li")
			for li in lis:
				if re.search(li.text,"Advanced"):
					li.click()
					break
		except Exception as e:
			print ("ERROR::",e)
			
		''' Date Range Set '''
		driver.find_element_by_id(Config.get(councilcode,"form_start_id")).send_keys(dumFrom_Date)
		sleep(randint(4,6))
		driver.find_element_by_id(Config.get(councilcode,"form_end_id")).send_keys(dumTo_Date)
		sleep(randint(4,6))
		
		''' advancedSearchForm Button - Click '''
		try:
			btn=driver.find_element_by_class_name("buttons")
			btns=btn.find_elements_by_tag_name("input")
			for searchBtn in btns:
				print ("SEARCH :: ",searchBtn.get_attribute("value"))
				if re.search(searchBtn.get_attribute("value"),"Search"):
					searchBtn.click()
					break
		except Exception as e:
			print ("Search Button ERROR::",e)
			
		try:
			sleep(randint(8,11))
			driver.find_element_by_id('resultsPerPage').send_keys('100')
			driver.find_element_by_xpath('//*[@id="searchResults"]/input[4]').click()
			sleep(randint(10,13))
		except:
			return ("Captcha Return")
			
		total_Page=1
		searchcontent=driver.page_source
		try:
			total_Page = re.findall("class\=\"page\">([^>]*?)<\/a>[^.]*?<[^>]*?class\=\"next\">", str(searchcontent), re.IGNORECASE)
			total_Page=total_Page[0]
		except:
			total_Page=1
		
		print ("total_Page::",total_Page)

		try:
			while(driver.find_elements_by_class_name("next")[0]):
				driver.find_elements_by_class_name("next")[0].click()
				print ("*** Next Page Clicked ***")
				sleep(randint(6,9))
				tempcontent = driver.page_source
				sleep(randint(4,8))
				tempcontent = BeautifulSoup(tempcontent, 'html.parser')
				searchcontent=searchcontent + str(tempcontent)
		except Exception as e:
			print ("No More Next Page::", e)
	 
		# -- Wait funtion
		sleep(randint(10,13))
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
		with open(filename, 'wb') as fd:
			fd.write(searchfinalCnt)
		
		searchfinalCnt = BeautifulSoup(searchfinalCnt, 'html.parser')
		return (searchfinalCnt)

	except Exception as e:
		searchcontent=driver.page_source
		searchfinalCnt = searchcontent.encode("utf-8")
		filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
		with open(filename, 'wb') as fd:
			fd.write(searchfinalCnt)
		print (e,sys.exc_traceback.tb_lineno)

def LinkCollection(councilCode,searchcontent):
	Links = re.findall("<li\s*class\=\"searchresult\">\s*<a\s*href\=\"([^>]*?)\">", str(searchcontent), re.IGNORECASE)
	Detail_Links=[]
	for Link in Links:
		if Link != '':
			if not (re.search("^http", Link)):
				# print ("councilCode:: ",str(councilCode))
				Link = Config.get(councilCode,'filter_url') + Link;
			Link = re.sub(r'\&amp\;',"&",str(Link))
			print ("Application_Link:: ", Link)
			Detail_Links.append(str(Link))
	return Detail_Links

def fetchData(dictionaryValue,keys):
	values=''
	keys=keys.lower()
	try:
		if dictionaryValue[keys]:
			values=dictionaryValue[keys]
			values=clean(values)
	except:
		pass
	return str(values)
	
def applicationsDataDumpPlanning(TabContent, applicationLink, Source, councilCode, councilName, scheduleDate, scheduleDateTime):

	Address = Application = dateApplicationvalidated = dateApplicationReceived = dateApplicationRegistered = Proposal = applicationStatus = actualDecisionLevel = agentAddress = agentCompanyName = agentName = agentTelephone1 = applicantAddress = applicantName = applicationType = agentEmail = agentMobile = agentFax = agentTelephone2 = agentContactDetails = newDocumentURL = actualCommitteeDate = actualCommitteeorPanelDate = advertisementExpiryDate = agreedExpiryDate = applicationExpiryDeadline = targetDecdt = temporaryPermissionExpiryDate = noDocument = sourceWithTime = documentURL = gridReference = Easting = Northing = ''
	 
	''' Summer Tab Details '''
	ADDRESS=fetchData(TabContent,'ADDRESS')
	ALTERNATIVE_REFERENCE=fetchData(TabContent,'ALTERNATIVE_REFERENCE')
	APPEAL_DECISION=fetchData(TabContent,'APPEAL_DECISION')
	DATE_APPLICATION_RECEIVED=fetchData(TabContent,'DATE_APPLICATION_RECEIVED')
	DATE_APPLICATION_VALIDATED=fetchData(TabContent,'DATE_APPLICATION_VALIDATED')
	DATE_APPLICATION_REGISTERED=fetchData(TabContent,'DATE_APPLICATION_REGISTERED')
	APPLICATION_REFERENCE=fetchData(TabContent,'APPLICATION_REFERENCE')
	APPLICATION_RECEIVED_DATE=fetchData(TabContent,'APPLICATION_RECEIVED_DATE')
	CASE_REFERENCE=fetchData(TabContent,'CASE_REFERENCE')
	APPLICATION_REGISTERED_DATE=fetchData(TabContent,'APPLICATION_REGISTERED_DATE')
	DECISION_ISSUED_DATE=fetchData(TabContent,'DECISION_ISSUED_DATE')
	LOCATION=fetchData(TabContent,'LOCATION')
	PLANNING_PORTAL_REFERENCE=fetchData(TabContent,'PLANNING_PORTAL_REFERENCE')
	PROPOSAL=fetchData(TabContent,'PROPOSAL')
	APPLICATION=fetchData(TabContent,'APPLICATION')
	APPLICATION_STATUS=fetchData(TabContent,'APPLICATION_STATUS')
	NO_DOCUMENT=fetchData(TabContent,'NO_DOCUMENT')
	NO_CASE=fetchData(TabContent,'NO_CASE')
	NO_PROPERTY=fetchData(TabContent,'NO_PROPERTY')
	SUMMARY_ADDRESS=fetchData(TabContent,'SUMMARY_ADDRESS')
	SUMMARY_PROPOSAL=fetchData(TabContent,'SUMMARY_PROPOSAL')
	SUMMARY_CASENUMBER=fetchData(TabContent,'SUMMARY_CASENUMBER')
	DOC_URL=fetchData(TabContent,'DOC_URL')
	DOC_URL1=fetchData(TabContent,'DOC_URL1')
	DOC_URL2=fetchData(TabContent,'DOC_URL2')
	DOC_URL3=fetchData(TabContent,'DOC_URL3')
	
	''' Details Tab '''
	ACTUAL_DECISION_LEVEL=fetchData(TabContent,'ACTUAL_DECISION_LEVEL')
	AGENT_ADDRESS=fetchData(TabContent,'AGENT_ADDRESS')
	AGENT_COMPANY_NAME=fetchData(TabContent,'AGENT_COMPANY_NAME')
	AGENT_NAME=fetchData(TabContent,'AGENT_NAME')
	AGENT_TELEPHONE=fetchData(TabContent,'AGENT_TELEPHONE')
	AMENITY_SOCIETY=fetchData(TabContent,'AMENITY_SOCIETY')
	APPLICANT_ADDRESS=fetchData(TabContent,'APPLICANT_ADDRESS')
	APPLICANT_NAME=fetchData(TabContent,'APPLICANT_NAME')
	APPLICATION_TYPE=fetchData(TabContent,'APPLICATION_TYPE')
	CASE_OFFICER=fetchData(TabContent,'CASE_OFFICER')
	COMMUNITY_COUNCIL=fetchData(TabContent,'COMMUNITY_COUNCIL')
	DECISION_STATUS=fetchData(TabContent,'DECISION_STATUS')
	DISTRICT_REFERENCE=fetchData(TabContent,'DISTRICT_REFERENCE')
	ENVIRONMENTAL_ASSESSMENT_REQUESTED=fetchData(TabContent,'ENVIRONMENTAL_ASSESSMENT_REQUESTED')
	ENVIRONMENTAL_ASSESSMENT_REQUIRED=fetchData(TabContent,'ENVIRONMENTAL_ASSESSMENT_REQUIRED')
	EXPECTED_DECISION_LEVEL=fetchData(TabContent,'EXPECTED_DECISION_LEVEL')
	NEIGHBOURHOOD_PARTNERSHIP_AREA=fetchData(TabContent,'NEIGHBOURHOOD_PARTNERSHIP_AREA')
	PARISH=fetchData(TabContent,'PARISH')
	WARD=fetchData(TabContent,'WARD')
	
	''' Contact Tab '''
	AGENT_EMAIL=fetchData(TabContent,'AGENT_EMAIL')
	AGENT_TELEPHONE1=fetchData(TabContent,'AGENT_TELEPHONE1')
	AGENT_MOBILE=fetchData(TabContent,'AGENT_MOBILE')
	HOME_PHONE=fetchData(TabContent,'HOME_PHONE')
	AGENT_FAX=fetchData(TabContent,'AGENT_FAX')
	MOBILE_NUMBER=fetchData(TabContent,'MOBILE_NUMBER')
	PHONE_NUMBER=fetchData(TabContent,'PHONE_NUMBER')
	TELEPHONE_NUMBER=fetchData(TabContent,'TELEPHONE_NUMBER')
	HOME_PHONE_NUMBER=fetchData(TabContent,'HOME_PHONE_NUMBER')
	AGENT_ADDRESS1=fetchData(TabContent,'AGENT_ADDRESS1')
	PERSONAL_EMAIL=fetchData(TabContent,'PERSONAL_EMAIL')
	PERSONAL_PHONE=fetchData(TabContent,'PERSONAL_PHONE')
	COMPANY_PHONE=fetchData(TabContent,'COMPANY_PHONE')
	WORK_PHONE=fetchData(TabContent,'WORK_PHONE')
	EMAIL_ADDRESSE=fetchData(TabContent,'EMAIL_ADDRESSE')
	FAX_NO=fetchData(TabContent,'FAX_NO')
	COMPANY_FAX=fetchData(TabContent,'COMPANY_FAX')
	EMAIL_ADDRESS=fetchData(TabContent,'EMAIL_ADDRESS')
	TELEPHONE=fetchData(TabContent,'TELEPHONE')
	PERSONAL_MOBILE=fetchData(TabContent,'PERSONAL_MOBILE')
	ELECTRONIC_MAIL=fetchData(TabContent,'ELECTRONIC_MAIL')
	WORK_PHONE_NUMBER=fetchData(TabContent,'WORK_PHONE_NUMBER')
	PHONE_NO=fetchData(TabContent,'PHONE_NO')
	OFFICE_PHONE_NUMBER=fetchData(TabContent,'OFFICE_PHONE_NUMBER')
	MAIN_PHONE=fetchData(TabContent,'MAIN_PHONE')
	PHONE_USED_BY_ALL_TEMPLATES=fetchData(TabContent,'PHONE_USED_BY_ALL_TEMPLATES')
	PHONE_CONTACT_NUMBER=fetchData(TabContent,'PHONE_CONTACT_NUMBER')
	AGENT_CONTACT_DETAILS=fetchData(TabContent,'AGENT_CONTACT_DETAILS')
	AGENT_CONTACT_NAME=fetchData(TabContent,'AGENT_CONTACT_NAME')
	ACKNOWLEDGEMENT_EMAIL=fetchData(TabContent,'ACKNOWLEDGEMENT_EMAIL')
	EMAIL=fetchData(TabContent,'EMAIL')
	MOBILE=fetchData(TabContent,'MOBILE')
	E_MAIL_ADDRESS=fetchData(TabContent,'E_MAIL_ADDRESS')
	
	''' Date Tab Details '''
	ACTUAL_COMMITTEE_DATE = fetchData(TabContent,'ACTUAL_COMMITTEE_DATE')
	ACTUAL_COMMITTEE_OR_PANEL_DATE = fetchData(TabContent,'ACTUAL_COMMITTEE_OR_PANEL_DATE')
	ADVERTISEMENT_EXPIRY_DATE = fetchData(TabContent,'ADVERTISEMENT_EXPIRY_DATE')
	AGREED_EXPIRY_DATE = fetchData(TabContent,'AGREED_EXPIRY_DATE')
	APPLICATION_EXPIRY_DEADLINE = fetchData(TabContent,'APPLICATION_EXPIRY_DEADLINE')
	APPLICATION_VALID_DATE = fetchData(TabContent,'APPLICATION_VALID_DATE')
	APPLICATION_VALIDATED_DATE = fetchData(TabContent,'APPLICATION_VALIDATED_DATE')
	CLOSING_DATE_FOR_COMMENTS = fetchData(TabContent,'CLOSING_DATE_FOR_COMMENTS')
	COMMENTS_TO_BE_SUBMITTED_BY = fetchData(TabContent,'COMMENTS_TO_BE_SUBMITTED_BY')
	COMMITTEE_DATE = fetchData(TabContent,'COMMITTEE_DATE')
	COMMITTEE_DELEGATED_LIST_DATE = fetchData(TabContent,'COMMITTEE_DELEGATED_LIST_DATE')
	CONSULTATION_END_DATE = fetchData(TabContent,'CONSULTATION_END_DATE')
	CONSULTATION_EXPIRY_DATE = fetchData(TabContent,'CONSULTATION_EXPIRY_DATE')
	CONSULTATION_PERIOD_EXPIRES = fetchData(TabContent,'CONSULTATION_PERIOD_EXPIRES')
	DATE_APPLICATION_VALID = fetchData(TabContent,'DATE_APPLICATION_VALID')
	DECISION_DATE = fetchData(TabContent,'DECISION_DATE')
	DECISION_ISSUED_DATE1 = fetchData(TabContent,'DECISION_ISSUED_DATE')
	DECISION_MADE_DATE = fetchData(TabContent,'DECISION_MADE_DATE')
	DECISION_NOTICE_DATE = fetchData(TabContent,'DECISION_NOTICE_DATE')
	DECISION_PRINTED_DATE = fetchData(TabContent,'DECISION_PRINTED_DATE')
	DETERMINATION_DEADLINE = fetchData(TabContent,'DETERMINATION_DEADLINE')
	EARLIEST_DECISION_DATE = fetchData(TabContent,'EARLIEST_DECISION_DATE')
	ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED = fetchData(TabContent,'ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED')
	EXPIRY_DATE = fetchData(TabContent,'EXPIRY_DATE')
	EXPIRY_DATE_FOR_COMMENT = fetchData(TabContent,'EXPIRY_DATE_FOR_COMMENT')
	LAST_ADVERTISED_IN_PRESS_DATE = fetchData(TabContent,'LAST_ADVERTISED_IN_PRESS_DATE')
	LAST_DATE_FOR_COMMENTS = fetchData(TabContent,'LAST_DATE_FOR_COMMENTS')
	LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE = fetchData(TabContent,'LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE')
	LAST_DATE_FOR_NEIGHBOUR_COMMENTS = fetchData(TabContent,'LAST_DATE_FOR_NEIGHBOUR_COMMENTS')
	LAST_DATE_FOR_NEIGHBOURS_RESPONSES = fetchData(TabContent,'LAST_DATE_FOR_NEIGHBOURS_RESPONSES')
	LAST_SITE_NOTICE_POSTED_DATE = fetchData(TabContent,'LAST_SITE_NOTICE_POSTED_DATE')
	LATEST_ADVERTISEMENT_EXPIRY_DATE = fetchData(TabContent,'LATEST_ADVERTISEMENT_EXPIRY_DATE')
	LATEST_NEIGHBOUR_CONSULTATION_DATE = fetchData(TabContent,'LATEST_NEIGHBOUR_CONSULTATION_DATE')
	LATEST_SITE_NOTICE_EXPIRY_DATE = fetchData(TabContent,'LATEST_SITE_NOTICE_EXPIRY_DATE')
	NEIGHBOUR_CONSULTATION_EXPIRY_DATE = fetchData(TabContent,'NEIGHBOUR_CONSULTATION_EXPIRY_DATE')
	NEIGHBOURS_LAST_NOTIFIED = fetchData(TabContent,'NEIGHBOURS_LAST_NOTIFIED')
	OVERALL_CONSULTATION_EXPIRY_DATE = fetchData(TabContent,'OVERALL_CONSULTATION_EXPIRY_DATE')
	OVERALL_DATE_OF_CONSULTATION_EXPIRY = fetchData(TabContent,'OVERALL_DATE_OF_CONSULTATION_EXPIRY')
	PERMISSION_EXPIRY_DATE = fetchData(TabContent,'PERMISSION_EXPIRY_DATE')
	PUBLIC_CONSULTATION_EXPIRY_DATE = fetchData(TabContent,'PUBLIC_CONSULTATION_EXPIRY_DATE')
	SITE_NOTICE_EXPIRY_DATE = fetchData(TabContent,'SITE_NOTICE_EXPIRY_DATE')
	STANDARD_CONSULTATION_DATE = fetchData(TabContent,'STANDARD_CONSULTATION_DATE')
	STANDARD_CONSULTATION_EXPIRY_DATE = fetchData(TabContent,'STANDARD_CONSULTATION_EXPIRY_DATE')
	STATUTORY_DETERMINATION_DATE = fetchData(TabContent,'STATUTORY_DETERMINATION_DATE')
	STATUTORY_DETERMINATION_DEADLINE = fetchData(TabContent,'STATUTORY_DETERMINATION_DEADLINE')
	STATUTORY_EXPIRY_DATE = fetchData(TabContent,'STATUTORY_EXPIRY_DATE')
	TARGET_DATE = fetchData(TabContent,'TARGET_DATE')
	TARGET_DETERMINATION_DATE = fetchData(TabContent,'TARGET_DETERMINATION_DATE')
	TEMPORARY_PERMISSION_EXPIRY_DATE = fetchData(TabContent,'TEMPORARY_PERMISSION_EXPIRY_DATE')
	VALID_DATE = fetchData(TabContent,'VALID_DATE')
	WEEKLY_LIST_EXPIRY_DATE = fetchData(TabContent,'WEEKLY_LIST_EXPIRY_DATE')	 
   
	if DOC_URL:
		documentURL = DOC_URL
	if re.search('^\s*$',documentURL):
		documentURL=DOC_URL1
	if re.search('^\s*$',documentURL):
		documentURL=DOC_URL2
	if re.search('^\s*$',documentURL):
		documentURL=DOC_URL3
	if not (re.search('^http',documentURL)) and (documentURL != ""):
		documentURL=Config.get(councilCode,'filter_url')+documentURL
	if documentURL == "":
		documentURL=applicationLink
		documentURL=re.sub(r'activeTab=summary','activeTab=documents',documentURL)
	
	if (re.search('^(105|149|232|245|246|273|292|15|179|171|320|533|211|268|350|406|429|455|481|78)$',str(councilCode))):
		if not documentURL:
			documentURL=applicationLink
			documentURL=re.sub(r'activeTab=summary','activeTab=externalDocuments',documentURL)
		else:
			documentURL=re.sub(r'activeTab=documents','activeTab=externalDocuments',documentURL)
	
	if not APPLICATION:
		if not (re.search('^\s*$',CASE_REFERENCE)):
			Application=CASE_REFERENCE
		elif not (re.search('^\s*$',SUMMARY_CASENUMBER)):
			Application=SUMMARY_CASENUMBER
		elif not (re.search('^\s*$',APPLICATION_REFERENCE)):
			Application=APPLICATION_REFERENCE
	else:
		Application = APPLICATION

	if PROPOSAL:
		Proposal = PROPOSAL
	elif SUMMARY_PROPOSAL:
		Proposal=SUMMARY_PROPOSAL
	
	if ADDRESS == "":
		if not re.search('^\s*$',LOCATION):
			Address=LOCATION
		elif not re.search('^\s*$',SUMMARY_ADDRESS):
			Address=SUMMARY_ADDRESS
	else:
		Address = ADDRESS
	   
	if re.search('^\s*$',AGENT_EMAIL):
		if not re.search('^\s*$',PERSONAL_EMAIL):
			agentEmail = PERSONAL_EMAIL
		elif not re.search('^\s*$',EMAIL_ADDRESSE):
			agentEmail = EMAIL_ADDRESSE
		elif not re.search('^\s*$',EMAIL_ADDRESS):
			agentEmail = EMAIL_ADDRESS
		elif not re.search('^\s*$',ACKNOWLEDGEMENT_EMAIL):
			agentEmail = ACKNOWLEDGEMENT_EMAIL
		elif not re.search('^\s*$',EMAIL):
			agentEmail = EMAIL
		elif not re.search('^\s*$',E_MAIL_ADDRESS):
			agentEmail = E_MAIL_ADDRESS
		elif not re.search('^\s*$',ELECTRONIC_MAIL):
			agentEmail = ELECTRONIC_MAIL
	else:
		agentEmail = AGENT_EMAIL
	
	if re.search('^\s*$',AGENT_FAX):
		if not re.search('^\s*$',FAX_NO):
			agentFax = FAX_NO
		elif not re.search('^\s*$',COMPANY_FAX):
			agentFax = COMPANY_FAX
	else:
		agentFax = AGENT_FAX
		
	if re.search('^\s*$',AGENT_MOBILE):
		if not re.search('^\s*$',MOBILE_NUMBER):
			agentMobile = MOBILE_NUMBER
		elif not re.search('^\s*$',MOBILE):
			agentMobile = MOBILE
	else:
		agentMobile = AGENT_MOBILE
	
	if AGENT_TELEPHONE1:
		agentTelephone2 = AGENT_TELEPHONE1
	elif re.search('^\s*$',AGENT_TELEPHONE1):
		if not re.search('^\s*$',HOME_PHONE):
			agentTelephone2 = HOME_PHONE
		elif not re.search('^\s*$',PHONE_NUMBER):
			agentTelephone2 = PHONE_NUMBER
		elif not re.search('^\s*$',TELEPHONE_NUMBER):
			agentTelephone2 = TELEPHONE_NUMBER
		elif not re.search('^\s*$',HOME_PHONE_NUMBER):
			agentTelephone2 = HOME_PHONE_NUMBER
		elif not re.search('^\s*$',PERSONAL_PHONE):
			agentTelephone2 = PERSONAL_PHONE
		elif not re.search('^\s*$',COMPANY_PHONE):
			agentTelephone2 = COMPANY_PHONE
		elif not re.search('^\s*$',WORK_PHONE):
			agentTelephone2 = WORK_PHONE
		elif not re.search('^\s*$',TELEPHONE):
			agentTelephone2 = TELEPHONE
		elif not re.search('^\s*$',WORK_PHONE_NUMBER):
			agentTelephone2 = WORK_PHONE_NUMBER
		elif not re.search('^\s*$',PHONE_NO):
			agentTelephone2 = PHONE_NO
		elif not re.search('^\s*$',OFFICE_PHONE_NUMBER):
			agentTelephone2 = OFFICE_PHONE_NUMBER
		elif not re.search('^\s*$',MAIN_PHONE):
			agentTelephone2 = MAIN_PHONE
		elif not re.search('^\s*$',PHONE_CONTACT_NUMBER):
			agentTelephone2 = PHONE_CONTACT_NUMBER
		elif not re.search('^\s*$',PHONE_USED_BY_ALL_TEMPLATES):
			agentTelephone2 = PHONE_USED_BY_ALL_TEMPLATES
		elif not re.search('^\s*$',PERSONAL_MOBILE):
			agentTelephone2 = PERSONAL_MOBILE
			
	if AGENT_ADDRESS:
		agentAddress = AGENT_ADDRESS
	
	if re.search('^\s*$',agentAddress):
		if not re.search('^\s*$',AGENT_ADDRESS1):
			agentAddress = AGENT_ADDRESS1
	
	if re.search('^\s*$',AGENT_CONTACT_DETAILS):
		if not re.search('^\s*$',AGENT_CONTACT_NAME):
			agentContactDetails = AGENT_CONTACT_NAME
	else:
		agentContactDetails = AGENT_CONTACT_DETAILS
	
	if re.search('^\s*$',DATE_APPLICATION_RECEIVED):
		if not re.search('^\s*$',APPLICATION_RECEIVED_DATE):
			dateApplicationReceived = APPLICATION_RECEIVED_DATE
	else:
		dateApplicationReceived = DATE_APPLICATION_RECEIVED
	
	if re.search('^\s*$',DATE_APPLICATION_REGISTERED):
		if not re.search('^\s*$',APPLICATION_REGISTERED_DATE):
			dateApplicationRegistered = APPLICATION_REGISTERED_DATE
	else:
		dateApplicationRegistered = DATE_APPLICATION_REGISTERED
		
	if re.search('^\s*$',DATE_APPLICATION_VALIDATED):
		if not re.search('^\s*$',APPLICATION_VALID_DATE):
			dateApplicationvalidated = APPLICATION_VALID_DATE
		elif not re.search('^\s*$',APPLICATION_VALIDATED_DATE):
			dateApplicationvalidated = APPLICATION_VALIDATED_DATE
		elif not re.search('^\s*$',DATE_APPLICATION_VALID):
			dateApplicationvalidated = DATE_APPLICATION_VALID
		elif not re.search('^\s*$',VALID_DATE):
			dateApplicationvalidated = VALID_DATE
	else:
		dateApplicationvalidated = DATE_APPLICATION_VALIDATED
	
	if re.search('^\s*$',targetDecdt):
		if not re.search('^\s*$',DETERMINATION_DEADLINE):
			targetDecdt = DETERMINATION_DEADLINE
		elif not re.search('^\s*$',EARLIEST_DECISION_DATE):
			targetDecdt = EARLIEST_DECISION_DATE
		elif not re.search('^\s*$',TARGET_DATE):
			targetDecdt = TARGET_DATE
		elif not re.search('^\s*$',TARGET_DETERMINATION_DATE):
			targetDecdt = TARGET_DETERMINATION_DATE
	
	if (agentName == ""): agentName = AGENT_NAME
	if (applicationStatus == ""): applicationStatus = APPLICATION_STATUS
	if (actualDecisionLevel == ""): actualDecisionLevel = ACTUAL_DECISION_LEVEL
	if (agentCompanyName == ""): agentCompanyName = AGENT_COMPANY_NAME
	if (applicantAddress == ""): applicantAddress = APPLICANT_ADDRESS
	if (applicantName == ""): applicantName = APPLICANT_NAME
	if (applicationType == ""): applicationType = APPLICATION_TYPE
	if (agentTelephone1 == ""): agentTelephone1 = AGENT_TELEPHONE
	if (agentTelephone2 == ""): agentTelephone2 = AGENT_TELEPHONE1
	if (actualCommitteeDate == ""): actualCommitteeDate = ACTUAL_COMMITTEE_DATE
	if (actualCommitteeorPanelDate == ""): actualCommitteeorPanelDate = ACTUAL_COMMITTEE_OR_PANEL_DATE
	if (advertisementExpiryDate == ""): advertisementExpiryDate = ADVERTISEMENT_EXPIRY_DATE
	if (agreedExpiryDate == ""): agreedExpiryDate = AGREED_EXPIRY_DATE
	if (applicationExpiryDeadline == ""): applicationExpiryDeadline = APPLICATION_EXPIRY_DEADLINE
	if (temporaryPermissionExpiryDate == ""): temporaryPermissionExpiryDate = TEMPORARY_PERMISSION_EXPIRY_DATE
	if (noDocument == ""): noDocument = NO_DOCUMENT
	
	sourceWithTime=Source+"_"+scheduleDateTime+"-python"
	
	Address=re.sub(r'\n+'," ",Address)
	applicationStatus=re.sub(r'\n+'," ",applicationStatus)
	Proposal=re.sub(r'\n+'," ",Proposal)
	agentAddress=re.sub(r'\n+'," ",agentAddress)
	agentName=re.sub(r'\n+'," ",agentName)
	applicantAddress=re.sub(r'\n+'," ",applicantAddress)
	applicantName=re.sub(r'\n+'," ",applicantName)
	applicationType=re.sub(r'\n+'," ",applicationType)

	applicationStatus=re.sub(r'^\s*(Decision\s*made)\,\s*view[\w\W]*?$',r"\1",applicationStatus)
	applicationStatus=re.sub(r'^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$',r"\1",applicationStatus)
	applicationStatus=re.sub(r'^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$',r"\1",applicationStatus)
	applicationStatus=re.sub(r'^\s*(Pending\sConsideration)[\w\W]*?$',r"\1",applicationStatus)
	applicationStatus=re.sub(r'^\s*(Pending\sDecision)[\w\W]*?$',"",applicationStatus)
	
	actualDecisionLevel=re.sub(r'\s*Expected\s*Decision\s*Level\s*Case\s*Officer\s*Parish\s*Ward\s*District\s*Reference\s*Applicant\s*Name\s*',"",actualDecisionLevel)
	
	insert_query="("+"'"+str(councilCode)+"','"+str(councilName)+"','"+str(Address)+"','"+str(dateApplicationReceived)+"','"+str(Application)+"','"+str(dateApplicationRegistered)+"','"+str(dateApplicationvalidated)+"','"+str(Proposal)+"','"+str(applicationStatus)+"','"+str(actualDecisionLevel)+"','"+str(agentAddress)+"','"+str(agentCompanyName)+"','"+str(agentName)+"','"+str(agentTelephone1)+"','"+str(applicantAddress)+"','"+str(applicantName)+"','"+str(applicationType)+"','"+str(agentEmail)+"','"+str(agentTelephone2)+"','"+str(agentMobile)+"','"+str(agentFax)+"','"+str(agentContactDetails)+"','"+str(actualCommitteeDate)+"','"+str(actualCommitteeorPanelDate)+"','"+str(advertisementExpiryDate)+"','"+str(agreedExpiryDate)+"','"+str(applicationExpiryDeadline)+"','"+str(targetDecdt)+"','"+str(temporaryPermissionExpiryDate)+"','"+str(applicationLink)+"','"+str(documentURL)+"','"+str(noDocument)+"','"+str(sourceWithTime)+"','"+str(scheduleDate)+"','"+str(gridReference)+"','"+str(Easting)+"','"+str(Northing)+"')"
	
	return(insert_query)
	
# Application parse section 
def parsesection(searchcontent, CouncilCode, Source, conn, cursor, driver):
	try:
		# searchcount = re.findall("class\=\"showing\">[\w\W]*?Showing[\w\W]*?of\s*([\d]+)\s*<[^>]*?>", str(searchcontent), re.IGNORECASE)	
		# totalcount = int(searchcount[0])
		# print ("Totalcount", totalcount)
		
		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Grid_Reference, Easting, Northing) values '
		 
		bulkValuesForQuery=''
		appCount=1
		# resultsCount=0
		appcollection=LinkCollection(CouncilCode,searchcontent)
		print ("LINK COUNT :: ",len(appcollection))
		for Application_Link in appcollection:
			print ("app_url :: ",Application_Link)
			
			sleep(randint(5,10))
			dataDetails = applicationPageContentResponse = appNumber=''
			dataDetails, applicationPageContentResponse, appNumber = getApplicationDetails(str(Application_Link),"Planning",CouncilCode)

			print ("applicationPageContentResponse::",applicationPageContentResponse)
			
			fileTxt=logDirectory+"/"+councilcode+"_"+gcs+".txt"
			with open(fileTxt, 'a') as fd:
				fd.write(str(Application_Link)+"\t"+str(applicationPageContentResponse)+"\t"+str(appNumber)+"\n")
			
			# if(re.search("^\s*(200|ok)\s*$",applicationPageContentResponse):
				# resultsCount+=1
			
			Council_Name=Config.get(CouncilCode,'COUNCIL_NAME')
			format1 = "%Y/%m/%d %H:%M"	
			Schedule_Date = datetime.now().strftime(format1)

			if appNumber:
				appDataCollection = applicationsDataDumpPlanning(dataDetails,Application_Link,Source,CouncilCode,Council_Name,Schedule_Date,Schedule_Date);
			
				print ("appDataCollection :: ",appDataCollection)
			   
				if appCount == 1:
					bulkValuesForQuery = insertQuery+appDataCollection
					appCount += 1
				elif appCount == 5:
					bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					
					print ("bulkValuesForQuery :: ",bulkValuesForQuery)
					''' Insert Query '''
					dbInsert(bulkValuesForQuery,"bulkValuesForQuery")

					appCount=1
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					appCount += 1
		return bulkValuesForQuery		
	except Exception as e:
		print (e,sys.exc_traceback.tb_lineno)
		
# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print ('Councilcode arugument is missing')
		sys.exit()
		
	if gcs is None:
		print ('GCS arugument is missing')
		sys.exit()
	
	conn=""
	cursor=""
	conn = dbConnection("SCREENSCRAPPER") 
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	# browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver.exe')
	browser.maximize_window()
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	print ('fromdate	 :', fromdate)
	print ('todate	 :', todate)
	try:
		Content = inputsection(fromdate, todate, browser, councilcode, gcs)
		# Content = inputsection('01/01/2019', '03/04/2019', browser)
		sleep(randint(10,12))

		''' Weekly/Monthly Wise Application '''
		if Content == "Captcha Return":
			Content = WeeklyMonthlySearch("week","Planning","GCS")
		
		#browser.quit()
		try:
			browser.quit()
		except:
			pass
		
		finalinsertQuery = parsesection(Content, councilcode, gcs, conn, cursor, browser)
		# print ("finalinsertQuery::",finalinsertQuery)
		
		''' Insert Query - final output query insertion '''
		if (finalinsertQuery != '') or finalinsertQuery is not None:
			dbInsert(finalinsertQuery,"finalinsertQuery")
	except Exception as e:
		try:
			browser.quit()
		except:
			pass
		print ("Completed Error:",e)