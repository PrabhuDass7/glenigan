use strict;
use DBI;
use DBD::ODBC;
use WWW::Mechanize;
use HTTP::Cookies;
use HTML::Entities;
use IO::Socket::SSL;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;
use Win32; 
use Glenigan_DB_Windows;



# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];

my $COUNCIL_NAME = "Colchester";


if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' \(Eg: \"160\"\)", 16, "Error message");
    exit();
}


print "Council_Code: $Council_Code\n";


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


############
my $mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');


my $Applicant_Flag = 'Y';


#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();


my $home_url = "https://www.colchester.gov.uk/received-applications-last-6-months/";
$driver->get("$home_url");
sleep(15);
my $model_content=$driver->get_page_source($driver);
open FH, ">Home.html";
print FH $model_content;
close FH;
exit;


$driver->find_element_by_xpath("//*[\@id=\"ctl00_ContentPlaceHolder1_pcSearchControls_T1T\"]/span")->click;
sleep(5);	
my $temp1=$driver->get_page_source($driver);

		
			
			

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}