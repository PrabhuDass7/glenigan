use WWW::Mechanize;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use URI::Escape;
use HTTP::CookieMonster qw( cookies );
use DBI;
use Win32; 
use DBD::ODBC;
use FileHandle;
use File::Copy;
use Time::Piece;
use MIME::Base64;

my $Council_Code = $ARGV[0];


if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' \(Eg: \"160\"\)", 16, "Error message");
    exit();
}


# Establish connection with DB server
my $dbh = &DbConnection();

# User Agent
my $mech = WWW::Mechanize->new(autocheck => 0);

# Add Proxy
# BEGIN {
 # $ENV{HTTPS_PROXY} = 'https://172.27.137.199:3128';
 # $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 # $ENV{HTTPS_DEBUG} = 1;  #Add debug output
# }


# Location    
my $pgm_path='C:/Glenigan/Live_Schedule/Planning/colchester/Program';
$pgm_path=~s/\s+$//igs;
my $archive_path=$pgm_path;
$archive_path=~s/Program$/Archive/igs;

print "Archive Path :: $archive_path \n ";


opendir(DIR, $archive_path) or die $!;
my @folder_array;
while (my $file = readdir(DIR))
{
	next if ($file=~m/^\./);
	$file=~s/\.pdf\s*$//igs;
	push(@folder_array,$file);
	# print "$file\n";
}
print "folder_array :: @folder_array \n";
closedir(DIR);

my $URL = 'https://www.colchester.gov.uk/WAM-received-list/?id=0';
$mech->get($URL);
my $list_content = $mech->content;


my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
my @all_cookies = $monster->all_cookies;

my $cook;
my ($sessionid,$sessionval,$sessionsec,$sessionexp,$cookiesession)=("","","","","");
foreach my $cookie ( @all_cookies ) {
	$sessionid=$cookie->key;
	$sessionval=$cookie->val;
	$sessionsec=$cookie->secure;
	$sessionexp=$cookie->expires;
	print "Session: $sessionid::$sessionval\n";
	$cookiesession = $sessionid."=".$sessionval;
	$cook.=$cookiesession."; ";
}

print "Cookie::$cook\n";

my $t = Time::Piece->new();
my $currentYear = $t->year;
print "currentYear=>$currentYear\n";

open SR, ">colchester.html";
print SR $list_content;
close SR;
# exit;


my $totalPage;
if($list_content=~m/<li\s*class=\"entitylist-filter-option\">([\w\W]*?)<\/li>\s*<\/ul>/is)
{
	my $li = $1;
	$totalPage = $1 if($li=~m/<option\svalue=\"(\d+)\"[^>]*?>[^>]*?<\/option>\s*<\/select>/is);
}

my $base64 = $1 if($list_content=~m/\&quot\;Base64SecureConfiguration\&quot\;\:&quot\;([\w\W]*?)\&quot\;\,/is);
	
if($list_content=~m/data\-get\-url=\"(\/_services\/entity\-grid\-data\.json\/[^\"]*?)\"\sdata\-grid\-/is)
{
	my $URL=$1;
	# print "$pdfURL\n"; <STDIN>;
	
	my $pdfURL;
	if($URL!~m/^http/is)
	{
		$pdfURL="https://www.colchester.gov.uk".$URL;
	}
	
	print "$pdfURL\n"; <STDIN>;
	
	my $postCnt1 = "\{\"base64SecureConfiguration\"\:\"$base64\"\,\"sortExpression\"\:\"new_date_received\sASC\"\,\"search\"\:null\,\"page\"\:1\,\"pageSize\":500\,\"filter\"\:null\,\"metaFilter\"\:null\,\"timezoneOffset\"\:\-330\,\"customParameters\"\:\[\]\}";
	
	print "$postCnt1\n"; <STDIN>;
	
	$cook=$cook."ContextLanguageCode=en-US; browserupdateorg=pause; _gat=1";
	

	print "SSSSSSSSSSSSSSSS==>$cook\n";

	$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0');
	$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
	$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$mech->add_header( 'Content-Type' => 'text/html; charset=utf-8');
	$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
	$mech->add_header( 'Host' => 'www.colchester.gov.uk');
	$mech->add_header( 'Referer' => $URL);
	$mech->add_header( 'Cookie' => $cook);
	$mech->add_header( 'Upgrade-Insecure-Requests' => '1');
	$mech->add_header( 'Connection' => 'keep-alive');
	
	$mech->post($pdfURL, content => $postCnt1);
	my $responseCode = $mech->status;
	print "responseCode==>$responseCode\n";	
	
	sleep(10);
	my $pdfList_cent = $mech->content;
	open SR, ">colchester1.html";
	print SR $list_content;
	close SR;
	
	
	my $postCnt2 = "\{\"base64SecureConfiguration\"\:\"$base64\"\,\"sortExpression\"\:\"new_date_received\sASC\"\,\"search\"\:null\,\"page\"\:1\,\"pageSize\"\:500\,\"filter\"\:null\,\"metaFilter\"\:\"0\=0\"\,\"timezoneOffset\"\:\-330\,\"customParameters\"\:\[\]\}";
	
	$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0');
	$mech->add_header( 'Accept' => 'application/json, text/javascript, */*; q=0.01');
	$mech->add_header( 'Content-Type' => 'text/html; charset=utf-8');
	$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
	$mech->add_header( 'Host' => 'www.colchester.gov.uk');
	$mech->add_header( 'Referer' => $pdfURL);
	$mech->add_header( 'Cookie' => $cook);
	$mech->add_header( 'X-Requested-With' => 'XMLHttpRequest');	
	$mech->add_header( 'access-control-allow-headers' => 'https://cbc-live-dotgovresources.azurewebsites.net/*');
	$mech->add_header( 'Access-Control-Allow-Methods' => 'GET, OPTIONS');
	$mech->add_header( 'Access-Control-Allow-Origin' => 'https://login.microsoftonline.com');
	$mech->add_header( 'X-Frame-Options' => 'SAMEORIGIN');

	
	$mech->add_header( 'Connection' => 'keep-alive');
	$mech->post($pdfURL, content => $postCnt2);
	sleep(10);
	my $responseCode = $mech->status;
	print "responseCode==>$responseCode\n";	
	my $pdfList_content = $mech->content;

	open SR, ">colchester2.html";
	print SR $pdfList_content;
	close SR;
	exit;
	
	
	my $pdfCnt = $1 if($pdfList_content=~m/<div\s*class=\"kb-content\">\s*<p>\s*To\s*view\s*the\s*document[^>]*?>\s*([\w\W]*?)\s*<\/div>\s*<\/div>\s*<div\s*class/is);
	
	my $file_occur_flag=0;
	while($pdfCnt=~m/<p[^>]*?>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*([^<]*?)\s*<\/a>\s*<\/p>/igs)
	{
		my $pdf_url=$1;
		my $file_name=$2;
		
					
		$file_name=~s/\s+//igs;
		$file_name=~s/\//_/igs;
		my $file_exist=0;		
		my @Match_file = grep(/$file_name/, @folder_array);
		if($Match_file[0] ne "")
		{
			my $file_exist=1;
			print "file_exist :: $file_exist \n";
			next;
		}
		else
		{
			$mech->get($pdf_url);
			my $pdf_content = $mech->content;			
			$file_occur_flag=1;
		
			my $temp_file = $pgm_path.'\\'.$file_name;
			$file_name=$pgm_path.'\\'.$file_name;
			
			my $fh = FileHandle->new("$temp_file.pdf", 'w') or die "Cannot open $file_name.pdf for write :$!";
			binmode($fh);
			$fh->print($pdf_content);
			$fh->close();
			
			my $copy_res=copy("$temp_file.pdf",$archive_path);
			print "Download URL :: $file_name.pdf \n";#<STDIN>;
			
			
			open (PDF, "$temp_file.pdf") or die "$!";

			my $raw_string = do{ local $/ = undef; <PDF>; };
			$encoded = encode_base64( $raw_string );
			print " \n";
			print " $encoded ";
			close(PDF);
			exit;
			
			
			my $exe_name="C:/Glenigan/Live_Schedule/Planning/colchester/pdftotext.exe -raw";
			system("$exe_name $temp_file.pdf");
			
			my $html_file_pdf=$file_name.'.pdf';
			$html_file_pdf=~s/\.pdf\s*$/\.txt/igs;
			
			print "html_file_pdf :: $html_file_pdf\n";
			
			
			undef $/;
			open KV, "$html_file_pdf";
			my $txt_cnt=<KV>;
			close KV;
			
			my @Appl_Num;
			while($txt_cnt=~m/^([0-9]{6})\s([A-Z0-9]+)\s/mgsi)
			{
				my $num = $1;
				print "App=>$num\n";	
				push(@Appl_Num,$num);
			}
			
			&Scrape_Details(\@Appl_Num,$Council_Code,$pdf_url);	
		}
	}
	if($file_occur_flag != 1)
	{
		print "No new file occur \n";
	}
}

sub Scrape_Details()
{
	my $AppNum=shift;
	my $Council_Code=shift;	
	my $page_url=shift;
	
	my @AppNums=@{$AppNum};
	
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	
	
	foreach my $ApplicationNo (@AppNums)
	{
		my $tempNum = $ApplicationNo;
		
		
		$tempNum=~s/\//\%2F/gsi;
		# my $simpleSearchURL = "https://www.colchester.gov.uk/planning-search-results/?query=$tempNum";
		my $simpleSearchURL = "https://www.colchester.gov.uk/planning-search/";
		$mech->get($simpleSearchURL);
		my $Page_Content = $mech->content;
		my $Page_url = $mech->uri();
		
		$mech-> submit_form(
				form_number => 3,
				fields      => {
					'Search by Street Name, Postcode or Application Number'	=> $tempNum,
					},
				);
		my $App_Page_Content = $mech->content;
		
		print "$Page_url\n"; 
		open SR, ">${ApplicationNo}_post.html";
		print SR $App_Page_Content;
		close SR;
		<STDIN>;
		
		
		my $postCnt1 = '{"base64SecureConfiguration":"<base64SecureConfiguration>","sortExpression":"<sortExpression>","search":"","page":1,"pageSize":10,"filter":null,"metaFilter":null,"timezoneOffset":-330,"customParameters":[]}';
		my $postCnt = '{"base64SecureConfiguration":"<base64SecureConfiguration>","sortExpression":"<sortExpression>","search":"*<APPNUM>","page":1,"pageSize":10,"filter":null,"metaFilter":null,"timezoneOffset":-330,"customParameters":[]}';
		
		my $postJSONURL	= $1 if($Page_Content=~m/data-get-url=\"(\/_services\/entity-grid-data.json\/[^\"]*?)\"/is);	
		if($postJSONURL!~m/^http/is)
		{
			$postJSONURL="https://www.colchester.gov.uk".$postJSONURL;
			print "$postJSONURL\n"; 
		}
		
		my $Base64SecureConfiguration = $1 if($Page_Content=~m/\&quot\;Base64SecureConfiguration\&quot\;\:\&quot\;([\w\W]*?)\&quot\;/is);	
		my $sortExpression = $1 if($Page_Content=~m/\&quot\;sortExpression\&quot\;\:\&quot\;([^<]*?)\&quot\;/is);	
		
		$postCnt=~s/<base64SecureConfiguration>/$Base64SecureConfiguration/is;
		$postCnt=~s/<sortExpression>/$sortExpression/is;
		$postCnt=~s/<APPNUM>/$tempNum/is;
		
		print "$postCnt\n"; 
		
		$mech->post( $postJSONURL, content => $postCnt1 );
		$mech->add_header( Referer => $simpleSearchURL );
		$mech->add_header( Content-Type => "application/json; charset=utf-8" );
		$mech->add_header( Host => "www.colchester.gov.uk" );
		my $App_Page_Content = $mech->content;
		open SR, ">${ApplicationNo}_post.html";
		print SR $App_Page_Content;
		close SR;
		
		$mech->post( $postJSONURL, content => $postCnt );
		$mech->add_header( Referer => $simpleSearchURL );
		$mech->add_header( Content-Type => "application/json; charset=utf-8" );
		$mech->add_header( Host => "www.colchester.gov.uk" );
		my $App_Page_Content = $mech->content;
		open SR, ">${ApplicationNo}_post1.html";
		print SR $App_Page_Content;
		close SR;
		exit;
		
		
		$postCnt=~s/$Base64SecureConfiguration/<base64SecureConfiguration>/is;
		$postCnt=~s/$sortExpression/<sortExpression>/is;
		$postCnt=~s/$tempNum/<APPNUM>/is;
		
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<br\s*\/?>\s*([\w\W]*?)\s*<\/p>\s*<div\s*class=\"download_box\">/is);	
		my $Application					= &clean($1) if($App_Page_Content=~m/<h1>Application\s*([^<]*?)\s*<\/h1>/is);			
		if($Application eq $ApplicationNo)
		{
			$Application;
		}
		else
		{
			$Application=$ApplicationNo;
		}
		my $Application_Type;
		my $Address						= &clean($1) if($App_Page_Content=~m/<td>\s*Development\s*address\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<td>\s*Applicant\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Applicant_Address			= &clean($1) if($App_Page_Content=~m/<td>\s*Applicant\s*<\/td>\s*<td>\s*<strong>\s*[^<]*?\s*<\/strong>\s*<br\s*\/?>\s*([^<]*?)\s*<\/td>/is);
		my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<td>\s*Agent\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>/is);
		my $Agent_Address				= &clean($1) if($App_Page_Content=~m/<td>\s*Agent\s*<\/td>\s*<td>\s*<strong>\s*[^<]*?\s*<\/strong>\s*<br\s*\/?>\s*([^<]*?)\s*<\/td>/is);
		my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<td>\s*Officer\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*<br\s*\/?>/is);
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/<td>\s*Planning\s*Status\s*<\/td>\s*<td>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*/is);
		my $Date_Application_Received;
		my $Date_Application_Registered;
		my $Date_Application_validated;
		if($App_Page_Content=~m/<td>\s*Key\s*dates\s*<\/td>\s*<td>\s*([\w\W]*?)\s*<\/td>/is)
		{
			$Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<strong>\s*Received\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
			$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<strong>\s*Valid\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
			$Date_Application_Registered= &clean($1) if($App_Page_Content=~m/<strong>\s*Registered\s*<\/strong>\s*\:?\s*([^<]*?)\s*<br\s*\/?>/is);
		}
		my $Agent_Telephone;
		my $TargetDec_dt;
		my $Doc_Url=$Page_url;
		my $Application_Link=$Page_url;
		my $Agent_Email;
		
		$Proposal=~s/\'/\'\'/gsi;	
		$Address=~s/\'/\'\'/gsi;	
		$Applicant_name=~s/\'/\'\'/gsi;	
		$Agent_Name=~s/\'/\'\'/gsi;	
		$Agent_Address=~s/\'/\'\'/gsi;	
		$Applicant_Address=~s/\'/\'\'/gsi;	
		$Application_Status=~s/\'/\'\'/gsi;	
		my $Source = "GCS001";			
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		my $COUNCIL_NAME = "Ribble Valley";
		
		print "ApplicationNo==>$Application\n";
		
		my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\')";
	
		$insert_query=~s/\,$//igs;
		print "insert_query::$insert_query\n";
		
		if($insert_query!~m/values\s*$/is)
		{
			&DB_Insert($dbh,$insert_query);
		}	
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;		
		
	}
	
	
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\n+/ /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}