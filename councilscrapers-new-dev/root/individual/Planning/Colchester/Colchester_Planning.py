# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Planning/Colchester'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	# conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def collectweeklyrls(driver,home_url):
	try:
		#-- Parse
		
		driver.get(home_url)
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		# with open(folder_path+"/160_out.html", 'wb') as fd:
			# fd.write(content)
		# sys.exit()	
		
		weeklyURLs=[]		
		try:
			urlContent=re.findall(r'<div\sclass=\"weekly-link-area\">\s*([\w\W]*?)\s*<\/div>\s*<\/main>', str(content), re.IGNORECASE)
			if re.findall(r'<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*(?:<p>)?[^<]*?\s*(?:<\/p>)?<\/a>', str(urlContent)):
				regionUrlContent=re.findall(r'<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*(?:<p>)?[^<]*?\s*(?:<\/p>)?<\/a>', str(urlContent), re.IGNORECASE) 
				
				for urlMatch in regionUrlContent:
					if not re.findall(r'^http', str(urlMatch)):
						absoluteRegionURL = urljoin(home_url,urlMatch)
						weeklyURLs.append(absoluteRegionURL)
					else:
						weeklyURLs.append(urlMatch)
						
		except Exception as e:
			print e,sys.exc_traceback.tb_lineno 	
		
		return weeklyURLs

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno
		

# To collect Application URLs here
def collectAppURL(weekcontent, home_url):	
	try:
		appURLCollections=[]
		
		appListContent=re.findall(r'<div\sclass=\"view-grid\shas-pagination\">\s*<table[^>]*?>([\w\W]*?)\s*<\/table>\s*<\/div>', str(weekcontent), re.IGNORECASE)	
		if re.findall(r'<td[^>]*?data-th=\"Application\sNumber\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?title=\"View\sdetails\">\s*[^<]*?\s*<\/a>\s*<\/td>', str(appListContent), re.IGNORECASE):
			appUrls=re.findall(r'<td[^>]*?data-th=\"Application\sNumber\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?title=\"View\sdetails\">\s*[^<]*?\s*<\/a>\s*<\/td>', str(appListContent), re.IGNORECASE) 
			
			for appUrl in appUrls:
				if not re.findall(r'^http', str(appUrl)):
					absoluteRegionURL = urljoin(home_url,appUrl)
					appURLCollections.append(absoluteRegionURL)
				else:
					appURLCollections.append(appUrl)
			
		return appURLCollections				
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 
		

# Application parse section	
def detailCollection(appURLsList, CouncilCode, Source, conn, cursor, driver, home_url):
	try:
		appCount = 0
		insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date) values '
		bulkValuesForQuery=''
		
		for appURL in appURLsList:
			print("ApplicationURL::", appURL)
			driver.get(appURL)
			time.sleep(5)
			appcontent=driver.page_source
			
			with open(folder_path+"/160_appcontent.html", 'wb') as fd:
				fd.write(appcontent)

			(Application,ProposalCnt,ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url) = appParseSection(appcontent)
			
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			
			Council_Name = "Colchester"
			joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived)+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated)+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"')"
			
			try:
				print "ApplicationValues: ", joinValues
				bulkValuesForQuery = insertQuery+joinValues
				cursor.execute(bulkValuesForQuery)
				conn.commit()
				
			except Exception as e:
				print ("Detail Inertion Error: ",e)
			
			bulkValuesForQuery=''
						
		print "InsertQuery: ", bulkValuesForQuery

		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application parse section	
def parsesection(weeklyURLsList, CouncilCode, Source, conn, cursor, driver, home_url):
	try:
		finalinsertQuery=''
		for weekURL in weeklyURLsList:
			# weekURL = re.sub(r'id=0', 'id=4', str(weekURL))
			# if re.findall(r'id=0', str(weekURL), re.IGNORECASE):
				# print("Weekly URL:::",weekURL)
				# continue
			if not weekURL:
				print ("Not URL")
			else:
				print("Weekly URL:::",weekURL)

				driver.get(weekURL)
				#-- Wait funtion
				time.sleep(25)
				weekcontent=driver.page_source

				with open(folder_path+"/Content.html", 'wb') as fd:
					fd.write(str(weekcontent))

				pageNumber = 2
				pageNum = 3
				
				fullAppURLs=[]
				try:	
					if re.findall(r'<div\s*class=\"view-pagination\"[^>]*?style=\"display\:\snone\;\">', str(weekcontent)):
						collectAppURLs = collectAppURL(str(weekcontent), home_url)
						print ("collectAppURLs 1:",collectAppURLs)
						finalinsertQuery = detailCollection(collectAppURLs, CouncilCode, Source, conn, cursor, driver, home_url)
						
						if (finalinsertQuery != ''):
							cursor.execute(finalinsertQuery)
							conn.commit()
							
					elif re.findall(r'<li[^>]*?>\s*<a\shref=\"\#\"[^>]*?aria-label=\"Next\spage\"[^>]*?>[^>]*?<\/a>\s*<\/li>', str(weekcontent)):
						collectAppURLs = collectAppURL(str(weekcontent), home_url)	
						fullAppURLs.extend(collectAppURLs)
						check_next_page = re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(weekcontent), re.I)
						print("check_next_page:",check_next_page)
						# raw_input("ELSEIF STOP")
						
						while (check_next_page):				
							
							if re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(weekcontent), re.I):
								
								nextPgeNum = driver.find_element_by_xpath('//*[@id="template-block"]/div/div[2]/div[2]/div[6]/div/ul/li['+str(pageNum)+']/a')	
								driver.execute_script("arguments[0].click();", nextPgeNum)
								# driver.execute_script("arguments[0].click();", driver.find_element_by_xpath('//*[@id="template-block"]/div/div[2]/div[2]/div[6]/div/ul/li['+str(pageNum)+']/a'))

								#-- Wait funtion
								time.sleep(15)  
								appNextPagecontent=driver.page_source
								
								# with open(folder_path+"/160_out2.html", 'wb') as fd:
									# fd.write(appNextPagecontent)
								# sys.exit()
								
								collectNextPageAppURLs = collectAppURL(str(appNextPagecontent), home_url)	
								fullAppURLs.extend(collectNextPageAppURLs)
								pageNumber = pageNumber + 1   
								pageNum = pageNum + 1   
								
								if re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I):
									check_next_page = re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I)
							
									print "pageNumber:",pageNumber              
												 
									if check_next_page != "":
										continue
									else:
										break
							else:
								break
						
						print("fullAppURLs:",fullAppURLs)
						
						finalinsertQuery = detailCollection(fullAppURLs, CouncilCode, Source, conn, cursor, driver, home_url)
						
						if (finalinsertQuery != ''):
							cursor.execute(finalinsertQuery)
							conn.commit()
				except Exception as e:
					print ("*** Content Error ***")
			
		return (finalinsertQuery)

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application details parse section	
def appParseSection(Appcontent):	
	try:
		Application_Number=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?aria-label=\"Application\sNumber\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		DtAppReceived=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_date_received\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		DtAppRegistered=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_registration_date\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		new_target_date=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_target_date\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		new_application_type=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_application_type\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		new_applicant_name=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_applicant_name\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		new_agent_name=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_agent_name\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		Application_Status=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_application_status\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		Address=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_concatenatedaddress\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		Proposal=re.findall(r'<textarea[^>]*?id=\"new_developmentdescription[^>]*?>\s*([\w\W]*?)\s*<\/textarea>', str(Appcontent), re.IGNORECASE)
		Doc_Url=re.findall(r'<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*View\s*Documents\s*<\/a>', str(Appcontent), re.IGNORECASE)
		print ("DtAppReceived:",DtAppReceived)
		(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url) = ('','','','','','','','','','','','','','','','')

		if len(Application_Number) > 0:
			Application=clean(Application_Number[0])

		if len(Proposal) > 0:
			ProposalCnt=clean(Proposal[0])

		if len(Address) > 0:
			AppAddress=clean(Address[0])
			
		if len(Doc_Url) > 0:
			Document_Url=clean(Doc_Url[0])

		if len(Application_Status) > 0:
			ApplicationStatus=clean(Application_Status[0])

		if len(new_applicant_name) > 0:
			ApplicantName=clean(new_applicant_name[0])
			
		if len(new_application_type) > 0:
			ApplicationType=clean(new_application_type[0])
			
		if len(new_agent_name) > 0:
			AgentName=clean(new_agent_name[0])
		
		#DtAppReceived = datetime.strptime(str(DtAppReceived[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
		#DtAppReceived = str(DtAppReceived.strftime("%d/%m/%Y"))

		if len(DtAppReceived) > 0:
			DtAppReceived = datetime.strptime(str(DtAppReceived[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
			DtAppReceived = str(DtAppReceived.strftime("%d/%m/%Y"))
			DateApplicationReceived=DtAppReceived
			
		#DtAppRegistered = datetime.strptime(str(DtAppRegistered[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
		#DtAppRegistered = str(DtAppRegistered.strftime("%d/%m/%Y"))
		if len(DtAppRegistered) > 0:
			DtAppRegistered = datetime.strptime(str(DtAppRegistered[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
			DtAppRegistered = str(DtAppRegistered.strftime("%d/%m/%Y"))
			DateApplicationRegistered=DtAppRegistered

		#new_target_date = datetime.strptime(str(new_target_date[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
		#new_target_date = str(new_target_date.strftime("%d/%m/%Y"))

		if len(new_target_date) > 0:
			new_target_date = datetime.strptime(str(new_target_date[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
			new_target_date = str(new_target_date.strftime("%d/%m/%Y"))
			TargetDecdt=new_target_date
			
		return (Application,ProposalCnt,ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url)    

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
		
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
	
	# conn = dbConnection("GLENIGAN")	
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	# browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Live_Schedule/Jar/chromedriver.exe')
	browser.maximize_window()
	home_url = 'https://www.colchester.gov.uk/received-applications-last-6-months/'
	
	resultURLs = collectweeklyrls(browser,home_url)
	
	finalQuery = parsesection(resultURLs, councilcode, 'GCS001', conn, cursor, browser, home_url)
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()
	print ("finalQuery::", finalQuery)
	if (finalQuery != ''):
		try:
			cursor.execute(finalQuery)
			conn.commit()
		except Exception as ex:
			print ex,sys.exc_traceback.tb_lineno
	
	browser.quit()