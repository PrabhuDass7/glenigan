import requests
import json
import sys,os,re
import pandas as pd
from datetime import datetime, timedelta
import pymssql
import configparser
import time

''' Create Log Directory'''
basePath = os.getcwd()
print ("basePath::",basePath)
Config = configparser.ConfigParser()
Config.read(str(basePath)+'\\'+str('Agile_Planning.ini'))

todaydate = time.strftime('%Y%m%d')
logDirectory=basePath+"/log/"+todaydate
if os.path.isdir(str(logDirectory)) == False:
	os.makedirs(str(logDirectory))

reload(sys)
sys.setdefaultencoding("utf-8")

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

proxies2 = {
  'http': 'http://172.27.137.199:3128',
  'https': 'http://172.27.137.199:3128',
}



# dbConnection Section
def dbConnection(database):
    conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
    return (conn)


def proxiesGenerator(proxies1,proxies2):    
    i = 0
    while i <= 1:
        try:
            print("Main URL is: ", Config.get(councilcode,'home_url'))
            res = requests.get(Config.get(councilcode,'home_url'), proxies=proxies1)			
            if res.status_code == 200:				
                return proxies1
                
        except Exception as ex:
            print("Error is: ", str(type(ex).__name__))
            
            if str(type(ex).__name__) == "ProxyError":
                while i <= 2:
                    print("Now trying in second Proxy", proxies2)
                    res = requests.get(Config.get(councilcode,'home_url'), proxies=proxies2)
                    print("second proxy URL status is: ", res.status_code)
                    if res.status_code == 200:
                        return proxies2
                    else:
                        i = i + 1	

def normalize_data_struct(dict_data):
    fields = ['areaId', 'wardId', 'parishId']
    for field in fields:
        value = dict_data.get(field)
        dict_data[field] = ", ".join([str(v) for v in value]) if value else None
    return dict_data
        
# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, councilcode, Source):
    try:	
        #Main Url
        mainUrl = Config.get(councilcode,'home_url')	
        finProxy = proxiesGenerator(proxies1,proxies2)
        
        print("Final working proxy is: ", finProxy)
        
        s = requests.session()
        headers = {
            "Accept": "application/json, text/plain, */*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Host": Config.get(councilcode,"host"),
            "Origin": Config.get(councilcode,"origin"),
            "Pragma": "no-cache",
            "Referer": Config.get(councilcode,"referer"),
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-site",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
            "x-client": Config.get(councilcode,"x_client"),
            "x-product": "CITIZENPORTAL",
            "x-service": "PA"
        }

        s.headers.update(headers)
	
        response = s.get(Config.get(councilcode,"search_date_url")+str(dumFrom_Date)+'&registrationDateTo='+str(dumTo_Date), proxies= finProxy,verify=False)

        json_page_source = response.json()
        results = json_page_source.get('results')

        filename=logDirectory+"/"+councilcode+"_"+Source+".html"
        with open(filename, 'wb') as fd:
            fd.write(response.content)

        # print(results)
        if isinstance(results, list):
            appCount = 0
            insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Easting, Northing) values '
            bulkValuesForQuery=''	
            
            for row in results:
                applicationid = clean(str(row['id']))
                application = clean(str(row['reference']))
                # print("applicationid==>",applicationid)
                print("application==>",application)
                headers = {
                    "Accept": "application/json, text/plain, */*",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
                    "Cache-Control": "no-cache",
                    "Connection": "keep-alive",
                    "Host": Config.get(councilcode,"host"),
                    "Origin": Config.get(councilcode,"origin"),
                    "Pragma": "no-cache",
                    "Referer": Config.get(councilcode,"referer"),
                    "Sec-Fetch-Mode": "cors",
                    "Sec-Fetch-Site": "same-site",
                    "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
                    "x-client": Config.get(councilcode,"x_client"),
                    "x-product": "CITIZENPORTAL",
                    "x-service": "PA"
                }
                applicationUrl = Config.get(councilcode,"application_detail_url") + str(applicationid)
        
                
                # print("applicationURL==>",applicationUrl)
                s.headers.update(headers)

                response = s.get(applicationUrl, proxies= finProxy,verify=False)
                AppJsonContent = json.loads(response.content)
                try:
                    application = clean(str(AppJsonContent['reference']))
                except Exception as e:
                    application=''
                print("application",application)
                # applicationStatus = clean(str(AppJsonContent['statusOwner'].encode('ascii', 'ignore')))
                try:
                    applicationStatus = clean(str(AppJsonContent['statusDescriptionOwner'].encode('ascii', 'ignore')))
                except Exception as e:
                    applicationStatus=''
                try:
                    proposal = clean(str(AppJsonContent['fullProposal'].encode('ascii', 'ignore')))
                except Exception as e:
                    proposal=''
                try:
                    location = clean(str(AppJsonContent['location'].encode('ascii', 'ignore').replace('\r', ' ')))
                except Exception as e:
                    location=''
                try:
                    applicantName = clean(str(AppJsonContent['applicantSurname']))		
                except Exception as e:
                    applicantName=''	
                try:
                    agentName = clean(str(AppJsonContent['agentSurname']))
                except Exception as e:
                    agentName=''
                try:
                    dateValid = clean(str(AppJsonContent['registrationDate']))
                except Exception as e:
                    dateValid=''
                try:
                    northings = clean(str(AppJsonContent['northing']))
                except Exception as e:
                    northings=''
                try:
                    eastings = clean(str(AppJsonContent['easting']))
                except Exception as e:
                    eastings=''
                try:
                    applicationType = clean(str(AppJsonContent['applicationType']))
                except Exception as e:
                    applicationType=''

                Council_Name=Config.get(councilcode,"council_name")
                
                # agentAddress = ''
                # dateReceived=''
                # targetdecdt=''
                # dateRegistered=''
                format1 = "%Y/%m/%d %H:%M"	
                Schedule_Date = datetime.now().strftime(format1)
                
            
                Source_With_Time = Source+"_"+Schedule_Date+"-perl"
                match = re.search(r'^R\d+', str(application), re.IGNORECASE)
                
                appUrl = Config.get(councilcode,"application_url") + str(applicationid)

                documentUrl = appUrl
                joinValues="("+"'"+str(councilcode)+"','"+str(Council_Name)+"','"+str(location)+"','','"+str(application)+"','','"+str(dateValid)+"','"+str(proposal)+"','"+str(applicationStatus)+"','','','','"+str(agentName)+"','','','"+str(applicantName)+"','"+str(applicationType)+"','','','','','','','','','','','','','"+str(appUrl)+"','"+str(documentUrl)+"','','"+Source_With_Time+"','"+str(Schedule_Date)+"','"+str(eastings)+"','"+str(northings)+"')"
               
                print("ApplicationValues: ", joinValues)

                if appCount == 0:
                    bulkValuesForQuery = insertQuery+joinValues
                    appCount += 1
                elif appCount == 10:
                    bulkValuesForQuery = bulkValuesForQuery+","+joinValues
                    cursor.execute(bulkValuesForQuery)
                    conn.commit()
                    appCount=0
                    bulkValuesForQuery=''
                else:
                    bulkValuesForQuery = bulkValuesForQuery+","+joinValues
                    appCount += 1		
                    
                
            return bulkValuesForQuery
                # with open("output.html", 'wb') as handle:
                    # handle.write(str(AppJsonContent))
                # sys.exit()
            
    except Exception as ex:
        print("Post method response is: ", ex,sys.exc_traceback.tb_lineno)

# Clean function
def clean(cleanValue):
    try:
        clean=''
        clean = re.sub(r'\n', "", str(cleanValue)) 
        clean = re.sub(r'\'', "''", str(clean)) 
        clean = re.sub(r'\t', "", str(clean))
        clean = re.sub(r'\\', "", str(clean))
        clean = re.sub(r'\&nbsp\;', " ", str(clean))
        clean = re.sub(r'\&amp\;', "&", str(clean))
        clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
        clean = re.sub(r'^\s+|\s+$', "", str(clean))
        clean = re.sub(r'\s\s+', " ", str(clean))
        clean = re.sub(r'^\W+$', "", str(clean))
        clean = re.sub(r'\&\#39\;', "'", str(clean))
        clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
        clean = re.sub(r'\'', "''", str(clean))
        clean = re.sub(r'^\s*', "", str(clean))
        clean = re.sub(r'\s*$', "", str(clean))
        
        return clean    

    except Exception as ex:
        print(ex,sys.exc_traceback.tb_lineno)	
        
        

# Main section	
if __name__== "__main__":
    councilcode = sys.argv[1]
    gcs = sys.argv[2]	
    
    conn = dbConnection("SCREENSCRAPPER")	
    cursor = conn.cursor()
    
    # GCS calculation
    thisgcs =	{
      "GCS001": "0",
      "GCS002": "25",
      "GCS003": "51",
      "GCS004": "77",
      "GCS005": "103",	
      "GCS006": "129",
      "GCS007": "155",
    }	
    
    gcsdate = thisgcs[gcs]		
    date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
    
    format = "%Y-%m-%d"
    
    todate = date_N_days_ago.strftime(format)
    
    preday = date_N_days_ago - timedelta(days=26)
    fromdate = preday.strftime(format)
    
    print('fromdate  :', fromdate)
    print('todate    :', todate)
    
    # application extraction
    finalinsertQuery = inputsection(fromdate, todate, councilcode, gcs)
    
    # final output query insertion
    if (finalinsertQuery != '') or finalinsertQuery is not None:
        cursor.execute(finalinsertQuery)
        conn.commit()	