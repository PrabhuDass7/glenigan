use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $inFrom_Date = $ARGV[1];
my $inTo_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"197\"\)", 16, "Error message");
    exit();
}

$inFrom_Date=~s/\//%2F/gsi;
$inTo_Date=~s/\//%2F/gsi;


print "Council_Code: $Council_Code\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();


my $COUNCIL_NAME="Elmbridge";
print "COUNCIL_NAME	: $COUNCIL_NAME\n";


my $HOME_URL="http://emaps.elmbridge.gov.uk/ebc_planning.aspx?pageno=1&template=AdvancedSearchResultsTab.tmplt&requestType=parseTemplate&USRN%3APARAM=&apptype%3APARAM=&status%3APARAM=&decision%3APARAM=&ward%3APARAM=&txt_search%3APARAM=&daterec_from%3APARAM=$inFrom_Date&daterec_to%3APARAM=$inTo_Date&datedec_from%3APARAM=&datedec_to%3APARAM=&pagerecs=5000&orderxyz%3APARAM=REG_DATE_DT%3ADESCENDING&SearchType%3APARAM=Advanced";

print "URL		: $HOME_URL\n";


#### UserAgent Declaration ####	
my $mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
    verify_hostname => 0, 
	autoclose => 1,
});

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}


# Get search results using date ranges

$mech->get($HOME_URL);
my $content = $mech->content;
my $code = $mech->status;
print "Search Page response==>$code\n";

my $count=0;
while($content=~m/<a[^>]*?title=\"Application\s*number\s*/gsi)
{
	$count++;
}
my $pageCount = $count;

print "TotalApp Count==>$pageCount\n";


my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';
my $dupQury;
while($content=~m/<tr>\s*<td>\s*([^<]*?)\s*<\/td>[\w\W]*?<a[^>]*?title=\"Application\s*number\s*[^>]*?href=\"([^\"]*?)\"[^>]*?>/gsi)
{
	my $application = $1;
	my $applicationURL = $2;
	print "application==>$application\n";
	# print "applicationURL==>$applicationURL\n";
	$applicationURL=~s/\&amp\;/\&/gsi;
	
	$mech->get($applicationURL);
	my $appContent = $mech->content;
	
	my $insertQuery = &Scrape_Details($appContent,$application,$applicationURL);
	$dupQury .= $insertQuery;
	open(PP, ">>SuccessQuery.txt");
	print PP "$insertQuery";
	close(PP);
	# print "insert_query::$insert_query\n"; <STDIN>;
}

$insert_query = $insert_query.' '.$dupQury;
	
$insert_query=~s/\,$//igs;

if($insert_query!~m/values\s*$/is)
{
	&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
}


sub Scrape_Details()
{
	my $App_Page_Content = shift;
	my $Application = shift;
	my $applicationURL = shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $dumContent = $App_Page_Content;
	my $insert_query;
	if($dumContent=~m/<ul\s*id\=\"navlist\">\s*([\w\W]*?)\s*<\/ul>/is)
	{	
		my $block1=$1;
		my ($block2,$contacts_url,$agent_url,$date_url,$Doc_Url);
		
		while($block1=~m/<li>([\w\W]*?)<\/li>/igs)
		{
			$block2=$1;
			
			$contacts_url 			= $1 if ( $block2 =~ m/<a\s*href\=\"([^>]*?)\">\s*Contacts\s*<\/a>/is);
			$date_url 				= $1 if ( $block2 =~ m/<a\s*href\=\"([^>]*?)\">\s*Key\s*Dates\s*<\/a>/is);
			$Doc_Url 				= $1 if ( $block2 =~ m/<a\s*href\=\"([^>]*?)\">\s*Plans\s*\&\s*Documents\s*<\/a>/is);
			
			$contacts_url=~s/\&amp\;/\&/gsi;
			$date_url=~s/\&amp\;/\&/gsi;
			$Doc_Url=~s/\&amp\;/\&/gsi;
		}
		
		if($contacts_url!~m/https?\:/is)
		{
			$contacts_url = URI::URL->new($contacts_url)->abs( 'http://emaps.elmbridge.gov.uk/ebc_planning.aspx?requesttype=parseTemplate&template=AdvancedSearchTab.tmplt', 1 );
		}	
		else
		{
			$contacts_url;
		}
		if($date_url!~m/https?\:/is)
		{
			$date_url = URI::URL->new($date_url)->abs( 'http://emaps.elmbridge.gov.uk/ebc_planning.aspx?requesttype=parseTemplate&template=AdvancedSearchTab.tmplt', 1 );
		}	
		else
		{
			$date_url;
		}	
		if($Doc_Url!~m/https?\:/is)
		{
			$Doc_Url = URI::URL->new($Doc_Url)->abs( 'http://emaps.elmbridge.gov.uk/ebc_planning.aspx?requesttype=parseTemplate&template=AdvancedSearchTab.tmplt', 1 );
		}	
		else
		{
			$Doc_Url;
		}	
		
		$mech->get($contacts_url);
		my $contacts_url_Content = $mech->content;
		$mech->get($date_url);
		my $date_url_Content = $mech->content;
					
		my $newApplication					= &clean($1) if ( $App_Page_Content =~ m/<h1>\s*Planning\s*Applications\s*<\/h1>\s*<h2>\s*([^>]*?)\s*\-/is);
		my $Address							= &clean($1) if ( $App_Page_Content =~ m/<h1>\s*Planning\s*Applications\s*<\/h1>\s*<h2>\s*[^>]*?\s*\-\s*([^>]*?)\s*<\/h2>/is);
		my $Proposal						= &clean($1) if ( $App_Page_Content =~ m/<dt>\s*Description\s*\:\s*<\/dt>\s*<dd>\s*([\w\W]*?)\s*<\/dd>/is);
		my $Application_Type				= &clean($1) if ( $App_Page_Content =~ m/<dt>\s*Application\s*Type\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Application_Status  			= &clean($1) if ( $App_Page_Content =~ m/<dt>\s*Status\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Applicant_name					= &clean($1) if ( $contacts_url_Content =~ m/<dt>\s*Applicant\s*Name\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Applicant_Address				= &clean($1) if ( $contacts_url_Content =~ m/<dt>\s*Applicant\s*Address\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Agent_Name						= &clean($1) if ( $contacts_url_Content =~ m/<dt>\s*Agent\s*Name\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Agent_Address					= &clean($1) if ( $contacts_url_Content =~ m/<dt>\s*Agent\s*Address\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Agent_Tel						= &clean($1) if ( $contacts_url_Content =~ m/<dt>\s*Phone\s*Number\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Date_Application_Received		= &clean($1) if ( $date_url_Content =~ m/<dt>\s*Received\s*On\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Date_Application_Validated		= &clean($1) if ( $date_url_Content =~ m/<dt>\s*Valid\s*from\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Target_Dt						= &clean($1) if ( $date_url_Content =~ m/<dt>\s*Target\s*For\s*Decision\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		my $Date_Application_Registered		= &clean($1) if ( $date_url_Content =~ m/<dt>\s*Registere?d?\s*On\s*\:?\s*<\/dt>\s*<dd>\s*([^>]*?)\s*<\/dd>/is);
		
		my $Agent_Telephone;
		my $Applicant_Address;
		my $Agent_Address;		
		my $Application_Link=$applicationURL;
		my $Agent_Email;
		
		if($Application eq "")
		{	
			if($newApplication ne "")
			{
				$Application = $newApplication;
			}
		}
		if($Doc_Url eq "")
		{	
			$Doc_Url=$applicationURL;
		}
		
		$Application_Link=~s/\'/\'\'/igs;
		$Doc_Url=~s/\'/\'\'/igs;
		
		my $Source = 'GCS001';	
		
		
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$insert_query ="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_Validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$Target_Dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
		
		# print "insert_query::$insert_query\n";
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_Validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $Target_Dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
	}
	
	return ($insert_query);
	
}



###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}


######  Clean  ######
sub clean()
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	
	return($Data);
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}