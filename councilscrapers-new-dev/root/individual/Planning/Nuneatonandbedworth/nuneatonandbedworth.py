# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin


reload(sys)
sys.setdefaultencoding("utf-8")

folder_path = 'C:/Glenigan/Live_Schedule/Planning/Nuneatonandbedworth'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		driver.maximize_window()
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		advfinalCnt=content.encode("utf-8")


		driver.find_element_by_xpath('//*[@id="content"]/div[1]/p[2]/a').click()
		#Need to wait for the page to load
		time.sleep(10)
		main_content=driver.page_source
		
		with open(folder_path+"/302_out.html", 'wb') as fd:
			fd.write(main_content.encode("utf-8"))
		# raw_input()
		# driver.find_element_by_xpath('//*[@id="Mainpage_optValid"]').send_keys('optValid')
				
		#-- Wait funtion
		# time.sleep(10)  

		
		return searchfinalCnt

	except Exception as e:
		print(e,sys.exc_traceback.tb_lineno)





# Clean function
def clean(cleanValue):
	try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

	except Exception as ex:
		print(ex,sys.exc_traceback.tb_lineno)

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	# PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	driver = webdriver.PhantomJS("C://Glenigan//Live_Schedule//phantomjs//phantomjs.exe")
	
	
	
	homeURL = 'https://www.nuneatonandbedworth.gov.uk/info/20025/planning_and_building_control/54/planning/2'
	
	results = inputsection(driver, homeURL)

	# finalinsertQuery = nextPageSection(results, homeURL, driver, councilcode, conn, cursor)
	# print type(finalinsertQuery)
	
	# if (finalinsertQuery != ''):
		# cursor.execute(finalinsertQuery)
		# conn.commit()
	
	driver.quit()