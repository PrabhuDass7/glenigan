use strict;
use WWW::Mechanize;
use WWW::Mechanize::Firefox;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;
use Cwd qw(abs_path);
use Cwd;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"142\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


my $dir = getcwd;
my $logFolder="$dir/Log";

my $time = Time::Piece->new;
my $date = $time->strftime('%d%m%Y');
my $dataFolder="$logFolder/$date";

&createdirectory($logFolder);
&createdirectory($dataFolder);

# Establish connection with DB server

my $dbh = &Glenigan_DB_Windows::DB_Planning();

system('"C:/Program Files/Mozilla Firefox/firefox.exe"');
sleep(10);
### Get Council Details from ini file ###

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:\\Glenigan\\Live_Schedule\\Planning\\Carlisle_Planning\\Carlisle_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
my $FORM_START_ID = $Config->{$Council_Code}->{'FORM_START_ID'};
my $FORM_END_ID = $Config->{$Council_Code}->{'FORM_END_ID'};

#### UserAgent Declaration ####
reping:

my ($mech,$mechz);
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize::Firefox->new(autocheck => 0);
}
else
{	
	# $mech = WWW::Mechanize->new(  tab => 'current',ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize::Firefox->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			autoclose => 1,
			});
	
	$mechz = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);	
}	

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

# Get search results using date ranges

my ($Responce, $javascript, $Ping_Status1);
$Responce = $mech->get($HOME_URL);
sleep(10);

my $contents = $mech->content;
my $code = $mech->status;

goto reping if($code!=200);

$mech->form_number($FORM_NUMBER);
$mech->set_fields($FORM_START_ID => $From_Date);
$mech->set_fields($FORM_END_ID => $To_Date);
sleep(5);

$mech->click({ xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' });
sleep(10);

my $retries = 5;
while ($retries-- and $mech->is_visible( xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' )) {
	  print "Loading****\n";
	  eval{
		$mech->click({ xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' });
		};
		if($@)
		{
			print "Error:: $@";
		}
		sleep(5);
};

sleep(20);

my $content = $mech->content;
my $code = $mech->status;

#--------- Select Search Criteria -------------
sleep(5);
$mech->form_number($FORM_NUMBER);
$mech->select('searchCriteria.resultsPerPage', '100');
$mech->submit();
sleep(60);
my $content = $mech->content;
my $code = $mech->status;

sleep(7);

open HH,">$dataFolder/$Date_Range.html";
print HH "$content";
close HH;

my @Detail_Links = ();
my ($Detail_Links) =&LinkCollection($content);
@Detail_Links = @$Detail_Links;

my $totalPage = $1 if($content=~m/$TOTAL_PAGE_COUNT/is);

my $i=2;

next_page:
if($i<=$totalPage)
{	
	print "$i======$totalPage\n";
	my $temp_var=$i;
	eval
	{
		$mech->click({ selector => '.next', 1 });
		sleep(60);
		my $datePage_url = $mech->uri();
		my $nextpagecontent = $mech->content;
		
		open HH,">>$dataFolder/$Date_Range.html";
		print HH "$nextpagecontent";
		close HH;
		
		my ($Detail_Links) = &LinkCollection($nextpagecontent);
		@Detail_Links = @$Detail_Links;
		if($nextpagecontent=~m/$TOTAL_PAGE_COUNT/is)
		{
			$totalPage = $1;
		}else{
			$totalPage = 0;
		}
		$i++;
	}; 
	if($@)
	{ 
		print "Error:: $@";
		sleep(10);
		
		goto reping if($temp_var == $i);
	}
	
	goto next_page;
}

my @Detail_Links = uniq(@Detail_Links);

&Scrape_Details( $Council_Code,\@Detail_Links );

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

sub Scrape_Details()
{
	my $Council_Code = shift;
	my $Detl_Links = shift;
	my @Detail_Links = @{$Detl_Links};
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
    my $Search_Results_Count = scalar(@Detail_Links);
	
	my $code;
	
	my $resultsCount=0;
	foreach my $Application_Link(@Detail_Links)
	{
		print "Application_Link==>$Application_Link\n";
		
		my $Application; my $Proposal; my $Application_Status; my $Date_Decision_Made; my $Decision_Status;
		my $Source = $Date_Range;
		
		my $count_c=0;
		repingg:
		
		# my ($code,$App_Page_Content)=&getMechCont($Application_Link);
		$mechz->get($Application_Link);
		my $code= $mechz->status();
		my $App_Page_Content = $mechz->content;

		if(($code !=200) && ($count_c < 3))
		{
			$count_c++;
			goto repingg;
		}
		
		# open(PP,">App_Page_Content.html");
		# print PP "$App_Page_Content\n";
		# close(PP);
		
		open HH,">>$dataFolder/StatusCode_$Date_Range.txt";
		print HH "$Application_Link\t$code\n";
		close HH;

		my $categoryType='Planning';
		my $insertQuery;
		if(($Council_Code=~m/^142$/is) && ($code ==200))
		{
			my %dataDetails;
			
			my ($dataDetails,$applicationPageContentResponse, $appNumber) = &getApplicationDetails($Application_Link,$mechz, $categoryType,$Council_Code);
		
			%dataDetails = %{$dataDetails};
			
			if($applicationPageContentResponse=~m/^\s*(200|ok)\s*$/is)
			{
				$resultsCount++;
			}	
			
			if($appNumber ne "")
			{
				my ($insert_query_From_Func) = &applicationsDataDumpPlanning(\%dataDetails,$Application_Link,$Source,$Council_Code,$COUNCIL_NAME,$Schedule_Date,$Schedule_Date);
				
				$insertQuery="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Grid_Reference, Easting, Northing) values ".$insert_query_From_Func;
			}
		}
		
		print "insertQuery:: $insertQuery\n";
		
		if($insertQuery!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insertQuery);
		}
	}
}


sub getMechCont()
{
	my $Link = shift;

	$mech->get($Link);
	sleep(7);
	my $content = $mech->content;
	
	my $code= $mech->status();
	print "CODE :: $code\n";
	my $Link_Reset_Status = $mech->success();
	
	return ($code,$content);
}

sub LinkCollection()
{
	my $page_cnt = shift;
	while($page_cnt=~m/<li\s*class\=\"searchresult\">\s*<a\s*href\=\"([^>]*?)\">/igs)
	{
		my $App_Link = $1;
		
		my $Application_Link;
		if($App_Link!~m/^\s*$/is)
		{
			if($App_Link!~m/https?\:/is)
			{
				$Application_Link = $FILTER_URL.$App_Link;
			}	
			else
			{
				$Application_Link = $App_Link;
			}	
		}
		$Application_Link=~s/&amp;/&/igs;
		print "Application_Link:: $Application_Link\n";
		push(@Detail_Links,$Application_Link);		
	}
	return(\@Detail_Links);
}

sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;
	return($data2Clean);
}

sub clean()
{
	my $Data=shift;
	
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime) = @_;	
	
	my %activeTabContent = %{$TabContent};
	
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	foreach my $AppDataKey (keys %activeTabContent)
	{				
		# print "$AppDataKey<==>$activeTabContent{$AppDataKey}[0]\n";<STDIN>;
		
		my ($ACTUAL_COMMITTEE_DATE, $ACTUAL_COMMITTEE_OR_PANEL_DATE, $ADVERTISEMENT_EXPIRY_DATE, $AGREED_EXPIRY_DATE, $APPLICATION_EXPIRY_DEADLINE, $APPLICATION_VALID_DATE, $APPLICATION_VALIDATED_DATE, $CLOSING_DATE_FOR_COMMENTS, $COMMENTS_TO_BE_SUBMITTED_BY, $COMMITTEE_DATE, $COMMITTEE_DELEGATED_LIST_DATE, $CONSULTATION_END_DATE, $CONSULTATION_EXPIRY_DATE, $CONSULTATION_PERIOD_EXPIRES, $DATE_APPLICATION_VALID, $DECISION_DATE, $DECISION_ISSUED_DATE1, $DECISION_MADE_DATE, $DECISION_NOTICE_DATE, $DECISION_PRINTED_DATE, $DETERMINATION_DEADLINE, $EARLIEST_DECISION_DATE, $ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED, $EXPIRY_DATE, $EXPIRY_DATE_FOR_COMMENT, $LAST_ADVERTISED_IN_PRESS_DATE, $LAST_DATE_FOR_COMMENTS, $LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE, $LAST_DATE_FOR_NEIGHBOUR_COMMENTS, $LAST_DATE_FOR_NEIGHBOURS_RESPONSES, $LAST_SITE_NOTICE_POSTED_DATE, $LATEST_ADVERTISEMENT_EXPIRY_DATE, $LATEST_NEIGHBOUR_CONSULTATION_DATE, $LATEST_SITE_NOTICE_EXPIRY_DATE, $NEIGHBOUR_CONSULTATION_EXPIRY_DATE, $NEIGHBOURS_LAST_NOTIFIED, $OVERALL_CONSULTATION_EXPIRY_DATE, $OVERALL_DATE_OF_CONSULTATION_EXPIRY, $PERMISSION_EXPIRY_DATE, $PUBLIC_CONSULTATION_EXPIRY_DATE, $SITE_NOTICE_EXPIRY_DATE, $STANDARD_CONSULTATION_DATE, $STANDARD_CONSULTATION_EXPIRY_DATE, $STATUTORY_DETERMINATION_DATE, $STATUTORY_DETERMINATION_DEADLINE, $STATUTORY_EXPIRY_DATE, $TARGET_DATE, $TARGET_DETERMINATION_DATE, $TEMPORARY_PERMISSION_EXPIRY_DATE, $VALID_DATE, $WEEKLY_LIST_EXPIRY_DATE, $AGENT_EMAIL, $AGENT_TELEPHONE1, $AGENT_MOBILE, $HOME_PHONE, $AGENT_FAX, $MOBILE_NUMBER, $PHONE_NUMBER, $TELEPHONE_NUMBER, $HOME_PHONE_NUMBER, $AGENT_ADDRESS1, $PERSONAL_EMAIL, $PERSONAL_PHONE, $COMPANY_PHONE, $WORK_PHONE, $EMAIL_ADDRESSE, $FAX_NO, $COMPANY_FAX, $EMAIL_ADDRESS, $TELEPHONE, $PERSONAL_MOBILE, $ELECTRONIC_MAIL, $WORK_PHONE_NUMBER, $PHONE_NO, $OFFICE_PHONE_NUMBER, $MAIN_PHONE, $PHONE_USED_BY_ALL_TEMPLATES, $PHONE_CONTACT_NUMBER, $AGENT_CONTACT_DETAILS, $AGENT_CONTACT_NAME, $ACKNOWLEDGEMENT_EMAIL, $EMAIL, $MOBILE, $E_MAIL_ADDRESS, $ACTUAL_DECISION_LEVEL, $AGENT_ADDRESS, $AGENT_COMPANY_NAME, $AGENT_NAME, $AGENT_TELEPHONE, $AMENITY_SOCIETY, $APPLICANT_ADDRESS, $APPLICANT_NAME, $APPLICATION_TYPE, $CASE_OFFICER, $COMMUNITY_COUNCIL, $DECISION_STATUS, $DISTRICT_REFERENCE, $ENVIRONMENTAL_ASSESSMENT_REQUESTED, $ENVIRONMENTAL_ASSESSMENT_REQUIRED, $EXPECTED_DECISION_LEVEL, $NEIGHBOURHOOD_PARTNERSHIP_AREA, $PARISH, $WARD, $ADDRESS, $ALTERNATIVE_REFERENCE, $APPEAL_DECISION, $DATE_APPLICATION_RECEIVED, $DATE_APPLICATION_VALIDATED, $DATE_APPLICATION_REGISTERED, $APPLICATION_REFERENCE, $APPLICATION_RECEIVED_DATE, $CASE_REFERENCE, $APPLICATION_REGISTERED_DATE, $DECISION_ISSUED_DATE, $LOCATION, $PLANNING_PORTAL_REFERENCE, $PROPOSAL, $APPLICATION, $APPLICATION_STATUS, $NO_DOCUMENT, $NO_CASE, $NO_PROPERTY, $SUMMARY_ADDRESS, $SUMMARY_PROPOSAL, $SUMMARY_CASENUMBER, $DOC_URL, $DOC_URL1, $DOC_URL2, $DOC_URL3);
		
		### Summary ###
		$ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ADDRESS$/is);
		$ALTERNATIVE_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ALTERNATIVE_REFERENCE$/is);
		$APPEAL_DECISION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPEAL_DECISION$/is);
		$DATE_APPLICATION_RECEIVED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_RECEIVED$/is);
		$DATE_APPLICATION_VALIDATED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_VALIDATED$/is);
		$DATE_APPLICATION_REGISTERED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_REGISTERED$/is);
		$APPLICATION_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_REFERENCE$/is);
		$APPLICATION_RECEIVED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_RECEIVED_DATE$/is);
		$CASE_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CASE_REFERENCE$/is);
		$APPLICATION_REGISTERED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_REGISTERED_DATE$/is);
		$DECISION_ISSUED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATE$/is);
		$LOCATION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LOCATION$/is);
		$PLANNING_PORTAL_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PLANNING_PORTAL_REFERENCE$/is);
		$PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PROPOSAL$/is);
		$APPLICATION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION$/is);
		$APPLICATION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_STATUS$/is);
		$NO_DOCUMENT = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NO_DOCUMENT$/is);
		$NO_CASE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NO_CASE$/is);
		$NO_PROPERTY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NO_PROPERTY$/is);
		$SUMMARY_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_ADDRESS$/is);
		$SUMMARY_PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_PROPOSAL$/is);
		$SUMMARY_CASENUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_CASENUMBER$/is);
		$DOC_URL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL$/is);
		$DOC_URL1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL1$/is);
		$DOC_URL2 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL2$/is);
		$DOC_URL3 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DOC_URL3$/is);
		
		### Details ###
		$ACTUAL_DECISION_LEVEL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACTUAL_DECISION_LEVEL$/is);
		$AGENT_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_ADDRESS$/is);
		$AGENT_COMPANY_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_COMPANY_NAME$/is);
		$AGENT_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_NAME$/is);
		$AGENT_TELEPHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_TELEPHONE$/is);
		$AMENITY_SOCIETY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AMENITY_SOCIETY$/is);
		$APPLICANT_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICANT_ADDRESS$/is);
		$APPLICANT_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICANT_NAME$/is);
		$APPLICATION_TYPE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_TYPE$/is);
		$CASE_OFFICER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CASE_OFFICER$/is);
		$COMMUNITY_COUNCIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMUNITY_COUNCIL$/is);
		$DECISION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_STATUS$/is);
		$DISTRICT_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DISTRICT_REFERENCE$/is);
		$ENVIRONMENTAL_ASSESSMENT_REQUESTED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ENVIRONMENTAL_ASSESSMENT_REQUESTED$/is);
		$ENVIRONMENTAL_ASSESSMENT_REQUIRED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ENVIRONMENTAL_ASSESSMENT_REQUIRED$/is);
		$EXPECTED_DECISION_LEVEL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EXPECTED_DECISION_LEVEL$/is);
		$NEIGHBOURHOOD_PARTNERSHIP_AREA = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NEIGHBOURHOOD_PARTNERSHIP_AREA$/is);
		$PARISH = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PARISH$/is);
		$WARD = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WARD$/is);
		
		### Contact ###
		$AGENT_EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_EMAIL$/is);
		$AGENT_TELEPHONE1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_TELEPHONE1$/is);
		$AGENT_MOBILE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_MOBILE$/is);
		$HOME_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^HOME_PHONE$/is);
		$AGENT_FAX = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_FAX$/is);
		$MOBILE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^MOBILE_NUMBER$/is);
		$PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_NUMBER$/is);
		$TELEPHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TELEPHONE_NUMBER$/is);
		$HOME_PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^HOME_PHONE_NUMBER$/is);
		$AGENT_ADDRESS1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_ADDRESS1$/is);
		$PERSONAL_EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERSONAL_EMAIL$/is);
		$PERSONAL_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERSONAL_PHONE$/is);
		$COMPANY_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMPANY_PHONE$/is);
		$WORK_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WORK_PHONE$/is);
		$EMAIL_ADDRESSE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EMAIL_ADDRESSE$/is);
		$FAX_NO = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^FAX_NO$/is);
		$COMPANY_FAX = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMPANY_FAX$/is);
		$EMAIL_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EMAIL_ADDRESS$/is);
		$TELEPHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TELEPHONE$/is);
		$PERSONAL_MOBILE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERSONAL_MOBILE$/is);
		$ELECTRONIC_MAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ELECTRONIC_MAIL$/is);
		$WORK_PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WORK_PHONE_NUMBER$/is);
		$PHONE_NO = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_NO$/is);
		$OFFICE_PHONE_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^OFFICE_PHONE_NUMBER$/is);
		$MAIN_PHONE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^MAIN_PHONE$/is);
		$PHONE_USED_BY_ALL_TEMPLATES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_USED_BY_ALL_TEMPLATES$/is);
		$PHONE_CONTACT_NUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PHONE_CONTACT_NUMBER$/is);
		$AGENT_CONTACT_DETAILS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_CONTACT_DETAILS$/is);
		$AGENT_CONTACT_NAME = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGENT_CONTACT_NAME$/is);
		$ACKNOWLEDGEMENT_EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACKNOWLEDGEMENT_EMAIL$/is);
		$EMAIL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EMAIL$/is);
		$MOBILE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^MOBILE$/is);
		$E_MAIL_ADDRESS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^E_MAIL_ADDRESS$/is);
		
		### Date ###
		$ACTUAL_COMMITTEE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACTUAL_COMMITTEE_DATE$/is);
		$ACTUAL_COMMITTEE_OR_PANEL_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ACTUAL_COMMITTEE_OR_PANEL_DATE$/is);
		$ADVERTISEMENT_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ADVERTISEMENT_EXPIRY_DATE$/is);
		$AGREED_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AGREED_EXPIRY_DATE$/is);
		$APPLICATION_EXPIRY_DEADLINE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_EXPIRY_DEADLINE$/is);
		$APPLICATION_VALID_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_VALID_DATE$/is);
		$APPLICATION_VALIDATED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_VALIDATED_DATE$/is);
		$CLOSING_DATE_FOR_COMMENTS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CLOSING_DATE_FOR_COMMENTS$/is);
		$COMMENTS_TO_BE_SUBMITTED_BY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMENTS_TO_BE_SUBMITTED_BY$/is);
		$COMMITTEE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMITTEE_DATE$/is);
		$COMMITTEE_DELEGATED_LIST_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^COMMITTEE_DELEGATED_LIST_DATE$/is);
		$CONSULTATION_END_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CONSULTATION_END_DATE$/is);
		$CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CONSULTATION_EXPIRY_DATE$/is);
		$CONSULTATION_PERIOD_EXPIRES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CONSULTATION_PERIOD_EXPIRES$/is);
		$DATE_APPLICATION_VALID = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_APPLICATION_VALID$/is);
		$DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_DATE$/is);
		$DECISION_ISSUED_DATE1 = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATE$/is);
		$DECISION_MADE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_MADE_DATE$/is);
		$DECISION_NOTICE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_NOTICE_DATE$/is);
		$DECISION_PRINTED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_PRINTED_DATE$/is);
		$DETERMINATION_DEADLINE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DETERMINATION_DEADLINE$/is);
		$EARLIEST_DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EARLIEST_DECISION_DATE$/is);
		$ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^ENVIRONMENTAL_IMPACT_ASSESSMENT_RECEIVED$/is);
		$EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EXPIRY_DATE$/is);
		$EXPIRY_DATE_FOR_COMMENT = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^EXPIRY_DATE_FOR_COMMENT$/is);
		$LAST_ADVERTISED_IN_PRESS_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_ADVERTISED_IN_PRESS_DATE$/is);
		$LAST_DATE_FOR_COMMENTS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_COMMENTS$/is);
		$LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_COMMENTS_FOLLOWING_PRESS_NOTICE$/is);
		$LAST_DATE_FOR_NEIGHBOUR_COMMENTS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_NEIGHBOUR_COMMENTS$/is);
		$LAST_DATE_FOR_NEIGHBOURS_RESPONSES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_DATE_FOR_NEIGHBOURS_RESPONSES$/is);
		$LAST_SITE_NOTICE_POSTED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LAST_SITE_NOTICE_POSTED_DATE$/is);
		$LATEST_ADVERTISEMENT_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LATEST_ADVERTISEMENT_EXPIRY_DATE$/is);
		$LATEST_NEIGHBOUR_CONSULTATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LATEST_NEIGHBOUR_CONSULTATION_DATE$/is);
		$LATEST_SITE_NOTICE_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^LATEST_SITE_NOTICE_EXPIRY_DATE$/is);
		$NEIGHBOUR_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NEIGHBOUR_CONSULTATION_EXPIRY_DATE$/is);
		$NEIGHBOURS_LAST_NOTIFIED = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^NEIGHBOURS_LAST_NOTIFIED$/is);
		$OVERALL_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^OVERALL_CONSULTATION_EXPIRY_DATE$/is);
		$OVERALL_DATE_OF_CONSULTATION_EXPIRY = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^OVERALL_DATE_OF_CONSULTATION_EXPIRY$/is);
		$PERMISSION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PERMISSION_EXPIRY_DATE$/is);
		$PUBLIC_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PUBLIC_CONSULTATION_EXPIRY_DATE$/is);
		$SITE_NOTICE_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SITE_NOTICE_EXPIRY_DATE$/is);
		$STANDARD_CONSULTATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STANDARD_CONSULTATION_DATE$/is);
		$STANDARD_CONSULTATION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STANDARD_CONSULTATION_EXPIRY_DATE$/is);
		$STATUTORY_DETERMINATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STATUTORY_DETERMINATION_DATE$/is);
		$STATUTORY_DETERMINATION_DEADLINE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STATUTORY_DETERMINATION_DEADLINE$/is);
		$STATUTORY_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^STATUTORY_EXPIRY_DATE$/is);
		$TARGET_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TARGET_DATE$/is);
		$TARGET_DETERMINATION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TARGET_DETERMINATION_DATE$/is);
		$TEMPORARY_PERMISSION_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^TEMPORARY_PERMISSION_EXPIRY_DATE$/is);
		$VALID_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^VALID_DATE$/is);
		$WEEKLY_LIST_EXPIRY_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^WEEKLY_LIST_EXPIRY_DATE$/is);
			
		if($DOC_URL ne "")
		{
			$documentURL = $DOC_URL;
		}
		if($documentURL=~m/^\s*$/is)
		{
			$documentURL=$DOC_URL1;
		}
		if($documentURL=~m/^\s*$/is)
		{
			$documentURL=$DOC_URL2;
		}
		if($documentURL=~m/^\s*$/is)
		{
			$documentURL=$DOC_URL3;
		}	
		
		if(($documentURL!~m/^http/is) && ($documentURL ne ""))
		{
			my $applicationLinkUri = URI::URL->new($documentURL)->abs( $Config->{$councilCode}->{'HOME_URL'}, 1 );
			$documentURL=$applicationLinkUri;
		}
		
		if($documentURL eq "")
		{
			my $dumDocURL = $applicationLink;
			$dumDocURL=~s/activeTab=summary/activeTab=documents/is;
			$documentURL=$dumDocURL;
		}

		if($councilCode=~m/^(105|149|232|245|246|273|292|15|179|171|320|533|211|268|350|406|429|455|481|78)$/is)
		{
			if($documentURL eq "")
			{
				my $dumDocURL = $applicationLink;
				$dumDocURL=~s/activeTab=summary/activeTab=externalDocuments/is;
				$documentURL=$dumDocURL;
			}
			else
			{
				$documentURL=~s/activeTab=documents/activeTab=externalDocuments/is;
			}
		}		
		
		if($APPLICATION eq '')
		{
			if($CASE_REFERENCE!~m/^\s*$/is)
			{
				$Application=$CASE_REFERENCE;
			}
			elsif($SUMMARY_CASENUMBER!~m/^\s*$/is)
			{
				$Application=$SUMMARY_CASENUMBER;
			}
			elsif($APPLICATION_REFERENCE!~m/^\s*$/is)
			{
				$Application=$APPLICATION_REFERENCE;
			}
		}
		else
		{
			$Application = $APPLICATION;
		}
		
		if($PROPOSAL=~m/^\s*$/is)
		{
			if($SUMMARY_PROPOSAL!~m/^\s*$/is)
			{
				$Proposal=$SUMMARY_PROPOSAL;
			}
		}	
		else
		{
			$Proposal = $PROPOSAL;
		}
		
		if($ADDRESS eq '')
		{
			if($LOCATION!~m/^\s*$/is)
			{
				$Address=$LOCATION;
			}
			elsif($SUMMARY_ADDRESS!~m/^\s*$/is)
			{
				$Address=$SUMMARY_ADDRESS;
			}
		}	
		else
		{
			$Address = $ADDRESS;
		}
		
		if($AGENT_EMAIL=~m/^\s*$/is)
		{
			if($PERSONAL_EMAIL!~m/^\s*$/is)
			{
				$agentEmail = $PERSONAL_EMAIL;
			}
			elsif($EMAIL_ADDRESSE!~m/^\s*$/is)
			{
				$agentEmail = $EMAIL_ADDRESSE;
			}
			elsif($EMAIL_ADDRESS!~m/^\s*$/is)
			{
				$agentEmail = $EMAIL_ADDRESS;
			}
			elsif($ACKNOWLEDGEMENT_EMAIL!~m/^\s*$/is)
			{
				$agentEmail = $ACKNOWLEDGEMENT_EMAIL;
			}
			elsif($EMAIL!~m/^\s*$/is)
			{
				$agentEmail = $EMAIL;
			}
			elsif($E_MAIL_ADDRESS!~m/^\s*$/is)
			{
				$agentEmail = $E_MAIL_ADDRESS;
			}
			elsif($ELECTRONIC_MAIL!~m/^\s*$/is)
			{
				$agentEmail = $ELECTRONIC_MAIL;
			}
		}
		else
		{
			$agentEmail = $AGENT_EMAIL;
		}
		
		if($AGENT_FAX=~m/^\s*$/is)
		{
			if($FAX_NO!~m/^\s*$/is)
			{
				$agentFax = $FAX_NO;
			}
			elsif($COMPANY_FAX!~m/^\s*$/is)
			{
				$agentFax = $COMPANY_FAX;
			}
		}
		else
		{
			$agentFax = $AGENT_FAX;
		}
		
		if($AGENT_MOBILE=~m/^\s*$/is)
		{
			if($MOBILE_NUMBER!~m/^\s*$/is)
			{
				$agentMobile = $MOBILE_NUMBER;
			}
			elsif($MOBILE!~m/^\s*$/is)
			{
				$agentMobile = $MOBILE;
			}
		}	
		else
		{
			$agentMobile = $AGENT_MOBILE;
		}
			
		if($AGENT_TELEPHONE1 ne "")
		{
			$agentTelephone2 = $AGENT_TELEPHONE1;
		}
		elsif($AGENT_TELEPHONE1=~m/^\s*$/is)
		{
			if($HOME_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $HOME_PHONE;
			}
			elsif($PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_NUMBER;
			}
			elsif($TELEPHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $TELEPHONE_NUMBER;
			}
			elsif($HOME_PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $HOME_PHONE_NUMBER;
			}
			elsif($PERSONAL_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $PERSONAL_PHONE;
			}
			elsif($COMPANY_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $COMPANY_PHONE;
			}
			elsif($WORK_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $WORK_PHONE;
			}
			elsif($TELEPHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $TELEPHONE;
			}
			elsif($WORK_PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $WORK_PHONE_NUMBER;
			}
			elsif($PHONE_NO!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_NO;
			}
			elsif($OFFICE_PHONE_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $OFFICE_PHONE_NUMBER;
			}
			elsif($MAIN_PHONE!~m/^\s*$/is)
			{
				$agentTelephone2 = $MAIN_PHONE;
			}
			elsif($PHONE_CONTACT_NUMBER!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_CONTACT_NUMBER;
			}
			elsif($PHONE_USED_BY_ALL_TEMPLATES!~m/^\s*$/is)
			{
				$agentTelephone2 = $PHONE_USED_BY_ALL_TEMPLATES;
			}
			elsif($PERSONAL_MOBILE!~m/^\s*$/is)
			{
				$agentTelephone2 = $PERSONAL_MOBILE;
			}
		}	
		
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
		if($agentAddress=~m/^\s*$/is)
		{
			if($AGENT_ADDRESS1!~m/^\s*$/is)
			{
				$agentAddress = $AGENT_ADDRESS1;
			}
		}

		if($AGENT_CONTACT_DETAILS=~m/^\s*$/is)
		{
			if($AGENT_CONTACT_NAME!~m/^\s*$/is)
			{
				$agentContactDetails = $AGENT_CONTACT_NAME;
			}
		}
		else
		{
			$agentContactDetails = $AGENT_CONTACT_DETAILS;
		}
		
		if($DATE_APPLICATION_RECEIVED=~m/^\s*$/is)
		{
			if($APPLICATION_RECEIVED_DATE!~m/^\s*$/is)
			{
				$dateApplicationReceived=$APPLICATION_RECEIVED_DATE;
			}
		}
		else
		{
			$dateApplicationReceived = $DATE_APPLICATION_RECEIVED;
		}
		
		if($DATE_APPLICATION_REGISTERED=~m/^\s*$/is)
		{
			if($APPLICATION_REGISTERED_DATE!~m/^\s*$/is)
			{
				$dateApplicationRegistered = $APPLICATION_REGISTERED_DATE;
			}
		}
		else
		{
			$dateApplicationRegistered = $DATE_APPLICATION_REGISTERED;
		}
		
		if($DATE_APPLICATION_VALIDATED=~m/^\s*$/is)
		{
			if($APPLICATION_VALID_DATE!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $APPLICATION_VALID_DATE;
			}
			elsif($APPLICATION_VALIDATED_DATE!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $APPLICATION_VALIDATED_DATE;
			}
			elsif($DATE_APPLICATION_VALID!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $DATE_APPLICATION_VALID;
			}
			elsif($VALID_DATE!~m/^\s*$/is)
			{
				$dateApplicationvalidated = $VALID_DATE;
			}
		}
		else
		{
			$dateApplicationvalidated = $DATE_APPLICATION_VALIDATED;
		}
		
		if($targetDecdt=~m/^\s*$/is)
		{
			if($DETERMINATION_DEADLINE!~m/^\s*$/is)
			{
				$targetDecdt = $DETERMINATION_DEADLINE;
			}
			elsif($EARLIEST_DECISION_DATE!~m/^\s*$/is)
			{
				$targetDecdt = $EARLIEST_DECISION_DATE;
			}
			elsif($TARGET_DATE!~m/^\s*$/is)
			{
				$targetDecdt = $TARGET_DATE;
			}
			elsif($TARGET_DETERMINATION_DATE!~m/^\s*$/is)
			{
				$targetDecdt = $TARGET_DETERMINATION_DATE;
			}
		}
				
		$agentName = $AGENT_NAME if($agentName eq "");	
		$applicationStatus = $APPLICATION_STATUS if($applicationStatus eq "");
		$actualDecisionLevel = $ACTUAL_DECISION_LEVEL if($actualDecisionLevel eq "");
		$agentCompanyName = $AGENT_COMPANY_NAME if($agentCompanyName eq "");
		$applicantAddress = $APPLICANT_ADDRESS if($applicantAddress eq "");
		$applicantName = $APPLICANT_NAME if($applicantName eq "");
		$applicationType = $APPLICATION_TYPE if($applicationType eq "");
		$agentTelephone1 = $AGENT_TELEPHONE if($agentTelephone1 eq "");
		$agentTelephone2 = $AGENT_TELEPHONE1 if($agentTelephone2 eq "");
		$actualCommitteeDate = $ACTUAL_COMMITTEE_DATE if($actualCommitteeDate eq "");
		$actualCommitteeorPanelDate = $ACTUAL_COMMITTEE_OR_PANEL_DATE if($actualCommitteeorPanelDate eq "");
		$advertisementExpiryDate = $ADVERTISEMENT_EXPIRY_DATE if($advertisementExpiryDate eq "");
		$agreedExpiryDate = $AGREED_EXPIRY_DATE if($agreedExpiryDate eq "");
		$applicationExpiryDeadline = $APPLICATION_EXPIRY_DEADLINE if($applicationExpiryDeadline eq "");
		$temporaryPermissionExpiryDate = $TEMPORARY_PERMISSION_EXPIRY_DATE if($temporaryPermissionExpiryDate eq "");
		$noDocument = $NO_DOCUMENT if($noDocument eq "");
		
	}
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	$applicationStatus=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Pending\sConsideration)[\w\W]*?$/$1/msi;
	$applicationStatus=~s/^\s*(Pending\sDecision)[\w\W]*?$/$1/msi;
	$actualDecisionLevel=~s/\s*Expected\s*Decision\s*Level\s*Case\s*Officer\s*Parish\s*Ward\s*District\s*Reference\s*Applicant\s*Name\s*//msi;
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\')";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	return($insert_query);		
}

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $category,$councilCode) = @_;
	
	my $tempURL = $receivedApplicationUrl;
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	# sleep(5);
	if($category eq "Planning")
	{
		$regexFile=$Config;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$Config;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'CASE_REFERENCE'}/is)
	{
		$applicationNumber=$1;
	}
	elsif($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'APPLICATION_REFERENCE'}/is)
	{
		$applicationNumber=$1;
	}
	elsif($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'SUMMARY_CASENUMBER'}/is)
	{
		$applicationNumber=$1;
	}
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		$logName = $1 if($receivedApplicationUrl=~m/\&KeyVal\=([^\n]*?)$/is);
		
		if($logName eq "")
		{
			$logName = $1 if($receivedApplicationUrl=~m/KeyVal\=([^\&]*?)\&/is);
		}
		
		my $fileName= $category."_".$councilCode."_".$logName;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	my %dataDetails;
	if($tempURL=~m/activeTab=summary/is)
	{
		print "ActiveTab ==> Summary\n";
		my %activeTabContents = &tabDetailsCollection($applicationDetailsPageContent, "summary", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
		
	if(($councilCode!~m/^149$/is) && ($tempURL=~s/activeTab=summary/activeTab=details/is))
	{
		print "ActiveTab ==> Details\n";
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $detailsPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($detailsPageContent, "details", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	
	if(($category ne "Decision") && ($tempURL=~s/activeTab=details/activeTab=contacts/is))
	{
		print "ActiveTab ==> Contacts\n";
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $contactsPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($contactsPageContent, "contacts", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	elsif(($category eq "Decision") && ($tempURL=~s/activeTab=details/activeTab=dates/is))
	{
		print "ActiveTab ==> Dates\n";
		print "ActiveTabURL ==> $tempURL\n";
		my $keyVal=$1 if($tempURL=~m/keyVal=([^>]*?)\&activeTab=dates\s*$/is);
		$tempURL=~s/^([^>]*?)(keyVal=[^>]*?)\&(activeTab=dates)\s*$/$1$3\&$2/igs;
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $datesPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($datesPageContent, "dates", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	
	if(($category eq "Planning") && ($tempURL=~s/activeTab=contacts/activeTab=dates/is))
	{
		print "ActiveTab ==> Dates\n";
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $datesPageContent = $receivedCouncilApp->content;
		my %activeTabContents = &tabDetailsCollection($datesPageContent, "dates", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	return(\%dataDetails, $applicationDetailsPageResponse, $applicationNumber);
}

####
# Scraping of details for an given Tab and Application
####

sub tabDetailsCollection()
{
	my ($tabContent, $blockSection, $category) = @_;
	my %tabDetails;
	my $regexFile;
	
    if($category eq "Planning")
	{
		$regexFile=$Config;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$Config;
	}
	
	foreach my $detailsKey (keys %{$regexFile->{$blockSection}}) {
		if($tabContent=~/$regexFile->{$blockSection}->{$detailsKey}/is)
		{
			push ( @{ $tabDetails{$detailsKey}}, $1);
		}
	}
	
	return(%tabDetails);
}

sub createdirectory()
{
	my $dirname=shift;

	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}