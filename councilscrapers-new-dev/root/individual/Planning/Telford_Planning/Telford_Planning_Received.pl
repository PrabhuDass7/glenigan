use strict;
use WWW::Mechanize;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL qw();
use Config::Tiny;
use Time::Piece;
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"449\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Telford_Planning/Telford_Planning.ini" );

my $COUNCIL_NAME 	= $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL 		= $Config->{$Council_Code}->{'HOMEURL'};
my $HOST 			= $Config->{$Council_Code}->{'HOST'};
my $FORM_NUMBER 	= $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE 	= $Config->{$Council_Code}->{'FORM_END_DATE'};
my $SSL_VERIFICATION= $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $SRCH_BTN_NME	= $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL	= $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX=$Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $NEXT_PAGE_REGEX	= $Config->{$Council_Code}->{'NEXT_PAGE_REGEX'};
my $FILTER_URL 		= $Config->{$Council_Code}->{'FILTER_URL'};
my $SEARCH_URL 		= $Config->{$Council_Code}->{'SEARCH_URL'};
my $SRCH_NXT_PAGE	= $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "HOME_URL	: $HOME_URL\n";

my $mech = WWW::Mechanize->new( 
					ssl_opts => {
									SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
									verify_hostname => 0, 
								}
								, autocheck => 0
					);

### Proxy settings ###

print "Scraping via proxy 199...\n";
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');


# now fetch page 

$mech->get( $HOME_URL );
my $Code_status = $mech->status;
my $content = $mech->content;

my ($Temp_From_Date,$Temp_To_Date);
if($From_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_From_Date = $1."-".$2."-".$3;
}
if($To_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_To_Date = $1."-".$2."-".$3;
}
print "Temp_From_Date::$Temp_From_Date\n";
print "Temp_To_Date::$Temp_To_Date\n";

my ($VIEWSTATE,$VIEWSTATEGENERATOR,$PREVIOUSPAGE,$EVENTVALIDATION,$EVENTTARGET,$EVENTARGUMENT);
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATE=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATEGENERATOR=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTVALIDATION=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTTARGET=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTARGUMENT=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$PREVIOUSPAGE=uri_escape($1);
}
	
my $post_content = "__EVENTTARGET=$EVENTTARGET&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__PREVIOUSPAGE=$PREVIOUSPAGE&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24txtPlanningKeywords=&ctl00%24ContentPlaceHolder1%24dlPlanningParishs=0&ctl00%24ContentPlaceHolder1%24dlPlanningWard=0&ctl00%24ContentPlaceHolder1%24ddlPlanningapplicationtype=0&ctl00%24ContentPlaceHolder1%24DCdatefrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24DCdateto=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24txtDCAgent=&ctl00%24ContentPlaceHolder1%24txtDCApplicant=&ctl00%24ContentPlaceHolder1%24btnSearchPlanningDetails=Search";

my $searchResultCon;
my @fullAPPURLs;
if($post_content ne '')
{
	$mech->post( $SEARCH_URL, Content => "$post_content");
	my $content1 = $mech->content;
	
	my ($VIEWSTATE,$VIEWSTATEGENERATOR,$PREVIOUSPAGE,$EVENTVALIDATION,$EVENTTARGET,$EVENTARGUMENT);
	if($content1=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$VIEWSTATE=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$VIEWSTATEGENERATOR=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$EVENTVALIDATION=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$EVENTTARGET=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$EVENTARGUMENT=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$PREVIOUSPAGE=uri_escape($1);
	}
	
	my $post_content1 = "__EVENTTARGET=ctl00%24ContentPlaceHolder1%24lbPlanning2ndLevel1&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop=10";
	
	if($post_content1 ne '')
	{
		$mech->post( $SEARCH_URL, Content => "$post_content1");
		my $content2 = $mech->content;
		$searchResultCon=$content2;
				
		# open(PP,">a.html");
		# print PP $searchResultCon;
		# close(PP);
		
		if($searchResultCon!~m/Sorry\s*no\s*records\s*were\s*found/is)
		{		
			my $fullAppURLs = &scrape_details($searchResultCon);
			@fullAPPURLs = @{$fullAppURLs};
		}
		else
		{
			print "0 search results found\n";
			exit;
		}
	}
	
}
	
&collectAppDetails(\@fullAPPURLs);


sub collectAppDetails
{
	my $AppURLs = shift;	
	my @AppURLs = @{$AppURLs};
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';

	my ($Application,$Address,$Proposal,$Case_Officer,$Application_Status,$Date_Application_Received,$Date_Application_validated,$Date_Application_Registered,$Applicant_name,$Applicant_Address,$Agent_Name,$Agent_Address,$Agent_Telephone,$Application_Type,$Doc_Url,$ApplicationLink,$TargetDec_dt,$Agent_Email);
	
	foreach $ApplicationLink(@AppURLs)
	{
		# print "ApplicationLink=>$ApplicationLink\n";
		$mech->get($ApplicationLink);
		my $App_Page_Content = $mech->content;
		
		$Application				= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>Application\s*number<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		
		print "Now processing application::$Application\n";
		
		if($App_Page_Content=~m/<ul\s*id=\"ApplicationStatuslist\">\s*([\w\W]*?)\s*<ul>/is)
		{
			my $status = $1;			
			$Application_Status			= &clean($1) if($App_Page_Content=~m/<li\s*class=\"[^\"]*?selected\">\s*([^<]*?)\s*<\/li>/is);
		}
		
		$Address					= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>Site\s*address<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Proposal					= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>(?:Description\s*of)?\s*proposal<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*(.*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Application_Type			= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Application\s*type\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Date\s*valid\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Date_Application_validated=~s/^N\/A$//is;
		$Applicant_name				= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Applicant\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Applicant_Address			= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Applicant\s*address\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Agent_Name					= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Agent\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Agent_Address				= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Agent\s*address\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Case_Officer				= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Case\s*officer\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Doc_Url				= &clean($1) if($App_Page_Content=~m/<li>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*Documents\s*<\/a>\s*<\/li>/is);
		
		
		if($Doc_Url!~m/^\s*$/is)
		{
			if($Doc_Url!~m/https?\:/is)
			{
				$Doc_Url = $FILTER_URL.$Doc_Url;
			}	
			else
			{
				$Doc_Url = $ApplicationLink;
			}	
		}	
		if($App_Page_Content=~m/<li>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*Progress\s*<\/a>\s*<\/li>/is)
		{
			my $progress = $1;
			if($progress!~m/https?\:/is)
			{
				$progress = $FILTER_URL.$progress;
			}	
			
			$mech->get($progress);
			my $App_Page = $mech->content;
			
			$Date_Application_Received	= &clean($1) if($App_Page=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Application\s*Received\s*<\/th>\s*<td[^>]*?>\s*([^<]*?)\s*\s*<\/td>/is);
			$Date_Application_Registered= &clean($1) if($App_Page=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Application\s*Registered\s*<\/th>\s*<td[^>]*?>\s*([^<]*?)\s*\s*<\/td>/is);
			$Date_Application_Registered=~s/^N\/A$//is;
		}
		
		
		$Doc_Url= $ApplicationLink if($Doc_Url eq "");
		# my $Source = "GCS001";	
		my $Source = $Date_Range;	
	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";	
		
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$ApplicationLink\',\'$Agent_Email\'),";
		
		
		$Application=""; $Address=""; $Proposal=""; $Application_Status=""; $Date_Application_validated=""; $Date_Application_Registered=""; $Applicant_name=""; $Applicant_Address=""; $Agent_Name=""; $Agent_Address=""; $Agent_Telephone=""; $Date_Application_Received=""; $TargetDec_dt=""; $Doc_Url=""; $ApplicationLink=""; $Agent_Email=""; $Application_Type=""; $Source_With_Time="";			
	}
	$insert_query=~s/\,$//igs;
		
	print "insert_query::$insert_query\n";
	
	
	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
}

sub scrape_details()
{
	my $dtlCont = shift;
	
	
	my ($target,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$VIEWSTATE,$EVENTARGUMENT,@appURLs);
	
	my @URLs;
	my $i = 2;
	nextPage:
	my $URLs = &collect_URLs($dtlCont);
	@URLs = @{$URLs};
	push(@appURLs,@URLs);
									
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATEGENERATOR\"\s*id\s*=\s*\"__VIEWSTATEGENERATOR\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$VIEWSTATEGENERATOR = uri_escape($1);
	}
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTVALIDATION\"\s*id\s*=\s*\"__EVENTVALIDATION\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$EVENTVALIDATION = uri_escape($1);
	}
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATE\"\s*id\s*=\s*\"__VIEWSTATE\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$VIEWSTATE = uri_escape($1);
	}
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTARGUMENT\"\s*id\s*=\s*\"__EVENTARGUMENT\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$EVENTARGUMENT = uri_escape($1);
	}
	if($dtlCont =~ m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')(Page\$\d+)?(\&\#39\;|\')\)\"\s*>Next[^<]*?\s*<\/a>/is)
	{	
		$target = uri_escape($2);
	}
	
	my ($Temp_From_Date,$Temp_To_Date);
	if($From_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
	{
		$Temp_From_Date = $1."-".$2."-".$3;
	}
	if($To_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
	{
		$Temp_To_Date = $1."-".$2."-".$3;
	}
	
	my $nextPostcont = "__EVENTTARGET=$target&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop=10";
	
	
	$mech->post( $SEARCH_URL, Content => "$nextPostcont");
	print "\ni-value:::::$i\n";
	
	my $nextPageStatus = $mech->status;
	my $nextPageCnt = $mech->content;
	$dtlCont=$nextPageCnt;
	
	# open(PP, ">449$i.html");
	# print PP "$nextPageCnt\n";
	# close(PP);

	# exit;
	
	$i++;
	
	if($dtlCont=~m/<a[^>]*?disabled=\"disabled\"\s*class=\"selectedpage\"[^>]*?>\s*Next\s*<\/a>\s*<\/li>/is)
	{
		my $URLs = &collect_URLs($dtlCont);
		@URLs = @{$URLs};
		push(@appURLs,@URLs);
		
		return(\@appURLs);
		
	}
	else
	{
		if($i == 50)
		{
			exit;
		}
		else
		{
			goto nextPage;
		}
	}
				
}
	
	
sub collect_URLs
{
	my $urlCont = shift;
		
	my @Detail_Links;
	while($urlCont=~m/$APPLICATION_REGEX/igs)
	{
		my $Application_url=$1;		
		my $Application_no=$2;	

		print "Application_no==>$Application_no\n";
		
		my $Application_Link;	
	
		if($Application_url!~m/^\s*$/is)
		{
			if($Application_url!~m/https?\:/is)
			{
				$Application_Link = $FILTER_URL.$Application_url;
			}	
			else
			{
				$Application_Link = $Application_url;
			}	
		}	
		
		$Application_Link=~s/amp\;//igs;
		
		push(@Detail_Links,$Application_Link);
		undef $Application_Link;
	}
	
	return(\@Detail_Links);
}

		

sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}