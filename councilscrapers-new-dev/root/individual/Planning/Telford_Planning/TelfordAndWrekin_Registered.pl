use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
my $COUNCIL_NAME = 'Telford & Wrekin';
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"449\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


# my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://secure.telford.gov.uk/planningsearch/search.aspx";
$mech->get($searchURL);
my $content = $mech->content;
my $status = $mech->status;



# open(DC,">Content.html");
# print DC $content;
# close DC;

# $mech->add_header( "Accept" => 'text/plain, */*; q=0.01' );
# $mech->add_header( "Host" => 'www.gov.je' );
# $mech->add_header( "Content-Type" => 'application/json; charset=UTF-8' );
# $mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
# $mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
# $mech->add_header( "Origin" => 'https://www.gov.je' );
# $mech->add_header( "Referer" => "$searchURL" );	

my ($Temp_From_Date,$Temp_To_Date);
if($From_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_From_Date = $1."-".$2."-".$3;
}
if($To_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_To_Date = $1."-".$2."-".$3;
}
print "Temp_From_Date::$Temp_From_Date\n";
print "Temp_To_Date::$Temp_To_Date\n";

my ($VIEWSTATE,$VIEWSTATEGENERATOR,$PREVIOUSPAGE,$EVENTVALIDATION,$EVENTTARGET,$EVENTARGUMENT);
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATE=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATEGENERATOR=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTVALIDATION=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTTARGET=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTARGUMENT=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$PREVIOUSPAGE=uri_escape($1);
}
	
my $searchPostContent = "__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__PREVIOUSPAGE=$PREVIOUSPAGE&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24txtPlanningKeywords=&ctl00%24ContentPlaceHolder1%24dlPlanningParishs=0&ctl00%24ContentPlaceHolder1%24dlPlanningWard=0&ctl00%24ContentPlaceHolder1%24ddlPlanningapplicationtype=0&ctl00%24ContentPlaceHolder1%24DCdatefrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24DCdateto=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24txtDCAgent=&ctl00%24ContentPlaceHolder1%24txtDCApplicant=&ctl00%24ContentPlaceHolder1%24btnSearchPlanningDetails=Search";



		
$mech->post( 'https://secure.telford.gov.uk/planningsearch/default.aspx', Content => "$searchPostContent");
my $PostContent = $mech->content;
# open(DC,">PostContent.html");
# print DC $PostContent;
# close DC;
# exit;


my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Application, Address, Date_Application_Registered, Date_Application_validated, Applicant_name, Applicant_Address, Application_type, Proposal, Agent_Name, Agent_Telephone, Agent_Company_Name, Agent_Address, Application_Status, Date_Application_Received, TargetDec_dt, Easting, Northing, PAGE_URL, Document_Url, Source, Schedule_Date) values';

if($PostContent=~m/<tr[^>]*?>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*<\/a>\s*<\/td>/is)
{
	my $Page_Number=1;
	NextPage:
	while($PostContent=~m/<tr[^>]*?>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*<\/a>\s*<\/td>/gsi)
	{
		my $Application_URL = $1;
		my $Application_Num = $2;
		$Application_URL=~s/\&amp;/\&/gsi;
		
		print "Application_Num==>$Application_Num\n";
		
		$mech->get($Application_URL);
		my $App_Page_Content = $mech->content;		

		# open(DC,">App_content.html");
		# print DC $App_Page_Content;
		# close DC;
		# exit;
		
		my ($Application,$Address,$Agent_Name,$Agent_Telephone,$Agent_Address,$Applicant_Address,$Applicant_name,$Application_Status,$Application_type,$Date_Application_Received,$Date_Application_Registered,$Date_Application_validated,$Easting,$Northing,$Grid_reference,$Proposal,$TargetDec_dt,$Agent_Company_Name,$Doc_Url);
		
		$Application = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>Application\s*number<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Proposal = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>(?:Description\s*of)?\s*proposal<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([\w\W]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Address = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>Site\s*address<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Application_type = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Application\s*type\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Date\s*valid\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);		
		$Applicant_name = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Applicant\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Applicant_Address = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Applicant\s*address\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Agent_Name = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Agent\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Agent_Address = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Agent\s*address\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);		
		$Doc_Url = &clean($1) if($App_Page_Content=~m/<li>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*Documents\s*<\/a>\s*<\/li>/is);
		
		if($Doc_Url!~m/https?\:/is)
		{
			$Doc_Url = 'https://secure.telford.gov.uk/planning/'.$Doc_Url;
		}	
		
		if($App_Page_Content=~m/<ul\s*id=\"ApplicationStatuslist\">\s*([\w\W]*?)\s*<\/ul>/is)
		{
			my $status = $1;
			$Application_Status = &clean($1) if($status=~m/<li[^>]*?selected\">\s*([^<]*?)\s*<\/li>/is);
		}
		
		# $Decision_Status = &clean($1) if($App_Content=~m/<th[^>]*?>\s*<p>\s*Decision\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		# $Date_Decision_Made = &clean($1) if($App_Content=~m/<th[^>]*?>\s*<p>\s*Decision\s*date\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		
		$Applicant_name=~s/\'/\'\'/igs;	
		$Agent_Name=~s/\'/\'\'/igs;	
		$Address=~s/\'/\'\'/igs;	
		$Agent_Address=~s/\'/\'\'/igs;	
		$Applicant_Address=~s/\'/\'\'/igs;	
		
		my $time = Time::Piece->new;
		my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
		# my $Source = "GCS001";	
		my $Source = $Date_Range;	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Application\', \'$Address\', \'$Date_Application_Registered\', \'$Date_Application_validated\', \'$Applicant_name\', \'$Applicant_Address\', \'$Application_type\', \'$Proposal\', \'$Agent_Name\', \'$Agent_Telephone\', \'$Agent_Company_Name\', \'$Agent_Address\', \'$Application_Status\', \'$Date_Application_Received\',  \'$TargetDec_dt\',  \'$Easting\',  \'$Northing\', \'$Application_URL\', \'$Doc_Url\', \'$Source_With_Time\',\'$Schedule_Date\'),";
		
		undef($Application); undef($Address); undef($Date_Application_Registered); undef($Date_Application_validated); undef($Applicant_name); undef($Applicant_Address); undef($Application_type); undef($Proposal); undef($Agent_Name); undef($Agent_Telephone); undef($Agent_Company_Name); undef($Agent_Address); undef($Application_Status); undef($Date_Application_Received); undef($TargetDec_dt); undef($Easting); undef($Northing); undef($Doc_Url);
	}
	
	if($PostContent=~m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')(Page\$\d+)?(\&\#39\;|\')\)\"\s*>Next[^<]*?\s*<\/a>/is)
	{
		my ($target,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$VIEWSTATE,$EVENTARGUMENT);
		if($PostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATEGENERATOR\"\s*id\s*=\s*\"__VIEWSTATEGENERATOR\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$VIEWSTATEGENERATOR = uri_escape($1);
		}
		if($PostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTVALIDATION\"\s*id\s*=\s*\"__EVENTVALIDATION\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$EVENTVALIDATION = uri_escape($1);
		}
		if($PostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATE\"\s*id\s*=\s*\"__VIEWSTATE\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$VIEWSTATE = uri_escape($1);
		}
		if($PostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTARGUMENT\"\s*id\s*=\s*\"__EVENTARGUMENT\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$EVENTARGUMENT = uri_escape($1);
		}
		if($PostContent =~ m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')(Page\$\d+)?(\&\#39\;|\')\)\"\s*>Next[^<]*?\s*<\/a>/is)
		{	
			$target = uri_escape($2);
		}
		
		my $nextPagePostCont = "__EVENTTARGET=$target&__EVENTARGUMENT=&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop=10";
		
		$mech->post('https://secure.telford.gov.uk/planningsearch/default.aspx', Content => "$nextPagePostCont");
		my $nextPagePostCnt = $mech->content;
		$PostContent=$nextPagePostCnt;
		
		# open(DC,">nextPagePostCnt.html");
		# print DC $nextPagePostCnt;
		# close DC;
		# exit;
		
	
		$Page_Number++;
		print "Page_Number: $Page_Number\n";
		
		
		goto NextPage;
	}
}


$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);




sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}