use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"348\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();




### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Sedgemoor_Planning/Sedgemoor.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";
	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0, autoclose => 1);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
	autoclose => 1,
    verify_hostname => 0, 
});
}	


### Proxy settings ###
if($Council_Code=~m/^(348)$/is)
{
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
}

# Find home page url
my $Home_Url;
if($HOME_URL=~m/^(https?\:\/\/.*?)\//is)
{
	$Home_Url=$1;
}

# Get search results using date ranges

my ($Responce, $cookie_jar, $Ping_Status1);
eval{
$Responce = $mech->get($HOME_URL);
$cookie_jar = $mech->cookie_jar;

$mech->form_number($FORM_NUMBER); 
$mech->set_fields($RADIO_BTN_NME => $RADIO_BTN_VAL, $FORM_START_DATE => $From_Date, $FORM_END_DATE => $To_Date );
$mech->click( $SRCH_BTN_NME );
		
};

if ($@) {
	$Ping_Status1 = $@;
};	

my $content = $mech->content;
my $code = $mech->status;
my $Search_Content=$content;

# open(PP, ">app_cont.html");
# print PP "$Search_Content\n";
# close(PP); 
# exit;

my $totalPage = $1 if($Search_Content=~m/$TOTAL_PAGE_COUNT/is);

my @Appl_Num = ();
@Appl_Num = &getNextPageLink( $Search_Content,$Council_Code,$totalPage,$COUNCIL_NAME,$code );

&Scrape_Details(\@Appl_Num ) ;

my @new_app=();

my $arrSize = @new_app;

if($arrSize ne "")
{
	print "Error array size=>$arrSize\n";
	&Scrape_Details(\@new_app ) ;
}
print "No Error found\n";

sub Scrape_Details()
{
	my $Appl_Num = shift;
	my @Appl_Num = @{$Appl_Num};
	
	my $s=1;
	
	print "Council_Code: $Council_Code\n";
	foreach my $Appl_nums (@Appl_Num)
	{
		my ($a,$b,$c);
		if($Appl_nums=~m/^(\d+)\/(\d+)\/(\d+)$/is)
		{
			$a=$1;
			$b=$2;
			$c=$3;
		}
		
		print "$s\) $Appl_nums\n";
		
		my $mechs = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$mechs->get($HOME_URL);
		sleep(5);
		$mechs->form_number($FORM_NUMBER); 
		$mechs->set_fields('ctl00$MainContent$planSearch$ddlCaseType' => $a, 'ctl00$MainContent$planSearch$ddlCaseYear' => $b, 'ctl00$MainContent$planSearch$txtCaseNo' => $c );
		$mechs->set_fields($RADIO_BTN_NME => $RADIO_BTN_VAL);
		$mechs->click( $SRCH_BTN_NME );
		
		my $App_Detail_contents = $mechs->content;
		
		if($App_Detail_contents=~m/Planning\s*Online\s*has\s*experienced\s*an\s*error\./is)
		{
			push(@new_app,$Appl_nums);
		}
		
		$mechs->form_number($FORM_NUMBER); 
		$mechs->set_fields('ctl00$MainContent$planList$lvPlanList$ctrl0$btnView' => 'View Details' );
		$mechs->click();	
		
		my $Page_url = $mechs->uri();
		
		my $App_Page_Content = $mechs->content;
		
		# open(PPP,">PageContent$s.html");
		# print PPP "$App_Page_Content\n";
		# close(PPP);
		# exit;
		$s++;
		
		
		
		my $time = Time::Piece->new;
		my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
		my $Application					= &clean($1) if($App_Page_Content=~m/<p>\s*<b>\s*Planning\s*Application\s*Number\:\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Application_Type			= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Type\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Address						= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Location\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);	
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Proposal\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/div>/is);	
		my $Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Application\s*Received\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		if($Date_Application_Received=~m/^\s*$/is)
		{	
			$Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Submission\s*Received\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		}
		my $Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Valid(?:ated)?\s*Date\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		if($Date_Application_validated=~m/^\s*$/is)
		{	
			$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Application\s*valid(?:ated)?\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		}	
		if($Date_Application_validated=~m/^\s*$/is)
		{	
			$Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Submission\s*valid(?:ated)?\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		}
		
		my $Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Registered\s*Date\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		if($Date_Application_Registered=~m/^\s*$/is)
		{	
			$Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Application\s*Registered\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		}	
		if($Date_Application_Registered=~m/^\s*$/is)
		{	
			$Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Submission\s*Registered\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		}
		my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Agent\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Case\s*Officer\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Applicant\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Applicant_Address			= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Applicant\s*Address\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Agent_Address				= &clean($1) if($App_Page_Content=~m/<span[^>]*?>\s*Agent\s*Address\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);
		my $Application_Status;
		my $Agent_Telephone;
		my $TargetDec_dt;
		my $Doc_Url=$Page_url;
		my $Application_Link=$Page_url;
		my $Agent_Email;
		
		$Address=~s/\'/\'\'/gsi;	
		my $Source = $Date_Range;	
		
		
		if($Application=~m/^\s*$/is)
		{
			$Application=$Appl_nums;
		}
		if($Date_Application_Received=~m/^\s*$/is)
		{
			$Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>Received\s*Date\:\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*</is);	
		}
		if($Date_Application_Registered=~m/^\s*$/is)
		{
			$Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<span[^>]*?>Application\s*Registered\:?\s*<\/span>\s*<br\s*\/>\s*<span[^>]*?>\s*([^<]*?)\s*</is);	
		}
		
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		my $insert_query="insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values (\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\')";
		
		if($insert_query!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
		}
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
		
	}
	# $insert_query=~s/\,$//igs;
	
	# print "insert_query::$insert_query\n";

}



sub getNextPageLink
{
	my $searchContent = shift;
	my $councilCode = shift;
	my $totalPageCount = shift;
	my $Council_Name = shift;
	my $Search_Code = shift;
	
	# print "totalPageCount==>$totalPageCount\n";
	
	my ($Appl_Num) = &getDetailPage( $searchContent,$councilCode,$Council_Name,$Search_Code );
	@Appl_Num=@$Appl_Num;
	
	my $i=2;
	next_page:
	
	$mech->reload();
	
	if($totalPageCount ne "")
	{
		if($i<=$totalPageCount)
		{	
			print "$i======$totalPageCount\n";
			
			$mech->form_number($FORM_NUMBER); 
		
			my $next_page;
			if(($Council_Code =~ m/^(348)$/is) && ($i!~m/^(16|31|46|61|76)$/is))
			{
				$next_page = $2 if($searchContent =~ m/<a\s*class\s*=\s*\"pgItemStd\s*\"\s*href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')[^>]*?>\s*$i\s*<\/a>/is);
				
				$mech->form_number($FORM_NUMBER); 
				$mech->set_fields($next_page => $i );
				$mech->click();	
			}
			else
			{
				$next_page = $2 if($searchContent =~ m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')[^>]*?>\s*\>\>\s*<\/a>/is);
				
				$mech->form_number($FORM_NUMBER); 
				$mech->set_fields($next_page => '>>' );
				$mech->click();	
			}
			
			my $App_content = $mech->content;	
			my $Code = $mech->status;		
			$searchContent = $App_content;
			
			# open(PPP,">NextPageContent$i.html");
			# print PPP "$searchContent\n";
			# close(PPP);
			# exit;
			
			
			my ($Appl_Num) = &getDetailPage( $searchContent,$councilCode,$Council_Name,$Search_Code );
			@Appl_Num=@$Appl_Num;
	
			$i++;
			goto next_page;
			
		}
	} 
	else
	{
		print "Zero Page found\n";
	}
	
	return (@Appl_Num);
}

sub getDetailPage
{
	my $DetailContent = shift;
	my $councilCode = shift;
	my $Council_Name = shift;
	my $Search_Code = shift;
	
	while($DetailContent=~m/<div\s*class=\"plContainer\">\s*<a[^>]*?href=\"javascript\:__doPostBack\(\&\#39\;([^\,]*?)\&\#39\;\,\&\#39\;\&\#39\;\)\"[\w\W]*?<div\s*class=\"colRight\">\s*<span[^>]*?class=\"label\">\s*([^<]*?)\s*<\/span>/gsi)
	{
		my $viewValue = $2;
		push(@Appl_Num,$viewValue);
		undef $viewValue;
	}
	
	return(\@Appl_Num);
}

sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}