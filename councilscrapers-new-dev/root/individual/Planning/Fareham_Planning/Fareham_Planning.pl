use strict;
use WWW::Mechanize;
use HTML::Entities;
use HTTP::CookieMonster qw( cookies );
use URI::URL;
use Encode qw(decode encode);
use IO::Socket::SSL;
use Cwd;
use DBI;
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];
my $Date_Range = $ARGV[1];


if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"202\" \"GCS001\"\)", 16, "Error message");
    exit();
}


my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";
if($From_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$sDay = $1;
	$sMonth = $month{$2};
	$sYear = $3;
	print "sDay==>$sDay \n";
	print "sMonth==>$sMonth \n";
	print "sYear==>$sYear \n";
}
if($To_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$eDay = $1;
	$eMonth = $month{$2};
	$eYear = $3;
	print "eDay==>$eDay \n";
	print "eMonth==>$eMonth \n";
	print "eYear==>$eYear \n";
}





my $mech = WWW::Mechanize->new( 
	ssl_opts => {
	SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
	verify_hostname => 0, 
	}
	, autocheck => 0
);

#### COOKIE DECLARATION ####


		
### Proxy settings ###
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');
			
my $searchURL = "https://www.fareham.gov.uk/resources/acceptcookies.aspx?return=https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx";
$mech->get($searchURL);

my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
my @all_cookies = $monster->all_cookies;

my ($sessionid,$sessionval,$sessionsec,$sessionexp,$cookiesession)=("","","","","");
foreach my $cookie ( @all_cookies ) {
	$sessionid=$cookie->key;
	$sessionval=$cookie->val;
	$sessionsec=$cookie->secure;
	$sessionexp=$cookie->expires;
	print "Session: $sessionid::$sessionval\n";
	$cookiesession=$sessionid."=".$sessionval;
}

my $responseCode = $mech->status;
# print "responseCode==>$responseCode\n";
my $contents = $mech->content;
my $currentURL = $mech->uri();
my $Content = $mech->content;

open(PPP,">Search_Content.html");
print PPP "$Content\n";
close(PPP);
exit;



sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}