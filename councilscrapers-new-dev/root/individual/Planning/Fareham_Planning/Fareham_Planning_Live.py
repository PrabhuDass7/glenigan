import requests
import os, re, sys
from bs4 import BeautifulSoup
import threading
from datetime import datetime,timedelta
from multiprocessing.pool import ThreadPool
import pymssql

# Main Url
mainUrl = 'https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx'
folder_path = 'C:/Glenigan/Live_Schedule/Planning/Fareham_Planning'

proxies1 = {
    'http': 'http://172.27.137.192:3128',
    'https': 'http://172.27.137.192:3128',
}

proxies2 = {
    'http': 'http://172.27.137.199:3128',
    'https': 'http://172.27.137.199:3128',
}

# dbConnection Section
def dbConnection(database):
    conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
    return (conn)
	
def proxiesGenerator(proxies1, proxies2, homePageHeaders):
    i = 0
    while i <= 1:
        try:
            print "Main URL is: ", mainUrl
            res = requests.get(mainUrl, proxies=proxies1, headers=homePageHeaders)
            if res.status_code == 200:
                return proxies1

        except Exception as ex:
            print "Error is: ", str(type(ex).__name__)

            if str(type(ex).__name__) == "ProxyError":
                while i <= 2:
                    print  "Now trying in second Proxy", proxies2
                    res = requests.get(mainUrl, proxies=proxies2, headers=homePageHeaders)
                    print "second proxy URL status is: ", res.status_code
                    if res.status_code == 200:
                        return proxies2
                    else:
                        i = i + 1


def viewState(content):
    soup = BeautifulSoup (content, 'lxml')
    viewStateValue = soup.find ('input', {'id': '__VIEWSTATE'}).get ('value')
    return viewStateValue

def viewStateGenerator(content):
    soup = BeautifulSoup (content, 'lxml')
    viewStateGeneratorValue = soup.find ('input', {'id': '__VIEWSTATEGENERATOR'}).get ('value')
    return viewStateGeneratorValue

def eventValidation(content):
    soup = BeautifulSoup (content, 'lxml')
    eventValidationValue = soup.find ('input', {'id': '__EVENTVALIDATION'}).get ('value')
    return eventValidationValue

def viewStateRegex(content):
    r1 = 'VIEWSTATE\|([^<]*?)\|'
    c = re.findall (r1, str (content))
    return c[0]

def viewStateGeneratorRegex(content):
    r2 = 'VIEWSTATEGENERATOR\|([^<]*?)\|'
    d = re.findall (r2, str (content))
    return d[0]

def eventValidationRegex(content):
    r3 = '__EVENTVALIDATION\|([^<]*?)\|'
    e = re.findall (r3, str (content))
    return e[0]


# To collect Application URLs here
def collectAppURL(content, CouncilCode, Source, conn, cursor, workingproxy):	
	try:		 
		bulkValuesForQuery=''
		if re.findall(r'<div[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/div>', str(content), re.IGNORECASE):   
			AppURL=re.findall(r'<div[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^>]*?\s*<\/a>\s*<\/div>', str(content), re.IGNORECASE)       
			# print(AppURL)
			
			appCount = 0
			insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date,Easting,Northing) values '
			# with open(folder_path+"/202_out.html", 'wb') as fd:
				# fd.write(content)
			

			for appURL in AppURL:
				appURL = re.sub(r'\&amp\;', "&", str(appURL))
				PageHeaders = {
				'User-Agent' : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0",
				'host' : "www.fareham.gov.uk",
				'Content-Type' : "text/html; charset=utf-8"
				}
				res = requests.get(appURL, proxies=workingproxy, headers=PageHeaders)
				Appcontent=res.content	
				
				# with open("Appcontent.html","wb") as f:
					# f.write(str(Appcontent))
				# sys.exit()
				
				Application_Number=re.findall(r'<div[^>]*?>\s*Reference\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Application_Status=re.findall(r'<div[^>]*?>\s*Status\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Proposal=re.findall(r'<div[^>]*?>\s*Proposal\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Address=re.findall(r'<div[^>]*?>\s*Location\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)				
				DtAppReceived=re.findall(r'<div[^>]*?>\s*Received\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)				
				DtAppValid=re.findall(r'<div[^>]*?>\s*Accepted\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				
				# new_target_date=re.findall(r'>\s*Target\s*Decision\sDate\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
				# new_application_type=re.findall(r'>\s*Application\s*Type\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
				new_applicant_name=re.findall(r'<div[^>]*?>\s*Applicant\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^\,]*?)\,[^<]*?\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				new_applicant_addrs=re.findall(r'<div[^>]*?>\s*Applicant\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				new_agent_name=re.findall(r'<div[^>]*?>\s*Agent\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^\,]*?)\,[^<]*?\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				new_agent_addrs=re.findall(r'<div[^>]*?>\s*Agent\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)	
				Easting=re.findall(r'<div[^>]*?>\s*Easting\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Northing=re.findall(r'<div[^>]*?>\s*Northing\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				
				
				(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url,east,north) = ('','','','','','','','','','','','','','','','','','')

				if len(Application_Number) > 0:
					Application=clean(Application_Number[0])

				if len(Proposal) > 0:
					ProposalCnt=clean(Proposal[0])

				if len(Address) > 0:
					AppAddress=clean(Address[0])
					
				Document_Url=appURL

				if len(Application_Status) > 0:
					ApplicationStatus=clean(Application_Status[0])

				if len(new_applicant_name) > 0:
					ApplicantName=clean(new_applicant_name[0])
					
				if len(new_applicant_addrs) > 0:
					ApplicantAddress=clean(new_applicant_addrs[0])
					
				# if len(new_application_type) > 0:
					# ApplicationType=clean(new_application_type[0])
					
				if len(new_agent_name) > 0:
					AgentName=clean(new_agent_name[0])
					
				if len(new_agent_addrs) > 0:
					AgentAddress=clean(new_agent_addrs[0])
					
				if len(Easting) > 0:
					east=clean(Easting[0])
					
				if len(Northing) > 0:
					north=clean(Northing[0])
				
				if len(DtAppReceived) > 0:
					DateApplicationReceived=DtAppReceived
					
					
				if len(DtAppValid) > 0:
					DateApplicationvalidated=DtAppValid
					
				# if len(new_target_date) > 0:
					# TargetDecdt=new_target_date
					
				print("Application_Number::",Application_Number)		
				print("appURL::",appURL)
				
				format1 = "%Y/%m/%d %H:%M"					
				Schedule_Date = datetime.now().strftime(format1)

				Source_With_Time = Source+"_"+Schedule_Date+"-perl"
				
				Council_Name = "Fareham"
				joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived[0])+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated[0])+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"','"+str(east)+"','"+str(north)+"')"
				
				# print "ApplicationValues: ", joinValues

				if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
				elif appCount == 10:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					appCount=0
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					appCount += 1	
				
		else:
			print("ERROR!!!!!")
			
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno


def inputsection(fromDate, Todate, CouncilCode, Source):
    try:
        # MainUrl
        mainUrl = 'https://www.fareham.gov.uk/resources/acceptcookies.aspx?return=https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx'
		
        homePageHeaders = {
        'User-Agent' : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
		'host' : "www.fareham.gov.uk"
		}
        finProxy = proxiesGenerator(proxies1, proxies2, homePageHeaders)
        print "Final working proxy is: ", finProxy
        # Maintaining Session form the mainUrl

        sessionByRequest = requests.session ()
        req1 = sessionByRequest.get (mainUrl,proxies= finProxy, headers=homePageHeaders)
        # with open("content.html","wb") as f:
			# f.write(str(req1.content))
        # sys.exit()
		
        # getCookie = sessionByRequest.cookies.get_dict ()
        # print ("getCookie:::::", getCookie)
        if req1.status_code == 200:
			# Advanced Search Page - url, Request Headers and postcontent which are used to accept the  policies of the site.
			advancedSearchUrl = 'https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx'
			advancedSearchHeaders = {
				'Accept': '*/*',
				'Accept-Encoding': 'gzip, deflate, br',
				'Accept-Language': 'en-US,en;q=0.9',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
				'Host': 'www.fareham.gov.uk',
				'Origin': 'https://www.fareham.gov.uk',
				'Referer': 'https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx',
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
			}
			
			AdvancedPagePostCon = 'ctl00%24BodyPlaceHolder%24ScriptManager1=ctl00%24BodyPlaceHolder%24UpdatePanel1%7Cctl00%24BodyPlaceHolder%24uxLinkButtonShowAdvancedSearch&__EVENTTARGET=ctl00%24BodyPlaceHolder%24uxLinkButtonShowAdvancedSearch&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=' + str(viewState(req1.content).replace ('=', '%3D')) + '&__VIEWSTATEGENERATOR=' + str (viewStateGenerator (req1.content)) + '&__EVENTVALIDATION=' + str (eventValidation (req1.content)) + '&ctl00%24BodyPlaceHolder%24uxTextSearchKeywords=&ctl00%24BodyPlaceHolder%24uprnFromSearchKeywords=&ctl00%24BodyPlaceHolder%24appRefFromSearchKeywords=&hiddenInputToUpdateATBuffer_CommonToolkitScripts=1&__ASYNCPOST=true&'
			AdvancedPagePostCon = AdvancedPagePostCon.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
			# print(AdvancedPagePostCon)
			
			# Advanced search page requires Post method
			req2 = sessionByRequest.post (url=advancedSearchUrl, data=AdvancedPagePostCon, headers=advancedSearchHeaders, proxies= finProxy)
			
			if req2.status_code == 200:
				SearchUrl = 'https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx'
				SearchUrlHeaders = {
					'Accept': '*/*',
					'Accept-Encoding': 'gzip, deflate, br',
					'Accept-Language': 'en-US,en;q=0.9',
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
					'Host': 'www.fareham.gov.uk',
					'Origin': 'https://www.fareham.gov.uk',
					'Referer': 'https://www.fareham.gov.uk/casetrackerplanning/ApplicationSearch.aspx',
					'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
				}
				# print(req2.content)
				viewstate=re.findall(r'_viewstate\|([^<]*?)\|', str(req2.content), re.IGNORECASE)
				viewsategenerator=re.findall(r'_viewstategenerator\|([^<]*?)\|', str(req2.content), re.IGNORECASE)
				eventvalidation=re.findall(r'_eventvalidation\|([^<]*?)\|', str(req2.content), re.IGNORECASE)		
				# print(viewstate)
				# print(viewsategenerator)
				# print(eventvalidation)
				
				searchPagePostCon = 'ctl00%24BodyPlaceHolder%24ScriptManager1=ctl00%24BodyPlaceHolder%24UpdatePanel1%7Cctl00%24BodyPlaceHolder%24uxButtonSearch&ctl00%24BodyPlaceHolder%24uxTextSearchKeywords=&ctl00%24BodyPlaceHolder%24uprnFromSearchKeywords=&ctl00%24BodyPlaceHolder%24appRefFromSearchKeywords=&ctl00%24BodyPlaceHolder%24uxCurrentStatusDropDown=All&ctl00%24BodyPlaceHolder%24uxWardDropDown=All&ctl00%24BodyPlaceHolder%24uxStartDateDecisionTextBox=&ctl00%24BodyPlaceHolder%24uxStopDateDecisionTextBox=&ctl00%24BodyPlaceHolder%24uxDateDecisionStartMEE_ClientState=&ctl00%24BodyPlaceHolder%24uxDateDecisionStopMEE_ClientState=&ctl00%24BodyPlaceHolder%24uxStartDateReceivedTextBox=' + str(fromDate) + '&ctl00%24BodyPlaceHolder%24uxStopDateReceivedTextBox=' + str(Todate) + '&ctl00%24BodyPlaceHolder%24uxDateReceivedStartMEE_ClientState=&ctl00%24BodyPlaceHolder%24uxDateReceivedStopMEE_ClientState=&hiddenInputToUpdateATBuffer_CommonToolkitScripts=1&__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=' + str(viewstate[0].replace ('=', '%3D')) + '&__VIEWSTATEGENERATOR=' + str (viewsategenerator[0]) + '&__EVENTVALIDATION=' + str (eventvalidation[0]) + '&__ASYNCPOST=true&ctl00%24BodyPlaceHolder%24uxButtonSearch=Search'
				
				searchPagePostCon = searchPagePostCon.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
				# print(searchPagePostCon)
				
				# Date Range search page requires Post method
				req3 = sessionByRequest.post (url=SearchUrl, data=searchPagePostCon, headers=SearchUrlHeaders, proxies= finProxy)		
				# with open("content2.html","wb") as f:
					# f.write(str(req3.content))
				# sys.exit()
				
				return req3.content, finProxy
		
    except Exception as e:
        print str(e) + str(sys.exc_traceback.tb_lineno)

# Main section
if __name__ == "__main__":
    councilcode = sys.argv[1]
    gcs = sys.argv[2]

    conn = dbConnection("SCREENSCRAPPER")
    cursor = conn.cursor()

    # GCS calculation
    thisgcs = {
        "GCS001": "0",
        "GCS002": "7",
        "GCS003": "14",
        "GCS004": "21",
        "GCS005": "28",
        "GCS090": "90",
        "GCS180": "180",
    }

    gcsdate = thisgcs[gcs]
    date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
    format = "%d/%m/%Y"
	
    todate = date_N_days_ago.strftime(format)
	
    preday = date_N_days_ago - timedelta(days=6)
    fromdate = preday.strftime(format)
	
    print 'fromdate  :', fromdate
    print 'todate    :', todate

    # application extraction 
    searchContent, wrkProxy = inputsection(fromdate, todate, councilcode, gcs)
	
	
    finalinsertQuery = collectAppURL(str(searchContent), councilcode, gcs, conn, cursor, wrkProxy)
 
    if (finalinsertQuery != '') or finalinsertQuery is not None:
		cursor.execute(finalinsertQuery)
		conn.commit()