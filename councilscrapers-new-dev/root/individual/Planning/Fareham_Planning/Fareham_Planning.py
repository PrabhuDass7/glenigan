# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Planning/Fareham_Planning'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(fromDay, toDay, driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		driver.maximize_window()
		#-- Wait funtion
		time.sleep(10)
		homecontent=driver.page_source
		
		advfinalCnt=''
		if len(homecontent) > 0:
			advfinalCnt = homecontent.encode("utf-8")

		
		# with open(folder_path+"/202_out.html", 'wb') as fd:
			# fd.write(advfinalCnt)
		# raw_input()

		driver.find_element_by_xpath('//*[@id="lnkAllowCookies"]').click()
		time.sleep(10)
		driver.find_element_by_xpath('//*[@id="BodyPlaceHolder_uxLinkButtonShowAdvancedSearch"]').click()
		time.sleep(10)
		content=driver.page_source
		# driver.find_element_by_xpath('//*[@id="Mainpage_optValid"]').send_keys('optValid')
		print("fromDay::::", fromDay)		
		print("toDay::::", toDay)		
		driver.find_element_by_xpath('//*[@id="uxStartDateReceivedTextBox"]').send_keys(fromDay)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath('//*[@id="uxStopDateReceivedTextBox"]').send_keys(toDay)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="BodyPlaceHolder_uxButtonSearch"]').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		# with open(folder_path+"/202_out.html", 'wb') as fd:
			# fd.write(searchfinalCnt)
		# raw_input()
		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno



# To collect Application URLs here
def collectAppURL(content, home_url, driver, CouncilCode, Source, conn, cursor):	
	try:		 
		bulkValuesForQuery=''
		if re.findall(r'<div[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/div>', str(content), re.IGNORECASE):   
			AppURL=re.findall(r'<div[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^>]*?\s*<\/a>\s*<\/div>', str(content), re.IGNORECASE)       
			# print(AppURL)
			aplNum = 3
			appCount = 0
			insertQuery = 'insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date,Easting,Northing) values '
			with open(folder_path+"/202_out.html", 'wb') as fd:
				fd.write(content)
			
			for appURL in AppURL:
				appURL = re.sub(r'\&amp\;', "&", str(appURL))
				driver.get(appURL)
				time.sleep(10)  
				Appcontent=driver.page_source	
				# raw_input()
				# with open(folder_path+"/202_out_"+str(aplNum)+".html", 'wb') as fd:
					# fd.write(Appcontent)
				aplNum = aplNum + 1 
				# sys.exit()
				
				Application_Number=re.findall(r'<div[^>]*?>\s*Reference\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Application_Status=re.findall(r'<div[^>]*?>\s*Status\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Proposal=re.findall(r'<div[^>]*?>\s*Proposal\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Address=re.findall(r'<div[^>]*?>\s*Location\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)				
				DtAppReceived=re.findall(r'<div[^>]*?>\s*Received\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)				
				DtAppValid=re.findall(r'<div[^>]*?>\s*Accepted\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				
				# new_target_date=re.findall(r'>\s*Target\s*Decision\sDate\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
				# new_application_type=re.findall(r'>\s*Application\s*Type\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
				new_applicant_name=re.findall(r'<div[^>]*?>\s*Applicant\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^\,]*?)\,[^<]*?\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				new_applicant_addrs=re.findall(r'<div[^>]*?>\s*Applicant\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				new_agent_name=re.findall(r'<div[^>]*?>\s*Agent\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^\,]*?)\,[^<]*?\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				new_agent_addrs=re.findall(r'<div[^>]*?>\s*Agent\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)	
				Easting=re.findall(r'<div[^>]*?>\s*Easting\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				Northing=re.findall(r'<div[^>]*?>\s*Northing\s*<\/div>\s*<div\s*Class=\"detailsCells\s*detailsValues\">\s*([^<]*?)\s*<\/div>\s*<\/div>', str(Appcontent), re.IGNORECASE)
				
				
				(Application, ProposalCnt, ApplicationStatus, AppAddress, ApplicationType, DateApplicationReceived, DateApplicationRegistered, DateApplicationvalidated, ApplicantName, AgentName, AgentAddress, ApplicantAddress, AgentEmail, AgentTelephone, TargetDecdt, Document_Url,east,north) = ('','','','','','','','','','','','','','','','','','')

				if len(Application_Number) > 0:
					Application=clean(Application_Number[0])

				if len(Proposal) > 0:
					ProposalCnt=clean(Proposal[0])

				if len(Address) > 0:
					AppAddress=clean(Address[0])
					
				Document_Url=appURL

				if len(Application_Status) > 0:
					ApplicationStatus=clean(Application_Status[0])

				if len(new_applicant_name) > 0:
					ApplicantName=clean(new_applicant_name[0])
					
				if len(new_applicant_addrs) > 0:
					ApplicantAddress=clean(new_applicant_addrs[0])
					
				# if len(new_application_type) > 0:
					# ApplicationType=clean(new_application_type[0])
					
				if len(new_agent_name) > 0:
					AgentName=clean(new_agent_name[0])
					
				if len(new_agent_addrs) > 0:
					AgentAddress=clean(new_agent_addrs[0])
					
				if len(Easting) > 0:
					east=clean(Easting[0])
					
				if len(Northing) > 0:
					north=clean(Northing[0])
				
				if len(DtAppReceived) > 0:
					DateApplicationReceived=DtAppReceived
					
					
				if len(DtAppValid) > 0:
					DateApplicationvalidated=DtAppValid
					
				# if len(new_target_date) > 0:
					# TargetDecdt=new_target_date
					
				print("Application_Number::",Application_Number)		
				print("appURL::",appURL)
				
				format1 = "%Y/%m/%d %H:%M"					
				Schedule_Date = datetime.now().strftime(format1)

				Source_With_Time = Source+"_"+Schedule_Date+"-perl"
				
				Council_Name = "Fareham"
				joinValues="('"+str(CouncilCode)+"','"+str(Council_Name)+"','"+str(AppAddress)+"','"+str(DateApplicationReceived[0])+"','"+str(Application)+"','"+str(DateApplicationRegistered)+"','"+str(DateApplicationvalidated[0])+"','"+str(ProposalCnt)+"','"+str(ApplicationStatus)+"','','"+str(AgentAddress)+"','','"+str(AgentName)+"','"+str(AgentTelephone)+"','"+str(ApplicantAddress)+"','"+str(ApplicantName)+"','"+str(ApplicationType)+"','"+str(AgentEmail)+"','','','','','','','','','','"+str(TargetDecdt)+"','','"+str(appURL)+"','"+str(Document_Url)+"','','"+str(Source_With_Time)+"','"+str(Schedule_Date)+"','"+str(east)+"','"+str(north)+"')"
				
				# print "ApplicationValues: ", joinValues

				if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
				elif appCount == 10:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					appCount=0
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					appCount += 1	
				
		else:
			print("ERROR!!!!!")
			
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("SCREENSCRAPPER")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	# chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	print("gcs::",gcs)
	
	gcsdate = thisgcs[gcs]	
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	# format = "%d/%m/%Y"
	format = "%m/%d/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	
	print 'fromdate  :', fromdate
	print 'todate    :', todate
		
	
	homeURL = 'https://www.fareham.gov.uk/casetrackerplanning/applicationsearch.aspx'
	
	results = inputsection(fromdate, todate, browser, homeURL)
	finalinsertQuery = collectAppURL(str(results), homeURL, browser, councilcode, gcs, conn, cursor)
	# print type(finalinsertQuery)
	
	if (finalinsertQuery != ''):
		# print (finalinsertQuery)
		cursor.execute(finalinsertQuery)
		conn.commit()
	
	browser.quit()