use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"385\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Planning();




### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Planning/Stroud_Planning/Stroud_Planning.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";
	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0, autoclose => 1);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
    verify_hostname => 0, 
	autoclose => 1,
});
}	


### Proxy settings ###
if($Council_Code=~m/^(385)$/is)
{
	$mech->proxy(['http','https'], 'http://172.27.137.192:3128');
}


# Get search results using date ranges

my ($Responce, $cookie_jar, $Ping_Status1);

$Responce = $mech->get($HOME_URL);

if($Council_Code=~m/^(385)$/is)
{	
	$mech->form_number($FORM_NUMBER);
	$mech->set_fields( $RADIO_BTN_NME => $RADIO_BTN_VAL );
	$mech->click();
}


my $contents = $mech->content;
open(PP, ">homecontent.html");
print PP "$contents\n";
close(PP); 

my $viewstate= uri_escape($1) if($contents=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?\s*value=\"([^\"]*?)\"[^>]*?>/is);
my $viewstategen= uri_escape($1) if($contents=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?\s*value=\"([^\"]*?)\"[^>]*?>/is);
my $PREVIOUSPAGE= uri_escape($1) if($contents=~m/<input[^>]*?name=\"__PREVIOUSPAGE\"[^>]*?\s*value=\"([^\"]*?)\"[^>]*?>/is);
my $EVENTVALIDATION= uri_escape($1) if($contents=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?\s*value=\"([^\"]*?)\"[^>]*?>/is);
my $RequestVerificationToken= uri_escape($1) if($contents=~m/<input[^>]*?name=\"__RequestVerificationToken\"[^>]*?\s*value=\"([^\"]*?)\"[^>]*?>/is);

my $DumFrom_Date = $From_Date;
my $DumTo_Date = $To_Date;
$DumFrom_Date=~s/\//%2F/gsi;
$DumTo_Date=~s/\//%2F/gsi;

my $post_con = "__EVENTTARGET=ctl00%24MainContent%24advancedLinkButton&__EVENTARGUMENT=&__VIEWSTATE=$viewstate&__VIEWSTATEGENERATOR=$viewstategen&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=1026&__PREVIOUSPAGE=$PREVIOUSPAGE&__EVENTVALIDATION=$EVENTVALIDATION&__RequestVerificationToken=$RequestVerificationToken&ctl00%24MainContent%24HiddenField1=&ctl00%24MainContent%24receivedafter=$DumFrom_Date&ctl00%24MainContent%24receivedb4=$DumTo_Date&ctl00%24MainContent%24dropapptype=&ctl00%24MainContent%24dropParish=&ctl00%24MainContent%24DropWard=&ctl00%24MainContent%24txtProposal=&ctl00%24MainContent%24txtAddress=%25&ctl00%24MainContent%24txtPostcode=&ctl00%24MainContent%24txtapplicantName=&ctl00%24MainContent%24txtAgentName=&ctl00%24MainContent%24ApplicationDecided=N";


$mech->post( $SEARCH_URL, Content => "$post_con");

my $content = $mech->content;
my $code = $mech->status;
my $Search_Content=$content;

open(PP, ">app_cont.html");
print PP "$Search_Content\n";
close(PP); 
# exit;

my $totalPage = $1 if($Search_Content=~m/$TOTAL_PAGE_COUNT/is);


&Scrape_Details( $Search_Content,$Council_Code,$SEARCH_URL,$COUNCIL_NAME,$code );


sub Scrape_Details
{
	my $searchContent = shift;
	my $councilCode = shift;
	my $Page_url = shift;
	my $Council_Name = shift;
	my $Search_Code = shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_ACCESS_STAGING (Council_Code, Council_Name, Address, Proposal, Date_Application_Received, Application_Type, Application, Date_Application_validated, Date_Application_Registered, Application_Status, Agent_Name, Applicant_name, Applicant_Address, Agent_Address, Agent_Telephone, TargetDec_dt, Source, Schedule_Date,Document_Url,PAGE_URL,Agent_Email) values';
	print "Council_Code: $Council_Code\n";
	
	while($searchContent=~m/<tr>\s*<td>\s*<a\s*href=\"javascript\:expandcollapse\(\'div([^\']*?)\'\)\;\"\s*title=\"show\/hide\s*further\s*information\">\s*([\w\W]*?)\s*<\/td>\s*<\/tr>\s*<\/td>\s*<\/tr>/gis)
	{
		my $App_nums = $1;
		my $App_Page_Content = $2;
		
		my $Application					= &clean($1) if($App_Page_Content=~m/<td>\s*<span\s*id=\"refvalid4\">\s*<b>\s*([^<]*?)\s*<\/b>\s*<\/span>\s*<\/td>/is);
		my $Application_Type			= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Application\s*Type\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Address						= &clean($1) if($App_Page_Content=~m/<td>\s*<span\s*id=\"refvalid4\">\s*<b>\s*[^<]*?\s*<\/b>\s*<\/span>\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is);	
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<td>\s*<span\s*id=\"refvalid4\">\s*<b>\s*[^<]*?\s*<\/b>\s*<\/span>\s*<\/td>\s*<td[^>]*?>\s*[^<]*?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>/is);	
		my $Date_Application_Received	= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Application\s*received\s*on\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Date_Application_validated	= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Application\s*validated\s*on\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Date_Application_Registered	= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Application\s*Registered\s*on\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);	
		my $Applicant_name				= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Applicant\s*Name\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Agent_Name					= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Agent\s*Name\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Case_Officer				= &clean($1) if($App_Page_Content=~m/<tr>\s*<td[^>]*?>\s*Handling\s*Officer\:?\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/<td>\s*<span\s*id=\"refvalid4\">\s*<b>\s*[^<]*?\s*<\/b>\s*<\/span>\s*<\/td>\s*<td[^>]*?>\s*[^<]*?\s*<\/td>\s*<td[^>]*?>\s*[\w\W]*?\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is);
		my $Agent_Telephone;
		my $TargetDec_dt;
		my $Applicant_Address;
		my $Agent_Address;
		my $Doc_Url="https://www.stroud.gov.uk/apps/getAssocDocs.ashx?appref=".$Application;
		my $Application_Link=$Page_url;
		my $Agent_Email;
		
		$Address=~s/\'/\'\'/gsi;	
		my $Source = "GCS001";	
		
		if($Application=~m/^\s*$/is)
		{
			$Application=$App_nums;
		}
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$insert_query.="(\'$Council_Code\', \'$COUNCIL_NAME\', \'$Address\', \'$Proposal\', \'$Date_Application_Received\', \'$Application_Type\', \'$Application\', \'$Date_Application_validated\', \'$Date_Application_Registered\', \'$Application_Status\', \'$Agent_Name\', \'$Applicant_name\', \'$Applicant_Address\', \'$Agent_Address\', \'$Agent_Telephone\', \'$TargetDec_dt\', \'$Source_With_Time\', \'$Schedule_Date\',\'$Doc_Url\',\'$Application_Link\',\'$Agent_Email\'),";
		
		undef $Applicant_name; undef $Agent_Name; undef $Application_Link; undef $Application; undef $Proposal; undef $Application_Type; undef $Address; undef $Date_Application_Received; undef $Date_Application_validated; undef $Date_Application_Registered; undef $Doc_Url; undef $Agent_Email; undef $TargetDec_dt; undef $Agent_Telephone; undef $Agent_Address; undef $Applicant_Address; undef $Application_Status;
	}
	
	
		
	$insert_query=~s/\,$//igs;
	
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
	
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}