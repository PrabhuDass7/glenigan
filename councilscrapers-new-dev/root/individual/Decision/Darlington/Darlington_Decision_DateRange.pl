use strict;
use WWW::Mechanize::Firefox;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;


my $Council_Code = $ARGV[0];
my $From_Date =$ARGV[1];
my $To_Date=$ARGV[2];
# my $Date_Range = $ARGV[1];

# if(($Council_Code eq "") or ($Date_Range eq ""))
# {
	# Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"172\" \"GCS001\"\)", 16, "Error message");
    # exit();
# }

# chomp($Date_Range);

# my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
# print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();

system('"C:/Program Files/Mozilla Firefox/firefox.exe"');
# my $PID = $$;
# print "PID==>$PID\n"; <STDIN>;

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Darlington/Darlington_Decision.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";


#### UserAgent Declaration ####	
my $mech = WWW::Mechanize::Firefox->new(autoclose => 1);


### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}


# Get search results using date ranges

$mech->get($HOME_URL);
sleep(15);
$mech->form_number($FORM_NUMBER);
$mech->set_fields($FORM_START_DATE => $From_Date);
$mech->set_fields($FORM_END_DATE => $To_Date);
$mech->click({ xpath => "//*[\@id=\"_id113:_id182\"]" });
sleep(10);


my $content = $mech->content;
my $code = $mech->status;
print "$code\n";
my $datePage_urls = $mech->uri();
my $Home_Content=$content;

# open(PPP,">Search_Content1.html");
# print PPP "$Home_Content\n";
# close(PPP);
# exit;
&Scrape_Details($Home_Content,$datePage_urls);

my $pageCount;
if($Home_Content=~m/<table\s*class=\"scroller\">\s*([\w\W]*?)\s*<\/table>/is)
{
	my $page_num = $1;
	my $count=0;
	while($page_num=~m/<td[^>]*?>\s*<a\s*href=\"#\"\s*onclick=\"return oamSubmitForm[^>]*?>(\d+)<\/a>\s*<\/td>/gsi)
	{
		$count++;
	}
	$pageCount=$count;
}

print "TotalPage==>$pageCount\n";

my $i=2;
next_page:
if($i<=$pageCount)
{	
	print "$i======$pageCount\n";	
	$mech->click({ xpath => "//*[\@id=\"_id116:scroll_2idx$i\"]" });
	sleep(5);
	my $datePage_url = $mech->uri();
	my $nextpagecontent = $mech->content;
	my $nextpagecontent = $mech->content;
	open(PP,">Search_Content$i.html");
	print PP "$nextpagecontent\n";
	close(PP);
	
	&Scrape_Details($nextpagecontent,'');
	
	
	$i++;
	goto next_page;
}

# system("taskkill /IM firefox.exe /F");
# system("taskkill /PID $PID /F");

sub Scrape_Details
{
	my $App_PageContent = shift;
	my $App_Link = shift;
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';
	while($App_PageContent=~m/<input\s*id=\"[^>]*?results[^>]*?\"\s*name=\"([^\"]*?)\"\s*value=\"([^\"]*?)\"[^>]*?>/gsi)
	{
		my $click=$1;
		my $App_Num=$2;
		
		print "Application_Number==>$App_Num\n";
		
		$mech->click({ xpath => "//*[\@id=\"$click\"]" });
		sleep(5);
		my $App_Links = $mech->uri();
		my $App_Page_Content = $mech->content;
		# open(PP,">App_Content.html");
		# print PP "$App_Page_Content\n";
		# close(PP);
		# exit;
		
		my $Application			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>Case\s*No\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
		my $Proposal			= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Proposal\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
		my $Date_Decision_Made	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Decision\s*Date\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);	
		my $Decision_Status		= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Decision\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);	
		
		my $Application_Status;
		my $Application_Link=$App_Links;
			
		my $Source = "GCS001";	
		
		if($Application=~m/^\s*$/is)
		{
			$Application=$App_Num;
		}
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
		
		undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 			
		
		my $back = $1 if($App_Page_Content=~m/<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"Back\"[^>]*?>/is);
		$mech->click({ xpath => "//*[\@id=\"$back\"]" });
		sleep(5);
	}
	
	
		
	$insert_query=~s/\,$//igs;
	
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
	
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}


######  Clean  ######
sub clean()
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\=\&\s*/\=/igs;
	$Data=~s/\&\s*$//igs;
	$Data=~s/\&amp$//igs;
	$Data=~s/^\s*//igs;
	
	return($Data);
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}