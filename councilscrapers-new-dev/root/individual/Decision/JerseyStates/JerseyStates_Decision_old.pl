use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"467\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


# my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";
if($From_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$sDay = $1;
	# $sMonth = $month{$2};
	$sMonth = $2;
	$sYear = $3;
	$sDay=~s/^0//si;
	$sMonth=~s/^0//si;
	print "sDay==>$sDay \n";
	print "sMonth==>$sMonth \n";
	print "sYear==>$sYear \n";
}
if($To_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$eDay = $1;
	# $eMonth = $month{$2};
	$eMonth = $2;
	$eYear = $3;
	
	$eDay=~s/^0//si;
	$eMonth=~s/^0//si;
	print "eDay==>$eDay \n";
	print "eMonth==>$eMonth \n";
	print "eYear==>$eYear \n";
}


my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://www.gov.je/citizen/Planning/Pages/planning.aspx";
# $mech->get($searchURL);
# my $Content = $mech->content;
# my $status = $mech->status;


my $searchPagePostCont = '{"URL":"https://www.gov.je//citizen/Planning/Pages/planning.aspx","CommonParameters":"|05|1||||","SearchParameters":"|1301||||0|All|All|<FromDay>|<FromMonth>|<FromYear>|<ToDay>|<ToMonth>|<ToYear>"}';


# open(DC,">Content.html");
# print DC $Content;
# close DC;

$searchPagePostCont=~s/<FromDay>/$sDay/igs;
$searchPagePostCont=~s/<ToDay>/$eDay/igs;
$searchPagePostCont=~s/<FromMonth>/$sMonth/igs;
$searchPagePostCont=~s/<ToMonth>/$eMonth/igs;
$searchPagePostCont=~s/<FromYear>/$sYear/igs;
$searchPagePostCont=~s/<ToYear>/$eYear/igs;
	
$mech->add_header( "Accept" => 'text/plain, */*; q=0.01' );
$mech->add_header( "Host" => 'www.gov.je' );
$mech->add_header( "Content-Type" => 'application/json; charset=UTF-8' );
$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
$mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
$mech->add_header( "Origin" => 'https://www.gov.je' );
$mech->add_header( "Referer" => "$searchURL" );	

$mech->post('https://www.gov.je/_layouts/15/PlanningAjaxServices/PlanningSearch.svc/Search', Content => "$searchPagePostCont");
my $SearchResultContent = $mech->content;
print $mech->status;

# open(DC,">SearchResultContent.html");
# print DC $SearchResultContent;
# close DC;
# exit;

my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

my $MaxPage=$1 if($SearchResultContent=~m/\"\s*Max\s*Page\s*Number\s*\"\s*\:\s*([\d]+?)\s*\,/is);

my $Page_Number=1;
NextPage:
while($SearchResultContent=~m/<a\s*href\s*=\s*\\"([^>]*?)\\"\s*>\s*See\s*details[^>]*?to\s*([^>]*?)\s*<\\\/a>/igs)
{
	my $Application_URL=$1;
	my $Application_Num=$2;
	
	
	
	$Application_URL=~s/\\\//\//gsi;
	$Application_URL=~s/\&amp;/\&/gsi;
	$Application_Num=~s/\\\//\//gsi;
	
	
	print "Application_Num==>$Application_Num\n";	
	print "Application_URL=>$Application_URL\n";
	
	$mech->get($Application_URL);
	my $App_content = $mech->content;		

	# open(DC,">App_content.html");
	# print DC $App_content;
	# close DC;
	# exit;
	
	my ($Application,$Proposal,$Decision_Status,$Application_Status,$Date_Decision_Made);
	
	$Application=&clean($1) if($App_content=~m/Application\s*reference\s*:\s*([^>]*?)\s*</is);

	if($App_content=~m/<a\s*href\s*=\s*"([^>]*?)\"\s*>\s*Application\s*timeline\s*<\/a>/is)
	{
		my $Application_timeline='https://www.gov.je'.$1;
		$mech->get($Application_timeline);
		my $App_content_tmp = $mech->content;
			
		$Date_Decision_Made=&clean($1) if($App_content_tmp=~m/<span\s*id[^>]*?_lblDecisiondate\"\s*>\s*([^>]*?)\s*</is);
		print "Before Date_Decision_Made==>$Date_Decision_Made\n";
		$Date_Decision_Made=~s/^\s*n\/a\s*$//igs;
		print "After Date_Decision_Made==>$Date_Decision_Made\n";
	}
	$Application_Status=&clean($1) if($App_content=~m/<span\s*id[^>]*?_lblStatus\"\s*>\s*([^>]*?)\s*</is);
	$Decision_Status=&clean($1) if($App_content=~m/<span\s*id[^>]*?_lblStatus\"\s*>\s*([^>]*?)\s*</is);
	$Proposal=&clean($1) if($App_content=~m/<span\s*id[^>]*?_lblDescription\"\s*>\s*([^>]*?)\s*</is);
	
	
	
	my $COUNCIL_NAME = 'Jersey States';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	# my $Source = "GCS001";	
	my $Source = $Date_Range;	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_URL\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_URL;
	
}

if($SearchResultContent=~m/<a\s*id[^>]*?NextButtonBottom[^>]*?>\s*Next\s*Page\s*<\\\/a>/is)
{	
	my $nextPagePostCont = '{"URL":"https://www.gov.je//citizen/Planning/Pages/planning.aspx","CommonParameters":"|05|<Page_number>|<Total_Page>|||","SearchParameters":"|1301||||0|All|All|<FromDay>|<FromMonth>|<FromYear>|<ToDay>|<ToMonth>|<ToYear>"}';
	
	
	$nextPagePostCont=~s/<Page_number>/$Page_Number/igs;
	$nextPagePostCont=~s/<Total_Page>/$MaxPage/igs;
	
	
	$nextPagePostCont=~s/<FromDay>/$sDay/igs;
	$nextPagePostCont=~s/<ToDay>/$eDay/igs;
	$nextPagePostCont=~s/<FromMonth>/$sMonth/igs;
	$nextPagePostCont=~s/<ToMonth>/$eMonth/igs;
	$nextPagePostCont=~s/<FromYear>/$sYear/igs;
	$nextPagePostCont=~s/<ToYear>/$eYear/igs;
	

	
	$mech->add_header( "Accept" => 'text/plain, */*; q=0.01' );
	$mech->add_header( "Host" => 'www.gov.je' );
	$mech->add_header( "Content-Type" => 'application/json; charset=UTF-8' );
	$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
	$mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
	$mech->add_header( "Origin" => 'https://www.gov.je' );
	$mech->add_header( "Referer" => "$searchURL" );	

	$mech->post('https://www.gov.je/_layouts/15/PlanningAjaxServices/PlanningSearch.svc/Search', Content => "$nextPagePostCont");
	my $content = $mech->content;
	$SearchResultContent = $content;
	# open(DC,">SearchResultContent.html");
	# print DC $SearchResultContent;
	# close DC;
	# exit;

	
	print "Page_Number: $Page_Number\n";
	
	$Page_Number++;
	
	
	goto NextPage;
}

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
# print "insert_query: $insert_query\n";


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=Glenigan;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}