# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Decision/Cherwell'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		
		advfinalCnt=''
		if len(content) > 0:
			advfinalCnt = content.encode("utf-8")

		
		# with open(folder_path+"/151_out.html", 'wb') as fd:
			# fd.write(advfinalCnt)
		# raw_input()

		driver.find_element_by_xpath('//*[@id="SearchPlanning"]').click()
		# driver.find_element_by_xpath('//*[@id="SearchPlanning"]').send_keys('true')

		fDate = re.findall("<td[^>]*?>\s*Decision\s*Date\s*\:\s*<\/td>\s*<td>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advfinalCnt), re.IGNORECASE)  
		# print(str(fDate[0]))
		fromDate = '//*[@id="'+str(fDate[0])+'"]'
		

		tDate = re.findall("<td[^>]*?>\s*Decision\s*Date\s*\:\s*<\/td>\s*<td>\s*<input[^>]*?id=\"[^\"]*?\"[^>]*?>[\w\W]*?<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advfinalCnt), re.IGNORECASE)  
		# print(str(tDate[0]))
		toDate = '//*[@id="'+str(tDate[0])+'"]'
		
		driver.find_element_by_xpath(fromDate).send_keys(dumFrom_Date)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath(toDate).send_keys(dumTo_Date)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="submitBtn"]/span').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		with open(folder_path+"/151_out.html", 'wb') as fd:
			fd.write(searchfinalCnt)
		# raw_input()
		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno



# To collect Application URLs here
def collectAppURL(content, home_url):	
	try:
		appURLCollections=[]
		appListContent=re.findall(r'<table\s*class=\"table-striped\s*tblResults\">([\w\W]*?)\s*<\/table>\s*<\/div>', str(content), re.IGNORECASE)	
		appListContent = re.sub(r'\\t', " ", str(appListContent)) 
		appListContent = re.sub(r'\\n', " ", str(appListContent)) 
		appListContent = re.sub(r'\\r', "", str(appListContent)) 
		
		if re.findall(r'<td[^>]*?>\s*<span[^>]*?>\s*Reference\s*No\.<\/span>\s*<strong>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>[^<]*?<\/a>', str(appListContent), re.IGNORECASE):
			appUrls=re.findall(r'<td[^>]*?>\s*<span[^>]*?>\s*Reference\s*No\.<\/span>\s*<strong>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>[^<]*?<\/a>', str(appListContent), re.IGNORECASE) 
			
			for appUrl in appUrls:
				if not re.findall(r'^http', str(appUrl)):
					absoluteRegionURL = urljoin(home_url,appUrl)
					appURLCollections.append(absoluteRegionURL)
				else:
					appURLCollections.append(appUrl)
			
		return appURLCollections				
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# nextPageSection parse section	
def nextPageSection(searchcontent, homeURL, driver):
	try:
		searchcount = re.findall("<li[^>]*?>\s*Planning\s*\((\d+)\)\s*<\/li>", str(searchcontent), re.IGNORECASE)  
		print "searchcount:", int(searchcount[0])
		
		pageNumber = 2
		pageNum = 3
		fullAppURLs=[]
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		if re.findall(r'<li>\s*<a[^>]*?>\s*Next\s*<\/a>\s*<\/li>', str(searchcontent)):
			collectAppURLs = collectAppURL(str(searchcontent), homeURL)
			fullAppURLs.extend(collectAppURLs)
			check_next_page = re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(searchcontent), re.I):					
					driver.find_element_by_xpath('//*[@id="content"]/div/div[3]/div/div[2]/div/ul/li['+str(pageNum)+']/a').click()	
					#-- Wait funtion
					time.sleep(10)  
					appNextPagecontent=driver.page_source
					
					# with open(folder_path+"/151_out1.html", 'wb') as fd:
						# fd.write(appNextPagecontent)
					# sys.exit()
					
					collectNextPageAppURLs = collectAppURL(str(appNextPagecontent), homeURL)	
					fullAppURLs.extend(collectNextPageAppURLs)
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I):
						check_next_page = re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			collectAppURLs = collectAppURL(str(searchcontent), homeURL)				
			fullAppURLs.extend(collectAppURLs)
			
		
		return fullAppURLs

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application details parse section	
def appParseSection(appURLsList, CouncilCode, Source, conn, cursor, driver):	
	try:
		appCount = 0
		insertQuery = 'insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values '
		
		bulkValuesForQuery=''
		
		for appURL in appURLsList:
			print("ApplicationURL::", appURL)
			driver.get(appURL)
			time.sleep(5)
			Appcontent=driver.page_source
			
			# with open(folder_path+"/151_Appcontent.html", 'wb') as fd:
				# fd.write(Appcontent)
			# sys.exit()
			
			Application_Number=re.findall(r'>\s*Application\s*Number\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
			Application_Status=re.findall(r'>\s*Status\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
			Proposal=re.findall(r'>\s*Proposal\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([\w\W]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
			new_decision_type=re.findall(r'>\s*Decision\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
			DtDecision=re.findall(r'>\s*Decision\s*Issued\s*Date\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*(?:<[^>]*?>)?\s*([^<]*?)\s*(?:<[^>]*?>)?\s*<\/div>\s*<\/td>', str(Appcontent), re.IGNORECASE)
									
			(Application, ProposalCnt, ApplicationStatus, Date_Decision_Dispatched, Decision, DateDecisionMade) = ('','','','','','')

			if len(Application_Number) > 0:
				Application=clean(Application_Number[0])

			if len(Proposal) > 0:
				ProposalCnt=clean(Proposal[0])

			if len(Application_Status) > 0:
				ApplicationStatus=clean(Application_Status[0])

			if len(new_decision_type) > 0:
				Decision=clean(new_decision_type[0])
				
			if len(DtDecision) > 0:
				DateDecisionMade=DtDecision
			
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			
			Council_Name = "Cherwell"
			joinValues="("+"'"+str(Application)+"','"+str(ProposalCnt)+"','"+str(Decision)+"','"+str(DateDecisionMade[0])+"','"+str(Date_Decision_Dispatched)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+Source_With_Time+"','"+str(ApplicationStatus)+"')"
			
			print "ApplicationValues: ", joinValues
			bulkValuesForQuery = insertQuery+joinValues
			cursor.execute(bulkValuesForQuery)
			conn.commit()
			bulkValuesForQuery=''
			

			# if appCount == 0:
				# bulkValuesForQuery = insertQuery+joinValues
				# appCount += 1
			# elif appCount == 20:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# cursor.execute(bulkValuesForQuery)
				# conn.commit()
				# appCount=0
				# bulkValuesForQuery=''
			# else:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# appCount += 1		
				
				
		print "InsertQuery: ", bulkValuesForQuery

		return bulkValuesForQuery  

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
		
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	
	homeURL = 'https://planningregister.cherwell.gov.uk/Search/Advanced'
	
	results = inputsection(fromdate, todate, browser, homeURL)

	fullAppURLs = nextPageSection(results, homeURL, browser)
	
	finalinsertQuery = appParseSection(fullAppURLs, councilcode, gcs, conn, cursor, browser)
	
	if (finalinsertQuery != ''):
		cursor.execute(finalinsertQuery)
		conn.commit()
	
	browser.quit()