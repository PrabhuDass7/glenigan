use strict;
use WWW::Mechanize;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL qw();
use Config::Tiny;
use Time::Piece;
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $inFrom_Date = $ARGV[1];
my $inTo_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"449\" \"GCS001\"\)", 16, "Error message");
    exit();
}

my $From_Date = $inFrom_Date;
my $To_Date = $inTo_Date;

print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Telford_Decision/Telford_Decision.ini" );

my $COUNCIL_NAME 	= $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL 		= $Config->{$Council_Code}->{'HOMEURL'};
my $HOST 			= $Config->{$Council_Code}->{'HOST'};
my $FORM_NUMBER 	= $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE 	= $Config->{$Council_Code}->{'FORM_END_DATE'};
my $SSL_VERIFICATION= $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $SRCH_BTN_NME	= $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL	= $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX=$Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $NEXT_PAGE_REGEX	= $Config->{$Council_Code}->{'NEXT_PAGE_REGEX'};
my $FILTER_URL 		= $Config->{$Council_Code}->{'FILTER_URL'};
my $SEARCH_URL 		= $Config->{$Council_Code}->{'SEARCH_URL'};
my $SRCH_NXT_PAGE	= $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "HOME_URL	: $HOME_URL\n";

my $mech = WWW::Mechanize->new( 
					ssl_opts => {
									SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
									verify_hostname => 0, 
								}
								, autocheck => 0
					);

### Proxy settings ###

print "Scraping via proxy 199...\n";
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');


# now fetch page 

$mech->get( $HOME_URL );
my $Code_status = $mech->status;
my $content = $mech->content;

my ($Temp_From_Date,$Temp_To_Date);
if($From_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_From_Date = $1."-".$2."-".$3;
}
if($To_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_To_Date = $1."-".$2."-".$3;
}
print "Temp_From_Date::$Temp_From_Date\n";
print "Temp_To_Date::$Temp_To_Date\n";

my ($VIEWSTATE,$VIEWSTATEGENERATOR,$PREVIOUSPAGE,$EVENTVALIDATION,$EVENTTARGET,$EVENTARGUMENT);
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATE=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATEGENERATOR=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTVALIDATION=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTTARGET=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTARGUMENT=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$PREVIOUSPAGE=uri_escape($1);
}
	
my $post_content = "__EVENTTARGET=$EVENTTARGET&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__PREVIOUSPAGE=$PREVIOUSPAGE&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24txtPlanningKeywords=&ctl00%24ContentPlaceHolder1%24dlPlanningParishs=0&ctl00%24ContentPlaceHolder1%24dlPlanningWard=0&ctl00%24ContentPlaceHolder1%24ddlPlanningapplicationtype=0&ctl00%24ContentPlaceHolder1%24DCdatefrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24DCdateto=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24txtDCAgent=&ctl00%24ContentPlaceHolder1%24txtDCApplicant=&ctl00%24ContentPlaceHolder1%24btnSearchPlanningDetails=Search";

my $searchResultCon;
my @fullAPPURLs;
if($post_content ne '')
{
	$mech->post( $SEARCH_URL, Content => "$post_content");
	my $content1 = $mech->content;
	
	my ($VIEWSTATE,$VIEWSTATEGENERATOR,$PREVIOUSPAGE,$EVENTVALIDATION,$EVENTTARGET,$EVENTARGUMENT);
	if($content1=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$VIEWSTATE=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$VIEWSTATEGENERATOR=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$EVENTVALIDATION=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$EVENTTARGET=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$EVENTARGUMENT=uri_escape($1);
	}
	if($content1=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
	{
		$PREVIOUSPAGE=uri_escape($1);
	}
	
	my $post_content1 = "__EVENTTARGET=ctl00%24ContentPlaceHolder1%24lbPlanning2ndLevel3&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop";
	
	if($post_content1 ne '')
	{
		$mech->post( $SEARCH_URL, Content => "$post_content1");
		my $content2 = $mech->content;
		$searchResultCon=$content2;
				
		# open(PP,">a.html");
		# print PP $searchResultCon;
		# close(PP);
		
		my $fullAppURLs = &scrape_details($searchResultCon);
		@fullAPPURLs = @{$fullAppURLs};
	}
	
}
	
&collectAppDetails(\@fullAPPURLs);


sub collectAppDetails
{
	my $AppURLs = shift;	
	my @AppURLs = @{$AppURLs};
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

	my ($Application,$Proposal,$Decision_Status,$Application_Status,$ApplicationLink,$Date_Decision_Made);
	
	foreach $ApplicationLink(@AppURLs)
	{
		# print "ApplicationLink=>$ApplicationLink\n";
		$mech->get($ApplicationLink);
		my $App_Page_Content = $mech->content;
		
		$Application				= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>Application\s*number<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		
		print "Now processing application::$Application\n";
		
		if($App_Page_Content=~m/<ul\s*id=\"ApplicationStatuslist\">\s*([\w\W]*?)\s*<ul>/is)
		{
			my $status = $1;			
			$Application_Status			= &clean($1) if($App_Page_Content=~m/<li\s*class=\"[^\"]*?selected\">\s*([^<]*?)\s*<\/li>/is);
		}
		
		$Proposal					= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>(?:Description\s*of)?\s*proposal<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*(.*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Decision_Status			= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Decision\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Date_Decision_Made			= &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Decision\s*date\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
			
			
		my $Source = "GCS001";	
	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";	
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$ApplicationLink\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
       
        undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $ApplicationLink; 			
	}
	$insert_query=~s/\,$//igs;
		
	print "insert_query::$insert_query\n";
	
	
	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
}

sub scrape_details()
{
	my $dtlCont = shift;
	
	
	my ($target,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$VIEWSTATE,$EVENTARGUMENT,@appURLs);
	
	my @URLs;
	my $i = 2;
	nextPage:
	my $URLs = &collect_URLs($dtlCont);
	@URLs = @{$URLs};
	push(@appURLs,@URLs);
									
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATEGENERATOR\"\s*id\s*=\s*\"__VIEWSTATEGENERATOR\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$VIEWSTATEGENERATOR = uri_escape($1);
	}
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTVALIDATION\"\s*id\s*=\s*\"__EVENTVALIDATION\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$EVENTVALIDATION = uri_escape($1);
	}
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATE\"\s*id\s*=\s*\"__VIEWSTATE\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$VIEWSTATE = uri_escape($1);
	}
	if($dtlCont=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTARGUMENT\"\s*id\s*=\s*\"__EVENTARGUMENT\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
	{
		$EVENTARGUMENT = uri_escape($1);
	}
	if($dtlCont =~ m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')(Page\$\d+)?(\&\#39\;|\')\)\"\s*>Next[^<]*?\s*<\/a>/is)
	{	
		$target = uri_escape($2);
	}
	
	my ($Temp_From_Date,$Temp_To_Date);
	if($From_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
	{
		$Temp_From_Date = $1."-".$2."-".$3;
	}
	if($To_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
	{
		$Temp_To_Date = $1."-".$2."-".$3;
	}
	
	my $nextPostcont = "__EVENTTARGET=$target&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop=10";
	
	
	$mech->post( $SEARCH_URL, Content => "$nextPostcont");
	print "\ni-value:::::$i\n";
	
	my $nextPageStatus = $mech->status;
	my $nextPageCnt = $mech->content;
	$dtlCont=$nextPageCnt;
	
	# open(PP, ">449$i.html");
	# print PP "$nextPageCnt\n";
	# close(PP);

	# exit;
	
	$i++;
	
	if($dtlCont=~m/<a[^>]*?disabled=\"disabled\"\s*class=\"selectedpage\">\s*Next\s*<\/a>\s*<\/li>/is)
	{
		my $URLs = &collect_URLs($dtlCont);
		@URLs = @{$URLs};
		push(@appURLs,@URLs);
		
		return(\@appURLs)
	}
	else
	{
		goto nextPage;
	}
				
}
	
	
sub collect_URLs
{
	my $urlCont = shift;
		
	my @Detail_Links;
	while($urlCont=~m/$APPLICATION_REGEX/igs)
	{
		my $Application_url=$1;		
		my $Application_no=$2;	

		print "Application_no==>$Application_no\n";
		
		my $Application_Link;	
	
		if($Application_url!~m/^\s*$/is)
		{
			if($Application_url!~m/https?\:/is)
			{
				$Application_Link = $FILTER_URL.$Application_url;
			}	
			else
			{
				$Application_Link = $Application_url;
			}	
		}	
		
		$Application_Link=~s/amp\;//igs;
		
		push(@Detail_Links,$Application_Link);
		undef $Application_Link;
	}
	
	return(\@Detail_Links);
}

		

sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}