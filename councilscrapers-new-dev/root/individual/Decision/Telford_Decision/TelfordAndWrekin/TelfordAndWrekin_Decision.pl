use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
my $COUNCIL_NAME = 'Telford & Wrekin';
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"449\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


# my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://secure.telford.gov.uk/planningsearch/search.aspx";
$mech->get($searchURL);
my $content = $mech->content;
my $status = $mech->status;



# open(DC,">Content.html");
# print DC $content;
# close DC;

# $mech->add_header( "Accept" => 'text/plain, */*; q=0.01' );
# $mech->add_header( "Host" => 'www.gov.je' );
# $mech->add_header( "Content-Type" => 'application/json; charset=UTF-8' );
# $mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
# $mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
# $mech->add_header( "Origin" => 'https://www.gov.je' );
# $mech->add_header( "Referer" => "$searchURL" );	

my ($Temp_From_Date,$Temp_To_Date);
if($From_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_From_Date = $1."-".$2."-".$3;
}
if($To_Date=~m/(\d+)\/(\d+)\/(\d+)/is)
{
	$Temp_To_Date = $1."-".$2."-".$3;
}
print "Temp_From_Date::$Temp_From_Date\n";
print "Temp_To_Date::$Temp_To_Date\n";

my ($VIEWSTATE,$VIEWSTATEGENERATOR,$PREVIOUSPAGE,$EVENTVALIDATION,$EVENTTARGET,$EVENTARGUMENT);
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATE=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATEGENERATOR=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTVALIDATION=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTTARGET=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTARGUMENT=uri_escape($1);
}
if($content=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$PREVIOUSPAGE=uri_escape($1);
}
	
my $searchPostContent = "__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__PREVIOUSPAGE=$PREVIOUSPAGE&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24txtPlanningKeywords=&ctl00%24ContentPlaceHolder1%24dlPlanningParishs=0&ctl00%24ContentPlaceHolder1%24dlPlanningWard=0&ctl00%24ContentPlaceHolder1%24ddlPlanningapplicationtype=0&ctl00%24ContentPlaceHolder1%24DCdatefrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24DCdateto=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24txtDCAgent=&ctl00%24ContentPlaceHolder1%24txtDCApplicant=&ctl00%24ContentPlaceHolder1%24btnSearchPlanningDetails=Search";



		
$mech->post( 'https://secure.telford.gov.uk/planningsearch/default.aspx', Content => "$searchPostContent");
my $PostContent = $mech->content;
# open(DC,">PostContent.html");
# print DC $PostContent;
# close DC;
# exit;

if($PostContent=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATE=uri_escape($1);
}
if($PostContent=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$VIEWSTATEGENERATOR=uri_escape($1);
}
if($PostContent=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTVALIDATION=uri_escape($1);
}
if($PostContent=~m/<input\s*[^<]*?id=[\"\']__EVENTTARGET[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTTARGET=uri_escape($1);
}
if($PostContent=~m/<input\s*[^<]*?id=[\"\']__EVENTARGUMENT[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$EVENTARGUMENT=uri_escape($1);
}
if($PostContent=~m/<input\s*[^<]*?id=[\"\']__PREVIOUSPAGE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$PREVIOUSPAGE=uri_escape($1);
}

my $receivedPostContent = "__EVENTTARGET=ctl00%24ContentPlaceHolder1%24lbPlanning2ndLevel3&__EVENTARGUMENT=&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop=10";

$mech->post( 'https://secure.telford.gov.uk/planningsearch/default.aspx', Content => "$receivedPostContent");
my $receivedPostContent = $mech->content;
# $con=$receivedPostContent;
# open(DC,">receivedPostContent.html");
# print DC $receivedPostContent;
# close DC;
# exit;

my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

if($receivedPostContent=~m/<tr[^>]*?>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*<\/a>\s*<\/td>/is)
{
	my $Page_Number=1;
	NextPage:
	while($receivedPostContent=~m/<tr[^>]*?>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*<\/a>\s*<\/td>/gsi)
	{
		my $Application_URL = $1;
		my $Application_Num = $2;
		$Application_URL=~s/\&amp;/\&/gsi;
		
		print "Application_Num==>$Application_Num\n";
		
		$mech->get($Application_URL);
		my $App_Page_Content = $mech->content;		

		# open(DC,">App_content.html");
		# print DC $App_Page_Content;
		# close DC;
		# exit;
		
		my ($Application,$Proposal,$Decision_Status,$Application_Status,$Date_Decision_Made);
		
		$Application = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>Application\s*number<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Proposal = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>(?:Description\s*of)?\s*proposal<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([\w\W]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		
		if($App_Page_Content=~m/<ul\s*id=\"ApplicationStatuslist\">\s*([\w\W]*?)\s*<\/ul>/is)
		{
			my $status = $1;
			$Application_Status = &clean($1) if($status=~m/<li[^>]*?selected\">\s*([^<]*?)\s*<\/li>/is);
		}
		
		$Decision_Status = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Decision\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		$Date_Decision_Made = &clean($1) if($App_Page_Content=~m/<th[^>]*?>\s*<p>\s*Decision\s*date\s*<\/p>\s*<\/th>\s*<td>\s*<p>\s*<span>\s*([^<]*?)\s*<\/span>\s*<\/p>\s*<\/td>/is);
		
		
		
		my $time = Time::Piece->new;
		my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
		# my $Source = "GCS001";	
		my $Source = $Date_Range;	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_URL\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
		undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_URL;
	}
	
	if($receivedPostContent=~m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')(Page\$\d+)?(\&\#39\;|\')\)\"\s*>Next[^<]*?\s*<\/a>/is)
	{
		my ($target,$VIEWSTATEGENERATOR,$EVENTVALIDATION,$VIEWSTATE,$EVENTARGUMENT);
		if($receivedPostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATEGENERATOR\"\s*id\s*=\s*\"__VIEWSTATEGENERATOR\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$VIEWSTATEGENERATOR = uri_escape($1);
		}
		if($receivedPostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTVALIDATION\"\s*id\s*=\s*\"__EVENTVALIDATION\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$EVENTVALIDATION = uri_escape($1);
		}
		if($receivedPostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATE\"\s*id\s*=\s*\"__VIEWSTATE\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$VIEWSTATE = uri_escape($1);
		}
		if($receivedPostContent=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__EVENTARGUMENT\"\s*id\s*=\s*\"__EVENTARGUMENT\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
		{
			$EVENTARGUMENT = uri_escape($1);
		}
		if($receivedPostContent =~ m/<a[^>]*?href\s*=\s*\"javascript\:__doPostBack\((\&\#39\;|\')([^\"]*?)(\&\#39\;|\')\,(\&\#39\;|\')(Page\$\d+)?(\&\#39\;|\')\)\"\s*>Next[^<]*?\s*<\/a>/is)
		{	
			$target = uri_escape($2);
		}
		
		my $nextPagePostCont = "__EVENTTARGET=$target&__EVENTARGUMENT=$EVENTARGUMENT&__VIEWSTATE=$VIEWSTATE&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION&ctl00%24ContentPlaceHolder1%24KeyWords=&ctl00%24ContentPlaceHolder1%24PageNo=&ctl00%24ContentPlaceHolder1%24pagesize=&ctl00%24ContentPlaceHolder1%24totalrecordCount=&ctl00%24ContentPlaceHolder1%24ParishID=0&ctl00%24ContentPlaceHolder1%24WardID=0&ctl00%24ContentPlaceHolder1%24AppTypeID=0&ctl00%24ContentPlaceHolder1%24daterecFrom=$Temp_From_Date&ctl00%24ContentPlaceHolder1%24daterecTo=$Temp_To_Date&ctl00%24ContentPlaceHolder1%24Agent=&ctl00%24ContentPlaceHolder1%24Applicant=&ctl00%24ContentPlaceHolder1%24gvResults%24ctl01%24PageSizeDropDownTop=10&ctl00%24ContentPlaceHolder1%24gvResults%24ctl14%24PageSizeDropDownTop=10";
		
		$mech->post('https://secure.telford.gov.uk/planningsearch/default.aspx', Content => "$nextPagePostCont");
		my $nextPagePostCnt = $mech->content;
		$receivedPostContent=$nextPagePostCnt;
		
		# open(DC,">nextPagePostCnt.html");
		# print DC $nextPagePostCnt;
		# close DC;
		# exit;
		
	
		$Page_Number++;
		print "Page_Number: $Page_Number\n";
		
		
		goto NextPage;
	}
}


$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);




sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=Glenigan;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}