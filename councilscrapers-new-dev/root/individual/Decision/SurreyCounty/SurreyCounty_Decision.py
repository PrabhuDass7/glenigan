# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Merit/Projects/Individual_councils'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, driver):
	try:
		#-- Parse
		driver.get('https://planning.surreycc.gov.uk/planappsearch.aspx')
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		finalCnt=''
		if len(content) > 0:
			finalCnt = content.encode("utf-8")
		
		# print advfinalCnt

		
		fromDate = '//*[@id="ContentPlaceHolder1_txtDecFrom"]'
 
		toDate = '//*[@id="ContentPlaceHolder1_txtDecTo"]'
		
		driver.find_element_by_xpath(fromDate).send_keys(dumFrom_Date)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath(toDate).send_keys(dumTo_Date)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnSearch"]').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		# with open("190_out.html", 'wb') as fd:
			# fd.write(searchfinalCnt)
		# raw_input()
		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Application parse section	
def parsesection(searchcontent, CouncilCode, Source, conn, cursor, driver):
	try:
		searchcount = re.findall("Your\s*search\s*has\s*returned\s*(\d+)\s*application", str(searchcontent), re.IGNORECASE)  
		print "searchcount:", int(searchcount[0])
		totalcount = int(searchcount[0])
		print "Totalcount", totalcount
		
		pageNum = re.findall("Currently\s*showing\s*page\s*1\s*of\s*(\d+)", str(searchcontent), re.IGNORECASE)  
		print "pageNumCount:", pageNum
		
		

		a = 1
		insertQuery = 'insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values '
		
		bulkValuesForQuery=''
		
		if re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>'+str(pageNum)+'<\/a>', str(searchcontent)):
			check_next_page = re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>('+str(pageNum)+')<\/a>', str(searchcontent), re.I)
			print check_next_page
			print type(check_next_page)
			
			pageNumber = 1
			nextPageNumber = 0
			while (check_next_page):

				appCount = 1
				b = 2
				
				if pageNumber <= int(check_next_page[0]):
					appcollection=[]
					if pageNumber >= 2:	
						print("For next page click")		
						nextPageNumber = nextPageNumber + 1	
						print "CurrentPage::",nextPageNumber
						driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_grdResults"]/tbody/tr[42]/td/div/a['+str(nextPageNumber)+']').click()
						time.sleep(4)  
						searchcontent=driver.page_source		
						appcollection = re.findall("<td>[^<]*?<\/td>\s*<td>\s*<a\s*href=\"[^\"]*\"[^<]*>\s*([^<]*)\s*<\/a>\s*<\/td>", str(searchcontent), re.IGNORECASE)
					else:
						print("For first page click")
						appcollection = re.findall("<td>[^<]*?<\/td>\s*<td>\s*<a\s*href=\"[^\"]*\"[^<]*>\s*([^<]*)\s*<\/a>\s*<\/td>", str(searchcontent), re.IGNORECASE)

					
					
					# print appcollection
					# print type(appcollection)
					for appMatch in appcollection:
						app_num = appMatch
						print a ,"<==>", b,"<==>", appCount ,"<==>", totalcount
						print "Current Application Number is:", app_num
						
						driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_grdResults"]/tbody/tr['+str(b)+']/td[2]/a').click()	
						app_url=driver.current_url	
						#-- Wait funtion
						time.sleep(4)  
						appcontent=driver.page_source
						
						appfinalCnt=''
						if len(appcontent) > 0:
							appfinalCnt = appcontent.encode("utf-8")

						appDataCollection = appParseSection(appfinalCnt, app_num, app_url, CouncilCode, Source)
						print "appDataCollection:  ", appDataCollection

						# with open(folder_path+"/filename.html", 'wb') as fd:
						# 	fd.write(searchcontent)
						
						b += 4
						a += 1
						
						
						# driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnReturnToResults"]').click()
						driver.execute_script("window.history.go(-1)")
						# driver.back()
						
						#-- Wait funtion
						time.sleep(7)
						
						
						if appCount == 1:
							bulkValuesForQuery = insertQuery+appDataCollection
							appCount += 1
						elif appCount == 5:
							bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
							cursor.execute(bulkValuesForQuery)
							conn.commit()
							appCount=1
							bulkValuesForQuery=''
						else:
							bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
							appCount += 1
					
					if re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>'+str(pageNum)+'<\/a>', str(searchcontent)):
						check_next_page = re.findall(r'<a[^>]*?href=\"javascript\:__doPostBack[^>]*?>('+str(pageNum)+')<\/a>', str(searchcontent), re.I)
						print "pageNumber:",pageNum              					
						if check_next_page != "":
							pageNumber = pageNumber + 1
							continue
						else:
							break
					else:
						break
				else:
					break	
				
		else:
			appCount = 1
			b = 2
					
			appcollection = re.findall("<td>[^<]*?<\/td>\s*<td>\s*<a\s*href=\"[^\"]*\"[^<]*>\s*([^<]*)\s*<\/a>\s*<\/td>", str(searchcontent), re.IGNORECASE)
			print appcollection
			print type(appcollection)
			for appMatch in appcollection:
				app_num = appMatch
				print a ,"<==>", totalcount
				print "Current Application Number is:", app_num
				driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_grdResults"]/tbody/tr['+str(b)+']/td[2]/a').click()	
				app_url=driver.current_url	
				#-- Wait funtion
				time.sleep(8)  
				appcontent=driver.page_source
				
				appfinalCnt=''
				if len(appcontent) > 0:
					appfinalCnt = appcontent.encode("utf-8")

				appDataCollection = appParseSection(appfinalCnt, app_num, app_url, CouncilCode, Source)
				print "appDataCollection:  ", appDataCollection

				# with open(folder_path+"/filename.html", 'wb') as fd:
				# 	fd.write(appfinalCnt)
				
				b += 4
				a += 1
				
				driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnReturnToResults"]').click()
								
				#-- Wait funtion
				time.sleep(7)
				bulkValuesForQuery = insertQuery+joinValues
				cursor.execute(bulkValuesForQuery)
				conn.commit()
				bulkValuesForQuery=''
				
				
				# if appCount == 1:
					# bulkValuesForQuery = insertQuery+appDataCollection
					# appCount += 1
				# elif appCount == 5:
					# bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					# cursor.execute(bulkValuesForQuery)
					# conn.commit()
					# appCount=1
					# bulkValuesForQuery=''
				# else:
					# bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					# appCount += 1
			
			
			
		return bulkValuesForQuery
			
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application details parse section	
def appParseSection(Appcontent, AppNum, AppURL, CouncilCode, Source):
    try:
        Application_Number=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Our\s*reference\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Proposal=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Proposal\s*<\/label>\s*<[^>]*?>\s*([\w\W]*?)\s*<\/textarea>\s*', str(Appcontent), re.IGNORECASE)        
        Application_Status=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Status\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        Decision=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Decision\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
        DecisionDate=re.findall(r'<div[^>]*?>\s*<[^>]*?>\s*Decision\s*issued\s*date\s*<\/label>\s*<[^>]*?value\=\"\s*([^>]*?)\s*\"\s*[^>]*?>\s*<\/div>', str(Appcontent), re.IGNORECASE)
		
        
        (Application, ProposalCnt, ApplicationStatus, decision, dateDecision, Date_Decision_Dispatched) = ('','','','','','')


        if len(Application_Number) > 0:
			Application=clean(Application_Number[0])
        else:
			Application=AppNum


        if len(Proposal) > 0:
			ProposalCnt=clean(Proposal[0])
        else:
			ProposalCnt=''
        ProposalCnt = re.sub(r'\'', "\'\'", str(ProposalCnt))

        if len(Application_Status) > 0:
			ApplicationStatus=clean(Application_Status[0])
        else:
			ApplicationStatus=''


        if len(Decision) > 0:
			decision=clean(Decision[0])
        else:
			decision=''

        if len(DecisionDate) > 0:
			dateDecision=clean(DecisionDate[0])
        else:
			dateDecision=''

       

		
        format1 = "%Y/%m/%d %H:%M"	
        Schedule_Date = datetime.now().strftime(format1)
	
        Source_With_Time = Source+"_"+Schedule_Date+"-perl"
		
        Council_Name = "Surrey County Council";
		
		
        joinValues="("+"'"+str(Application)+"','"+str(ProposalCnt)+"','"+str(decision)+"','"+str(dateDecision)+"','"+str(Date_Decision_Dispatched)+"','"+str(CouncilCode)+"','"+str(AppURL)+"','"+Source_With_Time+"','"+str(ApplicationStatus)+"')"


        # print "ApplicationValues: ", joinValues
		
        return joinValues 

    except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:      
        clean = ''  
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
        return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	
	
	
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	
	results = inputsection(fromdate, todate, browser)
	# results = inputsection('01/01/2019', '10/04/2019', browser)

	finalinsertQuery = parsesection(results, councilcode, gcs, conn, cursor, browser)
	
	
	# final output query insertion
	if (finalinsertQuery != '') or finalinsertQuery is not None:
		cursor.execute(finalinsertQuery)
		conn.commit()
		
	
	browser.quit()