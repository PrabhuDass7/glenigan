use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $inFrom_Date = $ARGV[1];
my $inTo_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"486\" \"GCS001\"\)", 16, "Error message");
    exit();
}


my ($From_Date, $To_Date) = ($inFrom_Date, $inTo_Date);

my $dumFrom_Date = $From_Date;
my $dumTo_Date = $To_Date;

print "Council_Code: $Council_Code\n";
print "From_Date: $dumFrom_Date\n";
print "To_Date: $dumTo_Date\n";

my $time = Time::Piece->new;
my $currentDate = $time->strftime('%y%m%d');       # 140109

print "currentDate=>$currentDate\n";


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Cheshire_East/Cheshire_East_Decision.ini" );

my $COUNCIL_NAME 		= $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL 			= $Config->{$Council_Code}->{'HOME_URL'};
my $HOST 				= $Config->{$Council_Code}->{'HOST'};
my $FORM_NUMBER 		= $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FILTER_URL 			= $Config->{$Council_Code}->{'FILTER_URL'};
my $SEARCH_URL 			= $Config->{$Council_Code}->{'SEARCH_URL'};
my $SSL_VERIFICATION	= $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $FORM_START_DATE		= $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE		= $Config->{$Council_Code}->{'FORM_END_DATE'};
my $SRCH_BTN_NME		= $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL		= $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX	= $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $RADIO_BTN_NME		= $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL 		= $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $TOTAL_PAGES_REGEX 	= $Config->{$Council_Code}->{'TOTAL_PAGES_REGEX'};
my $TOTAL_APP_REGEX 	= $Config->{$Council_Code}->{'TOTAL_APP_REGEX'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";


#### UserAgent Declaration ####	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0, autoclose => 1);
}
else
{	
	$mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
    verify_hostname => 0, 
	autoclose => 1,
	}, autocheck => 0);
}	

### Proxy settings ###
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');


# Get search results using date ranges
Retry:
my ($responseContent, $pingStatus);
eval{
$responseContent = $mech->get($HOME_URL);

$mech->form_number($FORM_NUMBER); 
$mech->set_fields($RADIO_BTN_NME => $RADIO_BTN_VAL, $FORM_START_DATE => $From_Date, $FORM_END_DATE => $To_Date);
$mech->click( $SRCH_BTN_NME );
		
};

if ($@) {
	$pingStatus = $@;
};	

$responseContent= $mech->content;

my ($totalPages,$totalAppCount);
if($responseContent=~m/$TOTAL_PAGES_REGEX/is)
{
	$totalPages = $1;
	print "Total number of pages found: $totalPages\n";
}
else
{
	print "Next page not found\n";
}

if($responseContent=~m/$TOTAL_APP_REGEX/is)
{
	$totalAppCount = $1;
	print "Total number of Application found: $totalAppCount\n";
}
else
{
	print "No Application found\!\!\n";
}

# open(PP, ">app_cont.html");
# print PP "$responseContent\n";
# close(PP); 
# exit;


my $totalAppLinks = &SearchAppLink( $responseContent);

&Scrape_Details($Council_Code, $totalAppLinks);

sub SearchAppLink()
{
	my $appContent = shift;
	
	my $i = 1;			
	my $pageCount = 2;	
	my $startAt = 10;		
	my @DetailLinks;
	
	next_page:
	open(PP, ">>app_cont_M_${currentDate}.html");
	print PP "$appContent\n";
	close(PP); 
	while($appContent=~m/$APPLICATION_REGEX/isg)
	{
		my $ApplicationLink = $1;
		my $ApplicationNumber = $2;
		
		print "ApplicationNumber==>$ApplicationNumber\n";
		
		my $Application_Link;
		if($ApplicationLink!~m/^\s*$/is)
		{
			if($ApplicationLink!~m/https?\:/is)
			{
				$Application_Link = $FILTER_URL.$ApplicationLink;
			}	
			else
			{
				$Application_Link = $ApplicationLink;
			}	
		}	
		$Application_Link=~s/amp\;//igs;
		
		# print "$Application_Link\n";
		
		push(@DetailLinks,$Application_Link);
		undef $Application_Link;
		
		print "count ::::::::::::::::::::::::::::$i\n";
		
		$i++;
	}
	
	if($totalPages >= "2")
	{			
		my $nextPageURL;
		
		if($pageCount <= $totalPages)
		{
			print "$pageCount <=> $totalPages\n";
			if($appContent=~m/<li\s*>\s*<a\s*href\s*=\s*\'([^\']*?)\s*\'[^>]*?>\s*$pageCount\s*<\/a>\s*<\/li>/is)
			{
				$nextPageURL=$1;
			}
		}
		else
		{
			goto next_end;
		}
		
		my $postURL;
		if($nextPageURL!~m/^\s*$/is)
		{
			if($nextPageURL!~m/https?\:/is)
			{
				$postURL = $FILTER_URL.$nextPageURL;
			}	
			else
			{
				$postURL = $nextPageURL;
			}	
		}	
		$postURL=~s/amp\;//igs;		
		
		my $query = $1 if($nextPageURL=~m/\.aspx\?query=([^<]*?)\&start=/is);			
		my $Referer = "http://planning.cheshireeast.gov.uk/SearchResultsInfinite.aspx?query=".$query;
			
		$mech->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' );
		$mech->add_header( "Host" => "$HOST" );		
		$mech->add_header( "Content-Type" => 'application/json; charset=utf-8; application/x-www-form-urlencoded' );
		$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
		$mech->add_header( "Accept-Encoding" => 'gzip, deflate' );
		$mech->add_header( "Referer" => "$Referer" );
		
		
		my $postcont = "\{\"startAt\"\:$startAt\,\"pageSize\"\:10\}";
		$startAt = $startAt + 10;
					
		$mech->post( $postURL, Content => "$postcont");			
		
		sleep(60);

		my $content = $mech->content;
		$appContent=$content;
		
		$pageCount++;
	
		goto next_page;
	}
	# else
	# {
		# print "Hi welcome\n";
	# }
	
	next_end:
	
	return (\@DetailLinks);
}

sub Scrape_Details()
{
	my $Council_Code=shift;
	my $AppLink=shift;
	
	my @totalAppLinks = @{$AppLink};
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query="insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status, Imported_Date) values ";
	foreach my $appLink (@totalAppLinks)
	{
		$mech->get($appLink);
		sleep(10);
		my $appPageContent = $mech->content;
		
		my $Application					= &clean($1) if($appPageContent=~m/Reference\s*Number\:?<\/h3>\s*<[^>]*?>\s*([^>]*?)\s*<\/span>/is);
		my $Proposal					= &clean($1) if($appPageContent=~m/Proposal\:?<\/h3>\s*<[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<br\s*\/?>/is);	
		my $Application_Status			= &clean($1) if($appPageContent=~m/>\s*Status\:?<\/h3>\s*<[^>]*?>\s*([^>]*?)\s*<\/span>/is);
		my $Decision_Status				= &clean($1) if($appPageContent=~m/Decision\s*\/\s*Date\s*Decision\s*Made\:?\s*<\/h3>\s*<[^>]*?>\s*([^>]*?)\/\s*[^>]*?\s*</is);
		my $Decision_Made_Dt			= &clean($1) if($appPageContent=~m/Decision\s*\/\s*Date\s*Decision\s*Made\:?\s*<\/h3>\s*<[^>]*?>\s*[^>]*?\/\s*([^>]*?)\s*</is);
		
		my $Application_Link=$appLink;
		my $Decision_Dispatched_Dt;
	
		
		my $Source = 'GCS001';	
		
		print "Application==>$Application\n";
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Decision_Made_Dt\', \'$Decision_Dispatched_Dt\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\', \'$Schedule_Date\'), ";
		
		undef $Application; undef $Proposal;  undef $Decision_Status; undef $Decision_Made_Dt; undef $Decision_Dispatched_Dt; undef $Application_Status; undef $Application_Link;
		
	}
	
	$insert_query=~s/\,\s*$//igs;
		
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
}


sub clean()
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s*\.?\(([^>]*?)\)\s*$/$1/si;		
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}