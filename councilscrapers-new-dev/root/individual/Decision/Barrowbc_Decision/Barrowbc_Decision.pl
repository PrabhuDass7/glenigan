#!/usr/bin/perl
use strict;
use warnings;
use WWW::Mechanize;
use URI::URL;
use URI::Escape;
use IO::Socket::SSL;
use Net::Ping;
use HTML::Entities;
use HTTP::CookieMonster qw( cookies );
use Glenigan_DB_Windows;
use Time::Piece;
use DBI;
use DBD::ODBC;
use Win32; 


my $Council_Code = $ARGV[0];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\'  \(Eg: \"103\" \)", 16, "Error message");
    exit();
}

BEGIN 
{	
my $target = '172.27.137.192';
my $ping_obj = Net::Ping->new('tcp');

$ping_obj->port_number('3128');

if ($ping_obj->ping($target)) {
print "Yes, I can ping $target\n";
$ENV{HTTPS_PROXY} = 'http://172.27.137.192:3128';
$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
} else {
print "No, I cannot ping $target\n";
}

$ping_obj->close();
}


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();



my $mech = WWW::Mechanize->new( 
ssl_opts => {
SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
verify_hostname => 0, 
}
, autocheck => 0
);

my $url = 'https://webapps.barrowbc.gov.uk/webapps/f?p=BARROWPLANNINGHUB:WEEKLYLIST:983607072542:';

$mech->proxy(['http','https'], 'http://172.27.137.192:3128');

$mech->get($url);



my $monster = HTTP::CookieMonster->new( $mech->cookie_jar );
my @all_cookies = $monster->all_cookies;

my ($sessionid,$sessionval,$sessionsec,$sessionexp,$cookiesession)=("","","","","");
foreach my $cookie ( @all_cookies ) {
	$sessionid=$cookie->key;
	$sessionval=$cookie->val;
	$sessionsec=$cookie->secure;
	$sessionexp=$cookie->expires;
	print "Session: $sessionid::$sessionval\n";
	$cookiesession=$sessionid."=".$sessionval;
}

my $responseCode = $mech->status;
# print "responseCode==>$responseCode\n";
my $contents = $mech->content;
my $currentURL = $mech->uri();
# print "currentURL==>$currentURL\n";

# open(PP, ">id.html");
# print PP "$contents";
# close(PP);
# exit;

if($contents=~m/<table[^>]*?summary=\"Weekly\s*List\">\s*([\w\W]*?)\s*<\/table>/is)
{
	my $weeklyCnt = $1;
	while($weeklyCnt=~m/<td[^>]*?\"WeekCommencing\">\s*[\w\W]*?\s*<\/td>\s*<td[^>]*?headers=\"Decided\">\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>/igs)
	{
		my $dateURLCnt=$1;
		$dateURLCnt=decode_entities($dateURLCnt);
				
		$dateURLCnt=~s/\\u00252F/\%2F/gsi;
		$dateURLCnt=~s/\\u0026/\&/gsi;
		
		my $dateURL = $1 if($dateURLCnt=~m/javascript\:apex\.navigation\.dialog\(\'([^\']*?)\'/is);
		
		$dateURL = URI::URL->new($dateURL)->abs( $url, 1 );
		
		# print "Date==>$date\n";
		print "dateURL==>$dateURL\n";
		
		$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0');
		$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
		$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
		$mech->add_header( 'Upgrade-Insecure-Requests' => '1');
		$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
		$mech->add_header( 'Host' => 'webapps.barrowbc.gov.uk');
		$mech->add_header( 'Connection' => 'keep-alive');
		$mech->add_header( 'Referer' => $currentURL);
		$mech->add_header( 'Cookie' => $cookiesession);
				
		$mech->get($dateURL);
		my $responseCode = $mech->status;
		print "responseCode==>$responseCode\n";	
		my $content = $mech->content;
		
		# open(PP, ">id.html");
		# print PP "$content";
		# close(PP);
		# exit;
		
		if($content=~m/<table[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
		{
			my $appCnt = $1;
			while($appCnt=~m/<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*<div[^>]*?>\s*<span[^>]*?>\s*View\s*<\/span>[\w\W]*?<td[^>]*?>\s*([^<]*?)\s*<\/td>/gsi)
			{
				my $appUrlCnt=$1;
				my $appNum=$2;
				$appUrlCnt=decode_entities($appUrlCnt);
				$appNum=decode_entities($appNum);
				
				$appUrlCnt=~s/\\u00252F/\%2F/gsi;
				$appUrlCnt=~s/\\u0026/\&/gsi;
				
				my $appURL = $1 if($appUrlCnt=~m/javascript\:apex\.navigation\.dialog\(\'([^\']*?)\'/is);
				
				$appURL = URI::URL->new($appURL)->abs( $url, 1 );
				
				print "appNum==>$appNum\n";
				print "appURL==>$appURL\n";
				
				$mech->get($appURL);
				my $appURLCode = $mech->status;
				print "appURLCode==>$appURLCode\n";	
				my $appContent = $mech->content;
				
				# open(PP, ">id.html");
				# print PP "$appContent";
				# close(PP);
				# exit;
				
				
				my $time = Time::Piece->new;
				my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
				
				
				print "Council_Code: $Council_Code\n";
				
				my ($Application_Status, $Decision_Status, $Date_Decision_Made, $Application_Link, $Application, $Address, $Proposal);
				
				$Application				= $appNum;
				$Address					= &clean($1) if($appContent=~m/<span\s*id=\"[^\"]*?_LOCATION_DISPLAY\"[^>]*?>\s*([^<]*?)\s*<\/span>/is);	
				$Proposal					= &clean($1) if($appContent=~m/<span\s*id=\"[^\"]*?_PROPOSAL_DISPLAY\"[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);	
				$Date_Decision_Made			= &clean($1) if($appContent=~m/<span\s*id=\"[^\"]*?_DECISION_DATE_DISPLAY\"[^>]*?>\s*([^<]*?)\s*<\/span>/is);	
				$Decision_Status			= &clean($1) if($appContent=~m/<span\s*id=\"[^\"]*?_DECISION_DISPLAY\"[^>]*?>\s*([^<]*?)\s*<\/span>/is);	
				
				$Application_Status='';
				$Application_Link=$appURL;
			
				
				$Address=~s/\'/\'\'/gsi;	
				my $Source = 'GCS001';	
				my $COUNCIL_NAME = 'Barrow In Furness';	
				
				
				my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
				
				my $insert_query = "insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values (\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\')";
				
				undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 
				
				print "insert_query::$insert_query\n";

				if($insert_query!~m/values\s*$/is)
				{
					&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
				}
				
			}
		}
	}
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	if($Data=~m/\&\#/is)
	{
		$Data=decode_entities($Data);
	}
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}