use strict;
use DBI;
use DBD::ODBC;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Text::CSV;
use XML::CSV;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;


# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
# my $COUNCIL_NAME = "Eastleigh";
my $input_from_date = $ARGV[1];
my $input_to_date = $ARGV[2];

# my $Status_query="insert into TBL_SCRAP_STATUS (SCHEDULE_NO, SCHEDULE_DATE, GCS_NAME, COUNCIL_CODE, STATUS, PLANNING_OR_DECISION, COUNCIL_NAME) values (\'$Schedule_no\',GETDATE(),\'\',\'$Council_Code\',\'Running\',\'Decision\',\'$COUNCIL_NAME\')";

# &DB_Insert($dbh,$Status_query);

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}

############
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");

#### COOKIE SETTING
my $cookie_jar=HTTP::Cookies->new(file=>$0."_Cookie.txt",autosave=>1);                          
$ua->cookie_jar($cookie_jar);

my $csv_filename="Eastleigh_Decision_Output.csv";
my $xml_filename="Eastleigh_Decision_Output.xml";

my $Applicant_Flag = 'Y';
my $ID = 1;


#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();

open my $fh, ">", "$csv_filename" or die "Failed to open file: $!";
$csv->print($fh,["ID","Application","ProPosal","Decision_Status","Date_Decision_Made","Date_Decision_Dispatched","Council_code","Page_URL","Application_Status"]);
close $fh;

my $home_url = "https://planning.eastleigh.gov.uk/s/";
$driver->get("$home_url");
sleep(10);
my $model_cont=$driver->get_page_source($driver);


# open FH, ">Eastleigh.html";
# print FH $model_cont;
# close FH;
# exit;

$driver->find_element_by_xpath("//*[\@id=\"NapiliCommunityTemplate\"]/div[4]/div/div[2]/div[1]/div/div/div/div[1]/p[1]/strong/a")->click;
sleep(10);	

$driver->find_element_by_xpath("//*[\@id=\"352:0__item\"]")->click;
sleep(10);	

# $driver->find_element("//*[\@id=\"768:0\"]")->send_keys("$input_from_date");
$driver->find_element("//*[\@id=\"782:0\"]")->send_keys("$input_from_date");
sleep(10);
# $driver->find_element("//*[\@id=\"784:0\"]")->send_keys("$input_to_date");
$driver->find_element("//*[\@id=\"798:0\"]")->send_keys("$input_to_date");
sleep(10);	

my $contents;
# $driver->find_element_by_xpath("//*[\@id=\"352:0\"]/div/div/div/form/div/div/div[2]/button")->click;
$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
# $driver->find_element("//*[\@id=\"352:0\"]/div/div/div/form/div/div/div[2]/button")->send_keys('submit');
sleep(25);	
my $model_content=$driver->get_page_source($driver);

# open FH, ">Eastleigh.html";
# print FH $model_content;
# close FH;
# exit;

$contents = $model_content;

# if($model_content eq "")
# {
	# $driver->find_element_by_xpath("//*[\@id=\"339:0\"]/div/div[2]/div/div[2]/a")->click;
	# sleep(10);	
	# my $model_contents=$driver->get_page_source($driver);
	
	# if($model_contents ne "")
	# {
		# $contents =$model_contents;
	# }
# }
# else
# {
	# $contents = $model_content;
# }

# open FH, ">Home.html";
# print FH $model_content;
# close FH;
# exit;

my $total_count = $1 if($contents=~m/Planning\s+Applications\s+\((\d+)\)<\/h2>/si);
my $click_count = $total_count + 1;
my $a=1;
while($contents=~m/<h4[^>]*?>\s*<a[^>]*?>([^>]*?)\s+\-\s*([^<]*?)<\/a>\s*<\/h4>/gsi)
{
	my $Application = $1;
	
	
	
	print "${a}::::$total_count\n";
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	print "\n\nApplication Number is Processing::$Application\n\n";
	
	if($a<=$total_count)
	{
		if($a >= 6)
		{
			$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$click_count]/a")->click;
		}
		
		$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$a]/div/h4/a")->click;
		sleep(10);	
		my $content=$driver->get_page_source($driver);
		
		my $current_URL = $driver->get_current_url();
		print "current_URL::$current_URL\n";
		
				
		# open FH, ">Output-$a.html";
		# print FH $content;
		# close FH;
		# exit;
		
		
		my $Application_Number = &clean($1) if($content=~m/<label[^>]*?>\s*Planning\s*Application\s*Number\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $ProPosal = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal1 = &clean($1) if($content=~m/<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal2 = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
		my $Application_Status = &clean($1) if($content=~m/<label[^>]*?>\s*Application\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Decision_Status = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Decision_Status1 = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Type\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Decision_Made = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		# my $Date_Decision_Dispatched = &clean($1) if($content=~m/<label[^>]*?>\s*Date\s*Received\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Decision_Dispatched = "";
			
		if($Application_Number=~m/^\s*$/is)
		{
			if($Application!~m/^\s*$/is)
			{
				$Application_Number = $Application;
			}
		}
				
		if($ProPosal=~m/^\s*$/is)
		{
			if($Proposal1!~m/^\s*$/is)
			{
				$ProPosal = $Proposal1;
			}
			elsif($Proposal2!~m/^\s*$/is)
			{
				$ProPosal = $Proposal2;
			}
		}
		if($Decision_Status=~m/^\s*$/is)
		{
			if($Decision_Status1!~m/^\s*$/is)
			{
				$Decision_Status = $Decision_Status1;
			}
		}
		
		# print "Application_Number::$Application_Number\n";
		# print "ProPosal::$ProPosal\n";
		# print "Application_Status::$Application_Status\n";
		# print "Decision_Status::$Decision_Status\n";
		# print "Date_Decision_Made::$Date_Decision_Made\n";
		
		# my $Council_Code = "190";
		# my $COUNCIL_NAME = "Eastleigh";
		my $Page_URL = $current_URL;
		
		open my $fh, ">>", "$csv_filename" or die "Failed to open file: $!";
		$csv->print($fh,["$ID","$Application_Number","$ProPosal","$Decision_Status","$Date_Decision_Made","$Date_Decision_Dispatched","$Council_Code","$Page_URL","$Application_Status"]);
		close $fh;	
		
		my $Source = "GCS001";	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
		my $insert_query="insert into Import_Non_Public_Planning_Decision_Automation_Staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values (\'$Application_Number\', \'$ProPosal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Date_Decision_Dispatched\', \'$Council_Code\', \'$Page_URL\', \'$Source_With_Time\', \'$Application_Status\')";
		
		undef $Application_Number;  undef $ProPosal;  undef $Decision_Status;  undef $Date_Decision_Made;  undef $Date_Decision_Dispatched;  undef $Page_URL; undef $Application_Status;
		
		
		print "insert_query::$insert_query\n";

		if($insert_query!~m/values\s*$/is)
		{
			&DB_Insert($dbh,$insert_query);
		}
		
		$driver->get("https://planning.eastleigh.gov.uk/s/public-register");
		sleep(15);
		
		eval{$driver->find_element_by_xpath("//*[\@id=\"352:0__item\"]")->click;};
		sleep(10);	
		
		my $model_content=$driver->get_page_source($driver);

		if($model_content eq "")
		{
			$driver->find_element_by_xpath("//*[\@id=\"346:0__item\"]")->click;
			sleep(10);
			
			# $driver->find_element("//*[\@id=\"768:0\"]")->send_keys("$input_from_date");
			$driver->find_element("//*[\@id=\"782:0\"]")->send_keys("$input_from_date");
			sleep(10);
			# $driver->find_element("//*[\@id=\"784:0\"]")->send_keys("$input_to_date");
			$driver->find_element("//*[\@id=\"798:0\"]")->send_keys("$input_to_date");
			sleep(10);	

			# $driver->find_element_by_xpath("//*[\@id=\"352:0\"]/div/div/div/form/div/div/div[2]/button")->click;
			$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
			sleep(10);
			
		}
		else
		{
			# $driver->find_element("//*[\@id=\"768:0\"]")->send_keys("$input_from_date");
			$driver->find_element("//*[\@id=\"782:0\"]")->send_keys("$input_from_date");
			sleep(10);
			# $driver->find_element("//*[\@id=\"784:0\"]")->send_keys("$input_to_date");
			$driver->find_element("//*[\@id=\"798:0\"]")->send_keys("$input_to_date");
			sleep(10);	

			# $driver->find_element_by_xpath("//*[\@id=\"352:0\"]/div/div/div/form/div/div/div[2]/button")->click;
			$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
			sleep(10);
		}
		
		

			
		
		$a++;
		$ID++;
	}
	else
	{
		print "\nEastleigh Completed\n\n";
	}
}

my $csv_obj = XML::CSV->new();
$csv_obj->parse_doc("$csv_filename", {headings => 1});
$csv_obj->declare_xml({version => '1.0', encoding => 'Windows-1252'}); 
$csv_obj->print_xml("$xml_filename",
		{file_tag    => 'Items',
		parent_tag  => 'Item'});

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;
	return($values);
}
