use strict;
use DBI;
use DBD::ODBC;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;



# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $input_from_date = $ARGV[1];
my $input_to_date = $ARGV[2];
my $COUNCIL_NAME = "Eastleigh";


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


############
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");

#### COOKIE SETTING
my $cookie_jar=HTTP::Cookies->new(file=>$0."_Cookie.txt",autosave=>1);                          
$ua->cookie_jar($cookie_jar);


my $Applicant_Flag = 'Y';


#### 
my $driver = Selenium::Remote::Driver->new();
# my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
# $driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();


my $home_url = "https://planning.eastleigh.gov.uk/s/";
$driver->get("$home_url");
sleep(10);
my $model_cont=$driver->get_page_source($driver);

$driver->find_element_by_xpath("//*[\@id=\"NapiliCommunityTemplate\"]/div[2]/div/div[2]/div[1]/div/div/div/div[1]/p[1]/strong/a")->click;
sleep(10);	
my $temp1=$driver->get_page_source($driver);

my $advSearch = $1 if($temp1=~m/<a[^>]*?id=\"([^\"]*?)\"[^>]*?>\s*Advanced\s*Search\s*<\/a>\s*<\/li>/is);

$driver->find_element_by_xpath("//*[\@id=\"$advSearch\"]")->click;
sleep(5);	
my $temp2=$driver->get_page_source($driver);

my $fromDate = $1 if($temp2=~m/<span[^>]*?>\s*Decision\s*Date\s*From\s*<\/span>\s*<\!\-\-[^>]*?\-\->\s*<\!\-\-[^>]*?\-\->\s*<\/label>\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>/is);
my $toDate = $1 if($temp2=~m/<span[^>]*?>\s*Decision\s*Date\s*To\s*<\/span>\s*<\!\-\-[^>]*?\-\->\s*<\!\-\-[^>]*?\-\->\s*<\/label>\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>/is);

$driver->find_element("//*[\@id=\"$fromDate\"]")->send_keys("$input_from_date");
sleep(5);

$driver->find_element("//*[\@id=\"$toDate\"]")->send_keys("$input_to_date");
sleep(5);	

$driver->find_element_by_xpath("//*[\@id=\"expando-unique-id\"]/div/div/div/form/div/div/div[2]/button")->click;
sleep(10);	

my $model_content=$driver->get_page_source($driver);

# open FH, ">Home.html";
# print FH $model_content;
# close FH;
# exit;

# Create insert query for required columns
my $insert_query='insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values ';

my $total_count = $1 if($model_content=~m/Planning\s+Applications\s+\((\d+)\)<\/h2>/si);
my $click_count = $total_count + 1;
my $a=1;
while($model_content=~m/<h4[^>]*?>\s*<a[^>]*?>([^>]*?)\s+\-\s*([^<]*?)<\/a>\s*<\/h4>/gsi)
{
	my $Application = $1;
	print "${a}::::$total_count\n";
	
	print "\n\nApplication Number is Processing::$Application\n\n";
	
	if($a<=$total_count)
	{		
		if($a==6)
		{			
			$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$click_count]/a")->click;
			sleep(5);				
		}
		
		$driver->find_element_by_xpath("//*[\@id=\"arcusbuilt__PApplication__c\"]/div[$a]/div/h4/a")->click;
		sleep(10);	
		
		my $current_URL = $driver->get_current_url();
		# print "$current_URL\n";
		
		my $content=$driver->get_page_source($driver);
				
		# open FH, ">Output-$a.html";
		# print FH $content;
		# close FH;
		# exit;
		
		my $time = Time::Piece->new;
		my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
		
		
		
		
		my $Application_Number = &clean($1) if($content=~m/<label[^>]*?>\s*Planning\s*Application\s*Number\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $ProPosal = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal1 = &clean($1) if($content=~m/<label[^>]*?>\s*Description\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Proposal2 = &clean($1) if($content=~m/<label[^>]*?>\s*Proposal\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>/is);
		my $Application_Status = &clean($1) if($content=~m/<label[^>]*?>\s*Application\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Decision_Status = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Status\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Decision_Status1 = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Type\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Decision_Made = &clean($1) if($content=~m/<label[^>]*?>\s*Decision\s*Date\s*<\/label>\s*(?:<div[^>]*?>)?\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>/is);
		my $Date_Decision_Dispatched = "";
			
		if($Application_Number=~m/^\s*$/is)
		{
			if($Application!~m/^\s*$/is)
			{
				$Application_Number = $Application;
			}
		}
				
		if($ProPosal=~m/^\s*$/is)
		{
			if($Proposal1!~m/^\s*$/is)
			{
				$ProPosal = $Proposal1;
			}
			elsif($Proposal2!~m/^\s*$/is)
			{
				$ProPosal = $Proposal2;
			}
		}
		if($Decision_Status=~m/^\s*$/is)
		{
			if($Decision_Status1!~m/^\s*$/is)
			{
				$Decision_Status = $Decision_Status1;
			}
		}
		
		my $Page_URL = $current_URL;
		
			
		my $Council_Code = "190";
		my $COUNCIL_NAME = "Eastleigh";	
		my $Source = "GCS001";	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
		# Create insert query for required columns
		$insert_query.="(\'$Application_Number\', \'$ProPosal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Date_Decision_Dispatched\', \'$Council_Code\', \'$Page_URL\', \'$Source_With_Time\', \'$Application_Status\'),";
		
		undef $Application_Number;  undef $ProPosal;  undef $Decision_Status;  undef $Date_Decision_Made;  undef $Date_Decision_Dispatched;  undef $Page_URL; undef $Application_Status;
		
		
		
		$a++;
		
		$driver->find_element_by_xpath("//*[\@id=\"NapiliCommunityTemplate\"]/div[2]/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[2]/div/button[1]")->click;
		sleep(7);	
		
	}
	else
	{
		print "\nEastleigh Completed\n\n";
	}
}
$insert_query=~s/\,$//igs;

print "insert_query::$insert_query\n";

if($insert_query!~m/values\s*$/is)
{
	&DB_Insert($dbh,$insert_query);
}


sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}