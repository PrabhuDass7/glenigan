use WWW::Mechanize;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use URI::Escape;
use DBI;
use DBD::ODBC;
use FileHandle;
use File::Copy;
use Time::Piece;


# Establish connection with DB server
my $dbh = &DbConnection();

# User Agent
my $mech = WWW::Mechanize->new(autocheck => 0);

# Location
my $pgm_path='C:/Glenigan/Live_Schedule/Decision/NorthLincolnshire/Program';
$pgm_path=~s/\s+$//igs;
my $archive_path=$pgm_path;
$archive_path=~s/Program$/Archive/igs;

print "Archive Path :: $archive_path \n ";


opendir(DIR, $archive_path) or die $!;
my @folder_array;
while (my $file = readdir(DIR))
{
	next if ($file=~m/^\./);
	$file=~s/\.pdf\s*$//igs;
	push(@folder_array,$file);
	# print "$file\n";
}
print "folder_array :: @folder_array \n";
closedir(DIR);

my $URL = 'http://www.northlincs.gov.uk/planning-and-environment/planning/planning-weekly-lists/';
$mech->get($URL);
my $list_content = $mech->content;

my $t = Time::Piece->new();
my $currentYear = $t->year;
print "currentYear=>$currentYear\n";


# open SR, ">Northlincs.html";
# print SR $list_content;
# close SR;
# exit;
	
if($list_content=~m/<a[^>]*?title=\"Planning\s*Application\s*Withdrawals\"\s*href=\"([^\"]*?)\"[^>]*?>\s*Planning\s*Application\s*Withdrawals\s*<\/a>/is)
{
	my $pdfURL=$1;
	if($pdfURL!~m/^http/is)
	{
		$pdfURL="http://www.northlincs.gov.uk".$pdfURL;
	}
	$mech->get($pdfURL);
	my $pdfList_content = $mech->content;
	
	my $ScrollPos=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"ScrollPos\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $LinkState=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"LinkState\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $EVENTTARGET=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__EVENTTARGET\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $EVENTARGUMENT=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__EVENTARGUMENT\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $VIEWSTATEZIP=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__VIEWSTATEZIP\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $LASTFOCUS=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__LASTFOCUS\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $VIEWSTATE=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__VIEWSTATE\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $EVENTVALIDATION=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__EVENTVALIDATION\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $VIEWSTATEGENERATOR=uri_escape($1) if($pdfList_content=~m/<input[^>]*?name=\"__VIEWSTATEGENERATOR\"[^>]*?value=\"([^\"]*?)\"\s*\/>/is);
	my $Records=uri_escape($1) if($pdfList_content=~m/<select[^>]*?name=\"([^\"]*?)\"[^>]*?class=\"inline\">/is);
	my $totalRecords=$2 if($pdfList_content=~m/<select[^>]*?name=\"([^\"]*?)\"[^>]*?class=\"inline\">[\w\W]*?(\d+)\s*<\/option>\s*<\/select>/is);
	
	my $post_con = "EasySitePostBack=&ScrollPos=1092&LinkState=$LinkState&__EVENTTARGET=$Records&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATEZIP=$VIEWSTATEZIP&__VIEWSTATE=$VIEWSTATE&PageNotifications%24hfState=&$Records=$totalRecords&__VIEWSTATEGENERATOR=$VIEWSTATEGENERATOR&__EVENTVALIDATION=$EVENTVALIDATION";
	my $SEARCH_URL = "http://www.northlincs.gov.uk/planning-and-environment/planning/planning-weekly-lists/?albumid=155";
	
	$mech->post( $SEARCH_URL, Content => "$post_con");
	my $Code= $mech->status;
	my $full_content = $mech->content;
	
	# open SR, ">Northlincs_PDF.html";
	# print SR $full_content;
	# close SR;
	# exit;
	
	my $file_occur_flag=0;
	while($full_content=~m/<\/div>\s*<span[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?title=\"[^\"]*?\"[^>]*?>\s*([^<]*?)\s*<\/a>\s*<\/span>/igs)
	{
		my $pdf_url=$1;
		my $file_name=$2;
		# print "pdfURL=>$pdf_url\n";
		# print "file_name=>$file_name\n";
		
		$file_name=~s/\(pdf[^<]*?\)$//si;
		$file_name=~s/(\d+)$/$1.pdf/si;
		
		$file_name=~s/\s+//igs;
		my $file_exist=0;
		unless($pdf_url =~ m/^\s*http/is)
		{
			$pdf_url = 'http://www.northlincs.gov.uk'.$pdf_url;
		}
		my @Match_file = grep(/$file_name/, @folder_array);
		if($Match_file[0] ne "")
		{
			my $file_exist=1;
			print "file_exist :: $file_exist \n";
			next;
		}
		else
		{
			$mech->get($pdf_url);
			my $pdf_content = $mech->content;			
			$file_occur_flag=1;
		
			my $temp_file = $pgm_path.'\\'.$file_name;
			$file_name=$pgm_path.'\\'.$file_name;
			
			my $fh = FileHandle->new("$temp_file", 'w') or die "Cannot open $file_name for write :$!";
			binmode($fh);
			$fh->print($pdf_content);
			$fh->close();
			
			my $copy_res=copy("$temp_file",$archive_path);
			print "Download URL :: $file_name \n";#<STDIN>;
			
			
			my $exe_name="C:/Glenigan/Live_Schedule/Decision/NorthLincolnshire/pdftotext.exe -raw";
			system("$exe_name $temp_file");
			
			my $html_file_pdf=$file_name;
			$html_file_pdf=~s/\.pdf\s*$/\.txt/igs;
			
			print "html_file_pdf :: $html_file_pdf\n";
			
			
			undef $/;
			open KV, "$html_file_pdf";
			my $txt_cnt=<KV>;
			close KV;
			
			
			# open FH, ">$html_file_pdf";
			# print FH $txt_cnt;
			# close FH;
			# exit;
			
			if($txt_cnt=~m/Page\s+\d+\s+of\s+\d+/s)
			{
				$txt_cnt=~s/^[\w\W]*?\n+1\s+/1 /is;
				$txt_cnt=~s/\n+[^\n]*?Page\s+\d+\s+of\s+\d+//gsi;
				$txt_cnt=~s///gsi;
				$txt_cnt=~s/(Decision\:\s*([^<]*?)\n+)/$1<END>/gsi;
				$txt_cnt=~s/(Withdrawn\s*Date\:\s*([^<]*?)\n+)/$1<END>/gsi;
				$txt_cnt=~s/(Date\s*Withdrawn\:\s*([^<]*?)\n+)/$1<END>/gsi;
			}
			else
			{
				# print "Welcome Pari";
				$txt_cnt=~s/^[\w\W]*?\n+([A-Z]{2}\/[^\n]*?\n)/$1/is;
				$txt_cnt=~s/\d+\s*/\n/is;
				$txt_cnt=~s/(Grid\s*Reference\:\s*([^<]*?)\n+)/$1<END>/gsi;
			}
			
			open FH, ">$html_file_pdf";
			print FH $txt_cnt;
			close FH;
			
			# exit;			
			
						
			&Scrape_Details($txt_cnt,"346",$pdf_url);
			
		}
	}
	if($file_occur_flag != 1)
	{
		print "No new file occur \n";
	}
}

sub Scrape_Details()
{
	my $txtCnt=shift;
	my $Council_Code=shift;	
	my $page_url=shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';	
	
	my ($Application,$Decision_Status,$ProPosal,$Date_Decision_Made,$Application_Link,$Application_Status);
	while($txtCnt=~m/(([A-Z]{2}\/[^\n]*?)\s+[\w\W]*?)<END>/igs)
	{
		my $app_value=$1;
		$Application=$2;
		# print "app_value::$app_value\n";<STDIN>;
		
	
		if($app_value=~m/\s*[A-Z]{2}\/[^\n]*?\s+([^<]*?)\s*Location\s*of\s*Application\:/is)
		{
			$ProPosal=&clean($1);
		}
		if($ProPosal eq "")
		{
			if($app_value=~m/\s*[A-Z]{2}\/[^\n]*?\s+([^<]*?)\s*\d+\s*Location\s*of\s*development\:/is)
			{
				$ProPosal=&clean($1);
			}			
		}
		if($app_value=~m/Location\s*of\s*Application\:\s*([^<]*?)\s*Decision\s*Issued\:/is)
		{
			$Location=&clean($1);
		}
		if($app_value=~m/Withdrawn\s*Date\:\s*([^<]*?)\n/is)
		{
			$Date_Decision_Made=&clean($1);
		}
		elsif($app_value=~m/Date\s*Withdrawn\:\s*([^<]*?)\n/is)
		{
			$Date_Decision_Made=&clean($1);
		}
		elsif($app_value=~m/(\d{2}\/\d{2}\/\d{4})\n[^\n]*?\s*Ward/is)
		{
			$Date_Decision_Made=&clean($1);
		}
		
		$Decision_Status="Withdrawn";
		
		
		$Location			=~s/\'/\'\'/igs;
		$Proposal			=~s/\'/\'\'/igs;	
		$page_url			=~s/\'/\'\'/igs;
		
		
		my $Source = "GCS001";	
		my $COUNCIL_NAME = "North Lincolnshire Council";	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";	
	
		
		$Application_Link=$page_url;
		$insert_query.="(\'$Application\', \'$ProPosal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
		
		# print "insert_query::$insert_query\n"; <STDIN>;
	
		undef $Application; undef $ProPosal ;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 		
	}
	$insert_query=~s/\,$//igs;
    print "insert_query::$insert_query\n";
	
	if($insert_query!~m/values\s*$/is)
	{
		&DB_Insert($dbh,$insert_query);
	}	
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\n+/ /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


sub ConvertPdftoWord()
{
	$outfile = shift;
	my $res;
	
	
	my $outfile_html = $outfile;
	$outfile_html =~ s/\.(docx?|pdf)\s*$/.html/igs;
			
	print "\$outfile_html = $outfile_html\n";
	my $word;
	eval {$word = Win32::OLE->GetActiveObject('Word.Application')};
	die "Word not installed" if $@;
	unless (defined $word) {
	$word = Win32::OLE->new('Word.Application', sub { $_[0]->Quit; })
		or die "Cannot start Word";
	}
	$word->{Visible} = 0 ;
	$word->{DisplayAlerts} = 0 ;
   
	Win32::OLE->Option(Warn => 3);
	my $doc = $word->{'Documents'}->Open("$outfile");
	# Save in .doc and .html formats
	$doc->SaveAs( { Filename => $outfile_html, FileFormat => 10 } );
	$doc->Close();
	undef $doc;
	undef $word;
}


###### DB Connection ####
sub DbConnection()
{
    my $dsn                          =  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
    my $dbh                          =    DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
   
    if(!$dbh)
    {
        &DBIconnect($dsn);
    }
    else
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}