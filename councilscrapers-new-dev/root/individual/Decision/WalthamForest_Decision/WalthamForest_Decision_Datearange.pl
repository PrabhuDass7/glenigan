use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use JSON;
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $inFrom_Date = $ARGV[1];
my $inTo_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"16\" \"GCS001\"\)", 16, "Error message");
    exit();
}

# chomp($Date_Range);

# my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);
my ($From_Date, $To_Date) = ($inFrom_Date, $inTo_Date);
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();


### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/WalthamForest_Decision/WalthamForest_Decision.ini" );

my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $POST_CNT = $Config->{$Council_Code}->{'POST_CNT'};
my $POST_DETAIL_URL = $Config->{$Council_Code}->{'POST_DETAIL_URL'};
my $POST_DETAIL_CNT = $Config->{$Council_Code}->{'POST_DETAIL_CNT'};
my $POST_NPAGE_CNT = $Config->{$Council_Code}->{'POST_NPAGE_CNT'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";

	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
}	


## Proxy settings ###
# if($Council_Code=~m/^(16)$/is)
# {
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
# }

my ($fDate,$tDate) = ($From_Date,$To_Date);

$fDate=~s/\//%2F/gsi;
$tDate=~s/\//%2F/gsi;

$HOME_URL=~s/<FromDate>/$fDate/gsi;
$HOME_URL=~s/<ToDate>/$tDate/gsi;

# Get search results using date ranges
my ($Responce, $cookie_jar, $Ping_Status1);
eval{
$Responce = $mech->get($HOME_URL);
$cookie_jar = $mech->cookie_jar;		
};

if ($@) {
	$Ping_Status1 = $@;
};	


$POST_CNT=~s/<FromDate>/$From_Date/gsi;
$POST_CNT=~s/<ToDate>/$To_Date/gsi;


$mech->add_header( "Accept" => 'application/json, text/javascript, */*; q=0.01' );
$mech->add_header( "Host" => "$HOST" );
$mech->add_header( "Content-Type" => 'application/json; charset=utf-8' );
$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
$mech->add_header( "Accept-Encoding" => 'gzip, deflate' );
$mech->add_header( "Referer" => "$HOME_URL" );

$mech->post( $POST_URL, Content => "$POST_CNT");
sleep(10);				
my $Code_status = $mech->status;
my $Content = $mech->content;

# open(PP, ">app_cont.html");
# print PP "$Content\n";
# close(PP); 
# exit;

&scrape_details($Content);


sub scrape_details()
{
	my $dtlCont = shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	my $json = JSON->new;
	my $data = $json->decode($dtlCont);
	
	
	# my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';
	
	my ($Application,$Proposal,$Decision_Status,$Application_Status,$Application_Link,$Date_Decision_Made);
	
	my @KeyObjects = @{$data->{'KeyObjects'}};
	foreach my $Keys ( @KeyObjects) 
	{							
		my $KeyText = $Keys->{'KeyText'};
		print "KeyText=>$KeyText\n";
		
		$POST_DETAIL_CNT=~s/<KeyText>/$KeyText/gsi;
		
		$mech->post( $POST_DETAIL_URL, Content => "$POST_DETAIL_CNT");
						
		my $Code_status = $mech->status;
		my $detailContent = $mech->content;
		
		
		my $detailData = $json->decode($detailContent);
		my $Items = $detailData->[0]->{'Items'};
		my $SummaryDetails = $detailData->[0]->{'SummaryDetails'};
		my @Items = @$Items;
		my @SummaryDetails = @$SummaryDetails;
		
			
		
		$Application_Link=$HOME_URL."#VIEW?RefType=APPPlanCase&KeyNo=0&KeyText=$KeyText";
		# print "Application_Link===>$Application_Link\n";
		
		$POST_DETAIL_CNT=~s/\"$KeyText\"\}$/\"\<KeyText\>\"\}/gsi;
		
		for my $Each_Time(@Items)
		{
			my $Label = $Each_Time->{'Label'};
			my $Value = $Each_Time->{'Value'};
			
			if($Label eq "Application Id")
			{
				$Application=&clean($Value);
			}
			if($Label eq "Application Description")
			{
				$Proposal=&clean($Value);
			}
			if($Label eq "Decision")
			{
				$Decision_Status=&clean($Value);
			}
			if($Label eq "Decision Date")
			{
				$Date_Decision_Made=&clean($Value);
			}
			if($Label eq "Application Status")
			{
				$Application_Status=&clean($Value);
			}
				
		}
		
		# print "Application==>$Application\n";
		# print "Proposal==>$Proposal\n";
		# print "Decision_Status==>$Decision_Status\n";
		# print "Date_Decision_Made==>$Date_Decision_Made\n";
		# print "Application_Status==>$Application_Status\n";
		# print "Application_Link==>$Application_Link\n";
		# <STDIN>;
		
		my $Source = "GCS001";	
	
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";	
		
		# $insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\')";
		
		my $insert_query="insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values (\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\')";
        print "insert_query::$insert_query\n";
		
		if($insert_query!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
		}
		
        undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 
				
	}
	# $insert_query=~s/\,$//igs;
		
	# print "insert_query::$insert_query\n";
	
	# if($insert_query!~m/values\s*$/is)
	# {
		# &Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	# }
	
}
		

sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						= 'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}