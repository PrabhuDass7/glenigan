import requests
import pandas as pd


def normalize_data_struct(dict_data):
    fields = ['areaId', 'wardId', 'parishId']
    for field in fields:
        value = dict_data.get(field)
        dict_data[field] = ", ".join([str(v) for v in value]) if value else None
    return dict_data
        

s = requests.session()
headers = {
    "Accept": "application/json, text/plain, */*",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "Host": "planningapi.agileapplications.co.uk",
    "Origin": "https://planning.agileapplications.co.uk",
    "Pragma": "no-cache",
    "Referer": "https://planning.agileapplications.co.uk/exmoor/search-applications/results?criteria=%7B%22decisionDateFrom%22:%222018-12-01%22,%22decisionDateTo%22:%222019-12-04%22%7D",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-site",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
    "x-client": "EXMOOR",
    "x-product": "CITIZENPORTAL",
    "x-service": "PA"
}

s.headers.update(headers)

response = s.get('https://planningapi.agileapplications.co.uk/api/application/search?decisionDateFrom=2018-12-01&decisionDateTo=2019-12-04')

json_page_source = response.json()
results = json_page_source.get('results')
if isinstance(results, list):
    df = pd.DataFrame([normalize_data_struct(row) for row in results])
    df.to_excel('listing_exmoor.xlsx', index=None)