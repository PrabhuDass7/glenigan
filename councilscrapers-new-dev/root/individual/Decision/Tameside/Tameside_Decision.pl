use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"392\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";
if($From_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$sDay = $1;
	# $sMonth = $month{$2};
	$sMonth = $2;
	$sYear = $3;
	print "sDay==>$sDay \n";
	print "sMonth==>$sMonth \n";
	print "sYear==>$sYear \n";
}
if($To_Date=~m/^(\d{2})\/(\d{2})\/(\d{4})$/is)
{
	$eDay = $1;
	# $eMonth = $month{$2};
	$eMonth = $2;
	$eYear = $3;
	print "eDay==>$eDay \n";
	print "eMonth==>$eMonth \n";
	print "eYear==>$eYear \n";
}


my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://public.tameside.gov.uk/plan/f422planapp.asp";
$mech->get($searchURL);
my $Content = $mech->content;
my $status = $mech->status;
print "status:::$status\n\n";

my ($ViewState,$ViewstateGenerator,$EventValidation,$Eventtarget);
my $searchPagePostCont = "F01_AppNo=+&F02_District=99&F03_Proposal=+&F04_Site=+&F05_Name=+&F06_Pmm=+&F07_Pyyyy=&F08_Fdd=<FDAY>&F09_Fmm=<FMON>&F10_Fyyyy=<FYEAR>&F11_Tdd=<TDAY>&F12_Tmm=<TMON>&F13_Tyyyy=<TYEAR>&submit=Find&action=Home";

open(DC,">Content.html");
print DC $Content;
close DC;

$searchPagePostCont=~s/<FDAY>/$sDay/igs;
$searchPagePostCont=~s/<TDAY>/$eDay/igs;
$searchPagePostCont=~s/<FMON>/$sMonth/igs;
$searchPagePostCont=~s/<TMON>/$eMonth/igs;
$searchPagePostCont=~s/<FYEAR>/$sYear/igs;
$searchPagePostCont=~s/<TYEAR>/$eYear/igs;

print "searchPagePostCont==>$searchPagePostCont\n";


$mech->add_header( "Accept" => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' );
$mech->add_header( "Host" => 'public.tameside.gov.uk' );
$mech->add_header( "Content-Type" => 'application/x-www-form-urlencoded' );
$mech->add_header( "Accept-Language" => 'en-US,en;q=0.5' );
$mech->add_header( "Accept-Encoding" => 'gzip, deflate, br' );
$mech->add_header( "Origin" => 'https://public.tameside.gov.uk' );
$mech->add_header( "Upgrade-Insecure-Requests" => '1' );
$mech->add_header( "Referer" => "$searchURL" );	

$mech->post($searchURL, Content => "$searchPagePostCont");

my $SearchResultContent = $mech->content;
print $mech->status;

open(DC,">SearchResultContent.html");
print DC $SearchResultContent;
close DC;
exit;

my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

my $Page_Number=2;
NextPage:
while($SearchResultContent=~m/<form\s*action=\"f422planapp\.asp\"\s*method=\"post\">\s*<table[^>]*?>([\w\W]*?Application\s*Number[^>]*?<\/b>\s*<\/td>\s*<td\s*class\s*=\s*\"\s*center\s*\"\s*>\s*([^>]*?)\s*<\/td>[\w\W]*?)<\/div>\s*<\/form>/igs)
{
	my $App_content=$1;
	my $Application_Num=$2;
	print "Application_Num==>$Application_Num\n";
	
	
	my $From_Date_temp = uri_escape($From_Date);
	my $To_Date_temp = uri_escape($To_Date);
	
		
	# open(DC,">App_content.html");
	# print DC $App_content;
	# close DC;
	# exit;
	
	my ($Application,$Proposal,$Decision_Status,$Application_Status,$Date_Decision_Made);
	
	$Application=&clean($1) if($App_content=~m/<td[^>]*?>\s*(?:<b>)?\s*Application\s*Number[^>]*?(?:<\/b>)?\s*<\/td>\s*<td[^>]*?>\s*(?:<b>)?\s*([^<]*?)\s*(?:<\/b>)?\s*<\/td>/is);
	$Date_Decision_Made=&clean($1) if($App_content=~m/<input[^>]*?name=\"DecisionDate\d+\"\s*value=\"([^\"]*?)\"[^>]*?>/is);	
	$Proposal=&clean($1) if($App_content=~m/<td[^>]*?>\s*(?:<b>)?\s*Proposal[^>]*?(?:<\/b>)?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	$Application_Status=&clean($1) if($App_content=~m/<td[^>]*?>\s*(?:<b>)?\s*Status[^>]*?(?:<\/b>)?\s*<\/td>\s*<td[^>]*?>\s*(?:<b>)?([^<]*?)\s*(?:<\/b>)?\s*<\/td>/is);
	$Decision_Status=&clean($1) if($App_content=~m/<input[^>]*?name=\"Decision\d+\"\s*value=\"([^\"]*?)\"[^>]*?>/is);
	
	my $Application_URL = $searchURL;
	
	my $COUNCIL_NAME = 'Tameside';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	# my $Source = "GCS001";	
	my $Source = $Date_Range;	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_URL\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_URL; 
	
}

if($SearchResultContent=~m/<input\s*type\s*=\s*\"hidden\"\s*name\s*=\s*\"([^>]*?ArraySub)\"\s*value\s*=\s*\"([\d]+)"[^>]*?>([\w\W]*?)<\/form>/is)
{	
	
	my $Next_Page_Url=$1;
	my $Page_no=$2;
	my $Block=$3;
	
	my $Next_ref_app_no;
	while($Block=~m/<input[^>]*?name\s*=\s*\"(ref[^>]*?)\"\s*value=\s*\"([^>]+?)\"\s*\/\s*>/igs)
	{
		my $ref_val=$1;
		my $App_no=uri_escape($2);
		$Next_ref_app_no.=$ref_val.'='.$App_no.'&';
	}
	
	my $nextPostContent = "submit=More&<ArraySub>=<Pageno>&<Ref_no>F01_AppNo=&F02_District=99&F03_Proposal=&F04_Site=&F05_Name=&F06_Pmm=&F07_Pyyyy=&F08_Fdd=<FromDay>&F09_Fmm=<FromMonth>&F10_Fyyyy=<FromYear>&F11_Tdd=<ToDay>&F12_Tmm=<ToMonth>&F13_Tyyyy=<ToYear>";
	
	$nextPostContent=~s/<ArraySub>/$Next_Page_Url/igs;
	$nextPostContent=~s/<Pageno>/$Page_no/igs;
	$nextPostContent=~s/<Ref_no>/$Next_ref_app_no/igs;
	$nextPostContent=~s/<FromDay>/$sDay/igs;
	$nextPostContent=~s/<FromMonth>/$sMonth/igs;
	$nextPostContent=~s/<FromYear>/$sYear/igs;
	$nextPostContent=~s/<ToDay>/$eDay/igs;
	$nextPostContent=~s/<ToMonth>/$eMonth/igs;
	$nextPostContent=~s/<ToYear>/$eYear/igs;
	
	$mech->add_header( "Host" => 'public.tameside.gov.uk' );
	$mech->add_header( "Referer" => "$searchURL" );	
	
	$mech->form_name( 'form3' );
	$mech->set_fields( 'submit' => 'More' );
	$mech->click();
	
	
	# $mech->post($searchURL, Content => "$searchPagePostCont");

	
	
	my $content = $mech->content;
	$SearchResultContent = $content;
	
	goto ENDPAGE if($SearchResultContent!~m/<input\s*type=\"submit\"\s*name=\"submit\"\s*value=\"More\"\s*\/>/is);
	
	# open(DC,">SearchResultContent.html");
	# print DC $SearchResultContent;
	# close DC;
	# exit;

	
	print "Page_Number: $Page_Number\n";
	
	$Page_Number++;
	
	
	goto NextPage;
}
ENDPAGE:

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
# print "insert_query: $insert_query\n";


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=Glenigan;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}