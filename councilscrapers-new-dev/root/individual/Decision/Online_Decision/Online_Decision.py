# -*- coding: utf-8 -*-
import requests as req
import json
import sys,os,re
import pandas as pd
from datetime import datetime, timedelta
import pymssql
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import ssl
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import imp,math
import configparser
import codecs
from time import sleep
from random import randint

''' Create Log Directory'''
basePath = os.getcwd()
print ("basePath::",basePath)
Config = configparser.ConfigParser()
Config.read(str(basePath)+'\\'+str('Online_Decision.ini'))

todaydate = time.strftime('%Y%m%d')
logDirectory=basePath+"/log/"+todaydate
if os.path.isdir(str(logDirectory)) == False:
	os.makedirs(str(logDirectory))

reload(sys)
sys.setdefaultencoding("utf-8")

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	# conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	return (conn)

# Insert Query
def dbInsert(InsertQuery,queryType):
	try:
		cursor.execute(InsertQuery)
		conn.commit()
		print ("*** DB Insertion Succesfully ***")
	except Exception as e:
		QueryTxt=logDirectory+"/"+'Failed_Query_'+councilcode+"_"+gcs+".txt"
		with open(QueryTxt, 'a') as fd:
			fd.write(str(InsertQuery)+"\t"+str(queryType)+"\t"+str(e)+"\t"+"\n")
			
# Clean function
def clean(cleanValue):
	try:	  
		clean = ''	
		clean = re.sub(r'\&lt\;', "\<", str(cleanValue))
		clean = re.sub(r'\&gt\;', "\>", str(cleanValue))
		clean = re.sub(r'<br\s*\/?\s*>', " ", str(cleanValue))
		clean = re.sub(r'<\/br>', " ", str(cleanValue))
		clean = re.sub(r'<\/br>', " ", str(cleanValue))
		clean = re.sub(r'\n', "", str(cleanValue))
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
		
		return clean	

	except Exception as ex:
		print (ex,sys.exc_traceback.tb_lineno)
		
def tabDetailsCollection(tabContent, blockSection, category):
	
	''' Ascii Code Conversion '''
	tabContent = tabContent.encode("ascii", "ignore")
	tabContent = tabContent.decode()
	
	tabDetails={}
	for Key in Config[blockSection]:
		Regex=Config.get(blockSection,Key)
		dataPoint=re.findall(Regex, str(tabContent), re.IGNORECASE)
		try:
			tabDetails[Key] = dataPoint[0]
		except:
			tabDetails[Key] = ''
	return(tabDetails)

def getApplicationDetails(receivedApplicationUrl, category, councilCode):
	tempURL=str(receivedApplicationUrl)
	ses = req.session()
	headers = {
				'Host': Config.get(councilCode,"host"),
				'Content-Type': 'text/html;charset=ISO-8859-1',
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36',
			}
			
	ses.headers.update(headers)
	
	count=0
	applicationDetailsPageResponse=500
	applicationDetailsPageContent=''
	dataDetails={}
	applicationNumber=''
	try:
		while(count<=3):
			if not (re.search("^200$",str(applicationDetailsPageResponse))):
				try:
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					sleep(randint(5,10))
					applicationDetailsPageResponse=response1.status_code
					applicationDetailsPageContent=response1.content

					applicationDetailsPageContent = BeautifulSoup(applicationDetailsPageContent, 'html.parser')
					applicationDetailsPageContent=str(applicationDetailsPageContent)
				except:
					print ("*** Error 1 ***")
					pass
				count+=1;
			else:
				break

		if(re.findall(Config.get('summary','case_reference'),str(applicationDetailsPageContent),re.IGNORECASE)):
			applicationNumber=re.findall(Config.get('summary','case_reference'),applicationDetailsPageContent)
			applicationNumber=applicationNumber[0]
		elif(re.findall(Config.get('summary','application_reference'),applicationDetailsPageContent,re.IGNORECASE)):
			applicationNumber=re.findall(Config.get('summary','application_reference'),applicationDetailsPageContent,re.IGNORECASE)
			applicationNumber=applicationNumber[0]
		elif(re.findall(Config.get('summary','summary_casenumber'),applicationDetailsPageContent,re.IGNORECASE)):
			applicationNumber=re.findall(Config.get('summary','summary_casenumber'),applicationDetailsPageContent,re.IGNORECASE)
			applicationNumber=applicationNumber[0]
		
		if(re.search("activeTab=summary",str(tempURL))):
			print ("ActiveTab ==> Summary")
			activeTabContents = tabDetailsCollection(applicationDetailsPageContent, "summary", category)
			dataDetails.update(activeTabContents)
		
		if not re.search("^149$",str(councilCode)):
			count=0
			while(count<=3):
				try:
					tempURL=re.sub(r'activeTab=summary','activeTab=details',str(tempURL))
					print ("ActiveTab ==> Details")
					print ("ActiveTabURL ==> ",tempURL)
					sleep(randint(5,10))
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')
						
					activeTabContents = tabDetailsCollection(detailsPageContent, "details", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Details Error1 ***")
					pass
				count+=1
		if (category != "Decision"):
			tempURL=re.sub(r'activeTab=details','activeTab=contacts',str(tempURL))
			print ("ActiveTab ==> Contacts")
			print ("ActiveTabURL ==> ",tempURL)
			sleep(randint(5,10))
			count=0
			while(count<=3):
				try:
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')

					activeTabContents = tabDetailsCollection(detailsPageContent, "contacts", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Planning Contacts Error ***")
					pass
				count+=1	
					
		elif(category == "Decision"):
			tempURL=re.sub(r'activeTab=details','activeTab=dates',tempURL)
			print ("ActiveTab ==> Dates")
			print ("ActiveTabURL ==> ",tempURL)
			
			keyVal=re.findall(r'keyVal=([^>]*?)\&activeTab=dates\s*$',str(tempURL), re.IGNORECASE)
		  
			count=0
			while(count<=3):
				try:
					tempURL = re.search(r'^([^>]*?)('+str(keyVal[0])+'=[^>]*?)\&(activeTab=dates)\s*$', tempURL)
					tempURL=tempURL.group()+tempURL.group(1)+'\&'+tempURL.group(1)
					print ("Elif ActiveTabURL ==> ",tempURL)
					sleep(randint(5,10))
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')
					activeTabContents = tabDetailsCollection(detailsPageContent, "dates", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Decision Dates Error ***")
					pass
				count+=1
		if(category == "Planning"):
			tempURL=re.sub(r'activeTab=contacts','activeTab=dates',tempURL)
			print ("ActiveTab ==> Dates")
			print ("ActiveTabURL ==> ",tempURL)
			sleep(randint(5,10))
			count=0
			while(count<=3):
				try:
					response1=ses.get(tempURL,headers=headers,proxies=proxies1,verify=False)
					code=response1.status_code
					detailsPageContent=response1.content
					detailsPageContent = BeautifulSoup(detailsPageContent, 'html.parser')

					activeTabContents = tabDetailsCollection(detailsPageContent, "dates", category)
					dataDetails.update(activeTabContents)
					break
				except:
					print ("*** Planning Dates Error ***")
					pass
				count+=1
	except Exception as e:
		print ("getApplicationDetails Error =>",e)
	
	return(dataDetails, applicationDetailsPageResponse, applicationNumber)

def WeeklyMonthlySearch(DayWise,category,GCS):
	print ("*** Month Wise Application ***")

	if DayWise == "week":
		monthly_URL='https://caps.woking.gov.uk/online-applications/search.do?action=weeklyList'
	else:
		monthly_URL='https://caps.woking.gov.uk/online-applications/search.do?action=monthlyList'
	
	browser.get(monthly_URL)
	sleep(randint(7,10))
	monID=browser.find_element_by_id(str(DayWise))
	selectCount=len(monID.find_elements_by_tag_name("option"))
	# print("SelectCount::", len(monID.find_elements_by_tag_name("option")))
	
	MonthlyURLs=[]
	Count=0
	
	position =	 {
	  "GCS001": 1,
	  "GCS002": 2,
	  "GCS003": 3,
	  "GCS004": 4,
	  "GCS005": 5,	 
	  "GCS090": 6,
	  "GCS180": 7,
	} 
	
	pos=searchfinalCnt=""
	if gcs !="":
		pos = position[gcs]
	
	while(selectCount > Count):
		
		if Count != 0:
			browser.get(monthly_URL)
			sleep(randint(7,10))
		
		if category == "Decision":
			browser.find_element_by_id("dateDecided").click()
		
		if GCS == "GCS":
			Count=int(pos)
			Count=Count-1
	   
		monID=browser.find_element_by_id(str(DayWise))
		monID.find_elements_by_tag_name("option")[Count].click()
		
		Count+=1
		try:
			clickCount=1
			while(1):
				try:
					btn=browser.find_element_by_class_name("buttons")
					btns=btn.find_elements_by_tag_name("input")
					for searchBtn in btns:
						print ("SEARCH :: ",searchBtn.get_attribute("value"))
						if re.search(searchBtn.get_attribute("value"),"Search"):
							searchBtn.click()
							print ("Button Clicked ***")
							sleep(randint(5,8))
							break
				except Exception as e:
					print ("Button Error::",e)
					break
				
				if clickCount == 3:
					break
				
				clickCount+=1
					
			sleep(randint(8,10))
			browser.find_element_by_id('resultsPerPage').send_keys('100')
			browser.find_element_by_xpath('//*[@id="searchResults"]/input[4]').click()
			sleep(randint(10,13))
			
			total_Page=1
			searchcontent=browser.page_source
			try:
				total_Page = re.findall("class\=\"page\">([^>]*?)<\/a>[^.]*?<[^>]*?class\=\"next\">", str(searchcontent), re.IGNORECASE)
				total_Page=total_Page[0]
			except:
				total_Page=1

			print ("total_Page::",total_Page)

			try:
				while(browser.find_elements_by_class_name("next")[0]):
					browser.find_elements_by_class_name("next")[0].click()
					print ("*** Next Page Clicked ***")
					sleep(randint(6,9))
					tempcontent = browser.page_source
					sleep(randint(5,8))
					tempcontent = BeautifulSoup(tempcontent, 'html.parser')
					searchcontent=searchcontent + str(tempcontent)
			except Exception as e:
				print ("No More Next Page::", e)
		 
			# -- Wait funtion
			# sleep(randint(10,15))
			
			if len(searchcontent) > 0:
				searchfinalCnt = searchcontent.encode("utf-8")
			
			filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
			with open(filename, 'wb') as fd:
				fd.write(searchfinalCnt)
			
			searchfinalCnt = BeautifulSoup(searchfinalCnt, 'html.parser')
		except Exception as e:
			print ("Search Button ERROR::",e)
			
		if GCS == "GCS":
			break
	return (searchfinalCnt)
	
# daterange search section	
def inputsection(dumFrom_Date, dumTo_Date, driver, councilcode, gcs):
	try:
		print ("home_url:: ",Config.get(councilcode,'home_url'))
		driver.get(Config.get(councilcode,'home_url'))
		sleep(randint(7,10))
		
		''' Advance Button - Click '''
		try:
			tab=driver.find_element_by_class_name("tabs")
			lis =tab.find_elements_by_tag_name("li")
			for li in lis:
				if re.search(li.text,"Advanced"):
					li.click()
					break
		except Exception as e:
			print ("ERROR::",e)
			
		''' Date Range Set '''
		driver.find_element_by_id(Config.get(councilcode,"form_start_id")).send_keys(dumFrom_Date)
		sleep(randint(4,6))
		driver.find_element_by_id(Config.get(councilcode,"form_end_id")).send_keys(dumTo_Date)
		sleep(randint(4,6))
		
		''' advancedSearchForm Button - Click '''
		try:
			btn=driver.find_element_by_class_name("buttons")
			btns=btn.find_elements_by_tag_name("input")
			for searchBtn in btns:
				print ("SEARCH :: ",searchBtn.get_attribute("value"))
				if re.search(searchBtn.get_attribute("value"),"Search"):
					searchBtn.click()
					time.sleep(4)
					# wc=0
					# while(1):
						# wc+1
						# if re.search(searchBtn.get_attribute("value"),"Search"):
							# searchBtn.click()
							# time.sleep(4)
						# if wc == 3:
							# break
						# if not re.search(searchBtn.get_attribute("value"),"Search"):
							# break
					break
		except Exception as e:
			print ("Search Button ERROR::",e)
		
		
		try:
			sleep(randint(8,11))
			driver.find_element_by_id('resultsPerPage').send_keys('100')
			driver.find_element_by_xpath('//*[@id="searchResults"]/input[4]').click()
			sleep(randint(10,13))
		except:
			return ("Captcha Return")
		
		total_Page=1
		searchcontent=driver.page_source
		searchcontent = BeautifulSoup(searchcontent, 'html.parser')
		try:
			total_Page = re.findall("class\=\"page\">([^>]*?)<\/a>[^.]*?<[^>]*?class\=\"next\">", str(searchcontent), re.IGNORECASE)
			total_Page=total_Page[0]
		except:
			total_Page=1

		
		print ("total_Page::",total_Page)

		try:
			while(driver.find_elements_by_class_name("next")[0]):
				driver.find_elements_by_class_name("next")[0].click()
				print ("*** Next Page Clicked ***")
				sleep(randint(6,9))
				tempcontent = driver.page_source
				sleep(randint(4,8))
				tempcontent = BeautifulSoup(tempcontent, 'html.parser')
				searchcontent=str(searchcontent) + str(tempcontent)
		except Exception as e:
			print ("No More Next Page::", e)
	 
		# -- Wait funtion
		sleep(randint(10,13))
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
		with open(filename, 'wb') as fd:
			fd.write(searchfinalCnt)
		
		searchfinalCnt = BeautifulSoup(searchfinalCnt, 'html.parser')
		return (searchfinalCnt)
	except Exception as e:
		searchcontent=driver.page_source
		searchfinalCnt = searchcontent.encode("utf-8")
		filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
		with open(filename, 'wb') as fd:
			fd.write(searchfinalCnt)
		print (e,sys.exc_traceback.tb_lineno)

def LinkCollection(councilCode,searchcontent):
	Links = re.findall("<li\s*class\=\"searchresult\">\s*<a\s*href\=\"([^>]*?)\">", str(searchcontent), re.IGNORECASE)
	Detail_Links=[]
	for Link in Links:
		if Link != '':
			if not (re.search("^http", Link)):
				# print ("councilCode:: ",str(councilCode))
				Link = Config.get(councilCode,'filter_url') + Link;
			Link = re.sub(r'\&amp\;',"&",str(Link))
			# print ("Application_Link:: ", Link)
			Detail_Links.append(str(Link))
	return Detail_Links

def fetchData(dictionaryValue,keys):
	values=''
	keys=keys.lower()
	try:
		if dictionaryValue[keys]:
			values=dictionaryValue[keys]
			values=clean(values)
			# print ("keys",keys)
			# print ("values",values)
	except:
		pass
	return str(values)
	
def applicationsDataDumpPlanning(TabContent, applicationLink, Source, councilCode, councilName, scheduleDate, scheduleDateTime):
	
	Date_Decision_Made = Decision_Status = Decision_Issued_Date = Proposal = Application = Application_Status = sourceWithTime =''
	
	''' Summer Tab Details '''
	DATE_DECISION_MADE = fetchData(TabContent,'DATE_DECISION_MADE')
	APPLICATION_REFERENCE = fetchData(TabContent,'APPLICATION_REFERENCE')
	CASE_REFERENCE = fetchData(TabContent,'CASE_REFERENCE')
	DECISION_STATUS = fetchData(TabContent,'DECISION_STATUS')
	DECISION_ISSUED_DATE = fetchData(TabContent,'DECISION_ISSUED_DATE')
	PROPOSAL = fetchData(TabContent,'PROPOSAL')
	APPLICATION = fetchData(TabContent,'APPLICATION')
	APPLICATION_STATUS = fetchData(TabContent,'APPLICATION_STATUS')
	SUMMARY_CASENUMBER = fetchData(TabContent,'SUMMARY_CASENUMBER')
	SUMMARY_PROPOSAL = fetchData(TabContent,'SUMMARY_PROPOSAL')
	AUTHORITY_DECISION_STATUS = fetchData(TabContent,'AUTHORITY_DECISION_STATUS')
	AUTHORITY_DECISION_DATE = fetchData(TabContent,'AUTHORITY_DECISION_DATE')
	
	''' Dates '''
	DECISION_ISSUED_DATES = fetchData(TabContent,'DECISION_ISSUED_DATES')
	DECISION_MADE_DATE = fetchData(TabContent,'DECISION_MADE_DATE')
	DECISION_DATE = fetchData(TabContent,'DECISION_DATE')
	DECISION_PRINTED_DATE = fetchData(TabContent,'DECISION_PRINTED_DATE')

	''' Details '''
	DECISION = fetchData(TabContent,'DECISION')
		
	if not (re.search('^\s*$',APPLICATION)):
		Application=APPLICATION
	elif not (re.search('^\s*$',CASE_REFERENCE)):
		Application=CASE_REFERENCE
	elif not (re.search('^\s*$',APPLICATION_REFERENCE)):
		Application=APPLICATION_REFERENCE
	elif not (re.search('^\s*$',SUMMARY_CASENUMBER)):
		Application=SUMMARY_CASENUMBER
			
	if not (re.search('^\s*$',PROPOSAL)):
		Proposal = PROPOSAL
	elif not (re.search('^\s*$',SUMMARY_PROPOSAL)):
		Proposal=SUMMARY_PROPOSAL
	
	if not re.search('^\s*$',DECISION_ISSUED_DATE):
		Decision_Issued_Date=DECISION_ISSUED_DATE
	elif not (re.search('^\s*$',DECISION_ISSUED_DATES)):
		Decision_Issued_Date=DECISION_ISSUED_DATES
	
	if not re.search('^\s*$',DECISION_STATUS):
		Decision_Status=DECISION_STATUS
	elif not (re.search('^\s*$',DECISION)):
		Decision_Status=DECISION
	elif not (re.search('^\s*$',AUTHORITY_DECISION_STATUS)):
		Decision_Status=AUTHORITY_DECISION_STATUS
	
	if not re.search('^\s*$|Not\s*Available[^>]*?$',DECISION_MADE_DATE):
		Date_Decision_Made=DECISION_MADE_DATE
	elif not (re.search('^\s*$|Not\s*Available[^>]*?$',DATE_DECISION_MADE)):
		Date_Decision_Made=DATE_DECISION_MADE
	elif not (re.search('^\s*$|Not\s*Available[^>]*?$',DECISION_DATE)):
		Date_Decision_Made=DECISION_DATE
	elif not (re.search('^\s*$|Not\s*Available[^>]*?$',DECISION_PRINTED_DATE)):
		Date_Decision_Made=DECISION_PRINTED_DATE
	elif not (re.search('^\s*$|Not\s*Available[^>]*?$',AUTHORITY_DECISION_DATE)):
		Date_Decision_Made=AUTHORITY_DECISION_DATE	  
		
	if not re.search('^\s*$',APPLICATION_STATUS):
		Application_Status=APPLICATION_STATUS
	
	sourceWithTime=Source+"_"+scheduleDateTime+"-python"
	
	dateLength=len(Date_Decision_Made)

	if(int(dateLength)>50):
		Date_Decision_Made = ''
		
	Date_Decision_Made=re.sub(r'Not\s*Available[^>]*?',"",Date_Decision_Made)
	Decision_Status=re.sub(r'^\s*(Decision\s*made)\,\s*view[\w\W]*?$',r"\1",Decision_Status)
	Decision_Status=re.sub(r'^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$',r"\1",Decision_Status)
	Application_Status=re.sub(r'^\s*(Decision\s*made)\,\s*view[\w\W]*?$',r"\1",Application_Status)
	Application_Status=re.sub(r'^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$',r"\1",Application_Status)
	Application_Status=re.sub(r'^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$',r"\1",Application_Status)

	Proposal=re.sub(r'\n+'," ",Proposal)
	Application_Status=re.sub(r'\n+'," ",Application_Status)
	Decision_Status=re.sub(r'\n+'," ",Decision_Status)
	
	insert_query="("+"'"+str(Application)+"','"+str(Proposal)+"','"+str(Decision_Status)+"','"+str(Date_Decision_Made)+"','"+str(Decision_Issued_Date)+"','"+str(councilCode)+"','"+str(applicationLink)+"','"+str(sourceWithTime)+"','"+str(Application_Status)+"','"+str(scheduleDate)+"')"
	
	return(insert_query)
	
# Application parse section 
def parsesection(searchcontent, CouncilCode, Source, conn, cursor, driver):
	try:
		# searchcount = re.findall("class\=\"showing\">[\w\W]*?Showing[\w\W]*?of\s*([\d]+)\s*<[^>]*?>", str(searchcontent), re.IGNORECASE)	
		# totalcount = int(searchcount[0])
		# print ("Totalcount", totalcount)
		
		insertQuery = 'insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status, Imported_Date) values '
		 
		bulkValuesForQuery=''
		appCount=1
		# resultsCount=0
		appcollection=LinkCollection(CouncilCode,searchcontent)
		print ("LINK COUNT :: ",len(appcollection))
		for Application_Link in appcollection:
			print ("app_url :: ",Application_Link)
			sleep(randint(5,10))
			
			dataDetails = applicationPageContentResponse = appNumber=''
			dataDetails, applicationPageContentResponse, appNumber = getApplicationDetails(str(Application_Link),"Decision",CouncilCode)
			
			# print ("dataDetails::", dataDetails)
			# input ("STOP")
			fileTxt=logDirectory+"/"+councilcode+"_"+gcs+".txt"
			with open(fileTxt, 'a') as fd:
				fd.write(str(Application_Link)+"\t"+str(applicationPageContentResponse)+"\n")
			
			# if(re.search("^\s*(200|ok)\s*$",applicationPageContentResponse):
				# resultsCount+=1
			
			Council_Name=Config.get(CouncilCode,'COUNCIL_NAME')
			format1 = "%Y/%m/%d %H:%M"	
			Schedule_Date = datetime.now().strftime(format1)

			if appNumber:
				appDataCollection = applicationsDataDumpPlanning(dataDetails,Application_Link,Source,CouncilCode,Council_Name,Schedule_Date,Schedule_Date);
				print ("appDataCollection :: ",appDataCollection)
				if appCount == 1:
					bulkValuesForQuery = insertQuery+appDataCollection
					appCount += 1
				elif appCount == 5:
					bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					# print ("bulkValuesForQuery :: ",bulkValuesForQuery)
					''' DB Insert '''
					dbInsert(bulkValuesForQuery,"bulkValuesForQuery")
					appCount=1
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
					appCount += 1
		return bulkValuesForQuery		
	except Exception as e:
		print (e,sys.exc_traceback.tb_lineno)
		
# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print ('Councilcode arugument is missing')
		sys.exit()
		
	if gcs is None:
		print ('GCS arugument is missing')
		sys.exit()
	
	conn=cursor=""
	conn = dbConnection("GLENIGAN") 
	cursor = conn.cursor()

	PROXY = "172.27.137.192:3128" # IP:PORT or HOST:PORT
	# PROXY = "172.27.140.48:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	# browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver.exe')
	browser.maximize_window()
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]		
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%d/%m/%Y"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	print ('fromdate	 :', fromdate)
	print ('todate	 :', todate)
	
	try:
		Content = inputsection(fromdate, todate, browser, councilcode, gcs)
		# Content = inputsection('01/01/2019', '03/04/2019', browser)
		# sleep(randint(10,12))

		''' Weekly/Monthly Wise Application '''
		if Content == "Captcha Return":
			Content = WeeklyMonthlySearch("week","Decision","GCS")
		
		#browser.quit()
		try:
			browser.quit()
		except:
			pass
		
		finalinsertQuery = parsesection(Content, councilcode, gcs, conn, cursor, browser)
		# print ("finalinsertQuery::",finalinsertQuery)
		
		''' DB Insert - final output query insertion'''
		if (finalinsertQuery != '') or finalinsertQuery is not None:
			dbInsert(finalinsertQuery,"finalinsertQuery")
	except:
		try:
			browser.quit()
		except:
			pass
		pass
