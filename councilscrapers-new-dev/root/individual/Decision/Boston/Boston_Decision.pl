use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server

my $dbh = Glenigan_DB_Windows::DB_Decision;

my $Council_Code = $ARGV[0];

my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://services.boston.gov.uk/agile/searchResults.aspx";
$mech->add_header("Host"=>"services.boston.gov.uk");
$mech->add_header("Referer"=>"https://services.boston.gov.uk/agile/?q=/planning-application-search/");
$mech->post($searchURL, Content=>'inputSearch=%25');

my $SearchResultContent = $mech->content;

my $tableCnt = $1 if($SearchResultContent=~m/<table[^>]*?id=\"results\"[^>]*?>[\w\W]*?<\/thead>\s*<tbody>\s*([\w\W]*?)\s*<\/table>/is);

my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

my $Page_Number=1;
NextPage:
while($tableCnt=~m/<tr[^>]*?>\s*<td>\s*([^<]*?)\s*<\/td>[\w\W]*?<\/tr>/igs)
{
	my $Application_Num=$1;
	next if($Application_Num=~m/^Ref$/is);
	print "Application_Num==>$Application_Num\n";
	
	my $Application_URL="https://services.boston.gov.uk/agile/planApp.aspx?ref=<APPNUM>";
	$Application_URL=~s/<APPNUM>/$Application_Num/is;
	
	
	$mech->get( $Application_URL );
	my $Code_status = $mech->status;
	my $AppCnt = $mech->content;
	
	my ($Application,$Proposal,$Decision_Status,$Application_Status,$Date_Decision_Made);
	
	$Application=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Reference\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Date_Decision_Made=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Decision\s*Date\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	$Proposal=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Proposal\s*<\/th>\s*<td>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	$Decision_Status=&clean($1) if($AppCnt=~m/<tr[^>]*?>\s*<th>\s*Decision\s*<\/th>\s*<td>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	
	
	if($Application_Num ne "")
	{
		$Application = $Application_Num;
	}
	
	
	my $COUNCIL_NAME = 'Boston';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	my $Source = "GCS001";	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_URL\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_URL;
	
}

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
print "insert_query: $insert_query\n";


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=Glenigan;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}