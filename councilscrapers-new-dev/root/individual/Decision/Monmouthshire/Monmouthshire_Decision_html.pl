use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use FileHandle;
use Glenigan_DB_Windows;

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();


my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
my $ua=LWP::UserAgent->new(show_progress=>1);

$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect('7');
my $file=$0."_cookie.txt";
my $cookie=HTTP::Cookies->new(file=>$file,autosave=>1,);
$ua->cookie_jar($cookie);

BEGIN {
 $ENV{HTTPS_PROXY} = 'https://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

my $spefication_url="https://www.monmouthshire.gov.uk/planning-3/weekly-planning-lists/";
my $content=&Getcontent($spefication_url,"GET","","");

# open FH, ">HomeContent.html";
# print FH $content;
# close FH;
# exit;

if($content=~m/Decided\s*planning\s*applications<\/h2>\s*([\w\W]*?)\s*<\/ul>/is)
{
	my $sub_content=$1;
	my $currentYearURL = $1 if($sub_content=~m/<li[^>]*?>\s*<a\s*href=\"([^\"]*?)\"[^>]*?>\s*2018\s*<\/a>\s*<\/li>/is);
	my $contents=&Getcontent($currentYearURL,"GET","","");
	
	my $subContent = $1 if($contents=~m/<p>Determined\s*Lists\s*\d{4}<\/p>\s*([\w\W]*?)\s*<\/ul>/is);
	
	while($subContent=~m/<li[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*<\/a>\s*<\/li>/igs)
	{
		my $url_a=$1;
		my $file_name=$2;
		$file_name=~s/[^A-z\d]+//isg;	
		
		print "\nfile_name--->$file_name\n";
		
		my $pdm_name1=$file_name.'.pdf';
		my $somedir="C:/Glenigan/Live_Schedule/Decision/Monmouthshire/export";
		
		opendir(DIR, $somedir) || die "can't opendir $somedir: $!";
		my @files = grep { (!/^\./) && -f "$somedir/$_" } 
		readdir(DIR);
		closedir DIR;
		
		next if($pdm_name1~~@files);	
	
		my $pdf_url;
		if($url_a!~m/^http/is)
		{
			$pdf_url="http://www.monmouthshire.gov.uk/app/$url_a";
		}
		else
		{
			$pdf_url=$url_a;
		}
		
		my $pdf_content =&Getcontent("$pdf_url","","","GET");
		print "\n$pdf_url\n";
			
		my $fh = FileHandle->new("C:/Glenigan/Live_Schedule/Decision/Monmouthshire/export/$file_name.pdf", 'w') or die "Cannot open $file_name.pdf for write :$!";
		binmode($fh);
		$fh->print($pdf_content);
		$fh->close();
		
		my $filename1 = "C:/Glenigan/Live_Schedule/Decision/Monmouthshire/export/$file_name.xml";			
		
		system("C:/Glenigan/Live_Schedule/Decision/Monmouthshire/pdftohtml.exe \-xml \"C:/Glenigan/Live_Schedule/Decision/Monmouthshire/export/$file_name.pdf\"");
		sleep(10);
		
		undef $/;
		
		open KV, "$filename1";
		my $text=<KV>;
		close KV;
		# exit;
=pod			
		my $front="";
		if($text=~m/^\s*([^<]*?)\s*Rhif\s+y\s+Cais\s+/is)
		{
			$front=$1;
		}
		$text=~s/$front//gsi;
		$text=~s///gsi;
=cut			

		$text=~s/^([\w\W]*?)(DC\/\d{4}\/\d{5})/<START>\n$2/si;
		$text=~s/(\d+\-[A-Za-z]+\-\d{4})([\w\W]*?)(DC\/\d{4}\/\d{5})/$1\n<START>\n$3/gsi;
		$text=~s/(\d+\-[A-Za-z]+\-\d{4})\s*<START>/$1\n<END>\n<START>/gsi;
		$text=~s/(\d+\-[A-Za-z]+\-\d{4})\s*<\/text>[\w\W]*?$/$1\n<END>/si;
		
		
	
		open FH, ">$filename1";
		print FH $text;
		close FH;
		
		# exit;			
		
					
		&Scrape_Details($text,"276",$pdf_url);
		
						
	}	
}

sub Scrape_Details()
{
    my $html_content=shift;
    my $Council_Code=shift;  
    my $PDF_URL=shift;  
   
    my $Source = "GCS001";  
    my $COUNCIL_NAME = "Monmouthshire";  
   	
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';
	
	
	while($html_content=~m/<START>([\w\W]*?)<END>/gis)
	{
		my $AppContent = $1;
		
		
		# print "$AppContent"; <STDIN>;
		
		my $time = Time::Piece->new;
		my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
		
		my($Application,$Proposal,$Application_Status,$Decision_Status,$Date_Decision_Made); 
		
		if($AppContent=~m/\s*(DC\/\d{4}\/\d{5})\s*<\/text>/s)
		{
			$Application=&clean($1);
			print "Application=>$Application"; 
			# <STDIN>;
		}
		
		if($AppContent=~m/<text[^>]*?>\s*([^<]*?)\s*<\/text>\s*<text[^>]*?>[^<]*?<\/text>\s*<text[^>]*?>[^<]*?<\/text>\s*<text[^>]*?>(\d+\-[A-Za-z]+\-\d{4})\s*/s)
		{
			$Decision_Status=&clean($1);
			print "Decision_Status=>$Decision_Status"; 
			# <STDIN>;
		}
		
		if($AppContent=~m/<text[^>]*?>(\d+\-[A-Za-z]+\-\d{4})/s)
		{
			$Date_Decision_Made=&clean($1);
		}
		
		if($AppContent=~m/\s*DC\/\d{4}\/\d{5}\s*<\/text>\s*<text[^>]*?>\s*([^<]*?)\s*<\/text>\s*<text[^>]*?>\s*([a-z]+[^<]*?)\s*<\/text>\s*<text[^>]*?>\s*([a-z]+[^<]*?)\s*<\/text>\s*<text[^>]*?>\s*([a-z]+[^<]*?)\s*<\/text>/s)
		{
			my $pro1 = &clean($1);
			my $pro2 = &clean($2);
			my $pro3 = &clean($3);
			my $pro4 = &clean($4);
			$Proposal=$pro1." ".$pro2." ".$pro3." ".$pro4;
		}
		
		if($Proposal eq "")
		{
			if($AppContent=~m/\s*DC\/\d{4}\/\d{5}\s*<\/text>\s*<text[^>]*?>\s*([^<]*?)\s*<\/text>\s*<text[^>]*?>\s*([a-z]+[^<]*?)\s*<\/text>\s*<text[^>]*?>\s*([a-z]+[^<]*?)\s*<\/text>/s)
			{
				my $pro1 = &clean($1);
				my $pro2 = &clean($2);
				my $pro3 = &clean($3);
				$Proposal=$pro1." ".$pro2." ".$pro3;
			}
		}
		
		if($Proposal eq "")
		{
			if($AppContent=~m/\s*DC\/\d{4}\/\d{5}\s*<\/text>\s*<text[^>]*?>\s*([^<]*?)\s*<\/text>\s*<text[^>]*?>\s*([a-z]+[^<]*?)\s*<\/text>/s)
			{
				my $pro1 = &clean($1);
				my $pro2 = &clean($2);
				$Proposal=$pro1." ".$pro2;
			}
		}
		
		if($Proposal eq "")
		{
			if($AppContent=~m/\s*DC\/\d{4}\/\d{5}\s*<\/text>\s*<text[^>]*?>\s*([^<]*?)\s*<\/text>/s)
			{
				my $pro1 = &clean($1);
				$Proposal=$pro1;
			}
		}
		
		my $Application_Link=$PDF_URL;
		
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$insert_query.= "(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'), ";
		
		
		undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 	
		
	}

    $insert_query=~s/\,\s*$//igs;    
		
    print "insert_query::$insert_query\n"; 
	# <STDIN>;
	
	# open FH, ">Query.txt";
	# print FH $insert_query;
	# close FH;
	# exit;
	
	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
	
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}



###### DB Connection ####
sub DbConnection()
{
    my $dsn                          =  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
    my $dbh                          =    DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
   
    if(!$dbh)
    {
        &DBIconnect($dsn);
    }
    else
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}

#####GET Content####
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(GET=>$url);	
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;	
	
	my $Content_Disposition=$res->header("Content-Disposition");

	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}	