use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use File::Copy;
use FileHandle;

# Establish connection with DB server
my $dbh = &DbConnection();
my $Council_Code = '163';




# Location    
my $pgm_path="C:/Glenigan/Live_Schedule/Decision/Copeland/Program";
$pgm_path=~s/\s+$//igs;
my $archive_path=$pgm_path;
$archive_path=~s/Program$/Archive/igs;

# Cookies decleration
my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);

# User Agent decleration
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect('7');


# Write Cookies
my $file=$0."_cookie.txt";
my $cookie=HTTP::Cookies->new(file=>$file,autosave=>1,);
$ua->cookie_jar($cookie);

# Add Proxy
BEGIN {
 $ENV{HTTPS_PROXY} = 'https://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

# Get Home Page Content
my $spefication_url="https://www.copeland.gov.uk/section/planning-applications-determined";
my $content=&Getcontent($spefication_url,"GET","","");

# open(DIR, ">Home_Content.html");
# print DIR "$content\n";
# close(DIR); 
# exit;

#Get Current Year
my $time = Time::Piece->new;
my $current_year= $time->strftime('%Y');

# Get current year Decision report
if($content=~m/<h2>\s*$current_year\s*<\/h2>\s*([\w\W]*?)\s*<\/p>\s*<\/div>\s*<\/div>\s*<\/div>/is)
{
	my $tableArea = $1;
	while($tableArea=~m/<a[^<]*?href\=\"([^<]*?)\"[^>]*?>\s*([^<]*?)\s*</igs)
	{
		my $monthURL = $1;
		my $monthName = $2;
		my $content1=&Getcontent($monthURL,"GET","","");
		if($content1=~m/<a[^<]*?href\=\"([^<]*?\.pdf)\"[^>]*?>\s*([^<]*?)\s*</is)
		{
			my $url=$1;
			my $file_name=$2;
			$file_name=~s/\s+//igs;
			my $pdm_name=$file_name;		
			
			print "$file_name\n";
					
			opendir(DIR, $archive_path) || die "can't opendir $archive_path: $!";
			my @files = grep { (!/^\./) && -f "$archive_path/$_" } 
			readdir(DIR);
			closedir DIR;
			
			next if($pdm_name~~@files);	
		
			my $pdf_url=$url;
			print "\n$pdf_url\n";
			
			my $pdf_content=&Getcontent($pdf_url,"GET","","");
			
			# open(DIR, ">second_url_content.html");
			# print DIR "$second_url_content\n";
			# close(DIR); 
			# exit;
			
			my $temp_file = $pgm_path.'\\'.$file_name;
			$file_name=$pgm_path.'\\'.$file_name;
				
			my $fh = FileHandle->new("$temp_file", 'w') or die "Cannot open $file_name for write :$!";
			binmode($fh);
			$fh->print($pdf_content);
			$fh->close();
			
			my $copy_res=copy("$temp_file",$archive_path);
			
			if($copy_res ne "")
			{
				# print "$copy_res\n";
				# <STDIN>;
				print "Downloaded file copied into Archive folder\n";#<STDIN>;
			}
			
			print "Download URL :: $file_name \n";#<STDIN>;
			
			
			my $deillustrate_name="C:/Glenigan/Live_Schedule/Decision/Copeland/deillustrate.pl";
			system("perl $deillustrate_name $temp_file $file_name ");
			
			my $exe_name="C:/Glenigan/Live_Schedule/Decision/Copeland/pdftotext.exe -raw";
			system("$exe_name $file_name");
			
			my $html_file_pdf=$file_name;
			$html_file_pdf=~s/\.pdf\s*$/\.txt/igs;
			
			print "html_file_pdf :: $html_file_pdf\n";
			
			
			&Scrape_Details($html_file_pdf,$Council_Code,$pdf_url);
		}		
	}	
}

sub Scrape_Details()
{
    my $html_content=shift;
    my $Council_Code=shift;  
    my $PDF_URL=shift;  
	
    my $time = Time::Piece->new;
    my $ScheduleDateTime= $time->strftime('%Y/%m/%d %H:%M');
    my $Source = "GCS001";  
    my $COUNCIL_NAME = "Copeland";  
    my $Source_With_Time=$Source."_".$ScheduleDateTime."-perl";  
   		
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

    open IN,"$html_content";
	my $App_content;
	while(<IN>)
	{
		$App_content.=$_;
	}
	
	$App_content=~s/^(.*?)$/$1<END>/gsi;	
	$App_content=~s/(Application\s+Num\s+)/<END>$1/gsi;
	
    my ($Application,$Decision_Date,$Decision,$Proposal,$Application_Status,$Application_Link);
	
    while($App_content=~m/(Num\s+[\w\W]*?<END>)/igs)
	{
		my $app_value=$1;
		# print "app_value::$app_value\n"; <STDIN>;
		
		if($app_value=~m/Num\s+([^>]*?)\s*Applicant/is)
		{
			$Application=$1;
			# $Application=trim($Application);
		}
		if($app_value=~m/Proposal\s*([^>]*?)\s*Decision/is)
		{
			$Proposal=$1;
			# $Proposal=trim($Proposal);
		}
		if($app_value=~m/Decision\s*([^>]*?)\s*Decision\s+Date/is)
		{
			$Decision=$1;
			# $Decision=trim($Decision);
		}
		if($app_value=~m/Decision\s+Date\s*([^<]*?)\s*\n/is)
		{
			$Decision_Date=$1;
			# $Decision_Date=trim($Decision_Date);
		}
		
		my $time 			= Time::Piece->new;
		my $Schedule_Date 	= $time->strftime('%m/%d/%Y %H:%M');
		
		$Application_Link 	= $PDF_URL;
		$Decision_Date		=~s/\'/\'\'/igs;
		$Proposal			=~s/\'/\'\'/igs;
		$Application		=~s/\'/\'\'/igs;
		$Application_Status	=~s/\'/\'\'/igs;
		$Decision			=~s/\'/\'\'/igs;
		$Application_Link	=~s/\'/\'\'/igs;
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision\', \'$Decision_Date\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
		
		# print "insert_query::$insert_query\n";
		
		undef $Application;  undef $Proposal; undef $Decision_Date; undef $Application_Status; undef $Decision; 
	}
	
	close(IN);
	
    $insert_query=~s/\,$//igs;
    
    print "insert_query::$insert_query\n";

    if($insert_query!~m/values\s*$/is)
    {
        &DB_Insert($dbh,$insert_query);
    }	 
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}



###### DB Connection ####
sub DbConnection()
{
    my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}

#####GET Content####
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;	
	my $Content_Disposition=$res->header("Content-Disposition");
	
	my $content;
	if($code=~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		if($rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if($rerun_count<= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}	


# SUBROUTINE TO GET THE POST HTML CONTENT
sub Postcontent()
{
	my $post_url=shift;
	my $Post_Content=shift;
	my $Host=shift;
	my $Referer=shift;	
	my $rerun_count=0;
	
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "$Host");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
	$req->header("Referer"=> "$Referer");
	$req->content($Post_Content);
	
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;
	my $Content_Disposition=$res->header("Content-Disposition");
	
	my ($content,$redir_url);
	if($code=~m/20/is)
	{
		$content = $res->decoded_content;
	}
	elsif($code=~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			# $Content_Disposition=$u2;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if($rerun_count <= 1)
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}