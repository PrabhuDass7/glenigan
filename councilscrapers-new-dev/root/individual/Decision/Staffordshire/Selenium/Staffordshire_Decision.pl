use strict;
use DBI;
use DBD::ODBC;
use WWW::Mechanize;
use HTTP::Cookies;
use HTML::Entities;
use IO::Socket::SSL;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;
use Win32; 
use Glenigan_DB_Windows;



# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

my $COUNCIL_NAME = "Staffordshire";


if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"626\" \"GCS001\"\)", 16, "Error message");
    exit();
}




chomp($Date_Range);

my ($input_from_date, $input_to_date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $input_from_date\n";
print "To_Date: $input_to_date\n";


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


############
my $mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
$mech->proxy(['http','https'], 'http://172.27.137.199:3128');


my $Applicant_Flag = 'Y';


#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();


my $home_url = "https://apps2.staffordshire.gov.uk/SCC/CPLand/Search.aspx";
$driver->get("$home_url");
sleep(15);
my $model_cont=$driver->get_page_source($driver);


$driver->find_element_by_xpath("//*[\@id=\"ctl00_ContentPlaceHolder1_pcSearchControls_T1T\"]/span")->click;
sleep(5);	
my $temp1=$driver->get_page_source($driver);

print "FromDate:$input_from_date\n";
print "ToDate:$input_to_date\n";

$driver->find_element("//*[\@id=\"ctl00_ContentPlaceHolder1_pcSearchControls_dteDecisionDateFrom_I\"]")->send_keys("$input_from_date");
$driver->find_element("//*[\@id=\"ctl00_ContentPlaceHolder1_pcSearchControls_dteDecisionDateTo_I\"]")->send_keys("$input_to_date");
$driver->find_element_by_xpath("//*[\@id=\"ctl00_ContentPlaceHolder1_pcSearchControls_btnSubmitAdvancedSearch_CD\"]/span")->click;
sleep(5);
$driver->find_element("//*[\@id=\"ctl00_ContentPlaceHolder1_cbResultsPerPage_I\"]")->click;
$driver->find_element("//*[\@id=\"ctl00_ContentPlaceHolder1_cbResultsPerPage_DDD_L_LBI5T0\"]")->click;
sleep(3);
my $model_content=$driver->get_page_source($driver);

# open FH, ">Home.html";
# print FH $model_content;
# close FH;
# exit;


my $total_Pagecount = $1 if($model_content=~m/<strong>Page\s*1\s*of\s*(\d+)\s*<\/strong>/si);
my $total_Appcount = $1 if($model_content=~m/<strong>Total\s*matches\s*for\s*search\:\s*(\d+)\s*<\/strong>/si);

print "\n\nTotal number of Application::$total_Appcount\n\n";
print "\n\nTotal page size::$total_Pagecount\n\n";


my @fullApplLink;
if(($total_Pagecount ne "") && ($total_Pagecount >= 2))
{	
	my $ApplLink = &Scrape_Applinks($model_content);
	my @ApplLink=@$ApplLink;
	push(@fullApplLink,@ApplLink);
	
	
	my $a=2;
	next_page:
	if($a<=$total_Pagecount)
	{	
		print "${a}::::$total_Pagecount\n";
		
		$driver->find_element("//*[\@id=\"ctl00_ContentPlaceHolder1_imgButtonNext\"]")->click;
		sleep(5);
		my $nextPageContent=$driver->get_page_source($driver);

		# open FH, ">Home.html";
		# print FH $model_content;
		# close FH;
		# exit;
		
		my $ApplLink = &Scrape_Applinks($nextPageContent);
		my @ApplLink=@$ApplLink;
		push(@fullApplLink,@ApplLink);
		
		$a++;
		goto next_page;
		
	}
}
else
{
	my $ApplLink = &Scrape_Applinks($model_content);
	my @ApplLink=@$ApplLink;
	push(@fullApplLink,@ApplLink);
}

&Scrape_Details(\@fullApplLink);	
	
	
sub Scrape_Applinks()
{	
	my $content = shift;
	
	my @applinkArray;
	while($content=~m/>\s*Application\s*No\:\s*([^>]*?)\s+[^<]*?<\/p>\s*<a\s*href=\"([^\"]*?)\">/gsi)
	{
		my $Application = $1;
		my $ApplicationURL = $2;
		
		my $applicationLinkUri = URI::URL->new($ApplicationURL)->abs( $home_url, 1 );
		print "\n\nApplication Link::$applicationLinkUri\n\n";
		
		push(@applinkArray,$applicationLinkUri);
	}	
	
	return(\@applinkArray);
}		
	


sub Scrape_Details()
{
	my $Appl_link = shift;
	my @Appl_link = @{$Appl_link};
		
	print "Council_Code: $Council_Code\n";
	my $insert_query="insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values ";
	
	foreach my $Appl_links (@Appl_link)
	{
	
		$mech->get($Appl_links);			
		my $App_Page_Content = $mech->content;
		
		# open(PPP,">PageContent$s.html");
		# print PPP "$App_Page_Content\n";
		# close(PPP);
		# exit;
		
		
		my $time = Time::Piece->new;
		my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
		
		
		
		my $Application_Status			= &clean($1) if($App_Page_Content=~m/<td[^>]*?>\s*Status\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is);
		my $Proposal					= &clean($1) if($App_Page_Content=~m/<td[^>]*?>\s*Proposal\s*<\/td>\s*<td[^>]*?>\s*(.*?)\s*<\/td>/is);
		my $Application					= &clean($1) if($App_Page_Content=~m/<td[^>]*?>\s*Application\s*No\.\s*<\/td>\s*<td[^>]*?>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*<\/td>/is);
		my $Date_Decision_Made			= &clean($1) if($App_Page_Content=~m/<td[^>]*?>\s*Decision\s*Date\s*<\/td>\s*<td[^>]*?>\s*(.*?)\s*<\/td>/is);
		my $Decision_Status				= &clean($1) if($App_Page_Content=~m/<td[^>]*?>\s*Delegated\s*Decision\s*<\/td>\s*<td[^>]*?>\s*(.*?)\s*<\/td>/is);
		
		my $Date_Decision_Dispatched;
		my $Application_Link=$Appl_links;
		
		print "\nApplication Number is Processing::$Application\n";
		
		# $Address=~s/\'/\'\'/gsi;	
		my $Source = $Date_Range;	
	
		
		my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Date_Decision_Dispatched\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\'),";
		
		undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Date_Decision_Made;  undef $Date_Decision_Dispatched;  undef $Application_Link; undef $Application_Status;
		
	}
	$insert_query=~s/\,\s*$//igs;
	
	# print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	}
}
		
			
			

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}