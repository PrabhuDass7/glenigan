use strict;
use DBI;
use DBD::ODBC;
use WWW::Mechanize;
use HTTP::Cookies;
use HTML::Entities;
use IO::Socket::SSL;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;

# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();

my $cdate = localtime->strftime('%m/%d/%Y');
$cdate=~s/\//\%2F/gsi;

print "hour: $hour\n";
print "CurrentDate: $cdate\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

my $COUNCIL_NAME = "Staffordshire";


if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"626\" \"GCS001\"\)", 16, "Error message");
    exit();
}




chomp($Date_Range);

my ($input_from_date, $input_to_date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $input_from_date\n";
print "To_Date: $input_to_date\n";


my $temp_fdate;
my $temp_tdate;
if($input_from_date=~m/^(\d+)\/(\d+)\/(\d+)$/is)
{
	$temp_fdate	= $2."\/".$1."\/".$3;
}
if($input_to_date=~m/^(\d+)\/(\d+)\/(\d+)$/is)
{
	$temp_tdate	= $2."\/".$1."\/".$3;
}

print "input_from_date==>$input_from_date\n";	
print "input_to_date==>$input_to_date\n";	
print "temp_fdate==>$temp_fdate\n";	
print "temp_tdate==>$temp_tdate\n";	


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=SCREENSCRAPPER;uid=User2;pwd=Merit456';
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	Reconnect1:
	if($dbh  = DBI->connect("DBI:ODBC:$dsn"))
	{
		print "\nDB SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect1;
		}	
	}
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}


############
my $mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');
$mech->proxy(['http','https'], 'http://172.27.137.192:3128');

my $home_url = "https://apps2.staffordshire.gov.uk/SCC/CPLand/Search.aspx";

$mech->get($home_url);
my $responseCode = $mech->status;
print "responseCode==>$responseCode\n";	
my $list_content = $mech->content;

# open(DIR, ">staffordshire.html");
# print DIR "$list_content\n";
# close(DIR); 
# exit;


my ($eventvalid,$viewstate,$viewstategen);

if($list_content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATE[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$viewstate=uri_escape($1);
}
if($list_content=~m/<input\s*[^<]*?id=[\"\']__VIEWSTATEGENERATOR[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$viewstategen=uri_escape($1);
}
if($list_content=~m/<input\s*[^<]*?id=[\"\']__EVENTVALIDATION[\"\']\s*value=[\"\']([^<]*?)[\"\']/is)
{
	$eventvalid=uri_escape($1);
}

my $dt1 = Time::Piece->strptime($input_from_date, '%d/%m/%Y');
my $dateMilliFrom = 1000 * $dt1->epoch;

my $dt2 = Time::Piece->strptime($input_to_date, '%d/%m/%Y');
my $dateMilliTo = 1000 * $dt2->epoch;

print "dateMilliFrom==>$dateMilliFrom\n"; 
print "dateMilliTo==>$dateMilliTo\n";


$temp_fdate=~s/\//\%2F/gsi;
$input_from_date=~s/\//\%2F/gsi;
$temp_tdate=~s/\//\%2F/gsi;
$input_to_date=~s/\//\%2F/gsi;

my $postContent = "ctl00%24ContentPlaceHolder1%24sm=ctl00%24ContentPlaceHolder1%24upnlSearch%7Cctl00%24ContentPlaceHolder1%24pcSearchControls%24btnSubmitAdvancedSearch&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$viewstate&__VIEWSTATEGENERATOR=$viewstategen&__EVENTVALIDATION=$eventvalid&ctl00%24ContentPlaceHolder1%24pcSearchControls=%7B%26quot%3BactiveTabIndex%26quot%3B%3A1%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24txtSearchText=&ctl00%24ContentPlaceHolder1%24pcSearchControls%24txtApplicationNo=&ctl00%24ContentPlaceHolder1%24pcSearchControls%24txtPostCode=&ctl00_ContentPlaceHolder1_pcSearchControls_cbDistanceFromPostCode_VI=1&ctl00%24ContentPlaceHolder1%24pcSearchControls%24cbDistanceFromPostCode=1km%20from%20Post%20Code&ctl00%24ContentPlaceHolder1%24pcSearchControls%24cbDistanceFromPostCode%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24cbDistanceFromPostCode%24DDD%24L=1&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateFrom%24State=%7B%26quot%3BrawValue%26quot%3B%3A%26quot%3BN%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateFrom=&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateFrom%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateFrom%24DDD%24C=%7B%26quot%3BvisibleDate%26quot%3B%3A%26quot%3B$cdate%26quot%3B%2C%26quot%3BselectedDates%26quot%3B%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateFrom%24DDD%24C%24FNPState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateTo%24State=%7B%26quot%3BrawValue%26quot%3B%3A%26quot%3BN%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateTo=&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateTo%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateTo%24DDD%24C=%7B%26quot%3BvisibleDate%26quot%3B%3A%26quot%3B$cdate%26quot%3B%2C%26quot%3BselectedDates%26quot%3B%3A%5B%5D%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteReceivedDateTo%24DDD%24C%24FNPState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateFrom%24State=%7B%26quot%3BrawValue%26quot%3B%3A%26quot%3B$dateMilliFrom%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateFrom=$input_from_date&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateFrom%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A137%3A414%3A1%3A%3A%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateFrom%24DDD%24C=%7B%26quot%3BvisibleDate%26quot%3B%3A%26quot%3B$cdate%26quot%3B%2C%26quot%3BselectedDates%26quot%3B%3A%5B%26quot%3B$temp_fdate%26quot%3B%5D%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateFrom%24DDD%24C%24FNPState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateTo%24State=%7B%26quot%3BrawValue%26quot%3B%3A%26quot%3B$dateMilliTo%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateTo=$input_to_date&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateTo%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A422%3A414%3A1%3A%3A%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateTo%24DDD%24C=%7B%26quot%3BvisibleDate%26quot%3B%3A%26quot%3B$cdate%26quot%3B%2C%26quot%3BselectedDates%26quot%3B%3A%5B%26quot%3B$temp_tdate%26quot%3B%5D%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24dteDecisionDateTo%24DDD%24C%24FNPState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00_ContentPlaceHolder1_pcSearchControls_cbCategory_VI=&ctl00%24ContentPlaceHolder1%24pcSearchControls%24cbCategory=All&ctl00%24ContentPlaceHolder1%24pcSearchControls%24cbCategory%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24pcSearchControls%24cbCategory%24DDD%24L=&ctl00%24ContentPlaceHolder1%24hfSearchType=&DXScript=1_247%2C1_138%2C1_241%2C1_164%2C1_141%2C1_135%2C1_239%2C1_242%2C1_162%2C1_170%2C1_151%2C1_143%2C1_176%2C1_161%2C1_233%2C1_159%2C1_231%2C1_165%2C1_160%2C1_152&DXCss=1_23%2C1_28%2C1_29%2C1_3%2C1_10%2C1_8%2C1_9%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F100-main.css%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F110-menu.css%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F800-Print.css%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F910-WebApps.css%2C%2F%2Fwww.staffordshire.gov.uk%2Faspnet_client%2FContensisThemes%2Fsimple%2FCommon%2Fstyles.css%2C%2F%2Fwww.staffordshire.gov.uk%2Faspnet_client%2FContensisThemes%2Fsimple%2FComments%2Fstyles.css%2C%2F%2Fmaxcdn.bootstrapcdn.com%2Ffont-awesome%2F4.2.0%2Fcss%2Ffont-awesome.min.css%2C%2F%2Ffonts.googleapis.com%2Fcss%3Ffamily%3DSource%2BSans%2BPro%3A400%2C600%2C700%2Chttps%3A%2F%2Fdfsrovckda8bt.cloudfront.net%2F%2Fcss%2Fmodal2%2Fmodal.css%3Fv%3D2.2.0%2C%2F%2Fwsstatic.govmetric.com%2Fcss%2Fclient%2Fgm_sidebar.css&__ASYNCPOST=true&ctl00%24ContentPlaceHolder1%24pcSearchControls%24btnSubmitAdvancedSearch=Search";


# print "$postContent\n";

$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36');
$mech->add_header( 'Accept' => '*/*');
$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
$mech->add_header( 'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate, br');
$mech->add_header( 'Host' => 'apps2.staffordshire.gov.uk');
$mech->add_header( 'Origin' => 'https://apps2.staffordshire.gov.uk');
$mech->add_header( 'Referer' => 'https://apps2.staffordshire.gov.uk/SCC/CPLand/Search.aspx');
$mech->add_header( 'Connection' => 'keep-alive');
	
$mech->post('https://apps2.staffordshire.gov.uk/SCC/CPLand/Search.aspx', Content => "$postContent");
my $postCode = $mech->status;
print "PostresponseCode==>$postCode\n";	
my $content = $mech->content;

# open(DIR, ">staffordshire.html");
# print DIR "$content\n";
# close(DIR); 
# exit;



my $insert_query='insert into glenigan..IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

my $Page_Number=2;
NextPage:
my $appCnt = $1 if($content=~m/<div\s*id=\"ctl00_ContentPlaceHolder1_pnlResultRows\">\s*([\w\W]*?)\s*<\/div>\s*<\/div>\s*<\/div>/is);

while($appCnt=~m/Application\s*No\:\s*([^\|]*?)\s*\|[\w\W]*?<a\s*href=\"(Details\.aspx\?applicationID=[^\"]*?)\">/igs)
{
	my $Application_Num=$1;
	my $Application_URL=$2;
	print "Application_Num==>$Application_Num\n";
	
	my $appLink;
	if($Application_URL!~m/^http/is)
	{
		$appLink = "https://apps2.staffordshire.gov.uk/SCC/CPLand/".$Application_URL;
	}
	print "Application_URL=>$appLink\n";
	
	$mech->get($appLink);
	my $App_Content = $mech->content;		

	# open(DC,">App_content.html");
	# print DC $App_Content;
	# close DC;
	# exit;
	
	my ($Application, $Application_Status, $Proposal,$Date_Decision_Made,$Decision_Status);
	
	$Application				= &clean($1) if($App_Content=~m/<td[^>]*?>\s*Application\s*No\.\s*<\/td>\s*<td[^>]*?>\s*<strong>\s*([^<]*?)\s*<\/strong>\s*<\/td>/is);
	$Application_Status			= &clean($1) if($App_Content=~m/<td[^>]*?>\s*Status\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is);
	$Proposal					= &clean($1) if($App_Content=~m/<td[^>]*?>\s*Proposal\s*<\/td>\s*<td[^>]*?>\s*(.*?)\s*<\/td>\s*<\/tr>/is);
	$Date_Decision_Made			= &clean($1) if($App_Content=~m/<td[^>]*?>\s*Decision\s*Date\s*<\/td>\s*<td[^>]*?>\s*(.*?)\s*<\/td>/is);
	$Decision_Status			= &clean($1) if($App_Content=~m/<td[^>]*?>\s*Delegated\s*Decision\s*<\/td>\s*<td[^>]*?>\s*(.*?)\s*<\/td>/is);
		
	
	my $COUNCIL_NAME = 'South Derbyshire';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	# my $Source = "GCS001";	
	my $Source = $Date_Range;	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$appLink\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	undef($Application);  undef($Proposal); undef($Application_Status); undef($Decision_Status); undef($Date_Decision_Made);  undef($appLink);
	
}


if($content=~m/<div[^>]*?>\s*<strong>Page(?:\&nbsp\;|\s+)?\d+(?:\&nbsp\;|\s+)?of(?:\&nbsp\;|\s+)?(\d+)\s*<\/strong>\s*<\/div>/is)
{
	my $totalPage = $1;
	print "Total page::$totalPage\n";
	if($Page_Number <= $totalPage)
	{	
		if($content=~m/<input[^>]*?name=\"ctl00\$ContentPlaceHolder1\$imgButtonNext\"[^>]*?alt=\"Next\s*Button\"/is)
		{	
			my ($ViewState,$ViewstateGenerator,$EventValidation,$x,$y);
			
			if($content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__ViewState\s*"\s*value="([^>]*?)"\s*\/>/is)
			{
				$ViewState=uri_escape($2);
				
			}
			if($ViewState eq "")
			{
				if($content=~m/__VIEWSTATE\|([^\|]*?)\|/is)
				{
					$ViewState=uri_escape($1);
				}
			}	
			if($content=~m/__VIEWSTATE\|[^\|]*?\|(\d+)\|/is)
			{
				$x=$1;
			}
			if($content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^>]*?)"\s*\/>/is)
			{
				$ViewstateGenerator=uri_escape($2);
			}
			if($ViewstateGenerator eq "")
			{
				if($content=~m/__VIEWSTATEGENERATOR\|([^\|]*?)\|/is)
				{
					$ViewstateGenerator=uri_escape($1);
				}
			}
			if($content=~m/<input[^>]*?name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
			{
				$EventValidation=uri_escape($2);
			}	
			if($EventValidation eq "")
			{
				if($content=~m/__EVENTVALIDATION\|([^\|]*?)\|/is)
				{
					$EventValidation=uri_escape($1);
				}
			}			
			if($content=~m/\|(\d+)\|formAction\|\|\.\/Search\.aspx\|/is)
			{
				$y = $1;
			}
			
			
			my $nextPagePostCont = "ctl00%24ContentPlaceHolder1%24sm=ctl00%24ContentPlaceHolder1%24upnlSearch%7Cctl00%24ContentPlaceHolder1%24imgButtonNext&ctl00_ContentPlaceHolder1_cbStatus_VI=All&ctl00%24ContentPlaceHolder1%24cbStatus=All&ctl00%24ContentPlaceHolder1%24cbStatus%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24cbStatus%24DDD%24L=All&ctl00_ContentPlaceHolder1_cbSortField_VI=pla_received&ctl00%24ContentPlaceHolder1%24cbSortField=Date%20Received&ctl00%24ContentPlaceHolder1%24cbSortField%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24cbSortField%24DDD%24L=pla_received&ctl00_ContentPlaceHolder1_cbSortDirection_VI=desc&ctl00%24ContentPlaceHolder1%24cbSortDirection=Most%20recent%20first&ctl00%24ContentPlaceHolder1%24cbSortDirection%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24cbSortDirection%24DDD%24L%24State=%7B%26quot%3BCustomCallback%26quot%3B%3A%26quot%3B%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24cbSortDirection%24DDD%24L=desc&ctl00_ContentPlaceHolder1_cbResultsPerPage_VI=5&ctl00%24ContentPlaceHolder1%24cbResultsPerPage=5&ctl00%24ContentPlaceHolder1%24cbResultsPerPage%24DDDState=%7B%26quot%3BwindowsState%26quot%3B%3A%26quot%3B0%3A0%3A-1%3A0%3A0%3A0%3A-10000%3A-10000%3A1%3A0%3A0%3A0%26quot%3B%7D&ctl00%24ContentPlaceHolder1%24cbResultsPerPage%24DDD%24L=5&DXScript=1_247%2C1_138%2C1_241%2C1_164%2C1_141%2C1_135%2C1_239%2C1_242%2C1_162%2C1_170%2C1_151%2C1_143%2C1_176%2C1_161%2C1_233%2C1_159%2C1_231%2C1_165%2C1_160%2C1_152%2C1_223&DXCss=1_23%2C1_28%2C1_29%2C1_3%2C1_10%2C1_8%2C1_9%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F100-main.css%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F110-menu.css%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F800-Print.css%2C%2F%2Fwww.staffordshire.gov.uk%2FSiteElements%2FStyles%2F910-WebApps.css%2C%2F%2Fwww.staffordshire.gov.uk%2Faspnet_client%2FContensisThemes%2Fsimple%2FCommon%2Fstyles.css%2C%2F%2Fwww.staffordshire.gov.uk%2Faspnet_client%2FContensisThemes%2Fsimple%2FComments%2Fstyles.css%2C%2F%2Fmaxcdn.bootstrapcdn.com%2Ffont-awesome%2F4.2.0%2Fcss%2Ffont-awesome.min.css%2C%2F%2Ffonts.googleapis.com%2Fcss%3Ffamily%3DSource%2BSans%2BPro%3A400%2C600%2C700%2Cchrome-extension%3A%2F%2Fkcehcblfpidimbihdfophhhdejckolgh%2Fstyles%2Fstyles.css%2Chttps%3A%2F%2Fdfsrovckda8bt.cloudfront.net%2F%2Fcss%2Fmodal2%2Fmodal.css%3Fv%3D2.2.0%2C%2F%2Fwsstatic.govmetric.com%2Fcss%2Fclient%2Fgm_sidebar.css&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$ViewState&__VIEWSTATEGENERATOR=$ViewstateGenerator&__EVENTVALIDATION=$EventValidation&__ASYNCPOST=true&ctl00%24ContentPlaceHolder1%24imgButtonNext.x=$x&ctl00%24ContentPlaceHolder1%24imgButtonNext.y=$y";
			
			# print "$nextPagePostCont\n"; <STDIN>;
			
			
			$mech->post('https://apps2.staffordshire.gov.uk/SCC/CPLand/Search.aspx', Content => "$nextPagePostCont");					
			my $content2 = $mech->content;
			$content = $content2;
			print "Current Page_Number: $Page_Number\n";
			
			open(DC,">SearchResultContent.html");
			print DC $content;
			close DC;
			# exit;

			
			$Page_Number++;
			
			
			goto NextPage;
		}
	}
}

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
# print "insert_query: $insert_query\n";

sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}