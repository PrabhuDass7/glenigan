# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Decision/Colchester_Decision'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def collectweeklyrls(driver,home_url):
	try:
		#-- Parse
		
		driver.get(home_url)
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		# with open(folder_path+"/160_out.html", 'wb') as fd:
			# fd.write(content)
		# sys.exit()	
		
		weeklyURLs=[]		
		try:
			urlContent=re.findall(r'<div\sclass=\"weekly-link-area\">\s*([\w\W]*?)\s*<\/div>\s*<\/main>', str(content), re.IGNORECASE)
			if re.findall(r'<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*(?:<p>)?[^<]*?\s*(?:<\/p>)?<\/a>', str(urlContent)):
				regionUrlContent=re.findall(r'<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*(?:<p>)?[^<]*?\s*(?:<\/p>)?<\/a>', str(urlContent), re.IGNORECASE) 
				
				for urlMatch in regionUrlContent:
					if not re.findall(r'^http', str(urlMatch)):
						absoluteRegionURL = urljoin(home_url,urlMatch)
						weeklyURLs.append(absoluteRegionURL)
					else:
						weeklyURLs.append(urlMatch)
						
		except Exception as e:
			print e,sys.exc_traceback.tb_lineno 	
		
		return weeklyURLs

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno
		

# To collect Application URLs here
def collectAppURL(weekcontent, home_url):	
	try:
		appURLCollections=[]
		appListContent=re.findall(r'<div\sclass=\"view-grid\shas-pagination\">\s*<table[^>]*?>([\w\W]*?)\s*<\/table>\s*<\/div>', str(weekcontent), re.IGNORECASE)	
		if re.findall(r'<td[^>]*?data-th=\"Application\sNumber\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?title=\"View\sdetails\">\s*[^<]*?\s*<\/a>\s*<\/td>', str(appListContent), re.IGNORECASE):
			appUrls=re.findall(r'<td[^>]*?data-th=\"Application\sNumber\"[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?title=\"View\sdetails\">\s*[^<]*?\s*<\/a>\s*<\/td>', str(appListContent), re.IGNORECASE) 
			
			for appUrl in appUrls:
				# print("appUrl:::",appUrl)
				if not re.findall(r'^http', str(appUrl)):
					absoluteRegionURL = urljoin(home_url,appUrl)
					appURLCollections.append(absoluteRegionURL)
				else:
					appURLCollections.append(appUrl)
			
		return appURLCollections				
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 
		

# Application parse section	
def detailCollection(appURLsList, CouncilCode, Source, conn, cursor, driver, home_url):
	try:
		appCount = 0
		insertQuery = 'insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values '
		bulkValuesForQuery=''
		
		for appURL in appURLsList:
			print("ApplicationURL::", appURL)
			driver.get(appURL)
			time.sleep(5)
			appcontent=driver.page_source
			
			# with open(folder_path+"/160_appcontent.html", 'wb') as fd:
				# fd.write(appcontent)
			# sys.exit()	
			
			(Application,ProposalCnt,ApplicationStatus, DecisionStatus, DateDecisionMade, Date_Decision_Dispatched) = appParseSection(appcontent)
			
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			
			Council_Name = "Colchester"
			joinValues="("+"'"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(Date_Decision_Dispatched)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+Source_With_Time+"','"+str(ApplicationStatus)+"')"
			
			print "ApplicationValues: ", joinValues
			bulkValuesForQuery = insertQuery+joinValues
			try:
				cursor.execute(bulkValuesForQuery)
				conn.commit()
			except Exception as e:
				print ("Error",e)
			bulkValuesForQuery=''
			
			# if appCount == 0:
				# bulkValuesForQuery = insertQuery+joinValues
				# appCount += 1
			# elif appCount == 50:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# cursor.execute(bulkValuesForQuery)
				# conn.commit()
				# appCount=0
				# bulkValuesForQuery=''
			# else:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# appCount += 1		
						
		print "InsertQuery: ", bulkValuesForQuery

		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno




# Application parse section	
def parsesection(weeklyURLsList, CouncilCode, Source, conn, cursor, driver, home_url):
	try:
		finalinsertQuery=''
		for weekURL in weeklyURLsList:
			# weekURL = re.sub(r'id=0', 'id=1', str(weekURL))
			# if re.findall(r'id=0', str(weekURL), re.IGNORECASE):
				# print("Weekly URL:::",weekURL)
				# continue
			# else:
				# print("Weekly URL:::",weekURL)
			driver.get(weekURL)
			#-- Wait funtion
			time.sleep(10)
			weekcontent=driver.page_source
			
			# with open(folder_path+"/160_out.html", 'wb') as fd:
				# fd.write(str(weekcontent))
			# sys.exit()	
			
			pageNumber = 2
			pageNum = 3
			
			fullAppURLs=[]
			try:
				if re.findall(r'<div\s*class=\"view-pagination\"[^>]*?style=\"display\:\snone\;\">', str(weekcontent)):
					collectAppURLs = collectAppURL(str(weekcontent), home_url)
					finalinsertQuery = detailCollection(collectAppURLs, CouncilCode, Source, conn, cursor, driver, home_url)
					
					if (finalinsertQuery != ''):
						cursor.execute(finalinsertQuery)
						conn.commit()
						
				elif re.findall(r'<li[^>]*?>\s*<a\shref=\"\#\"[^>]*?aria-label=\"Next\spage\"[^>]*?>[^>]*?<\/a>\s*<\/li>', str(weekcontent)):
					collectAppURLs = collectAppURL(str(weekcontent), home_url)	
					fullAppURLs.extend(collectAppURLs)
					check_next_page = re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(weekcontent), re.I)
					# print(check_next_page)
					# raw_input()
					
					while (check_next_page):				
						
						if re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(weekcontent), re.I):
							
							nextPgeNum = driver.find_element_by_xpath('//*[@id="template-block"]/div/div[2]/div[2]/div[6]/div/ul/li['+str(pageNum)+']/a')	
							driver.execute_script("arguments[0].click();", nextPgeNum)
							# driver.execute_script("arguments[0].click();", driver.find_element_by_xpath('//*[@id="template-block"]/div/div[2]/div[2]/div[6]/div/ul/li['+str(pageNum)+']/a'))
							#-- Wait funtion
							time.sleep(15)  
							appNextPagecontent=driver.page_source
							
							# with open(folder_path+"/160_out2.html", 'wb') as fd:
								# fd.write(appNextPagecontent)
							# sys.exit()
							
							collectNextPageAppURLs = collectAppURL(str(appNextPagecontent), home_url)	
							fullAppURLs.extend(collectNextPageAppURLs)
							pageNumber = pageNumber + 1   
							pageNum = pageNum + 1   
							
							if re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I):
								check_next_page = re.findall(r'<li>\s*<a\shref=\"\#\"[^>]*?aria-label=\"page\s'+str(pageNumber)+'\"\sdata-page=\"'+str(pageNumber)+'\">\s*('+str(pageNumber)+'|\.\.\.)\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I)
						
								print "pageNumber:",pageNumber              
											 
								if check_next_page != "":
									continue
								else:
									break
						else:
							break
						
					finalinsertQuery = detailCollection(fullAppURLs, CouncilCode, Source, conn, cursor, driver, home_url)
					
					if (finalinsertQuery != ''):
						cursor.execute(finalinsertQuery)
						conn.commit()			
			except Exception as e:
				print ("Error:",e)
		return (finalinsertQuery)

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno


# Application details parse section	
def appParseSection(Appcontent):	
	try:
		Application_Number=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?aria-label=\"Application\sNumber\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		DtDecision=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_decision_date\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		new_decision_type=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_decision_type\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		Application_Status=re.findall(r'<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"new_application_status\"[^>]*?>', str(Appcontent), re.IGNORECASE)
		Proposal=re.findall(r'<textarea[^>]*?id=\"new_developmentdescription[^>]*?>\s*([\w\W]*?)\s*<\/textarea>', str(Appcontent), re.IGNORECASE)
		
		(Application, ProposalCnt, ApplicationStatus, Date_Decision_Dispatched, Decision, DateDecisionMade) = ('','','','','','')

		if len(Application_Number) > 0:
			Application=clean(Application_Number[0])

		if len(Proposal) > 0:
			ProposalCnt=clean(Proposal[0])

		if len(Application_Status) > 0:
			ApplicationStatus=clean(Application_Status[0])
				
		DtDecision = datetime.strptime(str(DtDecision[0]), "%Y-%m-%dT%H:%M:%S.0%fZ")
		DtDecision = str(DtDecision.strftime("%d/%m/%Y"))

		if len(new_decision_type) > 0:
			Decision=clean(new_decision_type[0])
			
		if len(DtDecision) > 0:
			DateDecisionMade=DtDecision
			
		return (Application,ProposalCnt,ApplicationStatus, Decision, DateDecisionMade, Date_Decision_Dispatched)

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
		
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
	
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()

	# PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	# chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	# chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	# browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Live_Schedule/Jar/chromedriver.exe')
	# browser = webdriver.Chrome( executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
	browser.maximize_window()
	
	home_url = 'https://www.colchester.gov.uk/decided-applications-last-6-months/'
	
	resultURLs = collectweeklyrls(browser,home_url)
	
	finalQuery = parsesection(resultURLs, councilcode, 'GCS001', conn, cursor, browser, home_url)
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()
	
	if (finalQuery != ''):
		cursor.execute(finalQuery)
		conn.commit()		
	
	browser.quit()