use strict;
# use Net::SSL;
use URI::Escape;
use URI::Encode;
use URI::URL;
use MIME::Base64;
use HTTP::Cookies;
use HTML::Entities;
use LWP::UserAgent;
# use Unicode::Escape;
use DBI;
use DBD::ODBC;
use Time::Piece;


my $time = Time::Piece->new;
my $currentDate = $time->strftime('%y%m%d');       # 140109

print "currentDate=>$currentDate\n";

# Establish connection with DB server
my $dbh = &DbConnection();

my $COUNCIL_NAME = "Colchester";

my $uri_obj = URI::Encode->new( { encode_reserved => 0 } );

my $ua = LWP::UserAgent->new( ssl_opts =>{ verify_hostname => 0 },keep_alive => 1,show_progress => 1);
$ua->agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36");
$ua->timeout(30); 
$ua->max_redirect();

my $cookie = HTTP::Cookies->new(); 
$ua->cookie_jar(); 


my $home_url = 'https://www.colchester.gov.uk/decided-applications-last-6-months/';
my ($home_content,$php_session_id, $RequestVerificationTokencookie, $ARRAffinity, $Dynamics365PortalAnalytics)=&Lwp_Get($home_url);

my @weeklistURLs;
for(my $i = 0; $i < 26; $i++)
{
	my $weekurl = $1 if($home_content=~m/<a\s*href\=\"([^<]*?)\">\s*<p>\s*Applications\s*Received/is);
	$weekurl=~s/\'\s*\+\s*i\s*\+\s*\'/$i/is;
	push(@weeklistURLs, $weekurl)
}
# print (@weeklistURLs);
# exit;

foreach my $url(@weeklistURLs)
{
	my ($content1,$php_session_id1, $RequestVerificationTokencookie1, $ARRAffinity1, $Dynamics365PortalAnalytics1)=&Lwp_Get($url);
	
	my $tokenurl = $1 if($home_content=~m/<div\s*id\=\"antiforgerytoken\"\s*data-url\=\"([^<]*?)\">/is);
	
	
	if($tokenurl!~m/^https?/is)
	{
		$tokenurl = URI::URL->new($tokenurl)->abs( $home_url, 1 );
	}
	
	$tokenurl = $tokenurl."?_=".time;
	
	my ($content2,$php_session_id2, $RequestVerificationTokencookie2, $ARRAffinity2, $Dynamics365PortalAnalytics2)=&Lwp_Get($tokenurl);
	
	my $gridDataUrl = $1 if($content1=~m/<div\s*class\=\"entity-grid\s*entitylist\"[^>]*?data-get-url\=\"([^\"]*?)\"/is);
	my $dataView = $1 if($content1=~m/<div\s*class\s*=\"entity-grid\s*entitylist\"[^>]*?data-view-layouts\=(?:\'|\")([^\'\"]*?)(?:\'|\")/is);
	my $RequestVerificationToken = uri_escape($1) if($content2=~m/<input\s*name\=\"__RequestVerificationToken\"[^>]*?value\=\"([^\"]*?)\"/is);
	
	if($gridDataUrl!~m/^https?/is)
	{
		$gridDataUrl = URI::URL->new($gridDataUrl)->abs( $home_url, 1 );
	}
	
	my $decode = MIME::Base64::decode($dataView);
	
	my $base64SecureConfiguration = $1 if($decode=~m/\"Base64SecureConfiguration\"\:\"([^\"]*?)\"/is);
	my $SortExpression = $1 if($decode=~m/\"SortExpression\"\:\"([^\"]*?)\"/is);
	
	my %manual_headers=("Accept"=>"application/json, text/javascript, */*; q=0.01", "Accept-Encoding"=>"gzip, deflate, br", "Accept-Language"=>"en-US,en;q=0.5","Content-Type"=>"application/json; charset=UTF-8","Host"=>"www.colchester.gov.uk","X-Requested-With"=>"XMLHttpRequest","Cookie"=>"Dynamics365PortalAnalytics=$Dynamics365PortalAnalytics; ASP.NET_SessionId=$php_session_id; ARRAffinity=$ARRAffinity; ContextLanguageCode=en-US; __RequestVerificationToken=$RequestVerificationTokencookie2;", "Origin"=>"https://www.colchester.gov.uk", "Sec-Fetch-Mode"=>"cors", "Sec-Fetch-Site"=>"same-origin", "__RequestVerificationToken"=>"$RequestVerificationToken", "Access-Control-Allow-Headers"=>"https://cbc-live-dotgovresources.azurewebsites.net/*", "Access-Control-Allow-Methods"=>"GET, OPTIONS", "Access-Control-Allow-Origin"=>"https://login.microsoftonline.com", "X-Frame-Options"=>"SAMEORIGIN", "Connection"=>"keep-alive");	
	
	my $post_cont = '{"base64SecureConfiguration":"'.$base64SecureConfiguration.'","sortExpression":"'.$SortExpression.'","search":null,"page":1,"pageSize":25,"filter":null,"metaFilter":null,"timezoneOffset":-330,"customParameters":[]}';
	
	my $first_content = &Lwp_Post($gridDataUrl,$post_cont,\%manual_headers);
	
	open(PP,">gridDataUrl.html");
	print PP "$first_content";
	close(PP);
	exit;
	
	# &Scrape_Details('160',$final_content);
}

	
	


sub Lwp_Get()
{
    my $url = shift;
    my $ref = shift;
	
	my @param=@$ref if($ref ne '');
	
	$url =~ s/amp\;//igs;	
	
	re_ping:
    my $req = HTTP::Request->new(GET=>$url);
	$req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");	
	$cookie->add_cookie_header($req); 
    $cookie->save; 
	
	foreach my $header(@param)
	{
		if($header=~m/\"([^\"]*?)\"\=\>\"([^\"]*?)\"/is)
		{
			my $head=$1;
			my $tail=$2;

			$req->header("$head"=>"$tail");
		}
	}
    my $res = $ua->request($req); 
	my $res_cont=$res->headers_as_string();
	
	$cookie->extract_cookies($res);

	my $php_session_id = $1 if($res_cont=~m/ASP.NET_SessionId\=([^<]*?)\;/is);
	my $RequestVerificationToken = $1 if($res_cont=~m/RequestVerificationToken\=([^<]*?)\;/is);
	my $ARRAffinity = $1 if($res_cont=~m/ARRAffinity\=([^<]*?)\;/is);
	my $Dynamics365PortalAnalytics = $1 if($res_cont=~m/Dynamics365PortalAnalytics\=([^<]*?)\;/is);
	
    my $code = $res->code();
	return ($res->decoded_content,$php_session_id, $RequestVerificationToken, $ARRAffinity, $Dynamics365PortalAnalytics);
}


sub Lwp_Post() 
{
    my $url = shift;
    my $postcont = shift;
    my $ref = shift;
	my %param=%$ref if($ref ne '');
	
	chomp($postcont);
		
	$url =~ s/amp\;//igs;	

    my $req = HTTP::Request->new(POST=>$url);
	my $gzip_deflate=0;
	
	foreach my $value(keys %param)
	{
		print "$value => $param{$value}\n";
		$req->header($value => $param{$value} );
		if($param{$value}=~m/gzip\s*\,\s*deflate/is)
		{
			$gzip_deflate=1;
		}
	}

	$req->content($postcont);
	my $res = $ua->request($req);
    $cookie->extract_cookies($res); 
    $cookie->save; 
    $cookie->add_cookie_header($req); 
	
	my $code = $res->code(); 
	print "code::$code\n";
		
	if($gzip_deflate eq 1)
	{
		return $res->decoded_content;
	}
	else
	{
		return $res->content;
	}	
}


sub Scrape_Details()
{
	my $Council_Code=shift;
	my $appPageContent=shift;
	open (FF,">final_content.html");
	print FF "$appPageContent";
	close FF;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query="insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values ";
	
	while($appPageContent=~m/<Row[^>]*?>\s*(<result\s*column\s*=\s*\\\"referenceNumber\\\"[^>]*?>\s*[\w\W]*?\s*<\\\/result>\s*)<\\\/Row>/mgsi)
	{		
		my $appContent = $1;
		
		my $Application					= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"referenceNumber\\\"[^>]*?>\s*([^>]*?)\s*<\\\/result>/is);
		my $Proposal					= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"description\\\"[^>]*?>\s*([\w\W]*?)\s*<\\\/result>/is);	
		my $Decision_Status				= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"decision\\\"[^>]*?>\s*([^>]*?)\s*<\\\/result>/is);
		my $Date_Decision_Made			= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"decisionDate\\\"[^>]*?>\s*([\w\W]*?)\s*<\\\/result>/is);
		my $Application_Status			= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"applicationStatus\\\"[^>]*?>\s*([^>]*?)\s*<\\\/result>/is);
		my $Application_Link=$home_url;
		
		print "Application==>$Application\n";
		
		my $Source_With_Time="GCS001_".$Schedule_Date."-perl";
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'), ";
		
		undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 
	}
	

	$insert_query=~s/\,\s*$//igs;

	if($insert_query!~m/values\s*$/is)
	{
		print "insert_query::$insert_query\n";
		&DB_Insert($dbh,$insert_query);
	}
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}


sub clean()
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s*\.?\(([^>]*?)\)\s*$/$1/si;	
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}
