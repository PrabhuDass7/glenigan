use strict;
use Net::SSL;
use URI::Escape;
use URI::Encode;
use MIME::Base64;
use HTTP::Cookies;
use HTML::Entities;
use LWP::UserAgent;
use Unicode::Escape;
use DBI;
use DBD::ODBC;
use Time::Piece;
use Glenigan_DB_Windows;

my $time = Time::Piece->new;
my $currentDate = $time->strftime('%y%m%d');       # 140109

print "currentDate=>$currentDate\n";

# Establish connection with DB server
my $dbh = Glenigan_DB_Windows::DB_Decision;

my $COUNCIL_NAME = "Nuneaton & Bedworth";

my $uri_obj = URI::Encode->new( { encode_reserved => 0 } );

my $ua = LWP::UserAgent->new( ssl_opts =>{ verify_hostname => 0 },keep_alive => 1,show_progress => 1);
$ua->agent("Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0");
$ua->timeout(30); 
$ua->max_redirect();

my $cookie = HTTP::Cookies->new(); 
$ua->cookie_jar(); 


my $home_url = 'https://www.nuneatonandbedworth.gov.uk/info/20025/planning_and_building_control/54/planning/2';
my ($home_content,$php_session_id)=&Lwp_Get($home_url);

if($home_content=~m/<a[^<]*?href\=\"([^<]*?)\"\s*>\s*Search\s*planning\s*applications\s*<\/a>/is)
{
	my $search_url=$1;
	my ($content1,$php_session_id)=&Lwp_Get($search_url);

	my $form_id=$1 if($content1=~m/\"formID\"\:\"([^<]*?)\"/is);
	my $processID=$1 if($content1=~m/\"processID\"\:\"([^<]*?)\"/is);
	my $stage_id=$1 if($content1=~m/\"stage-id\"\:\"([^<]*?)\"/is);

	my $form_url = 'https://customer.nuneatonandbedworth.gov.uk/authapi/isauthenticated?uri=https://customer.nuneatonandbedworth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://'.$processID.'/'.$stage_id.'/definition.json&redirectlink=/en&cancelRedirectLink=/en&consentMessage=yes&noLoginPrompt=1&hostname=customer.nuneatonandbedworth.gov.uk';

	my ($form_content,$session_id)=&Lwp_Get($form_url);
	
	$form_content = $uri_obj->decode($form_content);
	$form_content=~s/\\//igs;

	my $sid=$1 if($form_content=~m/\"auth-session\"\:\"([^<]*?)\"/is);
	my $processID=$1 if($form_content=~m/\/(AF-Process[^<]*?)\//is);
	my $stage_id=$1 if($form_content=~m/\/(AF-Stage[^<]*?)\//is);

	my $site_base_64='https://customer.nuneatonandbedworth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://'.$processID.'/'.$stage_id.'/definition.json&redirectlink=/en&cancelRedirectLink=/en&consentMessage=yes&noLoginPrompt=1';
	my $site_base_64_encoded=MIME::Base64::encode($site_base_64);
	$site_base_64_encoded =~ s/^\s+|\s+$//igs;

	my %manual_headers=("Accept-Language"=>"en-US,en;q=0.5","Content-Type"=>"application/json","Host"=>"customer.nuneatonandbedworth.gov.uk","X-Requested-With"=>"XMLHttpRequest","Cookie"=>"PHPSESSID=$php_session_id; APP_LANG=en; AWSELB= ; SESSfsauthsession=0");
	my $post_url = 'https://customer.nuneatonandbedworth.gov.uk/apibroker/runLookup?id=5ccaf79be3bb4&repeat_against=&noRetry=false&getOnlyTokens=undefined&log_id=&app_name=AF-Renderer::Self&_='.time.'&sid='.$sid;
	my $post_cont = '{"stopOnFailure":true,"usePHPIntegrations":true,"stage_name":"Search","formId":"'.$form_id.'","isPublished":true,"formName":"Search Planning Applications","site":"'.$site_base_64_encoded.'","processName":"Search planning applications","reference":""}';

	my $final_content = &Lwp_Post($post_url,$post_cont,\%manual_headers);
	
	&Scrape_Details('302',$final_content);
}

sub Lwp_Get()
{
    my $url = shift;
    my $ref = shift;
	my @param=@$ref if($ref ne '');
	$url =~ s/amp\;//igs;	
	re_ping:
    my $req = HTTP::Request->new(GET=>$url);
	$req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	
	$cookie->add_cookie_header($req); 
    $cookie->save; 
	
	foreach my $header(@param)
	{
		if($header=~m/\"([^\"]*?)\"\=\>\"([^\"]*?)\"/is)
		{
			my $head=$1;
			my $tail=$2;

			$req->header("$head"=>"$tail");
		}
	}
    my $res = $ua->request($req); 
	my $res_cont=$res->headers_as_string();
	
	$cookie->extract_cookies($res); 
	my $php_session_id=$1 if($res_cont=~m/PHPSESSID\=([^<]*?)\;/is);
    my $code = $res->code();
	return ($res->decoded_content,$php_session_id);
}


sub Lwp_Post() 
{
    my $url = shift;
    my $postcont = shift;
    my $ref = shift;
	my %param=%$ref if($ref ne '');
	
	chomp($postcont);
	
	$url =~ s/amp\;//igs;	

    my $req = HTTP::Request->new(POST=>$url);
	my $gzip_deflate=0;
	
	foreach my $value(keys %param)
	{
		$req->header($value => $param{$value} );
		if($param{$value}=~m/gzip\s*\,\s*deflate/is)
		{
			$gzip_deflate=1;
		}
	}

	$req->content($postcont);
	my $res = $ua->request($req);
    $cookie->extract_cookies($res); 
    $cookie->save; 
    $cookie->add_cookie_header($req); 
	
	my $code = $res->code(); 
	print "code $code\n";
	
	if($gzip_deflate eq 1)
	{
		return $res->decoded_content;
	}
	else
	{
		return $res->content;
	}	
	
}


sub Scrape_Details()
{
	my $Council_Code=shift;
	my $appPageContent=shift;
	open (FF,">final_content.html");
	print FF "$appPageContent";
	close FF;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $insert_query="insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values ";
	
	while($appPageContent=~m/<Row[^>]*?>\s*(<result\s*column\s*=\s*\\\"referenceNumber\\\"[^>]*?>\s*[\w\W]*?\s*<\\\/result>\s*)<\\\/Row>/mgsi)
	{		
		my $appContent = $1;
		
		my $Application					= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"referenceNumber\\\"[^>]*?>\s*([^>]*?)\s*<\\\/result>/is);
		my $Proposal					= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"description\\\"[^>]*?>\s*([\w\W]*?)\s*<\\\/result>/is);	
		my $Decision_Status				= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"decision\\\"[^>]*?>\s*([^>]*?)\s*<\\\/result>/is);
		my $Date_Decision_Made			= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"decisionDate\\\"[^>]*?>\s*([\w\W]*?)\s*<\\\/result>/is);
		my $Application_Status			= &clean($1) if($appContent=~m/<result\s*column\s*=\s*\\\"applicationStatus\\\"[^>]*?>\s*([^>]*?)\s*<\\\/result>/is);
		my $Application_Link=$home_url;
		
		print "Application==>$Application\n";
		
		my $Source_With_Time="GCS001_".$Schedule_Date."-perl";
		
		$insert_query.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'), ";
		
		undef $Application; undef $Proposal;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 
	}
	

	$insert_query=~s/\,\s*$//igs;

	if($insert_query!~m/values\s*$/is)
	{
		print "insert_query::$insert_query\n";
		&DB_Insert($dbh,$insert_query);
	}
}

###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	Reconnect:
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	my $Recon_Flag=1;
	my $Connection_Flag=0;
	if(!$dbh)
	{
		print "\nDB CONNECTION FAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
		$Connection_Flag=1;
	}
	return $dbh;
}


sub clean()
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s*\.?\(([^>]*?)\)\s*$/$1/si;	
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}
