import requests
import os,sys,re
from bs4 import BeautifulSoup
import ast
from datetime import datetime, timedelta
import pymssql


#Main Url
mainUrl = 'https://planning.southderbyshire.gov.uk/'

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

proxies2 = {
  'http': 'http://172.27.137.199:3128',
  'https': 'http://172.27.137.199:3128',
}

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)
	

def proxiesGenerator(proxies1,proxies2):    
	i = 0
	while i <= 1:
		try:
			print "Main URL is: ", mainUrl
			res = requests.get(mainUrl, proxies=proxies1)			
			if res.status_code == 200:				
				return proxies1
				
		except Exception as ex:
			print "Error is: ", str(type(ex).__name__)
			
			if str(type(ex).__name__) == "ProxyError":
				while i <= 2:
					print  "Now trying in second Proxy", proxies2
					res = requests.get(mainUrl, proxies=proxies2)
					print "second proxy URL status is: ", res.status_code
					if res.status_code == 200:
						return proxies2
					else:
						i = i + 1

def viewState(content):
    soup = BeautifulSoup (content, 'lxml')
    viewStateValue = soup.find ('input', {'id': '__VIEWSTATE'}).get ('value')
    return viewStateValue

def viewStateGenerator(content):
    soup = BeautifulSoup (content, 'lxml')
    viewStateGeneratorValue = soup.find ('input', {'id': '__VIEWSTATEGENERATOR'}).get ('value')
    return viewStateGeneratorValue

def eventValidation(content):
    soup = BeautifulSoup (content, 'lxml')
    eventValidationValue = soup.find ('input', {'id': '__EVENTVALIDATION'}).get ('value')
    return eventValidationValue
	
# daterange search section	
def inputsection(fromDay, fromMonth, fromYear, toDay, toMonth, toYear):
	try:	
	
		finProxy = proxiesGenerator(proxies1,proxies2)
		
		print "Final working proxy is: ", finProxy

		reqHeaders = {
						'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Encoding': 'gzip, deflate, br',
						'Accept-Language': 'en-US,en;q=0.9',
						'Connection': 'keep-alive',
						'Content-Type': 'text/html; charset=utf-8',
						'Host': 'planning.southderbyshire.gov.uk',
						'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
		}
		
		sessionByRequest = requests.session()
		req = sessionByRequest.get(mainUrl, headers = reqHeaders, proxies= finProxy)
		# print "Main URL status is: ", req1.status_code
		Content = req.content
		
		# with open("output.html", 'wb') as handle:
			# handle.write(str(Content))
			
		if req.status_code == 200:
			# For select Catagory here		
			req2Headers = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Connection': 'keep-alive',
							'Content-Type': 'application/x-www-form-urlencoded',
							'Referer': 'https://planning.southderbyshire.gov.uk/',
							'Host': 'planning.southderbyshire.gov.uk',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
			}
			
			fromDateContent=re.findall(r'<strong>\s*From\s*Date\s*\:?\s*<\/strong>\s*<table>\s*([\w\W]*?)\s*<\/table>', str(Content), re.IGNORECASE)
			toDateContent=re.findall(r'<strong>\s*To\s*Date\s*\:?\s*<\/strong>\s*<table>\s*([\w\W]*?)\s*<\/table>', str(Content), re.IGNORECASE)
			
			fromday=re.findall(r'FromDay\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(fromDateContent[0]), re.IGNORECASE)   
			frommonth=re.findall(r'FromMonth\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(fromDateContent[0]), re.IGNORECASE)   
			fromyear=re.findall(r'FromYear\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(fromDateContent[0]), re.IGNORECASE) 
			
			today=re.findall(r'ToDay\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(toDateContent[0]), re.IGNORECASE)   
			tomonth=re.findall(r'ToMonth\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(toDateContent[0]), re.IGNORECASE)   
			toyear=re.findall(r'ToYear\"[^>]*?>[\w\W]*?<option\s*selected=\"selected\"\s*value=\"([^\"]*?)\">\s*\1\s*<\/option>', str(toDateContent[0]), re.IGNORECASE)   
			
			#Date as PostContent passing through URL
			catPostContent =  '__EVENTTARGET=ctl00%24Mainpage%24optDecision&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(Content).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (Content)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (Content)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromday[0]) +'&ctl00%24Mainpage%24FromMonth='+ str(frommonth[0]) +'&ctl00%24Mainpage%24FromYear='+ str(fromyear[0]) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(today[0]) +'&ctl00%24Mainpage%24ToMonth='+ str(tomonth[0]) +'&ctl00%24Mainpage%24ToYear='+ str(toyear[0]) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus='
			catPostContent = catPostContent.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')

			#Post method to select Catagory
			req1 = sessionByRequest.post(url= mainUrl, data= catPostContent, headers = req2Headers, proxies= finProxy )
			CatContent = req1.content
			
			datePostContent = '__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(CatContent).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (CatContent)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (CatContent)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromDay) +'&ctl00%24Mainpage%24FromMonth='+ str(fromMonth) +'&ctl00%24Mainpage%24FromYear='+ str(fromYear) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(toDay) +'&ctl00%24Mainpage%24ToMonth='+ str(toMonth) +'&ctl00%24Mainpage%24ToYear='+ str(toYear) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus=&ctl00%24Mainpage%24cmdSearch=Search'
			datePostContent = datePostContent.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
			# print(datePostContent)
			
			#Post method to select date range
			req2 = sessionByRequest.post(url= mainUrl, data= datePostContent, headers = req2Headers, proxies= finProxy )
			dateContent = req2.content
			
			# with open("dateContent.html", 'wb') as handle:
				# handle.write(str(dateContent))

		return dateContent, finProxy
		
	except Exception as ex:
		print "Post method response is: ", str(type(ex).__name__)


# To collect Application URLs here
def collectAppURL(content, CouncilCode, Source, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear, finProxy, ss):	
	try:		 
		bulkValuesForQuery=''
		if re.findall(r'<tr[^>]*?>\s*<td>\s*([^>]*?)\s*<\/td>', str(content), re.IGNORECASE):  
			AppNum=re.findall(r'<tr[^>]*?>\s*<td>\s*([^>]*?)\s*<\/td>', str(content), re.IGNORECASE)       
			# print(AppNum)
			
			insertQuery = 'insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values '
				
			appCount = 0		
			for appNumber in AppNum:
				print("appNumber::::", appNumber)
				
				appButtonNum=re.findall(r'<tr[^>]*?>\s*<td>\s*'+ str(appNumber) +'\s*<\/td>[\w\W]*?<input\s*type=\"button\"\s*value=\"view\"\s*onclick=\"javascript\:__doPostBack\([^>]*?\&\#39\;(Select\$\d+)[^>]*?>', str(content), re.IGNORECASE)
				# print(appButtonNum[0])
				
				appPostContent = '__EVENTTARGET=ctl00%24Mainpage%24gridMain&__EVENTARGUMENT='+ str(appButtonNum[0]) +'&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(content).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (content)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (content)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromDay) +'&ctl00%24Mainpage%24FromMonth='+ str(fromMonth) +'&ctl00%24Mainpage%24FromYear='+ str(fromYear) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(toDay) +'&ctl00%24Mainpage%24ToMonth='+ str(toMonth) +'&ctl00%24Mainpage%24ToYear='+ str(toYear) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus='
				appPostContent = appPostContent.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
				print(appPostContent)
				
				req3Headers = {
						'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Encoding': 'gzip, deflate, br',
						'Accept-Language': 'en-US,en;q=0.9',
						'Connection': 'keep-alive',
						'Content-Type': 'application/x-www-form-urlencoded',
						'Referer': 'https://planning.southderbyshire.gov.uk/',
						'Host': 'planning.southderbyshire.gov.uk',
						'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
				}
				
				#App Page Post method
				req4 = ss.post(url= mainUrl, data= appPostContent, headers = req3Headers, proxies= finProxy )
				Appcontent = req4.content
			
				with open("appPostContent.txt", 'wb') as handle:
					handle.write(str(appPostContent))
			
				with open("Appcontent.html", 'wb') as handle:
					handle.write(str(Appcontent))
				raw_input()
				
				Application_Number=re.findall(r'>\s*Application\s*Reference\s*<\/p>\s*<h1[^>]*?>\s*([^<]*?)\s*<\/h1>', str(Appcontent), re.IGNORECASE)
				Date_Decision_Made=re.findall(r'>\s*latest\sdecision\sdate\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*latest\s*date[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
				Application_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
				Decision_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
				Proposal=re.findall(r'>\s*Proposal\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Proposal[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
				
				(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')
				
				if len(Application_Number) > 0:
					Application=clean(Application_Number[0])
				else:
					Application = appNumber

				if len(Proposal) > 0:
					ProposalCnt=clean(Proposal[0])

				if len(Application_Status) > 0:
					ApplicationStatus=clean(Application_Status[0])

				if len(Decision_Status) > 0:
					DecisionStatus=clean(Decision_Status[0])

				if len(Date_Decision_Made) > 0:
					DateDecisionMade=clean(Date_Decision_Made[0])

				format1 = "%Y/%m/%d %H:%M"					
				Schedule_Date = datetime.now().strftime(format1)

				Source_With_Time = Source+"_"+Schedule_Date+"-perl"
				
				Council_Name = "South Derbyshire"
				joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
								
				if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
				elif appCount == 5:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					appCount=0
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					appCount += 1	
				
		else:
			print("ERROR!!!!!")
			
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# nextPageSection parse section	
def nextPageSection(searchcontent, CouncilCode, Source, Proxy, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear):
	try:
		
		pageNumber = 2
		pageNum = 2
		fullQuery=''
		
		sessionRequest = requests.session()
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent)):
			fullQuery = collectAppURL(str(searchcontent), CouncilCode, Source, conn, cursor, fromDay, fromMonth, fromYear, toDay, toMonth, toYear, Proxy, sessionRequest)
			cursor.execute(fullQuery)
			conn.commit()
			
			check_next_page = re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?(Page\$'+str(pageNumber)+')[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?(Page\$'+str(pageNumber)+')[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent), re.I):					
					eventArgument=check_next_page[0].replace ('$', '%24')
					nextPagePostCnt = '__EVENTTARGET=ctl00%24Mainpage%24gridMain&__EVENTARGUMENT='+ str(eventArgument) +'&__LASTFOCUS=&__VIEWSTATE='+ str(viewState(searchcontent).replace ('=', '%3D')) +'&__VIEWSTATEGENERATOR='+ str (viewStateGenerator (searchcontent)) +'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='+ str (eventValidation (searchcontent)) +'&ctl00%24Mainpage%24txtRef=&ctl00%24Mainpage%24Selectdate=optDecision&ctl00%24Mainpage%24txtLocation=&ctl00%24Mainpage%24FromDay='+ str(fromDay) +'&ctl00%24Mainpage%24FromMonth='+ str(fromMonth) +'&ctl00%24Mainpage%24FromYear='+ str(fromYear) +'&ctl00%24Mainpage%24txtProposal=&ctl00%24Mainpage%24ToDay='+ str(toDay) +'&ctl00%24Mainpage%24ToMonth='+ str(toMonth) +'&ctl00%24Mainpage%24ToYear='+ str(toYear) +'&ctl00%24Mainpage%24dropPostcode=&ctl00%24Mainpage%24dropStatus='
					nextPagePostCnt = nextPagePostCnt.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D').replace ('+','%2B')
					
					reqHeaders = {
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
							'Accept-Encoding': 'gzip, deflate, br',
							'Accept-Language': 'en-US,en;q=0.9',
							'Connection': 'keep-alive',
							'Content-Type': 'application/x-www-form-urlencoded',
							'Referer': 'https://planning.southderbyshire.gov.uk/',
							'Host': 'planning.southderbyshire.gov.uk',
							'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0'
					}
			
					# Next page Post method
					req3 = sessionRequest.post(url= mainUrl, data= nextPagePostCnt, headers = reqHeaders, proxies= Proxy )
					nextPageContent = req3.content
			
					with open("nextPageContent.html", 'wb') as handle:
						handle.write(str(nextPageContent))
			
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(nextPageContent), re.I):
						check_next_page = re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(nextPageContent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			print("Hello Pari!!")	
			
		
		return fullQuery

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno		

# Clean function
def clean(cleanValue):
    try:
		clean='' 
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno		
				
	
# Main section	
if __name__== "__main__":	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()
	
	# GCS calculation
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]	
	todayCalender = datetime.now()

	sdayCalender = todayCalender - timedelta(days=int(gcsdate))
	const_CalenderMonth = sdayCalender.strftime("%b")
	const_CalenderYear = sdayCalender.strftime("%Y")

	currentMonth = sdayCalender.strftime("%b")
	currentYear = sdayCalender.strftime("%Y")
	currentDay = sdayCalender.strftime("%d")
	
	preday = sdayCalender - timedelta(days=6)
	
	fromMonth = preday.strftime("%b")
	fromYear = preday.strftime("%Y")
	fromDay = preday.strftime("%d")

	print fromDay, fromMonth, fromYear
	print currentDay, currentMonth, currentYear
	
	searchCnt, proxy  = inputsection(fromDay, fromMonth, fromYear, currentDay, currentMonth, currentYear)
	
	finalinsertQuery = nextPageSection(searchCnt, councilcode, gcs, proxy, conn, cursor,fromDay, fromMonth, fromYear, currentDay, currentMonth, currentYear )
	
	# if (finalinsertQuery != ''):
		# print (finalinsertQuery)
		# cursor.execute(finalinsertQuery)
		# conn.commit()