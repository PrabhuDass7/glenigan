# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Decision/SouthDerbyshire'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(fromDay, fromMonth, fromYear, toDay, toMonth, toYear, driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		driver.maximize_window()
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		
		advfinalCnt=''
		if len(content) > 0:
			advfinalCnt = content.encode("utf-8")

		
		# with open(folder_path+"/361_out.html", 'wb') as fd:
			# fd.write(advfinalCnt)
		# raw_input()

		driver.find_element_by_xpath('//*[@id="Mainpage_optDecision"]').click()
		# driver.find_element_by_xpath('//*[@id="Mainpage_optDecision"]').send_keys('optDecision')
				
		
		driver.find_element_by_xpath('//*[@id="Mainpage_FromDay"]').send_keys(fromDay)
		driver.find_element_by_xpath('//*[@id="Mainpage_FromMonth"]').send_keys(fromMonth)
		driver.find_element_by_xpath('//*[@id="Mainpage_FromYear"]').send_keys(fromYear)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath('//*[@id="Mainpage_ToDay"]').send_keys(toDay)
		driver.find_element_by_xpath('//*[@id="Mainpage_ToMonth"]').send_keys(toMonth)
		driver.find_element_by_xpath('//*[@id="Mainpage_ToYear"]').send_keys(toYear)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="Mainpage_cmdSearch"]').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
		
		# with open(folder_path+"/361_out.html", 'wb') as fd:
			# fd.write(searchfinalCnt)
		# raw_input()
		
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno



# To collect Application URLs here
def collectAppURL(content, home_url, driver, CouncilCode, Source, conn, cursor, pagenumber):	
	try:		 
		bulkValuesForQuery=''
		if re.findall(r'<tr[^>]*?>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(content), re.IGNORECASE):
			# AppNum=re.findall(r'<tr[^>]*?>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(content), re.IGNORECASE)          
			AppNum=re.findall(r'<tr[^>]*?>\s*<td>\s*([^>]*?)\s*<\/td>', str(content), re.IGNORECASE)    
			# print(AppNum)
			aplNum = 0
			appCount = 0
			insertQuery = 'insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values '
			# with open(folder_path+"/361_out.html", 'wb') as fd:
				# fd.write(content)
			print(":::::::DECISION::::::::")
			
			print("pagenumber ==>",pagenumber)
			print("App Length ==>",len(AppNum))
			if pagenumber >= 2:
				aplNum = 3
			elif pagenumber == 1 and len(AppNum) >= 15:
				aplNum = 3
			elif pagenumber == 1 and len(AppNum) <= 14:
				aplNum = 2
			elif len(AppNum) <= 15:
				aplNum = 2
			else:
				aplNum = 3
				
			for appNumber in AppNum:
				print("appNumber::::", appNumber)
				driver.find_element_by_xpath('//*[@id="Mainpage_gridMain"]/tbody/tr['+str(aplNum)+']/td[8]/input').click()
				time.sleep(10)  
				Appcontent=driver.page_source
				# print ("aplNum:::",aplNum)
				appURL=driver.current_url
				print("appURL::",appURL)
				# raw_input()
				# with open(folder_path+"/361_out_"+str(aplNum)+".html", 'wb') as fd:
					# fd.write(Appcontent)
				aplNum = aplNum + 1 
				# sys.exit()
				
				if re.search(r'^https?\:\/\/southderbyshirepr\.force\.com\/', str(appURL), re.IGNORECASE):
					Application_Number=re.findall(r'>\s*Application\s*Reference\s*<\/p>\s*<h1[^>]*?>\s*([^<]*?)\s*<\/h1>', str(Appcontent), re.IGNORECASE)
					Date_Decision_Made=re.findall(r'>\s*latest\sdecision\sdate\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*latest\s*date[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Application_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Decision_Status=re.findall(r'<a\s*class=\"tabHeader\s*slds-path__link\"[^>]*?aria-selected=\"true\"[^>]*?>\s*<span[^>]*?>\s*(?:<\!\-\-[^>]*?\-\-\>)?\s*<\/span>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
					Proposal=re.findall(r'>\s*Proposal\s*<\/span>\s*<div[^>]*?>\s*<[\w\W]*?>\s*The\s*Proposal[^<]*?<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
					
					(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')
					
					if len(Application_Number) > 0:
						Application=clean(Application_Number[0])
					else:
						Application = appNumber

					if len(Proposal) > 0:
						ProposalCnt=clean(Proposal[0])

					if len(Application_Status) > 0:
						ApplicationStatus=clean(Application_Status[0])

					if len(Decision_Status) > 0:
						DecisionStatus=clean(Decision_Status[0])

					if len(Date_Decision_Made) > 0:
						DateDecisionMade=clean(Date_Decision_Made[0])

					format1 = "%Y/%m/%d %H:%M"					
					Schedule_Date = datetime.now().strftime(format1)

					Source_With_Time = Source+"_"+Schedule_Date+"-perl"
					
					Council_Name = "South Derbyshire"
					joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
					
					# print "ApplicationValues: ", joinValues
					bulkValuesForQuery = insertQuery+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					bulkValuesForQuery=''

					# if appCount == 0:
						# bulkValuesForQuery = insertQuery+joinValues
						# appCount += 1
					# elif appCount == 5:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# cursor.execute(bulkValuesForQuery)
						# conn.commit()
						# appCount=0
						# bulkValuesForQuery=''
					# else:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# appCount += 1	
					# driver.back()
					# if len(Application) > 0:
						# driver.execute_script("window.history.go(-2)")
						# time.sleep(10)  			
					
					driver.get(home_url)
					#-- Wait funtion
					time.sleep(10)
					driver.find_element_by_xpath('//*[@id="Mainpage_optDecision"]').click()
					content=driver.page_source	
				else:
					Application_Number=re.findall(r'Application\s*Reference\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Date_Decision_Made=re.findall(r'Date\s*of\s*Decision\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Decision_Status=re.findall(r'Decision\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([^>]*?)\s*(?:<[^>]*?>)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
					Proposal=re.findall(r'Proposed\s*Development\s*(?:<\/b>\s*<\/font>)?\s*<\/td>\s*<td>\s*(?:<[^>]*?>)?\s*([\w\W]*?)\s*(?:<[^>]*?>)?\s*<\/td>\s*<\/tr>', str(Appcontent), re.IGNORECASE)
					
					(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')
					
					if len(Application_Number) > 0:
						Application=clean(Application_Number[0])
					else:
						Application = appNumber

					if len(Proposal) > 0:
						ProposalCnt=clean(Proposal[0])


					if len(Decision_Status) > 0:
						DecisionStatus=clean(Decision_Status[0])

					if len(Date_Decision_Made) > 0:
						DateDecisionMade=clean(Date_Decision_Made[0])

					format1 = "%Y/%m/%d %H:%M"					
					Schedule_Date = datetime.now().strftime(format1)

					Source_With_Time = Source+"_"+Schedule_Date+"-perl"
					
					Council_Name = "South Derbyshire"
					joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
					
					
					bulkValuesForQuery = insertQuery+joinValues
					print "bulkValuesForQuery: ", bulkValuesForQuery
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					bulkValuesForQuery=''
					
					
					# if appCount == 0:
						# bulkValuesForQuery = insertQuery+joinValues
						# appCount += 1
					# elif appCount == 5:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# cursor.execute(bulkValuesForQuery)
						# conn.commit()
						# appCount=0
						# bulkValuesForQuery=''
					# else:
						# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
						# appCount += 1	
						
					driver.get(home_url)
					#-- Wait funtion
					time.sleep(10)
					driver.find_element_by_xpath('//*[@id="Mainpage_optDecision"]').click()
					content=driver.page_source
				# with open(folder_path+"/361_outback.html", 'wb') as fd:
					# fd.write(content)
				
		else:
			print("ERROR!!!!!")
			
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 

# nextPageSection parse section	
def nextPageSection(searchcontent, homeURL, driver, CouncilCode, Source, conn, cursor):
	try:
		
		pageNumber = 2
		pageNum = 2
		fullQuery=''
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>'+str(pageNumber)+'|\.\.\.<\/a>\s*<\/td>', str(searchcontent)):
			fullQuery = collectAppURL(str(searchcontent), homeURL, driver, CouncilCode, Source, conn, cursor, 1)			
			cursor.execute(fullQuery)
			conn.commit()
			check_next_page = re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>('+str(pageNumber)+'|\.\.\.)<\/a>\s*<\/td>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<td>\s*<a\s*href=\"javascript\:__doPostBack\([^>]*?Page\$'+str(pageNumber)+'[^>]*?>('+str(pageNumber)+'|\.\.\.)<\/a>\s*<\/td>', str(searchcontent), re.I):					
					driver.find_element_by_xpath('//*[@id="Mainpage_gridMain"]/tbody/tr[1]/td/table/tbody/tr/td['+str(pageNum)+']/a').click()	
					#-- Wait funtion
					time.sleep(10)  
					appNextPagecontent=driver.page_source
					
					# with open(folder_path+"/361_out1.html", 'wb') as fd:
						# fd.write(appNextPagecontent)
					# sys.exit()
					
					fullQuery = collectAppURL(str(appNextPagecontent), homeURL, driver, CouncilCode, Source, conn, cursor, pageNum)	
					cursor.execute(fullQuery)
					conn.commit()
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I):
						check_next_page = re.findall(r'<li>\s*<a\s*data-ajax-target=\"[^\"]*?\"\s*href=\"\">\s*('+str(pageNumber)+')\s*<\/a>\s*<\/li>', str(appNextPagecontent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			fullQuery = collectAppURL(str(searchcontent), homeURL, driver, CouncilCode, Source, conn, cursor, 1)		
			
		
		return fullQuery

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	gcs = sys.argv[2]	
	
	if councilcode is None:
		print 'Councilcode arugument is missing'
		sys.exit()
		
	if gcs is None:
		print 'GCS arugument is missing'
		sys.exit()
		
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
			
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	
	gcsdate = thisgcs[gcs]	
	todayCalender = datetime.now()

	sdayCalender = todayCalender - timedelta(days=int(gcsdate))
	const_CalenderMonth = sdayCalender.strftime("%b")
	const_CalenderYear = sdayCalender.strftime("%Y")

	currentMonth = sdayCalender.strftime("%b")
	currentYear = sdayCalender.strftime("%Y")
	# currentDay = sdayCalender.strftime("%#d")
	currentDay = sdayCalender.strftime("%d")
	
	preday = sdayCalender - timedelta(days=6)
	
	fromMonth = preday.strftime("%b")
	fromYear = preday.strftime("%Y")
	fromDay = preday.strftime("%d")

	print fromDay, fromMonth, fromYear
	print currentDay, currentMonth, currentYear
		
	
	homeURL = 'https://planning.southderbyshire.gov.uk/Applications.aspx'
	
	results = inputsection(fromDay, fromMonth, fromYear, currentDay, currentMonth, currentYear, browser, homeURL)

	finalinsertQuery = nextPageSection(results, homeURL, browser, councilcode, gcs, conn, cursor)
	# print type(finalinsertQuery)
	
	if (finalinsertQuery != ''):
		# print (finalinsertQuery)
		cursor.execute(finalinsertQuery)
		conn.commit()
	
	browser.quit()