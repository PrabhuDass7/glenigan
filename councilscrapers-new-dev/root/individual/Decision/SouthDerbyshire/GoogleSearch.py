from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os, re, sys
from bs4 import BeautifulSoup
import time
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import sys

def clean(datas):
    datas = re.sub(r'\n', "", str(datas))
    datas = re.sub(r'^\s*', "", str(datas))
    datas = re.sub(r'\s*$', "", str(datas))
    datas = re.sub(r'\'\s*$', "", str(datas))
    datas = re.sub(r'^\'\s*', "", str(datas))
    return datas

def removeContent(DateRange):
    try:
        if os.path.exists("Detail_Page_"+DateRange+".html"):
          os.remove("Detail_Page_"+DateRange+".html")
    except:
        pass
  
def WriteContent(DateRange,finalCnt):
    try:
        with open("Detail_Page_"+DateRange+".html", 'wb') as fd:
            fd.write(finalCnt)
        print ("YES")
    except:
        pass


# Main section  
if __name__== "__main__":
    ARGVS = sys.argv[1]
    DateRange,Main_URL=ARGVS.split("|")

    DateRange=clean(DateRange)
    Main_URL=clean(Main_URL)

    removeContent(DateRange)

    PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
    chrome_options.add_argument('--proxy-server=https://%s' % PROXY)


    #-- Setup
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument('--log-level=3')

    browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/Chrome_v_90/chromedriver.exe')
    # browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver.exe')

    browser.get(Main_URL)
    browser.maximize_window()

    time.sleep(10)
    content=browser.page_source
    finalCnt = content.encode("utf-8")

    browser.quit()

    WriteContent(DateRange,finalCnt)