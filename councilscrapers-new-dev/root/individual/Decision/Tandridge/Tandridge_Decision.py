import requests
from bs4 import BeautifulSoup
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

url = "https://tdcplanningsearch.tandridge.gov.uk/"

''' Create Log Directory'''
basePath = os.getcwd()
print ("basePath::",basePath)

todaydate = time.strftime('%Y%m%d')
logDirectory=basePath+"/log/"+todaydate
if os.path.isdir(str(logDirectory)) == False:
	os.makedirs(str(logDirectory))
	
reload(sys)
sys.setdefaultencoding("utf-8")

proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

proxies2 = {
  'http': 'http://172.27.137.199:3128',
  'https': 'http://172.27.137.199:3128',
}

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)
	
# Proxy Section
def proxiesGenerator(proxies1,proxies2):    
	i = 0
	while i <= 1:
		try:
			print("Main URL is: ", url)
			# res = requests.get(url, proxies=proxies1)			
			res = requests.get(url, proxies=proxies2)			
			if res.status_code == 200:				
				# return proxies1
				return proxies2
				
		except Exception as ex:
			print("Error is: ", str(type(ex).__name__))
			
			if str(type(ex).__name__) == "ProxyError":
				while i <= 2:
					# print("Now trying in second Proxy", proxies2)
					print("Now trying in second Proxy", proxies1)
					# res = requests.get(url, proxies=proxies2)
					res = requests.get(url, proxies=proxies1)
					print("second proxy URL status is: ", res.status_code)
					if res.status_code == 200:
						# return proxies2
						return proxies1
					else:
						i = i + 1		
						
					
		
# To collect Application URLs here		
def collectAppURLData(url,headers, payload, eventTargetList, Source):
	try:
		bulkValuesForQuery=''
				
		appCount = 0
		insertQuery = 'insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values '
		
		for eventTarget in eventTargetList:
			payload["__EVENTTARGET"] = str(eventTarget)
						
			s = requests.session()
			s.headers.update(headers)
	
			response = s.post(url,data=payload)	
			soup = BeautifulSoup(response.content, "html.parser")			
			print("Decision URL Details Response::",response.status_code)						
						
			(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')
						
			applicationNumber = soup.find('span', {'id': 'MainContent_lblPlanningAppRefText'})			
			proposal = soup.find('span', {'id': 'MainContent_lblProposalTextValue'})
			Decision_Status = soup.find('span', {'id': 'MainContent_lblDecisionTextValue'})
			Application_Status = soup.find('span', {'id': 'MainContent_lblDecisionTextValue'})
			Date_Decision_Made = soup.find('span', {'id': 'MainContent_lblDecisionDateValue'})		
			
			
			# Document_Url= soup.find("a", {'id':'MainContent_btnViewDocs'}).get('href')
			appURL="https://tdcplanningsearch.tandridge.gov.uk/PlanningApplicationDetail"
			
			if len(applicationNumber.text) > 0:
				Application=applicationNumber.text.strip()
			if len(proposal.text) > 0:
				ProposalCnt=proposal.text.strip()
			if len(Decision_Status.text) > 0:
				DecisionStatus=Decision_Status.text.strip()
			if len(Date_Decision_Made.text) > 0:
				DateDecisionMade=Date_Decision_Made.text.strip()
			if len(Application_Status.text) > 0:
				ApplicationStatus=Application_Status.text.strip()
							
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			CouncilCode = "394"
			Council_Name = "Tandridge"
			
			joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
			
			# print("ApplicationValues: ", joinValues)
			if len(Application) != 0:
				if appCount == 0:
					bulkValuesForQuery = insertQuery+joinValues
					appCount += 1
				elif appCount == 5:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					cursor.execute(bulkValuesForQuery)
					conn.commit()
					appCount=0
					bulkValuesForQuery=''
				else:
					bulkValuesForQuery = bulkValuesForQuery+","+joinValues
					appCount += 1
		# print(bulkValuesForQuery)	
		return bulkValuesForQuery
		
	except Exception as e:
		print(e,sys.exc_traceback.tb_lineno)
		
######### Main function ##########
def main(fdate, tdate, conn, cursor, gcs, CouncilCode):
	
	finProxy = proxiesGenerator(proxies1,proxies2)
		
	print("Final working proxy is: ", finProxy)
	
	"""creating request session"""
	headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp"
		",image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-GB,en;q=0.9,en-US;q=0.8,tr;q=0.7",
		"Connection": "keep-alive",
		"Upgrade-Insecure-Requests": "1",
		"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36"
		" (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
		"Host": "tdcplanningsearch.tandridge.gov.uk",
	}
	s = requests.session()
	s.headers.update(headers)

	response = s.get(url)
	# print ("response",response.text)
	soup = BeautifulSoup(response.text, "html.parser")
	
	eventTarget = soup.find('input', {'id': '__EVENTTARGET'}).get('value')
	eventArgument = soup.find('input', {'id': '__EVENTARGUMENT'}).get('value')
	lastFocus = soup.find('input', {'id': '__LASTFOCUS'}).get('value')
	viewState = soup.find('input', {'id': '__VIEWSTATE'}).get('value')
	viewStateGenerator = soup.find('input', {'id': '__VIEWSTATEGENERATOR'}).get('value')
	eventValidation = soup.find('input', {'id': '__EVENTVALIDATION'}).get('value')
	
	payload={
		"__EVENTTARGET":"ctl00$MainContent$ddlSearchCriteria",
		"__EVENTARGUMENT":str(eventArgument),
		"__LASTFOCUS":str(lastFocus),
		"__VIEWSTATE":str(viewState),
		"__VIEWSTATEGENERATOR":str(viewStateGenerator),
		"__EVENTVALIDATION":str(eventValidation),
		"ctl00$MainContent$ddlSearchCriteria":"Decision date",
	}
		
	headers={
		"Content-Type": "application/x-www-form-urlencoded",
		"Origin": "https://tdcplanningsearch.tandridge.gov.uk",
		"Host": "tdcplanningsearch.tandridge.gov.uk",
		"Referer": "https://tdcplanningsearch.tandridge.gov.uk/",
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
	}
	
	# Dropdown Select URL
	response = s.post(url, headers=headers,data=payload)
	
	soup = BeautifulSoup(response.content, "html.parser")
	eventTarget = soup.find('input', {'id': '__EVENTTARGET'}).get('value')
	eventArgument = soup.find('input', {'id': '__EVENTARGUMENT'}).get('value')
	lastFocus = soup.find('input', {'id': '__LASTFOCUS'}).get('value')
	viewState = soup.find('input', {'id': '__VIEWSTATE'}).get('value')
	viewStateGenerator = soup.find('input', {'id': '__VIEWSTATEGENERATOR'}).get('value')
	eventValidation = soup.find('input', {'id': '__EVENTVALIDATION'}).get('value')
		
	headers={
		"Content-Type": "application/x-www-form-urlencoded",
		"Origin": "https://tdcplanningsearch.tandridge.gov.uk",
		"Host": "tdcplanningsearch.tandridge.gov.uk",
		"Referer": "https://tdcplanningsearch.tandridge.gov.uk/",
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
	}
		
	payload={
		"__EVENTTARGET":str(eventTarget),
		"__EVENTARGUMENT":str(eventArgument),
		"__LASTFOCUS":str(lastFocus),
		"__VIEWSTATE":str(viewState),
		"__VIEWSTATEGENERATOR":str(viewStateGenerator),
		"__EVENTVALIDATION":str(eventValidation),
		"ctl00$MainContent$ddlSearchCriteria":"Decision date",
		"ctl00$MainContent$ddlParish":"Bletchingley",
		"ctl00$MainContent$txtStartDate":fdate,
		"ctl00$MainContent$txtEndDate":tdate,
		"ctl00$MainContent$btnSearch":"Search",
	}
	
	# Date Filter Search URL
	response = s.post(url,data=payload,headers=headers)
	searchcontent = response.content
	print("SearchPost Response::",response.status_code)
	
	filename=logDirectory+"/"+CouncilCode+"_"+gcs+".html"
	with open(filename, 'wb') as fd:
		fd.write(response.content)
	
	soup = BeautifulSoup(response.content, "html.parser")
	
	dataRows = soup.findAll("tr", {"class": "DataRow"})
	
	eventArgument = soup.find('input', {'id': '__EVENTARGUMENT'}).get('value')
	lastFocus = soup.find('input', {'id': '__LASTFOCUS'}).get('value')
	viewState = soup.find('input', {'id': '__VIEWSTATE'}).get('value')
	viewStateGenerator = soup.find('input', {'id': '__VIEWSTATEGENERATOR'}).get('value')
	eventValidation = soup.find('input', {'id': '__EVENTVALIDATION'}).get('value')
	eventTargetList=[]
	# print("dataRows",len(dataRows))
	
	# To collect Application URLs here
	if(len(dataRows)>0):	
		for dataRow in dataRows:
			td= dataRow.find("td").find("a").get('href')
			eventTarget = td.split('"')[1]
			eventTargetList.append(eventTarget)
		# print("eventTargetList",len(eventTargetList))
		
		if(len(eventTargetList)>0):
			headers={
				"Content-Type": "application/x-www-form-urlencoded",
				"Origin": "https://tdcplanningsearch.tandridge.gov.uk",
				"Host": "tdcplanningsearch.tandridge.gov.uk",
				"Referer": "https://tdcplanningsearch.tandridge.gov.uk/",
				"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
			}
			
			payload={		
				"__EVENTTARGET":str(eventTarget),
				"__EVENTARGUMENT":str(eventArgument),
				"__LASTFOCUS":str(lastFocus),
				"__VIEWSTATE":str(viewState),
				"__VIEWSTATEGENERATOR":str(viewStateGenerator),
				"__EVENTVALIDATION":str(eventValidation),
				"ctl00$MainContent$ddlSearchCriteria":"Decision date",
				"ctl00$MainContent$ddlParish":"Bletchingley",
				"ctl00$MainContent$txtStartDate":fdate,
				"ctl00$MainContent$txtEndDate":tdate,
				"tblSearchResult_length": 10,
			}
			
			finalinsertQuery = collectAppURLData(url,headers, payload, eventTargetList, gcs)
			
			if (finalinsertQuery != ''):
				cursor.execute(finalinsertQuery)
				conn.commit()
				print("Data Inserted Successfully")
	else:
		print("Planning Details URL count is 0")

	
if __name__ == '__main__':
	councilCode = sys.argv[1]
	sourceGCS = sys.argv[2]
	
	thisgcs =	{
	  "GCS001": "0",
	  "GCS002": "7",
	  "GCS003": "14",
	  "GCS004": "21",
	  "GCS005": "28",	
	  "GCS090": "90",
	  "GCS180": "180",
	}	
	print("gcs::",sourceGCS)
	
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()
	
	gcsdate = thisgcs[sourceGCS]	
	date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
	
	format = "%Y-%m-%d"
	
	todate = date_N_days_ago.strftime(format)
	
	preday = date_N_days_ago - timedelta(days=6)
	fromdate = preday.strftime(format)
	
	print('fromdate  :', fromdate)
	print('todate    :', todate)
	
	main(fromdate, todate, conn, cursor, sourceGCS, councilCode)