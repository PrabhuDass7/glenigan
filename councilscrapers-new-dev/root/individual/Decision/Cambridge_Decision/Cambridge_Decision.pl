use strict;
use WWW::Mechanize;
use WWW::Mechanize::Firefox;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;
use Cwd qw(abs_path);
use Cwd;
use POSIX;

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"137\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

my $dir = getcwd;
my $logFolder="$dir/Log";

my $time = Time::Piece->new;
my $date = $time->strftime('%d%m%Y');
my $dataFolder="$logFolder/$date";

&createdirectory($logFolder);
&createdirectory($dataFolder);

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();

system('"C:/Program Files/Mozilla Firefox/firefox.exe"');
sleep(3);
### Get Council Details from ini file ###

my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "Cambridge_Decision.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
my $FORM_START_ID = $Config->{$Council_Code}->{'FORM_START_ID'};
my $FORM_END_ID = $Config->{$Council_Code}->{'FORM_END_ID'};

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";

#### UserAgent Declaration ####
reping:
my ($mech,$mechz);
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize::Firefox->new(autocheck => 0);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize::Firefox->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
	
	$mechz = WWW::Mechanize->new( 
				agent => 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36',
				ssl_opts => {
				SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
				verify_hostname => 0, 
				}, autocheck => 0);
}	

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

# Get search results using date ranges

my ($Responce, $javascript, $Ping_Status1);
$Responce = $mech->get($HOME_URL);

my $contents = $mech->content;
my $code = $mech->status;
# print "Fisrt Page :: $code\n";

sleep(8);

$mech->form_number($FORM_NUMBER);
$mech->set_fields($FORM_START_ID => $From_Date);
$mech->set_fields($FORM_END_ID => $To_Date);
sleep(3);

$mech->click({ xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' });
sleep(10);

my $retries = 5;
while ($retries-- and $mech->is_visible( xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' )) {
      sleep (5);
	  print "Loading****\n";
	  eval{
		$mech->click({ xpath => '//*[@id="advancedSearchForm"]/div[4]/input[2]' });
		};
		if($@)
		{
			print "Error:: $@";
		}
};

sleep(2);
my $content = $mech->content;
my $code = $mech->status;

# open(PPP,">Search_Content1.html");
# print PPP "$Search_Content\n";
# close(PPP);
# exit;

sleep(5);

$mech->form_number($FORM_NUMBER);
$mech->select('searchCriteria.resultsPerPage', '100');
$mech->submit();

sleep(60);

my $content = $mech->content;
my $code = $mech->status;

sleep(10);

open HH,">$dataFolder/$Date_Range.html";
print HH "$content";
close HH;

my @Detail_Links = ();
my ($Detail_Links) =&LinkCollection($content);
@Detail_Links = @$Detail_Links;

my $totalPage = $1 if($content=~m/$TOTAL_PAGE_COUNT/is);

my $i=2;
next_page:
if($i<=$totalPage)
{	
	print "$i======$totalPage\n";	
	my $temp_var=$i;
	eval
	{
		$mech->click({ selector => '.next', 1 });

		sleep(60);
		my $datePage_url = $mech->uri();
		my $nextpagecontent = $mech->content;
		
		# open(PP,">Search_Content$i.html");
		# print PP "$nextpagecontent\n";
		# close(PP);
		
		open HH,">>$dataFolder/$Date_Range.html";
		print HH "$nextpagecontent";
		close HH;
		
		my ($Detail_Links) = &LinkCollection($nextpagecontent);
		@Detail_Links = @$Detail_Links;
		
		if($nextpagecontent=~m/$TOTAL_PAGE_COUNT/is)
		{
			$totalPage = $1;
		}else{
			$totalPage = 0;
		}
		
		$i++;
	}; 
	if($@)
	{ 
		print "Error:: $@";
		sleep(10);
		goto reping if($temp_var == $i);
	}
	goto next_page;
}

my @Detail_Links = uniq(@Detail_Links);

&Scrape_Details( $Council_Code,\@Detail_Links );

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

sub Scrape_Details()
{
	my $Council_Code = shift;
	my $Detl_Links = shift;
	my @Detail_Links = @{$Detl_Links};
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
    my $Search_Results_Count = scalar(@Detail_Links);
	
	my $code;
	
	my $resultsCount=0;
	foreach my $Application_Link(@Detail_Links)
	{
		print "Application_Link==>$Application_Link\n";
		
		my $Application; my $Proposal; my $Application_Status; my $Date_Decision_Made; my $Decision_Status;
		my $Source = $Date_Range;
		
		my $count_c=0;
		repingg:
		# my ($code,$App_Page_Content)=&getMechCont($Application_Link);
		
		$mechz->get($Application_Link);
		sleep(7);
		my $code= $mechz->status();
		my $App_Page_Content = $mechz->content;

		if(($code !=200) && ($count_c < 3))
		{
			$count_c++;
			goto repingg;
		}
		
		# open(PP,">App_Page_Content.html");
		# print PP "$App_Page_Content\n";
		# close(PP);
		
		open HH,">>$dataFolder/StatusCode_$Date_Range.txt";
		print HH "$Application_Link\t$code\n";
		close HH;

		my $categoryType='Decision';
		my $insertQuery;
		if(($Council_Code=~m/^137$/is) && ($code ==200))
		{
			sleep(3);
			my %dataDetails;
			
			my ($dataDetails,$applicationPageContentResponse, $appNumber) = &getApplicationDetails($Application_Link,$mechz, $categoryType,$Council_Code);
		
			%dataDetails = %{$dataDetails};
			
			if($applicationPageContentResponse=~m/^\s*(200|ok)\s*$/is)
			{
				$resultsCount++;
			}	
			
			if($appNumber ne "")
			{
				my ($insert_query_From_Func) = &applicationsDataDumpDecision(\%dataDetails,$Application_Link,$Source,$Council_Code,$COUNCIL_NAME,$Schedule_Date,$Schedule_Date);
				
				$insertQuery="insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status, Imported_Date) values ".$insert_query_From_Func;
			}
		}
		
		if($insertQuery!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insertQuery);
		}
	}
}


sub getMechCont()
{
	my $Link = shift;

	$mech->get($Link);
	sleep(7);
	my $content = $mech->content;
	
	my $code= $mech->status();
	print "CODE :: $code\n";
	my $Link_Reset_Status = $mech->success();
	
	return ($code,$content);
}

sub LinkCollection()
{
	my $page_cnt = shift;
	while($page_cnt=~m/<li\s*class\=\"searchresult\">\s*<a\s*href\=\"([^>]*?)\">/igs)
	{
		my $App_Link = $1;
		my $Application_Link;
		
		if($App_Link!~m/^\s*$/is)
		{
			if($App_Link!~m/https?\:/is)
			{
				$Application_Link = $FILTER_URL.$App_Link;
			}	
			else
			{
				$Application_Link = $App_Link;
			}	
		}
		$Application_Link=~s/&amp;/&/igs;
		print "Application_Link:: $Application_Link\n";
		push(@Detail_Links,$Application_Link);		
	}
	return(\@Detail_Links);
}

sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/\'/\'\'/igs;
	return($data2Clean);
}

sub clean()
{
	my $Data=shift;
	
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime) = @_;	
	
	my %activeTabContent = %{$TabContent};
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
		
	foreach my $AppDataKey (keys %activeTabContent)
	{				
		# print "$AppDataKey<==>$activeTabContent{$AppDataKey}[0]\n";<STDIN>;
		
		### Summary ###
		my ($DATE_DECISION_MADE,$APPLICATION_REFERENCE,$CASE_REFERENCE,$DECISION_STATUS,$DECISION_ISSUED_DATE,$PROPOSAL,$APPLICATION,$APPLICATION_STATUS,$SUMMARY_CASENUMBER,$SUMMARY_PROPOSAL,$DECISION_ISSUED_DATES,$DECISION_MADE_DATE,$DECISION_DATE,$DECISION_PRINTED_DATE,$DECISION,$AUTHORITY_DECISION_STATUS,$AUTHORITY_DECISION_DATE);
		$DATE_DECISION_MADE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DATE_DECISION_MADE$/is);
		$APPLICATION_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_REFERENCE$/is);
		$CASE_REFERENCE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^CASE_REFERENCE$/is);
		$DECISION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_STATUS$/is);
		$DECISION_ISSUED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATE$/is);
		$PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^PROPOSAL$/is);
		$APPLICATION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION$/is);
		$APPLICATION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^APPLICATION_STATUS$/is);
		$SUMMARY_CASENUMBER = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_CASENUMBER$/is);
		$SUMMARY_PROPOSAL = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^SUMMARY_PROPOSAL$/is);
		$AUTHORITY_DECISION_STATUS = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AUTHORITY_DECISION_STATUS$/is);
		$AUTHORITY_DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^AUTHORITY_DECISION_DATE$/is);
		
		
		### Dates ###
		$DECISION_ISSUED_DATES = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_ISSUED_DATES$/is);
		$DECISION_MADE_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_MADE_DATE$/is);
		$DECISION_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_DATE$/is);
		$DECISION_PRINTED_DATE = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION_PRINTED_DATE$/is);
	
		### Details ###
		$DECISION = &htmlTagClean($activeTabContent{$AppDataKey}[0]) if($AppDataKey=~m/^DECISION$/is);
		
		
		if($Application eq '')
		{
			if($APPLICATION!~m/^\s*$/is)
			{
				$Application=$APPLICATION;
			}
			elsif($CASE_REFERENCE!~m/^\s*$/is)
			{
				$Application=$CASE_REFERENCE;
			}
			elsif($APPLICATION_REFERENCE!~m/^\s*$/is)
			{
				$Application=$APPLICATION_REFERENCE;
			}
			elsif($SUMMARY_CASENUMBER!~m/^\s*$/is)
			{
				$Application=$SUMMARY_CASENUMBER;
			}
		}
		
		if($Proposal=~m/^\s*$/is)
		{
			if($PROPOSAL!~m/^\s*$/is)
			{
				$Proposal=$PROPOSAL;
			}
			elsif($SUMMARY_PROPOSAL!~m/^\s*$/is)
			{
				$Proposal=$SUMMARY_PROPOSAL;
			}
		}
		
		if($Decision_Issued_Date=~m/^\s*$/is)
		{
			if($DECISION_ISSUED_DATE!~m/^\s*$/is)
			{
				$Decision_Issued_Date=$DECISION_ISSUED_DATE;
			}
			elsif($DECISION_ISSUED_DATES!~m/^\s*$/is)
			{
				$Decision_Issued_Date=$DECISION_ISSUED_DATES;
			}
		}	
		
		if($Decision_Status=~m/^\s*$/is)
		{
			if($DECISION_STATUS!~m/^\s*$/is)
			{
				$Decision_Status=$DECISION_STATUS;
			}
			elsif($DECISION!~m/^\s*$/is)
			{
				$Decision_Status=$DECISION;
			}
			elsif($AUTHORITY_DECISION_STATUS!~m/^\s*$/is)
			{
				$Decision_Status=$AUTHORITY_DECISION_STATUS;
			}
		}	
		
		if($Date_Decision_Made=~m/^\s*$/is)
		{
			if($DECISION_MADE_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DECISION_MADE_DATE;
			}
			elsif($DATE_DECISION_MADE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DATE_DECISION_MADE;
			}			
			elsif($DECISION_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DECISION_DATE;
			}
			elsif($DECISION_PRINTED_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$DECISION_PRINTED_DATE;
			}
			elsif($AUTHORITY_DECISION_DATE!~m/^\s*$|Not\s*Available[^>]*?$/is)
			{
				$Date_Decision_Made=$AUTHORITY_DECISION_DATE;
			}
		}

		if($Application_Status=~m/^\s*$/is)
		{
			if($APPLICATION_STATUS!~m/^\s*$/is)
			{
				$Application_Status=$APPLICATION_STATUS;
			}
		}	
	}
	
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	my $dateLength = length( $Date_Decision_Made );
	
	if($dateLength>50)
	{
		$Date_Decision_Made = '';
	}
	
	$Date_Decision_Made=~s/Not\s*Available[^>]*?//msi;
	$Decision_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Decision_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Decision\s*made)\,\s*view[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(Application\s*under\s*consideration)\;\s*use\s*t[\w\W]*?$/$1/msi;
	$Application_Status=~s/^\s*(The\sapplication\sis\snow\sawaiting\sprocessing)[\w\W]*?$/$1/msi;
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\')";
	
	undef $Application;  undef $Proposal;  undef $Application_Status;   undef $Decision_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	return($insert_query);	
}

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $category,$councilCode) = @_;
	
	my $tempURL = $receivedApplicationUrl;
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	sleep(5);
	if($category eq "Planning")
	{
		$regexFile=$Config;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$Config;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'CASE_REFERENCE'}/is)
	{
		$applicationNumber=$1;
	}
	elsif($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'APPLICATION_REFERENCE'}/is)
	{
		$applicationNumber=$1;
	}
	elsif($applicationDetailsPageContent=~m/$regexFile->{'summary'}->{'SUMMARY_CASENUMBER'}/is)
	{
		$applicationNumber=$1;
	}
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		$logName = $1 if($receivedApplicationUrl=~m/\&KeyVal\=([^\n]*?)$/is);
		
		if($logName eq "")
		{
			$logName = $1 if($receivedApplicationUrl=~m/KeyVal\=([^\&]*?)\&/is);
		}
		
		my $fileName= $category."_".$councilCode."_".$logName;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	my %dataDetails;
	if($tempURL=~m/activeTab=summary/is)
	{
		print "ActiveTab ==> Summary\n";
		my %activeTabContents = &tabDetailsCollection($applicationDetailsPageContent, "summary", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
		
	if(($councilCode!~m/^149$/is) && ($tempURL=~s/activeTab=summary/activeTab=details/is))
	{
		print "ActiveTab ==> Details\n";
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $detailsPageContent = $receivedCouncilApp->content;
		sleep(3);
		my %activeTabContents = &tabDetailsCollection($detailsPageContent, "details", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
		
	}
	
	if(($category ne "Decision") && ($tempURL=~s/activeTab=details/activeTab=contacts/is))
	{
		print "ActiveTab ==> Contacts\n";
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $contactsPageContent = $receivedCouncilApp->content;
		sleep(3);
		my %activeTabContents = &tabDetailsCollection($contactsPageContent, "contacts", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	elsif(($category eq "Decision") && ($tempURL=~s/activeTab=details/activeTab=dates/is))
	{
		print "ActiveTab ==> Dates\n";
		print "ActiveTabURL ==> $tempURL\n";
		my $keyVal=$1 if($tempURL=~m/keyVal=([^>]*?)\&activeTab=dates\s*$/is);
		$tempURL=~s/^([^>]*?)(keyVal=[^>]*?)\&(activeTab=dates)\s*$/$1$3\&$2/igs;
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $datesPageContent = $receivedCouncilApp->content;
		sleep(3);
		my %activeTabContents = &tabDetailsCollection($datesPageContent, "dates", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
		
	}
	
	if(($category eq "Planning") && ($tempURL=~s/activeTab=contacts/activeTab=dates/is))
	{
		print "ActiveTab ==> Dates\n";
		print "ActiveTabURL ==> $tempURL\n";
		$receivedCouncilApp->get($tempURL);
		my $datesPageContent = $receivedCouncilApp->content;
		sleep(3);
		my %activeTabContents = &tabDetailsCollection($datesPageContent, "dates", $category);
		%dataDetails = (%activeTabContents,%dataDetails);
	}
	return(\%dataDetails, $applicationDetailsPageResponse, $applicationNumber);
}

####
# Scraping of details for an given Tab and Application
####

sub tabDetailsCollection()
{
	my ($tabContent, $blockSection, $category) = @_;
	my %tabDetails;
	my $regexFile;
	
    if($category eq "Planning")
	{
		$regexFile=$Config;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$Config;
	}
	
	foreach my $detailsKey (keys %{$regexFile->{$blockSection}}) {
		if($tabContent=~/$regexFile->{$blockSection}->{$detailsKey}/is)
		{
			push ( @{ $tabDetails{$detailsKey}}, $1);
		}
	}
	
	return(%tabDetails);
}


sub createdirectory()
{
	my $dirname=shift;

	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}
