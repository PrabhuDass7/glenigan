use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;


my $Council_Code = $ARGV[0];
# my $From_Date = $ARGV[1];
# my $To_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. \(Eg: \"185\"\)", 16, "Error message");
    exit();
}

# print "From_Date: $From_Date\n";
# print "To_Date: $To_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();



### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Eastbourne/Eastbourne_Decision.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $SEARCH_VIEWSTATE_REGEX = $Config->{$Council_Code}->{'SEARCH_VIEWSTATE_REGEX'};
my $NEXT_PAGE_VIEWSTATE = $Config->{$Council_Code}->{'NEXT_PAGE_VIEWSTATE'};
my $NEXT_PAGE_REGEX = $Config->{$Council_Code}->{'NEXT_PAGE_REGEX'};
my $TOTAL_PAGES_REGEX = $Config->{$Council_Code}->{'TOTAL_PAGES_REGEX'};
my $APPLICATION_POST_CONTENT = $Config->{$Council_Code}->{'APPLICATION_POST_CONTENT'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $RESULT_URL = $Config->{$Council_Code}->{'RESULT_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $CSRFTOKEN_REGEX = $Config->{$Council_Code}->{'CSRFTOKEN_REGEX'};
my $TOKEN = $Config->{$Council_Code}->{'TOKEN'};
my $SEARCH_POST_CONTENT = $Config->{$Council_Code}->{'SEARCH_POST_CONTENT'};
my $CONTENT_TYPE = $Config->{$Council_Code}->{'CONTENT_TYPE'};
my $NEXT_POST_CONTENT = $Config->{$Council_Code}->{'NEXT_POST_CONTENT'};
my $DETAIL_POST_CONTENT = $Config->{$Council_Code}->{'DETAIL_POST_CONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";
	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0, autoclose => 1);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
    verify_hostname => 0, 
	autoclose => 1,
});
}	


### Proxy settings ###
if($Council_Code=~m/^(185)$/is)
{
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
}

# Get search results using date ranges

my ($Responce, $cookie_jar, $Ping_Status1);

$mech->get($HOME_URL);
sleep(10);

my $content = $mech->content;
my $code = $mech->status;
my $Home_Content=$content;

open(PP, ">app_cont.html");
print PP "$Home_Content\n";
close(PP); 
# exit;

my @Weekly_List_Urls;

if($Home_Content=~m/<h3>\s*Weekly\s*List\s*of\s*Decisions\s*<\/h3>\s*<ul>\s*([\w\W]*?)\s*<\/ul>/is)
{
	my $decDates = $1;
	while($decDates=~m/<a[^>]*?href=\"\.\.([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*<\/a>/gis)
	{
		my $decDatesURL = $1;
		my $decDatesRange = $2;
		$decDatesURL=~s/\&amp\;/\&/gsi;
		my $decListURL;
		if($decDatesURL!~m/^\s*$/is)
		{
			if($decDatesURL!~m/https?\:/is)
			{
				$decListURL = $FILTER_URL.$decDatesURL;
			}	
			else
			{
				$decListURL = $decDatesURL;
			}	
		}
		push(@Weekly_List_Urls,$decListURL);
	}
}

my $a=1;
my $Reslts_Count=0;

my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

for my $Week_Url(@Weekly_List_Urls)
{
	print "\nWeek_Url=>$Week_Url\n";
	
	$mech->get($Week_Url);
	sleep(10);
	my $dateContent = $mech->content;
	my $dateSearchCode = $mech->status;
	# print "datePageresponceCode=>$dateSearchCode\n";
		
	open(PP, ">dateContent$a.html");
	print PP "$dateContent\n";
	close(PP); 
	# exit;
	
	$a++;
	
	my ($ViewState,$CSRFToken,$Token,$Number_Pages,$ViewState_key);
	
	my $Page_Number = 1;
	
	NextPage:
	if($dateContent=~m/$SEARCH_VIEWSTATE_REGEX/is)
	{
		$ViewState_key=uri_escape($1);
		$ViewState=uri_escape($2);
	}
	if($dateContent=~m/$TOKEN/is)
	{
		$Token=uri_escape($1);
	}		
	if($dateContent=~m/$CSRFTOKEN_REGEX/is)
	{
		$CSRFToken=uri_escape($1);
	}	
	
	while($dateContent=~m/$TOTAL_PAGES_REGEX/gsi)
	{
		$Number_Pages=$1;
	}	
	
	print "Number of Pages: $Number_Pages\n";
	
	while($dateContent=~m/$APPLICATION_REGEX/igs)
	{
		my $Reference_ID=$1;
		my $Application_Reference=$2;
		print "Application_Reference: $Application_Reference\n";
					
		
		if($DETAIL_POST_CONTENT ne "")
		{
			$DETAIL_POST_CONTENT =~s/<ViewState>/$ViewState/igs;
			$DETAIL_POST_CONTENT =~s/<Reference_ID>/$Reference_ID/igs;				
			$DETAIL_POST_CONTENT =~s/<Reference>/$Application_Reference/igs;				
			$DETAIL_POST_CONTENT =~s/<Token>/$Token/igs;
			$DETAIL_POST_CONTENT =~s/<CSRFToken>/$CSRFToken/igs;
		}
		
		$mech->post( $RESULT_URL, Content => "$DETAIL_POST_CONTENT");	
		my $App_content = $mech->content;
		my $Search_Code = $mech->status;

		if($Search_Code=~m/200/is)
		{
			$Reslts_Count++;
		}	
		
		# print "Search_Code=>$Search_Code\n";
				
		# open(PP,">App_Detail_content$a.html");
		# print PP "$App_content\n";
		# close(PP);
		# $a++;
		# exit;
				
		my ($insert_query1)=&Scrape_Details($Council_Code,$App_content,$Search_Code,$RESULT_URL,$Application_Reference);
		$insert_query.=$insert_query1;
		
		$DETAIL_POST_CONTENT =~s/$ViewState/<ViewState>/igs;
		$DETAIL_POST_CONTENT =~s/$Reference_ID/<Reference_ID>/igs;				
		$DETAIL_POST_CONTENT =~s/$Application_Reference/<Reference>/igs;				
		$DETAIL_POST_CONTENT =~s/$Token/<Token>/igs;
		$DETAIL_POST_CONTENT =~s/$CSRFToken/<CSRFToken>/igs;
	}

	if($Number_Pages > $Page_Number)
	{
		if($NEXT_POST_CONTENT ne "")
		{
			$NEXT_POST_CONTENT =~s/<ViewState>/$ViewState/igs;				
			$NEXT_POST_CONTENT =~s/<Token>/$Token/igs;
			$NEXT_POST_CONTENT =~s/<CSRFToken>/$CSRFToken/igs;
		}
		
		$mech->post( $RESULT_URL, Content => "$NEXT_POST_CONTENT");	
		my $App_contents = $mech->content;
		my $Search_Code = $mech->status;
		$dateContent=$App_contents;

		if($Search_Code=~m/200/is)
		{
			$Reslts_Count++;
		}	
		
		# print "Search_Code=>$Search_Code\n";
		
		# open(PP,">App_Detail_content$Page_Number.html");
		# print PP "$App_contents\n";
		# close(PP);
		# exit;
		
		
		$NEXT_POST_CONTENT =~s/$ViewState<ViewState>//igs;				
		$NEXT_POST_CONTENT =~s/$Token/<Token>/igs;
		$NEXT_POST_CONTENT =~s/$CSRFToken/<CSRFToken>/igs;
		
		$Page_Number++;
		goto NextPage;
	}
}

$insert_query=~s/\,$//igs;

if($insert_query!~m/values\s*$/is)
{
	print "\n\ninsert_query::$insert_query\n";
	&DB_Insert($dbh,$insert_query);
}

sub Scrape_Details
{
	my $Council_Code=shift;
	my $App_Page_Content=shift;
	my $codes=shift;
	my $Page_url=shift;	
	my $App_Num=shift;	
	my $insert_query;
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	my $Application = &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Application\s*Reference\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
	my $ProPosal = &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Application\s*Description\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>/is);
	my $Date_Decision_Made	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Decision\s*Date\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);	
	my $Decision_Status	= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<td[^>]*?>\s*Decision\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
	my $Application_Status;
	my $Application_Link=$Page_url;
	my $Source = "GCS001";	
	
	if($Application=~m/^\s*$/is)
	{
		$Application=$App_Num;
	}
	
	print "Application=>$Application\n";
	print "ProPosal=>$ProPosal\n";
	print "Date_Decision_Made=>$Date_Decision_Made\n";
	print "Decision_Status=>$Decision_Status\n\n";
	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
	$insert_query.="(\'$Application\', \'$ProPosal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	undef $Application; undef $ProPosal;  undef $Decision_Status; undef $Date_Decision_Made;  undef $Application_Status; undef $Application_Link; 		
	
	
	return($insert_query)
}



sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}