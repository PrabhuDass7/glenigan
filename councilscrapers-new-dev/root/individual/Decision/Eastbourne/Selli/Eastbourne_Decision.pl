use strict;
use DBI;
use DBD::ODBC;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use Time::Piece;
use Config::Tiny;
use Selenium::Remote::Driver;
use Selenium::Waiter qw/wait_until/;



# Establish connection with DB server
my $dbh = &DbConnection();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "hour: $hour\n";
my $Schedule_no;
if($hour < 12)
{
	$Schedule_no=1;
}
else
{
	$Schedule_no=2;
}


my $Council_Code = $ARGV[0];
my $input_from_date = $ARGV[1];
my $input_to_date = $ARGV[2];


###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=   'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}


### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Eastbourne/Eastbourne_Decision.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};

#### 
# my $driver = Selenium::Remote::Driver->new();
my $driver = Selenium::Remote::Driver->new( browser_name => 'chrome',auto_close => 0);
$driver->set_window_size(1080,1920,auto_close => 0);
$driver->maximize_window();


print "HOME_URL==>$HOME_URL\n";
print "Council_Code: $Council_Code\n";

$driver->get($HOME_URL);
sleep(10);
my $model_cont=$driver->get_page_source($driver);


$driver->find_element_by_xpath("//*[\@id=\"ac1\"]")->click;
sleep(5);	
$driver->find_element_by_xpath("//*[\@id=\"faux\"]/div[1]/div/form/fieldset/div/div/div[3]/input")->click;
sleep(5);	
my $temp1=$driver->get_page_source($driver);

# open(PPP,">Search_Content.html");
# print PPP "$model_cont\n";
# close(PPP);
# exit;


$driver->find_element("//*[\@id=\"view638\"]")->send_keys("$input_from_date");
sleep(5);
print "FromDate: $input_from_date\n";

$driver->find_element("//*[\@id=\"view639\"]")->send_keys("$input_to_date");
sleep(5);	
print "ToDate: $input_to_date\n";

$driver->find_element_by_xpath("/html/body/form/div[2]/div/div/div[3]/div/div[2]/div/div/button")->click;
# $driver->find_element_by_xpath("//*[\@id=\"espr_renderHost_PageStructureDisplayRenderer_esctl_99555ec1-e970-462d-957c-7eaa8d561fcc_javavscriptWidget_advancedsearch\"]/div/button/span")->click;
sleep(10);	

my $searchContent=$driver->get_page_source($driver);

# open FH, ">Home.html";
# print FH $searchContent;
# close FH;
# exit;



my $id = $1 if($searchContent=~m/<div\s*id=\"(espr_renderHost_PageStructureDisplayRenderer[^\"]*?javavscriptWidget_results)\">/is);

my $page=2;
my @totalAppLinks;
Next_Page:
while($searchContent=~m/<a[^>]*?href=\"([^\"]*?KeyText=[^\"]*?)\"\s*>\s*Planning\s*Application\s*([^>]*?)\s*\-[^>]*?<\/a>/gsi)
{
	my $ApplicationLink = $1;
	my $Application = $2;
	
	print "\nApplication Number is Processing::$Application\n";		
	
	if($ApplicationLink!~m/^http/is)
	{	
		$ApplicationLink = $FILTER_URL.$ApplicationLink;
	}	
	else
	{
		$ApplicationLink;
	}	
	
	
	$ApplicationLink=~s/\&amp\;/\&/gsi;
	
	
	push(@totalAppLinks,$ApplicationLink);
}	
	
if($searchContent=~m/<div\s*class=\"btn\s*secondary-btn\"[^>]*?>\s*Next\s*<\/div>/si)
{
	$driver->find_element_by_xpath("//*[\@id=\"$id\"]/div/div/div[2]/div/div/div")->click;
	sleep(7);	
	my $content=$driver->get_page_source($driver);
	$searchContent = $content;
	
	print "\#\#\#\#$page page content\#\#\#\#\n";
	
	$page++;
	
	goto Next_Page;
}

foreach my $current_URL (@totalAppLinks)
{
	$driver->get($current_URL);	
	sleep(5);
	print "$current_URL\n";
	
	my $content=$driver->get_page_source($driver);
	$content=~s/<\!\-\-[^<]*?\-\->//gsi;
	
	# open FH, ">Output-$a.html";
	# print FH $content;
	# close FH;
	# exit;
	
# =pod		
		
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	my $Application_Number = &clean($1) if($content=~m/>\s*Planning\s*Application\s*([^>]*?)\s*\-/is);
	my $Application_Status = &clean($1) if($content=~m/>\s*Stage\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Date_Decision_Made = &clean($1) if($content=~m/>\s*Decision\s*Date\s*<\/div>\s*<div[^>]*?>\s*([^<]*?)\s*<\/div>/is);
	my $Proposal = &clean($1) if($content=~m/>\s*Application\s*Description\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is);
	my $Decision_Status = &clean($1) if($content=~m/>\s*Decision\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is);
	
	my $Application_Link = $current_URL;
	my $Date_Decision_Dispatched;
	my $Doc_Url = "";
	
	
	my $Source = "GCS001";	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
	
	# Create insert query for required columns
	my $insert_query="insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values (\'$Application_Number\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Date_Decision_Dispatched\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\')";
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&DB_Insert($dbh,$insert_query);
	}
	
	undef $Application_Number;  undef $Proposal;  undef $Decision_Status;  undef $Date_Decision_Made;  undef $Date_Decision_Dispatched;  undef $Application_Link; undef $Application_Status;
			
		
# =cut	

}


$driver->quit();
$driver->close();

sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/[^[:print:]]+/ /igs;	
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	$values=~s/^\W+$//igs;	
	$values=~s/\'/\'\'/gsi;
	return($values);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}