# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
from datetime import datetime, timedelta
import pymssql
import ssl
from urlparse import urljoin

# import selenium.webdriver.chrome.service as service

reload(sys)
sys.setdefaultencoding("utf-8")

# ssl._create_default_https_context = _create_unverified_https_context

folder_path = 'C:/Glenigan/Live_Schedule/Decision/Anglesey'

# dbConnection Section
def dbConnection(database):
	conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
	return (conn)

# daterange search section	
def inputsection(fromdate, todate, driver, homeURL):
	try:
		#-- Parse
		driver.get(homeURL)
		driver.maximize_window()
		#-- Wait funtion
		time.sleep(10)
		content=driver.page_source
		
		advfinalCnt=''
		if len(content) > 0:
			advfinalCnt = content.encode("utf-8")

		
		driver.find_element_by_xpath('//*[@id="32:148;a"]').send_keys('PApplication')
		time.sleep(10)
		advcontent=driver.page_source
		
		fDate = re.findall(">\s*Decision\s*Date\s*From\s*(?:<[^>]*?>)?\s*<\/label>\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advcontent), re.IGNORECASE)  
		fromDate = '//*[@id="'+str(fDate[0])+'"]'
		

		tDate = re.findall(">\s*Decision\s*Date\s*To\s*(?:<[^>]*?>)?\s*<\/label>\s*(?:<[^>]*?>)?\s*<div[^>]*?>\s*<input[^>]*?id=\"([^\"]*?)\"[^>]*?>", str(advcontent), re.IGNORECASE) 
		toDate = '//*[@id="'+str(tDate[0])+'"]'	
		
		driver.find_element_by_xpath(fromDate).send_keys(fromdate)
		#-- Wait funtion
		time.sleep(5)  
		

		driver.find_element_by_xpath(toDate).send_keys(todate)
		#-- Wait funtion
		time.sleep(5)  

		driver.find_element_by_xpath('//*[@id="254:0"]/div/div/div/div/div/div/div/div[2]/button').click()
		#-- Wait funtion
		time.sleep(10)  

		searchcontent=driver.page_source
		searchfinalCnt=''
		if len(searchcontent) > 0:
			searchfinalCnt = searchcontent.encode("utf-8")
				
		return searchfinalCnt

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno



# To collect Application URLs here
def collectAppURL(content, home_url):	
	try:		 
		regionURLs=[]
		if re.findall(r'<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?data-aura-class=\"uiOutputURL\"[^>]*?>\s*[^>]*?\s*<\/a>\s*<\/td>', str(content), re.IGNORECASE):
			regexMatched=re.findall(r'<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\"[^>]*?data-aura-class=\"uiOutputURL\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/td>', str(content), re.IGNORECASE)       
			
			for appDetails in regexMatched:
				appURL,appNum = appDetails
				if not re.findall(r'^http', str(appURL)):
					appURL = urljoin(home_url,appURL)
				appNumber = re.sub(r'\/', "", str(appNum))
				# print("appNumber::",appNumber)
				appURL=appURL+'/'+appNumber+'?language=en_GB'
				# print("AfterappURL::",appURL)
				regionURLs.append(appURL)
				
				
		else:
			print("ERROR!!!!!")
			
		return regionURLs	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 



# To collect Application URLs here
def collectAppDetails(fullAppURLs, home_url, driver, CouncilCode, Source, conn, cursor):	
	try:		 
		bulkValuesForQuery=''
		
		appCount = 0
		insertQuery = 'insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values '
		
		for appURL in fullAppURLs:
			print("Now processing this URL::",appURL)
			driver.get(appURL)
			time.sleep(10)  
			Appcontent=driver.page_source
			# with open(folder_path+"/456_out.html", 'wb') as fd:
				# fd.write(Appcontent)
			# sys.exit()
			
			Application_Number=re.findall(r'>\s*Planning\s*Application\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Decision_Status=re.findall(r'>\s*Decision\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Date_Decision_Made=re.findall(r'>\s*Last\s*Decision\s*Date\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Application_Status=re.findall(r'>\s*Decision\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			Proposal=re.findall(r'>\s*Proposal\s*[^<]*?<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([\w\W]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			# Easting=re.findall(r'>\s*Easting\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			# Northing=re.findall(r'>\s*Northing\s*<\/span>\s*<\/div>\s*<div[^>]*?>\s*<span[^>]*?>\s*<span[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/span>', str(Appcontent), re.IGNORECASE)
			
			(Application, ProposalCnt, ApplicationStatus, DecisionStatus, DateDecisionMade) = ('','','','','')

			if len(Application_Number) > 0:
				Application=clean(Application_Number[0])
			
			if len(Proposal) > 0:
				ProposalCnt=clean(Proposal[0])
				
			if len(Application_Status) > 0:
				ApplicationStatus=clean(Application_Status[0])

			if len(Decision_Status) > 0:
					DecisionStatus=clean(Decision_Status[0])

			if len(Date_Decision_Made) > 0:
				DateDecisionMade=clean(Date_Decision_Made[0])
				
			format1 = "%Y/%m/%d %H:%M"					
			Schedule_Date = datetime.now().strftime(format1)

			Source_With_Time = Source+"_"+Schedule_Date+"-perl"
			
			Council_Name = "Anglesey"
			joinValues="('"+str(Application)+"','"+str(ProposalCnt)+"','"+str(DecisionStatus)+"','"+str(DateDecisionMade)+"','"+str(CouncilCode)+"','"+str(appURL)+"','"+str(Source_With_Time)+"','"+str(ApplicationStatus)+"','"+str(Schedule_Date)+"')"
			
			# print "ApplicationValues: ", joinValues
			bulkValuesForQuery = insertQuery+joinValues
			cursor.execute(bulkValuesForQuery)
			conn.commit()
			bulkValuesForQuery=''

			# if appCount == 0:
				# bulkValuesForQuery = insertQuery+joinValues
				# appCount += 1
			# elif appCount == 5:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# cursor.execute(bulkValuesForQuery)
				# conn.commit()
				# appCount=0
				# bulkValuesForQuery=''
			# else:
				# bulkValuesForQuery = bulkValuesForQuery+","+joinValues
				# appCount += 1	
				
		print(bulkValuesForQuery)	
		return bulkValuesForQuery	
		
	except Exception as e:
		print e,sys.exc_traceback.tb_lineno 		

# nextPageSection parse section	
def nextPageSection(searchcontent, homeURL, driver):
	try:
		
		pageNumber = 2
		pageNum = 4
		fullQuery=''
		
		searchcontent = re.sub(r'\\t', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\n', " ", str(searchcontent)) 
		searchcontent = re.sub(r'\\r', "", str(searchcontent)) 
		
		AppURLs=[]
		if re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*'+str(pageNumber)+'\s*(?:<[^>]*?>)?\s*<\/button>', str(searchcontent)):
			fullURL = collectAppURL(str(searchcontent), homeURL)
			AppURLs.extend(fullURL)
						
			check_next_page = re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(searchcontent), re.I)
			print("Page::::",check_next_page)
			while (check_next_page[0]):	
				if re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(searchcontent), re.I):					
					driver.find_element_by_xpath('//*[@id="PApplication"]/div[2]/div/button['+str(pageNum)+']').click()	
					#-- Wait funtion
					time.sleep(10)  
					appNextPagecontent=driver.page_source
					
					
					fullURL = collectAppURL(str(appNextPagecontent), homeURL)	
					AppURLs.extend(fullURL)
				
					pageNumber = pageNumber + 1   
					pageNum = pageNum + 1   
					
					if re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(appNextPagecontent), re.I):
						check_next_page = re.findall(r'<button[^>]*?>\s*(?:<[^>]*?>)?\s*('+str(pageNumber)+')\s*(?:<[^>]*?>)?\s*<\/button>', str(appNextPagecontent), re.I)
				
						print "pageNumber:",pageNumber              
									 
						if check_next_page != "":
							continue
						else:
							break
				else:
					break
		
		else:
			fullURL = collectAppURL(str(searchcontent), homeURL)		
			AppURLs.extend(fullURL)
			
		return AppURLs

	except Exception as e:
		print e,sys.exc_traceback.tb_lineno

# Clean function
def clean(cleanValue):
    try:
		clean=''
		
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
        
		return clean    

    except Exception as ex:
        print ex,sys.exc_traceback.tb_lineno

# Main section	
if __name__== "__main__":
	
	councilcode = sys.argv[1]
	fromdate = sys.argv[2]	
	todate = sys.argv[3]		
		
	conn = dbConnection("GLENIGAN")	
	cursor = conn.cursor()

	PROXY = "172.27.137.199:3128" # IP:PORT or HOST:PORT

	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

	
	#-- Setup
	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument('--log-level=3')
	browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/chromedriver.exe')
		
		
	print 'fromdate  :', fromdate
	print 'todate    :', todate
	gcs = 'GCS001'	
	
	homeURL = 'https://ioacc.force.com/s/pr-english?language=en_GB&tabset-8996a=11112'
	
	results = inputsection(fromdate, todate, browser, homeURL)

	fullAppURLs = nextPageSection(results, homeURL, browser)
	
	finalinsertQuery = collectAppDetails(fullAppURLs, homeURL, browser, councilcode, gcs, conn, cursor)
	# print type(finalinsertQuery)
	
	if (finalinsertQuery != ''):
		# print (finalinsertQuery)
		cursor.execute(finalinsertQuery)
		conn.commit()
	
	browser.quit()