use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::Entities;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use FileHandle;

# Establish connection with DB server
my $dbh = &DbConnection();


my $cookie_jar= HTTP::Cookies->new(file=>$0."_cookie.txt",autosave => 1,);
my $ua=LWP::UserAgent->new(show_progress=>1);

$ua->agent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect('7');
my $file=$0."_cookie.txt";
my $cookie=HTTP::Cookies->new(file=>$file,autosave=>1,);
$ua->cookie_jar($cookie);

BEGIN {
 $ENV{HTTPS_PROXY} = 'https://172.27.137.199:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

my $spefication_url="https://www.anglesey.gov.uk/en/Residents/Planning-building-control-and-conservation/Planning/Planning-decisions.aspx";
my $content=&Getcontent($spefication_url,"GET","","");

# open FH, ">HomeContent.html";
# print FH $content;
# close FH;
# exit;

if($content=~m/<ul\s*class=\"download-resource\">\s*([\w\W]*?)\s*<\/ul>\s*<\/div>\s*<\/div>/is)
{
	my $sub_content=$1;

	while($sub_content=~m/<span[^>]*?>\s*<a\s*href\=\"\s*([^\"]*?)\s*\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/span>/igs)
	{
		my $pdf_url=$1;
		my $file_name=$2;
		$file_name=~s/[^A-z\d]+//isg;	
		
		print "\nfile_name--->$file_name\n";
		
		my $pdm_name1=$file_name.'.pdf';
		my $somedir="C:/Glenigan/Live_Schedule/Decision/Anglesey/export";
		
		opendir(DIR, $somedir) || die "can't opendir $somedir: $!";
		my @files = grep { (!/^\./) && -f "$somedir/$_" } 
		readdir(DIR);
		closedir DIR;
		
		next if($pdm_name1~~@files);	
	
		
		my $pdf_content =&Getcontent("$pdf_url","","","GET");
		print "\n$pdf_url\n";
			
		my $fh = FileHandle->new("C:/Glenigan/Live_Schedule/Decision/Anglesey/export/$file_name.pdf", 'w') or die "Cannot open $file_name.pdf for write :$!";
		binmode($fh);
		$fh->print($pdf_content);
		$fh->close();
		
		my $filename1 = "C:/Glenigan/Live_Schedule/Decision/Anglesey/export/$file_name.txt";			
		
		system("C:/Glenigan/Live_Schedule/Decision/Anglesey/pdftotext.exe \-raw \"C:/Glenigan/Live_Schedule/Decision/Anglesey/export/$file_name.pdf\"");
		sleep(10);
		
		undef $/;
		
		open KV, "$filename1";
		my $text=<KV>;
		close KV;
# =pod			
		my $front="";
		if($text=~m/^\s*([^<]*?)\s*Rhif\s+Cais\s+/is)
		{
			$front=$1;
		}
		$text=~s/$front//gsi;
		$text=~s///gsi;
# =cut			
		open FH, ">$filename1";
		print FH $text;
		close FH;
		
		# exit;			
		
					
		&Scrape_Details($text,"456",$pdf_url);
				
	
						
	}	
}

sub Scrape_Details()
{
    my $html_content=shift;
    my $Council_Code=shift;  
    my $PDF_URL=shift;  
   
	my $time = Time::Piece->new;   
	my $ScheduleDateTime= $time->strftime('%Y/%m/%d %H:%M');
    my $Source = "GCS001";  
    my $COUNCIL_NAME = "ISLE OF ANGLESEY";  
    my $Source_With_Time=$Source."_".$ScheduleDateTime."-perl";  
   	
	
	my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';

    my(@Applicationss,@Case_officerss,@Proposalss,@Applicant_namess,@Addressss,@Date_Decision_Mades,@Decision_Statuss);
	
    my($Application,$Case_officer,$Applicant_name,$Proposal,$Address,$Date_Application_Received,$Application_Type,$Date_Application_validated,$Date_Application_Registered,$Application_Status,$Agent_Name,$Applicant_Address,$Agent_Telephone,$TargetDec_dt,$Schedule_Date,$Document_Url,$PAGE_URL,$Agent_Email,$Doc_Url,$Agent_Address,$Application_Link,$Decision_Status,$Date_Decision_Made); 
	
    while($html_content=~m/Rhif\s*Cais\s*[\w\W]*?\s*Application\s*No\:\s*([^\n]*?)\s*(?:\n|Cyf)/igs)
    {
        $Application=&clean($1);
        push(@Applicationss,$Application);
    }
    while($html_content=~m/Bwriad\s*[\w\W]*?\s*Proposal\s*\:\s*[^>]*?([\w\W]*?)\s*Lleoliad\s+\/\s+Location/gs)
    {
        $Proposal=&clean($1);
        push(@Proposalss,$Proposal);
    }
    while($html_content=~m/Decision\s*Date\s*(?:Issued)?\s*([^\n]*?)\s*(?:\n|Rhif|Diwedd)/igs)
    {
        $Date_Decision_Made=&clean($1);
		push(@Date_Decision_Mades,$Date_Decision_Made);
    }
	while($html_content=~m/Decision\s+([^\n]*?)\s*(?:Dyddiad)/igs)
    {
        $Decision_Status=&clean($1);		
		push(@Decision_Statuss,$Decision_Status);
    }
	
	$Application_Link=$PDF_URL;
	$Schedule_Date=$ScheduleDateTime;
	for(my $i=0;$i<=$#Applicationss;$i++)
    {		     
		$insert_query.="(\'$Applicationss[$i]\', \'$Proposalss[$i]\', \'$Decision_Statuss[$i]\', \'$Date_Decision_Mades[$i]\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
       
        undef $Applicationss[$i]; undef $Proposalss[$i];  undef $Decision_Statuss[$i]; undef $Date_Decision_Mades[$i]; undef $Application_Status;    
    }
    $insert_query=~s/\,$//igs;
    
    print "insert_query::$insert_query\n";

    if($insert_query!~m/values\s*$/is)
    {
        &DB_Insert($dbh,$insert_query);
    }
	 
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}



###### DB Connection ####
sub DbConnection()
{
    my $dsn                          =  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
    my $dbh                          =    DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
   
    if(!$dbh)
    {
        &DBIconnect($dsn);
    }
    else
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}

#####GET Content####
sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	
	Home:
	my $req = HTTP::Request->new(GET=>$url);	
	$req->header("Content-Type"=> "text/html; charset=UTF-8");
	
	my $res = $ua->request($req);
	
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	
	my $code=$res->code;
	my $status_line=$res->status_line;	
	
	my $Content_Disposition=$res->header("Content-Disposition");

	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content);
}	