use strict;
use WWW::Mechanize;
use WWW::Mechanize::Firefox;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use URI::Escape;
use URI::URL;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;


my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"99\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


print "Council_Code: $Council_Code\n";
print "Date_Range: $Date_Range\n";
print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";


### Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();

system('"C:/Program Files/Mozilla Firefox/firefox.exe"');

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Ashford/Ashford_Decision.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $SRCH_NXT_PAGE = $Config->{$Council_Code}->{'SRCH_NXT_PAGE'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";

#### UserAgent Declaration ####	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize::Firefox->new(autocheck => 0);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize::Firefox->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			autoclose => 1,
			});
}	


### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.192:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

# Get search results using date ranges

my ($Responce, $javascript, $Ping_Status1);
$Responce = $mech->get($HOME_URL);
sleep(10);

eval
{
	$mech->click({ xpath => "//*[\@id=\"CPH_Details_Agree_CheckBox\"]" });
	$mech->click({ xpath => "/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div/table/tbody/tr[3]/td[2]/input" });
	$mech->click({ xpath => "/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[1]/input[2]" });
};

$mech->click({ xpath => "/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[2]/div/div[1]/span[3]/span/span/a/span" });

sleep(5);
my $contents = $mech->content;
my $Home_Content=$contents;

# open(PPP,">Home_Content.html");
# print PPP "$Home_Content\n";
# close(PPP);
# exit;



my $dumFrom_Date = $From_Date;
my $dumTo_Date = $To_Date;

my ($sDate,$sMonth,$sYear,$twosYear,$twoeYear); 

my %month = ('01' =>'Jan', '02' => 'Feb', '03' => 'Mar','04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');

if($dumFrom_Date=~m/^(\d+)\/(\d+)\/(\d+)$/si)
{
	$sDate=sprintf("%01d",$1);
	$sMonth=$month{$2};
	$sYear=$3;
	my $Year= $1 if($sYear=~m/(\d\d)$/si);
	$twosYear = $Year;

}


my ($eDate,$eMonth,$eYear); 

if($dumTo_Date=~m/^(\d+)\/(\d+)\/(\d+)$/si)
{
	$eDate=sprintf("%01d",$1);
	$eMonth=$month{$2};
	$eYear=$3;
	my $Year= $1 if($eYear=~m/(\d\d)$/si);
	$twoeYear = $Year;
}

my $t = localtime;
my $currentMonth = $t->month;
print "currentMonth==>$currentMonth\n";
print "From Date==>$sDate\/$sMonth\/$sYear\n";
print "To Date==>$eDate\/$eMonth\/$eYear\n";


$mech->form_number($FORM_NUMBER);
$mech->set_fields('ctl00$CPH_Details$Details_TabContainer$Date_Tab$searchby_date1$DateSearchBy' => 'Decision Date');	
$mech->set_fields('ctl00$CPH_Details$Details_TabContainer$Date_Tab$searchby_date1$StartDate$DD_Month' => $sMonth);
sleep(10);
$mech->set_fields('ctl00$CPH_Details$Details_TabContainer$Date_Tab$searchby_date1$StartDate$DD_Year' => $sYear);
sleep(5);
# $mech->follow_link( tag => 'a', text => $sDate );
$mech->follow_link( url_regex => qr/javascript\:__doPostBack\(\'(([^<]*?StartDate)\$myCalendar)\'/i, text => $sDate );
sleep(10);

$mech->form_number($FORM_NUMBER);
$mech->set_fields('ctl00$CPH_Details$Details_TabContainer$Date_Tab$searchby_date1$EndDate$DD_Month' => $eMonth);
sleep(10);
$mech->set_fields('ctl00$CPH_Details$Details_TabContainer$Date_Tab$searchby_date1$EndDate$DD_Year' => $eYear);
sleep(5);
$mech->follow_link( url_regex => qr/javascript\:__doPostBack\(\'(([^<]*?EndDate)\$myCalendar)\'/i, text => $eDate );
sleep(10);
$mech->click({ xpath => "/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div[1]/div/div[1]/div/input[1]" });
sleep(10);

my $content = $mech->content;
my $code = $mech->status;
print "$code\n";
my $Search_Content=$content;
	
# open(PPP,">Search_Content1.html");
# print PPP "$Search_Content\n";
# close(PPP);
# exit;

my @Detail_Links = ();
my ($Detail_Links) =&LinkCollection($Search_Content);
@Detail_Links = @$Detail_Links;

my $totalPage = $1 if($Search_Content=~m/$TOTAL_PAGE_COUNT/is);

my $i=2;

next_page:
if($i<=$totalPage)
{	
	print "$i======$totalPage\n";	
	$mech->click({ xpath => "/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div[3]/table/tbody/tr/td[1]/div/table/tbody/tr/td[6]/a/img" });
	sleep(5);
	my $datePage_url = $mech->uri();
	
	my $nextpagecontent = $mech->content;
	
	# open(PP,">Search_Content$i.html");
	# print PP "$nextpagecontent\n";
	# close(PP);
	
	my ($Detail_Links) = &LinkCollection($nextpagecontent);
	@Detail_Links = @$Detail_Links;
	
	$i++;
	goto next_page;
}

&Scrape_Details( $Council_Code,\@Detail_Links );

# system("taskkill /IM firefox.exe /F");

sub Scrape_Details
{
	my $Council_Code = shift;
	my $Detl_Links = shift;
	my @Detail_Links = @{$Detl_Links};
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
    my $Search_Results_Count = scalar(@Detail_Links);
	
	my $code;
	
	
	# my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values';
	
	foreach my $Application_Link(@Detail_Links)
	{
		print "Application_Link==>$Application_Link\n";
		
		my $Application; my $Proposal; my $Application_Status; my $Date_Decision_Made; my $Decision_Status; my $Source_With_Time;
		
		# my ($code,$App_Page_Content)=&getMechCont($Application_Link);
		$mech->get($Application_Link);
		sleep(7);
		my $code= $mech->status();
		my $App_Page_Content = $mech->content;
		
		
		if($Council_Code=~m/^99$/is)
		{
			$Application					= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Application\s*Ref\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>/is);
			$Proposal						= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Proposal\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([\w\W]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>/is);			
			if($Proposal eq "")
			{
				$Proposal = &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Description\s*of\s*Works\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([\w\W]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>/is);
			}
			$Application_Status				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Status\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>/is);
			$Date_Decision_Made				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Decision\s*Date\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>/is);	
			$Decision_Status				= &clean($1) if($App_Page_Content=~m/<tr[^>]*?>\s*<th[^>]*?>\s*Council\s*Decision\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>/is);
		}
				
		my $Source = $Date_Range;	
				
		$Source_With_Time=$Source."_".$Schedule_Date."-perl";
		
		my $insert_query = "insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values (\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\')";
		
		print "insert_query::$insert_query\n";
		
		if($insert_query!~m/values\s*$/is)
		{
			&Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
		}
		
		undef $Application; undef $Proposal  ;  undef $Decision_Status; undef $Date_Decision_Made; undef $Application_Status; undef $Application_Link; 
	}
	# $insert_query=~s/\,$//igs;
		
	# print "insert_query::$insert_query\n";
		
	# if($insert_query!~m/values\s*$/is)
	# {
		# &Glenigan_DB_Windows::DB_Insert($dbh,$insert_query);
	# }
}


sub getMechCont
{
	my $Link = shift;

	$mech->get($Link);
	sleep(7);
	my $content = $mech->content;
	
	
	my $code= $mech->status();
	print "CODE :: $code\n";
	my $Link_Reset_Status = $mech->success();
	
	return ($code,$content);
}

sub LinkCollection
{
	my $page_cnt = shift;
	
	while($page_cnt=~m/<tr[^>]*?>\s*<td[^>]*?>\s*<a[^>]*?href=\"([^\"]*?)\">\s*([^<]*?)\s*<\/a>\s*<\/td>/igs)
	{
		my $App_Link = $1;
		my $App_Number = $2;
		
		my $Application_Link;
		if($App_Link!~m/^\s*$/is)
		{
			if($App_Link!~m/https?\:/is)
			{
				$Application_Link = $FILTER_URL.$App_Link;
			}	
			else
			{
				$Application_Link = $App_Link;
			}	
		}
		push(@Detail_Links,$Application_Link);		
	}
	
	return(\@Detail_Links);
	
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}