# -*- coding: utf-8 -*-
import requests as req
import json
import sys,os,re
import pandas as pd
from datetime import datetime, timedelta
import pymssql
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import os, re, sys
import ssl
# from urlparse import urljoin
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup

reload(sys)
sys.setdefaultencoding("utf-8")

basePath = os.getcwd()
print ("basePath::",basePath)

todaydate = time.strftime('%Y%m%d')
logDirectory=basePath+"/log/"+todaydate
if os.path.isdir(str(logDirectory)) == False:
    os.makedirs(str(logDirectory))
    
proxies1 = {
  'http': 'http://172.27.137.192:3128',
  'https': 'http://172.27.137.192:3128',
}

# dbConnection Section
def dbConnection(database):
    conn = pymssql.connect(server='CH1025BD03', user='User2', password='Merit456', database=database)
    # conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
    return (conn)

# Insert Query
def dbInsert(InsertQuery,queryType):
    
    try:
        # QueryTxt=logDirectory+"/"+'Dump_Query_'+councilcode+"_"+gcs+".txt"
        # with open(QueryTxt, 'a') as fd:
            # fd.write(str(InsertQuery)+"\t"+str(queryType)+"\n")
        cursor.execute(InsertQuery)
        conn.commit()
        print ("*** DB Insertion Succesfully ***")
        
        # QueryTxt=logDirectory+"/"+'Success_Query_'+councilcode+"_"+gcs+".txt"
        # with open(QueryTxt, 'a') as fd:
            # fd.write(str(InsertQuery)+"\t"+str(queryType)+"\n")
    except Exception as e:
        QueryTxt=logDirectory+"/"+'Failed_Query_'+councilcode+"_"+gcs+".txt"
        with open(QueryTxt, 'a') as fd:
            fd.write(str(InsertQuery)+"\t"+str(queryType)+"\t"+str(e)+"\t"+"\n")
            
# Clean function
def clean(cleanValue):
    try:      
        clean = ''  
        clean = re.sub(r'\n', "", str(cleanValue)) 
        clean = re.sub(r'\'', "''", str(clean)) 
        clean = re.sub(r'\t', "", str(clean))
        clean = re.sub(r'\\', "", str(clean))
        clean = re.sub(r'\&nbsp\;', " ", str(clean))
        clean = re.sub(r'\&amp\;', "&", str(clean))
        clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
        clean = re.sub(r'^\s+|\s+$', "", str(clean))
        clean = re.sub(r'\s\s+', " ", str(clean))
        clean = re.sub(r'^\W+$', "", str(clean))
        clean = re.sub(r'\&\#39\;', "'", str(clean))
        clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
        clean = re.sub(r'\'', "''", str(clean))
        clean = re.sub(r'^\s*', "", str(clean))
        clean = re.sub(r'\s*$', "", str(clean))
        
        return clean    

    except Exception as ex:
        print (ex,sys.exc_traceback.tb_lineno)
        
def Calender(date,months,years,monID,YearID,calenderID):

    print ("date:",date)
    browser.find_element_by_xpath('//*[@id="'+monID+'"]').send_keys(months)
    time.sleep(10)
    browser.find_element_by_xpath('//*[@id="'+YearID+'"]').send_keys(years)

    time.sleep(10)
    dateBlock = browser.find_element_by_id(calenderID)
    tr_Block=dateBlock.find_elements_by_tag_name("tr")
    
    for row,tr in enumerate(tr_Block):
        td_Block=tr.find_elements_by_tag_name("td")
        for tdCount,td in enumerate(td_Block):
            columns = td.find_elements_by_tag_name("a")    
            for cell in columns:
                dates=cell.text
                dates = re.sub("u", "", dates)
                dates = re.sub("\'", "", dates)
                dates = re.sub("\s*", "", dates)
                dates=int(dates)
                if dates == int(date):
                    dateRange="//*[@id=\""+calenderID+"\"]/tbody/tr["+str(row+1)+"]/td["+str(tdCount+1)+"]/a[text()='"+str(dates)+"']"
                    # dateRange="//*[@id=\"CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_StartDate_myCalendar\"]/tbody/tr["+str(row+1)+"]/td["+str(tdCount+1)+"]/a[text()='"+str(dates)+"']"

                    try:
                        # WebDriverWait(browser, 20).until(EC.staleness_of(browser.find_element_by_xpath(dateRange)))
                        
                        salesnav = WebDriverWait(browser, 20).until(EC.element_to_be_clickable((By.XPATH, dateRange)));
                        salesnav.click()
                        print ("*** Click first try ***")
                    except Exception as e:
                        print ("First try Failed : ",e,sys.exc_traceback.tb_lineno)
                    
                    try:
                        browser.find_element_by_xpath(dateRange).click()
                        print ("*** Click Second try ***")
                    except Exception as e:
                        print ("Second try failed : ",e,sys.exc_traceback.tb_lineno)
                    
                    time.sleep(20)
                    return("Cliked")

# Application details parse section 
def appParseSection(Appcontent, AppNum, AppURL, CouncilCode, Source):
    print ("*** appParseSection Process ***")
    try:
        Application_Number=re.findall(r'<tr[^>]*?>\s*<th[^>]*?>\s*Application\s*Ref\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
        Proposal=re.findall(r'<tr[^>]*?>\s*<th[^>]*?>\s*Proposal\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([\w\W]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
        if not (Proposal):
             Proposal=re.findall(r'<tr[^>]*?>\s*<th[^>]*?>\s*Description\s*of\s*Works\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([\w\W]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
             
        Application_Status=re.findall(r'<tr[^>]*?>\s*<th[^>]*?>\s*Status\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
        
        Decision=re.findall(r'<tr[^>]*?>\s*<th[^>]*?>\s*Council\s*Decision\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
        
        DecisionDate=re.findall(r'<tr[^>]*?>\s*<th[^>]*?>\s*Decision\s*Date\:?\s*<\/th>\s*<td[^>]*?>\s*(?:<span[^>]*?>)?\s*([^<]*?)\s*(?:\&nbsp\;)?\s*(?:<\/span>)?\s*(?:\&nbsp\;)?\s*<\/td>', str(Appcontent), re.IGNORECASE)
        
        
        (Application, ProposalCnt, ApplicationStatus, decision, dateDecision, Date_Decision_Dispatched) = ('','','','','','')


        if len(Application_Number) > 0:
            Application=clean(Application_Number[0])
        else:
            Application=AppNum


        if len(Proposal) > 0:
            ProposalCnt=clean(Proposal[0])
        else:
            ProposalCnt=''
        ProposalCnt = re.sub(r'\'', "\'\'", str(ProposalCnt))

        if len(Application_Status) > 0:
            ApplicationStatus=clean(Application_Status[0])
        else:
            ApplicationStatus=''


        if len(Decision) > 0:
            decision=clean(Decision[0])
        else:
            decision=''

        if len(DecisionDate) > 0:
            dateDecision=clean(DecisionDate[0])
        else:
            dateDecision=''
        
                
        format1 = "%Y/%m/%d %H:%M"  
        Schedule_Date = datetime.now().strftime(format1)
    
        Source_With_Time = Source+"_"+Schedule_Date+"-perl"
        
        Council_Name = "Ashford";
        
        joinValues="("+"'"+str(Application)+"','"+str(ProposalCnt)+"','"+str(decision)+"','"+str(dateDecision)+"','"+str(Date_Decision_Dispatched)+"','"+str(CouncilCode)+"','"+str(AppURL)+"','"+Source_With_Time+"','"+str(ApplicationStatus)+"')"
        
        # print ("ApplicationValues: ", joinValues)
        
        return joinValues

    except Exception as e:
        print ("appParseSection ERROR: ",e,sys.exc_traceback.tb_lineno)
        
# daterange search section  
def inputsection(dumFrom_Date, dumTo_Date, driver):
    
    FromDate,FromMonth,FromYear=dumFrom_Date.split("/")
    ToDate,ToMonth,ToYear=dumTo_Date.split("/")
    
    print ("From Date:",FromDate,FromMonth,FromYear)
    print ("To Date:",ToDate,ToMonth,ToYear)

    try:
        #-- Parse
        driver.get('https://planning.ashford.gov.uk/planning/Default.aspx?new=true')
        #-- Wait funtion
        time.sleep(10)
        content=driver.page_source
        finalCnt=''
        if len(content) > 0:
            finalCnt = content.encode("utf-8")
            
        
        # ------------------- Agree Page Acceptance ------------------
        try:
            driver.find_element_by_id('CPH_Details_Agree_CheckBox').click()
            time.sleep(1)
            driver.find_element_by_id('CPH_Details_Submit_Button').click()
            time.sleep(1)
        except:
            pass
        
        time.sleep(5)
        
        #--------------- Planning Application Button Click -----------
        try:    
            driver.find_element_by_id('CPH_Details_Button2').click()
        except:
            pass
        
        time.sleep(7)
        
        #--------------- Click Date Button ---------------------
        driver.find_element_by_xpath('/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[2]/div/div[1]/span[3]/span/span/a/span').click()
        #-- Wait funtion
        time.sleep(10)
        
        #--------------- Select Decision Search Option ----------------
        browser.find_element_by_xpath("//*[@id=\"CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_DateSearchBy\"]/option[2]").click()
        time.sleep(3)
        
        # driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        driver.execute_script("window.scrollTo(0,200);")
        time.sleep(4)
        Values=Calender(FromDate,FromMonth,FromYear,'CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_StartDate_DD_Month','CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_StartDate_DD_Year','CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_StartDate_myCalendar')
        
        print ("From Date ",Values)
        time.sleep(3)
        
        Values=Calender(ToDate,ToMonth,ToYear,'CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_EndDate_DD_Month','CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_EndDate_DD_Year','CPH_Details_Details_TabContainer_Date_Tab_searchby_date1_EndDate_myCalendar')
        print ("To Date ",Values)
        
        driver.find_element_by_xpath('/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div[1]/div/div[1]/div/input[1]').click()
        
        #-- Wait funtion
        time.sleep(20)  

        searchcontent=driver.page_source
        searchcontent = searchcontent.encode("utf-8")
        filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
        with open(filename, 'wb') as fd:
            fd.write(searchcontent)
            
        return str(searchcontent)

    except Exception as e:
        print ("inputsection ERROR :: ",e,sys.exc_traceback.tb_lineno)

# Application parse section 
def parsesection(searchcontent, CouncilCode, Source, conn, cursor, driver):
    try:
        
        searchcount = re.findall("Your\s*search\s*by[^>]*?([\d]+)\s*result", str(searchcontent), re.IGNORECASE)
        try:
            totalcount = int(searchcount[0])
            print ("Totalcount", totalcount)
        except:
            totalcount=0
        
        pageNum=1
        try:
            pageNum = re.findall("id\=\"CPH_Details_DateResults1_lblPageCount\">\s*of\s*([\d]+)\s*<[^>]*?>", str(searchcontent), re.IGNORECASE)
            pageNum=pageNum[0]
            print ("pageNumCount:", pageNum)
        except:
            pageNum=1
            
        insertQuery = 'insert into Import_Non_Public_Planning_Decision_Automation_staging (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values '

        bulkValuesForQuery=''
        pageNumber = 1
        appCount=1
        applicationCount=1
        Collected_AppLink=[]
        # while pageNumber < pageNum:
        while (1):
            print ("*** pageNumber ***",pageNumber , pageNum)
            
            appcollection = re.findall("id\=\"CPH\_Details\_DateResults[\d]+\_Results\_GridView\_hl\_view\_[\d]+\"\s*href\=\"([\w\W]*?)\">([^>]*?)<[^>]*?>", str(searchcontent), re.IGNORECASE)
            
            # print ("appcollection::", appcollection)
            # input("STOP")           
            for appMatch in appcollection:
                app_url = appMatch[0]
                app_num = appMatch[1]

                print ("Current Application Number is:", applicationCount,"<==> Total Application : ",totalcount)
                applicationCount+=1
                if not (re.search("^http", app_url)):
                    app_url='https://planning.ashford.gov.uk/planning/'+app_url
                
                app_url = re.sub(r'&amp\;', "&", str(app_url))
                print ("app_url:", app_url)
                
                ses = req.session()
                headers = {
                            'Host': 'planning.ashford.gov.uk',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
                            'Cookie': 'ABCPLANNINGTERMS=9 Apr 2021 06:54:33 AM; ABCPLANID=2021Apr09-972692; ASP.NET_SessionId=aq35zxsiiawtcukc2fugcqi0; OnlinePlanning_Cookie_Accepted=accepted',
                        }
                        
                ses.headers.update(headers)
                response1=ses.get(app_url,headers=headers,proxies=proxies1,verify=False)
                
                code=response1.status_code
                appfinalCnt=response1.content
                
                appfinalCnt = BeautifulSoup(appfinalCnt, 'html.parser')

                print ('response1',code)
                
                fileTxt=logDirectory+"/"+councilcode+"_"+gcs+".txt"
                with open(fileTxt, 'a') as fd:
                    fd.write(str(app_url)+"\t"+str(code)+"\t"+str(app_num)+"\n")
                
                # with open("Content2.html", 'w') as fd:
                    # fd.write(str(appfinalCnt))
                ''' Ascii Code Conversion '''
                appfinalCnt = appfinalCnt.encode("ascii", "ignore")
                appfinalCnt = appfinalCnt.decode()
                
                appDataCollection = appParseSection(appfinalCnt, app_num, app_url, CouncilCode, Source)
                print ('appDataCollection',appDataCollection)
                if appCount == 1:
                    bulkValuesForQuery = insertQuery+appDataCollection
                    appCount += 1
                elif appCount == 5:
                    bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
                    print ("bulkValuesForQuery::",bulkValuesForQuery)
                    dbInsert(bulkValuesForQuery,"bulkValuesForQuery")
                    appCount=1
                    bulkValuesForQuery=''
                else:
                    bulkValuesForQuery = bulkValuesForQuery+","+appDataCollection
                    appCount += 1
            
            ''' Next Page Click Option '''
            try:
                driver.find_element_by_xpath('/html/body/form/div[4]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div[3]/table/tbody/tr/td[1]/div/table/tbody/tr/td[6]/a/img').click()
                time.sleep(7)
                
                filename=logDirectory+"/"+councilcode+"_"+gcs+".html"
                searchcontent = driver.page_source
                searchcontent = searchcontent.encode("utf-8")
                with open(filename, 'a') as fd:
                    fd.write(str(searchcontent))
                    
            except Exception as e:
                print ("Next Page Not Available :",e)
                break
            # if pageNumber == int(pageNum):
                # break
            pageNumber+=1
               
        return bulkValuesForQuery
    except Exception as e:
        print (e,sys.exc_traceback.tb_lineno)
        
# Main section  
if __name__== "__main__":
    
    councilcode = sys.argv[1]
    gcs = sys.argv[2]   
    
    if councilcode is None:
        print ('Councilcode arugument is missing')
        sys.exit()
        
    if gcs is None:
        print ('GCS arugument is missing')
        sys.exit()
    
    conn=""
    cursor=""
    conn = dbConnection("GLENIGAN") 
    cursor = conn.cursor()

    PROXY = "172.27.137.192:3128" # IP:PORT or HOST:PORT

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
    chrome_options.add_argument('--proxy-server=https://%s' % PROXY)

    
    #-- Setup
    chrome_options = Options()
    # chrome_options.add_argument("--headless")
    # chrome_options.add_argument('--log-level=3')
    browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=r'C:/Glenigan/Selenium_Jar/Chrome_v_90/chromedriver.exe')
    time.sleep(3)
    # browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver.exe')
    browser.maximize_window()
    thisgcs =   {
      "GCS001": "0",
      "GCS002": "7",
      "GCS003": "14",
      "GCS004": "21",
      "GCS005": "28",   
      "GCS090": "90",
      "GCS180": "180",
    }   
    
    gcsdate = thisgcs[gcs]      
    date_N_days_ago = datetime.now() - timedelta(days=int(gcsdate))
    
    format = "%d/%b/%Y"
    
    todate = date_N_days_ago.strftime(format)
    
    preday = date_N_days_ago - timedelta(days=6)
    fromdate = preday.strftime(format)
    print ('fromdate     :', fromdate)
    print ('todate   :', todate)
    Content = inputsection(fromdate, todate, browser)
    # Content = inputsection('01/01/2019', '03/04/2019', browser)
    time.sleep(10)
    finalinsertQuery = parsesection(Content, councilcode, gcs, conn, cursor, browser)
    print ("finalinsertQuery:", finalinsertQuery)
    # final output query insertion
    if (finalinsertQuery != '') or finalinsertQuery is not None:
        dbInsert(finalinsertQuery,"finalinsertQuery")
    
    browser.quit()