use strict;
use WWW::Mechanize;
use Config::Tiny;
use DBI;
use DBD::ODBC;
use Time::Piece;
use HTML::Entities;
use URI::Escape;
use IO::Socket::SSL;
use Win32; 
use Glenigan_DB_Windows;

my $Council_Code = $ARGV[0];
my $inFrom_Date = $ARGV[1];
my $inTo_Date = $ARGV[2];

if($Council_Code eq "")
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"147\" \"GCS001\"\)", 16, "Error message");
    exit();
}


# my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);

my $dumFrom_Date = $inFrom_Date;
my $dumTo_Date = $inTo_Date;
$dumFrom_Date=~s/\//\-/gsi;
$dumTo_Date=~s/\//\-/gsi;
print "Council_Code: $Council_Code\n";
print "From_Date: $dumFrom_Date\n";
print "To_Date: $dumTo_Date\n";

# Establish connection with DB server
my $dbh = &Glenigan_DB_Windows::DB_Decision();

### Get Council Details from ini file ###
my $Config = Config::Tiny->new();
$Config = Config::Tiny->read( "C:/Glenigan/Live_Schedule/Decision/Ceredigion/Ceredigion_Decision.ini" );
my $COUNCIL_NAME = $Config->{$Council_Code}->{'COUNCIL_NAME'};
my $HOME_URL = $Config->{$Council_Code}->{'HOME_URL'};
my $SEARCH_URL = $Config->{$Council_Code}->{'SEARCH_URL'};
my $FORM_NUMBER = $Config->{$Council_Code}->{'FORM_NUMBER'};
my $FORM_START_DATE = $Config->{$Council_Code}->{'FORM_START_DATE'};
my $FORM_END_DATE = $Config->{$Council_Code}->{'FORM_END_DATE'};
my $RADIO_BTN_NME = $Config->{$Council_Code}->{'RADIO_BTN_NME'};
my $RADIO_BTN_VAL = $Config->{$Council_Code}->{'RADIO_BTN_VAL'};
my $SRCH_BTN_NME = $Config->{$Council_Code}->{'SRCH_BTN_NME'};
my $SRCH_BTN_VAL = $Config->{$Council_Code}->{'SRCH_BTN_VAL'};
my $APPLICATION_REGEX = $Config->{$Council_Code}->{'APPLICATION_REGEX'};
my $TOTAL_PAGE_COUNT = $Config->{$Council_Code}->{'TOTAL_PAGE_COUNT'};
my $FILTER_URL = $Config->{$Council_Code}->{'FILTER_URL'};
my $SSL_VERIFICATION = $Config->{$Council_Code}->{'SSL_VERIFICATION'};
my $POST_URL = $Config->{$Council_Code}->{'POST_URL'};
my $REFERER = $Config->{$Council_Code}->{'REFERER'};
my $HOST = $Config->{$Council_Code}->{'HOST'};
my $POSTCONTENT = $Config->{$Council_Code}->{'POSTCONTENT'};
	

print "COUNCIL_NAME	: $COUNCIL_NAME\n";
print "URL		: $HOME_URL\n";
print "FORM_NUMBER	: $FORM_NUMBER\n";


#### UserAgent Declaration ####	
my $mech;
if($SSL_VERIFICATION eq 'N')
{
	$mech = WWW::Mechanize->new(autocheck => 0, autoclose => 1);
}
else
{	
	# $mech = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
	$mech = WWW::Mechanize->new( ssl_opts => {
    SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
    verify_hostname => 0, 
	autoclose => 1,
});
}	

### Proxy settings ###
if($Council_Code=~m/^(147)$/is)
{
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');
}

# Find home page url
my $Home_Url;
if($HOME_URL=~m/^(https?\:\/\/.*?)\//is)
{
	$Home_Url=$1;
}

# Get search results using date ranges

Retry:
my ($Responce, $cookie_jar, $Ping_Status1);
eval{
$Responce = $mech->get($HOME_URL);
$cookie_jar = $mech->cookie_jar;

$mech->form_number($FORM_NUMBER); 
$mech->set_fields( $FORM_START_DATE => $dumFrom_Date, $FORM_END_DATE => $dumTo_Date );	
$mech->click();
		
};

if ($@) {
	$Ping_Status1 = $@;
};	

my $content = $mech->content;
my $Search_Content=$content;

# open(PP, ">app_cont.html");
# print PP "$Search_Content\n";
# close(PP); 
# exit;

&Search_Content( $Search_Content);

sub Search_Content()
{
	my $appContent = shift;
	
	my $i = 1;
	if($appContent=~m/<table\s*class\s*=\s*\"table\s*table-bordered\s*table-hover\s*table-condensed\">\s*([\w\W]*?)\s*<\/table>/is)
	{		
		my $appDetails = $1;
		while($appContent=~m/<tr>\s*<td>\s*([^<]*?)\s*<\/td>[\w\W]*?\s*<td>\s*<button[^>]*?data-id=\"([^\"]*?)\">\s*[\w\W]*?\s*<\/button>\s*<\/td>\s*<\/tr>/isg)
		{
			my $Application_number = $1;
			my $APPPAGE = $2;
			my $Page_url = "https://ceredigion-online.tascomi.com/planning/index.html?fa=getApplication&id=$APPPAGE";
			
			
			$mech->get($Page_url);
			
			my $App_content = $mech->content;	
			my $Code = $mech->status;		
			
			# open(PP,">147_$i.html");
			# print PP "$App_content\n";
			# close(PP);
			# exit;
			
			$i++;
			
			&Scrape_Details($Council_Code,$App_content,$Code,$Page_url,$Application_number);
			
		}
	}
}

sub Scrape_Details()
{
	my $Council_Code=shift;
	my $App_Page_Content=shift;
	my $codes=shift;
	my $Page_url=shift;
	my $Application=shift;
	
	my $time = Time::Piece->new;
	my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');
	
	
	print "Council_Code: $Council_Code\n";
	
	
	my $Application_No						= &clean($1) if($App_Page_Content=~m/>\s*Application\s*Reference\s*Number\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Application_Status					= &clean($1) if($App_Page_Content=~m/>\s*Application\s*Status\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Proposal							= &clean($1) if($App_Page_Content=~m/>\s*Proposal\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Date_Decision_Made					= &clean($1) if($App_Page_Content=~m/>\s*Decision\s*Issued\s*Date\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Decision_Status						= &clean($1) if($App_Page_Content=~m/>\s*Decision\:\s*<\/strong>\s*<\/div>\s*<div[^>]*?>\s*\s*([^<]*?)\s*<\/div>/is);
	my $Decision_Issued_Date;
	my $Address=~s/\'/\'\'/gsi;
	my $Applicant_Address=~s/\'/\'\'/gsi;
	my $Agent_Address=~s/\'/\'\'/gsi;		
	my $Source = 'GCS001';	

	
	
	decode_entities($Proposal);

	if($Application=~m/^\s*$/is)
	{
		if($Application_No!~m/^\s*$/is)
		{
			$Application = $Application_No;
		}
	}
	
	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	
	my $insert_query="insert into Import_Non_Public_Planning_Decision_Automation (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status) values (\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$Council_Code\', \'$Page_url\', \'$Source_With_Time\', \'$Application_Status\')";
	
	undef $Application; undef $Proposal; undef $Decision_Status; undef $Date_Decision_Made; undef $Decision_Issued_Date; undef $Page_url; undef $Application_Status;
	
	
	print "insert_query::$insert_query\n";

	if($insert_query!~m/values\s*$/is)
	{
		&DB_Insert($dbh,$insert_query);
	}
}




sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	
	
	$Data=~s/\&\#232\;/è/igs;
	$Data=~s/\&\#233\;/é/igs;
	$Data=~s/\&\#234\;/ê/igs;
	$Data=~s/\&\#235\;/ë/igs;
	$Data=~s/\&\#236\;/ì/igs;
	$Data=~s/\&\#237\;/í/igs;
	$Data=~s/\&\#238\;/î/igs;
	$Data=~s/\&\#239\;/ï/igs;
	$Data=~s/\&\#224\;/à/igs;
	$Data=~s/\&\#225\;/á/igs;
	$Data=~s/\&\#226\;/â/igs;
	$Data=~s/\&\#227\;/ã/igs;
	$Data=~s/\&\#228\;/ä/igs;
	$Data=~s/\&\#229\;/å/igs;
	$Data=~s/\&\#230\;/æ/igs;
	$Data=~s/\&\#231\;/ç/igs;
	$Data=~s/\&quot\;/\"/igs;
	$Data=~s/\&\#244\;/ô/igs;
	$Data=~s/\&\#251\;/û/igs;
	$Data=~s/&#x21;/!/igs;
	$Data=~s/&#x22;/\"/igs;
	$Data=~s/&#x23;/#/igs;
	$Data=~s/&#x24;/\$/igs;
	$Data=~s/&#x25;/%/igs;
	$Data=~s/&#x26;/&/igs;
	$Data=~s/&#x27;/\'/igs;
	$Data=~s/&#x28;/(/igs;
	$Data=~s/&#x29;/)/igs;
	$Data=~s/&#x2A;/*/igs;
	$Data=~s/&#x2B;/+/igs;
	$Data=~s/&#x2C;/,/igs;
	$Data=~s/&#x2D;/-/igs;
	$Data=~s/&#x2E;/./igs;
	$Data=~s/&#x2F;/\//igs;
	$Data=~s/&#x30;/0/igs;
	$Data=~s/&#x31;/1/igs;
	$Data=~s/&#x32;/2/igs;
	$Data=~s/&#x33;/3/igs;
	$Data=~s/&#x34;/4/igs;
	$Data=~s/&#x35;/5/igs;
	$Data=~s/&#x36;/6/igs;
	$Data=~s/&#x37;/7/igs;
	$Data=~s/&#x38;/8/igs;
	$Data=~s/&#x39;/9/igs;
	$Data=~s/&#x3A;/:/igs;
	$Data=~s/&#x3B;/;/igs;
	$Data=~s/&#x3C;/</igs;
	$Data=~s/&#x3D;/=/igs;
	$Data=~s/&#x3E;/>/igs;
	$Data=~s/&#x3F;/?/igs;
	$Data=~s/&#x40;/@/igs;
	$Data=~s/&#x41;/A/igs;
	$Data=~s/&#x42;/B/igs;
	$Data=~s/&#x43;/C/igs;
	$Data=~s/&#x44;/D/igs;
	$Data=~s/&#x45;/E/igs;
	$Data=~s/&#x46;/F/igs;
	$Data=~s/&#x47;/G/igs;
	$Data=~s/&#x48;/H/igs;
	$Data=~s/&#x49;/I/igs;
	$Data=~s/&#x4A;/J/igs;
	$Data=~s/&#x4B;/K/igs;
	$Data=~s/&#x4C;/L/igs;
	$Data=~s/&#x4D;/M/igs;
	$Data=~s/&#x4E;/N/igs;
	$Data=~s/&#x4F;/O/igs;
	$Data=~s/&#x50;/P/igs;
	$Data=~s/&#x51;/Q/igs;
	$Data=~s/&#x52;/R/igs;
	$Data=~s/&#x53;/S/igs;
	$Data=~s/&#x54;/T/igs;
	$Data=~s/&#x55;/U/igs;
	$Data=~s/&#x56;/V/igs;
	$Data=~s/&#x57;/W/igs;
	$Data=~s/&#x58;/X/igs;
	$Data=~s/&#x59;/Y/igs;
	$Data=~s/&#x5A;/Z/igs;
	$Data=~s/&#x5B;/[/igs;
	$Data=~s/&#x5C;/\\/igs;
	$Data=~s/&#x5D;/]/igs;
	$Data=~s/&#x5E;/^/igs;
	$Data=~s/&#x5F;/_/igs;
	$Data=~s/&#x60;/`/igs;
	$Data=~s/&#x61;/a/igs;
	$Data=~s/&#x62;/b/igs;
	$Data=~s/&#x63;/c/igs;
	$Data=~s/&#x64;/d/igs;
	$Data=~s/&#x65;/e/igs;
	$Data=~s/&#x66;/f/igs;
	$Data=~s/&#x67;/g/igs;
	$Data=~s/&#x68;/h/igs;
	$Data=~s/&#x69;/i/igs;
	$Data=~s/&#x6A;/j/igs;
	$Data=~s/&#x6B;/k/igs;
	$Data=~s/&#x6C;/l/igs;
	$Data=~s/&#x6D;/m/igs;
	$Data=~s/&#x6E;/n/igs;
	$Data=~s/&#x6F;/o/igs;
	$Data=~s/&#x70;/p/igs;
	$Data=~s/&#x71;/q/igs;
	$Data=~s/&#x72;/r/igs;
	$Data=~s/&#x73;/s/igs;
	$Data=~s/&#x74;/t/igs;
	$Data=~s/&#x75;/u/igs;
	$Data=~s/&#x76;/v/igs;
	$Data=~s/&#x77;/w/igs;
	$Data=~s/&#x78;/x/igs;
	$Data=~s/&#x79;/y/igs;
	$Data=~s/&#x7A;/z/igs;
	$Data=~s/&#x7B;/{/igs;
	$Data=~s/&#x7C;/|/igs;
	$Data=~s/&#x7D;/}/igs;
	$Data=~s/&#x7E;/~/igs;
	$Data=~s/&#x7F;//igs;
	$Data=~s/&#x80;/€/igs;
	$Data=~s/&#x81; //igs;
	$Data=~s/&#x82;/‚/igs;
	$Data=~s/&#x83;/ƒ/igs;
	$Data=~s/&#x84;/„/igs;
	$Data=~s/&#x85;/…/igs;
	$Data=~s/&#x86;/†/igs;
	$Data=~s/&#x87;/‡/igs;
	$Data=~s/&#x88; /ˆ/igs;
	$Data=~s/&#x89;/‰/igs;
	$Data=~s/&#x8A;/Š/igs;
	$Data=~s/&#x8B; /‹/igs;
	$Data=~s/&#x8C;/Œ/igs;
	$Data=~s/&#x8D;//igs;
	$Data=~s/&#x8E;/Ž/igs;
	$Data=~s/&#x8F;//igs;
	$Data=~s/&#x90;//igs;
	$Data=~s/&#x91;/‘/igs;
	$Data=~s/&#x92;/’/igs;
	$Data=~s/&#x93;/“/igs;
	$Data=~s/&#x94;/”/igs;
	$Data=~s/&#x95;/•/igs;
	$Data=~s/&#x96;/–/igs;
	$Data=~s/&#x97;/—/igs;
	$Data=~s/&#x98; /˜/igs;
	$Data=~s/&#x99;/™/igs;
	$Data=~s/&#x9A;/š/igs;
	$Data=~s/&#x9B; /›/igs;
	$Data=~s/&#x9C;/œ/igs;
	$Data=~s/&#x9D;//igs;
	$Data=~s/&#x9E; /ž/igs;
	$Data=~s/&#x9F;/Ÿ/igs;
	$Data=~s/&#xA0;//igs;
	$Data=~s/&#xA1;/¡/igs;
	$Data=~s/&#xA2;/¢/igs;
	$Data=~s/&#xA3;/£/igs;
	$Data=~s/&#xA4;/¤/igs;
	$Data=~s/&#xA5;/¥/igs;
	$Data=~s/&#xA6;/¦/igs;
	$Data=~s/&#xA7;/§/igs;
	$Data=~s/&#xA8;/¨/igs;
	$Data=~s/&#xA9;/©/igs;
	$Data=~s/&#xAA;/ª/igs;
	$Data=~s/&#xAB;/«/igs;
	$Data=~s/&#xAC;/¬/igs;
	$Data=~s/&#xAD;/­/igs;
	$Data=~s/&#xAE;/®/igs;
	$Data=~s/&#xAF;/¯/igs;
	$Data=~s/&#xB0;/°/igs;
	$Data=~s/&#xB1;/±/igs;
	$Data=~s/&#xB2;/²/igs;
	$Data=~s/&#xB3;/³/igs;
	$Data=~s/&#xB4;/´/igs;
	$Data=~s/&#xB5;/µ/igs;
	$Data=~s/&#xB6;/¶/igs;
	$Data=~s/&#xB7;/·/igs;
	$Data=~s/&#xB8;/¸/igs;
	$Data=~s/&#xB9;/¹/igs;
	$Data=~s/&#xBA;/º/igs;
	$Data=~s/&#xBB;/»/igs;
	$Data=~s/&#xBC;/¼/igs;
	$Data=~s/&#xBD;/½/igs;
	$Data=~s/&#xBE;/¾/igs;
	$Data=~s/&#xBF;/¿/igs;
	$Data=~s/&#xC0;/À/igs;
	$Data=~s/&#xC1;/Á/igs;
	$Data=~s/&#xC2;/Â/igs;
	$Data=~s/&#xC3;/Ã/igs;
	$Data=~s/&#xC4;/Ä/igs;
	$Data=~s/&#xC5;/Å/igs;
	$Data=~s/&#xC6;/Æ/igs;
	$Data=~s/&#xC7;/Ç/igs;
	$Data=~s/&#xC8;/È/igs;
	$Data=~s/&#xC9;/É/igs;
	$Data=~s/&#xCA;/Ê/igs;
	$Data=~s/&#xCB;/Ë/igs;
	$Data=~s/&#xCC;/Ì/igs;
	$Data=~s/&#xCD;/Í/igs;
	$Data=~s/&#xCE;/Î/igs;
	$Data=~s/&#xCF;/Ï/igs;
	$Data=~s/&#xD0;/Ð/igs;
	$Data=~s/&#xD1;/Ñ/igs;
	$Data=~s/&#xD2;/Ò/igs;
	$Data=~s/&#xD3;/Ó/igs;
	$Data=~s/&#xD4;/Ô/igs;
	$Data=~s/&#xD5;/Õ/igs;
	$Data=~s/&#xD6;/Ö/igs;
	$Data=~s/&#xD7;/×/igs;
	$Data=~s/&#xD8;/Ø/igs;
	$Data=~s/&#xD9;/Ù/igs;
	$Data=~s/&#xDA;/Ú/igs;
	$Data=~s/&#xDB;/Û/igs;
	$Data=~s/&#xDC;/Ü/igs;
	$Data=~s/&#xDD;/Ý/igs;
	$Data=~s/&#xDE;/Þ/igs;
	$Data=~s/&#xDF;/ß/igs;
	$Data=~s/&#xE0;/à/igs;
	$Data=~s/&#xE1;/á/igs;
	$Data=~s/&#xE2;/â/igs;
	$Data=~s/&#xE3;/ã/igs;
	$Data=~s/&#xE4;/ä/igs;
	$Data=~s/&#xE5;/å/igs;
	$Data=~s/&#xE6;/æ/igs;
	$Data=~s/&#xE7;/ç/igs;
	$Data=~s/&#xE8;/è/igs;
	$Data=~s/&#xE9;/é/igs;
	$Data=~s/&#xEA;/ê/igs;
	$Data=~s/&#xEB;/ë/igs;
	$Data=~s/&#xEC;/ì/igs;
	$Data=~s/&#xED;/í/igs;
	$Data=~s/&#xEE;/î/igs;
	$Data=~s/&#xEF;/ï/igs;
	$Data=~s/&#xF0;/ð/igs;
	$Data=~s/&#xF1;/ñ/igs;
	$Data=~s/&#xF2;/ò/igs;
	$Data=~s/&#xF3;/ó/igs;
	$Data=~s/&#xF4;/ô/igs;
	$Data=~s/&#xF5;/õ/igs;
	$Data=~s/&#xF6;/ö/igs;
	$Data=~s/&#xF7;/÷/igs;
	$Data=~s/&#xF8;/ø/igs;
	$Data=~s/&#xF9;/ù/igs;
	$Data=~s/&#xFA;/ú/igs;
	$Data=~s/&#xFB;/û/igs;
	$Data=~s/&#xFC;/ü/igs;
	$Data=~s/&#xFD;/ý/igs;
	$Data=~s/&#xFE;/þ/igs;
	$Data=~s/&#xFF;/ÿ/igs;
	$Data=~s/\&\#x2019\;/\’/igs;
	$Data=~s/\&\#x2018\;/\‘/igs;
	$Data=~s/&aacute;/á/gs;
	$Data=~s/&Aacute;/Á/gs;
	$Data=~s/&acirc;/â/gs;
	$Data=~s/&Acirc;/Â/gs;
	$Data=~s/&agrave;/à/gs;
	$Data=~s/&Agrave;/À/gs;
	$Data=~s/&aring;/å/gs;
	$Data=~s/&Aring;/Å/gs;
	$Data=~s/&atilde;/ã/gs;
	$Data=~s/&Atilde;/Ã/gs;
	$Data=~s/&auml;/ä/gs;
	$Data=~s/&Auml;/Ä/gs;
	$Data=~s/&aelig;/æ/gs;
	$Data=~s/&AElig;/Æ/gs;
	$Data=~s/&ccedil;/ç/gs;
	$Data=~s/&Ccedil;/Ç/gs;
	$Data=~s/&eacute;/é/gs;
	$Data=~s/&Eacute;/É/gs;
	$Data=~s/&ecirc;/ê/gs;
	$Data=~s/&Ecirc;/Ê/gs;
	$Data=~s/&egrave;/è/gs;
	$Data=~s/&Egrave;/È/gs;
	$Data=~s/&euml;/ë/gs;
	$Data=~s/&Euml;/Ë/gs;
	$Data=~s/&iacute;/í/gs;
	$Data=~s/&Iacute;/Í/gs;
	$Data=~s/&icirc;/î/gs;
	$Data=~s/&Icirc;/Î/gs;
	$Data=~s/&igrave;/ì/gs;
	$Data=~s/&Igrave;/Ì/gs;
	$Data=~s/&iuml;/ï/gs;
	$Data=~s/&Iuml;/Ï/gs;
	$Data=~s/&ntilde;/ñ/gs;
	$Data=~s/&Ntilde;/Ñ/gs;
	$Data=~s/&oacute;/ó/gs;
	$Data=~s/&Oacute;/Ó/gs;
	$Data=~s/&ocirc;/ô/gs;
	$Data=~s/&Ocirc;/Ô/gs;
	$Data=~s/&ograve;/ò/gs;
	$Data=~s/&Ograve;/Ò/gs;
	$Data=~s/&oslash;/ø/gs;
	$Data=~s/&Oslash;/Ø/gs;
	$Data=~s/&otilde;/õ/gs;
	$Data=~s/&Otilde;/Õ/gs;
	$Data=~s/&ouml;/ö/gs;
	$Data=~s/&Ouml;/Ö/gs;
	$Data=~s/&oelig;/œ/gs;
	$Data=~s/&OElig;/Œ/gs;
	$Data=~s/&scaron;/š/gs;
	$Data=~s/&Scaron;/Š/gs;
	$Data=~s/&szlig;/ß/gs;
	$Data=~s/&eth;/ð/gs;
	$Data=~s/&ETH;/Ð/gs;
	$Data=~s/&thorn;/þ/gs;
	$Data=~s/&THORN;/Þ/gs;
	$Data=~s/&uacute;/ú/gs;
	$Data=~s/&Uacute;/Ú/gs;
	$Data=~s/&ucirc;/û/gs;
	$Data=~s/&Ucirc;/Û/gs;
	$Data=~s/&ugrave;/ù/gs;
	$Data=~s/&Ugrave;/Ù/gs;
	$Data=~s/&uuml;/ü/gs;
	$Data=~s/&Uuml;/Ü/gs;
	$Data=~s/&yacute;/ý/gs;
	$Data=~s/&Yacute;/Ý/gs;
	$Data=~s/&yuml;/ÿ/gs;
	$Data=~s/&Yuml;/ÿ/gs;
	$Data=~s/&shy;/-/gs;
	$Data=~s/&quot;/"/gs;
	$Data=~s/&laquo;/«/gs;
	$Data=~s/&raquo;/»/gs;
	$Data=~s/&lsaquo;/‹/gs;
	$Data=~s/&rsaquo;/›/gs;
	$Data=~s/&ldquo;/“/gs;
	$Data=~s/&rdquo;/”/gs;
	$Data=~s/&bdquo;/„/gs;
	$Data=~s/&apos;/'/gs;
	$Data=~s/&lsquo;/‘/gs;
	$Data=~s/&rsquo;/’/gs;
	$Data=~s/&sbquo;/‚/gs;
	$Data=~s/&hellip;/…/gs;
	$Data=~s/&iexcl;/¡/gs;
	$Data=~s/&iquest;/¿/gs;
	$Data=~s/&uml;/¨/gs;
	$Data=~s/&acute;/´/gs;
	$Data=~s/&circ;/ˆ/gs;
	$Data=~s/&tilde;/˜/gs;
	$Data=~s/&cedil;/¸/gs;
	$Data=~s/&middot;/·/gs;
	$Data=~s/&bull;/•/gs;
	$Data=~s/&macr;/¯/gs;
	$Data=~s/&oline;/‾/gs;
	$Data=~s/&ndash;/–/gs;
	$Data=~s/&mdash;/—/gs;
	$Data=~s/&brvbar;/¦/gs;
	$Data=~s/&dagger;/†/gs;
	$Data=~s/&Dagger;/†/gs;
	$Data=~s/&sect;/§/gs;
	$Data=~s/&para;/¶/gs;
	$Data=~s/&copy;/©/gs;
	$Data=~s/&reg;/®/gs;
	$Data=~s/&trade;/™/gs;
	$Data=~s/\&middot\;/™/gs;
	$Data=~s/\&\#161\;/¡/gs;
	$Data=~s/\&\#162\;/¢/gs;
	$Data=~s/\&\#163\;/£/gs;
	$Data=~s/\&\#164\;/¤/gs;
	$Data=~s/\&\#165\;/¥/gs;
	$Data=~s/\&\#166\;/¦/gs;
	$Data=~s/\&\#167\;/§/gs;
	$Data=~s/\&\#168\;/¨/gs;
	$Data=~s/\&\#169\;/©/gs;
	$Data=~s/\&\#170\;/ª/gs;
	$Data=~s/\&\#171\;/«/gs;
	$Data=~s/\&\#172\;/¬/gs;
	$Data=~s/\&\#173\;/­/gs;
	$Data=~s/\&\#174\;/®/gs;
	$Data=~s/\&\#175\;/¯/gs;
	$Data=~s/\&\#176\;/°/gs;
	$Data=~s/\&\#177\;/±/gs;
	$Data=~s/\&\#178\;/²/gs;
	$Data=~s/\&\#179\;/³/gs;
	$Data=~s/\&\#180\;/´/gs;
	$Data=~s/\&\#181\;/µ/gs;
	$Data=~s/\&\#182\;/¶/gs;
	$Data=~s/\&\#183\;/·/gs;
	$Data=~s/\&\#184\;/¸/gs;
	$Data=~s/\&\#185\;/¹/gs;
	$Data=~s/\&\#186\;/º/gs;
	$Data=~s/\&\#187\;/»/gs;
	$Data=~s/\&\#188\;/¼/gs;
	$Data=~s/\&\#189\;/½/gs;
	$Data=~s/\&\#190\;/¾/gs;
	$Data=~s/\&\#191\;/¿/gs;
	$Data=~s/\&\#192\;/À/gs;
	$Data=~s/\&\#193\;/Á/gs;
	$Data=~s/\&\#194\;/Â/gs;
	$Data=~s/\&\#195\;/Ã/gs;
	$Data=~s/\&\#196\;/Ä/gs;
	$Data=~s/\&\#197\;/Å/gs;
	$Data=~s/\&\#198\;/Æ/gs;
	$Data=~s/\&\#199\;/Ç/gs;
	$Data=~s/\&\#200\;/È/gs;
	$Data=~s/\&\#201\;/É/gs;
	$Data=~s/\&\#202\;/Ê/gs;
	$Data=~s/\&\#203\;/Ë/gs;
	$Data=~s/\&\#204\;/Ì/gs;
	$Data=~s/\&\#205\;/Í/gs;
	$Data=~s/\&\#206\;/Î/gs;
	$Data=~s/\&\#207\;/Ï/gs;
	$Data=~s/\&\#208\;/Ð/gs;
	$Data=~s/\&\#209\;/Ñ/gs;
	$Data=~s/\&\#210\;/Ò/gs;
	$Data=~s/\&\#211\;/Ó/gs;
	$Data=~s/\&\#212\;/Ô/gs;
	$Data=~s/\&\#213\;/Õ/gs;
	$Data=~s/\&\#214\;/Ö/gs;
	$Data=~s/\&\#215\;/×/gs;
	$Data=~s/\&\#216\;/Ø/gs;
	$Data=~s/\&\#217\;/Ù/gs;
	$Data=~s/\&\#218\;/Ú/gs;
	$Data=~s/\&\#219\;/Û/gs;
	$Data=~s/\&\#220\;/Ü/gs;
	$Data=~s/\&\#221\;/Ý/gs;
	$Data=~s/\&\#222\;/Þ/gs;
	$Data=~s/\&\#223\;/ß/gs;
	$Data=~s/\&\#224\;/à/gs;
	$Data=~s/\&\#225\;/á/gs;
	$Data=~s/\&\#226\;/â/gs;
	$Data=~s/\&\#227\;/ã/gs;
	$Data=~s/\&\#228\;/ä/gs;
	$Data=~s/\&\#229\;/å/gs;
	$Data=~s/\&\#230\;/æ/gs;
	$Data=~s/\&\#231\;/ç/gs;
	$Data=~s/\&\#232\;/è/gs;
	$Data=~s/\&\#233\;/é/gs;
	$Data=~s/\&\#234\;/ê/gs;
	$Data=~s/\&\#235\;/ë/gs;
	$Data=~s/\&\#236\;/ì/gs;
	$Data=~s/\&\#237\;/í/gs;
	$Data=~s/\&\#238\;/î/gs;
	$Data=~s/\&\#239\;/ï/gs;
	$Data=~s/\&\#240\;/ð/gs;
	$Data=~s/\&\#241\;/ñ/gs;
	$Data=~s/\&\#242\;/ò/gs;
	$Data=~s/\&\#243\;/ó/gs;
	$Data=~s/\&\#244\;/ô/gs;
	$Data=~s/\&\#245\;/õ/gs;
	$Data=~s/\&\#246\;/ö/gs;
	$Data=~s/\&\#247\;/÷/gs;
	$Data=~s/\&\#248\;/ø/gs;
	$Data=~s/\&\#249\;/ù/gs;
	$Data=~s/\&\#250\;/ú/gs;
	$Data=~s/\&\#251\;/û/gs;
	$Data=~s/\&\#252\;/ü/gs;
	$Data=~s/\&\#253\;/ý/gs;
	$Data=~s/\&\#254\;/þ/gs;
	$Data=~s/\&\#255\;/ÿ/igs;
	$Data=~s/&#(?:0)?39;/\'/gs;
	$Data=~s/&#8217;/\’/gs;
	$Data=~s/&#8211;/\–/gs;
	$Data=~s/&#8226;//gs;
	$Data=~s/&#150;/\–/gs;
	$Data=~s/\\u00c0/À/igs;
	$Data=~s/\\u00c1/Á/igs;
	$Data=~s/\\u00c2/Â/igs;
	$Data=~s/\\u00c3/Ã/igs;
	$Data=~s/\\u00c4/Ä/igs;
	$Data=~s/\\u00c5/Å/igs;
	$Data=~s/\\u00c6/Æ/igs;
	$Data=~s/\\u00c7/Ç/igs;
	$Data=~s/\\u00c8/È/igs;
	$Data=~s/\\u00c9/É/igs;
	$Data=~s/\\u00ca/Ê/igs;
	$Data=~s/\\u00cb/Ë/igs;
	$Data=~s/\\u00cc/Ì/igs;
	$Data=~s/\\u00cd/Í/igs;
	$Data=~s/\\u00ce/Î/igs;
	$Data=~s/\\u00cf/Ï/igs;
	$Data=~s/\\u00d1/Ñ/igs;
	$Data=~s/\\u00d2/Ò/igs;
	$Data=~s/\\u00d3/Ó/igs;
	$Data=~s/\\u00d4/Ô/igs;
	$Data=~s/\\u00d5/Õ/igs;
	$Data=~s/\\u00d6/Ö/igs;
	$Data=~s/\\u00d8/Ø/igs;
	$Data=~s/\\u00d9/Ù/igs;
	$Data=~s/\\u00da/Ú/igs;
	$Data=~s/\\u00db/Û/igs;
	$Data=~s/\\u00dc/Ü/igs;
	$Data=~s/\\u00dd/Ý/igs;
	$Data=~s/\\u00df/ß/igs;
	$Data=~s/\\u00e0/à/igs;
	$Data=~s/\\u00e1/á/igs;
	$Data=~s/\\u00e2/â/igs;
	$Data=~s/\\u00e3/ã/igs;
	$Data=~s/\\u00e4/ä/igs;
	$Data=~s/\\u00e5/å/igs;
	$Data=~s/\\u00e6/æ/igs;
	$Data=~s/\\u00e7/ç/igs;
	$Data=~s/\\u00e8/è/igs;
	$Data=~s/\\u00e9/é/igs;
	$Data=~s/\\u00ea/ê/igs;
	$Data=~s/\\u00eb/ë/igs;
	$Data=~s/\\u00ec/ì/igs;
	$Data=~s/\\u00ed/í/igs;
	$Data=~s/\\u00ee/î/igs;
	$Data=~s/\\u00ef/ï/igs;
	$Data=~s/\\u00f0/ð/igs;
	$Data=~s/\\u00f1/ñ/igs;
	$Data=~s/\\u00f2/ò/igs;
	$Data=~s/\\u00f3/ó/igs;
	$Data=~s/\\u00f4/ô/igs;
	$Data=~s/\\u00f5/õ/igs;
	$Data=~s/\\u00f6/ö/igs;
	$Data=~s/\\u00f8/ø/igs;
	$Data=~s/\\u00f9/ù/igs;
	$Data=~s/\\u00fa/ú/igs;
	$Data=~s/\\u00fb/û/igs;
	$Data=~s/\\u00fc/ü/igs;
	$Data=~s/\\u00fd/ý/igs;
	$Data=~s/\\u00ff/ÿ/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={SQL Server};Server=CH1025BD03;database=GLENIGAN;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}
