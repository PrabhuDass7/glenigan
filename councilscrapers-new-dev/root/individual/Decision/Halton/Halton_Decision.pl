use strict;
use WWW::Mechanize;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use Win32; 
use Glenigan_DB_Windows;


# Establish connection with DB server
my $dbh = &DbConnection();

my $Council_Code = $ARGV[0];
my $Date_Range = $ARGV[1];

if(($Council_Code eq "") or ($Date_Range eq ""))
{
	Win32::MsgBox("Missing Aruguments. Either \'Council Code\' or \'Date Range\' \(Eg: \"218\" \"GCS001\"\)", 16, "Error message");
    exit();
}

chomp($Date_Range);

my ($From_Date, $To_Date) = &Glenigan_DB_Windows::Date_Range($Date_Range);


print "From_Date: $From_Date\n";
print "To_Date: $To_Date\n";

my $mech = WWW::Mechanize->new( ssl_opts => {
					SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
					verify_hostname => 0, 
				});
				
my $searchURL = "https://webapp.halton.gov.uk/planningapps/";
$mech->get($searchURL);
my $Content = $mech->content;
my $status = $mech->status;


$mech->form_number(1);
$mech->set_fields( 'DateApValFrom' => $From_Date );
$mech->set_fields( 'DateApValTo' => $To_Date );
$mech->click();

my $SearchResultContent = $mech->content;


my $insert_query='insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values ';

my $Page_Number=1;
NextPage:
while($SearchResultContent=~m/(Case\s*No\s*\:\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>[\w\W]*?<\/table>)/igs)
{
	my $AppCnt=$1;
	my $Application_Num=$2;
	print "Application_Num==>$Application_Num\n";
	
	my ($Application,$Decision_Date,$Decision,$Application_Status, $Proposal);
	
	$Application=&clean($1) if($AppCnt=~m/Case\s*No\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
	$Application_Status=&clean($1) if($AppCnt=~m/Status\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
	$Decision_Date=&clean($1) if($AppCnt=~m/Decision\s*Date\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
	$Proposal=&clean($1) if($AppCnt=~m/Details\s*of\s*proposal\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
	$Decision=&clean($1) if($AppCnt=~m/Status\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^>]*?)\s*<\/td>/is);
		
	
	my $Application_URL = 'https://webapp.halton.gov.uk/planningapps/';
	my $COUNCIL_NAME = 'Halton';
	
	my $time = Time::Piece->new;
	my $Schedule_Date = $time->strftime('%m/%d/%Y %H:%M');
	my $Source = "GCS001";	
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";

	$insert_query .= "(\'$Application\', \'$Proposal\', \'$Decision\', \'$Decision_Date\', \'$Council_Code\', \'$Application_URL\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	undef($Application); undef($Decision); undef($Decision_Date); undef($Proposal); undef($Application_Status); 
	
}

if($SearchResultContent=~m/<INPUT[^>]*?CurrentRecord\s*Value\s*=\s*([\d]+)\s*>\s*<INPUT[^>]*?MaxRecord\s*Value\s*=\s*([\d]+)\s*>\s*<INPUT[^>]*?MaxPage\s*Value\s*=\s*([\d]+)\s*>\s*<INPUT[^>]*?Pagesize\s*Value\s*=\s*([\d]+)\s*>/is)
{
	my $value1=$1;
	my $value2=$2;
	my $value3=$3;
	my $value4=$4;

	my $nextPagePostCont = "Action=Next&CurrentRecord=<Next_Value1>&MaxRecord=<Next_Value2>&MaxPage=<Next_Value3>&Pagesize=<Next_Value4>";
		
	$nextPagePostCont=~s/<Next_Value1>/$value1/igs;
	$nextPagePostCont=~s/<Next_Value2>/$value2/igs;
	$nextPagePostCont=~s/<Next_Value3>/$value3/igs;
	$nextPagePostCont=~s/<Next_Value4>/$value4/igs;
	
	

	if($Council_Code==218)
	{
		if($value3>$Page_Number)
		{
			if($value3=~m/\d+/is)
			{
				$mech->post('https://webapp.halton.gov.uk/planningapps/index.asp', Content => "$nextPagePostCont");
				my $content = $mech->content;
				$SearchResultContent = $content;
				# open(DC,">SearchResultContent.html");
				# print DC $SearchResultContent;
				# close DC;
				# exit;

			}
		}	
		else
		{
			goto End;
		}
	}
	
	print "Page_Number: $Page_Number\n";
	
	$Page_Number++;
	
	$nextPagePostCont=~s/Action=Next&CurrentRecord=$value1&MaxRecord=$value2&MaxPage=$value3&Pagesize=$value4/Action=Next&CurrentRecord=<Next_Value1>&MaxRecord=<Next_Value2>&MaxPage=<Next_Value3>&Pagesize=<Next_Value4>/igs;
	
	goto NextPage;
}
End:

$insert_query=~s/\,$//igs;

&DB_Insert($dbh,$insert_query);
print "insert_query: $insert_query\n";


sub clean()
{
	my $Data=shift;
	my $Tab = chr(9);
	my $LF = chr(10);
	my $CR = chr(13);
	
	$Data=~s/\s*(?:<\s*br\s*>|<br\s*\/>)\s*/, /igs;
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/\s*\&gt;\s*//igs;
	$Data=~s/\s*\&lt;\s*//igs;
	$Data=~s/$Tab/ /igs;
	$Data=~s/$LF/ /igs;
	$Data=~s/$CR/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s*|\s*$//igs;
	$Data=~s/\s*,\s*,\s*/, /igs;
	$Data=~s/^\,+|\,+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\'/\'\'/igs;
	return($Data);
}


###### Insert DB Query ####
sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		$dbh=&DbConnection();
	}
}



###### DB Connection ####
sub DbConnection()
{
	my $dsn 						=  'driver={ODBC Driver 17 for SQL Server};Server=CH1025BD03;database=Glenigan;uid=User2;pwd=Merit456';
	my $dbh							=	DBI->connect("dbi:ODBC:$dsn", { RaiseError => 1, PrintError => 0 });
	
	if(!$dbh)
	{
		&DBIconnect($dsn);
	}
	else
	{
		$dbh-> {'LongTruncOk'}			=	1;
		$dbh-> {'LongReadLen'}			=	90000;
		print "\n------->Connected database successfully---->\n";
	}
	return $dbh;
}