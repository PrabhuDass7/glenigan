use strict;
use Redis;
use RedisDB;

# my $redis = RedisDB->new(host => '127.0.0.1:6379', database => 7); # for Decision
my $redis = Redis->new(server => '127.0.0.1:6379'); # for Planning
print "redis::$redis\n";
$redis->get('key');
# my $keys = $redis->keys( "D_149_*" ); # for Decision
my @keys = $redis->keys( "149_*" ); # for Planning
# my @keys = @$keys; # for Decision
foreach my $K(sort @keys)
{
	print "$K\n";
   $redis->del ($K);
}