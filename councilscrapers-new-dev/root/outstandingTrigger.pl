use strict;
use Cwd qw(abs_path);
use File::Basename;
use Time::Piece;
use File::Path qw/make_path/;
use DateTime;

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todayDate = $Date->ymd(''); # get Current date

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####
my $logsDirectory = ($basePath.'/logs');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $rootDirectory = ($basePath.'/root');


require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

# Establish connection with DB server
my $dbHost = &Glenigan_DB::gleniganDB();

my $Query = "select * from GLENIGAN..tbl_outstanding_status";

my $inputCheck;
eval 
{
	$inputCheck = $dbHost->selectrow_array($Query);
};
	
if ( $@ ) 
{ 
	print "ERROR: $@";
	
	open(ERR,">>$logsDirectory/Outstanding_Failed_${todayDate}.txt");
	print ERR $@."\n";
	close ERR;
}
else
{
	print "QUERY:: $Query\n";
	
	print "inputCheck==>$inputCheck\n";
	
	if($inputCheck eq 'yes')
	{
		open(ERR,">>$logsDirectory/Outstanding_logs_${todayDate}.txt");
		print ERR $inputCheck."\t".$todayDate."\n";
		close ERR;
		my $update_statusquery = "update tbl_outstanding_status set import_completed=\'Started\'";
		my $sth= $dbHost->prepare($update_statusquery);
		$sth->execute();	
		$sth->finish();
		system("perl $rootDirectory/OutstandingScriptBothDB.pl Decision &");
	}
	elsif($inputCheck eq 'Started')
	{
		open(ERR,">>$logsDirectory/Outstanding_logs_${todayDate}.txt");
		print ERR "Already Started\t".$todayDate."\n";
		close ERR;
	}
	else
	{
		print "No input Found\n";
		open(ERR,">>$logsDirectory/Outstanding_logs_${todayDate}.txt");
		print ERR "Input not found\t".$todayDate."\n";
		close ERR;
	}
}
