=pod
# my $memUsage = system("free -m | awk 'NR==2{printf "Memory Usage: %s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }'");
# my $diskUsage = system("df -h | awk '$NF=="/"{printf "Disk Usage: %d/%dGB (%s)\n", $3,$2,$5}'");
# my $cpuUsage = system("top -bn1 | grep load | awk '{printf "CPU Load: %.2f\n", $(NF-2)}'");
my $memUsage = system("free -m \| awk \'NR==2\{printf \"Memory Usage\: \%s\/\%sMB \(\%\.2f\%\%\)\\n\"\, \$3\,\$2\,\$3*100\/\$2 \}\'");
my $diskUsage = system("df -h \| awk \'\$NF==\"\/\"\{printf \"Disk Usage\: \%d\/\%dGB \(\%s\)\\n\"\, \$3\,\$2\,\$5\}\'");
my $cpuUsage = system("top -bn1 | grep load | awk \'\{printf \"CPU Load\: \%\.2f\\n\"\, \$\(NF-2\)\}\'");
=cut
use strict;
use Sys::Info;
use Sys::Info::Constants qw( :device_cpu );
my $info = Sys::Info->new;
my $cpu  = $info->device('CPU');
printf "CPU: %s\n", scalar($cpu->identify)  || 'N/A';
 
my $result = sprintf("%s", $cpu->load  || 0);

printf "CPU speed is %s MHz\n", $cpu->speed || 'N/A';
printf "There are %d CPUs\n"  , $cpu->count || 1;
printf "CPU load: %s\n"       , $cpu->load  || 0;
print "CPU load new: $result\n";
exit;