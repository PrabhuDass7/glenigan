#!/usr/bin/perl
use strict;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Basename;
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use URI::URL;
use Redis;


############
# How to Run
#
# perl ApacheScraperPlanning.pl "3" "Apache_Planning" "2018/05/08T11:30:00" "2018/05/08T11:30:00"
#
# For ONDEMND#
#
# perl ApacheScraperPlanning.pl 1 Apache_Planning GCS001 09/07/2018 16/07/2018 "2018/07/16T10:53:00" "2018/07/16T10:53:00" onDemand
#
##############

####
# Declare global variables
####

my $councilCode = $ARGV[0];
my $category = $ARGV[1];
my $source = $ARGV[2];
my $fDate = $ARGV[3];
my $tDate = $ARGV[4];
my $startDateTime = $ARGV[5];
my $scheduleDateTime = $ARGV[6];
my $schedule = $ARGV[7];
my $onDemandID = $ARGV[8];
my $onDemand = $ARGV[9];

$startDateTime=~s/T/ /s;
$scheduleDateTime=~s/T/ /s;
print "\n===========================================================================================================\n";
print "\tCouncil Code::$councilCode\tCategory::$category\tScriptStartDate::$startDateTime\tScheduleDateTime::$scheduleDateTime\n";
print "===========================================================================================================\n";

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');

print "ScheduleDate==>$scheduleDate\n";
# <STDIN>;

my ($fromDate, $toDate, $rangeName,$Schedule_no,$categoryType);
my ($searchResultCount, $responseContents, $responseStatus);
my ($divCounter, $scheduleCounter);
my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n";

if($category=~m/^Apache_Planning$/is)
{
	$categoryType="Planning";
}

####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/etc/Apache');
my $libControlDirectory = ($basePath.'/lib/Control');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $logsDataDirectory = ($basePath.'/data');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');


require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($libControlDirectory.'/ApacheCouncilsController.pm'); # Private Module
require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Redis_connection
####
my $redisConnection = &Glenigan_DB::Redis_connection();


####
# Create new object of class Config::Tiny
####

my ($regexFile,$councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils, control properties for date range clculation and list of all councils repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Apache_Council_Details.ini');
$regexFile = Config::Tiny->read($ConfigDirectory.'/Apache_Regex_Heap.ini' );

####
# Get Schedule number based on Forenoon or Afternoon
####
$Schedule_no=&CouncilsScrapperCore::scheduleNumber(localtime());

####
# Get Council Name From INI file
####
my $councilName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};


if(($onDemand eq '') or ($onDemand=~m/^\s*$/is))
{
	if( !$councilCode || !$category || !$startDateTime || !$scheduleDateTime) 
	{
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Input Argument Missing',$categoryType,$councilName);
		exit;
	}
}

####
# Get class name for Mechanize method
####
my ($councilApp,$pingStatus,$paginationContent) = &ApacheCouncilsController::setVerficationSSL($councilCode,$categoryType,'Apache','13',$scheduleDate,$Schedule_no,$councilName);


if($pingStatus!~m/^\s*(200|Ok)\s*$/is)
{
	&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Terminated',$categoryType,$councilName);	
	
	my $rawFileName;
	if($onDemand ne "")
	{
		$rawFileName=&ApacheCouncilsController::rawFileName($startDateTime,$councilCode,'ONDEMAND001',$categoryType);
	}
	else
	{
		$rawFileName=&ApacheCouncilsController::rawFileName($startDateTime,$councilCode,'GCS001',$categoryType);
	}
	
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContent,$searchLogsDirectoryPath,$categoryType,'Failed');	
	
	my $dt = DateTime->now;
	my $FromDate = $dt->ymd('-');
	my $dt2 = $dt->clone->subtract( weeks => 1);
	my $ToDate = $dt2->ymd('-');
		
	if($onDemand ne "")
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
		
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
		
		my $onDemandStatus;
		if($pingStatus > 500)
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		elsif( $pingStatus >= 400 and $pingStatus <= 499 )
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		else
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		
		&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
	}
	else
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
	}
	exit;
}


####
# Beginining of script execution
####

&main();



sub main
{
    # Calculating default Date Ranges based on values from control file
    my %defaultDateRanges;
	if(($source ne "") && ($fDate ne "") && ($tDate ne ""))
	{
		my $strp = DateTime::Format::Strptime->new(
        # pattern  => '%d/%m/%Y',
		pattern  => '%Y-%m-%d',
		);
		my $inputFromDate = $strp->parse_datetime( $fDate );
		my $inputToDate = $strp->parse_datetime( $tDate );
		# print "$inputFromDate<==>$inputToDate\n"; <STDIN>;
		$defaultDateRanges{$source} = [$inputFromDate, $inputToDate];
	}
	else
	{
		%defaultDateRanges = &CouncilsScrapperCore::defaultGcsRange( DateTime->now, $onDemand, $category,$schedule );
	}
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandUpdate($councilCode,$categoryType,$onDemandID);
	}

    # Dynamic Scheduling Logic (DSL) - 
    # Divides the the specific GCS schedule if the council website have cap-off on maximum search results returned
	
    foreach my $rangeName (sort keys %defaultDateRanges) 
    {
		print "\n#####################################################################\n";
		print "GCS:$rangeName\tCouncilCode:$councilCode\tCouncilName:$councilName\tCategory::$categoryType";
		print "\n#####################################################################\n";
        $divCounter = 1;
        $scheduleCounter = 0;
		
		####
		# Insert Scrape Status in Database
		####
		&CouncilsScrapperCore::writtingToLog('Start',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		
		my $FromDate = $defaultDateRanges{$rangeName}[0];
		my $ToDate = $defaultDateRanges{$rangeName}[1];
		
		# print "FromDate:$FromDate\n";
		# print "ToDate:$ToDate\n";
				
		####
		# Insert Table: Scrape current Status and Council Type in Database
		####
		&CouncilsScrapperCore::scrapeStatusInsertion($Schedule_no,$scheduleDate,$rangeName,$councilCode,'Running',$categoryType,$councilName);
		
        
        my $checkResult = &defaultRangeCheck(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
		my ($fullinsertQuery, $pageSearchCode, $actualScrappedCount, $totalSearchCount, $deDup_Query);
		
        if ($checkResult == 1)
        {
			print "Search Page Response::$checkResult(Expected value also 1).\n";
            ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
            
			my ($insertQuery, $responseCode, $SearchCount, $TotalSearchCount, $deDupQuery) = &fetchSearchResult($rangeName);				
			
			$fullinsertQuery = $insertQuery;
			$pageSearchCode = $responseCode;
			$actualScrappedCount = $SearchCount;
			$totalSearchCount = $TotalSearchCount;
			$deDup_Query = $deDupQuery;
        }
        else
        {
			print "Search Page Response::$checkResult(Expected value is 1).\n";
            my %subRanges = &calculateSubRanges($divCounter, @{$defaultDateRanges{$rangeName}}[1],$rangeName);
           
			my ($insertQuery, $responseCode, $SearchCount, $TotalSearchCount, $deDupQuery) = &subRangesSearchResult(\%subRanges);
			
			$fullinsertQuery = $insertQuery;
			$pageSearchCode = $responseCode;
			$actualScrappedCount = $SearchCount;
			$totalSearchCount = $TotalSearchCount;
			$deDup_Query = $deDupQuery;
        }
			
		print "Total Application Links Count for the date range ".$FromDate->dmy('/')." to ".$ToDate->dmy('/')." is \:\t$totalSearchCount\n";
		
		print "But actual Scrapped Count is\:\t $actualScrappedCount\n";
		
				
		####
		# Insert application details in ScreenScrapper Database for Planning
		####
		&CouncilsScrapperCore::scrapeDetailsInsertion($fullinsertQuery,$categoryType);
				
		
		####
		# Get file name to store search page content in log folder
		####		
		my $rawFileName=&ApacheCouncilsController::rawFileName($startDateTime,$councilCode,$rangeName,$categoryType);			
		
		
		####
		# Insert actual scrapped details for Dashboard
		####
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, $rangeName, $fromDate, $toDate, $totalSearchCount, $actualScrappedCount, $pageSearchCode, $rawFileName, $scheduleDate);
		
		
		####
		# Insert deDup details
		####
		if($deDup_Query ne "")
		{
			&CouncilsScrapperCore::deDupInsertion($deDup_Query);
		}
		
		if($pageSearchCode=~m/^\s*(?:200|ok)\s*$/is)
		{	
			####
			# Update input date range search success response count into TBL_SCRAP_STATUS 
			####
			
			&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualScrappedCount,$councilCode,'Completed',$categoryType,$councilName);	
		}
		
		####
		# Insert Scrape Status in onDemand Database
		####
		if($onDemand eq 'onDemand')
		{
			my $onDemandStatus;
			if($totalSearchCount ne "")
			{
				if(($totalSearchCount=~m/^0$/is) or ($totalSearchCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No Application found";
				}
				elsif(($actualScrappedCount=~m/^0$/is) or ($actualScrappedCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No new application found";
				}
				else
				{
					$onDemandStatus = "\"$totalSearchCount\" Application found. But Valid application count: \"$actualScrappedCount\"";
				}
			}
			&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
		}
		
		
		####
		# Insert Scrape Status in Database
		####
		
		&CouncilsScrapperCore::writtingToLog('End',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
    }
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
	}
}




####
# Function to pass GCS sub-ranges dates to fetch search result
####

sub subRangesSearchResult
{
    my ($subRangesParam) = @_;
    my %subRanges = %$subRangesParam;
		
	my ($insertQuery, $responseCode, $SearchCount, $TotalSearchCount, $deDup_Query);
	my ($dupinsertQuery, $dupresponseCode, $dupSearchCount, $dupTotalSearchCount, $dupdeDup_Query);
    foreach my $subRangeName (sort keys %subRanges)
    {
        ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$subRanges{$subRangeName}}[0], @{$subRanges{$subRangeName}}[1]);
        
		($insertQuery, $responseCode, $SearchCount, $TotalSearchCount, $deDup_Query) = &fetchSearchResult($subRangeName); 
		$dupinsertQuery.=$insertQuery;
		$dupdeDup_Query.=$deDup_Query;
		$dupSearchCount=$dupSearchCount+$SearchCount;
		$dupTotalSearchCount=$dupTotalSearchCount+$TotalSearchCount;
		
    }

    # return($insertQuery, $responseCode, $SearchCount, $TotalSearchCount, $deDup_Query);
    return($dupinsertQuery, $responseCode, $dupSearchCount, $dupTotalSearchCount, $dupdeDup_Query);
}

####
# Function to match the search result count from response
####

sub fetchSearchResult
{
    my($passedsubRangeName) = @_;
    my($difference);

    $difference = $toDate->delta_days( $fromDate );
    $scheduleCounter = $scheduleCounter + $difference->delta_days;
    print "GCS::$passedsubRangeName ==> FromDate::".$fromDate->dmy('/')." to ToDate::".$toDate->dmy('/')." ~ NoOfDaysRange::".$difference->delta_days."\t";
    ($responseContents,$responseStatus) = &callCouncilsMechMethod($fromDate, $toDate);
		
	####
	# Insert input date range ping response into DB 
	####
	if($responseStatus!~m/^\s*(?:200|ok)\s*$/is) # Only If Fails
	{
		&CouncilsScrapperCore::writtingToStatus($passedsubRangeName,$councilCode,$councilName,$responseStatus,$logsDirectory,$categoryType);

		####
		# Update input date range search terminated response count into TBL_SCRAP_STATUS 
		####
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$searchResultCount,$councilCode,'Terminated',$categoryType,$councilName);		
	}
	
	my $tempSource;
	if($onDemand eq 'onDemand')
	{
		$tempSource = "onDemand_".$passedsubRangeName;
	}
	else
	{
		$tempSource = $passedsubRangeName;
	}
	
	my ($insertQuery, $searchResultCount, $totalAppResultCount, $deDup_Query) = &ApacheCouncilsController::applicationsDataDumpPlanning($responseContents,$tempSource,$councilCode,$councilName,$scheduleDate,$scheduleDateTime,$categoryType,$redisConnection,$Schedule_no,$councilApp);
	
   	
    return($insertQuery, $responseStatus, $searchResultCount, $totalAppResultCount, $deDup_Query);
}


####
# Common method to inisialise Mechanize user agent
####

sub callCouncilsMechMethod
{
    my($passedFromDate, $passedToDate) = @_;
	my ($councilAppResponse, $scriptStatus) = &ApacheCouncilsController::councilsMechMethod($passedFromDate->dmy('/'), $passedToDate->dmy('/'), $councilCode, $councilApp, $categoryType);
    return ($councilAppResponse, $scriptStatus);
}


####
# Function to check the default date rnages from the GCS schedule needs to be split
####

sub defaultRangeCheck
{
    my ($passedFromDate, $passedToDate) = @_;

    return( &rangeCheckMethod($passedFromDate, $passedToDate) );
}


####
# Function to check the search result page response to see if it returns in error
####

sub rangeCheckMethod
{
    my ($passedFromDate,$passedToDate) = @_;

    ($responseContents,$responseStatus) = &callCouncilsMechMethod($passedFromDate, $passedToDate);
	
    my $searchValue;
    
	if($responseContents=~m/$regexFile->{'MainRegexControl'}->{'ERR_MSG_RX'}/is)
	{	
		$searchValue = $1;
	}
	
	if($searchValue=~m/WARNING/s)
    {
        next;
    }
	elsif($searchValue=~m/No\s*Match/is)
	{
		next;
	}
	elsif($searchValue=~m/No\s*Result/is)
	{
		next;
	}
	elsif($searchValue=~m/No\s*records\s*found/is)
	{
		next;
	}
	elsif($responseContents=~m/>\s*No\s*Records?\s*Found/is)
	{
		next;
	}
	elsif($searchValue=~m/The\s*date\s*to\s*must\s*be\s*after\s*the\s*date\s*from/is)
	{
		next;
	}
	elsif($responseContents=~m/The\s*date\s*to\s*must\s*be\s*after\s*the\s*date\s*from/is)
	{
		next;
	}
	elsif($responseContents=~m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_COUNT_RX_NEW'}/is)
	{
		return(0);
	}
	elsif($responseContents=~m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_COUNT_RX'}/is)
	{
		return(0);
	}
	elsif($responseContents=~m/$councilDetailsFile->{$councilCode}->{'APPLICATION_REGEX'}/is)
	{
		return(1);
	}
    else
    {
        return(0);
    }
}


####
# Function to calculate GCS sub-ranges and pass the resulting date range into rangeCheckMethod is see the $resultValue results with 1
####

sub calculateSubRanges
{
    my ($divCount, $passedToDate, $gcsRange) = @_;
    my (%subRangeCheck, $resultValue);
	print "gcsRange==>$gcsRange\n";
	
    do
    {
        %subRangeCheck = &CouncilsScrapperCore::subRangesCalcMethod($divCount, $passedToDate, $onDemand);
        my ($subRangeFromDate, $subRangeToDate);
        if($onDemand ne "")
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'ONDEMAND001'}}[0], @{$subRangeCheck{'ONDEMAND001'}}[1]);
		}
		else
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'GCS001'}}[0], @{$subRangeCheck{'GCS001'}}[1]);
		}
        $resultValue = &rangeCheckMethod($subRangeFromDate, $subRangeToDate);
        $divCount = $divCount + 1;
		if($divCount==5)
		{
			# last;
			return(%subRangeCheck);
		}

    } while ($resultValue == 0);
	
    return(%subRangeCheck);
}