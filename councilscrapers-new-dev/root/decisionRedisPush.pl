use strict;
use DBI;
use DBD::ODBC;
use RedisDB;
use File::Basename;
use Cwd  qw(abs_path);
use DateTime;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;

my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module


## Redis Declaration ##
my $redis = RedisDB->new(host => '127.0.0.1', port => 6379, database => 7);


## Flush Redis decision records ##
# my $FlushDB_Flag = $redis->flushdb();
# print "FlushDB_Flag: $FlushDB_Flag\n";


# Establish connection with DB server
my $dbHost = &Glenigan_DB::gleniganDB();



my $dt = DateTime->now(time_zone => 'Asia/Kolkata', );
$dt->datetime();
my $todayDate = $dt->ymd('-');
print "\n###################\n";
print "Today Date::$todayDate\n";
# <STDIN>;



my $Query = "select council_code,application_no from GLENIGAN..tbl_decided_application where CONVERT(varchar,created_date,112) = convert(varchar,GETDATE(),112)";

my $sth;
eval 
{
	$sth= $dbHost->prepare($Query);
	$sth->execute();
};
	
if ( $@ ) 
{ 
	print "ERROR: $@";
	
	open(ERR,">>$logsDirectory/redisDecisionFailed_${todayDate}.txt");
	print ERR $@."\n";
	close ERR;
}
else
{
	print "QUERY:: $Query\n";
	
	while(my @record = $sth->fetchrow)
	{
		my $councilCode = &Trim($record[0]);
		my $appNumber = &Trim($record[1]);
		
		print "D_".$councilCode."_".$appNumber => $appNumber."\n";
		$redis->set("D_".$councilCode."_".$appNumber => $appNumber);
	}
	$sth->finish();
}

sub Trim
{
	my $txt = shift;
	$txt =~ s/^\s+|\s+$//igs;
	$txt =~ s/^\n+/\n/igs;
	return $txt;
}