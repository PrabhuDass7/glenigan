#!/usr/bin/perl
use strict;
use Tie::IxHash;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Basename;
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use URI::URL;
use RedisDB;
use Parallel::ForkManager;
use POSIX;


##############
# How to Run #
#
# perl OnlineScraperDecision.pl "2" "Online_Decision" "2018/03/16T11:30:00" "2018/03/16T11:30:00"
#
# For ONDEMND#
#
# perl OnlineScraperDecision.pl 1 Online_Planning GCS001 09/07/2018 16/07/2018 "2018/07/16T10:53:00" "2018/07/16T10:53:00" onDemand
#
##############

####
# Declare global variables
####

my $councilCode = $ARGV[0];
my $category = $ARGV[1];
my $source = $ARGV[2];
my $fDate = $ARGV[3];
my $tDate = $ARGV[4];
my $startDateTime = $ARGV[5];
my $scheduleDateTime = $ARGV[6];
my $schedule = $ARGV[7];
my $onDemandID = $ARGV[8];
my $onDemand = $ARGV[9];

$startDateTime=~s/T/ /s;
$scheduleDateTime=~s/T/ /s;
print "\n===========================================================================================================\n";
print "\tCouncil Code::$councilCode\tCategory::$category\tScriptStartDate::$startDateTime\tScheduleDateTime::$scheduleDateTime\n";
print "===========================================================================================================\n";

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');

print "ScheduleDate==>$scheduleDate\n";
# <STDIN>;

my ($fromDate, $toDate, $rangeName,$Schedule_no,$categoryType);
my ($searchResultCount, $responseContents, $responseStatus);
my ($divCounter, $scheduleCounter);
my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n";

if($category=~m/^Online_Decision$/is)
{
	$categoryType="Decision";
}


####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/etc/Online');
my $libControlDirectory = ($basePath.'/lib/Control');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $logsDataDirectory = ($basePath.'/data');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($libControlDirectory.'/OnlineCouncilsController.pm'); # Private Module
require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Redis_connection_Decision
####
my $redisConnection = &Glenigan_DB::Redis_connection_Decision();


####
# Create new object of class Config::Tiny
####

my ($regexFile,$councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils, control properties for date range clculation and list of all councils repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Online_Council_Decision_Details.ini');
$regexFile = Config::Tiny->read($ConfigDirectory.'/Regex_Heap_Decision.ini' );

####
# Get Council Name From INI file
####
my $councilName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};


####
# Get Schedule number based on Forenoon or Afternoon
####
$Schedule_no=&CouncilsScrapperCore::scheduleNumber(localtime());
# print "Schedule_no==>$Schedule_no\n"; <STDIN>;


if(($onDemand eq '') or ($onDemand=~m/^\s*$/is))
{
	if( !$councilCode || !$category || !$startDateTime || !$scheduleDateTime) 
	{
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Input Argument Missing',$categoryType,$councilName);
		exit;
	}
}

####
# Get class name for Mechanize method
####
my ($councilApp,$pingStatus,$paginationContent) = &OnlineCouncilsController::setVerficationSSL($councilCode,$categoryType,'Online','2',$scheduleDate,$Schedule_no,$councilName);


if($pingStatus!~m/^\s*(200|Ok)\s*$/is)
{
	&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Terminated',$categoryType,$councilName);	
	
	my $rawFileName;
	if($onDemand ne "")
	{
		$rawFileName=&OnlineCouncilsController::rawFileName($startDateTime,$councilCode,'ONDEMAND001',$categoryType);
	}
	else
	{
		$rawFileName=&OnlineCouncilsController::rawFileName($startDateTime,$councilCode,'GCS001',$categoryType);
	}
		
	my $dt = DateTime->now;
	my $FromDate = $dt->ymd('-');
	my $dt2 = $dt->clone->subtract( weeks => 1);
	my $ToDate = $dt2->ymd('-');
	
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContent,$searchLogsDirectoryPath,$categoryType,'Failed');	
		
	
	if($onDemand ne "")
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
		
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
		
		my $onDemandStatus;
		if($pingStatus > 500)
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		elsif( $pingStatus >= 400 and $pingStatus <= 499 )
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		else
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		
		&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
	}
	else
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
	}
	exit;
}

####
# Beginining of script execution
####

&main();



sub main
{
    # Calculating default Date Ranges based on values from control file
   
	my %defaultDateRanges;
	if(($source ne "") && ($fDate ne "") && ($tDate ne ""))
	{
		my $strp = DateTime::Format::Strptime->new(
        # pattern  => '%d/%m/%Y',
		pattern  => '%Y-%m-%d',
		);
		my $inputFromDate = $strp->parse_datetime( $fDate );
		my $inputToDate = $strp->parse_datetime( $tDate );
		# print "$inputFromDate<==>$inputToDate\n"; <STDIN>;
		$defaultDateRanges{$source} = [$inputFromDate, $inputToDate];
	}
	else
	{
		%defaultDateRanges = &CouncilsScrapperCore::defaultGcsRange( DateTime->now, $onDemand, $category,$schedule);
	}
	
	####
	# Update ID in onDemand Database
	####
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandUpdate($councilCode,$categoryType,$onDemandID);
	}


    # Dynamic Scheduling Logic (DSL) - 
    # Divides the the specific GCS schedule if the council website have cap-off on maximum search results returned
	
    foreach my $rangeName (sort keys %defaultDateRanges) 
    {
		print "\n#####################################################################\n";
		print "GCS:$rangeName\tCouncilCode:$councilCode\tCouncilName:$councilName\tCategory::$categoryType";
		print "\n#####################################################################\n";
        $divCounter = 1;
        $scheduleCounter = 0;
		
		####
		# Insert Scrape Status in Database
		####
		&CouncilsScrapperCore::writtingToLog('Start',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		
		my $FromDate = $defaultDateRanges{$rangeName}[0];
		my $ToDate = $defaultDateRanges{$rangeName}[1];
		
		# print "FromDate:$FromDate\n";
		# print "ToDate:$ToDate\n";
				
		####
		# Insert Table: Scrape current Status and Council Type in Database
		####
		&CouncilsScrapperCore::scrapeStatusInsertion($Schedule_no,$scheduleDate,$rangeName,$councilCode,'Running',$categoryType,$councilName);
		
        
        my $checkResult = &defaultRangeCheck(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
		my (@totalApplicationLinks,$deDupRedisQuery,$pageSearchCode, $actualSearchCount);
		
        if ($checkResult == 1)
        {
			print "Search Page Response::$checkResult(Expected value also 1).\n";
            ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
            my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($rangeName);		
			@totalApplicationLinks = @$recievedApplicationLinks;
			$pageSearchCode = $responseCode;
			$actualSearchCount = $SearchCount;
			$deDupRedisQuery = $deDupQuery;
        }
        else
        {
			print "Search Page Response::$checkResult(Expected value is 1).\n";
            my %subRanges = &calculateSubRanges($divCounter, @{$defaultDateRanges{$rangeName}}[1],$rangeName);
            my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &subRangesSearchResult(\%subRanges);		
			@totalApplicationLinks = @$recievedApplicationLinks;
			$pageSearchCode = $responseCode;	
			$actualSearchCount = $SearchCount;		
			$deDupRedisQuery = $deDupQuery;			
        }
			
		my $totalLinksCount =@totalApplicationLinks;
		print "Total Application Links Count for the date range ".$FromDate->dmy('/')." to ".$ToDate->dmy('/')." is \:\t$actualSearchCount\n";
		
		print "\nBut valid count for this date range is \:\t$totalLinksCount\n";
		
		my ($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@totalApplicationLinks,$rangeName);	
		
		####
		# Insert application details in ScreenScrapper Database for Planning
		####
		&CouncilsScrapperCore::scrapeDetailsInsertion($insertQuery,$categoryType);
				
		
		####
		# Get file name to store search page content in log folder
		####		
		my $rawFileName=&OnlineCouncilsController::rawFileName($startDateTime,$councilCode,$rangeName,$categoryType);			
		
		
		####
		# Insert actual scrapped details for Dashboard
		####
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, $rangeName, $fromDate, $toDate, $actualSearchCount, $actualScrappedCount, $pageSearchCode, $rawFileName, $scheduleDate);
		
		
		####
		# Insert deDup details
		####
		if($deDupRedisQuery ne "")
		{
			&CouncilsScrapperCore::deDupInsertion($deDupRedisQuery);
		}
		
		if($pageSearchCode=~m/^\s*(?:200|ok)\s*$/is)
		{	
			####
			# Update input date range search success response count into TBL_SCRAP_STATUS 
			####
			
			&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
		}
		
		
		####
		# Insert Scrape Status in Database
		####
		
		&CouncilsScrapperCore::writtingToLog('End',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		
		
		####
		# Insert Scrape Status in onDemand Database
		####
		if($onDemand eq 'onDemand')
		{
			my $onDemandStatus;
			if($actualSearchCount ne "")
			{
				if(($actualSearchCount=~m/^0$/is) or ($actualSearchCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No Application found";
				}
				elsif(($totalLinksCount=~m/^0$/is) or ($totalLinksCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No new application found";
				}
				else
				{
					$onDemandStatus = "\"$actualSearchCount\" Application found. But Valid application count: \"$totalLinksCount\"";
				}
			}
			&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
		}	
    }
	
	
	####
	# Delete input ID in onDemand Database
	####
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
	}
}




####
# Function to pass GCS sub-ranges dates to fetch search result
####

sub subRangesSearchResult
{
    my ($subRangesParam) = @_;
    my %subRanges = %$subRangesParam;
	
	my (@recAppLinks, $deDupQuery, $recAppLink, $responseCode, $SearchCount);
    foreach my $subRangeName (sort keys %subRanges)
    {
        ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$subRanges{$subRangeName}}[0], @{$subRanges{$subRangeName}}[1]);
        ($recAppLink, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($subRangeName); 
		my @recAppLink = @$recAppLink;	
		push(@recAppLinks,@recAppLink);
    }

    return(\@recAppLinks, $deDupQuery, $responseCode, $SearchCount);
}

####
# Function to match the search result count from response
####

sub fetchSearchResult
{
    my($passedsubRangeName) = @_;
    my($difference);

	my @totalURL;
    $difference = $toDate->delta_days( $fromDate );
    $scheduleCounter = $scheduleCounter + $difference->delta_days;
    print "GCS::$passedsubRangeName ==> FromDate::".$fromDate->dmy('/')." to ToDate::".$toDate->dmy('/')." ~ NoOfDaysRange::".$difference->delta_days."\t";
	my $currentPageURL;
    ($responseContents,$responseStatus,$currentPageURL) = &callCouncilsMechMethod($fromDate, $toDate);
	
    # Get number of results
    $searchResultCount = $1 if($responseContents =~ /$regexFile->{'MainRegexControl'}->{'SEARCH_RESULTS_COUNT_RX'}/is);
	if($searchResultCount eq "")
	{
		my $resultcnt=0;
		while($responseContents =~ m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
		{
			$resultcnt++;
		}
		
		$searchResultCount = $resultcnt;
	}
	
	
	####
	# Insert input date range ping response into DB 
	####
	if($responseStatus!~m/^\s*(?:200|ok)\s*$/is) # Only If Fails
	{
		&CouncilsScrapperCore::writtingToStatus($passedsubRangeName,$councilCode,$councilName,$responseStatus,$logsDirectory,$categoryType);

		####
		# Update input date range search terminated response count into TBL_SCRAP_STATUS 
		####
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$searchResultCount,$councilCode,'Terminated',$categoryType,$councilName);		
	}
	
	
	my ($paginationContents, $pageCheck);
	if($responseContents=~m/<strong>\s*Showing\s*1[^<]*?<\/strong>/is)
	{
		$paginationContents = &paginationResponse($councilCode, $councilApp, $fromDate->dmy('/'), $toDate->dmy('/'), $responseContents);
	}
	elsif($responseContents=~m/Application\s*Summary\s*<[^>]*?>\s*<\/h1>/is)
	{
		$pageCheck = "Y";
		$paginationContents = $responseContents;
	}
	elsif($responseContents!~m/<[^>]*?class=\"next\"[^>]*?>\s*Next\s*</is)
	{
		$pageCheck = "Y";
		$paginationContents = $responseContents;
	}
	else
	{
		$paginationContents = &paginationResponse($councilCode, $councilApp, $fromDate->dmy('/'), $toDate->dmy('/'), $responseContents);
	}
	
	if(($searchResultCount eq "") && ($pageCheck eq "Y"))
	{
		$searchResultCount = "1";		
	}
	
    print "SearchResult: $searchResultCount\n";
	
	
	my $rawFileName=&OnlineCouncilsController::rawFileName($startDateTime,$councilCode,$passedsubRangeName,$categoryType);	
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContents,$searchLogsDirectoryPath,$categoryType,'Success');	
		
	
	my ($totalURL, $duplicateQuery) = &getApplicationLinks($paginationContents, $passedsubRangeName,$currentPageURL);
	@totalURL=@$totalURL;
	
    return(\@totalURL, $duplicateQuery, $responseStatus, $searchResultCount);
}


####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationLinks()
{
	my $totalContent = shift;
	my $gcsRange = shift;
	my $cPageURL = shift;
		
	my (@appLinks, $deDup_Query); 
	if($totalContent=~m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_CONTENT_RX'}/is)
	{		
		while($totalContent=~m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
		{
			my $Applink = $1;
			my $AppNum = $2;
			my $ApplicationNumberBlock = $3;
			
			my $ApplicationNumber="";
			if($ApplicationNumberBlock=~m/Ref\s*\.\s*No\s*\:\s*([^>]*?)\s*$/is)
			{
				$ApplicationNumber=$1; 
			}
			elsif($ApplicationNumberBlock=~m/Ref\s*\.\s*No\s*\:\s*(?:<[^>]*?>)?\s*([^>]*?)\s*$/is)
			{
				$ApplicationNumber=$1; 
			}
			elsif($ApplicationNumberBlock=~m/Application\.\s*No\s*\:\s*<\/[^>]*?>\s*([^>]*?)\s*$/is)
			{
				$ApplicationNumber=$1; 
			}
			
			# print "Before::$Applink\n";
			my $fullAppPageLink;		
			if($Applink!~m/https?\:/is)
			{
				$fullAppPageLink = URI::URL->new($Applink)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
			}	
			else
			{
				$fullAppPageLink = $Applink;
			}	
			$fullAppPageLink =~ s/\&amp\;/\&/g;
			
			# print "After::$fullAppPageLink\n"; 
			# <STDIN>;

			
			if($redisConnection->exists("D_".$councilCode."_".$ApplicationNumber))	
			{
				print "Exists : $ApplicationNumber\n";	
				
				my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$ApplicationNumber\', \'$fullAppPageLink\', \'$categoryType\', \'$gcsRange\',\'$councilName\'),";
				$deDup_Query.=$deDupQuery;
				
				next;
			}
		
			
			push ( @appLinks, $fullAppPageLink);
		}
	}
	else
	{
		
		my $ApplicationNumber;
		if($totalContent =~ m/$regexFile->{'summary'}->{'APPLICATION'}/is)
		{
			$ApplicationNumber = $1;
		}
		elsif($totalContent =~ m/$regexFile->{'summary'}->{'CASE_REFERENCE'}/is)
		{
			$ApplicationNumber = $1;
		}
		elsif($totalContent =~ m/$regexFile->{'summary'}->{'SUMMARY_CASENUMBER'}/is)
		{
			$ApplicationNumber = $1;
		}
		elsif($totalContent =~ m/$regexFile->{'summary'}->{'APPLICATION_REFERENCE'}/is)
		{
			$ApplicationNumber = $1;
		}
		
		$ApplicationNumber = &OnlineCouncilsController::htmlTagClean($ApplicationNumber);
		
		
		my $tempURL = $1 if($totalContent=~m/<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*(?:<[^>]*?>)?\s*Summary\s*(?:<[^>]*?>)?\s*<\/a>/is);
				
		if($tempURL!~m/https?\:/is)
		{
			$cPageURL = URI::URL->new($tempURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
		}	
		else
		{
			$cPageURL = $tempURL;
		}	
		$cPageURL =~ s/\&amp\;/\&/g;
		
		if($redisConnection->exists("D_".$councilCode."_".$ApplicationNumber))	
		{
			print "Exists : $ApplicationNumber\n";	
			
			my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$ApplicationNumber\', \'$cPageURL\', \'$categoryType\', \'$gcsRange\',\'$councilName\'),";
			$deDup_Query.=$deDupQuery;
			
			next;
		}
			
		push ( @appLinks, $cPageURL);
	}
	
	# print "deDup_Query==>$deDup_Query\n"; <STDIN>;
	
	return(\@appLinks, $deDup_Query);
}



####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationDetails()
{
    my $passedApplicationLink = shift;
    my $rangeName = shift;
	
	my @passedApplicationLinks = @{$passedApplicationLink};


	my $insertQuery;
	
	print "GCS==>$rangeName\n";
	
	my $resultsCount=0;
	
    foreach my $eachApplicationLink (@passedApplicationLinks)
    {		
		# print "$resultsCount==>$eachApplicationLink\n";
			
		my ($fullAppPageLink);
	
		$fullAppPageLink = URI::URL->new($eachApplicationLink)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
		
		my $random_number = rand(3);
		
		my $sleepRandomNumber = ceil($random_number);
		
		print "Random Sleep Number:: $sleepRandomNumber\n";

		sleep($sleepRandomNumber);
		
		my %dataDetails;
		# print "AppPageLink==>$applicationLinkUri\n"; <STDIN>;
		my ($dataDetails,$applicationPageContentResponse, $appNumber) = &OnlineCouncilsController::getApplicationDetails($fullAppPageLink, $councilApp, $categoryType,$councilCode);
				
		%dataDetails = %{$dataDetails};
		
		if($applicationPageContentResponse=~m/^\s*(200|ok)\s*$/is)
		{
			$resultsCount++;
			
			## Insert new application in redis server
			if($appNumber ne "")
			{
				$redisConnection->set("D_".$councilCode."_".$appNumber => $appNumber);
				print "Not Exists : $appNumber\n";
	
				print "AppPageLink==>$resultsCount==>$fullAppPageLink\n";
			}
		}
		
		my $tempSource;
		if($onDemand eq 'onDemand')
		{
			$tempSource = "onDemand_".$rangeName;
		}
		else
		{
			$tempSource = $rangeName;
		}
		
		if($appNumber ne "")
		{
			my ($insert_query_From_Func) = &OnlineCouncilsController::applicationsDataDumpDecision(\%dataDetails,$fullAppPageLink,$tempSource,$councilCode,$councilName,$scheduleDate,$scheduleDateTime);
			
			$insertQuery.=$insert_query_From_Func;
		}
    }
	
	# print "$resultsCount\n"; <STDIN>;
		
	return($insertQuery, $resultsCount);
}

####
# Common method to inisialise Mechanize user agent
####

sub callCouncilsMechMethod
{
    my($passedFromDate, $passedToDate) = @_;
	my ($councilAppResponse, $scriptStatus,$currentPageURL) = &OnlineCouncilsController::councilsMechMethod($passedFromDate->dmy('/'), $passedToDate->dmy('/'), $councilCode, $councilApp, $categoryType);
    return ($councilAppResponse, $scriptStatus,$currentPageURL);
}


####
# Function to check the default date rnages from the GCS schedule needs to be split
####

sub defaultRangeCheck
{
    my ($passedFromDate, $passedToDate) = @_;

    return( &rangeCheckMethod($passedFromDate, $passedToDate) );
}


####
# Function to check the search result page response to see if it returns in error
####

sub rangeCheckMethod
{
    my ($passedFromDate,$passedToDate,$Schedule_no,$scheduleDate,$rangeName,$councilCode,$scriptStatus,$scriptType,$councilName) = @_;
	
	my $currentPageURL;
    ($responseContents,$responseStatus,$currentPageURL) = &callCouncilsMechMethod($passedFromDate, $passedToDate);
		
    my $searcgValue = $1 if($responseContents =~ qr/$regexFile->{'MainRegexControl'}->{'ERR_MSG_RX'}/i);
	
	if($searcgValue=~m/^\s*Too\s*many/is)
    {
        return(0);
    }
	elsif($searcgValue=~m/^\s*No\s*results/is)
	{
		next;
	}
    else
    {
        return(1);
    }
}


####
# Function to calculate GCS sub-ranges and pass the resulting date range into rangeCheckMethod is see the $resultValue results with 1
####

sub calculateSubRanges
{
    my ($divCount, $passedToDate,$gcsRange) = @_;
    my (%subRangeCheck, $resultValue);
	print "gcsRange==>$gcsRange\n";
	
    do
    {
        %subRangeCheck = &CouncilsScrapperCore::subRangesCalcMethod($divCount, $passedToDate, $onDemand);
		
		my ($subRangeFromDate, $subRangeToDate);
		if($onDemand ne "")
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'ONDEMAND001'}}[0], @{$subRangeCheck{'ONDEMAND001'}}[1]);
		}
		else
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'GCS001'}}[0], @{$subRangeCheck{'GCS001'}}[1]);
		}
		
        $resultValue = &rangeCheckMethod($subRangeFromDate, $subRangeToDate);
        $divCount = $divCount + 1;
		if($divCount==5)
		{
			# last;
			return(%subRangeCheck);
		}

    } while ($resultValue == 0);
	
    return(%subRangeCheck);
}



####
# Function to fetch pagenation response for resulting search result page
####

sub paginationResponse
{	
	my ($councilCode, $councilApp, $inputFromDate, $inputToDate, $Contents) = @_;
	
    $responseContents = &OnlineCouncilsController::searchPageNext($councilCode, $councilApp, $categoryType, $inputFromDate, $inputToDate, $Contents);
	
    return($responseContents);
}