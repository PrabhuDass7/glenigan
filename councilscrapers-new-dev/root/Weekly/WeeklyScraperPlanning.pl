#!/usr/bin/perl
use strict;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Basename;
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use URI::Escape;
use URI::URL;
use Redis;


##############
# How to Run
#
# perl WeeklyScraperPlanning.pl "3" "Weekly_Planning" "2018/06/11T11:30:00" "2018/06/11T11:30:00"
#
# For ONDEMND#
#
# perl WeeklyScraperPlanning.pl 1 Weekly_Planning "2018/07/16T10:53:00" "2018/07/16T10:53:00" onDemand
#
##############

####
# Declare global variables
####

my $councilCode = $ARGV[0];
my $category = $ARGV[1];
my $startDateTime = $ARGV[2];
my $scheduleDateTime = $ARGV[3];
my $schedule = $ARGV[4];
my $onDemandID = $ARGV[5];
my $onDemand = $ARGV[6];

$startDateTime=~s/T/ /s;
$scheduleDateTime=~s/T/ /s;
print "\n===========================================================================================================\n";
print "\tCouncil Code::$councilCode\tCategory::$category\tScriptStartDate::$startDateTime\tScheduleDateTime::$scheduleDateTime\n";
print "===========================================================================================================\n";

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');

print "ScheduleDate==>$scheduleDate\n";
# <STDIN>;

my ($fromDate, $toDate, $rangeName,$Schedule_no,$categoryType);
my ($searchResultCount, $responseContents, $responseStatus);
my ($divCounter, $scheduleCounter);
my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n";

if($category=~m/^Weekly_Planning$/is)
{
	$categoryType="Planning";
}

####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/etc/Weekly');
my $libControlDirectory = ($basePath.'/lib/Control');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $logsDataDirectory = ($basePath.'/data');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');


require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($libControlDirectory.'/WeeklyCouncilsController.pm'); # Private Module
require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Redis_connection
####
my $redisConnection = &Glenigan_DB::Redis_connection();


####
# Create new object of class Config::Tiny
####

my ($regexFile,$councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils, control properties for date range clculation and list of all councils repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Weekly_Council_Details.ini');
$regexFile = Config::Tiny->read($ConfigDirectory.'/Weekly_Regex_Heap.ini' );

####
# Get Schedule number based on Forenoon or Afternoon
####
$Schedule_no=&CouncilsScrapperCore::scheduleNumber(localtime());

####
# Get Council Name From INI file
####
my $councilName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
my $weeklyListURLs = $councilDetailsFile->{$councilCode}->{'URL'};


if(($onDemand eq '') or ($onDemand=~m/^\s*$/is))
{
	if( !$councilCode || !$category || !$startDateTime || !$scheduleDateTime) 
	{
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Input Argument Missing',$categoryType,$councilName);
		exit;
	}
}

####
# Get class name for Mechanize method
####
my ($councilApp,$pingStatus,$paginationContent) = &WeeklyCouncilsController::setVerficationSSL($councilCode,$categoryType,'Weekly','19',$scheduleDate,$Schedule_no,$councilName);


####
# Get file name to store search page content in log folder
####	
my $rawFileName;
if($onDemand ne "")
{
	$rawFileName=&WeeklyCouncilsController::rawFileName($startDateTime,$councilCode,'ONDEMAND001',$categoryType);
}
else
{
	$rawFileName=&WeeklyCouncilsController::rawFileName($startDateTime,$councilCode,'GCS001',$categoryType);
}


if($pingStatus!~m/^\s*(200|Ok)\s*$/is)
{
	&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Terminated',$categoryType,$councilName);	
	
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContent,$searchLogsDirectoryPath,$categoryType,'Failed');	
	
	my $dt = DateTime->now;
	my $FromDate = $dt->ymd('-');
	my $ToDate = $FromDate;
		
	if($onDemand ne "")
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
		
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
		
		my $onDemandStatus;
		if($pingStatus > 500)
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		elsif( $pingStatus >= 400 and $pingStatus <= 499 )
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		else
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		
		&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
	}
	else
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
	}
	
	exit;
}


####
# Beginining of script execution
####

&main();



sub main
{
	print "\n#####################################################################\n";
	print "CouncilCode:$councilCode\tCouncilName:$councilName\tCategory::$categoryType";
	print "\n#####################################################################\n";
	
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandUpdate($councilCode,$categoryType,$onDemandID);
	}


	
	my ($weeklyListCnt,$responseCode) = &WeeklyCouncilsController::getMechContent($weeklyListURLs,$councilApp,$councilCode);
	
	my $councilType = $councilDetailsFile->{$councilCode}->{'TYPE'};
	my $WEEKLY_LIST_REGEX = $councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_REGEX'};
	
	my @WeeklyListUrl;
	my ($councilAppResponse, $pingResponseStatus);
	if($councilType=~m/^GET$/is)
	{
		my $URLContent = $1 if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_CONTENT_REGEX'}/is);
					
		while($URLContent=~m/$WEEKLY_LIST_REGEX/igs)
		{
			my $weeklyURL=$1;
			my $WeeklyDateRange=$2;
		
			my $weeklyListURL;
			if($councilCode=~m/^(133|461|482)$/is)
			{
				my $weeklyURLDomin = $councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_DOMAIN'};
				$weeklyURLDomin=~s/<DateRange>/$weeklyURL/si;
				
				$weeklyListURL = $weeklyURLDomin;
			}
			elsif($weeklyURL!~m/^https?/is)
			{
				$weeklyURL = URI::URL->new($weeklyURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				
				$weeklyListURL = $weeklyURL;
			}
			else
			{
				$weeklyListURL = $weeklyURL;
			}
			
			$weeklyListURL=~s/\&amp\;/\&/gsi;
			
			# print "$weeklyListURL\n"; <STDIN>;
			
			push(@WeeklyListUrl,$weeklyListURL);	
			
			
		}
		
		$pingResponseStatus=$responseCode;
	}
	elsif($councilType=~m/^POST$/is)
	{
		
		my ($ViewState,$ViewstateGenerator,$EventValidation);
		if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_VIEWSTATE_REGEX'}/is)
		{
			$ViewState=uri_escape($2);
		}
		if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_VIEWSTATEGENERATOR'}/is)
		{
			$ViewstateGenerator=uri_escape($2);
		}
		if($weeklyListCnt=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_EVENTVALIDATION'}/is)
		{
			$EventValidation=uri_escape($2);
		}

		
		my $postContent = $councilDetailsFile->{$councilCode}->{'SEARCH_POST_CONTENT'};
		my $year = $dt->year();
		my $month = $dt->month();
		
		if($councilCode ==366)
		{
			my $month1 = $month;
			my $year1 = $year;
			for (my $i=1;$i<=7;$i++)
			{	
				print "month1[$i]::$month1\n";
				print "year1[$i]::$year1\n";				
				my $postContent1 = $postContent;
				$postContent1=~s/<Year>/$year1/is;
				$postContent1=~s/<Month>/$month1/is;
				
				$postContent1=~s/<VIEWSTATE>/$ViewState/is;
				$postContent1=~s/<VIEWSTATEGENERATOR>/$ViewstateGenerator/is;
				$postContent1=~s/<EVENTVALIDATION>/$EventValidation/is;
						
				my $councilAppResponse1 = $councilApp->post($councilDetailsFile->{$councilCode}->{'SEARCH_URL'}, Content => "$postContent1");
				$councilAppResponse1 = $councilApp->content;
				$pingResponseStatus = $councilApp->status;
				
				$councilAppResponse = $councilAppResponse.$councilAppResponse1;
				
				$month1--;
				$year1=$year1-1 if($month1==0);
				$month1=12 if($month1==0);
			}			
		}
		else
		{
			$postContent=~s/<Year>/$year/is;
			$postContent=~s/<Month>/$month/is;
			
			$postContent=~s/<VIEWSTATE>/$ViewState/is;
			$postContent=~s/<VIEWSTATEGENERATOR>/$ViewstateGenerator/is;
			$postContent=~s/<EVENTVALIDATION>/$EventValidation/is;
					
			$councilAppResponse = $councilApp->post($councilDetailsFile->{$councilCode}->{'SEARCH_URL'}, Content => "$postContent");
			$councilAppResponse = $councilApp->content;
			$pingResponseStatus = $councilApp->status;
				
			# open(TIME,">fileName.html");
			# print TIME "$councilAppResponse\n";
			# close TIME;
			# exit;
		}
	}
	elsif($councilType=~m/^NORMAL$/is)
	{		
		my $searchURL = $councilDetailsFile->{$councilCode}->{'SEARCH_URL'};		
		my $year = $dt->year();
		
		$searchURL=~s/<Year>/$year/is;
		
		$councilAppResponse = $councilApp->get($searchURL);
		$councilAppResponse = $councilApp->content;
		$pingResponseStatus = $councilApp->status;
			
		# open(TIME,">fileName.html");
		# print TIME "$councilAppResponse\n";
		# close TIME;
		# exit;
	}
	elsif($councilType=~m/^DATE$/is)
	{
		my $searchURL = $councilDetailsFile->{$councilCode}->{'SEARCH_URL'};		
		my $year = $dt->year();
		my $month = $dt->month();
		$month = $dt->strftime('%m') if ($councilCode == 240);
		$searchURL=~s/<Year>/$year/is;
		$searchURL=~s/<Month>/$month/is;
		
		my ($councilAppCnt, $councilAppStatus);
		$councilAppCnt = $councilApp->get($searchURL);
		$councilAppCnt = $councilApp->content;
		$councilAppStatus = $councilApp->status;
			
		# open(TIME,">fileName.html");
		# print TIME "$councilAppResponse\n";
		# close TIME;
		# exit;
		
		my $URLContent = $1 if($councilAppCnt=~m/$councilDetailsFile->{$councilCode}->{'WEEKLY_LIST_CONTENT_REGEX'}/is);
					
		while($URLContent=~m/$WEEKLY_LIST_REGEX/igs)
		{
			my $weeklyURL=$1;
			my $WeeklyDateRange=$2;
		
			my $weeklyListURL;
			if($weeklyURL!~m/^https?/is)
			{
				$weeklyURL = URI::URL->new($weeklyURL)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
				
				$weeklyListURL = $weeklyURL;
			}
			else
			{
				$weeklyListURL = $weeklyURL;
			}
			
			$weeklyListURL=~s/\&amp\;/\&/gsi;
			
			# print "$weeklyListURL\n"; <STDIN>;
			
			push(@WeeklyListUrl,$weeklyListURL);	
			
			
		}
		
		$pingResponseStatus=$councilAppStatus;
	}
	elsif($councilType=~m/^N\/A$/is)
	{
		my $searchURL = $councilDetailsFile->{$councilCode}->{'SEARCH_URL'};		
		
		my $councilAppCnt = $councilApp->get($searchURL);
		my $councilAppCnt = $councilApp->content;
		my $councilAppStatus = $councilApp->status;
		
		$councilAppResponse = $councilAppCnt;
	}
	
		
	my ($appURLs, $appLinkDup, $actualSearchCount, $newAppLinkcheck);
	if($onDemand ne "")
	{
		($appURLs, $appLinkDup, $actualSearchCount, $newAppLinkcheck) = &WeeklyCouncilsController::searchNextWeeklyRange($councilCode, $councilApp, $categoryType, \@WeeklyListUrl, 'ONDEMAND001', $councilName, $Schedule_no, $scheduleDateTime, $redisConnection, $councilAppResponse, $rawFileName);
	}
	else
	{	
		($appURLs, $appLinkDup, $actualSearchCount, $newAppLinkcheck) = &WeeklyCouncilsController::searchNextWeeklyRange($councilCode, $councilApp, $categoryType, \@WeeklyListUrl, 'GCS001', $councilName, $Schedule_no, $scheduleDateTime, $redisConnection, $councilAppResponse, $rawFileName);
	}
	
	my @appLinks = @{$appURLs};
	
		
	###
	# Insert deDup details
	###
	if($appLinkDup ne "")
	{
		&CouncilsScrapperCore::deDupInsertion($appLinkDup);
	}
	
	
	if($newAppLinkcheck!~m/^Yes$/is)
	{
		# print "newAppLinkcheck=>$newAppLinkcheck\n"; <STDIN>;
		
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
		
		print "No new Application found\n";
		
		print "Total Application Links Count for ths weekly List range is \:\t$actualSearchCount\n";
			
		print "But actual Scrapped Count is\:\t0\n";		
			
		my $dt = DateTime->now;
		my $FromDate = $dt->ymd('-');
		
		my $ToDate = $FromDate;
			
		###
		# Insert actual scrapped details for Dashboard
		###
		if($onDemand ne "")
		{
			&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, $actualSearchCount, '0', '200', $rawFileName, $scheduleDate);
		}
		else
		{
			&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, $actualSearchCount, '0', '200', $rawFileName, $scheduleDate);
		}
		
		####
		# Insert Scrape Status in onDemand Database
		####
		if($onDemand eq 'onDemand')
		{
			my $onDemandStatus;
			if($actualSearchCount ne "")
			{
				if(($actualSearchCount=~m/^0$/is) or ($actualSearchCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No Application found";
				}
				else
				{
					$onDemandStatus = "\"$actualSearchCount\" Application found.";
				}
			}
			&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
		}
			
		
		###
		# Insert Scrape Status in Database
		###
		if($onDemand ne "")
		{
			&CouncilsScrapperCore::writtingToLog('End','ONDEMAND001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		}
		else
		{
			&CouncilsScrapperCore::writtingToLog('End','GCS001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		}
	}
	else
	{		
		# print "actualSearchCount==>$actualSearchCount\n"; <STDIN>;
		# print "pingResponseStatus==>$pingResponseStatus\n"; <STDIN>;
			
		
		###
		# Insert input date range ping response into DB 
		###
		if($pingResponseStatus!~m/^\s*(?:200|ok)\s*$/is) # Only If Fails
		{
			if($onDemand ne "")
			{
				&CouncilsScrapperCore::writtingToStatus('ONDEMAND001',$councilCode,$councilName,$pingResponseStatus,$logsDirectory,$categoryType);
			
				&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
				
				my $onDemandStatus;
				if($pingResponseStatus > 500)
				{
					$onDemandStatus = "Site not Working. Response code: $pingResponseStatus";
				}
				elsif( $pingResponseStatus >= 400 and $pingResponseStatus <= 499 )
				{
					$onDemandStatus = "Site not Working. Response code: $pingResponseStatus";
				}
				else
				{
					$onDemandStatus = "Site not Working. Response code: $pingResponseStatus";
				}
				
				&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
			}
			else
			{
				&CouncilsScrapperCore::writtingToStatus('GCS001',$councilCode,$councilName,$pingResponseStatus,$logsDirectory,$categoryType);
			}

			###
			# Update input date range search terminated response count into TBL_SCRAP_STATUS 
			###
			&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Terminated',$categoryType,$councilName);		
		}
		else
		{
			my ($insertQuery, $actualScrappedCount);
			if($onDemand ne "")
			{
				($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@appLinks,'ONDEMAND001');
			}
			else
			{
				($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@appLinks,'GCS001');
			}
			
			###
			# Insert application details in ScreenScrapper Database for Planning
			###
			&CouncilsScrapperCore::scrapeDetailsInsertion($insertQuery,$categoryType);
			# <STDIN>;		
				
			print "Total Application Links Count for ths weekly List range is \:\t$actualSearchCount\n";
			
			print "But actual Scrapped Count is\:\t $actualScrappedCount\n";
			
			# print "actualScrappedCount==>$actualScrappedCount\n"; <STDIN>;
			
			if($actualScrappedCount>=1)
			{	
				###
				# Update input date range search success response count into TBL_SCRAP_STATUS 
				###
				
				&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
				
				
				my $dt = DateTime->now;
				my $FromDate = $dt->ymd('-');
				
				my $ToDate = $FromDate;
					
				###
				# Insert actual scrapped details for Dashboard
				###
				if($onDemand ne "")
				{			
					&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, $actualSearchCount, $actualScrappedCount, '200', $rawFileName, $scheduleDate);
				}
				else
				{				
					&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, $actualSearchCount, $actualScrappedCount, '200', $rawFileName, $scheduleDate);
				}
			}
			
			
			###
			# Insert Scrape Status in Database
			###
			
			if($onDemand ne "")
			{
				&CouncilsScrapperCore::writtingToLog('End','ONDEMAND001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
			}
			else
			{
				&CouncilsScrapperCore::writtingToLog('End','GCS001',$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
			}
			
			# return(\@appLinks, $deDup_Query);
		}
	}
	
	
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
	}
}



####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationDetails()
{
    my $passedApplicationLink = shift;
    my $rangeName = shift;
	
	my @passedApplicationLinks = @{$passedApplicationLink};


	my $insertQuery;
	
	print "GCS==>$rangeName\n";
	
	my $resultsCount=0;
	my ($fullAppPageLink);
    foreach my $eachApplicationLink (@passedApplicationLinks)
    {		
		$fullAppPageLink=$eachApplicationLink;
		# print "Going to fetch ==> $fullAppPageLink\n"; <STDIN>;
		my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &WeeklyCouncilsController::getApplicationDetails($fullAppPageLink, $councilApp, $councilCode, $categoryType);
		
		if($councilCode=~m/^376$/is)
		{
			$applicationNumber=~s/\,\s*/ \- /is;
		}
		elsif($councilCode=~m/^478$/is)
		{	
			$applicationNumber=~s/^\s*([^<]*?)\s*\-/$1/is;
		}
		
		if(($applicationNumber ne "") && ($pageResponse=~m/^\s*(200|ok)\s*$/is))
		{
			$resultsCount++;
			
			## Insert new application in redis server			
			$redisConnection->set($councilCode."_".$applicationNumber => $applicationNumber);
			print "Not Exists : $applicationNumber\n";
		}
		
		my $tempSource;
		if($onDemand eq 'onDemand')
		{
			$tempSource = "onDemand_".$rangeName;
		}
		else
		{
			$tempSource = $rangeName;
		}
		
		if($applicationNumber ne "")
		{
			my ($insert_query_From_Func) = &WeeklyCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$fullAppPageLink,$tempSource,$councilCode,$councilName,$scheduleDate,$scheduleDateTime,$categoryType, $councilApp);
			
			$insertQuery.=$insert_query_From_Func;
		}
    }
	
	# print "$resultsCount\n"; 
		
	return($insertQuery, $resultsCount);
}



