#!/usr/bin/perl
use strict;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Basename;
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use URI::URL;
use RedisDB;


############
# How to Run
#
# perl FastwebScraperDecision.pl "9" "Fastweb_Decision" "2018/05/11T11:30:00" "2018/05/11T11:30:00"
#
# For ONDEMND#
#
# perl FastwebScraperDecision.pl 1 Fastweb_Decision GCS001 09/07/2018 16/07/2018 "2018/07/16T10:53:00" "2018/07/16T10:53:00" onDemand
#
##############

####
# Declare global variables
####

my $councilCode = $ARGV[0];
my $category = $ARGV[1];
my $source = $ARGV[2];
my $fDate = $ARGV[3];
my $tDate = $ARGV[4];
my $startDateTime = $ARGV[5];
my $scheduleDateTime = $ARGV[6];
my $schedule = $ARGV[7];
my $onDemandID = $ARGV[8];
my $onDemand = $ARGV[9];

$startDateTime=~s/T/ /s;
$scheduleDateTime=~s/T/ /s;
print "\n===========================================================================================================\n";
print "\tCouncil Code::$councilCode\tCategory::$category\tScriptStartDate::$startDateTime\tScheduleDateTime::$scheduleDateTime\n";
print "===========================================================================================================\n";

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');

print "ScheduleDate==>$scheduleDate\n";
# <STDIN>;

my ($fromDate, $toDate, $rangeName,$Schedule_no,$categoryType);

my ($divCounter, $scheduleCounter);
my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n";

if($category=~m/^Fastweb_Decision$/is)
{
	$categoryType="Decision";
}


####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/etc/Fastweb');
my $libControlDirectory = ($basePath.'/lib/Control');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $logsDataDirectory = ($basePath.'/data');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');


require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($libControlDirectory.'/FastwebCouncilsController.pm'); # Private Module
require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Redis_connection_Decision
####
my $redisConnection = &Glenigan_DB::Redis_connection_Decision();


####
# Create new object of class Config::Tiny
####

my ($regexFile,$councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils, control properties for date range clculation and list of all councils repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Fastweb_Council_Decision_Details.ini');
$regexFile = Config::Tiny->read($ConfigDirectory.'/Fastweb_Regex_Heap_Decision.ini' );

####
# Get Council Name From INI file
####
my $councilName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};

####
# Get Schedule number based on Forenoon or Afternoon
####
$Schedule_no=&CouncilsScrapperCore::scheduleNumber(localtime());

if(($onDemand eq '') or ($onDemand=~m/^\s*$/is))
{
	if( !$councilCode || !$category || !$startDateTime || !$scheduleDateTime) 
	{
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Input Argument Missing',$categoryType,$councilName);
		exit;
	}
}

####
# Get class name for Mechanize method
####
my ($councilApp,$pingStatus,$paginationContent) = &FastwebCouncilsController::setVerficationSSL($councilCode,$categoryType,'Fastweb','10',$scheduleDate,$Schedule_no,$councilName);


if($pingStatus!~m/^\s*(200|Ok)\s*$/is)
{
	&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Terminated',$categoryType,$councilName);	
	
	my $rawFileName;
	if($onDemand ne "")
	{
		$rawFileName=&FastwebCouncilsController::rawFileName($startDateTime,$councilCode,'ONDEMAND001',$categoryType);
	}
	else
	{
		$rawFileName=&FastwebCouncilsController::rawFileName($startDateTime,$councilCode,'GCS001',$categoryType);
	}
		
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContent,$searchLogsDirectoryPath,$categoryType,'Failed');	
	
	my $dt = DateTime->now;
	my $FromDate = $dt->ymd('-');
	my $dt2 = $dt->clone->subtract( weeks => 1);
	my $ToDate = $dt2->ymd('-');
	
	if($onDemand ne "")
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
		
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
		
		my $onDemandStatus;
		if($pingStatus > 500)
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		elsif( $pingStatus >= 400 and $pingStatus <= 499 )
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		else
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		
		&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
	}
	else
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
	}
	exit;
}


####
# Beginining of script execution
####

&main();



sub main
{
    # Calculating default Date Ranges based on values from control file
    my %defaultDateRanges;
	if(($source ne "") && ($fDate ne "") && ($tDate ne ""))
	{
		my $strp = DateTime::Format::Strptime->new(
        # pattern  => '%d/%m/%Y',
		pattern  => '%Y-%m-%d',
		);
		my $inputFromDate = $strp->parse_datetime( $fDate );
		my $inputToDate = $strp->parse_datetime( $tDate );
		# print "$inputFromDate<==>$inputToDate\n"; <STDIN>;
		$defaultDateRanges{$source} = [$inputFromDate, $inputToDate];
	}
	else
	{
		%defaultDateRanges = &CouncilsScrapperCore::defaultGcsRange( DateTime->now, $onDemand , $category,$schedule);
	}
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandUpdate($councilCode,$categoryType,$onDemandID);
	}

    # Dynamic Scheduling Logic (DSL) - 
    # Divides the the specific GCS schedule if the council website have cap-off on maximum search results returned
	
    foreach my $rangeName (sort keys %defaultDateRanges) 
    {
		print "\n#####################################################################\n";
		print "GCS:$rangeName\tCouncilCode:$councilCode\tCouncilName:$councilName\tCategory::$categoryType";
		print "\n#####################################################################\n";
        $divCounter = 1;
        $scheduleCounter = 0;
		
		####
		# Insert Scrape Status in Database
		####
		&CouncilsScrapperCore::writtingToLog('Start',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		
		my $FromDate = $defaultDateRanges{$rangeName}[0];
		my $ToDate = $defaultDateRanges{$rangeName}[1];
		
		# print "FromDate:$FromDate\n";
		# print "ToDate:$ToDate\n";
				
		####
		# Insert Table: Scrape current Status and Council Type in Database
		####
		&CouncilsScrapperCore::scrapeStatusInsertion($Schedule_no,$scheduleDate,$rangeName,$councilCode,'Running',$categoryType,$councilName);
		
        
        my $checkResult = &defaultRangeCheck(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
		my (@totalApplicationLinks,$deDupRedisQuery,$pageSearchCode, $actualSearchCount);
		
        if ($checkResult == 1)
        {
			print "Search Page Response::$checkResult(Expected value also 1).\n";
            ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
            my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($rangeName);		
			@totalApplicationLinks = @$recievedApplicationLinks;
			$pageSearchCode = $responseCode;
			$actualSearchCount = $SearchCount;
			$deDupRedisQuery = $deDupQuery;
        }
        else
        {
			print "Search Page Response::$checkResult(Expected value is 1).\n";
            my %subRanges = &calculateSubRanges($divCounter, @{$defaultDateRanges{$rangeName}}[1],$rangeName);
            my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &subRangesSearchResult(\%subRanges);		
			@totalApplicationLinks = @$recievedApplicationLinks;
			$pageSearchCode = $responseCode;	
			$actualSearchCount = $SearchCount;		
			$deDupRedisQuery = $deDupQuery;			
        }
			
		my $totalLinksCount =@totalApplicationLinks;
		print "Total Application Links Count for the date range ".$FromDate->dmy('/')." to ".$ToDate->dmy('/')." is \:\t$actualSearchCount\n";
		
		print "\nBut valid count for this date range is \:\t$totalLinksCount\n";
		
		my ($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@totalApplicationLinks,$rangeName);	
		
		####
		# Insert application details in ScreenScrapper Database for Decision
		####
		&CouncilsScrapperCore::scrapeDetailsInsertion($insertQuery,$categoryType);
				
		
		####
		# Get file name to store search page content in log folder
		####		
		my $rawFileName=&FastwebCouncilsController::rawFileName($startDateTime,$councilCode,$rangeName,$categoryType);			
		
		
		####
		# Insert actual scrapped details for Dashboard
		####
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, $rangeName, $fromDate, $toDate, $actualSearchCount, $actualScrappedCount, $pageSearchCode, $rawFileName, $scheduleDate);
		
		
		####
		# Insert deDup details
		####
		if($deDupRedisQuery ne "")
		{
			&CouncilsScrapperCore::deDupInsertion($deDupRedisQuery);
		}
		
		if($pageSearchCode=~m/^\s*(?:200|ok)\s*$/is)
		{	
			####
			# Update input date range search success response count into TBL_SCRAP_STATUS 
			####
			
			&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
		}
		
		####
		# Insert Scrape Status in onDemand Database
		####
		if($onDemand eq 'onDemand')
		{
			my $onDemandStatus;
			if($actualSearchCount ne "")
			{
				if(($actualSearchCount=~m/^0$/is) or ($actualSearchCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No Application found";
				}
				elsif(($totalLinksCount=~m/^0$/is) or ($totalLinksCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No new application found";
				}
				else
				{
					$onDemandStatus = "\"$actualSearchCount\" Application found. But Valid application count: \"$totalLinksCount\"";
				}
			}
			&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
		}	
		
		####
		# Insert Scrape Status in Database
		####
		
		&CouncilsScrapperCore::writtingToLog('End',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
    }
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
	}
}




####
# Function to pass GCS sub-ranges dates to fetch search result
####

sub subRangesSearchResult
{
    my ($subRangesParam) = @_;
    my %subRanges = %$subRangesParam;
	
	my (@recAppLinks, $deDupQuery, $recAppLink, $responseCode, $SearchCount);
    foreach my $subRangeName (sort keys %subRanges)
    {
        ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$subRanges{$subRangeName}}[0], @{$subRanges{$subRangeName}}[1]);
        ($recAppLink, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($subRangeName); 
		my @recAppLink = @$recAppLink;	
		push(@recAppLinks,@recAppLink);
    }

    return(\@recAppLinks, $deDupQuery, $responseCode, $SearchCount);
}

####
# Function to match the search result count from response
####

sub fetchSearchResult
{
    my($passedsubRangeName) = @_;
    my($difference);

	my @totalURL;
    $difference = $toDate->delta_days( $fromDate );
    $scheduleCounter = $scheduleCounter + $difference->delta_days;
    print "GCS::$passedsubRangeName ==> FromDate::".$fromDate->dmy('/')." to ToDate::".$toDate->dmy('/')." ~ NoOfDaysRange::".$difference->delta_days."\t";
    my ($responseContents,$responseStatus) = &callCouncilsMechMethod($fromDate, $toDate);
	
	
    # Get number of results
    my $searchResultCount;
	if($responseContents =~ /$councilDetailsFile->{$councilCode}->{'SEARCH_RESULTS_COUNT_RX'}/is)
	{
		$searchResultCount = $1;
	}
	
	if($searchResultCount eq "")
	{
		my $resultcnt=0;
		while($responseContents =~ m/$councilDetailsFile->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
		{
			$resultcnt++;
		}
		
		$searchResultCount = $resultcnt;
	}
    print "SearchResult: $searchResultCount\n";
	
	
	####
	# Insert input date range ping response into DB 
	####
	if($responseStatus!~m/^\s*(?:200|ok)\s*$/is) # Only If Fails
	{
		&CouncilsScrapperCore::writtingToStatus($passedsubRangeName,$councilCode,$councilName,$responseStatus,$logsDirectory,$categoryType);

		####
		# Update input date range search terminated response count into TBL_SCRAP_STATUS 
		####
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$searchResultCount,$councilCode,'Terminated',$categoryType,$councilName);		
	}
	
	my $paginationContents;
	if($responseContents=~m/$regexFile->{'MainRegexControl'}->{'NEXT_PAGE_LINK_RX'}/is)
	{
		$paginationContents = &paginationResponse($councilCode, $councilApp, $responseContents, $fromDate, $toDate);
	}
	else
	{
		$paginationContents = $responseContents;
	}
	
	my $rawFileName=&FastwebCouncilsController::rawFileName($startDateTime,$councilCode,$passedsubRangeName,$categoryType);	
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContents,$searchLogsDirectoryPath,$categoryType,'Success');	
		
	
	my ($totalURL, $duplicateQuery) = &getApplicationLinks($paginationContents, $passedsubRangeName);
	@totalURL=@$totalURL;
	
    return(\@totalURL, $duplicateQuery, $responseStatus, $searchResultCount);
}


####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationLinks()
{
	my $totalContent = shift;
	my $gcsRange = shift;
		
	my (@appLinks, $deDup_Query); 
	# while($totalContent =~ m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
	while($totalContent =~ m/$councilDetailsFile->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
	{
		my $Applink = $1;
		my $ApplicationNumber = $2;
				
		# print "Before::$Applink\n";
		my $fullAppPageLink;		
		if($Applink!~m/https?\:/is)
		{
			$fullAppPageLink = URI::URL->new($Applink)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
		}	
		else
		{
			$fullAppPageLink = $Applink;
		}	
		$fullAppPageLink =~ s/\&amp\;/\&/g;
		$fullAppPageLink =~s/\'/\'\'/igs;
		
		# print "After::$fullAppPageLink\n"; 
		# <STDIN>;
		
		if($councilCode=~m/^(195|454)$/is)
		{
			$fullAppPageLink=~s/fastweb\/detail.asp/fastweb\/fulldetail\.asp/is;
		}
		elsif($councilCode=~m/^(91|289|333|367)$/is)
		{
			$fullAppPageLink=~s/\/detail.asp/\/fulldetail\.asp/is;
		}
		elsif($councilCode=~m/^318$/is)
		{
			$fullAppPageLink=~s/fastweb_PL\/detail.asp/fastweb_PL\/fulldetail\.asp/is;
		}
		elsif($councilCode=~m/^332$/is)
		{
			$fullAppPageLink=~s/fastweblive\/detail.asp/fastweblive\/fulldetail\.asp/is;
		}
		elsif($councilCode=~m/^444$/is)
		{
			$fullAppPageLink=~s/FastWebPL\/detail.asp/FastWebPL\/fulldetail\.asp/is;
		}

		
		if($redisConnection->exists("D_".$councilCode."_".$ApplicationNumber))	
		{
			print "Exists : $ApplicationNumber\n";	
			
			my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$ApplicationNumber\', \'$fullAppPageLink\', \'$categoryType\', \'$gcsRange\',\'$councilName\'),";
			$deDup_Query.=$deDupQuery;
			
			next;
		}
	
		
		push ( @appLinks, $fullAppPageLink);
	}
	
	# print "deDup_Query==>$deDup_Query\n"; <STDIN>;
	
	return(\@appLinks, $deDup_Query);
}



####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationDetails()
{
    my $passedApplicationLink = shift;
    my $rangeName = shift;
	
	my @passedApplicationLinks = @{$passedApplicationLink};


	my $insertQuery;
	
	print "GCS==>$rangeName\n";
	
	my $resultsCount=0;
	my ($fullAppPageLink);
    foreach my $eachApplicationLink (@passedApplicationLinks)
    {		
		$fullAppPageLink=$eachApplicationLink;
		# print "Going to fetch ==> $fullAppPageLink\n";
		sleep(2);
		my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &FastwebCouncilsController::getApplicationDetails($fullAppPageLink, $councilApp, $councilCode, $categoryType);
		
		if(($applicationNumber ne "") && ($pageResponse=~m/^\s*(200|ok)\s*$/is))
		{
			$resultsCount++;
			
			## Insert new application in redis server			
			$redisConnection->set("D_".$councilCode."_".$applicationNumber => $applicationNumber);
			print "Not Exists : $applicationNumber\n";
		}
		
		my $tempSource;
		if($onDemand eq 'onDemand')
		{
			$tempSource = "onDemand_".$rangeName;
		}
		else
		{
			$tempSource = $rangeName;
		}
		
		if($applicationNumber ne "")
		{
			my ($insert_query_From_Func) = &FastwebCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$fullAppPageLink,$tempSource,$councilCode,$councilName,$scheduleDate,$scheduleDateTime,$categoryType, $councilApp);
			
			$insertQuery.=$insert_query_From_Func;
		}
    }
	
	# print "$resultsCount\n"; 
		
	return($insertQuery, $resultsCount);
}

####
# Common method to inisialise Mechanize user agent
####

sub callCouncilsMechMethod
{
    my($passedFromDate, $passedToDate) = @_;
	my ($councilAppResponse, $scriptStatus) = &FastwebCouncilsController::councilsMechMethod($passedFromDate->dmy('/'), $passedToDate->dmy('/'), $councilCode, $councilApp, $categoryType);
    return ($councilAppResponse, $scriptStatus);
}


####
# Function to check the default date rnages from the GCS schedule needs to be split
####

sub defaultRangeCheck
{
    my ($passedFromDate, $passedToDate) = @_;

    return( &rangeCheckMethod($passedFromDate, $passedToDate) );
}


####
# Function to check the search result page response to see if it returns in error
####

sub rangeCheckMethod
{
    my ($passedFromDate,$passedToDate) = @_;

    my ($responseContents,$responseStatus) = &callCouncilsMechMethod($passedFromDate, $passedToDate);
	
	
    my $searchValue;
    
	if($responseContents=~m/$councilDetailsFile->{$councilCode}->{'ERR_MSG_RX'}/is)
	{	
		$searchValue = $1;
	}
	elsif($responseContents=~m/No\s*results\s*were\s*found\s*for\s*this\s*search\s*criteria/is)
	{	
		next;
	}
	elsif($responseContents=~m/<th[^>]*?>\s*Planning\s*application\s*number\:\s*<\/th>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/is)
	{	
		return(1);
	}
	elsif($responseContents=~m/<td[^>]*?>(?:App\.\s*No\.|Planning\s*application\s*number)\s*\:\s*<\/td>\s*<td[^>]*?>\s*<a\s*href\=\"([^>]*?)\"[^>]*?>\s*([^>]*?)\s*<\/a>\s*<\/td>/is)
	{	
		return(1);
	}
	
	if($searchValue=~m/WARNING/s)
    {
        next;
    }
	elsif($searchValue=~m/No\s*Match/is)
	{
		next;
	}
	elsif($searchValue=~m/No\s*Result/is)
	{
		next;
	}
	elsif($searchValue=~m/No\s*applications\s*found/is)
	{
		next;
	}
	elsif($searchValue=~m/Date\s*is\s*invalid/is)
	{
		next;
	}
	elsif($searchValue=~m/retrieve\s*more\s*than\s*200\s*results/is)
	{
		return(0);
	}
    else
    {
        return(1);
    }
}


####
# Function to calculate GCS sub-ranges and pass the resulting date range into rangeCheckMethod is see the $resultValue results with 1
####

sub calculateSubRanges
{
    my ($divCount, $passedToDate, $gcsRange) = @_;
    my (%subRangeCheck, $resultValue);
	print "gcsRange==>$gcsRange\n";
	
    do
    {
        %subRangeCheck = &CouncilsScrapperCore::subRangesCalcMethod($divCount, $passedToDate, $onDemand);
        my ($subRangeFromDate, $subRangeToDate);
		if($onDemand ne "")
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'ONDEMAND001'}}[0], @{$subRangeCheck{'ONDEMAND001'}}[1]);
		}
		else
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'GCS001'}}[0], @{$subRangeCheck{'GCS001'}}[1]);
		}
        $resultValue = &rangeCheckMethod($subRangeFromDate, $subRangeToDate);
        $divCount = $divCount + 1;
		if($divCount==5)
		{
			# last;
			return(%subRangeCheck);
		}

    } while ($resultValue == 0);
	
    return(%subRangeCheck);
}



####
# Function to fetch pagenation response for resulting search result page
####

sub paginationResponse
{	
	my ($councilCode, $councilApp, $responseContents, $fromDate, $toDate) = @_;
	
    $responseContents = &FastwebCouncilsController::searchPageNext($councilCode, $councilApp, $categoryType, $responseContents, $fromDate, $toDate);
		
    return($responseContents);
}