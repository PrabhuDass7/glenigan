use strict;
use Cwd qw(abs_path);
use File::Basename;
use Config::Tiny;
use Time::Piece;
use File::Path qw/make_path/;


my $category = $ARGV[0]; 	# Council Type(ie., Planning or Decision)

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root\s*$//si;
print "$basePath\n";
# <STDIN>;


####
# Declare and load local directories and required private module
####
my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $rootDirectory = ($basePath.'/root');

require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module



# Establish connection with DB server
my $dbHostPlanning = &Glenigan_DB::screenscrapperDB();
my $dbHostDecision = &Glenigan_DB::gleniganDB();


my ($councilCodeArray,$sourceArray,$fromDateArray,$toDateArray,$onDemandID);
if($category eq "Planning")
{
	($councilCodeArray,$sourceArray,$fromDateArray,$toDateArray,$onDemandID) = &Glenigan_DB::retrieveOnDemondInput_CC_Planning($dbHostPlanning);
}
elsif($category eq "Decision")
{
	($councilCodeArray,$sourceArray,$fromDateArray,$toDateArray,$onDemandID) = &Glenigan_DB::retrieveOnDemondInput_CC_Decision($dbHostDecision);
}

my @councilCode = @{$councilCodeArray};
my @sourceRanges = @{$sourceArray};
my @fromDateRanges = @{$fromDateArray};
my @toDateRanges = @{$toDateArray};
my @onDemandID = @{$onDemandID};


####
# Create new object of class Config::Tiny and Config::Simple
####
my ($coreCouncilFile) = Config::Tiny->new();
$coreCouncilFile = Config::Tiny->read($ConfigDirectory.'/Council_Format_List.ini' );


my $time = Time::Piece->new;
my $todaysDate = $time->ymd(''); # get Current date
my $scheduleDate= $time->strftime('%d/%m/%Y %H:%M:%S');

my $runDateTime = Time::Piece->strptime($scheduleDate, '%d/%m/%Y %H:%M:%S')->strftime('%Y/%m/%dT%H:%M:%S');
print "RunDateTime:::: $runDateTime\n";

my $startDateTime= $time->strftime('%Y/%m/%dT%H:%M:%S');
print "StartDateTime:::: $startDateTime\n";


for(my $reccnt = 0; $reccnt < @councilCode; $reccnt++ )
{
	my $inputCouncilCode = $councilCode[$reccnt];
	my $inputSource = $sourceRanges[$reccnt];	
	my $fromDate = $fromDateRanges[$reccnt];
	my $toDate = $toDateRanges[$reccnt];
	my $onDemandIDs = $onDemandID[$reccnt];
	
	if(($fromDate eq "") or ($toDate eq ""))
	{
		print "Please check OnDemand table FromDate and ToDate!..\n";
		next;
	}
	
	
	# print "inputCouncilCode==>$inputCouncilCode\n";
	# print "inputSource==>$inputSource\n";<STDIN>;
	
	my $codeFormat = $coreCouncilFile->{$inputCouncilCode}->{'Format'};
	
	next if($codeFormat eq "");
	
	if($category eq "Planning")
	{
		$codeFormat = $codeFormat.'_Planning';
	}
	elsif($category eq "Decision")
	{
		$codeFormat = $codeFormat.'_Decision';
	}	
	print "codeFormat: $codeFormat\n";

	my ($scriptName,$Execution_Status,$Return_Code,$formatType,$categoryType);		
	if($codeFormat=~m/^([^_]*?)_([^<]*?)$/is)
	{
		$formatType = $1;
		$categoryType = $2;
		
		$scriptName = $formatType."Scraper".$categoryType.".pl";
	}
	
	
	my $scriptRootDirectory = $rootDirectory.'/'.$formatType;
		
	# print "PATH==>perl $scriptRootDirectory/$scriptName $inputCouncilCode $codeFormat $inputSource $fromDate $toDate $startDateTime $runDateTime"; <STDIN>;
	
	if($codeFormat=~m/^(Weekly_Planning|Weekly_Decision)$/is)
	{
		eval{		
			$Return_Code = system("perl $scriptRootDirectory/$scriptName $inputCouncilCode $codeFormat $startDateTime $runDateTime $onDemandIDs onDemand &");
		};
	}
	else
	{			
		my ($inputFromDate,$inputToDate);
		if($fromDate=~m/^[A-Za-z]{3}\s+\d+\s+\d{4}/is)
		{
			my $inputFrom = Time::Piece->strptime($fromDate, '%b %d %Y %H:%M%p');
			my $inputTo = Time::Piece->strptime($toDate, '%b %d %Y %H:%M%p');
			
			$inputFromDate = $inputFrom->strftime('%Y-%m-%d');
			$inputToDate = $inputTo->strftime('%Y-%m-%d');
		}
		else
		{
			$inputFromDate = $fromDate;
			$inputToDate = $toDate;			
		}
    
		print "Inputs==>CouncilCode::$inputCouncilCode\tSource::$codeFormat\tFromDate::$fromDate\tToDate::$toDate\n";
		eval{		
			$Return_Code = system("perl $scriptRootDirectory/$scriptName $inputCouncilCode $codeFormat $inputSource $inputFromDate $inputToDate $startDateTime $runDateTime $onDemandIDs onDemand &");
		};
	}
	
	my $statusLog = 'ScheduleTriggerLog';	
	my $logLocation = $logsDirectory.'/'.$statusLog.'/'.$todaysDate;
	
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	
	open(ERR,">>$logLocation/onDemandFailedLog.txt");	
	if($Return_Code!=0)
	{
		$Execution_Status="Failed to Execute";
		print "CouncilCode:$inputCouncilCode\tCategory:$codeFormat\tStatus:$Execution_Status : $!\n";
		print ERR "CouncilCode::$inputCouncilCode\tCategory::$codeFormat\tSource::$inputSource\tFromDate::$fromDate\tToDate::$toDate\tTime::$scheduleDate\tStatus::$Execution_Status: $!\n";
	}
	else
	{
		$Execution_Status="Success";
		print "Status: $Execution_Status : $!\n";
		print ERR "CouncilCode::$inputCouncilCode\tCategory::$codeFormat\tSource::$inputSource\tFromDate::$fromDate\tToDate::$toDate\tTime::$scheduleDate\tStatus::$Execution_Status: $!\n";
	}
	close ERR;
		
}
