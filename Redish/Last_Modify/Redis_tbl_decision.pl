use strict;
use WWW::Mechanize;
use MIME::Lite;
use MIME::Base64;
use Net::SMTP;
use File::Basename;
use Cwd  qw(abs_path);
use Redis;
use RedisDB;
use DateTime;

my $redis = RedisDB->new(host => '172.27.139.39:6379', database => 7); # for Decision
print "redis::$redis\n";
$redis->get('key');

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# my $libCoreDirectory = ($basePath.'/GleniganScrapingServices/councilscrapers-new-dev/lib/Core');
my $libCoreDirectory = ($basePath.'/GSS/councilscrapers-new-dev/lib/Core');

require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

#Decision
my $dbh = &Glenigan_DB::gleniganDB();

my $decision_blank_query='select council_code,application_no from glenigan..tbl_decision_redis where application_no!=\'\' ORDER BY council_code';

my (@council_code_application)=&Execute($decision_blank_query,$dbh);
# print "council_code_application::".scalar @council_code_application."\n\n";

foreach my $council_code_application (@council_code_application)
{
	my ($council_code,$application)=split('_',$council_code_application);
	$council_code_application='D_'.$council_code_application;
	$council_code_application=~s/\//\//igs;
	$council_code_application=~s/\s+//igs;
	$council_code=~s/\s+//igs;
	# print "council_code::$council_code\n\n";
	# print "application::$application\n\n";
	# print "council_code_application::$council_code_application\n";
	# <STDIN>;
	my $keys = $redis->keys( "D_${council_code}_*" ); # for Decision
	my @keys = @$keys; # for Decision
	# print "keys::@keys\n\n";
	unless(@keys~~$council_code_application)
	{
		print "rediskeymatch:: $council_code_application\n";
		open(F1,">>Deleted_keys_decision.txt");
		print F1 $council_code_application."\n";
		close F1;
		# <STDIN>;
		$redis->del ($council_code_application);
		print "deleted\n\n";
	}
	else
	{
		next;
	}
}

sub Execute()
{
	my $Query=shift;
	my $dbh=shift;
	my $sth = $dbh->prepare($Query);
	my (@council_code_application);
	if($sth->execute())
	{
		print "Executed\n";
		my @row;
		while (@row = $sth->fetchrow_array())
		{
			# print "row:::@row\n\n";
			# print "row[0]:::$row[0]\n\n";
			# print "row[1]:::$row[1]\n\n";			
			my $council_code=$row[0];
			my $application=$row[1];
			push (@council_code_application, $council_code.'_'.$application);
		}		
	}
	else
	{
		print "QUERY:: $Query\n";
		open(ERR,">>Failed_Query_redis_decision.txt");
		print ERR $Query."\n";
		close ERR;
		
		&Send_Alert();
	}
	
	return (@council_code_application);
}

sub Send_Alert()
{
	my $Current_dateTime=localtime();
    my $subject="Glenigan - DB Import Error - $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='prabhu.dass@meritgroup.co.uk';
    my $cc ='santhosh.kailasan@meritgroup.co.uk';
    my $pass='11meritgroup11';
    my $body = "Hi Team, \n\n\t\tPlease check the Import_DB process.\n\nRegards\nGlenigan Team";
	
	my $file_path="/home/merit/Glenigan_New/Report";
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}
