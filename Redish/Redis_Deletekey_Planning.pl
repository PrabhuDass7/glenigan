use strict;
use Redis;
use RedisDB;

# my $redis = RedisDB->new(host => '127.0.0.1:6379', database => 7); # for Decision
my $redis = Redis->new(server => '127.0.0.1:6379'); # for Planning
print "redis::$redis\n";
$redis->get('key');
# my $keys = $redis->keys( "D_149_*" ); # for Decision

my @online = ("2010");

# my @online = ("12","34","96","138","174","181","275","284","355","360","371","417","420","469","478","479","603","605","624","629","631","5","22","23","24","26","89","110","112","130","148","193","198","254","334","375","393","470","489");
foreach my $code (@online)
{
	my @keys = $redis->keys( "${code}_*" ); # for Planning
	# my $keys = $redis->keys( "D_${code}_*" ); # for Decision
	# my @keys = @$keys; # for Decision
	foreach my $K(sort @keys)
	{
		if($K=~m/^2010_1816371/is)
		{
			print "$K\n";
			$redis->del ($K);
		}
		else
		{
			next;
		}
	}
}