#!/usr/bin/perl
use strict;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $scriptPath = ($basePath.'/root');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

my @alphabets = ("A".."Z");
foreach my $alphabet(@alphabets)
{
	chomp($alphabet);
	print "Alphabets:: $alphabet\n";

	system("start perl $scriptPath/ARB/arbDetailURL.pl $alphabet &");
	
	sleep (3);
}