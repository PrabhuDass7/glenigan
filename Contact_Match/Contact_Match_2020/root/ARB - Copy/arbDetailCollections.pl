#!/usr/bin/perl
use strict;
use URI::URL;
use URI::Escape;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw(make_path);
use Text::CSV;
use MIME::Lite;
use MIME::Base64;
use Net::SMTP;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

my $councilName = "ARB";
my $from= $ARGV[0];
my $to = $ARGV[1];

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Create new object of class Text::CSV here
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";

####
# Establish connection with DB server
####

my $dbh = &Hopper::DbConnection("Windows");

####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


####
# Get detail Url input from log table
####
# my @inputURL=("http://architects-register.org.uk/architect/068021D","https://architects-register.org.uk/Architect/084115C");

my $inputURL = &Hopper::retrieveDetailURLInput($dbh, $from, $to);
my @inputURL=@{$inputURL};

my $applicationCount = 0;

my $Location = $outputDirectory."/".$toDay;
unless ( -d $Location )
{
	make_path($Location);
}

my $insert_query="insert into ContactsHopper_Source_Dev (Member_Number, Contact_Name, Contact_Email, Company_Name, Company_Address, Company_Postcode, Company_Phone, Company_Website, Source_Name) values ";

my $bulkValuesForQuery='';

my $filename = "$Location/ARBOutputDetails11.csv";

open my $fh, ">:encoding(utf8)", $filename or die "failed to create $filename: $!";
my(@heading) = ("Registration Number", "Members Heading", "Company Name", "Address", "Telephone Number", "Email", "Company Website Address", "Detail URL");
$csv->print($fh, \@heading);    # Array ref!

my $count = 0;
foreach my $url(@inputURL)
{
	print "URL==>$url\n";

	my ($pingStatus, $PageContent);
	($pingStatus, $PageContent)  = &Hopper::MechGetMethod($url);

	# open JJ, ">arb_Detail_Collection_Home.html";
	# print JJ "$PageContent";
	# close JJ;

	if($PageContent!~m/<h1>[\w\W]*?<strong\s*class\=\"[^>]*?>([\w\W]*?)<\/h1>/is)
	{
		open(LL,">>Details_needtoCheck.txt");
		print LL "$url\n";
		close(LL);
	}
	else
	{
		my ($contactDetails, $joinValues) = &getContactDetails($PageContent,$url);	
		my @datarow = split('\t', $contactDetails);
		$csv->print($fh, \@datarow); 
		if($applicationCount == 0)
		{
			$bulkValuesForQuery = $insert_query.$joinValues;
			$applicationCount++;
		}
		elsif($applicationCount == 998)
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			
			if($bulkValuesForQuery!~m/values\s*$/is)
			{
				$bulkValuesForQuery=~s/\,\s*$//igs;
				&Hopper::DB_Insert($dbh,$bulkValuesForQuery);
				$applicationCount = 0;
				$bulkValuesForQuery="";
			}
		}
		else
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			$applicationCount++;
		}
		
	}
	
}
close $fh or die "failed to close $filename: $!";
print "Total Application Number Count::$applicationCount\n";

$bulkValuesForQuery=~s/\,\s*$//igs;
# print "insert_query::$bulkValuesForQuery\n";

if($bulkValuesForQuery!~m/values\s*$/is)
{
	&Hopper::DB_Insert($dbh,$bulkValuesForQuery);
}

# &send_mail();

sub getContactDetails
{
	my $contactCnt = shift;
	my $detailURL = shift;
	
	my ($conDetails, $insertquery);

	if($contactCnt=~m/<div\s*class=\"card\-body\"[^>]*?>([\w\W]*?)<div\s*id\=\"RegistrantDetailsInfoText\"/is)
	{
		my $tableCnt = $1;
		
		my ($memHeading, $companyName, $companyAddress1, $companyAddressPostCode, $companyAddress, $regNumber, $teleNumber, $cntEmail, $cntWebsite);
		
		$memHeading = &Hopper::clean($1) if($tableCnt=~m/<h1>\s*([\w\W]*?)<strong\s*class\=\"[^>]*?>[\w\W]*?<\/h1>/is);
		$companyName = &Hopper::clean($1) if($tableCnt=~m/<div[^<]*?\"Company\">([\w\W]*?)<\/div>\s*<\/div>/is);
		$companyAddress = &Hopper::clean($1) if($tableCnt=~m/<div[^<]*?\"Address\">([\w\W]*?)<\/div>\s*<\/div>/is);
		if($tableCnt=~m/<div[^<]*?\"Address\">([\w\W]*?)\s*<br[^>]*?>\s*([A-Z0-9\s]+)\s*(?:<br[^>]*?>)?\s*<\/div>/is)
		{
			$companyAddress1 = &Hopper::clean($1);
			$companyAddressPostCode = &Hopper::clean($2);
		}
		$regNumber = &Hopper::clean($1) if($tableCnt=~m/<h1>[\w\W]*?<strong\s*class\=\"[^>]*?>([\w\W]*?)<\/h1>/is);
		$teleNumber = &Hopper::clean($1) if($tableCnt=~m/<div[^<]*?\"Telephone\">([\w\W]*?)<\/div>/is);
		$cntEmail = &Hopper::clean($1) if($tableCnt=~m/<div[^<]*?\"Email\">([\w\W]*?)<\/div>/is);
		$cntWebsite = &Hopper::clean($1) if($tableCnt=~m/<div[^<]*?\"Website\">([\w\W]*?)<\/div>/is);
	
		print "Registration Number::$regNumber is Processing..\n";
		
		$conDetails = "$regNumber\t$memHeading\t$companyName\t$companyAddress\t$teleNumber\t$cntEmail\t$cntWebsite\t$detailURL";
		
		$insertquery = "(\'$regNumber\', \'$memHeading\', \'$cntEmail\', \'$companyName\', \'$companyAddress1\', \'$companyAddressPostCode\', \'$teleNumber\', \'$cntWebsite\', \'$councilName\'),";
		
		undef $regNumber;  undef $memHeading;  undef $cntEmail;  undef $companyAddress1;  undef $companyAddress;  undef $companyName;  undef $teleNumber; undef $companyAddressPostCode; undef $cntWebsite;
	}
	
	return ($conDetails, $insertquery);
}

sub send_mail()
{
    my $subject="Contact Match - Alert";
    my $host ='74.80.234.196';
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    # my $to ='prabhu.dass@meritgroup.co.uk';
    my $to ='prabhu.dass@meritgroup.co.uk , sandhiya.saravanan@meritgroup.co.uk, santhosh.kailasan@meritgroup.co.uk';
    my $cc ='';
    # my $cc ='sandhiya.saravanan@meritgroup.co.uk, santhosh.kailasan@meritgroup.co.uk';
    my $pass='sXNdrc6JU';
	
    my $body = "Hi Team, \n\nContact Match - ARB Execution has been Completed.\n\nRegards\nGlenigan Team";

	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";

    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}
