#!/usr/bin/perl
use strict;
use URI::Escape;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Encode qw(encode decode);
use Text::CSV;
use DateTime;
use File::Path qw(make_path);
use Digest::MD5;
use List::MoreUtils qw/uniq/;
my $md5 = Digest::MD5->new;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

my $dt = DateTime->now();
my $toDay = $dt->dmy('');

my $Location = $outputDirectory."/".$toDay;
unless ( -d $Location )
{
	make_path($Location);
}

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Create new object of class Text::CSV here
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";

####
# Establish connection with DB server
####
my $dbh = &Hopper::DbConnection("Windows");

####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');

my $councilName = "ISTRUCTE";

my ($responseContent, $scrollPositionX, $scrollPositionY, $matchedContent, $viewState, $viewStateGenerator);
my $insert_query="insert into ContactsHopper_Source (Member_Number, Contact_Name, Contact_Email, Company_Name, Company_Address, Company_Postcode, Company_Phone, Company_Website, Source_Name) values ";
my $count = 0;

my $filename = "$Location/IStructeOutput_$councilName.csv";
open my $fh, ">>:encoding(utf8)", $filename or die "failed to create $filename: $!";

open HH,">$Location/ISTRUCTE_Output_$toDay.txt";
print HH "Count\tAlphabet\n";
close HH;

open KK,">$Location/ISTRUCTE_Duplicates.txt";
print KK "Member_ID\tURL\n";
close KK;

my $bulkValuesForQuery = '';
my $Dublicate;

my @alphabets = ("A".."Z");
foreach my $alphabet(@alphabets)
{
	chomp($alphabet);
	print "Alphabets:: $alphabet\n";
	my $url="https://www.istructe.org/ise-api/witness/search-member/?currentPage=1&forename=$alphabet&pageSize=1000";

	my ($pingStatus, $responseContent) = &Hopper::MechGetMethod($url);

	# open(PP,">a.html");
	# print PP $responseContent;
	# close(PP);
	# exit;
	
	while($responseContent=~m/(\{\"name\"\:[\w\W]*?(?:\}\,|\}\]\,))/gsi)
	{
		my $postResponseContent = $1;	
		# print "postResponseContent:::$postResponseContent\n";
		
		my ($memNumbers, $contactDetails, $insertQuery) = &getContactDetails($postResponseContent);		
		
		open HH,">>$Location/ISTRUCTE_Output_$toDay.txt";
		print HH "$count\t$alphabet\t$contactDetails\n";
		close HH;
		
		($Dublicate,my $value)=&Duplicate($Dublicate, $memNumbers);
		if($value!=1)
		{
			open KK,">>$Location/ISTRUCTE_Duplicates.txt";
			print KK "$memNumbers\t$url\n";
			close KK;
			
			goto nxt;
		}
		
		if($count == 0)
		{
			$bulkValuesForQuery = $insert_query.$insertQuery;
			$count++;
		}
		elsif($count == 998)
		{
			if($bulkValuesForQuery!~m/values\s*$/is)
			{
				$bulkValuesForQuery=~s/\,\s*$//igs;
				&Hopper::DB_Insert($dbh,$bulkValuesForQuery);
				$bulkValuesForQuery="";
				$count=0;
			}
		}
		else
		{
			$bulkValuesForQuery=$bulkValuesForQuery.$insertQuery;
			$count++;
		}
		
		
		# print "contactDetails::$contactDetails\n";<STDIN>;
		my @datarow = split('\t', $contactDetails);
		$csv->print($fh, \@datarow);
		
		nxt:
	}
}

close $fh or die "failed to close $filename: $!";

$bulkValuesForQuery=~s/\,\s*$//igs;
# print "insert_query::$insert_query\n";

if($bulkValuesForQuery!~m/values\s*$/is)
{
	&Hopper::DB_Insert($dbh,$bulkValuesForQuery);
}

print "*** ISTRUCTE PROCESS IS COMPLETED SUCCESSFULLY ***\n";

sub getContactDetails()
{
	my $contactCnt = shift;
	my $memNumber;
	$memNumber = &Hopper::clean($1) if($contactCnt=~m/\"memberNumber\"\:\"([^>]*?)\"/is);
	print "memNumber:: $memNumber\n";
	
	my $memHeading = &Hopper::clean($1) if($contactCnt=~m/\"name\"\:\"([^>]*?)\"/is);	
	
	my $companyAddress = &Hopper::clean($1) if($contactCnt=~m/\"address\"\:\"([\w\W]*?)\"\,/is);
	my $companyAddressPostCode = &Hopper::clean($1) if($contactCnt=~m/\"address\"[\w\W]*?(?:<br[^>]*?>)?\s*([A-Z0-9\s]+)\s*<br\/>\s*<strong>/is);
	my $companyCountry = &Hopper::clean($1) if($contactCnt=~m/\"address\"[\w\W]*?<strong>([^>]*?)<\/strong>/is);
	my $cntEmail = &Hopper::clean($1) if($contactCnt=~m/\"email\"\:\"([^>]*?)\"\,/is);
	my $teleNumber = &Hopper::clean($1) if($contactCnt=~m/\"phoneNumber\"\:\"([^>]*?)\"\,/is);
	my $cntCPDApproved = &Hopper::clean($1) if($contactCnt=~m/\"cpdStatus\"\:\"([\w\W]*?)\"\,/is);
	
	my $conDetails = "$memNumber\t$memHeading\t$companyAddress\t$companyCountry\t$cntEmail\t$teleNumber\t$cntCPDApproved";
	
	my $insertquery = "(\'$memNumber\', \'$memHeading\', \'$cntEmail\', \'\', \'$companyAddress\', \'$companyAddressPostCode\', \'$teleNumber\', \'\', \'$councilName\'),";
	
	undef $memHeading;  undef $cntEmail; undef $companyAddress; undef $teleNumber; undef $companyAddressPostCode; undef $cntCPDApproved; undef $companyCountry;
	
	return ($memNumber,$conDetails, $insertquery);
}


sub Duplicate()
{
	my $Dublicate=shift;    
	my $string=shift;    
	print "*** string:: $string ***\n";
	$md5->add("$string");
	my $digest = $md5->hexdigest;                                                                                                                
	print "$digest\n";
	 
	if($Dublicate!~/<b>$digest<b>/is)
	{
		my $du_con="<b>".$digest."<b>";                                                                                                                                          
		$Dublicate=$Dublicate.$du_con;              
		return($Dublicate,'1');
	}
	else
	{                              
		return($Dublicate,'0');                          
	}
}