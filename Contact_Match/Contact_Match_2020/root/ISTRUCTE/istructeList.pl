#!/usr/bin/perl
use strict;
use URI::Escape;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Encode qw(encode decode);
use Text::CSV;
use DateTime;
use File::Path qw(make_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

my $dt = DateTime->now();
my $toDay = $dt->dmy('');

my $Location = $outputDirectory."/".$toDay;
unless ( -d $Location )
{
	make_path($Location);
}

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Create new object of class Text::CSV here
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";

####
# Establish connection with DB server
####
my $dbh = &Hopper::DbConnection("Windows");

####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');

my $councilName = "ISTRUCTE";

my ($responseContent, $scrollPositionX, $scrollPositionY, $matchedContent, $viewState, $viewStateGenerator);

my ($pingStatus, $responseContent) = &Hopper::MechGetMethod($councilDetailsFile->{$councilName}->{'URL'});

open(PP,">a.html");
print PP $responseContent;
close(PP);
 # exit;

my $insert_query="insert into ContactsHopper_Source_Dev (Member_Number, Contact_Name, Contact_Email, Company_Name, Company_Address, Company_Postcode, Company_Phone, Company_Website, Source_Name) values ";

my $filename = "$Location/IStructeOutput_$councilName.csv";
open my $fh, ">>:encoding(utf8)", $filename or die "failed to create $filename: $!";

my $count = 0;
while($responseContent=~m/(\{\"name\"\:[\w\W]*?(?:\}\,|\}\]\,))/gsi)
{
	my $postResponseContent = $1;	
	# print "postResponseContent:::$postResponseContent\n";
	$count++;
	my ($contactDetails, $insertQuery) = &getContactDetails($postResponseContent);		
	
	if($count == 998)
	{
		if($insert_query!~m/values\s*$/is)
		{
			$insert_query=~s/\,\s*$//igs;
			# &Hopper::DB_Insert($dbh,$insert_query);
			$insert_query="";
		}
	}
	else
	{
		$insert_query.=$insertQuery;
	}
	# print "contactDetails::$contactDetails\n";<STDIN>;
	my @datarow = split('\t', $contactDetails);
	$csv->print($fh, \@datarow); 
}

close $fh or die "failed to close $filename: $!";

$insert_query=~s/\,\s*$//igs;
# print "insert_query::$insert_query\n";

if($insert_query!~m/values\s*$/is)
{
	# &Hopper::DB_Insert($dbh,$insert_query);
}

sub getContactDetails
{
	my $contactCnt = shift;
	
	my $memNumber = &Hopper::clean($1) if($contactCnt=~m/\"memberNumber\"\:\"([^>]*?)\"/is);
	print "memNumber:: $memNumber\n";
	
	my $memHeading = &Hopper::clean($1) if($contactCnt=~m/\"name\"\:\"([^>]*?)\"/is);	
	
	my $companyAddress = &Hopper::clean($1) if($contactCnt=~m/\"address\"\:\"([\w\W]*?)\"\,/is);
	my $companyAddressPostCode = &Hopper::clean($1) if($contactCnt=~m/\"address\"[\w\W]*?(?:<br[^>]*?>)?\s*([A-Z0-9\s]+)\s*<br\/>\s*<strong>/is);
	my $companyCountry = &Hopper::clean($1) if($contactCnt=~m/\"address\"[\w\W]*?<strong>([^>]*?)<\/strong>/is);
	my $cntEmail = &Hopper::clean($1) if($contactCnt=~m/\"email\"\:\"([^>]*?)\"\,/is);
	my $teleNumber = &Hopper::clean($1) if($contactCnt=~m/\"phoneNumber\"\:\"([^>]*?)\"\,/is);
	my $cntCPDApproved = &Hopper::clean($1) if($contactCnt=~m/\"cpdStatus\"\:\"([\w\W]*?)\"\,/is);
	
	my $conDetails = "$memNumber\t$memHeading\t$companyAddress\t$companyCountry\t$cntEmail\t$teleNumber\t$cntCPDApproved";
	
	my $insertquery = "(\'$memNumber\', \'$memHeading\', \'$cntEmail\', \'\', \'$companyAddress\', \'$companyAddressPostCode\', \'$teleNumber\', \'\', \'$councilName\'),";
	
	undef $memNumber;  undef $memHeading;  undef $cntEmail; undef $companyAddress; undef $teleNumber; undef $companyAddressPostCode; undef $cntCPDApproved; undef $companyCountry;
	
	return ($conDetails, $insertquery);
}

print "*** ISTRUCTE PROCESS IS COMPLETED SUCCESSFULLY ***\n";
