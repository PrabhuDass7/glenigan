use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use File::Path qw/make_path/;
use Config::Tiny;
use DateTime;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

my $Log_Location = $logsDirectory."/".$toDay;
unless ( -d $Log_Location )
{
	make_path($Log_Location);
}

open JJ,">$Log_Location/Spliter_Output.txt";
close JJ;

open JJ,">$Log_Location/Spliter_Input.txt";
close JJ;
	
# open FH,"<input2.txt";
# my @inputURL=<FH>;

my $count=0;
my $offset=0;
my $temp=0;
my $Instance_No=0;

require ($libDirectory.'/Hopper.pm'); # Private Module
my $dbh = &Hopper::DbConnection("Windows");
my $inputURL = &Hopper::retrieveInputSpliter($dbh);
my @inputURL=@{$inputURL};

my ($first_Value,$sec_val,$third_val,$last_Val);
foreach my $arr(@inputURL)
{
	$count++;
	$temp=0;
	
	open JJ,">>$Log_Location/Spliter_Input.txt";
	print JJ "$arr\n";
	close JJ;
	
	chomp($arr);
	if($count == 1)
	{
		$first_Value=$arr;
	}
	
	if($count == 3000)
	{
		$sec_val=$arr;
	}
	
	$offset=$offset+1;
	
	# print "OFFSET_VALUE:: $offset\n";
	
	$third_val=@inputURL[$offset];
	
	if(($sec_val != $third_val) && ($count >=3000))
	{
		$Instance_No++;
		
		open JJ,">>$Log_Location/Spliter_Output.txt";
		print JJ "$Instance_No\tperl arbDetailCollections.pl $first_Value $sec_val\n";
		close JJ;
		
		print "INSTANCES NO :: $Instance_No TRIGGERED\n";
		
		system("start perl arbDetailCollections.pl $first_Value $last_Val $Instance_No &");
		
		sleep(3);
		
		$first_Value="";
		$temp=1;
		$count=0;
	}
	
	$last_Val=$arr;
}

if(($first_Value ne "") && ($temp ==0))
{
	$Instance_No++;
	
	open JJ,">>$Log_Location/Spliter_Output.txt";
	print JJ "$Instance_No\tperl arbDetailCollections.pl $first_Value $last_Val\n";
	close JJ;
	
	print "INSTANCES NO :: $Instance_No TRIGGERED\n";
	
	system("start perl arbDetailCollections.pl $first_Value $last_Val $Instance_No &");
}

print "*** All instances are Triggered Successfully ***\n";