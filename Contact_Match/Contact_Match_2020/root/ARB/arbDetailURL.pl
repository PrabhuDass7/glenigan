#!/usr/bin/perl
use strict;
use URI::URL;
use URI::Escape;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw(make_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n";

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

# Establish connection with DB server
my $dbHost = &Hopper::DbConnection("Windows");

my $councilName = "ARB";

my $ID = $ARGV[0];
my $alphabet = $ARGV[1];
my $inputURL = $ARGV[2];

my $Location = $outputDirectory."/".$toDay;
print "Location==>$Location\n";
unless ( -d $Location )
{
	make_path($Location);
}

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');

#--------------------------------------------------

my $sub_URL = $councilDetailsFile->{$councilName}->{'ARB_LIST_URL'};
$sub_URL=~s/<ALPHABET>/$alphabet/igs;

my $Total_Page_no=1;

my $query = "INSERT INTO ContactsScrape_Detail_URLLog (Id, detail_urls, created_date) VALUES ";
my $tempQuery;
my $count = 0;
my $Total_Count = 0;

for (my $page_no=0; $page_no < $Total_Page_no; $page_no++)
{

	# goto nxt_apl if ($page_no == 2);
	
	my $sub_URL_temp=$sub_URL;
	# $sub_URL_temp=~s/<ALPHABET>/$alphabet/igs;
	$sub_URL_temp=~s/<PAGE>/$page_no/igs;
	
	my ($pingStatus, $responseContent) = &Hopper::MechGetMethod($sub_URL_temp);

	# open JJ, ">arb_Detail_URL_Home.html";
	# print JJ "$responseContent";
	# close JJ;

	$Total_Page_no=$1 if(($responseContent=~m/<[^>]*?>\s*Page[^>]*?of\s*([\d]+)\s*<[^<]*?>/is) && ($page_no == 0));
	
	print "Alphabets:: $alphabet *** PGNO:: $page_no *** Total_Page_no:::$Total_Page_no*****\n";
	
	my @Detail_URL=$responseContent=~m/<div\s*class\=\"media\-body\">[\w\W]*?<a\s*href\=\"([^>]*?)\"[^>]*?class\=\"btn\s*btn\-primary\">\s*View\s*<\/a>/igs;
	
	if($#Detail_URL == -1)
	{
		open JJ,">>$logsDirectory/Cross_Ref_$toDay.txt";
		print JJ "$ID\t$alphabet\n";
		close JJ;
		
		goto nxt;
	}
	
	foreach my $url(@Detail_URL)
	{
		chomp($url);
		
		$url = URI::URL->new($url)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );

		# print "URL:: $url\n";
		my $queryValue = "(\'$ID\', \'$url\', GETDATE()), ";
		$tempQuery .= $queryValue;
		$count++;
		$Total_Count++;
		
		if($count=~m/^998$/is)
		{
			$tempQuery=~s/\,\s*$//gsi;

			$tempQuery = $query.$tempQuery;
			
			# print "query==>$tempQuery\n";<STDIN>;
			
			if($tempQuery!~m/values\s*$/is)
			{
				&Hopper::DB_Insert($dbHost,$tempQuery);
				undef $tempQuery;
			}
			
			$count = 0;
		}
	}
	nxt:
}

# nxt_apl:

$tempQuery=~s/\,\s*$//gsi;
$query .= $tempQuery;

# print "Final query==>$query\n";

if($query!~m/values\s*$/is)
{
	&Hopper::DB_Insert($dbHost,$query);
	undef $tempQuery;
}

open KK,">>$logsDirectory/Alphabets_Logs_$toDay.txt";
print KK "$ID\t$alphabet\t$Total_Count\n";
close KK;