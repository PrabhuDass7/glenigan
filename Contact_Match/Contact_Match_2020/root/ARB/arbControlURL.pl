#!/usr/bin/perl
use strict;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $scriptPath = ($basePath.'/root');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

# Establish connection with DB server
my $dbHost = &Hopper::DbConnection("Windows");

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');

my $councilName = "ARB";
my $sub_URL_temp = $councilDetailsFile->{$councilName}->{'ARB_LIST_URL'};

my $Query = "insert into Contacts_ScrapeLog (Region, Region_URL, Created_Date) values ";
my $tempQuery;

my @alphabets = ("A".."Z");
foreach my $alphabet(@alphabets)
{
	chomp($alphabet);
	print "Alphabets:: $alphabet\n";
	my $sub_URL=$sub_URL_temp;
	$sub_URL=~s/<ALPHABET>/$alphabet/igs;
	
	$sub_URL="";
	
	my $queryValue = "(\'$alphabet\', \'$sub_URL\', GETDATE()), ";
	$tempQuery .= $queryValue;
}

$tempQuery=~s/\,\s*$//gsi;
$tempQuery = $Query.$tempQuery;
# print "tempQuery:: $tempQuery\n";<STDIN>;

if($tempQuery!~m/values\s*$/is)
{
	&Hopper::DB_Insert($dbHost,$tempQuery);
	undef $tempQuery;
}


####
# Get input from format table
####

my (@inputID,@inputURL,@inputAlphabets);
my ($inputID,$inputAlphabets,$inputURL) = &Hopper::retrieveInput($dbHost);

my $reccount = @{$inputID};

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{	
	my $ID 		= @$inputID[$reccnt];
	my $Alphabets 	= @$inputAlphabets[$reccnt];
	my $inputURL 	= @$inputURL[$reccnt];
	print "ID:: $ID ** Alphabets:: $Alphabets ** INPUT:: $inputURL\n";
	system("start perl $scriptPath/ARB/arbDetailURL.pl $ID $Alphabets $inputURL &");
	sleep (2);
}	

print "*** ARB DETAIL URL SCRIPTS ARE TRIGERRED *** \n";