#!/usr/bin/perl
package Hopper;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Config::Tiny;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use HTTP::Request;
use LWP::UserAgent;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
# use Net::SSL;
use URI::URL;
use DBI;
use DBD::ODBC;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
$basePath=~s/\\root\s*$//si;

####
# Declare and load local directories
####
my $ConfigDirectory = ($basePath.'/etc/');
my $libDirectory = ($basePath.'/lib/Core');

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
	
my $Mech = WWW::Mechanize->new( 
				ssl_opts => {
								# SSL_version => 'SSLv3',
								SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
								verify_hostname => 1, 
							}
							, autocheck => 0
			);

my $ua = LWP::UserAgent->new(
				ssl_opts => {
								# SSL_version => 'SSLv3',
								SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
								verify_hostname => 0, 
							}
							, autocheck => 0
			);

# $Mech->proxy(['http','https'], 'http://172.27.137.192:3128');			
# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');			

sub MechPostMethod()
{
	my $postURL=shift;
	my $postContent=shift;
	
		
	eval
	{
		$Mech->post( $postURL, Content => "$postContent");
	};
	
		
	my $responseContent = $Mech->content;	
	my $pingStatus = $Mech->status;	
	
	
	return ($responseContent, $pingStatus);	
}


###########################################################################################
#Function    : DbConnection()
#Desc        : Establish the connection to the database
#Date	     : 21-12-2020
############################################################################################
sub DbConnection()
{		
	my $con_Method=shift;
	my $dsn;
	
	if($con_Method eq "Windows")
	{
		$dsn ='DBI:ODBC:driver={SQL Server};Server=10.204.202.246;database=Contacts_Hopper;uid=MeritUser;pwd=Ez@bfyGAf*JDyq3RxQPj;';
	}
	else
	{
		$dsn ="dbi:Sybase:server=10.204.202.246; database=Contacts_Hopper", 'MeritUser', 'Ez@bfyGAf*JDyq3RxQPj';
	}
	
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;

	Reconnect:
	
	if($dbh = DBI->connect($dsn))
	{
		print "\nLOCAL SERVER CONNECTED\n";
		$Connection_Flag=1;
	}
	else
	{
		print "\nFAILED\n";
		if($Recon_Flag<=3)
		{
			$Recon_Flag++;
			goto Reconnect;
		}	
	}

	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 90000;
	
	return $dbh;
}



sub DB_Insert()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);
	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		
		open(ERR,">>Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&DbConnection();
	}
}


sub MissingretrieveInput()
{
	my $dbh 			= shift;
	
	my $query="select ID, Town_URL from Contacts_ScrapeLog where Town_URL in ('http://architects-register.org.uk/towns/Herefordshire/Ross+on+Wye/5','http://architects-register.org.uk/towns/Devon/Axminster/3','http://architects-register.org.uk/towns/Cambridgeshire/Soham','http://architects-register.org.uk/towns/Kent/Kings+Hill/2','http://architects-register.org.uk/towns/Cheshire/Croft/4','http://architects-register.org.uk/towns/Fife/North+Queensferry','http://architects-register.org.uk/towns/Moray/Keith/5','http://architects-register.org.uk/towns/Hampshire/Eastleigh/3','http://architects-register.org.uk/towns/Kent/Dartford/3','http://architects-register.org.uk/towns/Carmarthenshire/Carmarthen/4','http://architects-register.org.uk/towns/Staffordshire/Wheaton+Aston','http://architects-register.org.uk/towns/Derbyshire/Ilkeston/5','http://architects-register.org.uk/towns/Devon/Bere+Alston/4','http://architects-register.org.uk/towns/Gloucestershire/Lechlade/5','http://architects-register.org.uk/towns/Northumberland/Berwick-Upon-Tweed/2','http://architects-register.org.uk/towns/North+Yorkshire/Monk+Fryston')";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my (@townURL,@ID);	
	while(my @record = $sth->fetchrow)
	{
		push(@ID, $record[0]);
		push(@townURL, $record[1]);
	}
	$sth->finish();
	
	return (\@ID,\@townURL);
}

sub retrieveInput()
{
	my $dbh 			= shift;
	my $from 			= shift;
	my $to 				= shift;
		
	# my $query="select ID,Town_URL from Contacts_ScrapeLog where (ID in ('186133','186134','186938','186952','186953','186668','186669','185237','185321','185322','184001','184161','184162','183425','183426','183572','183573','182372','182391','182392','191648','191649','191763','191764','192208','192222','192223','193290','193291','193328','193329','194642','194643','194713','194714','189178','189269','189270','189271','190642','190643','190758','190759')) and ID not in (select ID from ContactsScrape_Detail_URLLog where CONVERT(varchar,CREATED_DATE,111) ='2018/12/05') and CONVERT(varchar,CREATED_DATE,111) ='2018/12/05'";
	
	# my $query="select ID,Town_URL from Contacts_ScrapeLog where (ID between '220884' and '221345') and ID not in (select ID from ContactsScrape_Detail_URLLog where CONVERT(varchar,CREATED_DATE,111) ='2019/01/09') and CONVERT(varchar,CREATED_DATE,111) ='2019/01/09'";
	
	my $query="select ID,Town_URL from Contacts_ScrapeLog where CONVERT(varchar,CREATED_DATE,111) = convert(varchar,GETDATE(),111) and (ID between '$from' and '$to') order by ID Asc";
	# my $query="select ID,Town_URL from Contacts_ScrapeLog where CONVERT(varchar,CREATED_DATE,111) = '2019/07/09' and (ID between '$from' and '$to') order by ID Asc";
	print "Qery==>$query\n";
	
	
	
	# my $query="select ID,Town_URL from Contacts_ScrapeLog where ID not in (select ID from ContactsScrape_Detail_URLLog)";
	
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my (@townURL,@ID);	
	while(my @record = $sth->fetchrow)
	{
		push(@ID, $record[0]);
		push(@townURL, $record[1]);
	}
	$sth->finish();
	
	return (\@ID,\@townURL);
}

sub retrieveDetailURLInput()
{
	my $dbh 			= shift;
	my $from 			= shift;
	my $to 				= shift;
		
	my $query="select detail_urls from contact_detail_unique_urls where ID between '$from' and '$to' order by ID ASC";

=pod	
	my $query="with cte as
				(
					select ID, detail_urls, row_number() over (PARTITION by detail_urls order by detail_urls ) as row_numbers from ContactsScrape_Detail_URLLog
				)
				select * from cte where row_numbers = 1";
=cut	
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my (@detailURL);	
	while(my @record = $sth->fetchrow)
	{
		push(@detailURL, $record[0]);
	}
	$sth->finish();
	
	return (\@detailURL);
}

sub MechGetMethod()
{
	my $getURL=shift;
	my $tabName=shift;

	eval
	{
		print "Get::$getURL\n";
		$Mech->get( $getURL );
	};
	my ($responseContent,$pingStatus);
	$responseContent = $Mech->content;	
	$pingStatus = $Mech->status;	
	open(PP,">responseContent.html");
	print PP "$responseContent\n";
	close(PP);
	
	print "pingStatus::$pingStatus\n";
	# <STDIN>;
	
	if($pingStatus!~m/^20/is)
	{	
		sleep(5);
		($responseContent,$pingStatus) = &getMethodLWP($getURL);
		if($pingStatus!~m/^20/is) 
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);
		}
		
		if(($getURL=~m/\/towns\//is) && ($responseContent!~m/<th[^>]*?>\s*Distance\s*<\/th>/is))
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);			
		}
		elsif(($getURL=~m/\/regions\//is) && ($responseContent!~m/<table[^>]*?Towns\"/is))
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);			
		}
		elsif(($getURL=~m/\/architect\//is) && ($responseContent!~m/<span[^>]*?>\s*Registration\s*Number\s*<\/span>/is))
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);					
		}
	}
	else
	{
		if(($getURL=~m/\/towns\//is) && ($responseContent!~m/<th[^>]*?>\s*Distance\s*<\/th>/is))
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);			
		}
		elsif(($getURL=~m/\/regions\//is) && ($responseContent!~m/<table[^>]*?Towns\"/is))
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);			
		}
		elsif(($getURL=~m/\/architect\//is) && ($responseContent!~m/<span[^>]*?>\s*Registration\s*Number\s*<\/span>/is))
		{
			open(PP,">>$tabName.txt");
			print PP "$getURL\t$pingStatus\n";
			close(PP);					
		}
	}
	
	return ($pingStatus, $responseContent);	
}

###########################################################
#Function : getMethodLWP()
#Desc  	  : LWP::UserAgent objects can be used to dispatch web requests and returns a HTTP::Response object and content etc.
#Date	  : 19-09-2018
###########################################################
sub getMethodLWP()
{
	my $documentURL=shift;
	
	my $rerunCount=0;
	my ($redirectURL,$content);
	
	Home:
	my $req = HTTP::Request->new(GET=>$documentURL);
	$req->header("Accept-Language"=> "en-US,en;q=0.5");
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
	$req->header("content-type"=> "text/html; charset=utf-8");
	$req->header("accept-encoding"=> "gzip, deflate, br");
	$req->header("user-agent"=> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36");
		
	my $res = $ua->request($req);
		
	my $code=$res->code;
	my $status_line=$res->status_line;
	
	print "ResponseCode:: $code\n";
	
	if($code=~m/^20/is)
	{
		$content = $res->content;
	}
	elsif($code=~m/^30/is)
	{
		my $loc=$res->header("location");
		if( $rerunCount <= 3 )
		{
			$rerunCount++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$documentURL);
				my $u2=$u1->abs;
				$documentURL=$u2;
				$redirectURL=$u2;
			}
			else
			{
				$documentURL=$loc;
				$redirectURL=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if( $rerunCount <= 1 )
		{
			$rerunCount++;
			
			sleep 1;
			goto Home;
		}
	}
	
	return ($content,$code);
}

sub MechClickMethod()
{
	my $getURL=shift;
	my $councilName=shift;
	my $gradeValue=shift;
		
	eval
	{
		$Mech->get( $getURL );
		
		my $gradeName = $councilDetailsFile->{$councilName}->{'GRADE_NAME'};
		my $countriesName = $councilDetailsFile->{$councilName}->{'COUNTRIES_NAME'};
		my $countriesValue = $councilDetailsFile->{$councilName}->{'COUNTRIES_VALUE'};

		$Mech->form_number($councilDetailsFile->{$councilName}->{'FORM_NUMBER'});
		$Mech->set_fields( $gradeName => $gradeValue );
		$Mech->set_fields( $countriesName => $countriesValue );
		$Mech->click();
	};
	
	my $responseContent = $Mech->content;	
	my $pingStatus = $Mech->status;	
	
	return ($pingStatus, $responseContent);	
}


sub Redis_Connection()
{
	my $redis;
	
	eval
	{
		$redis = RedisDB->new(host => '127.0.0.1', port => 6379, database => 9);
	};
	
	if($@)
	{
		print "Redis not Connected\n";
		
		eval
		{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = RedisDB->new(host => '127.0.0.1', port => 6379, database => 9);
		};
	}
	
	return $redis;
}


sub clean
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\&\#39\;/\'/igs;
	$Data=~s/^\s*//igs;
	$Data=~s/\s*$//igs;
	
	return ($Data);
}

1;