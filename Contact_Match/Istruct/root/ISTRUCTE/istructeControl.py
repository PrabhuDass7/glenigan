# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 13:14:57 2018

@author: parielavarasan
"""

import os, re, sys
import requests
import configparser

folder_path = 'C:/Glenigan/Merit/Projects/hopper/'

proxies = {
    'http': 'http://172.27.137.199:3128',
    'https': 'http://172.27.137.199:3128',
}

# Create the session and set the proxies.
s = requests.Session()
s.proxies = proxies


#############################################################Main function##############################################

try:
    # Read settings from configuration file.
    config = configparser.ConfigParser()
    config.read(folder_path+'etc/Site_Details.ini')
       
    # read values from a section
    istructe_url = config.get('ISTRUCTE', 'URL')
    #print istructe_url
    
    # Make the HTTP request through the session.
    r = s.get(istructe_url)
    content=r.content
    
    # with open(folder_path+"output/istructe.html", 'wb') as fd:
		# fd.write(content)
    # raw_input()
	   
        
    paginationContent=re.findall(r'<select[^>]*?MembersSearchForm\$ddlMembershipGrade[^>]*?>\s*([\w\W]*?)\s*<\/select>', str(content), re.IGNORECASE)  
    paginationContent = re.sub(r'$', "\n", str(paginationContent[0]))
    # print paginationContent
    gradeContent=re.findall("<option[^>]*?value=\"(\d+)\">\s*([^<]*?)\s*\n", str(paginationContent), re.IGNORECASE)
    # print gradeContent
    # sys.exit(0)   
    councilName = 'ISTRUCTE'
    
    for gradeMatch in gradeContent:
        gradeValue = gradeMatch[0]
        gradeName = gradeMatch[1]     
        gradeName = re.sub(r'(\\n|\s+)$', "", str(gradeName))	
        print("Now processing GradeName:", str(gradeValue))
        print("Now processing GradeValue:", str(gradeName))
        
        os.system("python "+folder_path+"root/"+str(councilName)+"/istructeList.py "+str(gradeValue)+" "+str(gradeName)+" "+str(councilName)+"")
        
	
except Exception as e:
	print(e)