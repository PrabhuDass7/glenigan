use strict;
use WWW::Mechanize;
use URI::URL;
use DBI;
use DBD::ODBC;
use Time::Piece;
use IO::Socket::SSL;

# Establish connection with DB server
my $dbh = &DbConnection();

my $mech = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
			});
			

### Proxy settings ###

BEGIN {
 $ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
 $ENV{HTTPS_DEBUG} = 1;  #Add debug output
}

my $homeURL = "https://planning.cumbria.gov.uk/Search/Standard?searchType=Decided&days=28";


$mech->get($homeURL);

my $acceptURL = "https://planning.cumbria.gov.uk/Disclaimer?returnUrl=%2FSearch%2FStandard%3FsearchType%3DDecided%26days%3D28";
$mech->post( $acceptURL, Content => "returnUrl=%2FSearch%2FStandard%3FsearchType%3DDecided%26days%3D28");
$mech->add_header( 'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3');
$mech->add_header( 'Accept-Encoding' => 'gzip, deflate');
$mech->add_header( 'Accept-Language' => 'en-US,en;q=0.9');
$mech->add_header( 'Referer' => $acceptURL);
$mech->add_header( 'Host' => 'planning.cumbria.gov.uk');
$mech->add_header( 'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
$mech->get($homeURL);
my $appPageContent = $mech->content;
open(DC,">appPageContent.html");
print DC $appPageContent;
close DC;
exit;

my $appFullURL;
my @appURL;
while($appPageContent=~m/<tr>\s*<td[^>]*?>\s*<a\s*href=\"([^\"]*?)\"\s*>\s*([^<]*?)\s*<\/a>\s*<\/td>/igs)
{
	my $applicationURL = $1;
	my $application = $2;
	
	if($applicationURL!~m/^http/is)
	{
		my $u1=URI::URL->new($applicationURL,$homeURL);
		my $u2=$u1->abs;
		$appFullURL = $u2;
	}
	else
	{
		$appFullURL = $applicationURL;
	}
	push(@appURL, $appFullURL)
}

my $time = Time::Piece->new;
my $Schedule_Date= $time->strftime('%Y/%m/%d %H:%M');

my $insertQuery="insert into IMPORT_NON_PUBLIC_PLANNING_DECISION_AUTOMATION_STAGING (Application, ProPosal, Decision_Status, Date_Decision_Made, Council_code, Page_URL, Source, Application_Status,Imported_Date) values ";
my $count=1;
my $bulkValuesForQuery='';
my $applicationCount=0;
foreach my $Page_url (@appURL)
{
	# print "$count==>$Page_url\n";
	$count++;
	
	my $App_Page_Content;
	eval{
		$mech->get($Page_url);
		$App_Page_Content = $mech->content;
	};
	
	
	my $Proposal					= &clean($1) if($App_Page_Content=~m/>\s*Summary\s*<\/h3>\s*<p>\s*([^<]*?)\s*<\/p>/is);	
	my $Application					= &clean($1) if($App_Page_Content=~m/<h2>\s*Application\s+([^<]*?)\s*<\/h2>/is);	
	my $Application_Status			= &clean($1) if($App_Page_Content=~m/>\s*Decision\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
	my $Decision					= &clean($1) if($App_Page_Content=~m/>\s*Decision\s*<\/b>\s*<\/td>\s*<td>\s*([^<]*?)\s*<\/td>/is);
	my $Decision_Date				= &clean($1) if($App_Page_Content=~m/<li>\s*Decision\s*made\s*on\s*<b>([^<]*?)<\/b>\s*<\/li>/is);
	my $Application_Link=$Page_url;
	$Proposal=~s/\'/\'\'/gsi;	
	$Application_Status=~s/\'/\'\'/gsi;	
	$Decision=~s/\'/\'\'/gsi;	
	
	my $Source = "GCS001";			
	my $Source_With_Time=$Source."_".$Schedule_Date."-perl";
	my $COUNCIL_NAME = "Cumbria County Council";
	my $Council_Code = "606";
	
	print "ApplicationNo==>$Application\n";
	
	
	next if($Application eq "");
	
	
	my $joinValues = "(\'$Application\', \'$Proposal\', \'$Decision\', \'$Decision_Date\', \'$Council_Code\', \'$Application_Link\', \'$Source_With_Time\', \'$Application_Status\',\'$Schedule_Date\'),";
	
	
	if($applicationCount == 0)
	{
		$bulkValuesForQuery = $insertQuery.$joinValues;
		$applicationCount++;
	}
	elsif($applicationCount == 10)
	{
		$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
		
		$bulkValuesForQuery=~s/\,\s*$//igs;
		&DB_Insert($dbh,$bulkValuesForQuery);
		$applicationCount = 0;
		$bulkValuesForQuery="";
	}
	else
	{
		$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
		$applicationCount++;
	}
			
	undef $Application;  undef $Proposal; undef $Decision_Date; undef $Application_Status; undef $Decision;
}

$bulkValuesForQuery=~s/\,\s*$//igs;
if($bulkValuesForQuery ne "")
{
	&DB_Insert($dbh,$bulkValuesForQuery);
}




###### Insert DB Query ####
sub DB_Insert()
{
    my $dbh     = shift;
    my $Query    = shift;

    my $sth = $dbh->prepare($Query);
   
    if($sth->execute())
    {
        print "Executed\n";
    }
    else
    {
        print "QUERY:: $Query\n";
        open(ERR,">>Failed_Query.txt");
        print ERR $Query."\n";
        close ERR;
        $dbh=&DbConnection();
    }
}


sub clean()
{
	my $Data=shift;
	my $tab=chr(9);
	my $line_feed=chr(10);
	my $c_return=chr(13);
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/&nbsp;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/,\s*(?:,\s*)+/, /igs;
	$Data=~s/^\s*,\s*|\s*,\s*$//igs;
	$Data=~s/\'/\'\'/igs;
	$Data=~s/\&\#039\;/'/igs;
	$Data=~s/$tab/ /igs;
	$Data=~s/$line_feed/, /igs;
	$Data=~s/$c_return/ /igs;
	$Data=~s/\\r\\n/ /igs;
	$Data=~s/\s\s+/ /igs;
	
	return($Data);
}

###### DB Connection ####
sub DbConnection()
{
    if($dbh = DBI->connect("dbi:Sybase:server=10.101.53.25;database=Glenigan", 'User2', 'Merit456'))
    {
        $dbh-> {'LongTruncOk'}            =    1;
        $dbh-> {'LongReadLen'}            =    90000;
        print "\n------->Connected database successfully---->\n";
    }
    return $dbh;
}