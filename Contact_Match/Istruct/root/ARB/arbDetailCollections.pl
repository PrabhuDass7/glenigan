#!/usr/bin/perl
use strict;
use URI::URL;
use URI::Escape;
use Config::Tiny;
# use RedisDB;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw(make_path);
use Text::CSV;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

# Establish connection with DB server
my $dbHost = &Hopper::DbConnection();

my $councilName = "ARB";
my $from= $ARGV[0];
my $to = $ARGV[1];

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();


####
# Create new object of class Text::CSV here
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";


####
# Create redis connection
####
# my $redis = &Hopper::Redis_Connection();


####
# Establish connection with DB server
####
my $dbh = &Hopper::DbConnection();


####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


####
# Get detail Url input from log table
####
my @inputURL;
my $inputURL = &Hopper::retrieveDetailURLInput($dbHost, $from, $to);
@inputURL=@{$inputURL};


my $applicationCount = 0;

my $Location = $outputDirectory."/".$toDay;
unless ( -d $Location )
{
	make_path($Location);
}


my $insert_query="insert into ContactsHopper_Source (Member_Number, Contact_Name, Contact_Email, Company_Name, Company_Address, Company_Postcode, Company_Phone, Company_Website, Source_Name) values ";
my $bulkValuesForQuery='';
my $filename = "$Location/ARBOutputDetails11.csv";
open my $fh, ">:encoding(utf8)", $filename or die "failed to create $filename: $!";
my(@heading) = ("Registration Number", "Members Heading", "Company Name", "Address", "Telephone Number", "Email", "Company Website Address", "Detail URL");
$csv->print($fh, \@heading);    # Array ref!
my $count = 0;
foreach my $url(@inputURL)
{
	# print "URL==>$url\n"; <STDIN>;
	my ($pingStatus, $PageContent);
	($pingStatus, $PageContent)  = &Hopper::MechGetMethod($url);
	
	if($PageContent!~m/<span[^>]*?>\s*Registration\s*Number\s*<\/span>\s*(?:<[^>]*?>)?\s*<strong>[^<]*?<\/strong>/is)
	{
		open(LL,">>Details_needtoCheck.txt");
		print LL "$url\n";
		close(LL);
	}
	else
	{
		my ($contactDetails, $joinValues) = &getContactDetails($PageContent,$url);	
		my @datarow = split('\t', $contactDetails);
		$csv->print($fh, \@datarow); 
		if($applicationCount == 0)
		{
			$bulkValuesForQuery = $insert_query.$joinValues;
			$applicationCount++;
		}
		elsif($applicationCount == 999)
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			
			if($bulkValuesForQuery!~m/values\s*$/is)
			{
				$bulkValuesForQuery=~s/\,\s*$//igs;
				&Hopper::DB_Insert($dbh,$bulkValuesForQuery);
				$applicationCount = 0;
				$bulkValuesForQuery="";
			}
		}
		else
		{
			$bulkValuesForQuery = $bulkValuesForQuery.$joinValues;
			$applicationCount++;
		}
		
	}
	
}
close $fh or die "failed to close $filename: $!";
print "Total Application Number Count::$applicationCount\n";


$bulkValuesForQuery=~s/\,\s*$//igs;
print "insert_query::$bulkValuesForQuery\n";

if($bulkValuesForQuery!~m/values\s*$/is)
{
	&Hopper::DB_Insert($dbh,$bulkValuesForQuery);
}

sub getContactDetails
{
	my $contactCnt = shift;
	my $detailURL = shift;
	
	my ($conDetails, $insertquery);
	if($contactCnt=~m/<div\s*class=\"list_registrants\s*generalcontent\"[^>]*?>\s*(?:<[^>]*?>)?\s*<table[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
	{
		my $tableCnt = $1;
		
		my ($memHeading, $companyName, $companyAddress1, $companyAddressPostCode, $companyAddress, $regNumber, $teleNumber, $cntEmail, $cntWebsite);
		
		$memHeading = &Hopper::clean($1) if($tableCnt=~m/<h1>\s*([^<]*?)\s*<\/h1>/is);
		$companyName = &Hopper::clean($1) if($tableCnt=~m/>\s*Address\s*<\/span>\s*<div[^>]*?>\s*<strong>\s*([\w\W]*?)\s*<\/strong>/is);
		$companyAddress = &Hopper::clean($1) if($tableCnt=~m/>\s*Address\s*<\/span>\s*<div[^>]*?>\s*<strong>\s*[\w\W]*?\s*<\/strong>\s*([\w\W]*?)\s*<\/div>/is);
		if($tableCnt=~m/>\s*Address\s*<\/span>\s*<div[^>]*?>\s*<strong>\s*[\w\W]*?\s*<\/strong>\s*([\w\W]*?)\s*<br[^>]*?>\s*([A-Z0-9\s]+)\s*<br[^>]*?>\s*<\/div>/is)
		{
			$companyAddress1 = &Hopper::clean($1);
			$companyAddressPostCode = &Hopper::clean($2);
		}
		$regNumber = &Hopper::clean($1) if($tableCnt=~m/>\s*Registration\s*Number\s*<\/span>\s*(?:<div[^>]*?>)?\s*<strong>\s*([\w\W]*?)\s*<\/strong>/is);
		$teleNumber = &Hopper::clean($1) if($tableCnt=~m/<li[^>]*?>\s*<img[^>]*?>\s*Tel\:\s*([^<]*?)\s*(?:<[^<]*?>)?\s*<\/li>/is);
		$cntEmail = &Hopper::clean($1) if($tableCnt=~m/<a\s*href=mailto\:([^>]*?)>\s*Send\s*an\s*Email\s*</is);
		$cntWebsite = &Hopper::clean($1) if($tableCnt=~m/<a\s*href=\'([^\']*?)\'[^>]*?>\s*Visit\s*Website\s*</is);
	
		print "Registration Number::$regNumber is Processing..\n";
		
		$conDetails = "$regNumber\t$memHeading\t$companyName\t$companyAddress\t$teleNumber\t$cntEmail\t$cntWebsite\t$detailURL";
		
		$insertquery = "(\'$regNumber\', \'$memHeading\', \'$cntEmail\', \'$companyName\', \'$companyAddress1\', \'$companyAddressPostCode\', \'$teleNumber\', \'$cntWebsite\', \'$councilName\'),";
		
		undef $regNumber;  undef $memHeading;  undef $cntEmail;  undef $companyAddress1;  undef $companyAddress;  undef $companyName;  undef $teleNumber; undef $companyAddressPostCode; undef $cntWebsite;
	}
	
	return ($conDetails, $insertquery);
}