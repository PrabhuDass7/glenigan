#!/usr/bin/perl
use strict;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;
use URI::Escape;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');

####
# Declare and load local directories
####
my $ConfigDirectory = ($basePath.'/etc/');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $scriptPath = ($basePath.'/root');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


my ($pingStatus, $paginationContent, $viewState, $viewStateGenerator, $eventValidation, $councilName);

$councilName = 'ARB';

($pingStatus, $paginationContent) = &Hopper::MechGetMethod($councilDetailsFile->{$councilName}->{'URL'});

$viewState = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATE_REGEX'}/is);
$viewStateGenerator = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATEGENERATOR_REGEX'}/is);
$eventValidation = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'EVENTVALIDATION_REGEX'}/is);

my $rContent;
if($paginationContent=~m/<div\s*class=\"alphabet_list_content\">\s*([\w\W]*?)\s*<\/div>/is)
{
	$rContent = $1;
}

my $Location = $outputDirectory."/".$toDay;

# while($rContent=~m/<input\s*type=\"submit\"\s*name=\"(ctl00\$mainContent\$Button8)\"\s*value=\"(J)\"[^>]*?>/gsi)
while($rContent=~m/$councilDetailsFile->{$councilName}->{'ALPHABET_REGEX'}/gsi)
{
	my $name = uri_escape($1);
	my $value = $2;
	
	
	print "Now processing region:\t$name\n";
	print "Now processing region:\t$value\n";
	
		
	my $PostCnt = $councilDetailsFile->{$councilName}->{'SEARCH_POSTCONTENT'};
		
	$PostCnt=~s/<VIEWSTATE>/$viewState/s;
	$PostCnt=~s/<VIEWSTATEGENERATOR>/$viewStateGenerator/s;
	$PostCnt=~s/<EVENTVALIDATION>/$eventValidation/s;
	$PostCnt=~s/<NAME>/$name/s;
	$PostCnt=~s/<VALUE>/$value/s;
	
	$PostCnt=~s/\&/\#HOPPER\#/gs;
	
	# Initializing the List script for each arguments	
	# if($value=~m/^J$/is)
	# {
		# print "value==>$value\n";
		system("start perl $scriptPath/ARB/arbListURL.pl $PostCnt $value $councilName &");
	# }
	
	# sleep (60);
	
}