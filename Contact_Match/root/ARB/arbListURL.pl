#!/usr/bin/perl
use strict;
use URI::URL;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use URI::Escape;
use URI::Encode;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');
my $scriptPath = ($basePath.'/root');

require ($libDirectory.'/Hopper.pm'); # Private Module

# Establish connection with DB server
my $dbHost = &Hopper::DbConnection();

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Create redis connection
####
# my $redis = &Hopper::Redis_Connection();


####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


my $alphabet = $ARGV[0];
# my $tabName = $ARGV[1];

# my $councilName = $ARGV[2];

my $tabName = $alphabet;
my $councilName = "ARB";

my @fullCount;

# print "\"$tabName\" started processing..\n";
# <STDIN>;

if($alphabet eq "")
{
	print "ARB arugument not found!. Please check ARB control script again.\n";
	exit;
}
else
{
	print "ARB arugument found.\n";
}
	
# $postContent =~s/\#HOPPER\#/\&/gsi;




my $sub_URL="https://architects-register.org.uk/registrant/list?";
	
my $sub_URL = $councilDetailsFile->{$councilName}->{'ARB_LIST_URL'};
	
$sub_URL=~s/<ALPHABET>/$alphabet/igs;
$sub_URL=~s/<PAGE>/0/igs;

# my $Total_Page_no=$1 if($paginationContent=~m/<[^>]*?>\s*Page[^>]*?of\s*([\d]+)\s*<[^<]*?>/is);
print "sub_URL::: $sub_URL\n";<STDIN>;


my ($responseContent, $pingStatus);
# ($responseContent, $pingStatus) = &Hopper::MechPostMethod($councilDetailsFile->{$councilName}->{'URL'}, $postContent);
($pingStatus, $responseContent) = &Hopper::MechGetMethod($sub_URL);


open JJ, ">arb_URL_Home.html";
print JJ "$responseContent";
close JJ;

print "arb_URL_Home:::*****\n";<STDIN>;

my $insert_query;


my (@locationURLs, @townURLs, @detailURLs);	
if($responseContent=~m/<table\s*id=\"[^\"]*?AllCountr?ies\"[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
{
	my $urlContent = $1;
	print "Hiiiii\n";<STDIN>;
	if($urlContent=~m/<a[^>]*?href=(?:\"|\')(?:\.?\.?)?([^\"\']*?)(?:\"|\')[^>]*?>\s*([^<]*?)\s*<\/a>/si)
	{
		while($urlContent=~m/<a[^>]*?href=(?:\"|\')(?:\.?\.?)?([^\"\']*?)(?:\"|\')[^>]*?>\s*([^<]*?)\s*<\/a>/gsi)
		{
			my $RegionlocationURL = $1;
			my $RegionlocationName = $2;
			$RegionlocationName=~s/\'/\'\'/gsi;
			
			if($RegionlocationName=~m/^Channel\s*Islands$/is)
			{
				if($urlContent=~m/>\s*([^<]*?)\s*<\/b>\s*\(\s*see\s*<a[^>]*?href=\'(?:\.?\.?)?([^\']*?)\'[^>]*?>\s*/is)
				{	
					$RegionlocationName = $1;
				}
			}
			
			print "Region Started:::$RegionlocationName\n";<STDIN>;
			
			$RegionlocationURL=~s/\.\.//si;
			
			if(($RegionlocationURL!~m/^http/is) && ($RegionlocationURL ne ""))
			{
				$RegionlocationURL = URI::URL->new($RegionlocationURL)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );
			}
			$RegionlocationURL=~s/\'/\'\'/gsi;
			
			print "RegionlocationURL==>$RegionlocationURL\n"; <STDIN>;
			
			my ($pingStatus, $locationPageContent);
			($pingStatus, $locationPageContent)  = &Hopper::MechGetMethod($RegionlocationURL,$tabName);
			
			if($locationPageContent=~m/<table\s*id=\"[^\"]*?Towns\"[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
			{
				my $townsContent = $1;
				
				my $tempQuery;
				while($townsContent=~m/<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*(?:\(?\d+\))?\s*<\/a>/gsi)
				{
					my $townURL = $1;
					my $townName = $2;
					
					print "Location\/Town Started:::$townName\n";
					
					$townURL=~s/\.\.//si;
					$townName=~s/\'/\'\'/gsi;
					
					if(($townURL!~m/^http/is) && ($townURL ne ""))
					{
						$townURL = URI::URL->new($townURL)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );
					}
					
					$townURL=~s/\'/\'\'/gsi;
					
					# print "townURL==>$townURL\n";
					
					
					my $townGridURL = $townURL;
					my $kmQuery;
					for(my $i = 2; $i <= 5; $i++)
					{
						$townGridURL=$townURL."/".$i;
										
						# print "townURL==>$townGridURL\n";
						
						$kmQuery = "insert into Contacts_ScrapeLog (Region, Region_URL, Town, Town_URL, Created_Date) values (\'$RegionlocationName\', \'$RegionlocationURL\', \'$townName\', \'$townGridURL\', GETDATE())\;\n";
						$tempQuery .= $kmQuery;
					}
					
					
					my $query = "insert into Contacts_ScrapeLog (Region, Region_URL, Town, Town_URL, Created_Date) values (\'$RegionlocationName\', \'$RegionlocationURL\', \'$townName\', \'$townURL\', GETDATE())\;\n";
					$tempQuery .= $query;
															
					# my ($townpingStatus, $townPageContent);
					# ($townpingStatus, $townPageContent)  = &Hopper::MechGetMethod($townURL);
					
					
					# my ($detailURL) = &collectDetailURL($townPageContent, $activeTownURL);
					# @detailURLs = @{$detailURL};
				}
				
				$insert_query.= $tempQuery;
			}
			elsif($locationPageContent=~m/<table\s*id=\"[^\"]*?Boroughs\"[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
			{
				my $BoroughsContent = $1;
				if($BoroughsContent=~m/<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*(?:\(?\d+\))?\s*<\/a>/is)
				{
					my $tempQuery;
					while($BoroughsContent=~m/<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*(?:\(?\d+\))?\s*<\/a>/gsi)
					{
						my $BoroughstownURL = $1;
						my $BoroughstownName = $2;
						
						$BoroughstownURL=~s/\.\.//si;
						
						my $regionBoroughsName = "$RegionlocationName\-$BoroughstownName";
						
						print "Boroughs Started:::$RegionlocationName\-$BoroughstownName\n";
						
						if(($BoroughstownURL!~m/^http/is) && ($BoroughstownURL ne ""))
						{
							$BoroughstownURL = URI::URL->new($BoroughstownURL)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );
						}
						
						
						my ($pingStatus, $BoroughsPageContent);
						($pingStatus, $BoroughsPageContent)  = &Hopper::MechGetMethod($BoroughstownURL,$tabName);
						
						if($BoroughsPageContent=~m/<table[^>]*?>\s*([\w\W]*?)\s*<\/table>\s*<\/td>\s*<\/tr>\s*<\/table>/is)
						{
							my $BoroughstownsContent = $1;
							
							my $tempBoroughsQuery;
							while($BoroughstownsContent=~m/<a[^>]*?href=\"(?:\.?\.?)?([^\"]*?)\"[^>]*?>\s*([^<]*?)\s*(?:\(?\d+\))?\s*<\/a>/gsi)
							{
								my $BoroughstownsURL = $1;
								my $BoroughstownsName = $2;
								
								print "Boroughs Town Started:::$BoroughstownsName\n";
								
								$BoroughstownsURL=~s/\.\.//si;
								
								if(($BoroughstownsURL!~m/^http/is) && ($BoroughstownsURL ne ""))
								{
									$BoroughstownsURL = URI::URL->new($BoroughstownsURL)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );
								}
								$BoroughstownsURL=~s/\'/\'\'/si;
								$BoroughstownsName=~s/\'/\'\'/si;
								
								my $query = "insert into Contacts_ScrapeLog (Region, Region_URL, Town, Town_URL, Created_Date) values (\'$regionBoroughsName\', \'$BoroughstownURL\', \'$BoroughstownsName\', \'$BoroughstownsURL\', GETDATE())\;\n";
								$tempBoroughsQuery .= $query;
								
							}
							$insert_query.= $tempBoroughsQuery;
						}
						else
						{
							$BoroughstownURL=~s/\'/\'\'/si;
							$BoroughstownName=~s/\'/\'\'/si;
							
							my $query = "insert into Contacts_ScrapeLog (Region, Region_URL, Town, Town_URL, Created_Date) values (\'$RegionlocationName\', \'$RegionlocationURL\', \'$BoroughstownName\', \'$BoroughstownURL\', GETDATE())\;\n";
							$insert_query.= $query;
						}
					}
				}
				else
				{
					my $query = "insert into Contacts_ScrapeLog (Region, Region_URL, Town, Town_URL, Created_Date) values (\'$RegionlocationName\', \'$RegionlocationURL\', \'$RegionlocationName\', \'$RegionlocationURL\', GETDATE())\;\n";
					$insert_query .= $query;					
				}
			}
			else
			{
				my $query = "insert into Contacts_ScrapeLog (Region, Region_URL, Town, Town_URL, Created_Date) values (\'$RegionlocationName\', \'$RegionlocationURL\', \'$RegionlocationName\', \'$RegionlocationURL\', GETDATE())\;\n";
				$insert_query .= $query;
			}
		}
	}
	
	# print "$insert_query"; <STDIN>;

	$insert_query=~s/\s*$//igs;
	if($insert_query!~m/^\s*$/is)
	{
		# open(PP,">>Query.txt");
		# print PP "\n$insert_query";
		# close(PP);
		# &Hopper::DB_Insert($dbHost,$insert_query);
	}
	# system("start perl $scriptPath/ARB/arbDetailURL.pl &");		
}

