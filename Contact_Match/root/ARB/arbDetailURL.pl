#!/usr/bin/perl
use strict;
use URI::URL;
use URI::Escape;
use Config::Tiny;
# use RedisDB;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw(make_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
# my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

# Establish connection with DB server
my $dbHost = &Hopper::DbConnection("Windows");

my $councilName = "ARB";
# my $from = $ARGV[0];
# my $to = $ARGV[1];

my $alphabet = $ARGV[0];

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();


####
# Create redis connection
####
# my $redis = &Hopper::Redis_Connection();


####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');

#--------------------------------------------------
	
my $sub_URL = $councilDetailsFile->{$councilName}->{'ARB_LIST_URL'};
# my $sub_URL_temp=$sub_URL;


print ":*****\n";

my $Total_Page_no=1;
my $ID=1000;

for (my $page_no=0; $page_no < $Total_Page_no; $page_no++)
{
	# print "PGNO:: $page_no\n";
	# print "Total_Page_no:::$Total_Page_no*****\n";<STDIN>;
	
	goto nxt_apl if ($page_no == 2);
	
	my $sub_URL_temp=$sub_URL;
	$sub_URL_temp=~s/<ALPHABET>/$alphabet/igs;
	$sub_URL_temp=~s/<PAGE>/$page_no/igs;
	
	my ($pingStatus, $responseContent) = &Hopper::MechGetMethod($sub_URL_temp);

	open JJ, ">arb_Detail_URL_Home.html";
	print JJ "$responseContent";
	close JJ;
	
	if($page_no == 0)
	{
		$Total_Page_no=$1 if($responseContent=~m/<[^>]*?>\s*Page[^>]*?of\s*([\d]+)\s*<[^<]*?>/is);
	}
	
	print "AlPhabets:: $alphabet *** \n PGNO:: $page_no *** \n Total_Page_no:::$Total_Page_no*****\n";
	# print "arb_URL_Home:::*****\n";<STDIN>;
	
	my @Detail_URL=$responseContent=~m/<div\s*class\=\"media\-body\">[\w\W]*?<a\s*href\=\"([^>]*?)\"[^>]*?class\=\"btn\s*btn\-primary\">\s*View\s*<\/a>/igs;
	
	goto nxt if($#Detail_URL == -1);
	
	my $tempQuery;

	my $query = "INSERT INTO ContactsScrape_Detail_URLLog_Dev (ID,detail_urls,created_date) VALUES ";

	my $count = 0;
	
	foreach my $url(@Detail_URL)
	{
		chomp($url);
		
		$url = URI::URL->new($url)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );

		print "URL:: $url\n";
		
		# my $ID="";
		$ID++;
		my $queryValue = "(\'$ID\', \'$url\', GETDATE()), ";
		$tempQuery .= $queryValue;
		$count++;
		
		if($count=~m/^999$/is)
		{
			$tempQuery=~s/\,\s*$//gsi;

			$tempQuery = $query.$tempQuery;
		
			
			# print "query==>$tempQuery\n";
			
			if($tempQuery!~m/values\s*$/is)
			{
				&Hopper::DB_Insert($dbHost,$tempQuery);
				undef $tempQuery;
			}
			
			$count = 0;
		}
		
	}

	$tempQuery=~s/\,\s*$//gsi;
			
	$query .= $tempQuery;
	# print "Final query==>$query\n";<STDIN>;
	
	if($query!~m/values\s*$/is)
	{
		&Hopper::DB_Insert($dbHost,$query);
		undef $tempQuery;
	}
	nxt:
}

nxt_apl:

=e
#------------------------------------------------------
my $Location = $outputDirectory."/".$toDay;
print "Location==>$Location\n";
unless ( -d $Location )
{
	make_path($Location);
}



####
# Get input from format table
####
my (@inputID,@inputURL);
my ($inputID,$inputURL) = &Hopper::retrieveInput($dbHost, $from, $to);


my $reccount = @{$inputID};

for(my $reccnt = 0; $reccnt < $reccount; $reccnt++ )
{	
	my $ID 		= @$inputID[$reccnt];
	my $inputURL 	= @$inputURL[$reccnt];
	
	print "$ID==>$inputURL\n"; 
	# <STDIN>;
	
	my ($pingStatus, $PageContent);
	($pingStatus, $PageContent)  = &Hopper::MechGetMethod($inputURL);
	
	if($PageContent!~m/<table\s*class=\"results_grid\"\s*summary=\"Search\s*results\s*for\s*Architects\s*based\s*on\s*a\s*search\s*term\">/is)
	{
		open(LL,">>$Location/needtoCheck.txt");
		print LL "$ID\t$inputURL\n";
		close(LL);
	}
	else
	{

		my ($detailURL) = &collectDetailURL($PageContent, $inputURL);
		my @detailURLs = @{$detailURL};
		
		my $tempQuery;
		my $query = "INSERT INTO ContactsScrape_Detail_URLLog (ID,detail_urls,created_date) VALUES ";
		
		my $count = 0;
		foreach my $url(@detailURLs)
		{
			my $queryValue = "(\'$ID\', \'$url\', GETDATE()), ";
			# print "$ID==>$query\n"; 
			$tempQuery .= $queryValue;
			$count++;
			
			if($count=~m/^998$/is)
			{
				$tempQuery=~s/\,\s*$//gsi;
		
				$tempQuery = $query.$tempQuery;
			
				
				# print "query==>$tempQuery\n";<STDIN>;
				
				if($tempQuery!~m/values\s*$/is)
				{
					# &Hopper::DB_Insert($dbHost,$tempQuery);
					undef $tempQuery;
				}
				
				$count = 0;
			}
		}
		
		$tempQuery=~s/\,\s*$//gsi;
		
		$query .= $tempQuery;
		# print "Final query==>$query\n";<STDIN>;
		if($query!~m/values\s*$/is)
		{
			# &Hopper::DB_Insert($dbHost,$query);
			undef $tempQuery;
		}
	}
	# <STDIN>;
}

sub collectDetailURL
{
	my $townContent = shift;
	my $postURL = shift;
	
	my @dURLs;
	my $pageNumber = 2;

	my ($viewState, $viewStateGenerator, $eventValidation, $eventArgument);
	print "Page Number::1\n";
	NextPage:	
	$viewState = uri_escape($1) if($townContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATE_REGEX'}/is);
	$viewStateGenerator = uri_escape($1) if($townContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATEGENERATOR_REGEX'}/is);
	$eventValidation = uri_escape($1) if($townContent=~m/$councilDetailsFile->{$councilName}->{'EVENTVALIDATION_REGEX'}/is);
	$eventArgument = uri_escape($1) if($townContent=~m/$councilDetailsFile->{$councilName}->{'EVENTARGUMENT_REGEX'}/is);
	
	# open(RR, ">>$outputDirectory/Redis_Check_ARB.txt");
		
	my $gridTableContent = $1 if($townContent=~m/<table\s*class=\"results_grid\"\s*summary[^>]*?>\s*([\w\W]*?)\s*<\/table>/is);	
	while($gridTableContent=~m/<a[^>]*?href=\"(?:\.?\.?(?:\/?\.?\.?)?)?([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>/gsi)
	{
		my $detailGridURL = $1;
		my $detailGridNumber = $2;
		
		$detailGridURL=~s/\/\.\.//si;
				
		if(($detailGridURL!~m/^http/is) && ($detailGridURL ne ""))
		{
			$detailGridURL = URI::URL->new($detailGridURL)->abs( $councilDetailsFile->{$councilName}->{'URL'}, 1 );
		}
		
		$detailGridURL=~s/(architects-register\.org\.uk\/)list\//$1/gsi;
		
		# if($detailGridNumber!~m/^\s*$/is)
		# {
			# if($redis->exists("ARB_".$detailGridNumber))	
			# {
				# print "Exists : $detailGridNumber\n";
				# print RR "Exists : $detailGridNumber\t$detailGridURL\t".localtime()."\n";
				# next;
			# }
			# else
			# {
				# $redis->set("ARB_".$detailGridNumber => $detailGridNumber);
				# print "Not Exists : $detailGridNumber\n";
			# }
		# }	
				
		push(@dURLs, $detailGridURL)
	}
	# close RR;
		
	
	
	if($townContent=~m/<\/span>[^<]*?<a[^>]*?href=\"javascript\:__doPostBack\(\&\#39\;([^>]*?)\&\#39\;\,[^>]*?>\s*($pageNumber|\.\.\.)\s*<\/a>/is)
	{
		my $eventTarget = uri_escape($1);
		
		print "Page Number::$pageNumber\n";
		
		my $nextPagePostCnt = $councilDetailsFile->{$councilName}->{'NEXTPAGE_POSTCONTENT'};
			
		$nextPagePostCnt=~s/<VIEWSTATE>/$viewState/s;
		$nextPagePostCnt=~s/<VIEWSTATEGENERATOR>/$viewStateGenerator/s;
		$nextPagePostCnt=~s/<EVENTVALIDATION>/$eventValidation/s;
		$nextPagePostCnt=~s/<EVENTARGUMENT>/$eventArgument/s;
		$nextPagePostCnt=~s/<EVENTTARGET>/$eventTarget/s;
		
		my ($responseContent, $pingStatus);
		($responseContent, $pingStatus) = &Hopper::MechPostMethod($postURL, $nextPagePostCnt );		
		$townContent = $responseContent;
		
		
		# open(RR, ">$Location/$pageNumber.html");
		# print RR "$townContent";
		# close(RR);
				
		$pageNumber++;
		
		goto NextPage;
	}
	else
	{		
		print "Next page not available\n";
	}
	
	return (\@dURLs);
}