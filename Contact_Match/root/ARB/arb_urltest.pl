use strict;
use WWW::Mechanize;

my $filename = "C:/Glenigan/Merit/Projects/hopper/output/05122018/needtoCheck.txt";

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";
while (my $url = <$fh>) {
	chomp $url;
	print "$url\n";
	
	my $absURL = $1 if($url=~m/^\d+\s+(http[^\n]*?)$/is);
	
	my $mech = WWW::Mechanize->new();
	
	$mech->get($absURL);
	my $content = $mech->content;
	my $status = $mech->status;
	
	if($status!~m/^200$/is)
	{
		open(PP,">>failed.txt");
		print PP "$absURL\n";
		close(PP);
	}
	if($content=~m/<td\s*class=\s*"selected\">\s*(\d+)\s*<\/td>/is)
	{
		my $number = $1;
		
		if($number ne '0')
		{
			open(PP,">>checkagain.txt");
			print PP "$absURL\n";
			close(PP);
		}
	}
	
}