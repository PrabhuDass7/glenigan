use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use File::Path qw/make_path/;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;
my $scriptPath = ($basePath.'/root');

open JJ,">Output.txt";
close JJ;
		
open FH,"<input.txt";
my @arr=<FH>;

my $count=0;
my ($first_Value,$last_Val);
foreach my $arr(@arr)
{
	$count++;
	
	chomp($arr);
	
	if($count == 1)
	{
		$first_Value=$arr;
		print "Count:: $count\n";
	}
	
	if($count == 500)
	{
		print "Count:: $count\n";
		
		open JJ,">>Output.txt";
		print JJ "perl arbDetailURL.pl $first_Value $arr\n";
		close JJ;
		
		# system("start perl arbDetailURL.pl $first_Value $arr");
		
		system("start perl $scriptPath/ARB/arbDetailURL.pl $first_Value $arr &");
		
		# system("start Output.txt");
		
		$count=0;
		$first_Value="";
		sleep(3);
	}
	
	$last_Val=$arr;
}

if($first_Value ne "")
{
	open JJ,">>Output.txt";
	print JJ "perl arbDetailURL.pl $first_Value $last_Val\n";
	close JJ;
	
	# system("perl arbDetailURL.pl $first_Value $last_Val");
	
	system("start perl $scriptPath/ARB/arbDetailURL.pl $first_Value $last_Val &");
	
	sleep(3);
}

print "Triggered All Instants are COMPLETED ****** \n";