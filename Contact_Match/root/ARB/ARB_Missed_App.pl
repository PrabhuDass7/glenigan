#!/usr/bin/perl
use utf8;
use strict;
use URI::URL;
use URI::Escape;
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw(make_path);
use Text::CSV;
use IO::Socket::SSL;
use WWW::Mechanize;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');
# print "Today Date::$toDay\n"; <STDIN>;


my $Mech = WWW::Mechanize->new( 
				ssl_opts => {
								SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
								verify_hostname => 0, 
							}
							, autocheck => 0
			);
		
$Mech->proxy(['http','https'], 'http://172.27.137.199:3128');	



my $councilName = "ARB";


####
# Create new object of class Text::CSV here
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";

my @inputURL = ("http://architects-register.org.uk/architect/057934C","http://architects-register.org.uk/architect/077517G","http://architects-register.org.uk/architect/040688K","http://architects-register.org.uk/architect/052398D","http://architects-register.org.uk/architect/050708C","http://architects-register.org.uk/architect/046003F","http://architects-register.org.uk/architect/077300J","http://architects-register.org.uk/architect/053629F","http://architects-register.org.uk/architect/051134J","http://architects-register.org.uk/architect/039915I","http://architects-register.org.uk/architect/094731H","http://architects-register.org.uk/architect/056528H","http://architects-register.org.uk/architect/093246I","http://architects-register.org.uk/architect/050783K","http://architects-register.org.uk/architect/054614C","http://architects-register.org.uk/architect/063742D","http://architects-register.org.uk/architect/037539J","http://architects-register.org.uk/architect/031572I","http://architects-register.org.uk/architect/028319C","http://architects-register.org.uk/architect/052495F","http://architects-register.org.uk/architect/094315K","http://architects-register.org.uk/architect/064500A","http://architects-register.org.uk/architect/028532C","http://architects-register.org.uk/architect/061299E","http://architects-register.org.uk/architect/051137D","http://architects-register.org.uk/architect/040715A","http://architects-register.org.uk/architect/095586H","http://architects-register.org.uk/architect/053334C","http://architects-register.org.uk/architect/027996J","http://architects-register.org.uk/architect/049981A","http://architects-register.org.uk/architect/050037B","http://architects-register.org.uk/architect/041453K","http://architects-register.org.uk/architect/044386G","http://architects-register.org.uk/architect/042520F","http://architects-register.org.uk/architect/058730C","http://architects-register.org.uk/architect/082178K","http://architects-register.org.uk/architect/048650G","http://architects-register.org.uk/architect/093714B","http://architects-register.org.uk/architect/041855B","http://architects-register.org.uk/architect/048082G","http://architects-register.org.uk/architect/051052A","http://architects-register.org.uk/architect/052928A","http://architects-register.org.uk/architect/052491C","http://architects-register.org.uk/architect/048963H","http://architects-register.org.uk/architect/055802H","http://architects-register.org.uk/architect/057351E","http://architects-register.org.uk/architect/072315K","http://architects-register.org.uk/architect/034852J","http://architects-register.org.uk/architect/046486D","http://architects-register.org.uk/architect/058922E","http://architects-register.org.uk/architect/054380B","http://architects-register.org.uk/architect/036860A","http://architects-register.org.uk/architect/048894A","http://architects-register.org.uk/architect/041780G","http://architects-register.org.uk/architect/032714J","http://architects-register.org.uk/architect/045629B","http://architects-register.org.uk/architect/049369D","http://architects-register.org.uk/architect/052924I","http://architects-register.org.uk/architect/076496E","http://architects-register.org.uk/architect/042698I","http://architects-register.org.uk/architect/043980K","http://architects-register.org.uk/architect/037548I","http://architects-register.org.uk/architect/035121K");

my $applicationCount = 1;

my $Location = "C:\\Glenigan\\hopper\\root\\ARB\\New folder";
unless ( -d $Location )
{
	make_path($Location);
}

my $filename = "$Location/ARBOutputDetailsMissed.csv";
open my $fh, ">:utf8", $filename or die "failed to create $filename: $!";
my(@heading) = ("Registration Number", "Members Heading", "Company Name", "Address", "Telephone Number", "Email", "Company Website Address", "Detail URL");
$csv->print($fh, \@heading);    # Array ref!

my $filenames = "$Location/ARBOutputDetailsMissed.txt";
open my $fhs, ">", $filenames or die "failed to create $filenames: $!";
binmode($fhs, ":utf8");
foreach my $url(@inputURL)
{
	# print "URL==>$url\n";
	my ($pingStatus, $PageContent);	
	
	eval
	{
		$Mech->get( $url );
	};
	$PageContent = $Mech->content;	
	$pingStatus = $Mech->status;	
	
	if($PageContent!~m/<span[^>]*?>\s*Registration\s*Number\s*<\/span>\s*(?:<[^>]*?>)?\s*<strong>[^<]*?<\/strong>/is)
	{
		open(LL,">>Details_needtoCheck.txt");
		print LL "$url\n";
		close(LL);
	}
	else
	{
		my $contactDetails = &getContactDetails($PageContent,$url);	
		my @datarow = split('\t', $contactDetails);
		$csv->print($fh, \@datarow); 
		print $fhs "$contactDetails\n";
		$applicationCount++;
	}
	
}
close $fhs or die "failed to close $filenames: $!";
close $fh or die "failed to close $filename: $!";
print "Total Application Number Count::$applicationCount\n";


sub getContactDetails
{
	my $contactCnt = shift;
	my $detailURL = shift;
	
	my $conDetails;
	if($contactCnt=~m/<div\s*class=\"list_registrants\s*generalcontent\"[^>]*?>\s*(?:<[^>]*?>)?\s*<table[^>]*?>\s*([\w\W]*?)\s*<\/table>/is)
	{
		my $tableCnt = $1;
		
		my ($memHeading, $companyName, $companyAddress, $regNumber, $teleNumber, $cntEmail, $cntWebsite);
		
		$memHeading = &clean($1) if($tableCnt=~m/<h1>\s*([^<]*?)\s*<\/h1>/is);
		$companyName = &clean($1) if($tableCnt=~m/>\s*Address\s*<\/span>\s*<div[^>]*?>\s*<strong>\s*([\w\W]*?)\s*<\/strong>/is);
		$companyAddress = &clean($1) if($tableCnt=~m/>\s*Address\s*<\/span>\s*<div[^>]*?>\s*<strong>\s*[\w\W]*?\s*<\/strong>\s*([\w\W]*?)\s*<\/div>/is);
		$regNumber = &clean($1) if($tableCnt=~m/>\s*Registration\s*Number\s*<\/span>\s*(?:<div[^>]*?>)?\s*<strong>\s*([\w\W]*?)\s*<\/strong>/is);
		$teleNumber = &clean($1) if($tableCnt=~m/<li[^>]*?>\s*<img[^>]*?>\s*Tel\:\s*([^<]*?)\s*(?:<[^<]*?>)?\s*<\/li>/is);
		$cntEmail = &clean($1) if($tableCnt=~m/<a\s*href=mailto\:([^>]*?)>\s*Send\s*an\s*Email\s*</is);
		$cntWebsite = &clean($1) if($tableCnt=~m/<a\s*href=\'([^\']*?)\'[^>]*?>\s*Visit\s*Website\s*</is);
	
		print "Registration Number::$regNumber is Processing..\n";
		
		$conDetails = "$regNumber\t$memHeading\t$companyName\t$companyAddress\t$teleNumber\t$cntEmail\t$cntWebsite\t$detailURL";
	}
	
	return ($conDetails);
}


sub clean
{
	my $Data=shift;
	
	$Data=~s/\s*<[^>]*?>\s*/ /igs;
	$Data=~s/amp;//igs;
	$Data=~s/\&nbsp\;/ /igs;
	$Data=~s/^\s+|\s+$//igs;
	$Data=~s/\s\s+/ /igs;
	$Data=~s/^\W+$//igs;
	$Data=~s/\#xD\;\&\#xA\;//igs;
	$Data=~s/\&\#39\;/\'/igs;
	$Data=~s/^\s*//igs;
	$Data=~s/\s*$//igs;
	
	return ($Data);
}
