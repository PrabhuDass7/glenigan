#!/usr/bin/perl
use strict;
use Config::Tiny;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DateTime;
use File::Path qw/make_path/;


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\ARB\s*$//si;
$basePath=~s/\/ARB\s*$//si;
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

my $dt = DateTime->now();
my $toDay = $dt->dmy('');

####
# Declare and load local directories
####
my $ConfigDirectory = ($basePath.'/etc/');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $scriptPath = ($basePath.'/root');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module


####
# Create new object of class Config::Tiny
####

# my ($councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile" 
####

# $councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


# my ($pingStatus, $paginationContent, $viewState, $viewStateGenerator, $eventValidation, $councilName);

# $councilName = 'ARB';

# ($pingStatus, $paginationContent) = &Hopper::MechGetMethod($councilDetailsFile->{$councilName}->{'URL'});

# print "pingStatus:: $pingStatus\n";<STDIN>;

# open JJ, ">Home.html";
# print JJ "$paginationContent";
# close JJ;


# $viewState = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATE_REGEX'}/is);
# $viewStateGenerator = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATEGENERATOR_REGEX'}/is);
# $eventValidation = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'EVENTVALIDATION_REGEX'}/is);

# my $rContent;
# if($paginationContent=~m/<div\s*class=\"alphabet_list_content\">\s*([\w\W]*?)\s*<\/div>/is)
# {
	# $rContent = $1;
# }

# my $Location = $outputDirectory."/".$toDay;

my @alphabets = ("A".."Z");
# while($rContent=~m/<input\s*type=\"submit\"\s*name=\"(ctl00\$mainContent\$Button8)\"\s*value=\"(J)\"[^>]*?>/gsi)
# while($rContent=~m/$councilDetailsFile->{$councilName}->{'ALPHABET_REGEX'}/gsi)
foreach my $alphabet(@alphabets)
{
	chomp($alphabet);
	print "Alphabets:: $alphabet\n";
	
=e	
	my $sub_URL="https://architects-register.org.uk/registrant/list?";
	
	my $PostCnt = $councilDetailsFile->{$councilName}->{'SEARCH_POSTCONTENT'};
		
	$PostCnt=~s/<ALPHABET>/$alphabet/igs;
	# $PostCnt=uri_escape($PostCnt);
	$sub_URL.=$PostCnt;
	# my $Total_Page_no=$1 if($paginationContent=~m/<[^>]*?>\s*Page[^>]*?of\s*([\d]+)\s*<[^<]*?>/is);
	print "sub_URL::: $sub_URL";<STDIN>;
	
	# Initializing the List script for each arguments	
=cut
	# system("start perl $scriptPath/ARB/arbListURL.pl $alphabet &");
	system("start perl $scriptPath/ARB/arbDetailURL.pl $alphabet &");
	
	sleep (30);
	
}