# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 13:14:57 2018

@author: parielavarasan
"""

import os, re, sys
import requests
import configparser
import csv, codecs
from requests.utils import quote


folder_path = 'C:/Glenigan/Merit/Projects/hopper/'

proxies = {
    'http': 'http://172.27.137.199:3128',
    'https': 'http://172.27.137.199:3128',
}

# Create the session and set the proxies.
s = requests.Session()
s.proxies = proxies

gradeValue = sys.argv[1] 
gradeName = sys.argv[2] 
councilName = sys.argv[3]  

#gradeName = "Associate"
#gradeValue = "03"
#councilName = "ISTRUCTE" 
 
print(gradeName+" started processing..")

if not gradeValue or not gradeName: 
	print("IStructe arugument not found!. Please check IStructe control script again.")
	sys.exit()
else:    
    print("IStructe arugument found")

############################################################# Clean function######################################
def clean(cleanValue):
    try:        
        cleanValue = re.sub(r'\s*<[^>]*?>\s*', " ", str(cleanValue)) 
        cleanValue = re.sub(r'amp\;', "", str(cleanValue))
        cleanValue = re.sub(r'\&nbsp\;', " ", str(cleanValue))
        cleanValue = re.sub(r'^\s+|\s+$', "", str(cleanValue))
        cleanValue = re.sub(r'\s\s+', " ", str(cleanValue))
        cleanValue = re.sub(r'^\W+$', "", str(cleanValue))
        cleanValue = re.sub(r'\'', "\'\'", str(cleanValue))
        cleanValue = re.sub(r'\#xD\;\&\#xA\;', "", str(cleanValue))
        cleanValue = re.sub(r'\&\#39\;', "\'", str(cleanValue))
        cleanValue = re.sub(r'^\s*', "", str(cleanValue))
        cleanValue = re.sub(r'\s*$', "", str(cleanValue))
        
        return cleanValue    

    except Exception as ex:
        print (ex)
   
    
############################################################## Get applicant details function######################################
def getDetailsValue(dataValue):
    try:
        contactCnt = re.findall("<div[^>]*?SearchResultDetail\"[^>]*?>\s*([\w\W]*?)\s*<\/table>\s*<\/div>", str(dataValue), re.IGNORECASE)
        contactCnt = re.sub(r'\\n', " ", str(contactCnt)) 
        
        
        memNumber = re.findall("<td[^>]*?>\s*Member\s*No\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        memHeading = re.findall("<tr\s*class=\"MembersHeading\">\s*<th[^>]*?>\s*([^<]*?)\s*\&\#32\;\s*\&\#45\;\s*\&\#32\;\s*[^<]*?<\/th>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        if not memHeading:
            memHeading = re.findall("<tr\s*class=\"MembersHeading\">\s*<th[^>]*?>\s*([^<]*?)\s*\-\s*[^<]*?<\/th>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        companyAddress = re.findall("<td[^>]*?>\s*Address\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        companyCountry = re.findall("<td[^>]*?>\s*Country\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        teleNumber = re.findall("<td[^>]*?>\s*Telephone\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        cntEmail = re.findall("<td[^>]*?>\s*Email\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        cntFax = re.findall("<td[^>]*?>\s*Fax\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        cntGrade = re.findall("<td[^>]*?>\s*Grade\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        cntNarrative = re.findall("<td[^>]*?>\s*Narrative\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        cntCPDApproved = re.findall("<td[^>]*?>\s*CPD\s*Approved\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>", str(contactCnt), re.IGNORECASE)
        
        conDetails=[]
        
        if len(memNumber) > 0:
            conDetails.append( clean(memNumber[0]) );
        if len(memHeading) > 0:
            conDetails.append( clean(memHeading[0]) );
        if len(companyAddress) > 0:
            conDetails.append( clean(companyAddress[0]) );
        if len(companyCountry) > 0:
            conDetails.append( clean(companyCountry[0]) );
        if len(teleNumber) > 0:
            conDetails.append( clean(teleNumber[0]) );
        if len(cntEmail) > 0:
            conDetails.append( clean(cntEmail[0]) );
        if len(cntFax) > 0:
            conDetails.append( clean(cntFax[0]) );
        if len(cntGrade) > 0:
            conDetails.append( clean(cntGrade[0]) );
        if len(cntNarrative) > 0:
            conDetails.append( clean(cntNarrative[0]) );
        if len(cntCPDApproved) > 0:
            conDetails.append( clean(cntCPDApproved[0]) );
                    
        return conDetails    
    except Exception as e:
        print (e)
    
    
#############################################################Main function##############################################
def main():
    try:
        # Read settings from configuration file.
        config = configparser.ConfigParser()
        config.read(folder_path+'etc/Site_Details.ini')
		
        # read values from a section
        istructe_url = config.get(councilName, 'URL')
        istructe_countriesvalue = config.get(councilName, 'COUNTRIES_VALUE')
        print ("istructe_url:",istructe_url)
            
        # Make the HTTP request through the session.
        r = s.get(istructe_url)
        content=r.content
        # print("content : :",type(content))
        # with open(folder_path+"output/istructe.html", 'wb') as fd:
            # fd.write(content)
        # sys.exit(0)  
        
        VIEWSTATE = re.findall("__VIEWSTATE\s*\"[^>]*?value\=\"([^\"]*?)\"", str(content), re.IGNORECASE)
        SCROLLPOSITIONX = re.findall("<input[^>]*?name\s*=\s*\"__SCROLLPOSITIONX\"[^>]*?value=\"([^\"]*?)\"[^>]*?>", str(content), re.IGNORECASE)
        SCROLLPOSITIONY = re.findall("<input[^>]*?name\s*=\s*\"__SCROLLPOSITIONY\"[^>]*?value=\"([^\"]*?)\"[^>]*?>", str(content), re.IGNORECASE)
        VIEWSTATEGENERATOR = re.findall("__VIEWSTATEGENERATOR\s*\"[^>]*?value\=\"([^\"]*?)\"", str(content), re.IGNORECASE)
		
                
        post_content = '__EVENTTARGET=&__EVENTARGUMENT=&lng=en-GB&__VIEWSTATEGENERATOR=' + str(VIEWSTATEGENERATOR[0]) + '&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&p%24lt%24ctl06%24LoginForm%24Login1%24UserName=&p%24lt%24ctl06%24LoginForm%24Login1%24Password=&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24txtFirstName=&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24txtMembershipNumber=&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24txtLocation=&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24ddlCountries=' + istructe_countriesvalue + '&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24btnSubmit=Search+Members+Directory&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24txtSurname=&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24ddlMembershipGrade=' + str(gradeValue) + '&p%24lt%24ctl10%24pageplaceholder%24p%24lt%24ctl09%24MembersSearchForm%24rbLstCPD=0&__VIEWSTATE=' + str(VIEWSTATE[0].replace ('=', '%3D').replace ('+','%2B'))
        post_content = post_content.replace ('/', '%2F').replace ('$', '%24').replace ('==','%3D%3D')
       
        # print(post_content)
        
        header= {'Host':'www.istructe.org','Origin':'https://www.istructe.org','Referer':istructe_url,'Content-Type':'text/html; charset=utf-8','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Language': 'en-GB,en;q=0.5','Accept-Encoding': 'gzip, deflate, br'}
       
        con=s.post(istructe_url, data=post_content, headers=header, verify=True)
        # data_con = str(con.content).encode(encoding = 'UTF-8',errors = 'strict')
        print(str(con.content).encode("UTF-8", "ignore"))
        # with open ('NiftyList.html', 'w') as f:
            # f.write(str(encoded3))
        # input()
		
        
        filename = folder_path+"output/IStructeOutput_"+gradeName+".csv";
            
        matchedContent = re.findall("<div\s*class=\"MemberListContainer\"[^>]*?>\s*<table[^>]*?>\s*([\w\W]*?)\s*<\/table>\s*<\/div>", str(data_con), re.IGNORECASE)    
        matchedCtnt = re.sub(r'\\n', " ", str(matchedContent))        
        gradeContent=re.findall("<tr>\s*<td>\s*([^<]*?)\s*<\/td>\s*<td>\s*<a[^>]*?href=\"javascript\:__doPostBack\(\&\#39\;([^>]*?)\&\#39\;\,[^>]*?>", str(matchedCtnt), re.IGNORECASE)
         
        with open(filename, 'wb') as csvfile:
            fieldnames = ['Member Number', 'Member Heading', 'Address', 'Country', 'Email', 'Telephone Number', 'Fax', 'CPD Approved', 'Grade', 'Narrative']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames) 
            writer.writeheader()
            
            for gradeMatch in gradeContent:
                gradeID = gradeMatch[0]
                gradeEventTarget = quote(gradeMatch[1])  
                
                print ("Now processing GradeName:", str(gradeID))
                print ("Now processing GradeValue:", str(gradeEventTarget))
                print ("Grade Value:", str(gradeValue))
                print ("Countries:", str(istructe_countriesvalue))
                
                VIEWSTATENEW = re.findall("__VIEWSTATE\s*\" value\=\"([^>]*?)\"", data_con, re.IGNORECASE)
                VIEWSTATENEW = quote(VIEWSTATENEW[0])
                VIEWSTATEGENERATORNEW = re.findall("__VIEWSTATEGENERATOR\s*\" value\=\"([^>]*?)\"", data_con, re.IGNORECASE)
                VIEWSTATEGENERATORNEW = quote(VIEWSTATEGENERATORNEW[0])
                
                
                detailPostCont = 'manScript_HiddenField=&__EVENTTARGET=' + gradeEventTarget + '&__EVENTARGUMENT=&lng=en-GB&__VIEWSTATEGENERATOR=' + VIEWSTATEGENERATORNEW + '&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=0&p%24lt%24zoneSearch%24SearchBox%24txtWord=&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24txtSurname=&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24txtForenames=&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24txtMemNo=&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24rbLstCPD=0&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24ddlGrade=' + str(gradeValue) + '&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24txtLocation=&p%24lt%24zoneMain%24pageplaceholder%24p%24lt%24zoneMainContent%24IStructe_MemberSearchForm%24ddlCountries=' + istructe_countriesvalue + '&__VIEWSTATE=' + VIEWSTATENEW
                
                detailHeader= {'Host':'www.istructe.org','Referer':istructe_url,'Content-Type':'application/x-www-form-urlencoded','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Language': 'en-GB,en;q=0.5','Accept-Encoding': 'gzip, deflate, br'}
            
                detailCon=s.post(istructe_url, data=detailPostCont, headers=detailHeader, verify=True)
                detail_con = detailCon.content
                
#                with open(folder_path+"output/istructe.html", 'wb') as fd:
#                    fd.write(detail_con)
                                
                myData = getDetailsValue(detail_con)
#                print myData
                                 
                    
                writer.writerow({'Member Number': str(myData[0]), 'Member Heading': str(myData[1]), 'Address': str(myData[2]),'Country': str(myData[3]), 'Email': str(myData[4]), 'Telephone Number': str(myData[5]),'Fax': str(myData[6]), 'CPD Approved': str(myData[7]), 'Grade': str(myData[8]),'Narrative': str(myData[9])})
            
            
       
    except Exception as e:
    	print (e,sys.exc_traceback.tb_lineno)
	
if __name__ == '__main__':
    main()