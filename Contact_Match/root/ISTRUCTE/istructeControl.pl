#!/usr/bin/perl
use strict;
use Config::Tiny;
# use Log::Log4perl;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use DBI;
use DBD::ODBC;
use Text::CSV;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;

# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $scriptPath = ($basePath.'/root');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

####
# Create new object of class Text::CSV
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


my ($pingStatus, $paginationContent, $councilName);

$councilName = 'ISTRUCTE';

($pingStatus, $paginationContent) = &Hopper::MechGetMethod($councilDetailsFile->{$councilName}->{'URL'});

# All the arguments.
my @arguements;

my $gradeContent;
if($paginationContent=~m/<select[^>]*?MembersSearchForm\$ddlMembershipGrade[^>]*?>\s*([\w\W]*?)\s*<\/select>/is)
{
	$gradeContent = $1;
}

# while($gradeContent=~m/<option[^>]*?value=\"(\d+)\">\s*([^<]*?)\s*<\/option>/gsi)
while($gradeContent=~m/<option[^>]*?value=\"(\d+)\">\s*([^<]*?)\s*(?:<\/option>)?$/mgsi)
{
	my $gradeValue = $1;
	my $gradeName = $2;
	
	# print "$gradeValue\n";
	# print "$gradeName\n"; 
	# <STDIN>;
	
	# next if($gradeValue eq "08");
	
	my $filename = "$outputDirectory/IStructeOutput_$gradeName.csv";
	open my $fh, ">:encoding(utf8)", $filename or die "failed to create $filename: $!";
	my(@heading) = ("Member Number", "Members Heading", "Address", "Country", "Email", "Telephone Number", "Fax", "CPD Approved", "Grade", "Narrative");
	$csv->print($fh, \@heading);    # Array ref!
	close $fh or die "failed to close $filename: $!";
		
			
	# Initializing the List script for each arguments	
	system("start perl $scriptPath/ISTRUCTE/istructeList.pl $gradeName $gradeValue $councilName &");
	
	# sleep (60);
	
}