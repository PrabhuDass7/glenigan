use strict;
use warnings;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;
 
my $filename = 'email.txt';
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
 
while (my $row = <$fh>) {
  chomp $row;
  # print "$row\n";
  if($row=~m/^([0-9A-Z]{9})\t([^\n]*?)$/is)
  {
	my $memID = $1;
	my $encodeMail = $2;
	print "$memID==>$encodeMail\n";
	my $cntEmail = qx("c:/Anaconda/python.exe $basePath/root/Istructe/mailDecode_DBUpdate.py $memID $encodeMail");
	# print "cntEmail==>$cntEmail\n";
	# <STDIN>;
  }
}