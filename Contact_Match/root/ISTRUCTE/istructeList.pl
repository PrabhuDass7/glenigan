#!/usr/bin/perl
use strict;
use URI::Escape;
use Config::Tiny;
# use RedisDB;
# use Log::Log4perl;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Text::CSV;
use Encode qw(encode decode);
use MIME::Base64;
use MIME::Words qw(:all);

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\\root\s*$//si;
$basePath=~s/\/root\s*$//si;
# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $logsDirectory = ($basePath.'/logs');
my $outputDirectory = ($basePath.'/output');
my $libDirectory = ($basePath.'/lib/Core');

require ($libDirectory.'/Hopper.pm'); # Private Module

####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile) = Config::Tiny->new();

####
# Create new object of class Text::CSV here
####
my $csv = Text::CSV->new({binary => 1, eol => $/ }) or die "Failed to create a CSV handle: $!";

####
# Create redis connection
####
# my $redis = &Hopper::Redis_Connection();

####
# Establish connection with DB server
####
my $dbh = &Hopper::DbConnection();



####
# Read INI files from the objects "$councilDetailsFile" 
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Site_Details.ini');


my $gradeName = $ARGV[0];
my $gradeValue = $ARGV[1];
my $councilName = $ARGV[2];

print "\"$gradeName\" started processing..\n";


if(($gradeName eq "") && ($gradeValue eq ""))
{
	print "IStructe arugument not found!. Please check IStructe control script again.\n";
	exit;
}
else
{
	print "IStructe arugument found.\n";
}

my ($pingStatus, $paginationContent, $responseContent, $scrollPositionX, $scrollPositionY, $matchedContent, $viewState, $viewStateGenerator);

($pingStatus, $paginationContent) = &Hopper::MechGetMethod($councilDetailsFile->{$councilName}->{'URL'});

$viewState = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATE_REGEX'}/is);
$viewStateGenerator = uri_escape($1) if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'VIEWSTATEGENERATOR_REGEX'}/is);
	
if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'SCROLLPOSITIONX_REGEX'}/is)
{
	$scrollPositionX = $1;
}
if($paginationContent=~m/$councilDetailsFile->{$councilName}->{'SCROLLPOSITIONY_REGEX'}/is)
{
	$scrollPositionY = $1;
}

$scrollPositionX = 0 if($scrollPositionX eq "");
$scrollPositionY = 0 if($scrollPositionY eq "");

my $postCnt = $councilDetailsFile->{$councilName}->{'SEARCH_POSTCONTENT'};
	
	
$postCnt=~s/<VIEWSTATE>/$viewState/s;
$postCnt=~s/<VIEWSTATEGENERATOR>/$viewStateGenerator/s;
$postCnt=~s/<EVENTTARGET>//s;
$postCnt=~s/<EVENTARGUMENT>//s;
$postCnt=~s/<SCROLLPOSITIONX>/$scrollPositionX/s;
$postCnt=~s/<SCROLLPOSITIONY>/$scrollPositionY/s;
$postCnt=~s/<GRADE>/$gradeValue/s;

($responseContent, $pingStatus) = &Hopper::MechPostMethod($councilDetailsFile->{$councilName}->{'URL'}, $postCnt );
	

# open(PP,">a.html");
# print PP $responseContent;
# close(PP);
# exit;

my $insert_query="insert into ContactsHopper_Source (Member_Number, Contact_Name, Contact_Email, Company_Name, Company_Address, Company_Postcode, Company_Phone, Company_Website, Source_Name) values ";

my $filename = "$outputDirectory/IStructeOutput_$gradeName.csv";
open my $fh, ">>:encoding(utf8)", $filename or die "failed to create $filename: $!";
# open(RR, ">>$outputDirectory/Redis_Check_IStructe.txt");

my $count = 0;
while($responseContent=~m/<div\sclass=\"accordion\saccordion--committee\">([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>\s*<\/div>/gsi)
{
	my $postResponseContent = $1;	
	
	$count++;
	my ($contactDetails, $insertQuery) = &getContactDetails($postResponseContent);		
	
	
	if($count == 999)
	{
		if($insert_query!~m/values\s*$/is)
		{
			$insert_query=~s/\,\s*$//igs;
			&Hopper::DB_Insert($dbh,$insert_query);
			$insert_query="";
		}
	}
	else
	{
		$insert_query.=$insertQuery;
	}
	
	my @datarow = split('\t', $contactDetails);
	$csv->print($fh, \@datarow); 
}
# close RR;
close $fh or die "failed to close $filename: $!";

$insert_query=~s/\,\s*$//igs;
print "insert_query::$insert_query\n";

if($insert_query!~m/values\s*$/is)
{
	&Hopper::DB_Insert($dbh,$insert_query);
}

sub getContactDetails
{
	my $contactCnt = shift;
	
	my ($conDetails,$insertquery);

	my $tableCnt = $1;
	
	my ($memNumber, $memHeading, $companyAddress, $companyAddress1, $companyAddressPostCode, $companyCountry, $teleNumber, $cntEmail, $cntFax, $cntGrade, $cntNarrative, $cntCPDApproved);
	
	$memNumber = &Hopper::clean($1) if($tableCnt=~m/<h5>\s*[^<]*?\s*<\/h5>\s*<\/div>\s*<\/div>\s*<div\s*class=\"grid-column\">\s*([^<]*?)\s*<\/div>/is);
	print $memNumber;
	$memHeading = &Hopper::clean($1) if($tableCnt=~m/<h5>\s*([^<]*?)\s*<\/h5>\s*<\/div>/is);	
	print $memHeading;
	$companyAddress = &Hopper::clean($1) if($tableCnt=~m/<div\s*class=\"accordion__content\">\s*<div[^>]*?>\s*<div\s*class=\"grid-column\">\s*([\w\W]*?)\s*<\/div>/is);
	# $companyCountry = &Hopper::clean($1) if($tableCnt=~m/<td[^>]*?>\s*Country\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([^<]*?)\s*<\/td>\s*<\/tr>/is);
	# if($tableCnt=~m/<td[^>]*?>\s*Address\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<br\s*\/>([^<]*?)<br\s*\/>\s*\&nbsp\;<\/td>\s*<\/tr>/is)
	# {
		# my $address = &Hopper::clean($1);
		# my $pCode = $2;
		# if($pCode=~m/\d+/is)
		# {
			# $companyAddress1 = $address." ".$companyCountry;
		# }
		# else
		# {
			# $companyAddress1 = $address." ".$pCode." ".$companyCountry;
			# $companyAddressPostCode = $pCode;
		# }
	# }
	if($tableCnt=~m/<div\s*class=\"accordion__content\">\s*<div[^>]*?>\s*<div\s*class=\"grid-column\">\s*[\w\W]*?\s*<\/div>\s*<div[^>]*?>\s*([\w\W]*?)\s*<\/div>/is)
	{
		my $matched = $1;	
		
		if($matched=~m/<template[^>]*?cfemail=\"([^\"]*?)\"[^>]*?>/is)
		{
			my $encodeMail = $1;
			$cntEmail = qx("c:/Anaconda/python.exe $basePath/root/Istructe/mailDecode.py $encodeMail");
			$teleNumber = &Hopper::clean($1) if($matched=~m/<br>\s*T\:([^\n]*?)$/is);
		}
	}
	
	# $cntFax = &Hopper::clean($1) if($tableCnt=~m/<td[^>]*?>\s*Fax\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	# $cntGrade = &Hopper::clean($1) if($tableCnt=~m/<td[^>]*?>\s*Grade\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	$cntNarrative = &Hopper::clean($1) if($tableCnt=~m/<td[^>]*?>\s*Narrative\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	$cntCPDApproved = &Hopper::clean($1) if($tableCnt=~m/<td[^>]*?>\s*CPD\s*Approved\s*\:?\s*<\/td>\s*<td[^>]*?>\s*([\w\W]*?)\s*<\/td>\s*<\/tr>/is);
	
	print "Member Heading::$memNumber is Processing..\n";
	
	$conDetails = "$memNumber\t$memHeading\t$companyAddress\t$companyCountry\t$cntEmail\t$teleNumber\t$cntFax\t$cntCPDApproved\t$cntGrade\t$cntNarrative";
	
	$insertquery = "(\'$memNumber\', \'$memHeading\', \'$cntEmail\', \'\', \'$companyAddress\', \'$companyAddressPostCode\', \'$teleNumber\', \'\', \'$councilName\'),";
	
	
	undef $memNumber;  undef $memHeading;  undef $cntEmail;  undef $companyAddress1;  undef $companyAddress;  undef $cntFax;  undef $cntGrade; undef $teleNumber; undef $companyAddressPostCode; undef $cntCPDApproved; undef $companyCountry; undef $cntNarrative;
	
	return ($conDetails, $insertquery);
}