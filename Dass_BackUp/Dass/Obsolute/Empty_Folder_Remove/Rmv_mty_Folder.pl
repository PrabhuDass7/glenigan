use strict;
use File::Path 'rmtree';
use File::Find;
use File::stat;
use Cwd qw(abs_path);
use File::Basename;

open HH,">Folder_Name.txt";
print HH "Folder_Name\tFolder_Size\n";
close HH;

use Cwd;
my $dir = getcwd;

my @files = glob("*");

foreach my $file (@files)
{
	print "file:: $file\n";
	my $path="$dir/$file";
	print "path:: $path\n";
	my $Folder_Size;
	find(sub { $Folder_Size += -s if -f }, $path);
	
	print "Folder_Size:: $Folder_Size\n";
	
	open HH,">>Folder_Name.txt";
	print HH "$file\t$Folder_Size\n";
	close HH;
	
	if($Folder_Size eq "")
	{
		rmtree($path);
	}
}
