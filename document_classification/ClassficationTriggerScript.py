import os 
import pymssql
import time
from datetime import datetime, timedelta

## Get current Year ##
currentYear = time.strftime('%Y')
def dbConnection(database):
	# conn = pymssql.connect(server='172.27.137.184', user='User2', password='Merit456', database=database)
	connLive = pymssql.connect(server='10.101.53.25', user='User2', password='Merit456', database=database)
	return (connLive)
	
	
if __name__ == "__main__":
	# (conn,connLive) = dbConnection("SCREENSCRAPPER")
	(connLive) = dbConnection("SCREENSCRAPPER")
	logDirectory='C:/DocClassification/'
	testSharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload_test/log/'
	SharelogDirectory = '//ch1031sf02/commonglenigan/'+str(currentYear)+'/document_upload/log/'
	# cursorTest = conn.cursor()
	cursorLive = connLive.cursor()
	
	todaydate = time.strftime('%Y-%m-%d')
	
	if os.path.isdir(str(logDirectory)+'logs/'+str(todaydate)) == False:
		os.makedirs(str(logDirectory)+'logs/'+str(todaydate))
	if os.path.isdir(str(SharelogDirectory) + str(todaydate)) == False:
		os.makedirs(str(SharelogDirectory) + str(todaydate))

	cursorLive.execute("select document_upload_started from tbl_common ")
	records = cursorLive.fetchone()[0]
	
	print(records)
	
	if records == 'yes':
		with open(str(logDirectory)+'logs/'+str(todaydate)+'/TriggerRecord.log', 'a') as log:
			log.write(str(records)+"\t"+str(datetime.now())+"\n")
		with open(str(SharelogDirectory)+str(todaydate)+'/TriggerRecord.log', 'a') as log:
			log.write(str(records)+"\t"+str(datetime.now())+"\n")
				
		try:
			os.system('c:\Anaconda3\python.exe '+logDirectory+'/OneApp_Classification_Final.py')
			print("Triggered Succesfully - Pass")
			update_statusquery = "update tbl_common set document_upload_started='Started'"
			cursorLive.execute(update_statusquery)
		except Exception as error:
			print ("error",error)
			with open(str(logDirectory)+'logs/'+str(todaydate)+'/TriggerException.log', 'a') as log:
				log.write(str(error)+"\t"+str(datetime.now())+"\n")
			with open(str(SharelogDirectory)+str(todaydate)+'/TriggerException.log', 'a') as log:
				log.write(str(error)+"\t"+str(datetime.now())+"\n")
	elif records == 'Started':
		with open(str(logDirectory) + 'logs/' + str(todaydate) + '/TriggerRecord.log', 'a') as log:
			log.write("Already Started" + "\t" + str(datetime.now()) + "\n")
		with open(str(SharelogDirectory) + str(todaydate) + '/TriggerRecord.log', 'a') as log:
			log.write("Already Started" + "\t" + str(datetime.now()) + "\n")
	else:
		with open(str(logDirectory)+'logs/'+str(todaydate)+'/TriggerRecord.log', 'a') as log:
			log.write('Input not found'+"\t"+str(datetime.now())+"\n")
		with open(str(SharelogDirectory)+str(todaydate)+'/TriggerRecord.log', 'a') as log:
			log.write('Input not found'+"\t"+str(datetime.now())+"\n")
		