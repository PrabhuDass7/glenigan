use strict;
use File::Copy;
use File::Path 'rmtree';
use File::Find;
use File::stat;
use Cwd qw(abs_path);
use File::Basename;
use Cwd;
use File::Copy::Recursive;
my $dir = getcwd;

print "Please Enter Process Type (Grouping (G) Or Files (F) :: ";
my $File_Type=<STDIN>;

chomp($File_Type);
print "File_Type:: $File_Type";<>;

if($File_Type eq "")
{
	print "Please Enter Process Type***";
	exit;
}

my $PDF="$dir/PDF";
&createdirectory("$PDF");

open JJ,">Status_File.txt";
print JJ "file\tFormat_Id\tSource_Path\tDestination_Path\tstatus\n";
close JJ;

my $loca='\\\\172.27.137.202\\One_app_download\\';
# my $loca='C:\\Users\\prabhud\\Documents\\Live_Project\\2020\\oneapp-py_From_Git\\pdf';


open JJ,"<input.txt";
my @files=<JJ>;

foreach my $files(@files)
{
	my ($file,$Format_Id)=split('\t',$files);
	chomp($file);
	chomp($Format_Id);
	
	print "FILE:: $files\n";
	my($Src_path,$Des_path);
	
	if($File_Type eq "G")
	{
		my $Format="$PDF/$Format_Id";
		&createdirectory("$Format");
		
		$Src_path="$loca\\$file";
		$Des_path="$Format\\$file\\";
	}
	
	if($File_Type eq "F")
	{
		my $Format="$PDF";
		&createdirectory("$Format");
		
		$Src_path="$loca\\$file\\$Format_Id";
		$Des_path="$Format\\$file\\";
	}
	
	
	$Des_path=~s/\//\\/igs;
	
	# print "Src_path:: $Src_path\n";
	# print "Des_path:: $Des_path\n";<>;
	
	# my $Copy_Status = copy("$dir1", "$Format");
	
	my $Folder_Size,my $Copy_Status;
	find(sub { $Folder_Size += -s if -f }, $Src_path);
	if($Folder_Size ne "")
	{
		system("mkdir $Des_path");
		$Copy_Status = system("copy $Src_path $Des_path");
		# print "Copy_Status:: $Copy_Status\n";<>;
	}else{
		$Copy_Status="Empty_Folder";
	}
	
	open JJ,">>Status_File.txt";
	print JJ "$file\t$Format_Id\t$Src_path\t$Des_path\t$Copy_Status\n";
	close JJ;
}

print "Completed***\n";

sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}