package IndiCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use URI::Escape;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use IO::Socket::SSL;
# use Net::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Indi');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Indi_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Indi_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Indi_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Indi_Regex_Heap_Decision.ini' );



####
# Search using Application number in website
####
sub searchAppNumLogic()
{
	my ($receivedCouncilCode, $applicationNumber, $receivedCouncilApp, $category) = @_;
		

	$receivedCouncilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	
	my ($searchResponseContent, $scriptStatus, $rerunCount, $currentPageURL, $commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	Loop:
	eval
	{	
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});	

		if($receivedCouncilCode=~m/^103$/is)
        {
            my $responseContent = $receivedCouncilApp->content;

            my $p_flow_id = $1 if($responseContent=~m/<input[^>]*?name=\"p_flow_id\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
            my $p_flow_step_id = $1 if($responseContent=~m/<input[^>]*?name=\"p_flow_step_id\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
            my $p_instance = $1 if($responseContent=~m/<input[^>]*?name=\"p_instance\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
            my $p_request = $1 if($responseContent=~m/<input[^>]*?name=\"p_request\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is);
            my $pSalt = $1 if($responseContent=~m/<input[^>]*?value=\"([^\"]*?)\"[^>]*?id=\"pSalt\"[^>]*?>/is);

            $applicationNumber=~s/\//\%2F/gsi;
            
            my $postCnt = "p_flow_id=$p_flow_id&p_flow_step_id=$p_flow_step_id&p_instance=$p_instance&p_debug=&p_request=$p_request&p_widget_name=worksheet&p_widget_mod=ACTION&p_widget_action=QUICK_FILTER&p_widget_num_return=50&x01=12058721758773639&x02=19360959293161282&f01=R12058649422773638_column_search_current_column&f01=R12058649422773638_search_field&f01=R12058649422773638_row_select&f02=&f02=$applicationNumber&f02=50&p_json=%7B%22pageItems%22%3Anull%2C%22salt%22%3A%22$pSalt%22%7D";

            print "$postCnt";

            $receivedCouncilApp->post( $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'}, Content => "$postCnt");
        }
		# $receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'});
		# $receivedCouncilApp->set_fields( $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'} => $applicationNumber );
		# $receivedCouncilApp->click();
	};
	
	
    $searchResponseContent = $receivedCouncilApp->content;	
	$currentPageURL = $receivedCouncilApp->uri();
    $scriptStatus = $receivedCouncilApp->status;
	print "scriptStatus==>$scriptStatus\n";
	
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=2))
	{
		$rerunCount++;
		goto Loop;
	}
	
	return($searchResponseContent, $currentPageURL, $scriptStatus)
}



####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	
	return($data2Clean);
}

1;