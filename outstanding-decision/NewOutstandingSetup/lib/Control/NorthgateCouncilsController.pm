package NorthgateCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use File::Path qw/make_path/;
use DateTime;
use URI::Escape;
use WWW::Mechanize;
use IO::Socket::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Northgate');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Northgate_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Northgate_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Northgate_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Northgate_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');



###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Northgate_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Northgate_Decision";
	}	
	
	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);
		
		exit;
	}
		
	
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
	
	
	$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy	
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');	
	
	
	$responseContent = $councilApp->get($commonFileDetails->{$currentCouncilCode}->{'URL'});
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $redisConnection, $Schedule_no, $CouncilApp) = @_;	
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}	
		
	my $actualScrappedCount = 0;
	my ($deDup_Query,$insertQuery);
	while($TabContent =~ m/$commonFileDetails->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
	{
		my $Applicationblock = $1;	
		
		my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing,$pkCode);
	
		$Application = $1 if ( $Applicationblock =~ m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is );		
		$pkCode = $1 if ( $Applicationblock =~ m/$regexFile->{$councilCode}->{'PKCODE'}/is );		
		
		my $fullAppPageLink;
		if($councilCode=~m/^254$/is)
		{
			$fullAppPageLink="http://northgate.liverpool.gov.uk/PlanningExplorer/Generic/StdDetails.aspx?PT=Planning%20Applications%20On-Line&TYPE=PL/PlanningPK.xml&PARAM0=$pkCode&XSLT=/PlanningExplorer/SiteFiles/Skins/Liverpool_WIP/xslt/PL/PLDetails.xslt&FT=Planning%20Application%20Details&PUBLIC=Y&XMLSIDE=&DAURI=PLANNING";
		}
		else
		{
			$fullAppPageLink="StdDetails.aspx?PT=Planning Applications On-Line&TYPE=PL/PlanningPK.xml&PARAM0=$pkCode&XSLT=/Northgate/PlanningExplorer/SiteFiles/Skins/$councilName/xslt/PL/PLDetails.xslt&FT=Planning Application Details&PUBLIC=Y&XMLSIDE=/Northgate/PlanningExplorer/SiteFiles/Skins/$councilName/Menus/PL.xml&DAURI=PLANNING";
		}
		
				
		if($fullAppPageLink!~m/https?\:/is)
		{
			$fullAppPageLink=$commonFileDetails->{$councilCode}->{'FILTER_URL'}.$fullAppPageLink;
		}	
		else
		{
			$fullAppPageLink = $fullAppPageLink;
		}	
		
		$fullAppPageLink=~s/\s+//igs;	
		
		if( $councilName =~ m/^Camden$|^Liverpool$/is )
		{
			$fullAppPageLink =~ s/PlanningExplorer\//PlanningExplorer17\//igs;
		}
		elsif( $councilName=~m/^Charnwood$|^Merton$/is )
		{
			$fullAppPageLink =~ s/PlanningExplorer\//PlanningExplorerAA\//igs;
		}
		elsif($councilName=~m/^East\s*Staffordshire$/is)
		{
			$fullAppPageLink=~s/\/East\s*Staffordshire\//\/EastStaffs\//igs;
		}
		elsif($councilName=~m/^Runnymede$/is)
		{
			$fullAppPageLink=~s/\/Runnymede\//\/Runnymede_AA\//igs;
		}	
		elsif($councilName=~m/^South Tyneside$/is)
		{
			$fullAppPageLink=~s/\/South\s*Tyneside\//\/Default_AA\//igs;
		}
		elsif($councilName=~m/^Tamworth$/is)
		{
			$fullAppPageLink =~ s/PlanningExplorer\//PlanningExplorerAA\//igs;
			$fullAppPageLink=~s/\/Tamworth\//\/NewTamworth\//igs;
		}
		elsif($councilName=~m/^Epping\s*Forest$/is)
		{
			$fullAppPageLink=~s/\/Epping\s+Forest\//\/EppingForest\//igs;
		}
		elsif($councilName=~m/^Conwy\s*County$/is)
		{
			$fullAppPageLink=~s/PlanningExplorer\//EnglishPlanningExplorer\//igs;
			$fullAppPageLink=~s/Conwy\s*County/Conwy/igs;
		}
		elsif($councilName=~m/^Blackburn\s*with\s*Darwen$/is)
		{
			$fullAppPageLink=~s/Blackburn\s*with\s*Darwen/Blackburn/igs;
		}
		elsif($councilName=~m/^North\s*Yorkshire\s*Moors$/is)
		{
			$fullAppPageLink=~s/North\s*Yorkshire\s*Moors/NorthYorkMoors/igs;
		}
		
		$fullAppPageLink =~ s/\&amp\;/\&/g;
		$fullAppPageLink =~s/\'/\'\'/igs;
		
		# print "After::$fullAppPageLink\n"; 
		# <STDIN>;

		if($redisConnection!~m/^OUT$/s)
		{
			if($redisConnection->exists($councilCode."_".$Application))	
			{
				print "Exists : $Application\n";	
				
				my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$Application\', \'$fullAppPageLink\', \'$category\', \'$Source\',\'$councilName\'),";
				$deDup_Query.=$deDupQuery;
				
				next;
			}
			elsif($Application ne "")
			{			
				$actualScrappedCount++;
				## Insert new application in redis server			
				$redisConnection->set($councilCode."_".$Application => $Application);
				print "Not Exists : $Application\n";
			}
		}
		
		
		my ($SITE_LOCATION, $PROPOSAL, $REGISTRATION_DATE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICANT_NAME, $AGENT_NAME, $DOC_URL, $EASTING, $NORTHING, $GRIDREFERENCE, $AGENT_ADDRESS, $APPLICANT_ADDRESS, $DATE_TARGET, $PLANNING_APPLICATION);
		
		$PLANNING_APPLICATION = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
		$APPLICANT_ADDRESS = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
		$AGENT_ADDRESS = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
		$SITE_LOCATION = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
		$PROPOSAL = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
		$REGISTRATION_DATE = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
		$RECEIVED_DATE = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
		$VALID_DATE = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
		$APPLICATION_TYPE = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
		$APPLICATION_STATUS = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
		$APPLICANT_NAME = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
		$AGENT_NAME = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
		$DATE_TARGET = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'DATE_TARGET'}/is);
		
		
		$RECEIVED_DATE 					= &dateFormat($RECEIVED_DATE) if( $RECEIVED_DATE !~ m/^$/is );
		$REGISTRATION_DATE 				= &dateFormat($REGISTRATION_DATE) if( $REGISTRATION_DATE !~ m/^$/is );
		$VALID_DATE						= &dateFormat($VALID_DATE) if( $VALID_DATE !~ m/^$/is );
		$DATE_TARGET					= &dateFormat($DATE_TARGET) if( $DATE_TARGET !~ m/^$/is );
		
		
		if($Application eq "")
		{
			if($PLANNING_APPLICATION ne "")
			{
				$Application = $PLANNING_APPLICATION;
			}
		}		
				
		if($Address eq "")
		{
			if($SITE_LOCATION ne "")
			{
				$Address = $SITE_LOCATION;
			}
			$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
			$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
			$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
		}		
		
		if($Proposal eq "")
		{
			if($PROPOSAL ne "")
			{
				$Proposal = $PROPOSAL;
			}
		}	
			
		if($dateApplicationRegistered eq "")
		{
			if($REGISTRATION_DATE ne "")
			{
				$dateApplicationRegistered = $REGISTRATION_DATE;
			}
		}		
			
		if($dateApplicationReceived eq "")
		{
			if($RECEIVED_DATE ne "")
			{
				$dateApplicationReceived = $RECEIVED_DATE;
			}
		}				
			
		if($dateApplicationvalidated eq "")
		{
			if($VALID_DATE ne "")
			{
				$dateApplicationvalidated = $VALID_DATE;
			}
		}				
			
		if($agentName eq "")
		{
			if($AGENT_NAME ne "")
			{
				$agentName = $AGENT_NAME;
			}
		}		
		
		if($agentAddress eq "")
		{
			if($AGENT_ADDRESS ne "")
			{
				$agentAddress = $AGENT_ADDRESS;
			}
		}			
		
		if($applicantAddress eq "")
		{
			if($APPLICANT_ADDRESS ne "")
			{
				$applicantAddress = $APPLICANT_ADDRESS;
			}
		}				
			
		if($applicationType eq "")
		{
			if($APPLICATION_TYPE ne "")
			{
				$applicationType = $APPLICATION_TYPE;
			}
		}				
			
		if($applicationStatus eq "")
		{
			if($APPLICATION_STATUS ne "")
			{
				$applicationStatus = $APPLICATION_STATUS;
			}
		}	
		
		if($applicantName eq "")
		{
			if($APPLICANT_NAME ne "")
			{
				$applicantName = $APPLICANT_NAME;
			}
		}	
		
		if($targetDecdt eq "")
		{
			if($DATE_TARGET ne "")
			{
				$targetDecdt = $DATE_TARGET;
			}
		}	
		
		my ($gridContent,$gridResponse) = &getMechContent($fullAppPageLink,$CouncilApp,$councilCode);
		
		$GRIDREFERENCE = &getGridRef($gridContent);
		
		if($regexFile->{$councilCode}->{'DOC_URL'}!~m/^N\/A$/is)
		{
			$DOC_URL = &URLclean($1) if($gridContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
		}
		else
		{
			$documentURL = $fullAppPageLink;
		}
		
		if($documentURL eq "")
		{
			if($DOC_URL ne "")
			{
				if($DOC_URL!~m/^https?/is)
				{
					$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
				}
				$documentURL = $DOC_URL;
			}
			else
			{
				$documentURL = $fullAppPageLink;
			}
		
			$documentURL =~s/\'/\'\'/igs;
			$documentURL =~s/\=\s+/\=/igs;
			$documentURL=~s/amp;//igs;
			# print "documentURL==>$documentURL\n"; <STDIN>;
		}
		
		if($GRIDREFERENCE=~m/Easting\s+([^>]*?)\s*Northing\s+([^>]*)\s*/is)
		{
			$EASTING=$1;
			$NORTHING=$2;
			if(($EASTING ne "") && ($NORTHING ne ""))
			{
				$GRIDREFERENCE=$EASTING.":".$NORTHING;
				$GRIDREFERENCE=~s/^\s*\:\s*$//is;
			}
		}
		
		if($gridReference eq "")
		{
			if($GRIDREFERENCE ne "")
			{
				$gridReference = $GRIDREFERENCE;
			}
		}
		
		if($Easting eq "")
		{
			if($EASTING ne "")
			{
				$Easting = $EASTING;
			}
		}	
		
		if($Northing eq "")
		{
			if($NORTHING ne "")
			{
				$Northing = $NORTHING;
			}
		}	
			
			
		$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
		$Address=~s/\n+/ /gsi;
		$Proposal=~s/\n+/ /gsi;
		$applicationStatus=~s/\n+/ /gsi;
		$agentAddress=~s/\n+/ /gsi;
		$agentName=~s/\n+/ /gsi;
		$applicantAddress=~s/\n+/ /gsi;
		$applicantName=~s/\n+/ /gsi;
		$applicationType=~s/\n+/ /gsi;
			
		$insertQuery.="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$fullAppPageLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
		
		undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $fullAppPageLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
		
	}
	
		
	return($insertQuery,$actualScrappedCount,$deDup_Query);		
}

####
# URL Clean
####
sub URLclean()
{
	my $url=shift;
	
	$url=~s/amp\;//igs;
	$url=~s/\&nbsp\;/ /igs;
	$url=~s/\s\s+/ /igs;
	$url=~s/\'/\'\'/igs;
	$url=~s/\&\#xD\;\&\#xA\;//igs;	
	$url=~s/\s+/ /igs;
	$url=~s/\&\s*$//igs;
	$url=~s/^\.\.\/\.\.\/\.\.\//\//igs;
	$url=~s/^\s+|\s+$//igs;

	return $url; 
}

####
# To change the date format in valid,received,registed,target
####
sub dateFormat()
{
	my $date = shift;
	
	my $mDate = substr($date,0,10);

	$mDate =~ s/(\d{4})(-)(\d{2})(-)(\d{2})/$5\/$3\/$1/g;

	return $mDate; 
}

####
# Subroutine to get the Grid reference details from application
####
sub getGridRef()
{	
	my $appContent = shift;
	my $gridRef;
	
	if($appContent=~m/<div>\s*<span>Grid\s*Reference<\/span>([^>]*?)<\/div>\s*<\/li>/is)
	{
		$gridRef	= &htmlTagClean($1);
	}
	elsif ($appContent =~ m/<div>\s*<span>(?:Site|Location)\s*Co\s*ordinates<\/span>([^>]*?)<\/div>\s*<\/li>/is )
	{
		$gridRef	= &htmlTagClean($1); 
	}
	
	$gridRef=~s/^\s*Easting\s+Northing\s*$//is;
	
	return $gridRef;
}

####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $redisConnection, $Schedule_no, $CouncilApp) = @_;	
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
		
	my $actualScrappedCount = 0;
	my ($deDup_Query,$insertQuery);
	while($TabContent =~ m/$commonFileDetails->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
	{
		my $Applicationblock = $1;	
		
		my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime, $pkCode, $applicationLink);
		
	
		$Application = $1 if ( $Applicationblock =~ m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is );		
		$pkCode = $1 if ( $Applicationblock =~ m/$regexFile->{$councilCode}->{'PKCODE'}/is );			
		
		my $fullAppPageLink;
		if($councilCode=~m/^254$/is)
		{
			$fullAppPageLink="http://northgate.liverpool.gov.uk/PlanningExplorer17/Generic/StdDetails.aspx?PT=Planning%20Applications%20On-Line&TYPE=PL/PlanningPK.xml&PARAM0=$pkCode&XSLT=/PlanningExplorer17/SiteFiles/Skins/Liverpool_WIP/xslt/PL/PLDetails.xslt&FT=Planning%20Application%20Details&PUBLIC=Y&XMLSIDE=&DAURI=PLANNING";
		}
		else
		{
			$fullAppPageLink="StdDetails.aspx?PT=Planning Applications On-Line&TYPE=PL/PlanningPK.xml&PARAM0=$pkCode&XSLT=/Northgate/PlanningExplorer/SiteFiles/Skins/$councilName/xslt/PL/PLDetails.xslt&FT=Planning Application Details&PUBLIC=Y&XMLSIDE=/Northgate/PlanningExplorer/SiteFiles/Skins/$councilName/Menus/PL.xml&DAURI=PLANNING";
		}
			
		if($fullAppPageLink!~m/https?\:/is)
		{
			$fullAppPageLink=$commonFileDetails->{$councilCode}->{'FILTER_URL'}.$fullAppPageLink;
		}	
		else
		{
			$fullAppPageLink = $fullAppPageLink;
		}
		
		$fullAppPageLink=~s/\s+//igs;	
		
		if( $councilName =~ m/^Camden$|^Liverpool$/is )
		{
			$fullAppPageLink =~ s/PlanningExplorer\//PlanningExplorer17\//igs;
		}
		elsif( $councilName=~m/^Charnwood$|^Merton$/is )
		{
			$fullAppPageLink =~ s/PlanningExplorer\//PlanningExplorerAA\//igs;
		}
		elsif($councilName=~m/^East\s*Staffordshire$/is)
		{
			$fullAppPageLink=~s/\/East\s*Staffordshire\//\/EastStaffs\//igs;
		}
		elsif($councilName=~m/^Runnymede$/is)
		{
			$fullAppPageLink=~s/\/Runnymede\//\/Runnymede_AA\//igs;
		}	
		elsif($councilName=~m/^South Tyneside$/is)
		{
			$fullAppPageLink=~s/\/South\s*Tyneside\//\/Default_AA\//igs;
		}
		elsif($councilName=~m/^Tamworth$/is)
		{
			$fullAppPageLink =~ s/PlanningExplorer\//PlanningExplorerAA\//igs;
			$fullAppPageLink=~s/\/Tamworth\//\/NewTamworth\//igs;
		}
		elsif($councilName=~m/^Epping\s*Forest$/is)
		{
			$fullAppPageLink=~s/\/Epping\s+Forest\//\/EppingForest\//igs;
		}
		elsif($councilName=~m/^Conwy\s*County$/is)
		{
			$fullAppPageLink=~s/PlanningExplorer\//EnglishPlanningExplorer\//igs;
			$fullAppPageLink=~s/Conwy\s*County/Conwy/igs;
		}
		elsif($councilName=~m/^Blackburn\s*with\s*Darwen$/is)
		{
			$fullAppPageLink=~s/Blackburn\s*with\s*Darwen/Blackburn/igs;
		}
		elsif($councilName=~m/^North\s*Yorkshire\s*Moors$/is)
		{
			$fullAppPageLink=~s/North\s*Yorkshire\s*Moors/NorthYorkMoors/igs;
		}
		
		$fullAppPageLink =~ s/\&amp\;/\&/g;
		$fullAppPageLink =~s/\'/\'\'/igs;
		
		# print "After::$fullAppPageLink\n"; 
		# <STDIN>;
		if($redisConnection!~m/^OUT$/s)
		{
			if($redisConnection->exists("D_".$councilCode."_".$Application))	
			{
				print "Exists : $Application\n";	
				
				my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$Application\', \'$fullAppPageLink\', \'$category\', \'$Source\',\'$councilName\'),";
				$deDup_Query.=$deDupQuery;
				
				next;
			}
			elsif($Application ne "")
			{			
				$actualScrappedCount++;
				## Insert new application in redis server			
				$redisConnection->set("D_".$councilCode."_".$Application => $Application);
				print "Not Exists : $Application\n";
			}
		}
		
		my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_STATUS_NEW);
		$PLANNING_APPLICATION = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
		$PROPOSAL = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
		$APPLICATION_STATUS = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
		$DECISION_DATE = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
		$DECISION_STATUS = &htmlTagClean($1) if($Applicationblock=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
		
		
		$DECISION_DATE = &dateFormat($DECISION_DATE) if( $DECISION_DATE !~ m/^$/is );
			
		if($Application eq "")
		{
			if($PLANNING_APPLICATION ne "")
			{
				$Application = $PLANNING_APPLICATION;
			}
		}		
				
		if($Decision_Status eq "")
		{
			if($DECISION_STATUS ne "")
			{
				$Decision_Status=$DECISION_STATUS;
			}
		}		
		
		if($Date_Decision_Made eq "")
		{
			if($DECISION_DATE ne "")
			{
				$Date_Decision_Made=$DECISION_DATE;
			}
		}		
			
		if($Proposal eq "")
		{
			if($PROPOSAL ne "")
			{
				$Proposal = $PROPOSAL;
			}
		}		
		
		if($Application_Status eq "")
		{
			if($APPLICATION_STATUS ne "")
			{
				$Application_Status = $APPLICATION_STATUS;
			}
		}	
		
		if($applicationLink eq "")
		{
			if($fullAppPageLink ne "")
			{
				$applicationLink = $fullAppPageLink;
			}
		}	
			
		$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
		$Proposal=~s/\n+/ /gsi;
		$Application_Status=~s/\n+/ /gsi;
		$Decision_Status=~s/\n+/ /gsi;
		
		$insertQuery.="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
		
		undef $Application;  undef $Proposal;  undef $Application_Status;  undef $Decision_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
	}
	
	
		
	return($insertQuery,$actualScrappedCount,$deDup_Query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $councilApp = shift;
    my $currentCouncilCode = shift;
   

	$URL=~s/amp\;//igs;
	
	# if($currentCouncilCode =~ m/130/is)
	# {
		# $councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy
	# }
	# else
	# {
		# $councilApp->proxy(['http','https'], 'http://172.27.137.192:3128'); # Old Proxy	
	# }
	
	$councilApp->get($URL);
	
	my $appContent = $councilApp->content;
    my $appContentCode = $councilApp->status;
	
    return($appContent,$appContentCode);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;

    # Get search results using date ranges	
	print "\nFromDate==>$receivedFromDate\tToDate==>$receivedToDate\n";
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $cookie_jar, $commonFileDetails, $scriptStatus);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	if($receivedCouncilCode=~m/^110$/is)
	{
		eval
		{
			$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
			
			$receivedCouncilApp-> submit_form(
				form_number => $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'},
				fields      => 
							{
								$commonFileDetails->{$receivedCouncilCode}->{'SELECT_NME'}  => $commonFileDetails->{$receivedCouncilCode}->{'SELECT_VAL'},
								$commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_NME'}  => $commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_VAL'},
								$commonFileDetails->{$receivedCouncilCode}->{'FORM_START_ID'}  => $receivedFromDate,
								$commonFileDetails->{$receivedCouncilCode}->{'FORM_END_ID'}    => $receivedToDate,
								$commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'}  => $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_VAL'},
							},	
				);
					
		};
	}
	else
	{
		eval
		{
			$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
			
			$receivedCouncilApp-> submit_form(
				form_number => $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'},
				fields      => 
							{
								$commonFileDetails->{$receivedCouncilCode}->{'SELECT_NME'}  => $commonFileDetails->{$receivedCouncilCode}->{'SELECT_VAL'},
								$commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_NME'}  => $commonFileDetails->{$receivedCouncilCode}->{'RADIO_BTN_VAL'},
								$commonFileDetails->{$receivedCouncilCode}->{'FORM_START_ID'}  => $receivedFromDate,
								$commonFileDetails->{$receivedCouncilCode}->{'FORM_END_ID'}    => $receivedToDate,
								$commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'}  => $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_VAL'},
							},				
				button      => $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'}
				);
					
		};
	}	
	
    $councilAppResponse = $receivedCouncilApp->content;
	
    $scriptStatus = $receivedCouncilApp->status;
		
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=3))
	{
		$rerunCount++;
		goto Loop;
	}
	
	# open(PP, ">a.html");
	# print PP "$councilAppResponse\n";
	# close(PP);
		
    return ($councilAppResponse,$scriptStatus);
}

####
# Method to POST the pagenation settings for Online Councils
####

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $pageContent) = @_;
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my $filterURL = $commonFileDetails->{$receivedCouncilCode}->{'FILTER_URL'};
	if($filterURL!~m/N\/A/is)
	{
		if($pageContent=~m/$regexFile->{'MainRegexControl'}->{'NEXT_PAGE_CONTENT_CHECK_RX'}/is)
		{
			my $nextPageURLinXML=uri_unescape($1);
			
			# print "BEFORE::$nextPageURLinXML\n"; <STDIN>;
			
			if($nextPageURLinXML!~m/^http/is)
			{
				$nextPageURLinXML = URI::URL->new($nextPageURLinXML)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
			}
			
			print "AFTER::$nextPageURLinXML\n";
			# <STDIN>;
			
			$receivedCouncilApp->get($nextPageURLinXML);
			my $responseContent = $receivedCouncilApp->content;		
			my $responseContentStatus = $receivedCouncilApp->status;		

			$commonContent = $responseContent;
		}
	}

	return($commonContent);
}

####
# Search using Application number in website
####
sub searchAppNumLogic()
{
	my ($receivedCouncilCode, $applicationNumber, $receivedCouncilApp, $category) = @_;
		

	$receivedCouncilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	$receivedCouncilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	
	my ($searchResponseContent, $scriptStatus, $rerunCount, $currentPageURL, $commonFileDetails);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}	
	
	Loop:
	eval
	{	
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});	
		
		if($receivedCouncilCode=~m/^(110)$/is)
		{
			$receivedCouncilApp-> submit_form(
					form_number => $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'},
					fields      => 
								{
									$commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'}  => $applicationNumber,
								},	
					);
		}
		elsif($receivedCouncilCode=~m/^(23|393|489)$/is)
		{
			$receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'});
			$receivedCouncilApp->set_fields( $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'} => $applicationNumber );
			$receivedCouncilApp->click($commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'});
		}
		else
		{
			$receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'});
			$receivedCouncilApp->set_fields( $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'} => $applicationNumber );
			$receivedCouncilApp->click();
		}
	};
	
	
    $searchResponseContent = $receivedCouncilApp->content;	
	$currentPageURL = $receivedCouncilApp->uri();	
    $scriptStatus = $receivedCouncilApp->status;
	print "scriptStatus==>$scriptStatus\n";

	# open(PP,">a.html");
	# print PP "$searchResponseContent";
	# close(PP);
	# exit;
	
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=2))
	{
		$rerunCount++;
		goto Loop;
	}
	
	return($searchResponseContent, $currentPageURL, $scriptStatus)
}


####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	
	return($data2Clean);
}
1;