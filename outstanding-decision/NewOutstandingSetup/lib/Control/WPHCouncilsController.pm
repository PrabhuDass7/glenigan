package WPHCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use IO::Socket::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/WPH');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/WPH_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/WPH_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/WPH_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/WPH_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');



###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "WPH_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "WPH_Decision";
	}
	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
		
		exit;
	}
		
	
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
	$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy	
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');
	
	$responseContent = $councilApp->get($commonFileDetails->{$currentCouncilCode}->{'URL'});
	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $REGISTRATION_DATE_NEW, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $APPLICANT_NAME_NEW, $AGENT_NAME, $AGENT_NAME_NEW, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL);
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'FULL_DESCRIPTION'}/is);
	$REGISTRATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
	$REGISTRATION_DATE_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE_NEW'}/is);
	$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	$VALID_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
	$APPLICATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_DATE'}/is);
	$VALIDATED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALIDATED_DATE'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$APPLICATION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS_NEW'}/is);
	if($councilCode=~m/^420$/is)
	{
		$APPLICANT_NAME = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
		$AGENT_NAME = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
	}
	else
	{
		$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
		$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
	}
	$APPLICANT_NAME_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME_NEW'}/is);
	$AGENT_NAME_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME_NEW'}/is);
	$APPLICANT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
	$AGENT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
	$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
			if($councilCode=~m/479/is)
			{
				$Application=~s/\s*\(\s*[^<]*?\s*\)\s*$//is;
			}
		}
	}		
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
		elsif($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
	}	
		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
		elsif($REGISTRATION_DATE_NEW ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE_NEW;
		}
	}		
	# print "dateApplicationRegistered==>$dateApplicationRegistered\n";
		
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}				
	# print "dateApplicationReceived==>$dateApplicationReceived\n";	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
		elsif($APPLICATION_DATE ne "")
		{
			$dateApplicationvalidated = $APPLICATION_DATE;
		}
		elsif($VALIDATED_DATE ne "")
		{
			$dateApplicationvalidated = $VALIDATED_DATE;
		}
	}	
	# print "dateApplicationvalidated==>$dateApplicationvalidated\n";			<STDIN>;	
		
	if($agentName eq "")
	{
		if($councilCode=~m/^420$/is)
		{
			if($AGENT_NAME ne "")
			{
				my ($title,$name,$company);
				$title=&htmlTagClean($1) if($AGENT_NAME=~m/<P>\s*<strong>\s*Title\:?\s*<\/strong>\:?([^<]*?)<\/P>/is);
				$name=&htmlTagClean($1) if($AGENT_NAME=~m/<P>\s*<strong>\s*Name\:?\s*<\/strong>\:?([^<]*?)<\/P>/is);
				$company=&htmlTagClean($1) if($AGENT_NAME=~m/<P>\s*<strong>\s*Company\:?\s*<\/strong>\:?([^<]*?)<\/P>/is);
				$agentName=$title." ".$name;
				$agentCompanyName=$company;
			}
		}
		else
		{
			if($AGENT_NAME ne "")
			{
				$agentName = $AGENT_NAME;
			}
			elsif($AGENT_NAME_NEW ne "")
			{
				$agentName = $AGENT_NAME_NEW;
			}
		}
	}		

	if($agentCompanyName eq "")
	{
		$agentCompanyName = &htmlTagClean($1) if($TabContent=~m/AgtContactName\">\s*Company\s*Contact\s*Name\s*\:?\s*<[^>]*?>\s*(?:<p\s*class\=\"fieldset_data\">\s*|\s*)([\w\W]*?)\s*(?:<\/p>\s*|\s*)<\/div>/is);	
	}
		
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}				
		
	if($applicationStatus eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$applicationStatus = $APPLICATION_STATUS;
		}
		elsif($APPLICATION_STATUS_NEW ne "")
		{
			$applicationStatus = $APPLICATION_STATUS_NEW;
		}
	}	
	
	if($applicantName eq "")
	{
		if($councilCode=~m/^420$/is)
		{
			if($APPLICANT_NAME ne "")
			{
				my ($title,$name);
				$title=&htmlTagClean($1) if($APPLICANT_NAME=~m/<P>\s*<strong>\s*Title\:?\s*<\/strong>\:?([^<]*?)<\/P>/is);
				$name=&htmlTagClean($1) if($APPLICANT_NAME=~m/<P>\s*<strong>\s*Name\:?\s*<\/strong>\:?([^<]*?)<\/P>/is);
				if($name eq "")
				{
					if($APPLICANT_NAME_NEW ne "")
					{
						$applicantName = $APPLICANT_NAME_NEW;
					}
				}
				else				
				{
					$applicantName=$title." ".$name;
				}
			}
		}
		else
		{
			if($APPLICANT_NAME ne "")
			{
				$applicantName = $APPLICANT_NAME;
			}
			elsif($APPLICANT_NAME_NEW ne "")
			{
				$applicantName = $APPLICANT_NAME_NEW;
			}
		}
	}	
	
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}	
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}	
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			$documentURL = $applicationLink;
		}
		$documentURL =~s/\&amp\;/\&/igs;
		$documentURL =~s/\'/\'\'/igs;
	}
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_STATUS_NEW);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'FULL_DESCRIPTION'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$APPLICATION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS_NEW'}/is);
		
	if($councilCode=~m/^(96|138|275|337|355|360|420)$/is)
	{	
		my ($DECISION_CNT,$DECISION_LINK,$DECISION_CNT_STATUS);
		$DECISION_LINK = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_LINK'}/is);
		if($DECISION_LINK eq "")
		{
			$DECISION_LINK = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_LINK_NEW'}/is);
		}
	
		if($DECISION_LINK!~m/^https?/is)
		{
			$DECISION_LINK = URI::URL->new($DECISION_LINK)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
		}
		# print "DecisionLink==>$DECISION_LINK\n";
					
		($DECISION_CNT,$DECISION_CNT_STATUS) = &getMechContent($DECISION_LINK,$councilApp,$councilCode) if($DECISION_LINK ne "");	
		
		$DECISION_DATE = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
		$DECISION_STATUS = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
		$DECISION_STATUS_NEW = &htmlTagClean($1) if($DECISION_CNT=~m/$regexFile->{$councilCode}->{'DECISION_STATUS_NEW'}/is);
	}
	else
	{		
		$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
		$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
		$DECISION_STATUS_NEW = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS_NEW'}/is);
	}
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
			if($councilCode=~m/479/is)
			{
				$Application=~s/\s*\(\s*[^<]*?\s*\)\s*$//is;
			}
		}
	}		
			
	if($Decision_Status eq "")
	{
		if($DECISION_STATUS ne "")
		{
			$Decision_Status=$DECISION_STATUS;
		}
		elsif($DECISION_STATUS_NEW ne "")
		{
			$Decision_Status=$DECISION_STATUS_NEW;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
		}
	}		
		
	if($Proposal eq "")
	{
		if($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
		elsif($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		$Proposal=~s/\s*Show\s*full\s*description$//gsi;
	}		
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
		elsif($APPLICATION_STATUS_NEW ne "")
		{
			$Application_Status = $APPLICATION_STATUS_NEW;
		}
	}	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $Application;  undef $Proposal;  undef $Application_Status;   undef $Decision_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
    my $currentCouncilCode = shift;
   

	$URL=~s/amp\;//igs;
	
	# if($currentCouncilCode=~m/^(479)$/is)
	# {
		# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy
	# }
	
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
    return($con,$code);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;

    # Get search results using date ranges	
	print "\nFromDate==>$receivedFromDate\tToDate==>$receivedToDate\n";
	
	my $rerunCount=0;
	Loop:
    my ($councilAppResponse, $cookie_jar, $commonFileDetails, $scriptStatus);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	eval
	{
		$councilAppResponse = $receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});

		$receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'}); 
		$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'FORM_START_ID'}, $receivedFromDate );
		$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'FORM_END_ID'}, $receivedToDate );
		$receivedCouncilApp->click();
	};
	
	
    $councilAppResponse = $receivedCouncilApp->content;
	
    $scriptStatus = $receivedCouncilApp->status;
	
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=3))
	{
		$rerunCount++;
		goto Loop;
	}
		
    return ($councilAppResponse,$scriptStatus);
}

####
# Method to POST the pagenation settings for Online Councils
####

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $pageContent) = @_;
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	$commonContent .= $pageContent;
	
	my $filterURL = $commonFileDetails->{$receivedCouncilCode}->{'FILTER_URL'};
	if($filterURL!~m/N\/A/is)
	{
		if($pageContent=~m/$regexFile->{'MainRegexControl'}->{'NEXT_PAGE_CONTENT_CHECK_RX'}/is)
		{
			my $content=$1;
			while($content=~m/$regexFile->{'MainRegexControl'}->{'NEXT_PAGE_LINK_RX'}/igs)
			{
				my $link=$1;
				my $pageNo=$2;
				
				my $nextPageLink=$filterURL.$link;
				
				$nextPageLink=~s/amp\;//igs;
				
				$receivedCouncilApp->get($nextPageLink);
				my $responseContent = $receivedCouncilApp->content;		
	
				$commonContent .= $responseContent;
			}
		}
	}

	return($commonContent);
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
		
		if($councilCode=~m/479/is)
		{
			$applicationNumber=~s/\s*\(\s*[^<]*?\s*\)\s*$//is;
		}
	}
	
	# print "Current Application number::$applicationNumber\n";
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		$logName = $1 if($receivedApplicationUrl=~m/ApnID\=([^\&]*?)\&/is);
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}


####
# Search using Application number in website
####
sub searchAppNumLogic()
{
	my ($receivedCouncilCode, $applicationNumber, $receivedCouncilApp, $category) = @_;
		

	$receivedCouncilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	
	
	my ($searchResponseContent, $scriptStatus, $rerunCount, $currentPageURL, $commonFileDetails);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	Loop:
	eval
	{	
		$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});	
		
		$receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'});
		$receivedCouncilApp->set_fields( $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'} => $applicationNumber );
		$receivedCouncilApp->click();
	};
	
	
    $searchResponseContent = $receivedCouncilApp->content;	
	$currentPageURL = $receivedCouncilApp->uri();	
    $scriptStatus = $receivedCouncilApp->status;
	print "scriptStatus==>$scriptStatus\n";
	
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=2))
	{
		$rerunCount++;
		goto Loop;
	}
	
	return($searchResponseContent, $currentPageURL, $scriptStatus)
}

####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<\/br>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	$data2Clean=~s/^\s+//igs;
	$data2Clean=~s/\s+$//igs;
	
	return($data2Clean);
}
1;