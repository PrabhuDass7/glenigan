package CouncilsScrapperCore;
use strict;
use DateTime;
use DateTime::Duration;
use utf8;
use POSIX;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use IO::Socket::SSL qw();
use URI::URL;
use File::Path qw/make_path/;

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;
# print "BasePath==>$basePath\n"; <STDIN>;

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $libDirectory = ($basePath.'/lib/Core');

# print "libDirectory==>$libDirectory\n"; 
# print "ConfigDirectory==>$ConfigDirectory\n";

require ($libDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Config::Tiny
####

my ($controlFile, $councilsList) = Config::Tiny->new();

# Establish connection with DB server
my $dbHostPlanning = &Glenigan_DB::screenscrapperDB();
my $dbHostDecision = &Glenigan_DB::gleniganDB();

####
# Read INI files from the objects "$councilsList" and "$controlFile"
# These files contains - Details of councils and control properties for date range clculation repsectively
####
$controlFile = Config::Tiny->read($ConfigDirectory.'/Core_Config.ini' );
$councilsList = Config::Tiny->read($ConfigDirectory.'/Input_Council_Code.ini');

my $COVERAGE 		    = $controlFile->{'gcsControl'}->{'COVERAGE'};
my $DAYS_IN_A_WEEK	 	= $controlFile->{'gcsControl'}->{'DAYS_IN_A_WEEK'};
my $scrapeStatusTable	= $controlFile->{'databaseTableName'}->{'scrapeStatus'};
my $scrapeDeDupStatus	= $controlFile->{'databaseTableName'}->{'scrapeDeDupStatus'};
my $scheduleStatusTable	= $controlFile->{'databaseTableName'}->{'scheduleStatus'};
my $planningStagingTable= $controlFile->{'databaseTableName'}->{'planningStaging'};
my $planningLiveTable	= $controlFile->{'databaseTableName'}->{'planningLive'};
my $decisionStagingTable= $controlFile->{'databaseTableName'}->{'decisionStaging'};
my $decisionLiveTable	= $controlFile->{'databaseTableName'}->{'decisionLive'};


####
# Calculates the default GCS schedule for the current Date
####

sub defaultGcsRange
{
	my ($currentToDate, $onDemand) = @_;
	my %defaultRangeCalc = &gcsRangeCalc($DAYS_IN_A_WEEK, $COVERAGE/$DAYS_IN_A_WEEK, $currentToDate, $onDemand);
	return(%defaultRangeCalc);
}

####
# Calculates schedule number based on current hour of the script execution
####

sub scheduleNumber
{	
	####
	# Get local time
	####
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = @_;
	
	my $scheduleNo;
	if($hour < 12)
	{
		$scheduleNo=1;
	}
	else
	{
		$scheduleNo=2;
	}
	
	return($scheduleNo);
}

####
# Insert status during the begining of an script for a given GCS date range against the councils in Dashboard table in DB as "Running"
####
sub scrapeStatusInsertion()
{
	my ($scheduleNo,$scheduleDate,$rangeName,$councilCode,$scriptStatus,$scriptTypeName,$councilName) = @_;
	
	my $statusQueryInsertion = "insert into $scrapeStatusTable (SCHEDULE_NO, SCHEDULE_DATE, GCS_NAME, COUNCIL_CODE, STATUS, PLANNING_OR_DECISION, COUNCIL_NAME) values (\'$scheduleNo\',\'$scheduleDate\',\'$rangeName\',\'$councilCode\',\'$scriptStatus\',\'$scriptTypeName\',\'$councilName\')";
	
	# print "\n$statusQueryInsertion\n";
	
	if(($councilName ne "") && ($rangeName ne "") && ($scriptStatus ne ""))
	{
		print "Council Status Inserted in DataBase\n";
		
		&Glenigan_DB::DB_Insert_Planning($dbHostPlanning,$statusQueryInsertion);
	}
	else
	{
		print "Empty query values. Please check table status insertion query!\n";
	}
}


####
# This module is used to insert details of an council planning or decision of an application from the website into the database.
####
sub scrapeDetailsInsertion()
{
	my ($insertQuery,$category) = @_;
		
	$insertQuery=~s/\,\s*$//igs;
		
	if($category eq "Planning")
	{		
		my $insertDetailsQuery = "insert into $planningStagingTable (Council_Code, Council_Name, Address, Date_Application_Received, Application, Date_Application_Registered, Date_Application_validated, Proposal, Application_Status, Actual_Decision_Level, Agent_Address, Agent_Company_Name, Agent_Name, Agent_Telephone, Applicant_Address, Applicant_name, Application_type, Agent_Email, Agent_Telephone_1, Agent_mobile, Agent_Fax, agent_contact_details, Actual_Committee_Date, Actual_Committee_or_Panel_Date, Advertisement_Expiry_Date, Agreed_Expiry_Date, Application_Expiry_Deadline, TargetDec_dt, Temporary_Permission_Expiry_Date, PAGE_URL, Document_Url, No_of_Documents,Source, Schedule_Date, Grid_Reference, Easting, Northing) values ".$insertQuery;
		# print "\n$insertDetailsQuery\n";

		if($insertDetailsQuery!~m/values\s*$/is)
		{
			print "\nCouncil Application Details records Inserted in Planning(SCREENSCRAPPER) DataBase\n";
			&Glenigan_DB::DB_Insert_Planning($dbHostPlanning,$insertDetailsQuery);
		}
		else
		{
			print "Empty query values. Please check planning details insertion query!\n";
		}
	}
	elsif($category eq "Decision")
	{			
		my $insertDetailsQuery = "insert into $decisionStagingTable (Application, ProPosal, Decision_Status, Date_Decision_Made, Date_Decision_Dispatched, Council_code, Page_URL, Source, Application_Status, Imported_Date) values ".$insertQuery;
		# print "\n$insertDetailsQuery\n";
		
		if($insertDetailsQuery!~m/values\s*$/is)
		{
			print "\nCouncil Application Details records Inserted in Decision(GLENIGAN) DataBase\n";
			&Glenigan_DB::DB_Insert_Decision($dbHostDecision,$insertDetailsQuery);
		}
		else
		{
			print "Empty query values. Please check decision details insertion query!\n";
		}
	}
}

####
# Number of results fetched for the given GCS date range for the council will be insert into dashboard table in DB.
####
sub dashboardInsertion()
{
	my ($councilCode, $councilName, $Start_Date_Time, $rangeName, $fromDate, $toDate, $totalLinksCount, $actualScrappedCount, $pingStatus, $rawFileName, $scheduleDate) = @_;
	
	my $From_Date_TO_DB = $fromDate;
	my $To_Date_TO_DB = $toDate;	
	
	$From_Date_TO_DB=~s/T/ /s;
	$To_Date_TO_DB=~s/T/ /s;
		
	# print "$From_Date_TO_DB\t$To_Date_TO_DB\n";	
	
	my $reportQuery= "INSERT INTO $scheduleStatusTable (COUNCIL_CD, COUNCIL_NAME, SCHEDULE_DATE, SOURCE, DATE_RANGE_FROM, DATE_RANGE_TO, RESULTS_FOUND, APPLICATIONS_SCRAPPED, LOG_ERROR, SNAPSHOT_FILE_NAME, SCHEDULE_END_DATE) VALUES(\'$councilCode\', \'$councilName\', \'$Start_Date_Time\', \'$rangeName\', \'$From_Date_TO_DB\', \'$To_Date_TO_DB\', \'$totalLinksCount\', \'$actualScrappedCount\', \'$pingStatus\', \'$rawFileName\', \'$scheduleDate\')";
	
	# print "\n$reportQuery\n";
	
	if(($scheduleStatusTable ne "") && ($rawFileName ne "") && ($From_Date_TO_DB ne "") && ($To_Date_TO_DB ne ""))
	{
		print "Council Application records count Inserted Dashboard table in DataBase\n";
		
		&Glenigan_DB::DB_Insert_Planning($dbHostPlanning,$reportQuery);
	}
	else
	{
		print "Empty query values. Please check dashbord insertion query!\n";
	}
}


####
# Duplicates found for the given GCS date range shall be inserted for that council in a dedup table called Redis Table in database.
####
sub deDupInsertion()
{
	my ($deDupInsertion) = @_;
		
	$deDupInsertion=~s/\,\s*$//igs;
	

	my $duplicatequery="insert into $scrapeDeDupStatus (SCHEDULE_NO, SCHEDULE_DATE, COUNCIL_CODE, APPLICATION_NO, URL, PLANNING_OR_DECISION, GCS_NAME, COUNCIL_NAME) values ".$deDupInsertion;
	
	if($duplicatequery!~m/values\s*$/is)
	{
		# print "\n$duplicatequery\n";
		
		print "Council Application duplicate Inserted Redis table in DataBase\n";
		
		&Glenigan_DB::DB_Insert_Planning($dbHostPlanning,$duplicatequery);
	}
	else
	{
		print "Empty query values. Please check dedup table insertion query!\n";
	}
	
}

####
# Update status during the ending of the script or in between when interrupted for a given GCS date range against the councils in Dashboard database such as "Completed" 
# and/or "Terminated"
####
sub scrapeStatusUpdation()
{
	my ($scheduleNo,$scheduleDate,$searchResultCount,$councilCode,$scriptStatus,$scriptTypeName,$councilName) = @_;
		
	my $statusQuery="update $scrapeStatusTable set Status=\'$scriptStatus\', Searched_Count=\'$searchResultCount\' where COUNCIL_CODE=\'$councilCode\' and Schedule_no=\'$scheduleNo\' and schedule_date = \'$scheduleDate\' and Status='Running' and planning_or_decision = \'$scriptTypeName\'";
	
	# print "\n$statusQuery\n";
	
	if(($searchResultCount ne "") && ($scriptStatus ne ""))
	{
		print "Council Application Status Updated table in DataBase\n";
	
		&Glenigan_DB::DB_Insert_Planning($dbHostPlanning,$statusQuery);
	}
	else
	{
		print "Empty query values. Please check status table updation query!\n";
	}
}



####
# Calculates and provides gcs schedule for the given $weekDaysCount and $gcsSchedulesCount
####

sub gcsRangeCalc
{
	my ($weekDaysCount, $gcsSchedulesCount, $startDate, $onDemand) = @_;
	my %gcsRanges;
	print "weekDaysCount::$weekDaysCount<=>gcsSchedulesCount::$gcsSchedulesCount<=>StartDate::$startDate\n";
	my ($calcFromDate, $calcToDate, $gcsRangeName);
	for(my $k = 1; $k <= $gcsSchedulesCount; $k++)
	{
		$k=sprintf("%03d",$k);
		if($onDemand ne "")
		{
			$gcsRangeName="ONDEMAND".$k;
		}
		else
		{
			$gcsRangeName="GCS".$k;
		}
		
		if($k == 1)
		{
			$calcToDate = $startDate;
			$calcFromDate = $calcToDate - DateTime::Duration->new( days => $weekDaysCount );
			print "GCS::$gcsRangeName=>>FromDate::$calcFromDate<=>ToDate::$calcToDate\n";
		}
		else
		{
			$calcToDate = $calcFromDate;
			$calcFromDate = $calcToDate - DateTime::Duration->new( days => $weekDaysCount );
			print "GCS::$gcsRangeName=>>FromDate::$calcFromDate<=>ToDate::$calcToDate\n";
		}
        push ( @{ $gcsRanges{$gcsRangeName}}, $calcFromDate, $calcToDate);
	}
	
	return(%gcsRanges);
}


###
# Calculates the "Divider value" for the given search result
# and returns the calculated value
###

sub dividerCalc
{
	my ($divCount) = @_;
	my $dividerVal;
	
	# print "divCount==>$divCount\n";
	
	if($divCount == 1)
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'};
	}
	elsif ($divCount == 2)
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'}*$controlFile->{'gcsControl'}->{'INCREMENTAL'};
	}
	elsif ($divCount == 3)
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'}*$controlFile->{'gcsControl'}->{'INCREMENTAL'}+2;
	}
	else
	{
		$dividerVal = $controlFile->{'gcsControl'}->{'DIVIDER'}*$controlFile->{'gcsControl'}->{'INCREMENTAL'}*2;
	}
	# print "dividerVal==>$dividerVal\n";
	
	return ($dividerVal);
}


####
# Calculates the "No. of days for week range" from the obtained divider value
# and returns the calculated value
####

sub weekDaysCalc 
{
	my ($divCal) = @_;
	my $weekDaysVal;
	
	if($DAYS_IN_A_WEEK/$divCal>=$controlFile->{'gcsControl'}->{'MIN_NO_WEEK_DAYS'})
	{
		# $weekDaysVal = ceil($DAYS_IN_A_WEEK/$divCal);
		if($divCal == 4)
		{
			$weekDaysVal = floor($DAYS_IN_A_WEEK/2.5);
		}
		elsif($divCal == 6)
		{
			$weekDaysVal = ($DAYS_IN_A_WEEK/4);
		}
		else
		{
			$weekDaysVal = floor($DAYS_IN_A_WEEK/$divCal);
		}
	}
	else
	{
		$weekDaysVal = $controlFile->{'gcsControl'}->{'MIN_NO_WEEK_DAYS'};
	}
	print "WeekDaysVal==>$weekDaysVal\n";
	return ($weekDaysVal);
}

####
# Subroutine to set date ranges to GCS schedule
####

sub setDateRanges()
{
    my ($receivedFromDate, $receivedToDate) = @_;
    my ($setFromDate, $setToDate);
    $setFromDate = $receivedFromDate;
    $setToDate = $receivedToDate;
	return($setFromDate, $setToDate);
}

####
# Subroutine to get the Sub Ranges calualted for an GCS scheduled date range
####

sub subRangesCalcMethod
{
    my ($receivedDivCounter, $receivedToDate, $onDemand) = @_;

	my $divCalulator = &dividerCalc($receivedDivCounter);
    my $weekDaysCalculator = &weekDaysCalc($divCalulator);
	# my %subRanges = &gcsRangeCalc($weekDaysCalculator, ceil($DAYS_IN_A_WEEK/$weekDaysCalculator), $receivedToDate);
	my %subRanges = &gcsRangeCalc($weekDaysCalculator, floor($DAYS_IN_A_WEEK/$weekDaysCalculator), $receivedToDate, $onDemand);
	
	return(%subRanges);
}


####
# Method to write logs status
####

sub writtingToLog()
{
	my($status, $rangeName, $councilCode, $localTime, $logsDirectoryPath, $logsDataDirectoryPath,$category) = @_;
	
	my $logFileName = $category.'_Time_Logs';
	my $statusLog = 'TriggredStatusLog';	
	my $logLocation = $logsDirectoryPath.'/'.$statusLog.'/'.$todaysDate;
	my $logDataLocation = $logsDataDirectoryPath.'/'.$statusLog.'/'.$todaysDate;
	
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	unless ( -d $logDataLocation )
	{
		make_path($logDataLocation);
	}

	
	open(TIME,">>$logLocation/$logFileName.txt");
	print TIME "Status:$status\tRangeName:$rangeName\tCouncilCode:$councilCode\tTime:$localTime\n";
	close TIME;
	
	
	open(TIME,">>$logDataLocation/$logFileName.html");
	print TIME "Status:$status\tRangeName:$rangeName\tCouncilCode:$councilCode\tTime:$localTime\n";
	close TIME;
	
	print "\nStatus Logs inserted..\n";
}


####
# Method to write QC logs
####

sub writtingToSearchResultLog()
{
	my($rawFileName, $responseContent, $QcLogsDirectoryPath,$category,$statusType) = @_;
	
	my $qcLogLocation = $QcLogsDirectoryPath.'/'.$statusType.'/'.$todaysDate;
	
	unless ( -d $qcLogLocation )
	{
		make_path($qcLogLocation);
	}
	
	open(TIME,">>$qcLogLocation/$rawFileName");
	print TIME "$responseContent\n";
	close TIME;
	
	print "\nQC Logs Content inserted..\n";
}


####
# Method to search page ping status/response
####

sub writtingToStatus()
{
	my($rangeName, $councilCode, $councilName, $responseStatus, $logsDirectoryPath, $category) = @_;
	
	my $pingStatus = $rangeName.'_Ping_Status';
	my $searchFails = "SearchFailLogs";
	my $logLocation = $logsDirectoryPath.'/'.$searchFails.'/'.$todaysDate;
	
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	
	open(TIME,">>$logLocation/$pingStatus.html");	
	print TIME "RangeName:$rangeName\tCouncilCode:$councilCode\tCategory:$category\tCouncilName:$councilName\tPingStatus:$responseStatus\n";			
	close TIME;
	
	print "\nPing Status inserted..\n";
}
1;