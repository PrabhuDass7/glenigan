package Glenigan_DB;
### Package Name ####
use strict;
use DBI;
use DBD::ODBC;
use MIME::Lite;
use Date::Calc ("Add_Delta_Days","Today","Date_to_Text","Today_and_Now");
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use File::Path qw/make_path/;

require Exporter;
my @ISA = qw(Exporter);
my @EXPORT = qw(ImageDownload);


my ($year, $mon, $day) = Today();
my $today = [Today_and_Now()];
my $todayDateTime = sprintf("%s %02d:%02d:%02d", Date_to_Text(@{$today}[0..2]), @{$today}[3..5]);

my $todayDate = sprintf "%04d%02d%02d", $year, $mon, $day;


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n"; <STDIN>;

my $logsDirectory = ($basePath.'/logs');

my $logLocation = $logsDirectory.'/FailedQuery/'.$todayDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

my $dbFailedLogLocation = $logsDirectory.'/dbConnFailedQuery/'.$todayDate;

unless ( -d $dbFailedLogLocation )
{
	make_path($dbFailedLogLocation);
}


###### DB Connection ####
sub screenscrapperDB()
{	
	my $dbh;	
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect:
	if($dbh = DBI->connect("dbi:Sybase:server=10.101.53.25;database=SCREENSCRAPPER", 'User2', 'Merit456'))
	{
		print "\nSCREENSCRAPPER SERVER CONNECTED\n";
		$Connection_Flag=1;
		if($Recon_Flag_Main > 1)
		{	
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Success$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
		}
	}
	else
	{
		print "\nSCREENSCRAPPER SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag++;
			goto Reconnect;
		}	
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Planning.txt");
			print ERR "Failed_$Recon_Flag_Main==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag_Main++;
			goto Reconnect;
		}	
	}
	
	return $dbh;
}

sub gleniganDB()
{
	my $dbh;
	my $Connection_Flag=0;
	my $Recon_Flag=1;
	my $Recon_Flag_Main=1;
	
	Reconnect1:
	if($dbh = DBI->connect("dbi:Sybase:server=10.101.53.25;database=GLENIGAN", 'User2', 'Merit456'))
	{
		print "\nGLENIGAN SERVER CONNECTED\n";
		$Connection_Flag=1;
		if($Recon_Flag_Main > 1)
		{	
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Decision.txt");
			print ERR "Success$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
		}
	}
	else
	{
		print "\nGLENIGAN SERVER FAILED\n";
		if($Recon_Flag<=3)
		{
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Decision.txt");
			print ERR "Failed$Recon_Flag==>".$todayDateTime."\n";
			close ERR;
			$Recon_Flag++;
			goto Reconnect1;
		}
		elsif($Recon_Flag_Main<=2)
		{
			sleep(300);
			$Recon_Flag=1;
			$Recon_Flag_Main++;
			
			open(ERR,">>$dbFailedLogLocation/reconn_withsleep_Decision.txt");
			print ERR "Failed_$Recon_Flag_Main==>".$todayDateTime."\n";
			close ERR;
			
			goto Reconnect1;
		}		
	}

	return $dbh;
}

sub DB_Insert_Planning()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);

	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&screenscrapperDB();
	}
}

sub DB_Insert_Decision()
{
	my $dbh 	= shift;
	my $Query	= shift;

	my $sth = $dbh->prepare($Query);

	if($sth->execute())
	{
		print "Executed\n";
	}
	else
	{
		print "QUERY:: $Query\n";
		
		open(ERR,">>$logLocation/Failed_Query.txt");
		print ERR $Query."\n";
		close ERR;
		
		$dbh=&gleniganDB();
	}
}

sub retrieveOnDemondInput_CC_Planning()
{
	my $dbh = shift;

	my $query= "select council_code, gcs_name, start_date, end_date,id from TBL_ON_DEMAND_DOWNLOAD where download_type = 'application scrapping' and (status = \'\' or status is NULL)";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@councilCode,@gcsName,@fromDate,@toDate,@id);	
	while(my @record = $sth->fetchrow)
	{
		push(@councilCode,($record[0]));
		push(@gcsName,($record[1]));
		push(@fromDate,($record[2]));
		push(@toDate,($record[3]));		
		push(@id,($record[4]));		
	}
	$sth->finish();
	
	return (\@councilCode,\@gcsName,\@fromDate,\@toDate,\@id);
}

sub retrieveOnDemondInput_CC_Decision()
{
	my $dbh = shift;

	my $query= "select council_code, gcs_name, start_date, end_date,id from TBL_ON_DEMAND_DOWNLOAD where download_type = 'decision scrapping' and (status = \'\' or status is NULL)";
	my $sth = $dbh->prepare($query);
	$sth->execute();
	
	my (@councilCode,@gcsName,@fromDate,@toDate,@id);	
	while(my @record = $sth->fetchrow)
	{
		push(@councilCode,($record[0]));
		push(@gcsName,($record[1]));
		push(@fromDate,($record[2]));
		push(@toDate,($record[3]));		
		push(@id,($record[4]));		
	}
	$sth->finish();
	
	return (\@councilCode,\@gcsName,\@fromDate,\@toDate,\@id);
}

sub onDemandUpdate()
{
	my $Council_Code = shift;
	my $category = shift;
	my $id = shift;
	
	my $onDemand_Update_Query = "update TBL_ON_DEMAND_DOWNLOAD set status = 'Inprogress' where council_code=\'$Council_Code\' and id=\'$id\'";	
			
	if($category eq "Planning")
	{
		my $dbHostPlanning = &screenscrapperDB();
		&DB_Insert_Planning($dbHostPlanning,$onDemand_Update_Query);
	}
	elsif($category eq "Decision")
	{
		my $dbHostDecision = &gleniganDB();
		&DB_Insert_Decision($dbHostDecision,$onDemand_Update_Query);
	}
}

sub onDemandDelete()
{
	my $Council_Code = shift;
	my $category = shift;
	my $id = shift;
		
	my $onDemand_Delete_Query = "delete from TBL_ON_DEMAND_DOWNLOAD where council_code=\'$Council_Code\' and id=\'$id\'";
		
	if($category eq "Planning")
	{
		my $dbHostPlanning = &screenscrapperDB();
		&DB_Insert_Planning($dbHostPlanning,$onDemand_Delete_Query);
	}
	elsif($category eq "Decision")
	{
		my $dbHostDecision = &gleniganDB();
		&DB_Insert_Decision($dbHostDecision,$onDemand_Delete_Query);
	}
}

sub onDemandStatusUpdation()
{
	my $Council_Code = shift;
	my $category = shift;
	my $id = shift;
	my $status = shift;
		
	my $onDemand_StatusUpdate_Query = "update TBL_ON_DEMAND_DOWNLOAD_BACKUP set status = \'$status\' where council_code=\'$Council_Code\' and download_id=\'$id\'";
		
	if($category eq "Planning")
	{
		my $dbHostPlanning = &screenscrapperDB();
		&DB_Insert_Planning($dbHostPlanning,$onDemand_StatusUpdate_Query);
	}
	elsif($category eq "Decision")
	{
		my $dbHostDecision = &gleniganDB();
		&DB_Insert_Decision($dbHostDecision,$onDemand_StatusUpdate_Query);
	}
}

sub quote_add()
{
	my $data = shift;
	
	$data =~ s/\'/\'\'/igs;
	$data = "'".$data."'";
	
	return $data;
}



sub DATAGET_MultipleResultSet()
{
	my $dbh			= shift;
	my $Sp_Name		= shift;
	my $Date		= shift;
	my $Schedule	= shift;
	
	print "Sp_Name DB	:: $Sp_Name\n";
	print "Date			:: $Date\n";
	print "Schedule		:: $Schedule\n";
	
	my $query = "exec $Sp_Name \'$Date\', \'$Schedule\'";
	
	print "DATAGET_MultipleResultSet :: $query\n";
	my $sth= $dbh->prepare($query);
	$sth->execute();
	
	my (@Records,@Headers,$row);
	
	my $Count = $sth->rows;
	print join("\t\t", @{$sth->{NAME}}), "\n\n";
	
	
	my (@Records_temp);
	my $head = $sth->{NAME};
	push (@Headers, $head);
	my $count = 0;
	while(my @record = $sth->fetchrow) 
	{
		push (@Records_temp, \@record);
		$count++;
	}
	push (@Records, \@Records_temp);

	return (\@Records,\@Headers);
}

sub Send_Mail()
{
    my $Council_Code=shift;
    my $Council_Name=shift;
    my $body=shift;
	
    my $subject="Glenigan - Error in Council $Council_Code";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='smith.vincent@meritgroup.co.uk;parielavarasan.surulinathan@meritgroup.co.uk';
    # my $pass='11meritgroup11';
    my $pass='sXNdrc6JU';
	
    my $bodyCnt = "Hi Team, <br><br>\t\tCode: $Council_Code<br>Name: $Council_Name<br>Message: $body<br><br>Regards<br>Glenigan Team";
    
    my $msg = MIME::Lite->new (
      From => $from,
      To => $to,
      # Cc => $cc,
      Subject => $subject,
      Data => $bodyCnt,
      Type =>'text/html'
    ) or die "Error creating multipart container: $!\n";
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
    $msg->send;
}


################################## REDIS CONNECTION #####################################################
sub Redis_connection()
{
	my $redis;
	eval{
		$redis = Redis->new(server => '127.0.0.1:6379');
		print "Planning Redis Connected\n";
	};
	if($@)
	{
		print "Planning Redis not Connected\n";
		eval{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = Redis->new(server => '127.0.0.1:6379');
		};
		if($@)
		{
			&send_mail_redis("Redis Connection", "Unable to connect Redis", "Planning");
		}	
	}

	return $redis;
}

sub Redis_connection_Decision()
{
	my $redis;
	eval{
		$redis = RedisDB->new(host => '127.0.0.1', port => 6379, database => 7);
		print "Decision Redis Connected\n";
	};
	if($@)
	{
		print "Decision Redis not Connected\n";
		eval{
			system("sudo systemctl start redis");
			sleep(10);
			$redis = RedisDB->new(host => '127.0.0.1', port => 6379, database => 7);
		};
		if($@)
		{
			&send_mail_redis("Redis Connection", "Unable to connect Redis", "Decision");
		}	
	}
	return $redis;
}

sub send_mail_redis()
{
    my $Subject=shift;
    my $Body=shift;
    my $decision=shift;
	
	my $Current_dateTime=localtime();
	
    my $subject="Redis connection failed- $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='parielavarasan.surulinathan@meritgroup.co.uk';
    # my $cc ='parielavarasan.surulinathan@meritgroup.co.uk';
    # my $pass='11meritgroup11';
    my $pass='sXNdrc6JU';
    my $body = "Hi Team, \n\n\t\tUnable to connect Redis $decision server.\n\nRegards\nGlenigan Team";
	
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 # Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";

    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}


#################### Send a mail if any data_point gets updated ######################
sub Send_Report()
{
    my $Date=shift;
    my $File_Name=shift;
    my $Storefile=shift;
	my $Current_dateTime=localtime();
	
    my $subject="Glenigan - Perl Automation Report - $Current_dateTime";
    my $host ='74.80.234.196'; 
    my $from='autoemailsender@meritgroup.co.uk';
    my $user='meritgroup';
    my $to ='gleniganplans@meritgroup.co.uk;smith.vincent@meritgroup.co.uk';
    my $cc ='parielavarasan.surulinathan@meritgroup.co.uk';
    # my $pass='11meritgroup11';
    my $pass='sXNdrc6JU';
    my $body = "Hi Team, \n\n\t\tPlease find the attached Report for $Date.\n\nRegards\nGlenigan Team";
	
	my $file_path="/home/merit/Glenigan/Report";
	my $msg = MIME::Lite->new (
	 From => $from,
	 To => $to,
	 Cc => $cc,
	 Subject => $subject,
	 Data => $body
	) or die "Error creating multipart container: $!\n";
	
    ### Add parts (each "attach" has same arguments as "new"):
	$msg->attach(
        Type        =>'xls',
        Path        =>$Storefile,
        Filename    =>$File_Name,
        Disposition => 'attachment'
    );
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}