package ToughCouncilsController;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use URI::URL;
use URI::Escape;
use File::Path qw/make_path/;
use DateTime;
use WWW::Mechanize;
use IO::Socket::SSL;
# use Net::SSL;
use Net::Ping;


BEGIN 
{	
	my $target = '172.27.137.199';
	my $ping_obj = Net::Ping->new('tcp');

	$ping_obj->port_number('3128');

	if ($ping_obj->ping($target)) {
		print "Yes, I can ping $target\n";
		
		$ENV{HTTPS_PROXY} = 'http://172.27.137.199:3128';
		$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0; #works even with this
		$ENV{HTTPS_DEBUG} = 1;  #Add debug output 
		# <STDIN>;
	} else {
		print "No, I cannot ping $target\n";
	}

	$ping_obj->close();
}


my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;


####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc/Tough');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');

require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd(''); # get Current date


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $councilDetailsDecision, $regexFilePlanning, $regexFileDecision, $councilsList) = Config::Tiny->new();

####
# Read INI files from the objects "$councilDetailsFile" and "$councilDetailsDecision"
# These files contains - Details of councils and control properties for date range clculation repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Tough_Council_Details.ini');
$councilDetailsDecision = Config::Tiny->read($ConfigDirectory.'/Tough_Council_Decision_Details.ini');
$regexFilePlanning = Config::Tiny->read($ConfigDirectory.'/Tough_Regex_Heap.ini' );
$regexFileDecision = Config::Tiny->read($ConfigDirectory.'/Tough_Regex_Heap_Decision.ini' );
$councilsList = Config::Tiny->read($basePath.'/etc/Input_Council_Code.ini');



###
# Subroutine to set the constructor for WWW::Mechanize->new()
###
sub setVerficationSSL
{
    my ($currentCouncilCode,$category,$format,$sNo,$scheduleDate,$scheduleNo,$councilName) = @_;
	
	my ($councilApp, $commonFileDetails, $responseContent, $pingStatus, $categoryType);
	
			
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$categoryType = "Tough_Planning";
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$categoryType = "Tough_Decision";
	}	

	
	my $councilCodeList = $councilsList->{$sNo}->{$categoryType};
	
	my @cCode=split(',',$councilCodeList);
	
	if ( grep( /^$currentCouncilCode$/, @cCode ) ) {
	  print "Council Code \"$currentCouncilCode\" found it..\n";
	}
	else
	{
		print "\n\nCouncil Code not found in Input_Council_Code control file for $categoryType..\n\n";
		
		&CouncilsScrapperCore::scrapeStatusUpdation($scheduleNo,$scheduleDate,'0',$currentCouncilCode,'CCode not Available',$categoryType,$councilName);	
	
		exit;
	}
		
	
	if($commonFileDetails->{$currentCouncilCode}->{'SSL_VERIFICATION'} eq 'N')
	{
		$councilApp = WWW::Mechanize->new(autocheck => 0);
	}
	else
	{	
		# $councilApp = WWW::Mechanize->new( ssl_opts => {verify_hostname => 0,}, autocheck => 0);
		$councilApp = WWW::Mechanize->new( ssl_opts => {
			SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
			verify_hostname => 0, 
		}, autocheck => 0
		);
	}
		
	$councilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy	
	$councilApp->add_header( 'Accept-Language' => 'en-US,en;q=0.5');		
	
	
	$responseContent = $councilApp->get($commonFileDetails->{$currentCouncilCode}->{'URL'});
	
	if($currentCouncilCode=~m/^(158|316|425|439|608|612|614)$/is)
	{
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'}); 
		$councilApp->field( $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(304)$/is)
	{
		my $url = 'http://planning.bridgend.gov.uk/';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(625)$/is)
	{
		my $url = 'https://planning.somerset.gov.uk/Search/Advanced';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(168)$/is)
	{
		my $url = 'https://planningregister.crawley.gov.uk/';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(606)$/is)
	{
		my $url = 'https://planning.cumbria.gov.uk/Search/Advanced';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(619)$/is)
	{
		my $url = 'http://eplanning.lincolnshire.gov.uk/Search/Advanced';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(247)$/is)
	{
		my $url = 'https://planning.redcar-cleveland.gov.uk/Search/Planning/Advanced';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(616)$/is)
	{
		my $url = 'https://www.kentplanningapplications.co.uk/Search/Advanced';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}
	elsif($currentCouncilCode=~m/^(618)$/is)
	{
		my $url = 'http://leicestershire.planning-register.co.uk/Search/Advanced';
		$councilApp->get($url);
		$councilApp->form_number($commonFileDetails->{$currentCouncilCode}->{'FORM_NUMBER'});
		$councilApp->set_fields( '' => $commonFileDetails->{$currentCouncilCode}->{'ACC_BTN_VAL'} );
		$councilApp->click();
	}


	$responseContent = $councilApp->content;	
	$pingStatus = $councilApp->status;	
	
	print "Home Page ping Status==>$pingStatus\n";
	
	# open(TIME,">fileName.html");
	# print TIME "$responseContent\n";
	# close TIME;	
	# exit;
	
    return($councilApp, $pingStatus, $responseContent);
}



####
# Create File name with generic format
####
sub rawFileName()
{
	my ($startDateTime,$councilCode,$rangeName,$category) = @_;
	
	
	my ($year,$month,$day,$hour,$min,$sec);
	if($startDateTime=~m/^(\d+)\/(\d+)\/(\d+)\s+/is)
	{
		$year=$1;
		$month=$2;
		$day=$3;
	}
	my $Date = DateTime->new( year => $year,
							  month=> $month,
							  day  => $day,
							);

	my $currentDate = $Date->ymd('');

	print "currentDate==>$currentDate\n";
	
	my $councilTrimName;
	my $categoryFrontLetter;
	if($category eq "Planning")
	{
		$councilTrimName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'P_';
	}
	elsif($category eq "Decision")
	{
		$councilTrimName = $councilDetailsDecision->{$councilCode}->{'COUNCIL_NAME'};
		$categoryFrontLetter = 'D_';
	}
	
	
	$councilTrimName=~s/^([^>]{0,4})[^>]*?$/$1/igs;

	return($categoryFrontLetter.$currentDate.$councilCode.$councilTrimName.$rangeName.".html");
}

####
# Subroutine to get the details from application link(Server Machine) and insert into planning DB
####
sub applicationsDataDumpPlanning()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
		
	my ($Address,$Application,$dateApplicationvalidated,$dateApplicationReceived,$dateApplicationRegistered,$Proposal,$applicationStatus,$actualDecisionLevel,$agentAddress,$agentCompanyName,$agentName,$agentTelephone1,$applicantAddress,$applicantName,$applicationType,$agentEmail,$agentMobile,$agentFax,$agentTelephone2,$agentContactDetails,$newDocumentURL,$actualCommitteeDate,$actualCommitteeorPanelDate,$advertisementExpiryDate,$agreedExpiryDate,$applicationExpiryDeadline,$targetDecdt,$temporaryPermissionExpiryDate,$noDocument,$sourceWithTime,$documentURL,$gridReference,$Easting,$Northing);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	my ($PLANNING_APPLICATION, $SITE_LOCATION, $PROPOSAL, $FULL_DESCRIPTION, $REGISTRATION_DATE, $AGENT_PHONE, $RECEIVED_DATE, $VALID_DATE, $APPLICATION_DATE, $VALIDATED_DATE, $APPLICATION_TYPE, $APPLICATION_STATUS, $APPLICATION_STATUS_NEW, $APPLICANT_NAME, $AGENT_NAME, $APPLICANT_ADDRESS, $AGENT_ADDRESS, $DOC_URL, $TARGET_DATE,$AGENT_COMPANY,$NORTHING,$EASTING,$GRIDREFERENCE);
	
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$SITE_LOCATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'SITE_LOCATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$REGISTRATION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'REGISTRATION_DATE'}/is);
	$RECEIVED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'RECEIVED_DATE'}/is);
	$VALID_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'VALID_DATE'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$APPLICANT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_NAME'}/is);
	$APPLICANT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICANT_ADDRESS'}/is);
	$AGENT_NAME = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_NAME'}/is);
	$AGENT_ADDRESS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_ADDRESS'}/is);
	$AGENT_PHONE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_PHONE'}/is);
	$AGENT_COMPANY = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'AGENT_COMPANY'}/is);
	$TARGET_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'TARGET_DATE'}/is);
	$APPLICATION_TYPE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_TYPE'}/is);
	$EASTING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'EASTING'}/is);
	$NORTHING = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'NORTHING'}/is);
	$GRIDREFERENCE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'GRIDREFERENCE'}/is);
	$DOC_URL = $1 if($TabContent=~m/$regexFile->{$councilCode}->{'DOC_URL'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}	
		
	if($Easting eq "")
	{
		if($EASTING ne "")
		{
			$Easting = $EASTING;
		}
	}		
	
	if($Northing eq "")
	{
		if($NORTHING ne "")
		{
			$Northing = $NORTHING;
		}
	}		
	
	if($gridReference eq "")
	{
		if($GRIDREFERENCE ne "")
		{
			$gridReference = $GRIDREFERENCE;
			
			if($gridReference=~m/^\s*([^>]*?)\s*,\s*([^>]*?)\s*$/is)
			{
				my $newEasting=$1;
				my $newNorthing=$2;
				if($Easting eq "")
				{
					$Easting = $newEasting if($newEasting ne "");
				}
				if($Northing eq "")
				{
					$Northing = $newNorthing if($newNorthing ne "");
				}
			}
		}
	}	
	
	if($applicationType eq "")
	{
		if($APPLICATION_TYPE ne "")
		{
			$applicationType = $APPLICATION_TYPE;
		}
	}		
	
	if($agentCompanyName eq "")
	{
		if($AGENT_COMPANY ne "")
		{
			$agentCompanyName = $AGENT_COMPANY;
		}
	}		
			
	if($Address eq "")
	{
		if($SITE_LOCATION ne "")
		{
			$Address = $SITE_LOCATION;
		}
		$Address=~s/\s*\(\s*Location\s*Map\s*\)\s*$//is;
		$Address=~s/\s*\(?\s*View\s*Location\s*Map\s*\)?\s*$//is;
		$Address=~s/\s*Comment\s*on\s*this\s*application\s*$//is;
	}		
	
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}	
		
	if($dateApplicationRegistered eq "")
	{
		if($REGISTRATION_DATE ne "")
		{
			$dateApplicationRegistered = $REGISTRATION_DATE;
		}
	}		
	
	if($dateApplicationReceived eq "")
	{
		if($RECEIVED_DATE ne "")
		{
			$dateApplicationReceived = $RECEIVED_DATE;
		}
	}
	
	if($dateApplicationvalidated eq "")
	{
		if($VALID_DATE ne "")
		{
			$dateApplicationvalidated = $VALID_DATE;
		}
	}
	
	if($agentName eq "")
	{
		if($AGENT_NAME ne "")
		{
			$agentName = $AGENT_NAME;
		}
	}				
			
	if($agentTelephone1 eq "")
	{
		if($AGENT_PHONE ne "")
		{
			$agentTelephone1 = $AGENT_PHONE;
		}
	}				
			
	
	if($agentAddress eq "")
	{
		if($AGENT_ADDRESS ne "")
		{
			$agentAddress = $AGENT_ADDRESS;
		}
	}				
					
		
	if($applicantAddress eq "")
	{
		if($APPLICANT_ADDRESS ne "")
		{
			$applicantAddress = $APPLICANT_ADDRESS;
		}
	}					
		
	if($applicationStatus eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$applicationStatus = $APPLICATION_STATUS;
		}
	}	
	
	if($applicantName eq "")
	{
		if($APPLICANT_NAME ne "")
		{
			$applicantName = $APPLICANT_NAME;
		}
	}	
	
	if($targetDecdt eq "")
	{
		if($TARGET_DATE ne "")
		{
			$targetDecdt = $TARGET_DATE;
		}
	}	
	
	if($documentURL eq "")
	{
		if($DOC_URL ne "")
		{
			if($DOC_URL!~m/^https?/is)
			{
				$DOC_URL = URI::URL->new($DOC_URL)->abs( $commonFileDetails->{$councilCode}->{'URL'}, 1 );
			}
			$documentURL = $DOC_URL;
		}
		else
		{
			$documentURL = $applicationLink;
		}
	
		$documentURL =~s/\'/\'\'/igs;
		$documentURL =~s/\&amp\;/\&/igs;
	}
	$Proposal=~s/\s*Show\s*full\s*description$//gsi;	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Address=~s/\n+/ /gsi;
	$Proposal=~s/\n+/ /gsi;
	$applicationStatus=~s/\n+/ /gsi;
	$agentAddress=~s/\n+/ /gsi;
	$agentName=~s/\n+/ /gsi;
	$applicantAddress=~s/\n+/ /gsi;
	$applicantName=~s/\n+/ /gsi;
	$applicationType=~s/\n+/ /gsi;
	
		
	my $insert_query="(\'$councilCode\', \'$councilName\', \'$Address\', \'$dateApplicationReceived\', \'$Application\', \'$dateApplicationRegistered\', \'$dateApplicationvalidated\', \'$Proposal\', \'$applicationStatus\', \'$actualDecisionLevel\', \'$agentAddress\', \'$agentCompanyName\', \'$agentName\', \'$agentTelephone1\', \'$applicantAddress\', \'$applicantName\', \'$applicationType\', \'$agentEmail\', \'$agentTelephone2\', \'$agentMobile\', \'$agentFax\', \'$agentContactDetails\', \'$actualCommitteeDate\', \'$actualCommitteeorPanelDate\', \'$advertisementExpiryDate\', \'$agreedExpiryDate\', \'$applicationExpiryDeadline\', \'$targetDecdt\', \'$temporaryPermissionExpiryDate\', \'$applicationLink\', \'$documentURL\', \'$noDocument\', \'$sourceWithTime\', \'$scheduleDate\', \'$gridReference\', \'$Easting\', \'$Northing\'),";
	
	undef $Address; undef $dateApplicationReceived; undef $Application; undef $dateApplicationRegistered; undef $dateApplicationvalidated; undef $Proposal; undef $applicationStatus; undef $actualDecisionLevel; undef $agentAddress; undef $agentCompanyName; undef $agentName; undef $agentTelephone1; undef $applicantAddress; undef $applicantName; undef $applicationType; undef $agentEmail; undef $agentTelephone2; undef $agentMobile; undef $agentFax; undef $agentContactDetails; undef $actualCommitteeDate; undef $actualCommitteeorPanelDate; undef $advertisementExpiryDate; undef $agreedExpiryDate; undef $applicationExpiryDeadline; undef $targetDecdt; undef $temporaryPermissionExpiryDate; undef $applicationLink; undef $documentURL; undef $noDocument; undef $gridReference; undef $Easting; undef $Northing;
	
	# print "$insert_query\n";
	# <STDIN>;
	
	return($insert_query);		
}


####
# Subroutine to get the details from application link(Server Machine) and insert into Decision DB
####
sub applicationsDataDumpDecision()
{
	my ($TabContent, $applicationLink, $Source, $councilCode, $councilName, $scheduleDate, $scheduleDateTime, $category, $councilApp) = @_;	
	
	my ($Date_Decision_Made, $Decision_Status, $Decision_Issued_Date, $Proposal, $Application, $Application_Status, $sourceWithTime);
	
	my ($regexFile,$commonFileDetails);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
							
	my ($PLANNING_APPLICATION,$PROPOSAL,$FULL_DESCRIPTION,$APPLICATION_STATUS,$APPLICATION_STATUS_NEW,$DECISION_DATE,$DECISION_STATUS,$DECISION_ISSUED_DATE);
	$PLANNING_APPLICATION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
	$PROPOSAL = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL'}/is);
	$FULL_DESCRIPTION = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'PROPOSAL_NEW'}/is);
	$APPLICATION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'APPLICATION_STATUS'}/is);
	$DECISION_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_MADE_DATE'}/is);
	$DECISION_STATUS = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_STATUS'}/is);
	$DECISION_ISSUED_DATE = &htmlTagClean($1) if($TabContent=~m/$regexFile->{$councilCode}->{'DECISION_DATE'}/is);
	
		
	if($Application eq "")
	{
		if($PLANNING_APPLICATION ne "")
		{
			$Application = $PLANNING_APPLICATION;
		}
	}		
			
	if($Decision_Issued_Date eq "")
	{
		if($DECISION_ISSUED_DATE ne "")
		{
			$Decision_Issued_Date=$DECISION_ISSUED_DATE;
		}
	}		
	
	if($Date_Decision_Made eq "")
	{
		if($DECISION_DATE ne "")
		{
			$Date_Decision_Made=$DECISION_DATE;
			
			$Date_Decision_Made=~s/^Not\s*Known$//is;
		}
		
		if(($councilCode=~m/^477$/is) && ($Date_Decision_Made=~m/^01\/01\/1970$/is))
		{
			$Date_Decision_Made=~s/^01\/01\/1970$//is;
		}
	}		
		
	if($Proposal eq "")
	{
		if($PROPOSAL ne "")
		{
			$Proposal = $PROPOSAL;
		}
		elsif($FULL_DESCRIPTION ne "")
		{
			$Proposal = $FULL_DESCRIPTION;
		}
	}		
	
	if($Application_Status eq "")
	{
		if($APPLICATION_STATUS ne "")
		{
			$Application_Status = $APPLICATION_STATUS;
		}
	}		
	
	if($Decision_Status eq "")
	{
		if($DECISION_STATUS ne "")
		{
			$Decision_Status = $DECISION_STATUS;
		}
	}	
		
	$sourceWithTime=$Source."_".$scheduleDateTime."-perl";
	
	$Proposal=~s/\n+/ /gsi;
	$Application_Status=~s/\n+/ /gsi;
	$Decision_Status=~s/\n+/ /gsi;
	
	my $insert_query="(\'$Application\', \'$Proposal\', \'$Decision_Status\', \'$Date_Decision_Made\', \'$Decision_Issued_Date\', \'$councilCode\', \'$applicationLink\', \'$sourceWithTime\', \'$Application_Status\', \'$scheduleDate\'),";
	
	undef $PLANNING_APPLICATION;  undef $PROPOSAL;  undef $DECISION_STATUS;  undef $APPLICATION_STATUS;  undef $DECISION_DATE;  undef $DECISION_ISSUED_DATE;  undef $applicationLink;
	undef $Application;  undef $Proposal;  undef $Decision_Status;  undef $Application_Status;  undef $Date_Decision_Made;  undef $Decision_Issued_Date;  undef $applicationLink;
		
	return($insert_query);	
}


sub getMechContent() 
{
    my $URL = shift;
    my $mech = shift;
   
	$URL=~s/amp\;//igs;
	
	# $mech->proxy(['http','https'], 'http://172.27.137.192:3128');	# Old Proxy

	# $mech->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy

	
	$mech->get($URL);
	
	my $con = $mech->content;
    my $code = $mech->status;
	
    return($con,$code);
}



####
# Subroutine to fetch planning page
####

sub councilsMechMethod()
{
    my ($receivedFromDate, $receivedToDate, $receivedCouncilCode, $receivedCouncilApp, $category) = @_;
	
	
    my ($councilAppResponse, $commonFileDetails, $scriptStatus,$postContent, $searchURL);
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	$searchURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};	
	$postContent = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_POST_CONTENT'};
	my $formNumber = $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'};
	my $fromDate = $commonFileDetails->{$receivedCouncilCode}->{'FORM_START_DATE'};
	my $toDate = $commonFileDetails->{$receivedCouncilCode}->{'FORM_END_DATE'};
	my $searchButton = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'};
	my $searchButtonVal = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_VAL'};
	
	$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'}); 
	
	# print "Fromdate::$receivedFromDate\tTodate::$receivedToDate\n";
	# print "FromdateName::$fromDate\tTodateName::$toDate\n";
	# print "FormName::$formNumber\tSearchName::$searchButton\n";
	
	if($receivedCouncilCode=~m/^(120|413|480|612)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click( $searchButton );
	}
	elsif($receivedCouncilCode=~m/^(492|618|619)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(304)$/is)
	{
		my $url = 'http://planning.bridgend.gov.uk/';
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber);
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(625)$/is)
	{
		my $url = 'https://planning.somerset.gov.uk/Search/Advanced';
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber);
		$receivedCouncilApp->set_fields($fromDate => $receivedFromDate, $toDate => $receivedToDate );
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(249|315|628)$/is)
	{
		$receivedCouncilApp-> submit_form(
				form_number => $formNumber,
				fields      => {
					$fromDate	=> $receivedFromDate,
					$toDate	=> $receivedToDate,
					$searchButton	=> $searchButtonVal,
					},
					button          => $searchButton
				);
	}
	elsif($receivedCouncilCode=~m/^(384)$/is)
	{		
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		$startDateDisplay=~s/\//\%2F/gsi;
		$endDateDisplay=~s/\//\%2F/gsi;
		
		$searchURL=~s/<FromDate>/$startDateDisplay/is;
		$searchURL=~s/<ToDate>/$endDateDisplay/is;
		
		$receivedCouncilApp->get($searchURL);
	
	}
	elsif($receivedCouncilCode =~ m/^(316)$/is)	
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		if($category eq "Planning")
		{			
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_dpReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_MainContent_dpReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{			
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_dpReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_MainContent_dpReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode =~ m/^(620)$/is)	
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		my $val_from = $Temp_From_Date."-00-00-00";
		my $val_to = $Temp_To_Date."-00-00-00";
		
		if($category eq "Planning")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtCompleteDateFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtCompleteDateTo$dateInput' => $val_to );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtDecisionDateFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtDecisionDateTo$dateInput' => $val_to );
		}
		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(425)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		my $val_from = $Temp_From_Date."-00-00-00";
		my $val_to = $Temp_To_Date."-00-00-00";
		
		if($category eq "Planning")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtDateReceivedFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtDateReceivedTo$dateInput' => $val_to );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00$ContentPlaceHolder1$txtDateDeterminedFrom$dateInput' => $val_from, 'ctl00$ContentPlaceHolder1$txtDateDeterminedTo$dateInput' => $val_to );
		}

		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(168)$/is)
	{
		my $url = 'https://planningregister.crawley.gov.uk/';
		$receivedCouncilApp->get($url);
		
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<form[^>]*?>\s*<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		# print "postContent==>$postContent\n";
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
	}
	elsif($receivedCouncilCode=~m/^(606)$/is)
	{
		my $url = 'https://planning.cumbria.gov.uk/Search/Advanced';
		$receivedCouncilApp->get($url);
		
		my $getPageContent = $receivedCouncilApp->content;
		
		my $token;
		if($getPageContent=~m/<input[^>]*?name\s*=\s*\"__RequestVerificationToken\"[^>]*?value=\"([^\"]*?)\"[^>]*?>/is)
		{
			$token = uri_escape($1);
		}
		
		my $Temp_From_Date = $receivedFromDate;
		my $Temp_To_Date = $receivedToDate;
		
		$Temp_From_Date =~s/\//\%2F/gsi;
		$Temp_To_Date =~s/\//\%2F/gsi;
		
		$postContent=~s/<TOKEN>/$token/si;
		$postContent=~s/<FromDate>/$Temp_From_Date/sgi;
		$postContent=~s/<ToDate>/$Temp_To_Date/sgi;
		
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
	}
	elsif($receivedCouncilCode=~m/^(247)$/is)
	{
		my $url = 'https://planning.redcar-cleveland.gov.uk/Search/Planning/Advanced';
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(616)$/is)
	{
		my $url = 'https://www.kentplanningapplications.co.uk/Search/Advanced';
		$receivedCouncilApp->get($url);
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(158|244|261|428|439|451|608)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
				
		if($category eq "Planning")
		{
			$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDateReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDateReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{
			if($receivedCouncilCode=~m/^(428|608)$/is)
			{
				$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDateDeterminedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDateDeterminedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
			}
			else
			{
				$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDateIssuedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDateIssuedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1980-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
			}
		}
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(621)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		my $val_from = $Temp_From_Date."-00-00-00";
		my $val_to = $Temp_To_Date."-00-00-00";
		
		if($category eq "Planning")
		{			
			$receivedCouncilApp->set_fields( 'ctl00$MainContent$txtValidatedDateFrom$dateInput' => $val_from, 'ctl00$MainContent$txtValidatedDateTo$dateInput' => $val_to );
			
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtValidatedDateFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}', 'ctl00_MainContent_txtValidatedDateTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}' );			
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00$MainContent$txtDecisionDateFrom$dateInput' => $val_from, 'ctl00$MainContent$txtDecisionDateTo$dateInput' => $val_to );
			
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtDecisionDateFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}', 'ctl00_MainContent_txtDecisionDateTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","minDateStr":"1/1/1900 0:0:0","maxDateStr":"12/31/2099 0:0:0"}' );		
		}
		

		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(617)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		if($category eq "Planning")
		{		
			$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtAppValFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtAppValTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00_ContentPlaceHolder1_txtDecFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ContentPlaceHolder1_txtDecTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click();
	}
	elsif($receivedCouncilCode=~m/^(369)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		if($category eq "Planning")
		{		
			$receivedCouncilApp->set_fields( 'ctl00_ctl00_MainContent_MainContent_txtDateReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ctl00_MainContent_MainContent_txtDateReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
			
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00_ctl00_MainContent_MainContent_txtDateIssuedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_ctl00_MainContent_MainContent_txtDateIssuedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click($searchButton);
	}
	elsif($receivedCouncilCode=~m/^(623)$/is)
	{
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->set_fields( $fromDate => $receivedFromDate, $toDate => $receivedToDate );
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		if($category eq "Planning")
		{		
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtDateReceivedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_MainContent_txtDateReceivedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		elsif($category eq "Decision")
		{
			$receivedCouncilApp->set_fields( 'ctl00_MainContent_txtDateDeterminedFrom_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_From_Date.'-00-00-00","valueAsString":"'.$Temp_From_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedFromDate.'"}', 'ctl00_MainContent_txtDateDeterminedTo_dateInput_ClientState' => '{"enabled":true,"emptyMessage":"","validationText":"'.$Temp_To_Date.'-00-00-00","valueAsString":"'.$Temp_To_Date.'-00-00-00","minDateStr":"1900-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.$receivedToDate.'"}' );
		}
		
		$receivedCouncilApp->click($searchButton);
		
		$receivedCouncilApp->form_number($formNumber); 
		$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
		$receivedCouncilApp->click();
		
	}
	elsif($receivedCouncilCode=~m/^(394)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		$startDateDisplay=~s/\//\-/gsi;
		$endDateDisplay=~s/\//\-/gsi;
		
		my ($Temp_From_Date,$Temp_To_Date);
		if($receivedFromDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_From_Date = $3."-".$2."-".$1;
		}
		if($receivedToDate=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$Temp_To_Date = $3."-".$2."-".$1;
		}
		
		$postContent=~s/<FromDate>/$Temp_From_Date/si;
		$postContent=~s/<FromDateS>/$receivedFromDate/si;
		$postContent=~s/<ToDateS>/$receivedToDate/si;
		$postContent=~s/<ToDate>/$Temp_To_Date/si;
			
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(302)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my %monthFull = ('01' =>'January', '02' => 'February', '03' => 'March','04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $monthFull{$2};
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $monthFull{$2};
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$eDay=~s/^0//is;
				
		$postContent=~s/<SDATE>/$sDay/si;
		$postContent=~s/<EDATE>/$eDay/si;
		$postContent=~s/<SMONTH>/$sMonth/si;
		$postContent=~s/<EMONTH>/$eMonth/si;
		$postContent=~s/<SYEAR>/$sYear/si;
		$postContent=~s/<EYEAR>/$eYear/si;
			
		$receivedCouncilApp->post( $searchURL, Content => "$postContent");
		
	}
	elsif($receivedCouncilCode=~m/^(477)$/is)
	{
		my $startDateDisplay=$receivedFromDate;
		my $endDateDisplay=$receivedToDate;
		
		my %monthFull = ('01' =>'January', '02' => 'February', '03' => 'March','04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
		
		my ($sDay,$sMonth,$sYear,$eDay,$eMonth,$eYear);
		if($startDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$sDay = $1;
			$sMonth = $monthFull{$2};
			$sYear = $3;
		}
		if($endDateDisplay=~m/(\d+)\/(\d+)\/(\d+)/is)
		{
			$eDay = $1;
			$eMonth = $monthFull{$2};
			$eYear = $3;
		}
		
		$sDay=~s/^0//is;
		$eDay=~s/^0//is;
				
		$searchURL=~s/<SDATE>/$sDay/si;
		$searchURL=~s/<EDATE>/$eDay/si;
		$searchURL=~s/<SMONTH>/$sMonth/si;
		$searchURL=~s/<EMONTH>/$eMonth/si;
		$searchURL=~s/<SYEAR>/$sYear/si;
		$searchURL=~s/<EYEAR>/$eYear/si;
			
		$receivedCouncilApp->get( $searchURL );
		
	}
	elsif($receivedCouncilCode=~m/^(614)$/is)
	{	
		$receivedCouncilApp->get( $searchURL );
	}
	# $councilAppResponse = $receivedCouncilApp->post($searchURL, Content => "$postContent");
	
	$councilAppResponse = $receivedCouncilApp->content;
	
	my $currentPageURL = $receivedCouncilApp->uri();
	
	$scriptStatus = $receivedCouncilApp->status;		
	print "councilAppStatus==>$scriptStatus\n";
	
	# open(TIME,">fileName.html");
	# print TIME "$councilAppResponse\n";
	# close TIME;
	# <STDIN>;
	# exit;
	
    return ($councilAppResponse,$scriptStatus,$currentPageURL);
}

####
# Method to POST the pagenation settings for Online Councils
####

sub searchPageNext()
{
	my ($receivedCouncilCode, $receivedCouncilApp, $category, $pageContent, $receivedFromDate, $receivedToDate) = @_;
	my ($nextPageLink,$commonFileDetails,$regexFile,$commonContent);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
		$regexFile=$regexFileDecision;
	}
	
	$commonContent .= $pageContent;
	
	my $councilType = $commonFileDetails->{$receivedCouncilCode}->{'FORMAT_TYPE'};
	
	if($councilType=~m/^PAGENUMBER$/is)
	{
		my $pageCheck = 2;
		
		
		if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'TOTAL_PAGE_COUNT'}/is)
		{
			my $totalNumPage = $1;
			
			NextPage:
			if($pageCheck<=$totalNumPage)
			{
				print "CurrentPage::$pageCheck==>Total::$totalNumPage\n";
				$receivedCouncilApp->form_number($commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'}); 
				
				if($receivedCouncilCode=~m/^(120|158|168|247|261|316|369|413|425|428|439|451|480|608|612|614|621|623)$/is)
				{
					my $nextPage = $1 if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}/is);
					# print "nextPage==>$nextPage\n";<STDIN>;
					eval
					{
						$receivedCouncilApp->click( $nextPage );
					};					
				}
				
				my $nextPageContent = $receivedCouncilApp->content;	
				$pageContent = $nextPageContent;
								
				# open(TIME,">fileName$pageCheck.html");
				# print TIME "$pageContent\n";
				# close TIME;
		
				if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'}/is)
				{
					$commonContent .= $pageContent;
					$pageCheck++;
					goto NextPage;
				}
				else
				{
					$commonContent .= $pageContent;
				}
			}
		}		
	}
	elsif($councilType=~m/^POSTFORMAT$/is)
	{		
		my $pageCheck = 2;		
				
		NextPostPage:
		my $nextPagecheck = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'};		
		$nextPagecheck=~s/<PAGENUM>/$pageCheck/gsi;
		
		# print "nextPagecheck==>$nextPagecheck\n";
		# print "pageCheck==>$pageCheck\n"; <STDIN>;
		
		if($pageContent=~m/$nextPagecheck/is)
		{		
			my $eventTargetRegex = $commonFileDetails->{$receivedCouncilCode}->{'EVENTTARGET'};
			my $eventArgumentRegex = $commonFileDetails->{$receivedCouncilCode}->{'EVENTARGUMENT'};
			$eventTargetRegex=~s/<PAGENUM>/$pageCheck/gsi;
			my $eventTarget = uri_escape($1) if($pageContent=~m/$eventTargetRegex/is);
			$eventArgumentRegex=~s/<PAGENUM>/$pageCheck/gsi;
			my $eventArgument = uri_escape($1) if($pageContent=~m/$eventArgumentRegex/is);
			my $viewState = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATE'}/is);
			my $viewStateGen = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'VIEWSTATEGENERATOR'}/is);
			my $eventValidation = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'EVENTVALIDATION'}/is);
			my $activityID = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'ACTIVITYID'}/is);
			my $scrollPos = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SCROLLPOSITION'}/is);
			my $scrollX = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SCROLLPOSITIONX'}/is);
			my $scrollY = uri_escape($1) if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'SCROLLPOSITIONY'}/is);
			
			my $nextPagePostCnt = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_POST_CONTENT'};
			# print "eventTargetRegex==>$eventTargetRegex\n";
			# print "EVENTARGUMENT==>$eventArgument\n";
			
			$nextPagePostCnt=~s/<VIEWSTATE>/$viewState/is;
			$nextPagePostCnt=~s/<EVENTTARGET>/$eventTarget/is;
			$nextPagePostCnt=~s/<EVENTARGUMENT>/$eventArgument/is;
			$nextPagePostCnt=~s/<VIEWSTATEGENERATOR>/$viewStateGen/is;
			$nextPagePostCnt=~s/<EVENTVALIDATION>/$eventValidation/is;
			$nextPagePostCnt=~s/<ACTIVITYID>/$activityID/is;
			$nextPagePostCnt=~s/<SCROLL>/$scrollPos/is;
			$nextPagePostCnt=~s/<SCROLLX>/$scrollX/is;
			$nextPagePostCnt=~s/<SCROLLY>/$scrollY/is;
			
			my $SEARCH_URL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_URL'};
			# print "SEARCH_URL=>$SEARCH_URL\n"; <STDIN>;

			if($receivedCouncilCode=~m/^(249)$/is)
			{
				$SEARCH_URL=~s/<FromDate>/$receivedFromDate/is;
				$SEARCH_URL=~s/<ToDate>/$receivedToDate/is;
				
				if($category eq "Planning")
				{
					$SEARCH_URL=~s/(\&DateRecfrm=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
					$SEARCH_URL=~s/(\&DateRecTo=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
				}
				elsif($category eq "Decision")
				{
					$SEARCH_URL=~s/(\&DateDecfrm=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
					$SEARCH_URL=~s/(\&DateDecTo=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
				}
			}
			elsif($receivedCouncilCode=~m/^(384)$/is)
			{
				$SEARCH_URL=~s/<FromDate>/$receivedFromDate/is;
				$SEARCH_URL=~s/<ToDate>/$receivedToDate/is;
				if($category eq "Planning")
				{
					$SEARCH_URL=~s/(\&datereceivedfrom=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
					$SEARCH_URL=~s/(\&datereceivedto=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
				}
				elsif($category eq "Decision")
				{
					$SEARCH_URL=~s/(\&dateissuedfrom=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
					$SEARCH_URL=~s/(\&dateissuedto=\d+)\/(\d+)\/(\d+)/$1%2F$2%2F$3/gsi;
				}
			}
			
			if($receivedCouncilCode=~m/^(304|625|168)$/is)
			{
				my $nextPageURL = $1 if($pageContent=~m/$nextPagecheck/is);
				if($nextPageURL!~m/^https?/is)
				{
					$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
				}
				
				$nextPageURL=~s/\&amp\;/\&/gsi;
				$receivedCouncilApp->get( $nextPageURL );
			}
			else
			{
				$receivedCouncilApp->post( $SEARCH_URL, Content => "$nextPagePostCnt");
			}
			
			my $nextPageContent = $receivedCouncilApp->content;
			$pageContent = $nextPageContent;
						
			# open(TIME,">fileName$pageCheck.html");
			# print TIME "$pageContent\n";
			# close TIME;
			# <STDIN>;
			# exit;
			
			my $nextPageCheckRegex = $commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_CHECK_REGEX'};
			$nextPageCheckRegex=~s/<PAGENUM>/$pageCheck/is;
			
			if($pageContent=~m/$nextPageCheckRegex/is)
			{
				print "Current page number is $pageCheck\n";
				$commonContent .= $pageContent;
				
			}
			else
			{
				$commonContent .= $pageContent;
				
				print "Next page number is $pageCheck\n";
				
				$pageCheck++;
				goto NextPostPage;
			}			
		}
		else
		{
			# print "Current page number is 1\n";
			$commonContent;
		}
	}
	elsif($councilType=~m/^NEXTPAGEURL$/is)
	{	
		my $pageCheck = 2;		
				
		NextPostURLPage:	

		my $nextPageRegex=$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_REGEX'};
		$nextPageRegex=~s/<PAGENUM>/$pageCheck/is;
		if($pageContent=~m/$nextPageRegex/is)
		{		
			my $nextPageURL = $1;
			
			if($nextPageURL!~m/^https?/is)
			{
				$nextPageURL = URI::URL->new($nextPageURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
			}		
				
			$nextPageURL=~s/\&amp\;/\&/gsi;	
			
			$receivedCouncilApp->get( $nextPageURL );			
			
			my $nextPageContent = $receivedCouncilApp->content;	
			$pageContent = $nextPageContent;
			
			
			if($pageContent=~m/$commonFileDetails->{$receivedCouncilCode}->{'NEXT_PAGE_CHECK_REGEX'}/is)
			{
				print "Current page number is $pageCheck\n";
				$commonContent .= $pageContent;
			}
			else
			{
				$commonContent .= $pageContent;
				
				print "Next page number is $pageCheck\n";
				
				$pageCheck++;
				goto NextPostURLPage;
			}
		}		
	}
	
	return($commonContent);
}


####
# Method to GET the content after pagenation
####

sub getApplicationDetails()
{
	my ($receivedApplicationUrl, $receivedCouncilApp, $councilCode, $category) = @_;
	
	my $count = 0;
	Loop:
	my ($applicationDetailsPageContent,$applicationDetailsPageResponse,$applicationNumber,$regexFile);
	$applicationDetailsPageContent = $receivedCouncilApp->get($receivedApplicationUrl);
	$applicationDetailsPageResponse = $receivedCouncilApp->status();	
	$applicationDetailsPageContent = $receivedCouncilApp->content;
	
	if($category eq "Planning")
	{
		$regexFile=$regexFilePlanning;
	}
	elsif($category eq "Decision")
	{
		$regexFile=$regexFileDecision;
	}
	
	if($applicationDetailsPageContent=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{
		$applicationNumber=&htmlTagClean($1);
	}
	
	# print "Current Application number::$applicationNumber\n";
	
	if(($applicationNumber eq "") && ($count > 1))
	{	
		my $logName;
		if($receivedApplicationUrl=~m/reference\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/pRecordID\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/iAppID\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/appno\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/TheSystemkey\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/ApplicationReference\=([^\&]*?)\&/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/applicationNumber\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/AppNo\=([^<]*?)\$/s)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/id\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/recno\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/reference\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		elsif($receivedApplicationUrl=~m/appkey\=([^<]*?)\$/is)
		{
			$logName = $1;
		}
		$logName=~s/\//\_/is;		
		
		my $fileName= $category."_".$councilCode."_".$logName;
		my $logLocation = $logsDirectory.'/applicationFails/'.$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(TIME,">>$logLocation/$fileName.html");
		print TIME "$applicationDetailsPageContent\n";
		close TIME;
	}
	
	if(($applicationDetailsPageResponse!~m/^\s*200\s*$/is) && ($count<=3))
	{
		$count++;
		goto Loop;
	}
	
	# open(TIME,">fileName.html");
	# print TIME "$applicationDetailsPageContent\n";
	# close TIME;
	# exit;
	
	return($applicationDetailsPageContent, $applicationDetailsPageResponse, $applicationNumber);
	
}


####
# Search using Application number in website
####
sub searchAppNumLogic()
{
	my ($receivedCouncilCode, $applicationNumber, $receivedCouncilApp, $category) = @_;
	
	$receivedCouncilApp->proxy(['http','https'], 'http://172.27.137.199:3128');	# New Proxy			
	
	
	my ($searchResponseContent, $scriptStatus, $rerunCount, $currentPageURL, $commonFileDetails);
	
	if($category eq "Planning")
	{
		$commonFileDetails=$councilDetailsFile;
	}
	elsif($category eq "Decision")
	{
		$commonFileDetails=$councilDetailsDecision;
	}
	
	my $searchCriteria = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'};
	my $searchButtonVal = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_VAL'};
	my $formNumber = $commonFileDetails->{$receivedCouncilCode}->{'FORM_NUMBER'};
	my $searchButton = $commonFileDetails->{$receivedCouncilCode}->{'SRCH_BTN_NME'};
	my $searchAppRegex = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_PAGE_CONTENT_RX'};

	
	Loop:
	eval
	{	
		if($receivedCouncilCode=~m/^(120|369|413|425|480|612|620)$/is)
		{
			$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});	
			my $crntPageURL = $receivedCouncilApp->uri();
			
			if($crntPageURL=~m/disclaimer|returnUrl/is)
			{
				if($receivedCouncilCode=~m/^(425|612)$/is)
				{
					$receivedCouncilApp->form_number($formNumber); 
					$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
					$receivedCouncilApp->click();
				}
			}
			$receivedCouncilApp->form_number($formNumber);
			$receivedCouncilApp->set_fields( $searchCriteria => $applicationNumber );
			$receivedCouncilApp->click($searchButton);
					  
		}
		elsif($receivedCouncilCode=~m/^(302)$/is)
		{
			$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});	
			
			$receivedCouncilApp->form_number($formNumber);
			$receivedCouncilApp->set_fields( $searchCriteria => $applicationNumber );
			$receivedCouncilApp->click();		
		}
		elsif($receivedCouncilCode=~m/^(249)$/is)
		{
			$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});	
			my $crntPageURL = $receivedCouncilApp->uri();
			
		
			$receivedCouncilApp-> submit_form(
					form_number => $formNumber,
					fields      => {
						$searchCriteria	=> $applicationNumber,
						$searchButton	=> $searchButtonVal,
						},
						button          => $searchButton
					);

		}
		elsif($receivedCouncilCode=~m/^(492|158|261|316|394|428|439|451|608|621|617)$/is)
		{
			$receivedCouncilApp->get($commonFileDetails->{$receivedCouncilCode}->{'URL'});
			my $crntPageURL = $receivedCouncilApp->uri();
			print "crntPageURL==>$crntPageURL\n";
			
			if($crntPageURL=~m/disclaimer|returnUrl/is)
			{	
				if($receivedCouncilCode=~m/^(158|316|439|608|612|614)$/is)
				{
					$receivedCouncilApp->form_number($formNumber); 
					$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
					$receivedCouncilApp->click();
				}
			}

			print "applicationNumber==>$applicationNumber\n";
			# <STDIN>;

			$applicationNumber=~s/^([^<]*?)\s+\([^<]*?$/$1/gsi;

			$receivedCouncilApp->form_number($formNumber);
			$receivedCouncilApp->set_fields( $searchCriteria => $applicationNumber );
			$receivedCouncilApp->click();
									  
		}
		elsif($receivedCouncilCode=~m/^(614)$/is)
		{
			$receivedCouncilApp->get('https://planning.hants.gov.uk/Search.aspx');	
			my $crntPageURL = $receivedCouncilApp->uri();
			print "crntPageURL==>$crntPageURL\n";
			if($crntPageURL=~m/disclaimer|returnUrl/is)
			{	
				if($receivedCouncilCode=~m/^(614)$/is)
				{
					$receivedCouncilApp->form_number($formNumber); 
					$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
					$receivedCouncilApp->click();
				}
			}

			$receivedCouncilApp->form_number($formNumber);
			$receivedCouncilApp->set_fields( $searchCriteria => $applicationNumber );
			$receivedCouncilApp->click();
		}
		else
		{	
			my $searchURL = $commonFileDetails->{$receivedCouncilCode}->{'SEARCH_CRITERIA'};	
			$receivedCouncilApp->get( $commonFileDetails->{$receivedCouncilCode}->{'URL'} );
			my $cPageURL = $receivedCouncilApp->uri();	
			if($cPageURL=~m/disclaimer|returnUrl/is)
			{
				if($receivedCouncilCode=~m/^(168|247|304|315|606|616|618|619|625)$/is)
				{		
					$receivedCouncilApp->form_number($formNumber);
					$receivedCouncilApp->set_fields( '' => $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
					$receivedCouncilApp->click();
				}
			}

			if($receivedCouncilCode=~m/^(616|625)$/is)
			{
				$applicationNumber=~s/^([^<]*?)\s+\([^<]*?$/$1/gsi;
			}
			elsif($receivedCouncilCode!~m/^(623|168|315|384|606)$/is)
			{
				$applicationNumber=~s/\//\%2F/gsi;
			}

			if($receivedCouncilCode=~m/^(477)$/is)
			{
				my $dumNum = $applicationNumber;
				$dumNum=~s/\%2F/\%2E/gsi;	
				$searchURL=~s/\?appNo=<APPNUM>/\?appNo=$applicationNumber/si;	
				$searchURL=~s/<APPNUM>$/$dumNum/si;
			}
			else
			{
				$searchURL=~s/<APPNUM>/$applicationNumber/si;	
			}

			
			print "searchCriteria==>$searchURL\n";			
			
			$receivedCouncilApp->get( $searchURL );

		}
	};
	
	
    $searchResponseContent = $receivedCouncilApp->content;	
	$currentPageURL = $receivedCouncilApp->uri();	
    $scriptStatus = $receivedCouncilApp->status;
	print "scriptStatus==>$scriptStatus\n";
	print "currentPageURL==>$currentPageURL\n";

			
	if($currentPageURL=~m/disclaimer|returnUrl/is)
	{	
		if($receivedCouncilCode=~m/^(623)$/is)
		{
			$receivedCouncilApp->form_number($formNumber); 
			$receivedCouncilApp->field( $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_NME'}, $commonFileDetails->{$receivedCouncilCode}->{'ACC_BTN_VAL'} );
			$receivedCouncilApp->click();
			$currentPageURL = $receivedCouncilApp->uri();	
		}
	}	
	
	# open(TIME,">fileName.html");
	# print TIME "$searchResponseContent\n";
	# close TIME;
	# exit;

	
	
	if($receivedCouncilCode=~m/^(120|158|249|261|302|316|369|394|413|425|428|439|451|480|492|608|612|614|617|620|621|628)$/is)
	{
		if($searchResponseContent=~m/$searchAppRegex/is)
		{
			my $sourceURL=$1;
			$sourceURL = URI::URL->new($sourceURL)->abs( $commonFileDetails->{$receivedCouncilCode}->{'URL'}, 1 );
			$sourceURL=~s/\&amp\;/\&/gsi;
			$receivedCouncilApp->get($sourceURL);
			$searchResponseContent = $receivedCouncilApp->content;	
			$scriptStatus = $receivedCouncilApp->status;
			$currentPageURL=$sourceURL;
		}
	}
	
	# open(TIME,">fileName1.html");
	# print TIME "$searchResponseContent\n";
	# close TIME;
	# exit;
	# <STDIN>;
	
	
	if(($scriptStatus!~m/^\s*200\s*$/is) && ($rerunCount<=2))
	{
		$rerunCount++;
		goto Loop;
	}

	
	return($searchResponseContent, $currentPageURL, $scriptStatus)
}


####
# Clean unwanted tags in search result response html content
####
sub htmlTagClean()
{
	my ($data2Clean) = @_;
	$data2Clean=~s/\&lt\;/\</igs;
	$data2Clean=~s/\&gt\;/\>/igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/<br\s*\/?\s*>/ /igs;
	$data2Clean=~s/\s*<[^>]*?>\s*/ /igs;
	$data2Clean=~s/amp;//igs;
	$data2Clean=~s/&nbsp;/ /igs;
	$data2Clean=~s/^\s+|\s+$//igs;
	$data2Clean=~s/\s\s+/ /igs;
	$data2Clean=~s/^\W+$//igs;
	$data2Clean=~s/&\#39\;/\'/igs;
	$data2Clean=~s/\'/\'\'/igs;
	
	return($data2Clean);
}
1;