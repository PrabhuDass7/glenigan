#!/usr/bin/perl
use strict;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use File::Basename;
use Cwd  qw(abs_path);
use Config::Tiny;
use WWW::Mechanize;
use URI::URL;
use Redis;


############
# How to Run
#
# perl OnePageScraperPlanning.pl "27" "OnePage_Planning" "2018/06/04T11:30:00" "2018/06/04T11:30:00"
#
# For ONDEMND#
#
# perl OnePageScraperPlanning.pl 1 OnePage_Planning GCS001 09/07/2018 16/07/2018 "2018/07/16T10:53:00" "2018/07/16T10:53:00" onDemand
#
##############

####
# Declare global variables
####

my $councilCode = $ARGV[0];
my $category = $ARGV[1];
my $source = $ARGV[2];
my $fDate = $ARGV[3];
my $tDate = $ARGV[4];
my $startDateTime = $ARGV[5];
my $scheduleDateTime = $ARGV[6];
my $onDemandID = $ARGV[7];
my $onDemand = $ARGV[8];

$startDateTime=~s/T/ /s;
$scheduleDateTime=~s/T/ /s;
print "\n===========================================================================================================\n";
print "\tCouncil Code::$councilCode\tCategory::$category\tScriptStartDate::$startDateTime\tScheduleDateTime::$scheduleDateTime\n";
print "===========================================================================================================\n";

my $dt = DateTime->now(time_zone=>'local');
my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');

print "ScheduleDate==>$scheduleDate\n";
# <STDIN>;

my ($fromDate, $toDate, $rangeName,$Schedule_no,$categoryType);

my ($divCounter, $scheduleCounter);
my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
$basePath=~s/\/root\s*$//si;

# print "BasePath==>$basePath\n";

if($category=~m/^OnePage_Planning$/is)
{
	$categoryType="Planning";
}


####
# Declare and load local directories and required private module
####

my $ConfigDirectory = ($basePath.'/etc/OnePage');
my $libControlDirectory = ($basePath.'/lib/Control');
my $libCoreDirectory = ($basePath.'/lib/Core');
my $logsDirectory = ($basePath.'/logs');
my $logsDataDirectory = ($basePath.'/data');
my $searchLogsDirectoryPath = ($basePath.'/SearchResultLogs');


require ($libCoreDirectory.'/CouncilsScrapperCore.pm'); # Private Module
require ($libControlDirectory.'/OnePageCouncilsController.pm'); # Private Module
require ($libCoreDirectory.'/Glenigan_DB.pm'); # Private Module

####
# Create new object of class Redis_connection
####
my $redisConnection = &Glenigan_DB::Redis_connection();


####
# Create new object of class Config::Tiny
####

my ($regexFile,$councilDetailsFile) = Config::Tiny->new();


####
# Read INI files from the objects "$councilDetailsFile"
# These files contains - Details of councils, control properties for date range clculation and list of all councils repsectively
####

$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/OnePage_Council_Details.ini');
$regexFile = Config::Tiny->read($ConfigDirectory.'/OnePage_Regex_Heap.ini' );

####
# Get Council Name From INI file
####
my $councilName = $councilDetailsFile->{$councilCode}->{'COUNCIL_NAME'};

####
# Get Schedule number based on Forenoon or Afternoon
####
$Schedule_no=&CouncilsScrapperCore::scheduleNumber(localtime());


if(($onDemand eq '') or ($onDemand=~m/^\s*$/is))
{
	if( !$councilCode || !$category || !$startDateTime || !$scheduleDateTime) 
	{
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Input Argument Missing',$categoryType,$councilName);
		exit;
	}
}

####
# Get class name for Mechanize method
####
my ($councilApp,$pingStatus,$paginationContent) = &OnePageCouncilsController::setVerficationSSL($councilCode,$categoryType,'OnePage','17',$scheduleDate,$Schedule_no,$councilName);


if($pingStatus!~m/^\s*(200|Ok)\s*$/is)
{
	&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,'0',$councilCode,'Terminated',$categoryType,$councilName);	
	
	my $rawFileName;
	if($onDemand ne "")
	{
		$rawFileName=&OnePageCouncilsController::rawFileName($startDateTime,$councilCode,'ONDEMAND001',$categoryType);
	}
	else
	{
		$rawFileName=&OnePageCouncilsController::rawFileName($startDateTime,$councilCode,'GCS001',$categoryType);
	}
		
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContent,$searchLogsDirectoryPath,$categoryType,'Failed');	
	
	my $dt = DateTime->now;
	my $FromDate = $dt->ymd('-');
	my $dt2 = $dt->clone->subtract( weeks => 1);
	my $ToDate = $dt2->ymd('-');
	
	if($onDemand ne "")
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'ONDEMAND001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
		
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
		
		my $onDemandStatus;
		if($pingStatus > 500)
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		elsif( $pingStatus >= 400 and $pingStatus <= 499 )
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		else
		{
			$onDemandStatus = "Site not Working. Response code: $pingStatus";
		}
		
		&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
	}
	else
	{	
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, 'GCS001', $FromDate, $ToDate, '0', '0', $pingStatus, $rawFileName, $scheduleDate);
	}
	
	exit;
}


####
# Beginining of script execution
####

&main();



sub main
{
    # Calculating default Date Ranges based on values from control file
    my %defaultDateRanges;
	if(($source ne "") && ($fDate ne "") && ($tDate ne ""))
	{
		my $strp = DateTime::Format::Strptime->new(
        # pattern  => '%d/%m/%Y',
		pattern  => '%Y-%m-%d',
		);
		my $inputFromDate = $strp->parse_datetime( $fDate );
		my $inputToDate = $strp->parse_datetime( $tDate );
		# print "$inputFromDate<==>$inputToDate\n"; <STDIN>;
		$defaultDateRanges{$source} = [$inputFromDate, $inputToDate];
	}
	else
	{
		%defaultDateRanges = &CouncilsScrapperCore::defaultGcsRange( DateTime->now, $onDemand );
	}
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandUpdate($councilCode,$categoryType,$onDemandID);
	}

    # Dynamic Scheduling Logic (DSL) - 
    # Divides the the specific GCS schedule if the council website have cap-off on maximum search results returned
	
    foreach my $rangeName (sort keys %defaultDateRanges) 
    {
		print "\n#####################################################################\n";
		print "GCS:$rangeName\tCouncilCode:$councilCode\tCouncilName:$councilName\tCategory::$categoryType";
		print "\n#####################################################################\n";
        $divCounter = 1;
        $scheduleCounter = 0;
		
		####
		# Insert Scrape Status in Database
		####
		&CouncilsScrapperCore::writtingToLog('Start',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
		
		my $FromDate = $defaultDateRanges{$rangeName}[0];
		my $ToDate = $defaultDateRanges{$rangeName}[1];
		
		# print "FromDate:$FromDate\n";
		# print "ToDate:$ToDate\n";
				
		####
		# Insert Table: Scrape current Status and Council Type in Database
		####
		&CouncilsScrapperCore::scrapeStatusInsertion($Schedule_no,$scheduleDate,$rangeName,$councilCode,'Running',$categoryType,$councilName);
		
        
        my $checkResult = &defaultRangeCheck(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
		my (@totalApplicationLinks,$deDupRedisQuery,$pageSearchCode, $actualSearchCount);
		
        if ($checkResult == 1)
        {
			print "Search Page Response::$checkResult(Expected value also 1).\n";
            ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$defaultDateRanges{$rangeName}}[0], @{$defaultDateRanges{$rangeName}}[1]);
            my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($rangeName);		
			@totalApplicationLinks = @$recievedApplicationLinks;
			$pageSearchCode = $responseCode;
			$actualSearchCount = $SearchCount;
			$deDupRedisQuery = $deDupQuery;
        }
        else
        {
			print "Search Page Response::$checkResult(Expected value is 1).\n";
            my %subRanges = &calculateSubRanges($divCounter, @{$defaultDateRanges{$rangeName}}[1],$rangeName);
            my ($recievedApplicationLinks, $deDupQuery, $responseCode, $SearchCount) = &subRangesSearchResult(\%subRanges);		
			@totalApplicationLinks = @$recievedApplicationLinks;
			$pageSearchCode = $responseCode;	
			$actualSearchCount = $SearchCount;		
			$deDupRedisQuery = $deDupQuery;			
        }
			
		my $totalLinksCount =@totalApplicationLinks;
		print "Total Application Links Count for the date range ".$FromDate->dmy('/')." to ".$ToDate->dmy('/')." is \:\t$actualSearchCount\n";
		
		print "\nBut valid count for this date range is \:\t$totalLinksCount\n";
		
		my ($insertQuery, $actualScrappedCount) = &getApplicationDetails(\@totalApplicationLinks,$rangeName);	
		
		####
		# Insert application details in ScreenScrapper Database for Planning
		####
		&CouncilsScrapperCore::scrapeDetailsInsertion($insertQuery,$categoryType);
		# <STDIN>;		
		
		####
		# Get file name to store search page content in log folder
		####		
		my $rawFileName=&OnePageCouncilsController::rawFileName($startDateTime,$councilCode,$rangeName,$categoryType);			
		
		
		####
		# Insert actual scrapped details for Dashboard
		####
		&CouncilsScrapperCore::dashboardInsertion($councilCode, $councilName, $startDateTime, $rangeName, $fromDate, $toDate, $actualSearchCount, $actualScrappedCount, $pageSearchCode, $rawFileName, $scheduleDate);
		
		
		####
		# Insert deDup details
		####
		if($deDupRedisQuery ne "")
		{
			&CouncilsScrapperCore::deDupInsertion($deDupRedisQuery);
		}
		
		if($pageSearchCode=~m/^\s*(?:200|ok)\s*$/is)
		{	
			####
			# Update input date range search success response count into TBL_SCRAP_STATUS 
			####
			
			&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$actualSearchCount,$councilCode,'Completed',$categoryType,$councilName);	
		}
		
		####
		# Insert Scrape Status in onDemand Database
		####
		if($onDemand eq 'onDemand')
		{
			my $onDemandStatus;
			if($actualSearchCount ne "")
			{
				if(($actualSearchCount=~m/^0$/is) or ($actualSearchCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No Application found";
				}
				elsif(($totalLinksCount=~m/^0$/is) or ($totalLinksCount=~m/^\s*$/is))
				{
					$onDemandStatus = "No new application found";
				}
				else
				{
					$onDemandStatus = "\"$actualSearchCount\" Application found. But Valid application count: \"$totalLinksCount\"";
				}
			}
			&Glenigan_DB::onDemandStatusUpdation($councilCode,$categoryType,$onDemandID,$onDemandStatus);
		}	
		
		
		####
		# Insert Scrape Status in Database
		####
		
		&CouncilsScrapperCore::writtingToLog('End',$rangeName,$councilCode,$scheduleDate,$logsDirectory,$logsDataDirectory,$categoryType);
    }
	
	if($onDemand eq 'onDemand')
	{
		&Glenigan_DB::onDemandDelete($councilCode,$categoryType,$onDemandID);
	}
}




####
# Function to pass GCS sub-ranges dates to fetch search result
####

sub subRangesSearchResult
{
    my ($subRangesParam) = @_;
    my %subRanges = %$subRangesParam;
	
	my (@recAppLinks, $deDupQuery, $recAppLink, $responseCode, $SearchCount);
    foreach my $subRangeName (sort keys %subRanges)
    {
        ($fromDate, $toDate) = &CouncilsScrapperCore::setDateRanges(@{$subRanges{$subRangeName}}[0], @{$subRanges{$subRangeName}}[1]);
        ($recAppLink, $deDupQuery, $responseCode, $SearchCount) = &fetchSearchResult($subRangeName); 
		my @recAppLink = @$recAppLink;	
		push(@recAppLinks,@recAppLink);
    }

    return(\@recAppLinks, $deDupQuery, $responseCode, $SearchCount);
}

####
# Function to match the search result count from response
####

sub fetchSearchResult
{
    my($passedsubRangeName) = @_;
    my($difference);

	my @totalURL;
    $difference = $toDate->delta_days( $fromDate );
    $scheduleCounter = $scheduleCounter + $difference->delta_days;
    print "GCS::$passedsubRangeName ==> FromDate::".$fromDate->dmy('/')." to ToDate::".$toDate->dmy('/')." ~ NoOfDaysRange::".$difference->delta_days."\t";
    my ($responseContents,$responseStatus,$currentPageURL) = &callCouncilsMechMethod($fromDate, $toDate);
	
	# open(PP,">a.html");
	# print PP "$responseContents";
	# close(PP);
	# exit;
	
    # Get number of results
	my $searchResultCount;
	if($responseContents =~ /$councilDetailsFile->{$councilCode}->{'SEARCH_RESULTS_COUNT_RX'}/is)
	{
		$searchResultCount = $1;
	}
	
	if($searchResultCount eq "")
	{
		my $resultcnt=0;
		while($responseContents =~ m/$councilDetailsFile->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
		{
			$resultcnt++;
		}
		
		$searchResultCount = $resultcnt;
	}
	
	if($searchResultCount eq "")
	{
		if($responseContents =~ m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
		{
			$searchResultCount = "1";
		}
	}
    print "SearchResult: $searchResultCount\n";
	
	
	####
	# Insert input date range ping response into DB 
	####
	if($responseStatus!~m/^\s*(?:200|ok)\s*$/is) # Only If Fails
	{
		&CouncilsScrapperCore::writtingToStatus($passedsubRangeName,$councilCode,$councilName,$responseStatus,$logsDirectory,$categoryType);

		####
		# Update input date range search terminated response count into TBL_SCRAP_STATUS 
		####
		&CouncilsScrapperCore::scrapeStatusUpdation($Schedule_no,$scheduleDate,$searchResultCount,$councilCode,'Terminated',$categoryType,$councilName);		
	}
	
	my $paginationContents = $responseContents;
	
	
	my $rawFileName=&OnePageCouncilsController::rawFileName($startDateTime,$councilCode,$passedsubRangeName,$categoryType);	
	####
	# For QC check: result of search page content
	####
	&CouncilsScrapperCore::writtingToSearchResultLog($rawFileName,$paginationContents,$searchLogsDirectoryPath,$categoryType,'Success');	
		
	
	my ($totalURL, $duplicateQuery) = &getApplicationLinks($paginationContents, $passedsubRangeName,$currentPageURL);
	@totalURL=@$totalURL;
	
    return(\@totalURL, $duplicateQuery, $responseStatus, $searchResultCount);
}


####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationLinks()
{
	my $totalContent = shift;
	my $gcsRange = shift;
	my $cPageURL = shift;
		
	my (@appLinks, $deDup_Query); 
	
	if($totalContent =~ m/$councilDetailsFile->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/is)
	{
		# while($totalContent =~ m/$regexFile->{'MainRegexControl'}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
		while($totalContent =~ m/$councilDetailsFile->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/igs)
		{
			my $firstValue = $1;
			my $secValue = $2;
			
			my ($ApplicationNumber,$Applink);
			
			if($councilCode=~m/^50$/is)
			{
				my $ApplicationNo = $firstValue;
				$ApplicationNo=~s/\//%2F/igs;
				$Applink="https://www.west-dunbarton.gov.uk/uniform/dcdisplayfull.asp?vUPRN=".$ApplicationNo."&vPassword=&View1=View";
				$ApplicationNumber = $firstValue;
			}
			elsif($councilCode=~m/^(147)$/is)
			{			
				$ApplicationNumber = $firstValue;
				my $appNum = $secValue;
				$Applink = "https://ceredigion-online.tascomi.com/planning/index.html?fa=getApplication&id=$appNum";
			}
			elsif($councilCode=~m/^(16)$/is)
			{
				$ApplicationNumber = $firstValue;
				
				$Applink=$cPageURL."#VIEW?RefType=APPPlanCase&KeyNo=0&KeyText=$ApplicationNumber";
			}
			elsif($councilCode=~m/^(185)$/is)
			{
				$ApplicationNumber = $firstValue;
				
				$Applink="https://www.lewes-eastbourne.gov.uk/planning/application-summary/?RefType=APPPlanCase&KeyText=$ApplicationNumber";
			}
			elsif($councilCode=~m/^(285)$/is)
			{
				my $ApplicationNo = $1 if($totalContent=~m/\{\"FieldName\"\:\"ref_no\"\,\"Label\"\:\"[^\"]*?\"\,\"Value\"\:\"\s*$firstValue\s*\"\s*[\w\W]*?\,\"Label\"\:\"Key\s*Number\"\,\"Value\"\:\"([^\"]*?)\"/is);
				$ApplicationNumber = $firstValue;
				$Applink=$cPageURL."#VIEW?RefType=PBDC&KeyNo=$ApplicationNo";
			}
			elsif($councilCode=~m/^(424)$/is)
			{
				my $ApplicationNo = $1 if($totalContent=~m/\{\"FieldName\"\:\"SDescription\"\,\"Label\"\:\"[^\"]*?\"\,\"Value\"\:\"\s*$firstValue\s*\"\s*[\w\W]*?\,\"KeyNumber\"\:\"([^\"]*?)\"/is);
				$ApplicationNumber = $firstValue;
				
				$Applink=$cPageURL."#VIEW?RefType=GFPlanning&KeyNo=".$ApplicationNo."&KeyText=Subject";
			}
			elsif($councilCode=~m/^(20|31|32|102|107|119|134|143|235|276|329|370|377|388|395|397|414|433|438|475|477)$/is)
			{
				$ApplicationNumber = $secValue;
				$Applink=$firstValue;
			}
			elsif($councilCode=~m/^(437)$/is)
			{
				my $ApplicationNo = $firstValue;
				# $ApplicationNo=~s/\//%2F/igs;
				$Applink="https://webapps.westdorset-weymouth.gov.uk/PlanningApps/Pages/Planning.aspx?App=".$ApplicationNo;
				$ApplicationNumber = $firstValue;
			}
			else
			{
				$ApplicationNumber=$firstValue;
				$Applink=$secValue;
			}
					
			# print "Before::$Applink\n";
			my $fullAppPageLink;		
			if($Applink!~m/https?\:/is)
			{
				$fullAppPageLink = URI::URL->new($Applink)->abs( $councilDetailsFile->{$councilCode}->{'URL'}, 1 );
			}	
			else
			{
				$fullAppPageLink = $Applink;
			}	
			$fullAppPageLink =~ s/\&amp\;/\&/g;
			
			if($councilCode=~m/^(388)$/is)
			{
				$fullAppPageLink =~s/custom\/planning\/custom\/planning/custom\/planning/is;
			}
			
			
			if($councilCode=~m/^(107|197|329|377|388)$/is)
			{
				$fullAppPageLink =~s/\'\'/\'/igs;
			}
			else
			{
				$fullAppPageLink =~s/\'/\'\'/igs;
			}
			
			# print "After::$fullAppPageLink\n"; 
			# print "ApplicationNumber::$ApplicationNumber\n"; 
			# <STDIN>;
					
			if($redisConnection->exists($councilCode."_".$ApplicationNumber))	
			{
				print "Exists : $ApplicationNumber\n";	
				
				my $AppPageLink;
				if($councilCode=~m/^(107|197|329|377|388)$/is)
				{
					$AppPageLink= $fullAppPageLink;
					$AppPageLink =~s/\'/\'\'/igs;
				}
				else
				{
					$AppPageLink= $fullAppPageLink;
				}
				
				my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$ApplicationNumber\', \'$AppPageLink\', \'$categoryType\', \'$gcsRange\',\'$councilName\'),";
				$deDup_Query.=$deDupQuery;
				
				next;
			}
		
			
			push ( @appLinks, $fullAppPageLink);
		}
	}
	else
	{
		my ($applicationDetailsPageContent,$pageResponse) = &OnePageCouncilsController::getMechContent($cPageURL,$councilApp);
				
		my $ApplicationNumber = $1 if($applicationDetailsPageContent =~ m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is);
		
		$ApplicationNumber = &OnePageCouncilsController::htmlTagClean($ApplicationNumber);
		
		if($redisConnection->exists($councilCode."_".$ApplicationNumber))	
		{
			print "Exists : $ApplicationNumber\n";	
			
			my $deDupQuery="($Schedule_no, \'$scheduleDate\', $councilCode, \'$ApplicationNumber\', \'$cPageURL\', \'$categoryType\', \'$gcsRange\',\'$councilName\'),";
			$deDup_Query.=$deDupQuery;
			
			next;
		}
			
		push ( @appLinks, $cPageURL);
	}
	
	# print "deDup_Query==>$deDup_Query\n"; <STDIN>;
	
	return(\@appLinks, $deDup_Query);
}



####
# Method to fetch application URLs from the resulting search result page
####

sub getApplicationDetails()
{
    my $passedApplicationLink = shift;
    my $rangeName = shift;
	
	my @passedApplicationLinks = @{$passedApplicationLink};


	my $insertQuery;
	
	print "GCS==>$rangeName\n";
	
	my $resultsCount=0;
	my ($fullAppPageLink);
    foreach my $eachApplicationLink (@passedApplicationLinks)
    {		
		$fullAppPageLink=$eachApplicationLink;
		# print "Going to fetch ==> $fullAppPageLink\n";
		my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &OnePageCouncilsController::getApplicationDetails($fullAppPageLink, $councilApp, $councilCode, $categoryType);
		
		if(($applicationNumber ne "") && ($pageResponse=~m/^\s*(200|ok)\s*$/is))
		{
			$resultsCount++;
			
			## Insert new application in redis server			
			$redisConnection->set($councilCode."_".$applicationNumber => $applicationNumber);
			print "Not Exists : $applicationNumber\n";
		}
		
		my $tempSource;
		if($onDemand eq 'onDemand')
		{
			$tempSource = "onDemand_".$rangeName;
		}
		else
		{
			$tempSource = $rangeName;
		}
		
		if($applicationNumber ne "")
		{		
			my ($insert_query_From_Func) = &OnePageCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$fullAppPageLink,$tempSource,$councilCode,$councilName,$scheduleDate,$scheduleDateTime,$categoryType, $councilApp);
			
			$insertQuery.=$insert_query_From_Func;
		}
    }
	
	# print "$resultsCount\n"; 
		
	return($insertQuery, $resultsCount);
}

####
# Common method to inisialise Mechanize user agent
####

sub callCouncilsMechMethod
{
    my($passedFromDate, $passedToDate) = @_;
	my ($councilAppResponse, $scriptStatus,$currentPageURL) = &OnePageCouncilsController::councilsMechMethod($passedFromDate->dmy('/'), $passedToDate->dmy('/'), $councilCode, $councilApp, $categoryType);
    return ($councilAppResponse, $scriptStatus, $currentPageURL);
}


####
# Function to check the default date rnages from the GCS schedule needs to be split
####

sub defaultRangeCheck
{
    my ($passedFromDate, $passedToDate) = @_;

    return( &rangeCheckMethod($passedFromDate, $passedToDate) );
}


####
# Function to check the search result page response to see if it returns in error
####

sub rangeCheckMethod
{
    my ($passedFromDate,$passedToDate) = @_;

    my ($responseContents,$responseStatus,$currentPageURL) = &callCouncilsMechMethod($passedFromDate, $passedToDate);
	
	# open(PP,">a.html");
	# print PP "$responseContents";
	# close(PP);
	# exit;
	
    my $searchValue;
    
	if($responseContents=~m/There\s*was\s*a\s*problem\s*with\s*your\s*request/is)
	{	
		next;
	}
	elsif($responseContents=~m/>\s*No\s*applications\s*were\s*found/is)
	{	
		next;
	}
	elsif($responseContents=~m/<p>\s*This\s*application\s*number\s*cannot\s*be\s*found\!\s*<\/p>/is)
	{	
		next;
	}
	elsif($responseContents=~m/\{\"KeyObjects\"\:\[\]\,\"TotalRows\"\:0\}/is)
	{	
		next;
	}
	elsif($responseContents=~m/No\s*records\s*returned/is)
	{	
		next;
	}
	elsif($responseContents=~m/A\s*problem\s*has\s*ocurred\s*in\s*the\s*repeater\s*process/is)
	{	
		next;
	}
	elsif($responseContents=~m/\s*No\s*applications\s*found\s*/is)
	{	
		next;
	}
	elsif($responseContents=~m/>\s*No\s*data\s*Found/is)
	{	
		next;
	}
	elsif($responseContents=~m/Your\s*search\s*criteria\s*returned\s*no\s*results/is)
	{	
		next;
	}	
	elsif($responseContents=~m/>\s*Please\s*try\s*again\s*with\s*new\s*search\s*criteria/is)
	{	
		next;
	}	
	elsif($responseContents=~m/The\s*start\s*date\s*must\s*precede\s*the\s*end\s*date/is)
	{	
		next;
	}	
	elsif($responseContents=~m/Please\s*try\s*again\s*with\s*less\s*restrictive\s*criteria/is)
	{	
		next;
	}
	elsif($responseContents=~m/Service\s*Unavailable/is)
	{	
		next;
	}
	elsif($responseContents=~m/Server\s*closed\s*connection/is)
	{	
		next;
	}
	elsif($responseContents=~m/>\s*Number\s*of\s*cases\s*\:?\s*<strong>\s*0\s*<\/strong>/is)
	{	
		next;
	}
	elsif($responseContents=~m/>\s*Your\s*search\s*has\s*returned\s*too\s*many\s*results\s*please\s*go\s*back\s*to\s*the\s*search\s*page/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/There\s*are\s*too\s*many\s*results\s*to\s*display\,\s*the\s*most\s*recent\s*300\s*results/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/>\s*Number\s*of\s*cases\s*\:?\s*(500)\s*\(/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/>\s*Number\s*of\s*cases\s*\:?\s*(?:<[^>]*?>)?\s*(500)\s*\(/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/The\s*number\s*of\s*applications\s*found\s*exceeds\s*200/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/No\s*matches\s*found/is)
	{	
		return(0);
	}
	elsif(($councilCode=~m/^397$/is) && ($responseContents=~m/Application\s*Count\:\s*(\d+)\s*</is))
	{	
		my $appCount = $1;
		if($appCount >= 500)
		{
			return(0);
		}
		elsif($appCount <= 499)
		{
			return(1);
		}
	}
	elsif($responseContents=~m/please\s*refine\s*your\s*criteria/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/please\s*refine\s*your\s*criteria/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_PAGE_CONTENT_RX'}/is)
	{	
		return(1);
	}
	elsif($responseContents=~m/$regexFile->{$councilCode}->{'PLANNING_APPLICATION'}/is)
	{	
		return(1);
	}
	elsif($responseContents=~m/$councilDetailsFile->{$councilCode}->{'ERR_MSG_RX'}/is)
	{	
		$searchValue = $1;
	}
	
	if($searchValue=~m/WARNING/s)
    {
        next;
    }
	elsif($searchValue=~m/No\s*results\s*found/is)
	{	
		next;
	}
	elsif($searchValue=~m/Please\s*try\s*again/is)
	{	
		next;
	}
	elsif($searchValue=~m/Sorry\,\s*no\s*applications/is)
	{	
		next;
	}
	elsif($searchValue=~m/No\s*data\s*Found/is)
	{	
		next;
	}
	elsif($searchValue=~m/^0$/is)
	{
		next;
	}
	elsif($searchValue=~m/\s*No\s*applications\s*found\s*/is)
	{	
		next;
	}
	elsif($searchValue=~m/No\s*Match/is)
	{
		next;
	}
	elsif($searchValue=~m/No\s*Result/is)
	{
		next;
	}
	elsif($searchValue=~m/The\s*start\s*date\s*must\s*precede\s*the\s*end\s*date/is)
	{	
		next;
	}	
	elsif($searchValue=~m/No\s*applications\s*found/is)
	{
		next;
	}
	elsif($searchValue=~m/>\s*No\s*applications\s*were\s*found/is)
	{	
		next;
	}
	elsif($searchValue=~m/There\s*was\s*a\s*problem\s*with\s*your\s*request/is)
	{
		next;
	}
	elsif($searchValue=~m/Your\s*search\s*criteria\s*returned\s*no\s*results/is)
	{
		next;
	}
	elsif($searchValue=~m/^\s*$/is)
	{
		next;
	}
	elsif($searchValue=~m/range\s*of\s*a\s*month\s*or\s*less/is)
	{	
		return(0);
	}
	elsif($searchValue=~m/The\s*number\s*of\s*applications\s*found\s*exceeds\s*200/is)
	{	
		return(0);
	}
	elsif($searchValue=~m/The\s*first\s*500\s*records\s*will\s*be\s*returned/is)
	{	
		return(0);
	}
	elsif($searchValue=~m/please\s*refine\s*your\s*criteria/is)
	{	
		return(0);
	}
	elsif($responseContents=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_RESULTS_COUNT_RX'}/is)
	{
		return(1);
	}
	elsif($searchValue=~m/$councilDetailsFile->{$councilCode}->{'SEARCH_RESULTS_COUNT_RX'}/is)
	{
		return(1);
	}
    else
    {
        next;
    }
}


####
# Function to calculate GCS sub-ranges and pass the resulting date range into rangeCheckMethod is see the $resultValue results with 1
####

sub calculateSubRanges
{
    my ($divCount, $passedToDate, $gcsRange) = @_;
    my (%subRangeCheck, $resultValue);
	print "gcsRange==>$gcsRange\n";
	
    do
    {
        %subRangeCheck = &CouncilsScrapperCore::subRangesCalcMethod($divCount, $passedToDate, $onDemand);
        my ($subRangeFromDate, $subRangeToDate);
		if($onDemand ne "")
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'ONDEMAND001'}}[0], @{$subRangeCheck{'ONDEMAND001'}}[1]);
		}
		else
		{
			($subRangeFromDate, $subRangeToDate) = &CouncilsScrapperCore::setDateRanges(@{$subRangeCheck{'GCS001'}}[0], @{$subRangeCheck{'GCS001'}}[1]);
		}
        $resultValue = &rangeCheckMethod($subRangeFromDate, $subRangeToDate);
        $divCount = $divCount + 1;
		if($divCount==5)
		{
			# last;
			return(%subRangeCheck);
		}

    } while ($resultValue == 0);
	
    return(%subRangeCheck);
}