use strict;
use WWW::Mechanize;
use URI::URL;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);
use Config::Tiny;
use URI::Escape;
use DateTime;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use Spreadsheet::ParseExcel;
use Spreadsheet::ParseXLSX;
use Spreadsheet::XLSX;
use Parallel::ForkManager;
use File::Path qw(make_path);

######### Get Directory path Local ############

my $basePath = dirname (dirname abs_path $0); # Set root path for accessing files
# print "$basePath\n"; <STDIN>;


######### Get Current Local date ############
my $dt = DateTime->now();
my $todayDate = $dt->dmy('');

print "todayDate==>$todayDate\n";

my $categoryType = $ARGV[0];

if($categoryType eq "")
{
	print "\nCouncil category type is missing!!!\n";
	exit;
}

####
# Declare and load local directories
####

my $ConfigDirectory = ($basePath.'/etc');
my $onlineConfigDirectory = ($ConfigDirectory.'/Online');
my $apacheConfigDirectory = ($ConfigDirectory.'/Apache');
my $eplanningConfigDirectory = ($ConfigDirectory.'/ePlanning');
my $fastwebConfigDirectory = ($ConfigDirectory.'/Fastweb');
my $northgateConfigDirectory = ($ConfigDirectory.'/Northgate');
my $ocellaConfigDirectory = ($ConfigDirectory.'/Ocella');
my $onepageConfigDirectory = ($ConfigDirectory.'/OnePage');
my $servletConfigDirectory = ($ConfigDirectory.'/Servlet');
my $simpleConfigDirectory = ($ConfigDirectory.'/Simple');
my $toughConfigDirectory = ($ConfigDirectory.'/Tough');
my $weeklyConfigDirectory = ($ConfigDirectory.'/Weekly');
my $wphConfigDirectory = ($ConfigDirectory.'/WPH');
my $dataDirectory = ($basePath.'/data');
# my $outputDirectory = ($basePath.'/output');
my $coreLibDirectory = ($basePath.'/lib/Core');
my $controlLibDirectory = ($basePath.'/lib/Control');

require ($coreLibDirectory.'/CouncilsScrapperCore.pm'); # Private Module
# require ($coreLibDirectory.'/Glenigan_DB.pm'); # Private Module
require ($controlLibDirectory.'/ApacheCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/ePlanningCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/FastwebCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/NorthgateCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/OcellaCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/OnePageCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/OnlineCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/ServletCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/SimpleCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/ToughCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/WeeklyCouncilsController.pm'); # Private Module
require ($controlLibDirectory.'/WPHCouncilsController.pm'); # Private Module



####
# Create new object of class Redis_connection
####
# my $redisConnection = &Glenigan_DB::Redis_connection_Decision();


####
# Create new object of class Config::Tiny
####

my ($councilDetailsFile, $onlineCouncil, $apacheCouncil, $eplanningCouncil, $fastwebCouncil, $northgateCouncil, $ocellaCouncil, $onepageCouncil, $servletCouncil, $simpleCouncil, $toughCouncil, $weeklyCouncil, $wphCouncil, $fastwebRegex) = Config::Tiny->new();


####
# Create new object of class WWW::Mechanize
####
my $mech = WWW::Mechanize->new( 
						ssl_opts => {
										SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
										verify_hostname => 0, 
									}
									, autocheck => 0
						 );

####
# Read INI files
####
$councilDetailsFile = Config::Tiny->read($ConfigDirectory.'/Outstanding_Format.ini');
$onlineCouncil = Config::Tiny->read($onlineConfigDirectory.'/Online_Council_Decision_Details.ini');
# $apacheCouncil = Config::Tiny->read($apacheConfigDirectory.'/Apache_Council_Decision_Details.ini');
# $eplanningCouncil = Config::Tiny->read($eplanningConfigDirectory.'/ePlanning_Council_Decision_Details.ini');
# $fastwebCouncil = Config::Tiny->read($fastwebConfigDirectory.'/Fastweb_Council_Decision_Details.ini');
# $northgateCouncil = Config::Tiny->read($northgateConfigDirectory.'/Northgate_Council_Decision_Details.ini');
$ocellaCouncil = Config::Tiny->read($ocellaConfigDirectory.'/Ocella_Council_Decision_Details.ini');
$onepageCouncil = Config::Tiny->read($onepageConfigDirectory.'/OnePage_Council_Decision_Details.ini');
$servletCouncil = Config::Tiny->read($servletConfigDirectory.'/Servlet_Council_Decision_Details.ini');
# $simpleCouncil = Config::Tiny->read($simpleConfigDirectory.'/Simple_Council_Decision_Details.ini');
$toughCouncil = Config::Tiny->read($toughConfigDirectory.'/Tough_Council_Decision_Details.ini');
# $weeklyCouncil = Config::Tiny->read($weeklyConfigDirectory.'/Weekly_Council_Decision_Details.ini');
$wphCouncil = Config::Tiny->read($wphConfigDirectory.'/WPH_Council_Decision_Details.ini');
$fastwebRegex = Config::Tiny->read($fastwebConfigDirectory.'/Fastweb_Regex_Heap_Decision.ini' );


####
# Read Input excel files from the data folder
####
opendir(DIR, $dataDirectory) || die "can't opendir $dataDirectory: $!";
my @xlsxfiles = grep { /.xlsx|xls/ } readdir(DIR);
closedir DIR;

foreach my $File (@xlsxfiles) 
{
	my $inputFile=$dataDirectory."/".$File;

	print "inputFile==>$inputFile\n";

	next if($File!~m/Input2-6.xlsx/is);
# =pod
	####
	# Create new object of class Spreadsheet::ParseExcel
	####
	my $parser;
	if($inputFile=~m/xlsx$/is)
	{
		$parser = Spreadsheet::ParseXLSX->new();
	}
	elsif($inputFile=~m/xls$/is)
	{
		$parser = Spreadsheet::ParseExcel->new();
	}
	my $workbook = $parser->parse($inputFile);
	
	if ( !defined $workbook ) {
		die $parser->error(), ".\n";
	}

	my $count = 0;
	my $tempQuery;
	foreach my $worksheet ($workbook->worksheets()) {
	
		my ( $row_min, $row_max ) = $worksheet->row_range();
		my ( $col_min, $col_max ) = $worksheet->col_range();
		
		for my $row ( 1 .. $row_max ) {
			my $cell1 = $worksheet->get_cell( $row, 0 );
			my $cell2 = $worksheet->get_cell( $row, 1 );
			my $cell3 = $worksheet->get_cell( $row, 2 );
			next unless $cell1;
			next unless $cell2;
			next unless $cell3;
			
			my $applicationNo = $cell1->value();
			my $councilCode = $cell2->value();
			my $councilName = $cell3->value();
			
			# print "$applicationNo\n";
			# print "$councilCode\n";
			# print "$councilName\n";
			
		
			my $cCode 			= $councilCode;
			my $cName 			= $councilName;
			my $appNo			= $applicationNo;
			
			chomp($appNo);
			chomp($cCode);
			chomp($cName);
					
			if($cCode=~m/^(501|502|503|504|505|506|507|508|509|510|511|512|513|514|515|516|517|518|519|520|521|522|523|524|525)$/is)
			{
				$cCode = "500";
			}
			# elsif($cCode=~m/^(430)$/is)
			# {
				# $cCode = "363";			
			# }
			elsif($cCode=~m/^(431)$/is)
			{
				$cCode = "437";			
			}
			elsif($cCode=~m/^(401)$/is)
			{
				$cCode = "391";			
			}
			elsif($cCode=~m/^(389)$/is)
			{
				$cCode = "259";			
			}
			elsif($cCode=~m/^(359)$/is)
			{
				$cCode = "156";			
			}
			elsif($cCode=~m/^(319)$/is)
			{
				$cCode = "131";			
			}
			elsif($cCode=~m/^(101)$/is)
			{
				$cCode = "272";			
			}
			
			my $Format = $councilDetailsFile->{'Format'}->{$cCode};
			
			print "councilCode: $cCode\n";
			print "Format: $Format\n";
			print "Application: $appNo\n";
							
			my $tempSource = 'OUT001';				
			my $scheduleDate = $dt->mdy('/'). " " . $dt->hms(':');
			my $scheduleDateTime = $dt->ymd('/'). " " . $dt->hms(':');
			# if($Format=~m/^Northgate$/is)
			# {
			if($Format=~m/^Online$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &OnlineCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($responseContent=~m/<div\s*class=\"messagebox\">\s*([\w\W]*?)\s*<\/div>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*results\s*found/is);
				}
				elsif($searchStatus=~m/^200$/is)
				{			

					$fullAppPageLink = $1 if($responseContent=~m/<a[^>]*?href=\"([^\"]*?)\"[^>]*?>\s*<span[^>]*?>\s*Summary\s*<\/span>/is);
					$fullAppPageLink = URI::URL->new($fullAppPageLink)->abs( $onlineCouncil->{$cCode}->{'URL'}, 1 );
					$fullAppPageLink=&URLclean($fullAppPageLink);
					my @activeTabList = ('summary', 'details', 'dates');
					my $forks = 3;
					# print "Forking up to $forks at a time\n";

					my $parallelScrapeManager = Parallel::ForkManager->new($forks);
					my %results;
					
					my ($applicationNumber);
					my ($activeTabCnt,$pageResponse,$appNumber);
					$parallelScrapeManager->run_on_finish( sub {
						my ($pid, $exit_code, $ident, $exit_signal, $core_dump, $data_structure_reference) = @_;
						my $activeTab = $data_structure_reference->{input}; ## for active tab
						$results{$activeTab} = $data_structure_reference->{result};	## for get active tab page content
						$pageResponse = $data_structure_reference->{response};	## for get active tab page ping response
						$appNumber = $data_structure_reference->{number}; ## for get application page number
						$applicationNumber = $appNumber;
					});
					
					
					foreach my $activeTab (@activeTabList)
					{
						my $pid = $parallelScrapeManager->start and next;
						# print "New started :$pid\n"; 
						
						my $applicationLinkUri = URI::URL->new($fullAppPageLink)->abs( $onlineCouncil->{$cCode}->{'URL'}, 1 );
						# print "applicationLinkUri :$applicationLinkUri\n"; 
						my %queryParameters = $applicationLinkUri->query_form;
			
						$applicationLinkUri->query_form( activeTab => $activeTab, keyVal => $queryParameters{keyVal} );
						
						# print "AppPageLink==>$applicationLinkUri\n"; <STDIN>;
						my ($applicationPageContent,$applicationPageContentResponse, $appNumber) = &OnlineCouncilsController::getApplicationDetails($applicationLinkUri, $mech, $activeTab, $categoryType,$cCode);
											
						print "ActiveTab:::$activeTab==>PageResponse:::$applicationPageContentResponse\n";
						
						# print "$pid Ended\n";
						
						$parallelScrapeManager->finish(0, { result => $applicationPageContent, input => $activeTab, response => $applicationPageContentResponse, number => $appNumber});
					}
					
					$parallelScrapeManager->wait_all_children;
					
					my %dataDetails;
					foreach my $activeTab(sort keys %results)
					{	
						# print "$activeTab<=>$results{$activeTab}\n";
						# <STDIN>;
						my %activeTabContents  = &OnlineCouncilsController::tabDetailsCollection($results{$activeTab}, $activeTab, $categoryType);
											
						%dataDetails = (%activeTabContents,%dataDetails);
					}

					print "applicationNumber==>$applicationNumber\n";
					
					if($applicationNumber ne "")
					{	
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &OnlineCouncilsController::applicationsDataDumpPlanning(\%dataDetails,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &OnlineCouncilsController::applicationsDataDumpDecision(\%dataDetails,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime);
						}
						
						print "$insert_query_From_Func\n"; 
						# <STDIN>;
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^WPH$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &WPHCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($responseContent=~m/<td[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>/is)
				{
					my $currentPageURL=$1;
					$currentPageURL = URI::URL->new($currentPageURL)->abs( $wphCouncil->{$cCode}->{'URL'}, 1 );
					$currentPageURL=&URLclean($currentPageURL);
					print "currentPageURL==>$currentPageURL\n"; 
					
					my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &WPHCouncilsController::getApplicationDetails($currentPageURL, $mech, $cCode, $categoryType);
					
					if($applicationNumber ne "")
					{
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &WPHCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &WPHCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####						
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^Ocella$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &OcellaCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				if($responseContent=~m/<td[^>]*?>\s*<strong>\s*<span\s*style=color\:red>\s*([^<]*?)\s*<\/span>\s*<\/strong>\s*<\/td>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*applications\s*found/is);
				}
				
				if($responseContent=~m/<td[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>/is)
				{
					my $currentPageURL=$1;
					$currentPageURL = URI::URL->new($currentPageURL)->abs( $ocellaCouncil->{$cCode}->{'URL'}, 1 );
					$currentPageURL=&URLclean($currentPageURL);
					print "currentPageURL==>$currentPageURL\n"; 
					
					my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &OcellaCouncilsController::getApplicationDetails($currentPageURL, $mech, $cCode, $categoryType);
					
					if($applicationNumber ne "")
					{
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &OcellaCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &OcellaCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^Northgate$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &NorthgateCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
			
				if($responseContent=~m/<p>\s*<span\s*id=\"lblPagePosition\s*\"[^>]*?>\s*([^<]*?)\s*<\/span>\s*<\/p>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*results\s*found/is);
				}
				my ($applicationDetailsPageContent) = &NorthgateCouncilsController::searchPageNext($cCode, $mech, $categoryType, $responseContent);
				
				my ($insert_query_From_Func);
				if($categoryType eq 'Planning')
				{
					($insert_query_From_Func) = &NorthgateCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,'OUT','2',$mech);				
				}
				elsif($categoryType eq 'Decision')
				{
					($insert_query_From_Func) = &NorthgateCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,'OUT','2',$mech);
				}
				
				
				print "$insert_query_From_Func\n"; 
				
				####
				# Insert application details in Glenigan Database for Decision
				####
				if($insert_query_From_Func!~m/^\(\'\'\,/is)
				{
					$count++;
					$tempQuery .= $insert_query_From_Func;
					# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
				}
			}
			elsif($Format=~m/^Fastweb$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &FastwebCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
							
				if($responseContent=~m/<\/h1>\s*<p[^>]*?>\s*([^<]*?)\s*<\/p>/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/No\s*results\s*were\s*found/is);
				}

				my $applicationNumber;
				if($responseContent=~m/$fastwebRegex->{$cCode}->{'PLANNING_APPLICATION'}/is)
				{
					my $matchedText = $1;
					$applicationNumber = &FastwebCouncilsController::htmlTagClean($matchedText);	
				}
				
							
				if($applicationNumber ne "")
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &FastwebCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &FastwebCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}			
			}
			elsif($Format=~m/^Servlet$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &ServletCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				# open(PP,">a.html");
				# print PP "$responseContent";
				# close(PP);
										
				if($responseContent=~m/>\s*(Sorry\,\s*but\s*your\s*query\s*did\s*not\s*return\s*any\s*results)/is)
				{
					my $messagebox = $1;
					next if($messagebox=~m/Sorry\,\s*but\s*your\s*query/is);
				}

				if($responseContent=~m/<td[^>]*?>\s*<a\s*href\s*=\s*\"([^\"]*?)\"[^>]*?>\s*[^<]*?\s*<\/a>\s*<\/td>/is)
				{
					my $currentPageURL=$1;
					
					$currentPageURL = URI::URL->new($currentPageURL)->abs( $servletCouncil->{$cCode}->{'URL'}, 1 );
					$currentPageURL=&URLclean($currentPageURL);
					# print "currentPageURL==>$currentPageURL\n"; 
					# <STDIN>;
					
					my ($applicationDetailsPageContent,$pageResponse,$applicationNumber) = &ServletCouncilsController::getApplicationDetails($currentPageURL, $mech, $cCode, $categoryType);
						
					if($applicationNumber ne "")
					{
						my ($insert_query_From_Func);
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &ServletCouncilsController::applicationsDataDumpPlanning($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &ServletCouncilsController::applicationsDataDumpDecision($applicationDetailsPageContent,$currentPageURL,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType,$mech);
						}					
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
				
			}
			elsif($Format=~m/^Apache$/is)
			{		
				my ($insert_query_From_Func, $fullAppPageLink, $searchStatus) = &ApacheCouncilsController::searchAppNumLogic($cCode, $cName, $appNo, $mech, $tempSource, $scheduleDate, $scheduleDateTime, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^ePlanning$/is)
			{	
				my ($responseContent, $fullAppPageLink, $searchStatus) = &ePlanningCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &ePlanningCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &ePlanningCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Simple$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &SimpleCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &SimpleCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &SimpleCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Weekly$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &WeeklyCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					
					if($cCode=~m/^(366)$/is)
					{				
						$insert_query_From_Func=$responseContent;
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
					else
					{
						if($categoryType eq 'Planning')
						{
							($insert_query_From_Func) = &WeeklyCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
						}
						elsif($categoryType eq 'Decision')
						{
							($insert_query_From_Func) = &WeeklyCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
						}
						
						
						print "$insert_query_From_Func\n"; 
						
						####
						# Insert application details in Glenigan Database for Decision
						####
						if($insert_query_From_Func!~m/^\(\'\'\,/is)
						{
							$count++;
							$tempQuery .= $insert_query_From_Func;
							# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
						}
					}
				}
			}
			elsif($Format=~m/^OnePage$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &OnePageCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &OnePageCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &OnePageCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}
			elsif($Format=~m/^Tough$/is)
			{
				my ($responseContent, $fullAppPageLink, $searchStatus) = &ToughCouncilsController::searchAppNumLogic($cCode, $appNo, $mech, $categoryType);
				
				if($searchStatus=~m/^200$/is)
				{
					my ($insert_query_From_Func);
					if($categoryType eq 'Planning')
					{
						($insert_query_From_Func) = &ToughCouncilsController::applicationsDataDumpPlanning($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					elsif($categoryType eq 'Decision')
					{
						($insert_query_From_Func) = &ToughCouncilsController::applicationsDataDumpDecision($responseContent,$fullAppPageLink,$tempSource,$cCode,$cName,$scheduleDate,$scheduleDateTime,$categoryType, $mech);
					}
					
					
					print "$insert_query_From_Func\n"; 
					
					####
					# Insert application details in Glenigan Database for Decision
					####
					if($insert_query_From_Func!~m/^\(\'\'\,/is)
					{
						$count++;
						$tempQuery .= $insert_query_From_Func;
						# &CouncilsScrapperCore::scrapeDetailsInsertion($insert_query_From_Func,$categoryType);
					}
				}
			}

			if($count=~m/^500$/is)
			{
				$tempQuery=~s/\,\s*$//gsi;
		
				&CouncilsScrapperCore::scrapeDetailsInsertion($tempQuery,$categoryType);
				undef $tempQuery;
				
				# print "query==>$tempQuery\n";<STDIN>;
				
		
				$count = 0;
			}
			# }
		}

		$tempQuery=~s/\,\s*$//gsi;

		&CouncilsScrapperCore::scrapeDetailsInsertion($tempQuery,$categoryType);
		undef $tempQuery;
		
		# print "query==>$tempQuery\n";<STDIN>;
				
	}
# =cut
}

sub URLclean()
{
	my $url = shift;
	
	$url=~s/\&amp\;/\&/gsi;
	$url=~s/\'/\'\'/gsi;
	
	return $url;	
}