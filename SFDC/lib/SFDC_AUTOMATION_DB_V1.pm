package SFDC_AUTOMATION_DB_V1;
### Package Name ####
use strict;
use Text::CSV;
use DBI;
use DBI qw{:utils :sql_types};
# use DBIx::Interpolate qw(:all);
use DBD::SQLAnywhere;
use DBD::ODBC;
use MIME::Lite;
use MIME::Base64;
use Config::IniFiles;
use Data::Dumper;
use DateTime;
use Math::Round;
use Cwd qw(abs_path);
use File::Basename;
use File::Path qw(make_path remove_tree);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root$//is;

# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $logsDirectory = ($basePath.'/logs');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

# my $FilePath = $dataDirectory;

####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd('');

###### DB Connection ######
sub DbConnection()
{
	my $DBDETAILS 		= &ReadIniFile('SFDC_DB_SQL');
	my %Credentials		= %{$DBDETAILS};
	my $servername = $Credentials{'server_name'};
	print "ServerName :: $servername\n";

	my $dsn ='driver={SQL Server};Server='.$servername.';database='.$Credentials{'database_name'}.';uid='.$Credentials{'database_user'}.';pwd='.$Credentials{'database_pass'}.';';
	my $dbh  = DBI->connect("DBI:ODBC:$dsn",{AutoCommit => 0, RaiseError => 1, PrintError => 0, mysql_enable_utf8 => 1}) or die "$DBI::errstr\n";
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 900000;
	return $dbh;
}

###### DB Retrieve_Schedule_Logic ######
sub Retrieve_Schedule_LogicData()
{
	my $dbh	= shift;
	my $inputReportID	= shift;
	
	my $query_LogicData;
	if($inputReportID ne "")
	{
		$query_LogicData = "SELECT SR.ID as Report_Id,SR.Report_Name,SR.category,SP.ID as SP_Id,SP.Sp_Name,SPM.ID AS SPParam_Id,SPM.argument_count,SPM.Sp_Parameter,SPM.Multisheet_Flg,SPM.outfile_Name,SPM.Sheet_Name,SPM.Outfile_type,SPM.with_header,SPM.Output_split,SPM.Split_Count,SPM.Mail_attachement,SPM.FTP_Upload,SPM.Retryfrequencyminutes,SPM.Previous_run_duration,SPM.Timeout_Minutes,SPM.expected_count,
		CASE WHEN SR.ID IN (163, 1764) THEN '1' ELSE [Frequency_OrderId] END [Frequency_OrderId] 
		FROM Sfdc_Configurator..SFDC_Reports SR
		JOIN Sfdc_Configurator..SFDC_Reportfrequency SRF ON SR.id=SRF.Report_Id
		JOIN (select distinct ID,Value,case when Value='daily' THEN '1' 
		when Value='weekly' THEN '2'
		when Value='fortnightly' THEN '3'
		when Value='monthly' THEN '4'
		when Value='quarterly' THEN '5'
		when Value='bi-annual' THEN '6'
		when Value='annual' THEN '7' ELSE '' END AS [Frequency_OrderId]
		from Sfdc_Configurator..SFDC_ReportConfig
		where Config_Key='report_frequency') SRC ON SRF.frequency_id=src.ID
		JOIN Sfdc_Configurator..SFDC_SPList SP ON SR.ID=SP.Report_Id
		JOIN Sfdc_Configurator..SFDC_SPParameter SPM on SP.ID=SPM.SP_Id
		WHERE SR.active=1 and SP.active=1 and SPM.active=1 AND SR.id in ($inputReportID)
		AND SR.id not in (156,665)
		ORDER BY [Frequency_OrderId],sr.Id";		
	}
	else
	{
		$query_LogicData = "SELECT SR.ID as Report_Id,SR.Report_Name,SR.category,SP.ID as SP_Id,SP.Sp_Name,SPM.ID AS SPParam_Id,SPM.argument_count,SPM.Sp_Parameter,SPM.Multisheet_Flg,SPM.outfile_Name,SPM.Sheet_Name,SPM.Outfile_type,SPM.with_header,SPM.Output_split,SPM.Split_Count,SPM.Mail_attachement,SPM.FTP_Upload,SPM.Retryfrequencyminutes,SPM.Previous_run_duration,SPM.Timeout_Minutes,SPM.expected_count,
		CASE WHEN SR.ID IN (163, 19, 1764) THEN '1' 
			 WHEN SR.ID=67 THEN '3'
		ELSE [Frequency_OrderId] END [Frequency_OrderId] 
		FROM Sfdc_Configurator..SFDC_Reports SR
		JOIN Sfdc_Configurator..SFDC_Reportfrequency SRF ON SR.id=SRF.Report_Id
		JOIN (select distinct ID,Value,case when Value='daily' THEN '1' 
		when Value='weekly' THEN '2'
		when Value='fortnightly' THEN '3'
		when Value='monthly' THEN '4'
		when Value='quarterly' THEN '5'
		when Value='bi-annual' THEN '6'
		when Value='annual' THEN '7' ELSE '' END AS [Frequency_OrderId]
		from Sfdc_Configurator..SFDC_ReportConfig
		where Config_Key='report_frequency') SRC ON SRF.frequency_id=src.ID
		JOIN Sfdc_Configurator..SFDC_SPList SP ON SR.ID=SP.Report_Id
		JOIN Sfdc_Configurator..SFDC_SPParameter SPM on SP.ID=SPM.SP_Id
		WHERE SR.active=1 and SP.active=1 and SPM.active=1 AND CONVERT(date,next_run)= CONVERT(date,GETDATE())
		AND SR.id not in (156,665)
		ORDER BY [Frequency_OrderId],sr.Id";
	}

	
	$dbh->{RaiseError} = 1;
	
	my $sth;
	eval {
		$sth= $dbh->prepare($query_LogicData);
		$sth->execute();
	};
	
	if ( $@ ) 
	{ 
		print "ERROR: $@";
		
		&evalerror($@,'0',$dbh);
		
		next;
	}

	my (@Report_Id,@Report_Name,@category,@SP_Id,@Sp_Name,@SPParam_Id,@parameter_count,@Sp_Parameter,@Multisheet_Flg,@outfile_Name,@Sheet_Name,@outfile_type,@with_header,@Output_split,@Split_Count,@Mail_attachement,@FTP_Upload,@Retryfrequencyminutes,@Previous_run_duration,@Timeout_Minutes,@status,@expected_count,@FrequencyOrderBy);
	while(my @record = $sth->fetchrow)
	{
		push(@Report_Id,Trim($record[0]));
		push(@Report_Name,Trim($record[1]));
		push(@category,Trim($record[2]));
		push(@SP_Id,Trim($record[3]));
		push(@Sp_Name,Trim($record[4]));
		push(@SPParam_Id,Trim($record[5]));
		push(@parameter_count,Trim($record[6]));
		push(@Sp_Parameter,Trim($record[7]));
		push(@Multisheet_Flg,Trim($record[8]));
		push(@outfile_Name,Trim($record[9]));
		push(@Sheet_Name,Trim($record[10]));
		push(@outfile_type,Trim($record[11]));
		push(@with_header,Trim($record[12]));
		push(@Output_split,Trim($record[13]));
		push(@Split_Count,Trim($record[14]));
		push(@Mail_attachement,Trim($record[15]));
		push(@FTP_Upload,Trim($record[16]));
		push(@Retryfrequencyminutes,Trim($record[17]));
		push(@Previous_run_duration,Trim($record[18]));
		push(@Timeout_Minutes,Trim($record[19]));
		push(@status,Trim($record[20]));
		push(@expected_count,Trim($record[21]));
		push(@FrequencyOrderBy,Trim($record[22]));
	}
	$sth->finish();
	return (\@Report_Id,\@Report_Name,\@category,\@SP_Id,\@Sp_Name,\@SPParam_Id,\@parameter_count,\@Sp_Parameter,\@Multisheet_Flg,\@outfile_Name,\@Sheet_Name,\@outfile_type,\@with_header,\@Output_split,\@Split_Count,\@Mail_attachement,\@FTP_Upload,\@Retryfrequencyminutes,\@Previous_run_duration,\@Timeout_Minutes,\@status,\@FrequencyOrderBy);
	
}	

###### DB Retrieve_Schedule_LogicData_DOTNET ######
sub Retrieve_Schedule_LogicData_DOTNET()
{
	my $dbh	= shift;
	my $report_id = shift;
		
	my $query_LogicData = "SELECT SR.ID as Report_Id,SR.Report_Name,SR.category,SP.ID as SP_Id,SP.Sp_Name,SPM.ID AS SPParam_Id,SPM.argument_count,SPM.Sp_Parameter,
	SPM.Multisheet_Flg,SPM.outfile_Name,SPM.Sheet_Name,SPM.outfile_type ,SPM.with_header,SPM.Output_split,SPM.Split_Count,SPM.Mail_attachement,SPM.FTP_Upload,spm.Retryfrequencyminutes,
	SPM.Previous_run_duration,SPM.Timeout_Minutes,SPM.expected_count
	FROM Sfdc_Configurator..SFDC_Reports SR
	JOIN Sfdc_Configurator..SFDC_SPList SP ON SR.ID=SP.Report_Id
	JOIN Sfdc_Configurator..SFDC_SPParameter SPM on SP.ID=SPM.SP_Id
	WHERE SR.active=1 and SP.active=1 and SPM.active=1 and Report_Id in ($report_id)";
	
	
	$dbh->{RaiseError} = 1;
	
	my $sth;
	eval {
		$sth= $dbh->prepare($query_LogicData);
		$sth->execute();
	};
	
	if ( $@ ) 
	{ 
		print "ERROR: $@";
		
		&evalerror($@,'0',$dbh);
		
		next;
	}

	my (@Report_Id,@Report_Name,@category,@SP_Id,@Sp_Name,@SPParam_Id,@parameter_count,@Sp_Parameter,@Multisheet_Flg,@outfile_Name,@Sheet_Name,@outfile_type,@with_header,@Output_split,@Split_Count,@Mail_attachement,@FTP_Upload,@Retryfrequencyminutes,@Previous_run_duration,@Timeout_Minutes,@status,@expected_count);
	while(my @record = $sth->fetchrow)
	{
		push(@Report_Id,Trim($record[0]));
		push(@Report_Name,Trim($record[1]));
		push(@category,Trim($record[2]));
		push(@SP_Id,Trim($record[3]));
		push(@Sp_Name,Trim($record[4]));
		push(@SPParam_Id,Trim($record[5]));
		push(@parameter_count,Trim($record[6]));
		push(@Sp_Parameter,Trim($record[7]));
		push(@Multisheet_Flg,Trim($record[8]));
		push(@outfile_Name,Trim($record[9]));
		push(@Sheet_Name,Trim($record[10]));
		push(@outfile_type,Trim($record[11]));
		push(@with_header,Trim($record[12]));
		push(@Output_split,Trim($record[13]));
		push(@Split_Count,Trim($record[14]));
		push(@Mail_attachement,Trim($record[15]));
		push(@FTP_Upload,Trim($record[16]));
		push(@Retryfrequencyminutes,Trim($record[17]));
		push(@Previous_run_duration,Trim($record[18]));
		push(@Timeout_Minutes,Trim($record[19]));
		push(@status,Trim($record[20]));
		push(@expected_count,Trim($record[21]));
	}
	$sth->finish();
	return (\@Report_Id,\@Report_Name,\@category,\@SP_Id,\@Sp_Name,\@SPParam_Id,\@parameter_count,\@Sp_Parameter,\@Multisheet_Flg,\@outfile_Name,\@Sheet_Name,\@outfile_type,\@with_header,\@Output_split,\@Split_Count,\@Mail_attachement,\@FTP_Upload,\@Retryfrequencyminutes,\@Previous_run_duration,\@Timeout_Minutes,\@status,\@expected_count);
	
}

###### Retrieve_TableRefreshTime_Count ######
sub Retrieve_TableRefreshTime_Count()
{
	my $dbh			= shift;
	my $refereshTab	= shift;
	
	my $query_refersh_table = "select count(*) from $refereshTab where lastRefreshTime BETWEEN DATEADD(MINUTE, 0, DATEADD(HOUR, 17, CONVERT(DATETIME, CONVERT(DATE,GETDATE()-1)))) AND GETDATE()";
	my $sth = $dbh->prepare($query_refersh_table);
	$sth->execute();
	
	my $ary_ref  = $sth->fetchrow_arrayref;
	return ($ary_ref);
}


###### Retrieve_SplitCount_FileName ######
sub Retrieve_SplitCount_FileName()
{
	my $dbh			= shift;
	my $SPParam_Id	= shift;
	my $SFDC_Spparameter_table	= shift;
	
	my $query_Retrieve_SplitCount = "select Split_Count,outfile_Name from $SFDC_Spparameter_table where ID in ($SPParam_Id)";
	print "SplitCount_Query :: $query_Retrieve_SplitCount\n";
	
	my $sth = $dbh->prepare($query_Retrieve_SplitCount);
	$sth->execute();
	
	my (@split,@names);
	while ( my @record = $sth->fetchrow)
	{
		my $split_cnt = $record[0];
		my $file_name = $record[1];
		
		print "split_cnt :: $split_cnt\n";
		print "file_name :: $file_name\n";
		
		push(@split,$split_cnt);
		push(@names,$file_name);
	}
	$sth->finish();
	return (\@split,\@names);
	
}

###### Retrieve_FileType ######
sub Retrieve_FileType()
{
	my $dbh			= shift;
	my $tempID  	= shift;
	my $Outfile_key = shift;
	my $SFDC_ReportConfig_Table = shift;
	
	my $query_Retrieve_FileType = "select value from $SFDC_ReportConfig_Table where Config_Key =\'$Outfile_key\' and id in ($tempID)";
	print "FileType_Query :: $query_Retrieve_FileType\n";
	my $sth = $dbh->prepare($query_Retrieve_FileType);
	$sth->execute();
	
	my (@File_Type_Value);
	while(my @record = $sth->fetchrow)
	{
		push(@File_Type_Value,Trim($record[0]));
	}
	$sth->finish();
	return (\@File_Type_Value);
}

###### Retrieve data for executed Procedure ######
sub DATAGET_MultipleResultSet()
{
	my $dbh			= shift;
	my $Sp_Name		= shift;
	my $Sp_Parameter= shift;
	my $SheetCount	= shift;
	my $SPParam_Id	= shift;
	my $Status_Log_Table	= shift;
	
	$SheetCount=$SheetCount+1;
	print "SheetCount DB	:: $SheetCount\n";
	print "Sp_Parameter		:: $Sp_Parameter\n";
	my $query;
	if ($Sp_Parameter ne 'NULL')
	{
		$query = "exec [$Sp_Name] $Sp_Parameter";
	}
	else
	{
		$query = "exec [$Sp_Name]";
	}
	print "DATAGET_MultipleResultSet :: $query\n";
	
	$dbh->{RaiseError} = 1;
	
	my $sth;
	eval {
		$sth= $dbh->prepare($query);
		$sth->execute();
	};
	
	if ( $@ ) 
	{ 
		print "ERROR: $@";
		
		my $insertQuery_StatusLog_Terminated = "insert into $Status_Log_Table (Sp_paramid,start_time,process_status) VALUES (\'$SPParam_Id\',GETDATE(),\'Terminated\')";
		
		print "insertQuery_StatusLog_Terminated :: $insertQuery_StatusLog_Terminated\n"; 
		
		my $sth = $dbh->prepare($insertQuery_StatusLog_Terminated);
		if($sth->execute())
		{
			print "insertQuery_StatusLog_Terminated :: Executed\n";
		}
		else
		{
			# print "QUERY:: $insertQuery\n";
			
			my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
			
			# print "logLocation==>$logLocation"; <STDIN>;
			
			# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
			unless ( -d $logLocation )
			{
				make_path($logLocation);
			}
		
		
			open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
			print PP $insertQuery_StatusLog_Terminated."\n";
			close PP;
			
			$dbh=&DbConnection();
			# &send_mail_InsertFailed($insertQuery,$SPParam_Id);
		}
		
		&evalerror($@,$SPParam_Id,$dbh);
		
		next;
	}
	
	my (@Records,@Headers,$row,$overallCount);
	do 
	{
		my $head = $sth->{NAME};
		print "Fields:     $sth->{NUM_OF_FIELDS}\n";
		print "Header:     $head\n\n";
		
		push (@Headers, $head);
		my (@Records_temp);
		my $count = 0;
		while(my @record = $sth->fetchrow) 
		{
			push (@Records_temp, \@record);
			$count++;
		}
		print "count:     $count\n\n";
		push (@Records, \@Records_temp);
		$overallCount = $count;
		
    } while ($sth->{odbc_more_results});
	
	return (\@Records,\@Headers,$overallCount);
	
}


###### Retrieve data for executed Procedure ######
sub NewDATAGET_MultipleResultSet()
{
	my $dbh			= shift;
	my $Sp_Name		= shift;
	my $Sp_Parameter= shift;
	my $SheetCount	= shift;
	my $SPParam_Id	= shift;
	my $Status_Log_Table	= shift;
	
	$SheetCount=$SheetCount+1;
	print "SheetCount DB	:: $SheetCount\n";
	print "Sp_Parameter		:: $Sp_Parameter\n";
	
	my $query;
	if ($Sp_Parameter ne 'NULL')
	{
		$query = "exec [$Sp_Name] $Sp_Parameter";
	}
	else
	{
		$query = "exec [$Sp_Name]";
	}
	print "DATAGET_MultipleResultSet :: $query\n";
	
	$dbh->{RaiseError} = 1;
	
	my $sth;
	eval {
		$sth= $dbh->prepare($query);
		$sth->execute();
	};
	
	if ( $@ ) 
	{ 
		print "ERROR: $@";
		
		my $insertQuery_StatusLog_Terminated = "insert into $Status_Log_Table (Sp_paramid,start_time,process_status) VALUES (\'$SPParam_Id\',GETDATE(),\'Terminated\')";
		
		print "insertQuery_StatusLog_Terminated :: $insertQuery_StatusLog_Terminated\n"; 
		
		my $sth = $dbh->prepare($insertQuery_StatusLog_Terminated);
		if($sth->execute())
		{
			print "insertQuery_StatusLog_Terminated :: Executed\n";
		}
		else
		{
			# print "QUERY:: $insertQuery\n";
			
			my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
			
			# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
			unless ( -d $logLocation )
			{
				make_path($logLocation);
			}
			
			open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
			print PP $insertQuery_StatusLog_Terminated."\n";
			close PP;
			$dbh=&DbConnection();
			# &send_mail_InsertFailed($insertQuery,$SPParam_Id);
		}
		
		&evalerror($@,$SPParam_Id,$dbh);
		
		next;
	}
	
	my (@FRecords,@SRecords,@Headers,$row,$Count,@firstRecords,@secRecords);
	do 
	{
		my $head = $sth->{NAME};
		print "Fields:     $sth->{NUM_OF_FIELDS}\n";
		print "Header:     $head\n\n";
		
		push (@Headers, $head);
		my (@Records_temp);
		my $count = 0;
		while(my @record = $sth->fetchrow) 
		{
			push (@Records_temp, \@record);
			$count++;
		}
		
		my $csvcnt = $count/2;
		my $round = round($csvcnt);	
			
		@firstRecords = @Records_temp[0..$round]; 
		push (@FRecords, \@firstRecords);
		my $roundplus = $round+1;
		@secRecords = @Records_temp[$roundplus..$count]; 
						
		print "count:     $count\n\n";
		push (@SRecords, \@secRecords);
		$Count = $count;
		
    } while ($sth->{odbc_more_results});
	
	return (\@FRecords,\@SRecords,\@Headers,$Count);
	
}

###### Retrieve data for executed Procedure ######
sub DATAGET()
{
	my $dbh			= shift;
	my $Sp_Name		= shift;
	my $SheetCount	= shift;
	
	$SheetCount=$SheetCount+1;
	print "SheetCount DB :: $SheetCount\n";
	
	my $query = "exec [$Sp_Name]";
	my $sth= $dbh->prepare($query);
	$sth->execute();
	print "STH :: $sth\n";
	print Dumper($sth);
	# my $titles = $sth->{NAME};
	# print "TitlesDB :: @{$titles}\n";
	

	my (@Records,@Headers);
	my $i=1;
	while ( my $record = $sth->fetchall_arrayref())
	{
		my $titles = $sth->{NAME};
		print "TitlesDB :: @{$titles}\n";
		print "Inside the Whilee :: $record\n";
		my @temparray = @{$record};
		my $count = @temparray;
		print "Count of an Array :: $count\n";
		last if ($i==$SheetCount+1);
		push (@Records,$record);
		push (@Headers,$titles);
		# return ($record);
		$i++;
	}
	# exit;
	return (\@Records,\@Headers);
}

###### Insert_into_StatusLog_Running ######
sub Insert_into_StatusLog_Running()
{
	my $dbh					= shift;
	my $sp_parameter_ID 	= shift;
	my $Status_Log_Table	= shift;
	my $StatusLog_Running	= shift;
	 
	
	my $insertQuery_StatusLog_Running = "insert into $Status_Log_Table (Sp_paramid,start_time,process_status) VALUES (\'$sp_parameter_ID\',GETDATE(),\'$StatusLog_Running\')";
	print "insertQuery_StatusLog_Running :: $insertQuery_StatusLog_Running\n";
	my $sth = $dbh->prepare($insertQuery_StatusLog_Running);
	if($sth->execute())
	{
		print "Insert_into_StatusLog_Running :: Executed\n";
	}
	else
	{
		# print "QUERY:: $insertQuery\n";
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
			
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $insertQuery_StatusLog_Running."\n";
		close PP;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}


###### update_into_SFDC_Reports ######
sub update_into_SFDC_Reports()
{
	my $dbh					= shift;
	my $Report_Name		 	= shift;
	my $SFDC_Reports		= shift;
	my $previoustime		= shift;
	
	my $updateQuery_SFDCReport_Running = "UPDATE $SFDC_Reports set previous_run = \'$previoustime\' where Report_Name =\'$Report_Name\'";
	print "updateQuery_SFDCReport_Running :: $updateQuery_SFDCReport_Running\n";
	
	my $sth = $dbh->prepare($updateQuery_SFDCReport_Running);
	if($sth->execute())
	{
		print "updateQuery_SFDCReport_Running :: Executed\n";
	}
	else
	{
		# print "QUERY:: $insertQuery\n";
		
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $updateQuery_SFDCReport_Running."\n";
		close PP;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}


###### Update_into_SFDC_Spparameter ######
sub Update_into_SFDC_Spparameter()
{
	my $dbh					= shift;
	my $sp_parameter_ID 	= shift;
	my $Spparameter_Table	= shift;
	my $previous_run		= shift;
	
	my $UpdateQuery_Spparameter = "UPDATE $Spparameter_Table set Previous_run_duration = \'$previous_run\' where ID =\'$sp_parameter_ID\'";
	
	print "UpdateQuery_Spparameter :: $UpdateQuery_Spparameter\n"; 
	my $sth = $dbh->prepare($UpdateQuery_Spparameter);
	if($sth->execute())
	{
		print "UpdateQuery_Spparameter :: Executed\n";
	}
	else
	{
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $UpdateQuery_Spparameter."\n";
		close PP;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}

###### Insert_into_SFDC_ReportHistory ######
sub Insert_into_SFDC_ReportHistory
{
	my $DatabaseHandler		= shift;
	my $sp_parameter_ID 	= shift;
	my $SFDCReportHistoryTableName 	= shift;
	my $starttime			= shift;
	my $CurrentTime			= shift;
	my $OverAllCount		= shift;
	my $Size				= shift;
	
	# my $CurrentTime = DateTime->now(time_zone => 'Europe/London');
	# $CurrentTime=~s/T/ /gsi;
			
	my $QuerySelectReportHistory = "select $sp_parameter_ID from $SFDCReportHistoryTableName where sp_paramid=\'$sp_parameter_ID\' and cast(Rundate as date)=\'$CurrentTime\'";
		
	my $StatementSelectReportHistory = $DatabaseHandler->prepare($QuerySelectReportHistory);
	
	if($StatementSelectReportHistory->execute())
	{
		print "SelectQuery_ReportHistory :: Executed\n";
		
	}
	else
	{
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $StatementSelectReportHistory."\n";
		close PP;
		$DatabaseHandler=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
	
	my $CheckRowAvailability=$StatementSelectReportHistory->fetchrow();
	$StatementSelectReportHistory->finish;
	my $QueryInsertReportHistory;
	if($CheckRowAvailability eq '')
	{
		# print "Why";<>;
		$QueryInsertReportHistory = "insert into $SFDCReportHistoryTableName (SP_paramid,Start_time,End_time,Report_count,Filesize,Updated_On,RunDate) VALUES (\'$sp_parameter_ID\',\'$starttime\',\'$CurrentTime\',\'$OverAllCount\',\'$Size\',\'$CurrentTime\',\'$CurrentTime\')";
	}
	else
	{
		$QueryInsertReportHistory = "UPDATE $SFDCReportHistoryTableName set manualqa_status=\'NULL\', report_count=\'$OverAllCount\', RunDate=\'$CurrentTime\' Where sp_paramid=\'$sp_parameter_ID\' and cast(Rundate as date)=\'$CurrentTime\'";

	}
	
	
	print "InsertQuery_ReportHistory :: $QueryInsertReportHistory\n"; 
	
	my $sth = $DatabaseHandler->prepare($QueryInsertReportHistory);
	
	if($sth->execute())
	{
		print "QueryInsertReportHistory :: Executed\n";
	}
	else
	{
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $QueryInsertReportHistory."\n";
		close PP;
		$DatabaseHandler=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}

###### Insert_into_SFDC_ReportRunHistory ######
sub Insert_into_SFDC_ReportRunHistory
{
	my $DatabaseHandler		= shift;
	my $SPParameterID 		= shift;
	my $SFDC_ReportRunHistory 	= shift;
	my $StartTime			= shift;
	my $CurrentTime			= shift;
	my $OverAllCount		= shift;
	# my $Size				= shift;
	
		
	# my $CurrentTime = DateTime->now();
	# $CurrentTime=~s/T/ /gsi;
		
	my $QueryInsertReportRunHistory = "insert into $SFDC_ReportRunHistory (param_id,Start_time,End_time,Report_count,Updated_On) VALUES (\'$SPParameterID\',\'$StartTime\',\'$CurrentTime\',\'$OverAllCount\',\'$CurrentTime\')";
	
	print "InsertQuery_ReportRunHistory :: $QueryInsertReportRunHistory\n"; 
	
	my $StatementInsertReportRunHistory = $DatabaseHandler->prepare($QueryInsertReportRunHistory);
	
	if($StatementInsertReportRunHistory->execute())
	{
		print "InsertQuery_ReportRunHistory :: Executed\n";
	}
	else
	{
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $QueryInsertReportRunHistory."\n";
		close PP;
		$DatabaseHandler=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$SPParameterID);
	}
}


###### Insert_into_StatusLog_AutoQA ######
sub Insert_into_StatusLog_AutoQA()
{
	my $dbh					= shift;
	my $sp_parameter_ID 	= shift;
	my $Status_Log_Table	= shift;
	my $StatusLog_autoQA	= shift;
	
	my $insertQuery = "insert into $Status_Log_Table (Sp_paramid,start_time,process_status) VALUES (\'$sp_parameter_ID\',GETDATE(),\'$StatusLog_autoQA\')";

	my $sth = $dbh->prepare($insertQuery);
	if($sth->execute())
	{
		print "Insert_into_StatusLog_AutoQA :: Executed\n";
	}
	else
	{
		# print "QUERY:: $insertQuery\n";
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		
		# my $logLocation = "\\\\CH1021SF01\\Glenigan_SFDC\\SFDC_Exports\\FailedStatus\\".$todaysDate;
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $insertQuery."\n";
		close PP;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}

sub HistoryID_RetrieveUrl()
{
	my $dbh 		= shift;
	my $SPPara_ID 	= shift;
	my $Input_Table = shift;
	
	my $query = "select max(id) from $Input_Table  where SP_paramid=\'$SPPara_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Batch_ID);
	while(my @record = $sth->fetchrow)
	{
		$Batch_ID=Trim($record[0]);
	}
	$sth->finish();
	return ($Batch_ID);
}

#Mailing Process
sub send_mail()
{
	my $fileName = shift;
	my $filePath = shift;	
	
	my $hash_ref1 		= &ReadIniFile('MAILDETAILS');
	my %inihashvalue1	= %{$hash_ref1};
	
	my $subject	= "Report - SFDC";
	my $host   	= $inihashvalue1{'host'}; 
	my $from 	= $inihashvalue1{'from'};
	my $user 	= $inihashvalue1{'user'};
	my $to		= $inihashvalue1{'to'};
	my $pass	= $inihashvalue1{'password'};
	my $body	= "Hi Team, <br><br>\t\tPlease find the attached Report<br><br>"."Regards<br>SFDC IT Support";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
        Type     =>'text/csv',
        Path     =>"$filePath",
        Filename =>"$fileName"
    );
	
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	$msg->send;
}

###### Hidden values ######
sub ReadIniFile()
{
	my $INIVALUE = shift;
	
	
	my $cfg = new Config::IniFiles( -file => "$iniDirectory/SFDC_Automation.ini", -nocase => 1);
	
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}

###### Test ######
sub Retrieve_testConfig()
{
	my $dbh			= shift;
	
	my $query_Retrieve_testConfig = "select id,config_key,value from Sfdc_Configurator..SFDC_ReportConfig";
	print "FileType_Query :: $query_Retrieve_testConfig\n";
	my $sth = $dbh->prepare($query_Retrieve_testConfig);
	$sth->execute();
	
	my $titles = $sth->{NAME};
	my @titles = @{$titles};
	print "Title :: $titles\n";
	foreach my $key(@titles)
	{
		print "Title :: $key\n";
	}
	
	my (@id,@config_key,@value);
	while(my @record = $sth->fetchrow)
	{
		print "$record[0]\t$record[1]\t$record[2]\n";
	}
}

### SP_failures insert ###

sub evalerror()
{
	my $error = shift;
	my $SPParam_Id = shift;
	my $dbh = shift;
	
	my $failure_reason;
		
	if($error=~m/\[ODBC\s*SQL\s*Server\s*Driver\]\s*\[SQL\s*Server\]\s*([^<]*?)at\s*C\:\//is)
	{
		$failure_reason = $1;
		$failure_reason=~s/\'/\'\'/gsi;
	}
	elsif($error=~m/\[ODBC\s*SQL\s*Server\s*Driver\]\s*\[DBNETLIB\]\s*(ConnectionRead)/is)
	{
		$failure_reason = $1;
		$failure_reason=~s/\'/\'\'/gsi;		
	}
	elsif($error=~m/\[ODBC\s*SQL\s*Server\s*Driver\]\s*(Communication\s*link\s*failure)/is)
	{
		$failure_reason = $1;
		$failure_reason=~s/\'/\'\'/gsi;		
	}
	
	my $insertQuery_SFDC_spfailures = "insert into Sfdc_Configurator..SFDC_spfailures (Sp_paramid,failure_reason,created_on) VALUES (\'$SPParam_Id\',\'$failure_reason\',GETDATE())";
	
	print "insertQuery_SFDC_spfailures :: $insertQuery_SFDC_spfailures\n"; 
	
	my $sth = $dbh->prepare($insertQuery_SFDC_spfailures);
	if($sth->execute())
	{
		print "insertQuery_SFDC_spfailures :: Executed\n";
	}
	else
	{
		# print "QUERY:: $insertQuery\n";
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
		
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print PP $insertQuery_SFDC_spfailures."\n";
		close PP;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$SPParam_Id);
	}
	
	return $error;
}



###### Trim the given text ######
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
-1