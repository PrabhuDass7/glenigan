package SFDC_AUTOMATION_DB_Daily;
### Package Name ####
use strict;
use Text::CSV;
use DBI;
# use DBIx::Interpolate qw(:all);
use DBD::ODBC;
use MIME::Lite;
use MIME::Base64;
use Config::IniFiles;
use POSIX 'strftime';
use Cwd qw(abs_path);
use File::Basename;
use DateTime;
use File::Path qw(make_path remove_tree);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0);
$basePath=~s/\/root$//is;


####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd('');

# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $logsDirectory = ($basePath.'/logs');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');

###### DB Connection ######
sub DbConnection()
{
	my $DBDETAILS 		= &ReadIniFile('SFDC_DB_SQL');
	my %Credentials		= %{$DBDETAILS};
	my $servername = $Credentials{'server_name'};
	print "ServerName :: $servername\n";

	my $dsn ='driver={SQL Server};Server='.$servername.';database='.$Credentials{'database_name'}.';uid='.$Credentials{'database_user'}.';pwd='.$Credentials{'database_pass'}.';';
	my $dbh  = DBI->connect("DBI:ODBC:$dsn",{AutoCommit => 0, RaiseError => 1, PrintError => 0}) or die "$DBI::errstr\n";
	$dbh-> {'LongTruncOk'} = 1;
	$dbh-> {'LongReadLen'} = 900000;
	return $dbh;
}


###### DB Retrieve_TableRefreshTime  ######
sub Retrieve_SMTableRefreshTime_Count()
{
	my $dbh			= shift;
	my $refereshTab	= shift;
		
	my $query_refersh_table = "select count(*) from SFDC_DBAMP..$refereshTab where convert(date,lastRefreshTime)= convert(date,getdate()) and TblName in ('GR_PROJECT__C','GR_PROJECTCATEGORY__C','GR_ProjectEvent__c')";
	
	my $sth = $dbh->prepare($query_refersh_table);
	$sth->execute();
	
	my $ary_ref  = $sth->fetchrow_arrayref;
	return ($ary_ref);
}

###### DB Retrieve_Schedule_Logic ######
sub Retrieve_Schedule_LogicData()
{
	my $dbh	= shift;
	
	
	my $fulldate_DayCheck 				= strftime "%Y-%m-%d %A %H:%M:%S", localtime;
	print "Today :: $fulldate_DayCheck\n";
	my $query_LogicData;
	if ($fulldate_DayCheck=~m/Friday/is)
	{
		# open (DE,">>DayLog.txt");
		# print RE "$fulldate_DayCheck\n";
		# close RE;
		$query_LogicData = "	SELECT SR.ID as Report_Id,SR.Report_Name,SR.category,SP.ID as SP_Id,SP.Sp_Name,SPM.ID AS SPParam_Id,SPM.argument_count,SPM.Sp_Parameter,
		SPM.Multisheet_Flg,SPM.outfile_Name,SPM.Sheet_Name,SPM.outfile_type ,SPM.with_header,SPM.Output_split,SPM.Split_Count,SPM.Mail_attachement,SPM.FTP_Upload,spm.Retryfrequencyminutes,
		SPM.Previous_run_duration,SPM.Timeout_Minutes,SPM.expected_count
		FROM Sfdc_Configurator..SFDC_Reports SR
		JOIN Sfdc_Configurator..SFDC_SPList SP ON SR.ID=SP.Report_Id
		JOIN Sfdc_Configurator..SFDC_SPParameter SPM on SP.ID=SPM.SP_Id
		--JOIN SFDC_ReportConfig SRC ON SPM.outfile_type LIKE CONCAT('%', SRC.ID, '%')
		WHERE SR.active=1 and SP.active=1 and SPM.active=1 and Report_Id in ('156','665')";
	}	
	else{
		 $query_LogicData = "	SELECT SR.ID as Report_Id,SR.Report_Name,SR.category,SP.ID as SP_Id,SP.Sp_Name,SPM.ID AS SPParam_Id,SPM.argument_count,SPM.Sp_Parameter,
		SPM.Multisheet_Flg,SPM.outfile_Name,SPM.Sheet_Name,SPM.outfile_type ,SPM.with_header,SPM.Output_split,SPM.Split_Count,SPM.Mail_attachement,SPM.FTP_Upload,spm.Retryfrequencyminutes,
		SPM.Previous_run_duration,SPM.Timeout_Minutes,SPM.expected_count
		FROM Sfdc_Configurator..SFDC_Reports SR
		JOIN Sfdc_Configurator..SFDC_SPList SP ON SR.ID=SP.Report_Id
		JOIN Sfdc_Configurator..SFDC_SPParameter SPM on SP.ID=SPM.SP_Id
		--JOIN SFDC_ReportConfig SRC ON SPM.outfile_type LIKE CONCAT('%', SRC.ID, '%')
		WHERE SR.active=1 and SP.active=1 and SPM.active=1 and Report_Id='156'";
		}
	my $sth = $dbh->prepare($query_LogicData);
	$sth->execute();
	
	my (@Report_Id,@Report_Name,@category,@SP_Id,@Sp_Name,@SPParam_Id,@parameter_count,@Sp_Parameter,@Multisheet_Flg,@outfile_Name,@Sheet_Name,@outfile_type,@with_header,@Output_split,@Split_Count,@Mail_attachement,@FTP_Upload,@Retryfrequencyminutes,@Previous_run_duration,@Timeout_Minutes,@status,@expected_count);
	while(my @record = $sth->fetchrow)
	{
		push(@Report_Id,Trim($record[0]));
		push(@Report_Name,Trim($record[1]));
		push(@category,Trim($record[2]));
		push(@SP_Id,Trim($record[3]));
		push(@Sp_Name,Trim($record[4]));
		push(@SPParam_Id,Trim($record[5]));
		push(@parameter_count,Trim($record[6]));
		push(@Sp_Parameter,Trim($record[7]));
		push(@Multisheet_Flg,Trim($record[8]));
		push(@outfile_Name,Trim($record[9]));
		push(@Sheet_Name,Trim($record[10]));
		push(@outfile_type,Trim($record[11]));
		push(@with_header,Trim($record[12]));
		push(@Output_split,Trim($record[13]));
		push(@Split_Count,Trim($record[14]));
		push(@Mail_attachement,Trim($record[15]));
		push(@FTP_Upload,Trim($record[16]));
		push(@Retryfrequencyminutes,Trim($record[17]));
		push(@Previous_run_duration,Trim($record[18]));
		push(@Timeout_Minutes,Trim($record[19]));
		push(@status,Trim($record[20]));
		push(@expected_count,Trim($record[21]));
	}
	$sth->finish();
	return (\@Report_Id,\@Report_Name,\@category,\@SP_Id,\@Sp_Name,\@SPParam_Id,\@parameter_count,\@Sp_Parameter,\@Multisheet_Flg,\@outfile_Name,\@Sheet_Name,\@outfile_type,\@with_header,\@Output_split,\@Split_Count,\@Mail_attachement,\@FTP_Upload,\@Retryfrequencyminutes,\@Previous_run_duration,\@Timeout_Minutes,\@status,\@expected_count);
	
}	
sub Retrieve_TableRefreshTime_Count()
{
	my $dbh			= shift;
	my $refereshTab	= shift;
	
	my $query_refersh_table = "select count(*) from TableRefreshTime where convert(date,lastRefreshTime)=convert(date,getdate()-1) and TblName not in('GR_ObjectTransaction__c','GR_UserSettings__c','GR_InitialAztecProposal__c','GR_InitialAztecSite__c')";
	my $sth = $dbh->prepare($query_refersh_table);
	$sth->execute();
	
	my $ary_ref  = $sth->fetchrow_arrayref;
	return ($ary_ref);
}

sub Retrieve_testConfig()
{
	my $dbh			= shift;
	
	my $query_Retrieve_testConfig = "select id,config_key,value from Sfdc_Configurator..SFDC_ReportConfig";
	print "FileType_Query :: $query_Retrieve_testConfig\n";
	my $sth = $dbh->prepare($query_Retrieve_testConfig);
	$sth->execute();
	
	my $titles = $sth->{NAME};
	my @titles = @{$titles};
	print "Title :: $titles\n";
	foreach my $key(@titles)
	{
		print "Title :: $key\n";
	}
	
	my (@id,@config_key,@value);
	while(my @record = $sth->fetchrow)
	{
		print "$record[0]\t$record[1]\t$record[2]\n";
	}
}

###### Retrieve_FileType ######
sub Retrieve_FileType()
{
	my $dbh			= shift;
	my $tempID  	= shift;
	my $Outfile_key = shift;
	my $SFDC_ReportConfig_Table = shift;
	
	my $query_Retrieve_FileType = "select value from $SFDC_ReportConfig_Table where Config_Key =\'$Outfile_key\' and id in ($tempID)";
	print "FileType_Query :: $query_Retrieve_FileType\n";
	my $sth = $dbh->prepare($query_Retrieve_FileType);
	$sth->execute();
	
	my (@File_Type_Value);
	while(my @record = $sth->fetchrow)
	{
		push(@File_Type_Value,Trim($record[0]));
	}
	$sth->finish();
	return (\@File_Type_Value);
}

###### Retrieve data for executed Procedure ######
sub DATAGET()
{
	my $dbh			= shift;
	my $Sp_Name		= shift;
	my $SheetCount	= shift;
	
	$SheetCount=$SheetCount+1;
	print "SheetCount DB :: $SheetCount\n";
	
	my $query = "exec [$Sp_Name]";
	my $sth= $dbh->prepare($query);
	$sth->execute();
	
	
	my (@Records,$titles);
	my $i=1;
	while ( my $record = $sth->fetchall_arrayref())
	{
		$titles = $sth->{NAME};
		print "TitlesDB :: $titles\n";
		print "Inside the Whilee :: $record\n";
		my @temparray = @{$record};
		my $count = @temparray;
		print "Count of an Array :: $count\n";
		last if ($i==$SheetCount);
		push (@Records,$record);
		# return ($record);
		$i++;
	}
	return (\@Records,$titles);
}

###### Insert_into_StatusLog_Running ######
sub Insert_into_StatusLog_Running()
{
	my $dbh					= shift;
	my $sp_parameter_ID 	= shift;
	my $Status_Log_Table	= shift;
	my $StatusLog_Running	= shift;
	 
	
	my $insertQuery_StatusLog_Running = "insert into $Status_Log_Table (Sp_paramid,start_time,process_status) VALUES (\'$sp_parameter_ID\',GETDATE(),\'$StatusLog_Running\')";
	print "insertQuery_StatusLog_Running :: $insertQuery_StatusLog_Running\n";
	my $sth = $dbh->prepare($insertQuery_StatusLog_Running);
	if($sth->execute())
	{
		print "Insert_into_StatusLog_Running :: Executed\n";
	}
	else
	{
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
			
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print ERR $insertQuery_StatusLog_Running."\n";
		close ERR;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}

###### Insert_into_StatusLog_AutoQA ######
sub Insert_into_StatusLog_AutoQA()
{
	my $dbh					= shift;
	my $sp_parameter_ID 	= shift;
	my $Status_Log_Table	= shift;
	my $StatusLog_autoQA	= shift;
	
	my $insertQuery = "insert into $Status_Log_Table (Sp_paramid,start_time,process_status) VALUES (\'$sp_parameter_ID\',GETDATE(),\'$StatusLog_autoQA\')";

	my $sth = $dbh->prepare($insertQuery);
	if($sth->execute())
	{
		print "Insert_into_StatusLog_AutoQA :: Executed\n";
	}
	else
	{
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
			
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		
		open(PP,">>$logLocation/Failed_Query_InsertAgain_$todaysDate.txt");
		print ERR $insertQuery."\n";
		close ERR;
		$dbh=&DbConnection();
		# &send_mail_InsertFailed($insertQuery,$sp_parameter_ID);
	}
}

sub HistoryID_RetrieveUrl()
{
	my $dbh 		= shift;
	my $SPPara_ID 	= shift;
	my $Input_Table = shift;
	
	my $query = "select max(id) from $Input_Table  where SP_paramid=\'$SPPara_ID\'";
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my ($Batch_ID);
	while(my @record = $sth->fetchrow)
	{
		$Batch_ID=Trim($record[0]);
	}
	$sth->finish();
	return ($Batch_ID);
}

#Mailing Process
sub send_mail()
{
	my $fileName = shift;
	my $filePath = shift;	
	
	my $hash_ref1 		= &ReadIniFile('MAILDETAILS');
	my %inihashvalue1	= %{$hash_ref1};
	
	my $subject	= "Report - SFDC";
	my $host   	= $inihashvalue1{'host'}; 
	my $from 	= $inihashvalue1{'from'};
	my $user 	= $inihashvalue1{'user'};
	my $to		= $inihashvalue1{'to'};
	my $pass	= $inihashvalue1{'password'};
	my $body	= "Hi Team, <br><br>\t\tPlease find the attached Report<br><br>"."Regards<br>Suresh K";
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  # Cc => $cc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
        Type     =>'text/csv',
        Path     =>"$filePath",
        Filename =>"$fileName"
    );
	
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
    # MIME::Lite->send('smtp', '74.80.234.196', Timeout=>60,Auth=>'LOGIN',AuthUser=>'meritgroup',AuthPass=>'11meritgroup11',Port => 25, Debug => 1);
	$msg->send;
}

#Hidden values
sub ReadIniFile()
{
	my $INIVALUE = shift;
	
	my $cfg = new Config::IniFiles( -file => "$iniDirectory/CommonCredentials.ini", -nocase => 1);
	
	my %hash_ini1;
	my @FileExten = $cfg -> Parameters("$INIVALUE");
	foreach my $FileExten (@FileExten)
	{
		my $FileExt =  $cfg -> val("$INIVALUE", $FileExten);
		$hash_ini1{$FileExten}=$FileExt;
	}
	return(\%hash_ini1);
}

###### Trim the given text ######
sub Trim()
{
	my $txt = shift;
	
	$txt =~ s/^\s+|\s+$//g;
	$txt =~ s/^\n+/\n/g;
	
	return $txt;
}
-1