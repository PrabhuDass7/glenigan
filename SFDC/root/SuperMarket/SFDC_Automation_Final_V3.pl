##### Module Initialization #####
use strict;
use List::Util qw( max );
use POSIX 'strftime';
use Spreadsheet::WriteExcel;
use Spreadsheet::ParseExcel::SaveParser;
use Excel::Writer::XLSX;
use Archive::Zip qw( :ERROR_CODES );
use File::Spec;
use Cwd qw(abs_path);
use File::Basename;
use DateTime;
use File::Path qw(make_path remove_tree);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root$//is;
# print "basePath==>$basePath\n";<STDIN>;

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $logsDirectory = ($basePath.'/logs');

# my $FilePath = $dataDirectory;

# print "libDirectory==>$libDirectory\n"; <STDIN>;

# Private Module
require ($libDirectory.'/SFDC_AUTOMATION_DB_Daily.pm'); 


####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd('');

my $fulldate_DayCheck 	= strftime "%Y-%m-%d %A %H:%M:%S", localtime;
print "Today :: $fulldate_DayCheck\n";



if ($fulldate_DayCheck=~m/Saturday|Sunday/is)
{
	open (DE,">>$logsDirectory/DayLog_$todaysDate.txt");
	print RE "$fulldate_DayCheck\n";
	close RE;
	exit;
}

##### Read Inputs from INI File #####
my $SFDC_INPUTS1 			= &SFDC_AUTOMATION_DB_Daily::ReadIniFile('SFDCINPUTS');
my %SFDC_INPUTS				= %{$SFDC_INPUTS1};


##### Database Initialization #####
my $dbh = &SFDC_AUTOMATION_DB_Daily::DbConnection();
print "DBStatus	:: DB Connected\n";

##### Retrieve TableRefreshTime Count #####
my $TableRefreshTime 	= $SFDC_INPUTS{'tablerefresh'};
my $Referesh_count = &SFDC_AUTOMATION_DB_Daily::Retrieve_SMTableRefreshTime_Count($dbh,$TableRefreshTime);
my @Referesh_count = @{$Referesh_count};
print "Referesh_count :: $Referesh_count[0]\n"; 

if($Referesh_count[0] eq "3")
{
	print "******* Replication Successfull *******\n";
}
else
{
	print "******* Replication Failed *******\n";
	my $fulldate_temp = strftime "%Y-%m-%d %H:%M:%S", localtime;
	
	
	my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
	
	print "logLocation==>$logLocation\n";
				
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	
	
	open (RE,">>$logLocation/SM_ReplicationFailedLog_$todaysDate.txt");
	print RE "Replication Failed\t$Referesh_count[0]\t$fulldate_temp\n";
	exit;
}


##### Retrieve SP_NAME along with detail for schedule logic #####
my ($Report_Id,$Report_Name,$category,$SP_Id,$Sp_Name,$SPParam_Id,$parameter_count,$Sp_Parameter,$Multisheet_Flg,$outfile_Name,$Sheet_Name,$outfile_type,$with_header,$Output_split,$Split_Count,$Mail_attachement,$FTP_Upload,$Retryfrequencyminutes,$Previous_run_duration,$Timeout_Minutes,$status,$expected_count) = &SFDC_AUTOMATION_DB_Daily::Retrieve_Schedule_LogicData($dbh);

my @Report_Id				= @{$Report_Id};
my @Report_Name				= @{$Report_Name};
my @category				= @{$category};
my @SP_Id					= @{$SP_Id};
my @Sp_Name					= @{$Sp_Name};
my @SPParam_Id				= @{$SPParam_Id};
my @parameter_count			= @{$parameter_count};
my @Sp_Parameter			= @{$Sp_Parameter};
my @Multisheet_Flg			= @{$Multisheet_Flg};
my @outfile_Name			= @{$outfile_Name};
my @Sheet_Name				= @{$Sheet_Name};
my @outfile_type			= @{$outfile_type};
my @with_header				= @{$with_header};
my @Output_split			= @{$Output_split};
my @Split_Count				= @{$Split_Count};
my @Mail_attachement		= @{$Mail_attachement};
my @FTP_Upload				= @{$FTP_Upload};
my @Retryfrequencyminutes	= @{$Retryfrequencyminutes};
my @Previous_run_duration	= @{$Previous_run_duration};
my @Timeout_Minutes			= @{$Timeout_Minutes};
my @status					= @{$status};
my @expected_count			= @{$expected_count};

##### looping for report execution one by one #####
my @A_Category = grep { $category[$_]=~/A/ } 0..$#category;
my @B_Category = grep { $category[$_]=~/B/ } 0..$#category;
my @C_Category = grep { $category[$_]=~/C/ } 0..$#category;
my @D_Category = grep { $category[$_]=~/D/ } 0..$#category;


my $T_Count = @Report_Id;
my $A_Count = @A_Category;
my $B_Count = @B_Category;
my $C_Count = @C_Category;
my $D_Count = @D_Category;

# my $max = +(sort { $b <=> $a } $a, $b, $c)[0];
# my $higherCount = max ($#A_Category, $#B_Category, $#C_Category,$#D_Category);
my $higherCount = max ($A_Count, $B_Count, $C_Count,$D_Count);
my $LargerCount_AD = max ($A_Count, $D_Count);
my $LargerCount_BC = max ($B_Count, $C_Count);
my @scheduleArray;
foreach my $key1 (0..$LargerCount_AD)
{
	my $A_Value = $A_Category[$key1];
	my $D_Value = $D_Category[$key1];
	push (@scheduleArray,$A_Value) if ($A_Value ne '');
	push (@scheduleArray,$D_Value) if ($D_Value ne '');
}
foreach my $key1 (0..$LargerCount_BC)
{
	my $B_Value = $B_Category[$key1];
	my $C_Value = $C_Category[$key1];
	push (@scheduleArray,$B_Value) if ($B_Value ne '');
	push (@scheduleArray,$C_Value) if ($C_Value ne '');
}

my $fulldate 				= strftime "%Y-%m-%d %H:%M:%S", localtime;
my $CurrentDateFolder		= strftime "%Y%m%d", localtime;
my $SFDC_INPUTS1 			= &SFDC_AUTOMATION_DB_Daily::ReadIniFile('SFDCINPUTS');
# my $SFDC_INPUTS1 			= &ReadIniFile('SFDCINPUTS');
my %SFDC_INPUTS				= %{$SFDC_INPUTS1};
print "Date :: $fulldate\n";
my $SFDC_ReportConfig_table 	= $SFDC_INPUTS{'sfdc_reportconfig'};
my $Config_Key					= $SFDC_INPUTS{'config_key'};
my $SFDC_statuslog_table		= $SFDC_INPUTS{'sfdc_statuslog'};
my $FilePath					= $SFDC_INPUTS{'filepathdaily'};
my $SFDC_status_Running			= $SFDC_INPUTS{'running_status'};
my $SFDC_status_AutoQARunning	= $SFDC_INPUTS{'autoqa_running'};
# my $SFDC_status_Running		= $SFDC_INPUTS{'running_status'};

foreach my $Category(@scheduleArray)
{
	my $Report_Id				= $Report_Id[$Category];
	my $Report_Name				= $Report_Name[$Category];
	my $category				= $category[$Category];
	my $SP_Id					= $SP_Id[$Category];
	my $Sp_Name					= $Sp_Name[$Category];
	my $SPParam_Id				= $SPParam_Id[$Category];
	my $parameter_count			= $parameter_count[$Category];
	my $Sp_Parameter			= $Sp_Parameter[$Category];
	my $Multisheet_Flg			= $Multisheet_Flg[$Category];
	my $outfile_Name			= $outfile_Name[$Category];
	my $Sheet_Name				= $Sheet_Name[$Category];
	my $outfile_type			= $outfile_type[$Category];
	my $with_header				= $with_header[$Category];
	my $Output_split			= $Output_split[$Category];
	my $Split_Count				= $Split_Count[$Category];
	my $Mail_attachement		= $Mail_attachement[$Category];
	my $FTP_Upload				= $FTP_Upload[$Category];
	my $Retryfrequencyminutes	= $Retryfrequencyminutes[$Category];
	my $Previous_run_duration	= $Previous_run_duration[$Category];
	my $Timeout_Minutes			= $Timeout_Minutes[$Category];
	my $status					= $status[$Category];
	my $expected_count			= $expected_count[$Category];
	# next if ($Report_Name!~m/Travis\s*Perkin/igs);
	print "######################## $Report_Id ########################\n";
	##### Retrieve SP_NAME along with detail for schedule logic #####
	
	my $File_Type_Value 	= &SFDC_AUTOMATION_DB_Daily::Retrieve_FileType($dbh,$outfile_type,$Config_Key,$SFDC_ReportConfig_table);
	my @File_Type_Values	= @{$File_Type_Value};
	
	my @sheet_names_temp	= split ('\:',$Sheet_Name);
	my $SheetCount 			= @sheet_names_temp;
	
	&SFDC_AUTOMATION_DB_Daily::Insert_into_StatusLog_Running($dbh,$SPParam_Id,$SFDC_statuslog_table,$SFDC_status_Running);
	# my $HistoryID = &SFDC_AUTOMATION_DB_Daily::HistoryID_RetrieveUrl($dbh,$SPParam_Id,$History_Table);
	
	print "SP Name :: $Sp_Name\n";
	my ($Dataref_table,$titles) = &SFDC_AUTOMATION_DB_Daily::DATAGET($dbh,$Sp_Name,$SheetCount);
	my @ExcelRef	= @{$Dataref_table};
	my @Headers		= @{$titles};
	
	$outfile_Name=~s/\s+/_/igs;
	my $Report_Name_temp = $Report_Name;
	$Report_Name_temp=~s/\s+/_/igs;
	my $ReportName_Folder_creation = substr($Report_Name_temp, 0, 10);
	my $FileServerpath=$FilePath.$CurrentDateFolder."/$ReportName_Folder_creation";
	unless ( -d $FileServerpath ) 
	{
		$FileServerpath=~s/\//\\/igs;
		system("mkdir $FileServerpath");
	}
	
	my ($Mail_attachmentPath,$Doc_Name_temp);
	foreach my $fileType (@File_Type_Values)
	{
		print "FileType :: $fileType\n";
		$fileType=lc($fileType);
		
		my $Doc_Name = "$outfile_Name.$fileType";
		my $Storefile = "$FileServerpath/$Doc_Name";
		
		# push (@Mail_attachmentPaths,$Storefile);
		
		$Mail_attachmentPath 	= $Storefile;
		$Doc_Name_temp			= $Doc_Name;			
		print "ServerPATH  : $Storefile \n";
		if($fileType=~m/xlsx?\s*$/is)
		{
			#### Create a new Excel workbook ####
			my $workbook;
			if ($fileType=~m/^xls$/is)
			{
				print "Spreadsheet::WriteExcel -> $fileType";
				$workbook = Spreadsheet::WriteExcel->new( $Storefile );
			}	
			if ($fileType=~m/^xlsx$/is)
			{
				$workbook = Excel::Writer::XLSX->new( $Storefile );
			}
			my $format = $workbook->add_format(); # Add a format
			$format->set_font('Calibri');
			$format->set_size('11');
			
			&Excel_Formation(\@ExcelRef,$workbook,$Multisheet_Flg,$with_header,$titles,$Sheet_Name,$format);
			$workbook->close();
			
		}
		elsif($fileType=~m/csv\s*$/is)
		{
			&Csv_Formation(@ExcelRef,$Storefile,$with_header,$titles);
		}
		elsif($fileType=~m/zip\s*$/is)
		{
			&Zip_Formation($Doc_Name,$FileServerpath);
		}
		elsif($fileType=~m/xml\s*$/is)
		{
			&Xml_Formation(@ExcelRef,$Storefile);
		}
	}
	
	&SFDC_AUTOMATION_DB_Daily::Insert_into_StatusLog_AutoQA($dbh,$SPParam_Id,$SFDC_statuslog_table,$SFDC_status_AutoQARunning);
	# exit;
	if ($Mail_attachmentPath ne '')
	{
		&sendmail($Mail_attachmentPath,$Doc_Name_temp,$Report_Id);
	}
}

sub sendmail()
{
	my $fullpath 		= shift;
	my $Doc_Name		= shift;
	my $Report_Id		= shift;
	
	my $MAILDETAILS = &SFDC_AUTOMATION_DB_Daily::ReadIniFile('MAILDETAILS');
	my %MAILDETAILS	= %{$MAILDETAILS};
		
	my $fulldate = strftime "%d %m %Y %A %H:%M:%S", localtime;
	
	print "PAth :: $fullpath\n";
	
	my $subject	= "R105 - Glenigan Supermarkets Daily Reports";
	my $host   	= $MAILDETAILS{'host'}; 
	my $from 	= $MAILDETAILS{'from'};
	my $user 	= $MAILDETAILS{'user'};
	my $to		= $MAILDETAILS{'to'};
	my $cc		= $MAILDETAILS{'cc'};
	my $bcc		= $MAILDETAILS{'bcc'};
	my $pass	= $MAILDETAILS{'password'};
	
	my $body;
	if($Report_Id==156)
	{
		$body	= "Hi Sue,<br><br>Please find attached the output for �R105 - Glenigan Supermarkets Daily Reports�.<br><br>Note : This is an automatically generated email, please don't reply.<br><br>"."Thanks & Regards,<br>MERIT SFDC-IT";
	}
	else
	{
		$body	= "Hi Sue,<br><br>Please find attached the Friday's file for �R105 - Glenigan Supermarkets Daily Reports�.<br><br>Note : This is an automatically generated email, please don't reply.<br><br>"."Thanks & Regards,<br>MERIT SFDC-IT";
	}
	
	my $msg = MIME::Lite->new (
	  From => $from,
	  To => $to,
	  Cc => $cc,
	  Bcc => $bcc,
	  Subject => $subject,
	  Data => $body,
	  Type =>'text/html'
	) or die "Error creating multipart container: $!\n";
	$msg->attach(
        Type     =>'Excel',
        Path     =>"$fullpath",
        Filename =>"$Doc_Name"
    );
	
	
    MIME::Lite->send('smtp', $host, Timeout=>60,Auth=>'LOGIN',AuthUser=>$user,AuthPass=>$pass,Port => 25, Debug => 1);
	
	eval {$msg->send;};
	if($@)
	{
		my $fulldate = strftime "%Y-%m-%d %H:%M:%S", localtime;
		print "\n\n>>>>>>>>Mail Failed>>>>>>>>\n\n";
		
		my $logLocation = $logsDirectory.'/FailedStatus/'.$todaysDate;
				
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		open(ERR,">>$logLocation/MailLog_$todaysDate.txt");
		print ERR "MailFailed\t$fulldate\n";
		close ERR;
	}
	else
	{
		my $logLocation = $logsDirectory.'/Status/'.$todaysDate;
				
		unless ( -d $logLocation )
		{
			make_path($logLocation);
		}
		
		my $fulldate = strftime "%Y-%m-%d %H:%M:%S", localtime;
		print "\n\n>>>>>>>>Mail Sent>>>>>>>>\n\n";
		open(ERR,">>$logLocation/MailLog_$todaysDate.txt");
		print ERR "MailSend\t$fulldate\n";
		close ERR;
	}
}



sub Excel_Formation()
{
	my $ExcelRef1			= shift;
	my $workbookRef			= shift;
	my $Multisheet_Flg1		= shift;
	my $write_header_check	= shift;
	my $Titless				= shift;
	my $Sheet_Name1			= shift;
	my $format_temp			= shift;
	
	my @ExcelRef = @{$ExcelRef1};

	
	my @sheetNames = split ('\:',$Sheet_Name1);

	print "Inside the Excel Formation\n";
	


	my $count=0;
	my $row=0;
	my $Sheet_reference;
	foreach my $key (@ExcelRef) #### Fetch_All_Array Reference
	{
		print "Values :: $key\n";
		if ($Multisheet_Flg1==1)
		{
			# $Sheet_reference = $BookRefernce->Worksheets($count+1);
			my $SheetNames = $sheetNames[$count];
			print "SheetNames==>$SheetNames\n";
			my $worksheet = $workbookRef->add_worksheet($SheetNames);
			&ExcelWrite($key,$worksheet,$row,$write_header_check,$Titless,$format_temp);
		}
		else
		{
			# $Sheet_reference = $BookRefernce->Worksheets(1);
			my $worksheet = $workbookRef->add_worksheet();
			$row = &ExcelWrite($key,$worksheet,$row,$write_header_check,$Titless,$format_temp);
		}
		$count++;
	}
	print "Excel Formation End\n";
	
	# return ($BookRefernce);
}

sub ExcelWrite()
{
	my $array_Reference 	= shift;
	my $sheet_Reference		= shift;
	my $Row					= shift;
	my $header_check		= shift;
	my $titless				= shift;
	my $format_temps		= shift;
	
	print "Inside the Excel Write\n";
	my @ExcelRef1	= @{$array_Reference};
	my @HeaderS		= @{$titless};

	if ($header_check==1)
	{
		#### Add and define a format ####
		
		for( my $h=0; $h<=$#HeaderS; $h++)
		{
			$sheet_Reference->write(0, $h, $HeaderS[$h], $format_temps);
		}
		$Row++;
	}
	
	foreach my $key1 (@ExcelRef1) #### Row Reference
	{
		print "key :: $key1\n";
		
		my @ExcelRef2 = @{$key1};
		# open (RE,">>Result.txt");
		# print RE "$ExcelRef2[0]\t$ExcelRef2[1]\n";
		# close RE;
		my $col = 0;
		foreach my $key2 (@ExcelRef2) #### Coloumn Reference
		{
			print "Values :: $key2\n";
			# $sheet_Reference->Cells($Row,$col)->{Value} = $key2;
			$sheet_Reference->write($Row, $col, $key2, $format_temps);
			$col++;
		}
		$Row++;
	}
	return $Row;
}

sub Xml_Formation()
{
	my $row			= shift;
	my $filepath	= shift;

	my @RowReference = @{$row};
	# $filepath=~s/\.xml/.txt/igs;
	
	
	foreach my $rowRef (@RowReference)
	{
		my @RowReference1 = @{$rowRef};
		# print "Row Reference :: $rowRef\n";
		foreach my $rowRef1 (@RowReference1)
		{
			open (RE,">>$filepath");
			print RE "$rowRef1";
			close RE;
		}
	}
	
}

sub Csv_Formation()
{
	my $row			= shift;
	my $filepath	= shift;
	my $with_header1= shift;
	my $titles1		= shift;
	
	print "Inside the CSV Formation\n";
	
	my $csv = Text::CSV->new({ binary => 1, eol => $/ }) or die "Cannot use CSV: " . Text::CSV->error_diag();
	open my $fh, ">:encoding(utf8)", "$filepath" or die "new.csv: $!";
	
	my @RowReference = @{$row};
	
	if ($with_header1==1)
	{
		$csv->print($fh, $titles1);
	}	
	foreach my $rowRef (@RowReference)
	{
		$csv->print($fh, $rowRef);
	}
	close $fh or die "$filepath: $!";
}
sub Zip_Formation()
{
	my $docname		= shift;
	my $storepath	= shift;
	
	my $zip = Archive::Zip->new();
    $zip->setChunkSize(65536);
	
	print "Inside the Zip Formation\n";
	
	my @files = glob( $storepath .'/*' );
	
	foreach my $tempfilenames (@files)
	{
		my $tempfilenames1 =(split ('\/',$tempfilenames))[-1];
		
		print "Tempfiles:::$tempfilenames\n";
		
		eval{
			my $file1 = File::Spec->catfile( $storepath, $tempfilenames1 );
			$zip->addFile( $file1, $tempfilenames1 )->desiredCompressionLevel(6);
			
			my $zipped_file = File::Spec->catfile( $storepath, $docname );
			if ( $zip->writeToFileNamed($zipped_file) != AZ_OK ) 
			{
				print STDERR "Zip Creation error\n";
			}
		};
		if ($@)
		{
			print "Zip Creation error\n";
		}
	}
}
