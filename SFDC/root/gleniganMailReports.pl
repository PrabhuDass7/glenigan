############################################
# Team        :	SFDC                   	   #
# VERSION     : 1.1				           #
# LAST RELEASE: 11/10/2019				   #
# Developer   : Pari Elavarasan			   #
############################################

#!/usr/local/bin/perl
package main;

use utf8;
use strict;
use IO::Socket::SSL;
use Mail::IMAPClient;
use HTML::Entities;
use DateTime;
use Cwd qw(abs_path);
use File::Basename;
use File::Path qw(make_path remove_tree);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root$//is;

# print "$basePath\n"; <STDIN>;


my $Date = DateTime->now();
my $todaysDate = $Date->ymd('');

my $hms    = $Date->hms;
my $year = $Date->year;
my $day = $Date->day;

# $day=$day-1; # one day subract

my $month = $Date->format_cldr( $Date->locale->format_for('MMM') );
$day=sprintf("%02d",$day);

my $start_date = "$day-$month-$year";

chomp($start_date);

if(($start_date ne '') && ($start_date=~m/^\d{2}-[A-Za-z]{3}\-\d{4}$/is))
{
	print "You entered the input date is : $start_date\n"; 
	# <STDIN>;
}
else
{
	print "Sorry enter the correct date format like \(01-APR-2018\)\n";
	exit;
}

my $mail_ssl = 1;
my $socket;
if( $mail_ssl ) {
	# Open up a SSL socket to use with IMAPClient later
	$socket = IO::Socket::SSL->new(
		PeerAddr => 'imap.gmail.com',
		PeerPort => 993,
		Timeout => 5,
	);
} else {
	# Open up a none SSL socket to use with IMAPClient later
	$socket = IO::Socket::INET->new(
		PeerAddr => 'imap.gmail.com',
		PeerPort => 143,
		Timeout => 5,
	);
}
  
my $client = Mail::IMAPClient->new(
			Socket   => $socket,
			# port     => 993,
			# use_ssl  => 1,
			# Type     => 'image/gif',
			# Encoding => 'base64',
			User     => 'bespoke.reportingteam@meritgroup.co.uk',
			Password => '11merit11', 
			Timeout => 5, 
			)
			or die "new(): $@";

print "\n.......\[$start_date\] Inbox Mail Reading.......\n";

$client->select('INBOX');

my @mails = $client->since($start_date)
or warn "Could not find any messages sent since $start_date: $@\n";


my ($patternMatch, $mailMatch, $mailSentStatus,$newpatternMatch,$newMailMatch);
foreach my $id (@mails) 
{
	my $subject = $client->get_header($id, 'Subject');
	# my $text = $client->body_string($id); 
	$subject=&trim($subject);
	# $text=&trim($text);
	
	print "subject==>$subject\n"; 
	# <STDIN>;
	
	
}
$client->logout();


sub trim()
{
	my $main=shift;
	$main=~s/<[^>]*?>/ /igs;
	$main=~s/\&quot\;/\"/igs;
	$main=~s/amp\;//igs;
	$main=~s/\s+/ /igs;
	$main=~s/^\s+//igs;
	$main=~s/\s+$//igs;
	return $main;
}