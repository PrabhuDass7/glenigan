############################################
# Team        :	SFDC                   	   #
# VERSION     : 1.1				           #
# LAST RELEASE: 06/04/2018				   #
# Developer   : Pari Elavarasan			   #
############################################

#!/usr/local/bin/perl
package main;

use utf8;
use strict;
use IO::Socket::SSL;
use Mail::IMAPClient;
use HTML::Entities;
use DateTime;
# use SFDC_AUTOMATION_DB_Daily;
use Cwd qw(abs_path);
use File::Basename;
use File::Path qw(make_path remove_tree);

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0); 
$basePath=~s/\/root$//is;

# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $logsDirectory = ($basePath.'/logs');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $rootDirectory = ($basePath.'/root');
# print "$rootDirectory\n"; <STDIN>;


# Private Module
require ($libDirectory.'/SFDC_AUTOMATION_DB_Daily.pm'); 


##### Database Initialization #####
my $dbh = &SFDC_AUTOMATION_DB_Daily::DbConnection();
print "DBStatus	:: DB Connected\n";

##### Retrieve TableRefreshTime Count #####

my $Referesh_count = &SFDC_AUTOMATION_DB_Daily::Retrieve_SMTableRefreshTime_Count($dbh,'TableRefreshTime');
my @Referesh_count = @{$Referesh_count};
print "Referesh_count :: $Referesh_count[0]\n"; 


my $Date = DateTime->now();
my $todaysDate = $Date->ymd('');

my $hms    = $Date->hms;

my $year = $Date->year;
my $day = $Date->day;
# $day=$day-1; # one day subract
my $month = $Date->format_cldr( $Date->locale->format_for('MMM') );
$day=sprintf("%02d",$day);

my $start_date = "$day-$month-$year";

chomp($start_date);

if(($start_date ne '') && ($start_date=~m/^\d{2}-[A-Za-z]{3}\-\d{4}$/is))
{
	print "You entered the input date is : $start_date\n"; 
	# <STDIN>;
}
else
{
	print "Sorry enter the correct date format like \(01-APR-2018\)\n";
	exit;
}


my $socket = IO::Socket::SSL->new(
   PeerAddr => 'imap.gmail.com',
   PeerPort => 993,
  )
  or die "socket(): $@";
  
 my $client = Mail::IMAPClient->new(
    Socket   => $socket,
    port     => 993,
    use_ssl  => 1,
	Type     => 'image/gif',
	Encoding => 'base64',
    User     => 'bespoke.reportingteam@meritgroup.co.uk',
    Password => '11merit11',  
  )
  or die "new(): $@";

print "\n.......\[$start_date\] Inbox Mail Reading.......\n";

$client->select('INBOX');

my @mails = $client->since($start_date)
or warn "Could not find any messages sent since $start_date: $@\n";


my ($patternMatch, $mailMatch, $mailSentStatus,$newpatternMatch,$newMailMatch);
foreach my $id (@mails) 
{
	my $subject = $client->get_header($id, 'subject');
	my $text = $client->body_string($id); 
	$subject=&trim($subject);
	$text=&trim($text);
	
	print "subject==>$subject\n"; 
	# <STDIN>;
	
	if($subject=~m/R105\s*\-\s*Glenigan\s*Supermarkets\s*Daily\s*Reports/is)
	{
		$mailMatch = "Yes";
		$newMailMatch=$mailMatch;
		last;
	}
	else
	{
		$mailSentStatus = "No";
	}
	
	if($newpatternMatch ne "Yes")
	{
		if($subject=~m/SQL\s*Server\s*Job\s*System\s*\:\s*\'\s*Supermarket\s*\'\s*completed\s*on/is)
		{
			$patternMatch = "Yes";
			$newpatternMatch=$patternMatch;
			# last;
			# print "patternMatch==>$patternMatch\n"; <STDIN>;		
		}
		else
		{
			$patternMatch = "No";
			$mailSentStatus = "No";
			# print "Searching next Inbox Mail here\n"; 
			# <STDIN>;
		}
	}
}
$client->logout();

# print "NewpatternMatch==>$newpatternMatch\n"; <STDIN>;

my ($Return_Code,$errorValue,$script_name);
if($newpatternMatch eq "Yes")
{									               				
	$script_name = $rootDirectory."/SuperMarket/SFDC_Automation_Final_V3.pl";
	my $file = "SupermarketMailTrigger";
	
	print "SciptPath==>$script_name\n";
	
	# print "mailMatch==>$mailMatch\n";
	
	# <STDIN>;
	
	if(($newMailMatch=~m/^\s*$/is) or (($newMailMatch ne "Yes") && ($mailSentStatus eq "No")))
	{
		$mailSentStatus = "Yes";
		
		
		eval{		
			$Return_Code = system("perl $script_name &");
		};
		if($@)
		{
			$errorValue = $@;
		}
		
		
		print "Mail triggered successfully for \"Supermarket\"\n";
	}
	elsif($newMailMatch eq "Yes")
	{
		$mailSentStatus = "Yes, Already Sent";
		print "Already mail triggered for \"Supermarket\"\n";
	}
		
	# print "Bravo...Task Completed\n";
}
elsif($newMailMatch eq "Yes")
{
	$mailSentStatus = "Yes, Already Sent";
}
else
{
	print "No message found in INBOX like \"Supermarket\"\n";
}

my $logLocation = $logsDirectory.'/Status/'.$todaysDate;

unless ( -d $logLocation )
{
	make_path($logLocation);
}

open (PP,">>$logLocation/SuperMarketLogs_$todaysDate.txt");
print PP "Date::$start_date\tTime::$hms\tRefreshCount::$Referesh_count[0]\tJobMailFound::$newpatternMatch\tMailSentStatus::$mailSentStatus\n";
close(PP);

open(ERR,">>$logLocation/SuperMarketLogs_Trigger_Log.txt");
if($Return_Code!=0)
{
	print ERR "Failed::$errorValue\t$script_name\n";
}
else
{
	print ERR "Success: $errorValue\t$script_name\n";
}
close ERR;


sub trim()
{
	my $main=shift;
	$main=~s/<[^>]*?>/ /igs;
	$main=~s/\&quot\;/\"/igs;
	$main=~s/amp\;//igs;
	$main=~s/\s+/ /igs;
	$main=~s/^\s+//igs;
	$main=~s/\s+$//igs;
	return $main;
}