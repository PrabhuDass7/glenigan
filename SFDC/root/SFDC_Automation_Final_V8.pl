##### Module Initialization #####
use strict;
use Text::CSV;
use List::Util qw( max );
use POSIX 'strftime';
use Spreadsheet::WriteExcel;
use Spreadsheet::ParseExcel::SaveParser;
use Excel::Writer::XLSX;
use Date::Manip;
use DateTime;
use DateTime::Format::DateParse;
use Archive::Zip qw( :ERROR_CODES );
use File::Spec;
use File::stat;
use Data::Dumper;
use HTML::Entities;
use Encode;
use Encode qw( is_utf8 encode decode );
use Encode qw( decode_utf8 );
use utf8;
use File::Copy::Recursive qw(dircopy);
use experimental 'smartmatch';
use Time::Piece;
use Time::Seconds;
use Win32::OLE;
use Cwd qw(abs_path);
use File::Basename;
use File::Path qw(make_path remove_tree);
use DateTime::Duration;

# Set root path for accessing files
my $basePath = dirname (dirname abs_path $0);
$basePath=~s/\/root$//is; 

# print "$basePath\n"; <STDIN>;

####
# Declare and load local directories and required private module
####

my $libDirectory = ($basePath.'/lib');
my $iniDirectory = ($basePath.'/ctrl');
my $dataDirectory = ($basePath.'/data');
my $logDirectory = ($basePath.'/logs');
my $macroDirectory = ($basePath.'/macro');

# my $FilePath = $dataDirectory;

# Private Module
require ($libDirectory.'/SFDC_AUTOMATION_DB_V1.pm'); 

my $inputReportID =  $ARGV[0];



####
# Declare method for DateTime
####
my $Date = DateTime->now;
my $todaysDate = $Date->ymd('');



##### Converting from alphabet into numerical #####
my %MonthHash = ('JAN'=> '01','FEB'=> '02','MAR'=>'03','APR'=>'04','MAY'=>'05','JUN'=>'06','JUL'=>'07','AUG'=>'08','SEP'=>'09','OCT'=>'10','NOV'=>'11','DEC'=>'12','JANUARY'=> '01','FEBRUARY'=> '02','MARCH'=> '03','APRIL'=> '04','MAY'=>'05','JUNE'=>'06','JULY'=>'07','AUGUST'=> '08','SEPTEMBER' => '09','OCTOBER'=> '10','NOVEMBER'=> '11','DECEMBER'=> '12');

my $fulldate 				= strftime "%Y-%m-%d %H:%M:%S", localtime;
my $CurrentDateFolder		= strftime "%Y%m%d", localtime;
my $CurrentYear 			= strftime "%Y", localtime;

##### Read Inputs from INI File #####
my $SFDC_INPUTS1 			= &SFDC_AUTOMATION_DB_V1::ReadIniFile('SFDCINPUTS');
my %SFDC_INPUTS				= %{$SFDC_INPUTS1};

##### Database Initialization #####
my $dbh = &SFDC_AUTOMATION_DB_V1::DbConnection();
print "DBStatus	:: DB Connected\n";

##### Retrieve TableRefreshTime Count #####
my $TableRefreshTime 	= $SFDC_INPUTS{'tablerefresh'};
my $Referesh_count = &SFDC_AUTOMATION_DB_V1::Retrieve_TableRefreshTime_Count($dbh,$TableRefreshTime);
my @Referesh_count = @{$Referesh_count};
print "Referesh_count :: $Referesh_count[0]\n"; 

# if ($Referesh_count[0]>=108) #Commented and added below line to check table refresh count to be 111 on 18th Jan

if($Referesh_count[0]==118)
{
	print "******* Replication Successfull *******\n";
	my $fulldate_temp = strftime "%Y-%m-%d %H:%M:%S", localtime;
	
	my $logLocation = $logDirectory."/Status/".$todaysDate;
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	open (RE,">>$logLocation/ReplicationSuccessLog_$todaysDate.txt");
	print RE "Replication Success\t$Referesh_count[0]\t$fulldate_temp\n";
	close RE;
}
else
{
	print "******* Replication Failed *******\n";
	my $fulldate_temp = strftime "%Y-%m-%d %H:%M:%S", localtime;
	
	my $logLocation = $logDirectory."/FailedStatus/".$todaysDate;
	unless ( -d $logLocation )
	{
		make_path($logLocation);
	}
	
	open (RE,">>$logLocation/ReplicationFailedLog_$todaysDate.txt");
	print RE "Replication Failed\t$Referesh_count[0]\t$fulldate_temp\n";
	close RE;
	exit;
}

##### Retrieve SP_NAME along with detail for schedule logic #####
my ($Report_Id,$Report_Name,$category,$SP_Id,$Sp_Name,$SPParam_Id,$parameter_count,$Sp_Parameter,$Multisheet_Flg,$outfile_Name,$Sheet_Name,$outfile_type,$with_header,$Output_split,$Split_Count,$Mail_attachement,$FTP_Upload,$Retryfrequencyminutes,$Previous_run_duration,$Timeout_Minutes,$status,$expected_count,$FrequencyOrderBy) = &SFDC_AUTOMATION_DB_V1::Retrieve_Schedule_LogicData($dbh,$inputReportID);

my @Report_Id				= @{$Report_Id};
my @Report_Name				= @{$Report_Name};
my @category				= @{$category};
my @SP_Id					= @{$SP_Id};
my @Sp_Name					= @{$Sp_Name};
my @SPParam_Id				= @{$SPParam_Id};
my @parameter_count			= @{$parameter_count};
my @Sp_Parameter			= @{$Sp_Parameter};
my @Multisheet_Flg			= @{$Multisheet_Flg};
my @outfile_Name			= @{$outfile_Name};
my @Sheet_Name				= @{$Sheet_Name};
my @outfile_type			= @{$outfile_type};
my @with_header				= @{$with_header};
my @Output_split			= @{$Output_split};
my @Split_Count				= @{$Split_Count};
my @Mail_attachement		= @{$Mail_attachement};
my @FTP_Upload				= @{$FTP_Upload};
my @Retryfrequencyminutes	= @{$Retryfrequencyminutes};
my @Previous_run_duration	= @{$Previous_run_duration};
my @Timeout_Minutes			= @{$Timeout_Minutes};
my @status					= @{$status};
my @expected_count			= @{$expected_count};
# my @FrequencyOrderBy		= @{$FrequencyOrderBy};


##### Inputs from ini file #####
my $SFDC_ReportConfig_table 	= $SFDC_INPUTS{'sfdc_reportconfig'};
my $Config_Key					= $SFDC_INPUTS{'config_key'};
my $SFDC_statuslog_table		= $SFDC_INPUTS{'sfdc_statuslog'};
my $SFDC_Spparameter_table		= $SFDC_INPUTS{'sfdc_spparameter_update'};
my $SFDC_ReportHistory			= $SFDC_INPUTS{'sfdc_reporthistory'};
my $SFDC_ReportRunHistory		= $SFDC_INPUTS{'sfdc_reportrunhistory'};
my $SFDC_Reports				= $SFDC_INPUTS{'sfdc_reports'};
my $FilePath					= $SFDC_INPUTS{'filepathsfdc'};
my $FileSharePath				= $SFDC_INPUTS{'filepathsfdcsharepath'};
my $SFDC_status_Running			= $SFDC_INPUTS{'running_status'};
my $SFDC_status_AutoQARunning	= $SFDC_INPUTS{'autoqa_running'};
my $addressformattingid			= $SFDC_INPUTS{'addressformattingid'};
my @AddressFormattingID = split ("\,",$addressformattingid);

#Count Variables for Report 154 and 55
my $count_154=0;
my $count_55=0;

# =pod
##### Looping for report execution one by one #####
##### Retrieve SP_NAME along with detail for schedule logic #####
for(my $Category=0; $Category<@Report_Id; $Category++)
{
	my $Report_Id				= $Report_Id[$Category];
	my $Report_Name				= $Report_Name[$Category];
	my $category				= $category[$Category];
	my $SP_Id					= $SP_Id[$Category];
	my $Sp_Name					= $Sp_Name[$Category];
	my $SPParam_Id				= $SPParam_Id[$Category];
	my $parameter_count			= $parameter_count[$Category];
	my $Sp_Parameter			= $Sp_Parameter[$Category];
	my $Multisheet_Flg			= $Multisheet_Flg[$Category];
	my $outfile_Name			= $outfile_Name[$Category];
	my $Sheet_Name				= $Sheet_Name[$Category];
	my $outfile_type			= $outfile_type[$Category];
	my $with_header				= $with_header[$Category];
	my $Output_split			= $Output_split[$Category];
	my $Split_Count				= $Split_Count[$Category];
	my $Mail_attachement		= $Mail_attachement[$Category];
	my $FTP_Upload				= $FTP_Upload[$Category];
	my $Retryfrequencyminutes	= $Retryfrequencyminutes[$Category];
	my $Previous_run_duration	= $Previous_run_duration[$Category];
	my $Timeout_Minutes			= $Timeout_Minutes[$Category];
	my $status					= $status[$Category];
	my $expected_count			= $expected_count[$Category];
		
	print "######################## $Report_Id ########################\n";
	my $File_Type_Value 	= &SFDC_AUTOMATION_DB_V1::Retrieve_FileType($dbh,$outfile_type,$Config_Key,$SFDC_ReportConfig_table);  ### Retrieve FileTypes from DB ###
	my @File_Type_Values	= @{$File_Type_Value};
	
	my @sheet_names_temp	= split ('\:',$Sheet_Name);
	my $SheetCount 			= @sheet_names_temp;
	my @grepSheetNames = grep(/TRANSPOSE/is, @sheet_names_temp);
	my $transposeFlag;
	if (@grepSheetNames)
	{
		$transposeFlag='Y';
		print "transposeFlag :: $transposeFlag\n";
	}
	else
	{
		$transposeFlag='N';
		print "transposeFlag :: $transposeFlag\n";
	}
	
	&SFDC_AUTOMATION_DB_V1::Insert_into_StatusLog_Running($dbh,$SPParam_Id,$SFDC_statuslog_table,$SFDC_status_Running); ### Insert into StatusLog for running status ###
	
	my $datestring = time();
	my $todaystart = DateTime->now( time_zone => 'Europe/London' );
	# my $todaystart = DateTime->now();
	
	 
	$todaystart=~s/T/ /gsi;
	print "Start Time   :: $todaystart\n";
	
	print "SP Name		:: $Sp_Name\n";
	print "SP Parameter	:: $Sp_Parameter\n";
	# <STDIN>;
	
	my ($Dataref_table,$secDataref_table,$titles,$overallcount);
	if($Report_Id=~m/^89$/is)
	{
		($Dataref_table,$secDataref_table,$titles,$overallcount) = &SFDC_AUTOMATION_DB_V1::NewDATAGET_MultipleResultSet($dbh,$Sp_Name,$Sp_Parameter,$SheetCount,$SPParam_Id,$SFDC_statuslog_table);
	}
	else
	{
		($Dataref_table,$titles,$overallcount) = &SFDC_AUTOMATION_DB_V1::DATAGET_MultipleResultSet($dbh,$Sp_Name,$Sp_Parameter,$SheetCount,$SPParam_Id,$SFDC_statuslog_table);
	}
	
	my @Headers1	= @{$titles};

	##### Transpose Flag Check #####
	my (@ExcelRef,@SecExcelRef,@Headers);
	if ($transposeFlag eq 'Y')
	{
		my ($DataReference,$Headerss)=&Transpose($Dataref_table,\@Headers1,\@sheet_names_temp,$Report_Id);
		@ExcelRef	= @{$DataReference};
		@Headers	= @{$Headerss};
		# $titles = $Headerss;
	}
	else
	{
		@ExcelRef	= @{$Dataref_table};
		if($secDataref_table ne "")
		{
			@SecExcelRef	= @{$secDataref_table};
		}
		@Headers	= @{$titles};
	}
	
	##### Folder Creation #####
	# $outfile_Name=~s/\s+/_/igs; ## Request Mail Subject: SFDC Regular Report Automation: Formatting Requierments
	
	my $Report_Name_temp = $Report_Name;
	$Report_Name_temp=~s/\s+/_/igs;
	$Report_Name_temp=~s/\W+/_/igs;
	my $ReportName_Folder_creation = substr($Report_Name_temp, 0, 10);
	my $ReportName_and_ID = $Report_Id."_".$ReportName_Folder_creation;
	my $FileServerpath=$FilePath."/".$CurrentDateFolder."/$ReportName_and_ID";
	unless ( -d $FileServerpath ) 
	{
		$FileServerpath=~s/\//\\/igs;
		system("mkdir $FileServerpath");
	}
	
	if($outfile_type=~m/^\s*4\s*$|^\s*324\s*$|^\s*375\s*$|^\s*376\s*$|^\s*377\s*$|^\s*378\s*$|^\s*379\s*$/is)
	{
		my $PDF_Filename=$FileServerpath."/$outfile_Name";
		$PDF_Filename=~s/\s/\_/isg;
		$PDF_Filename=~s/\//\\/igs;
		system("mkdir $PDF_Filename");	
	}
	
	if($Report_Id=~m/^53$/is)
	{
		if($outfile_Name=~m/\_YYYY\-MM\-DD(?:\-HH\-mm)?$/is)
		{
			my $dupDate=$fulldate;
			$dupDate=~s/^(\d{4}\-\d{2}\-\d{2})\s(\d{2})\:(\d{2})\:(\d{2})$/$1\-$2\-$3/is;
			$outfile_Name=~s/\_YYYY\-MM\-DD(?:\-HH\-mm)?$/\_$dupDate/si;
		}
	}
	
	my @filetype_values="";
	my $fileTypes = "@File_Type_Values";
	if($fileTypes =~ m/\+/is)
	{
		@filetype_values=split (/\+/,$fileTypes);
	}
	else
	{
		@filetype_values=$fileTypes;
	}
	
	my $ZipFileCheck;
	foreach my $FileType (@filetype_values)
	{
		$ZipFileCheck=1 if($FileType=~m/zip/is);
	}
	
	foreach my $fileType (@filetype_values)
	{
		print "FileType :: $fileType\n";
		$fileType=lc($fileType);
		$fileType=~s/pdf/xlsx/igs;
		my $Doc_Name = "$outfile_Name.$fileType";
		
		print "Before formatting date: $Doc_Name\n";
		# <STDIN>;
			
		my %MonthsHash = ('01'=> 'JAN','02'=> 'FEB','03'=>'MAR','04'=>'APR','05'=>'MAY','06'=>'JUN','07'=>'JUL','08'=>'AUG','09'=>'SEP','10'=>'OCT','11'=>'NOV','12'=>'DEC', '-2'=>'OCT','-1'=>'NOV','00'=>'DEC');
					
		my $mon;
		if($Doc_Name=~m/DDMMMYYYY\s*to\s*DDMMMYYYY_YYYYMMDD/is)
		{
			# print "Before formatting date: $Doc_Name\n";<>;
			$Doc_Name=~s/\s*\-[^\.]*?\./\./is;
			my $t = localtime() - ONE_WEEK;
			my $last_week= $t->ymd;
			$last_week=~s/^(\d{4})\-(\d+)\-(\d+)$/$3$MonthsHash{$2}$1/isg;
			
			my $t = localtime() - ONE_DAY;
			my $yesterday=$t->ymd;
			$yesterday=~s/^(\d{4})\-(\d+)\-(\d+)$/$3$MonthsHash{$2}$1/isg;
			
			my $t=localtime();
			my $today=$t->ymd;
			$today=~s/[^\d]+//isg;
			
			my $join_date="- $last_week to $yesterday\_$today";
			print "$last_week\n$yesterday\n$today\n";
			print $join_date;
			
			$Doc_Name=~s/\.(.*?)$/$join_date\.$1/is;
			# print "After formatting date: $Doc_Name\n";<>;
						
		}
		elsif(($Doc_Name=~m/\sDDMMYYYY/is) && ($Report_Id=~m/^1753$/is))
		{
			print "Doc_Name==>$Doc_Name\n";
			my $year  = $Date->year;
			my $mon  = $Date->month;
			my $day  = $Date->day;
			my $cmon = sprintf ("%02d",$mon);
			my $mday = sprintf ("%02d",$day);
			
			$Doc_Name=~s/\sDDMMYYYY/ ${mday}${cmon}${year}/is;
			# print "$Doc_Name\n"; <STDIN>;
		}
		elsif(($Doc_Name=~m/YYYYMMDD/is) && ($Report_Id=~m/^(1764|1779)$/is))
		{
			print "Doc_Name==>$Doc_Name\n";
			my $year  = $Date->year;
			my $mon  = $Date->month;
			my $day  = $Date->day;
			my $cmon = sprintf ("%02d",$mon);
			my $mday = sprintf ("%02d",$day);
			
			$Doc_Name=~s/YYYYMMDD/${year}${cmon}${mday}/is;
			# print "$Doc_Name\n"; <STDIN>;
		}
		elsif($Doc_Name=~m/(MMM)\s*[A-Z]+/is)
		{
			my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();	
			my $one_month_ago = join("-", (1900+$year), ($mon), $mday);
			if($one_month_ago=~m/^\d{4}\-(\-?\d+)\-\d+$/is)
			{
				$mon = sprintf ("%02d",$1);
			}
			
			my $month  = $MonthsHash{$mon};
			
			$Doc_Name=~s/YYYY/$CurrentYear/is;
			$Doc_Name=~s/MMM/$month/is;
			print "After formatting date: $Doc_Name\n";
		}
		elsif($Doc_Name=~m/(MMM)(\d+)/is)
		{			
			my $pday = $2;
			if($pday=~m/^1$/is)
			{
				my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();	
				my $three_month_ago = join("-", (1900+$year), ($mon-2), $mday);
				if($three_month_ago=~m/^\d{4}\-(\-?\d+)\-\d+$/is)
				{
					$mon = sprintf ("%02d",$1);
				}
				
				my $month  = $MonthsHash{$mon};
				
				$Doc_Name=~s/YYYY/$CurrentYear/is;
				$Doc_Name=~s/MMM1/$month/is;
				print "After formatting date: $Doc_Name\n";
			}
			elsif($pday=~m/^2$/is)
			{
				my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();	
				my $two_month_ago = join("-", (1900+$year), ($mon-1), $mday);
				if($two_month_ago=~m/^\d{4}\-(\-?\d+)\-\d+$/is)
				{
					$mon = sprintf ("%02d",$1);
				}
				
				my $month  = $MonthsHash{$mon};
				
				$Doc_Name=~s/YYYY/$CurrentYear/is;
				$Doc_Name=~s/MMM2/$month/is;
				print "After formatting date: $Doc_Name\n";
			}
			elsif($pday=~m/^3$/is)
			{
				my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();	
				my $one_month_ago = join("-", (1900+$year), ($mon), $mday);
				if($one_month_ago=~m/^\d{4}\-(\-?\d+)\-\d+$/is)
				{
					$mon = sprintf ("%02d",$1);
				}
				
				my $month  = $MonthsHash{$mon};
				
				$Doc_Name=~s/YYYY/$CurrentYear/is;
				$Doc_Name=~s/MMM3/$month/is;
				print "After formatting date: $Doc_Name\n";
			}
		}
		elsif($Report_Id=~m/^660$/is)
		{
			# my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
			# my $one_month_ago = join("-", (1900+$year), ($mon), $mday);
			
			# if($one_month_ago=~m/^\d{4}\-(\-?\d+)\-(\d+)$/is)
			# {
				# $mon = sprintf ("%02d",$1);
				# $mday = sprintf ("%02d",$2);
			# }
			
			my $year = $Date->year;
			my $mon  = $Date->month;
			my $day  = $Date->day;
			
			my $cmon = sprintf ("%02d",$mon);
			my $mday = sprintf ("%02d",$day);
			
			$Doc_Name=~s/\s/\_/gis;
			$Doc_Name=~s/(\.xlsx)$/\_${year}${cmon}${mday}$1/is;
			# print "$Doc_Name\n"; <STDIN>;
		}
		elsif($Report_Id=~m/^1760$/is)
		{				
			my $month_before = $Date->month_name($Date->subtract(months => 1));
			
			$Doc_Name=~s/^([^<]*?)$/${month_before} $1/is;
			print "$Doc_Name\n"; 
			# <STDIN>;
		}
					
		my $Storefile = "$FileServerpath/$Doc_Name";
		print "ServerPATH  : $Storefile \n";
		my $Total;
		if($fileType=~m/xlsx?\s*$/is)
		{
			#### Create a new Excel workbook ####
			my $workbook;
			if ($fileType=~m/^xls$/is)
			{
				$workbook = Spreadsheet::WriteExcel->new( $Storefile );
			}	
			if ($fileType=~m/^xlsx$/is)
			{
				$workbook = Excel::Writer::XLSX->new( $Storefile );
			}
			
			my $format = $workbook->add_format(); # Add a format
			$format->set_font('Calibri');
			$format->set_size('11');

			my $date_format = $workbook->add_format(num_format => 'dd-mmm-yy'); # Add a format for Date
			$date_format->set_font('Calibri');
			$date_format->set_size('11');
			my $date_format1 = $workbook->add_format(num_format => 'dd/mm/yyyy'); # Add a format for Date
			$date_format1->set_font('Calibri');
			$date_format1->set_size('11');
			my $date_format2 =  $workbook->add_format(num_format => 'mmm-yy', align => 'left');
			$date_format2->set_font('Calibri');
			$date_format2->set_size('11');
			#For 662 Report ID####
			my $format1 = $workbook->add_format(); # Add a format
		   $format1->set_bold();
		   $format1->set_bg_color('#b3b3b3');
		   $format1->set_align('center');
			
			$Total=Excel_Formation(\@ExcelRef,$workbook,$Multisheet_Flg,$with_header,\@Headers,$Sheet_Name,$format,$date_format,$date_format1,$date_format2,$Report_Id,$format1);
			$workbook->close();
		}
		elsif($fileType=~m/csv\s*$/is)
		{
			if($Report_Id =~ m/^(89)$/is)
			{
				my ($Split_Count,$outfile_Name) = &SFDC_AUTOMATION_DB_V1::Retrieve_SplitCount_FileName($dbh,$SPParam_Id,$SFDC_Spparameter_table); 
				
				my @Split_Count_Values	= @{$Split_Count};
				my @outfile_Name_Values	= @{$outfile_Name};
				
				&New_Csv_Formation(@ExcelRef,@SecExcelRef,$Storefile,$with_header,@Headers,\@outfile_Name_Values,$FileServerpath);
			}
			elsif($Report_Id =~ m/^(154)$/is)
			{
				# my $Storefile1=$Storefile;
				$Storefile=~s/\.csv/\.txt/isg;
				&Csv_Formation(@ExcelRef,$Storefile,$with_header,@Headers,$Report_Id);
				$count_154=$count_154+1;
				
			}
			else
			{
				&Csv_Formation(@ExcelRef,$Storefile,$with_header,@Headers,$Report_Id);
				if($Report_Id =~ m/^(55)$/is)
				{
					$count_55=$count_55+1;
				}
			}
		}
		elsif($fileType=~m/zip\s*$/is)
		{
			&Zip_Formation($Doc_Name,$FileServerpath);
		}
		elsif($fileType=~m/xml\s*$/is)
		{
			# &Xml_Formation(@ExcelRef,$Storefile); 
			&New_Xml_Formation(@ExcelRef,$Storefile,$outfile_Name);
		}
		elsif($fileType=~m/txt\s*$/is)
		{
			if($Report_Id =~ m/^(47)$/is)
			{
				&New_Txt_Formation(@ExcelRef,$Storefile,$overallcount);
			}
			else
			{
				&Txt_Formation(@ExcelRef,$Storefile);
			}
		}
		
		# my $filesize = -s $Storefile;
		# my $size = &scaleIt($filesize);
		
		my ($size,$return_size_kb);
		if($Report_Id=~m/^89$/is)
		{
			my $StoreMultifile1=$Storefile;
			my $StoreMultifile2=$Storefile;
			$StoreMultifile1=~s/^(.*?)\|\|[^\.]*?(\..*$)/$1$2/isg;
			# print $StoreMultifile1;<>;
			$StoreMultifile2=~s/^(.*\/)[^\|]*?\|\|([^\.]*?\..*$)/$1$2/isg;
			# print $StoreMultifile2;<>;
			$size = stat($StoreMultifile1)->size;
			
			# print $size;<>;
			print "FileSizein_bytes  :: $size\n";
			$return_size_kb=sprintf("%.2f", $size / 1024);
			$size = stat($StoreMultifile2)->size;
			# print $size;<>;
			$return_size_kb=$return_size_kb.'||';
			$return_size_kb.=sprintf("%.2f", $size / 1024);
		}
		# elsif($Report_Id!~m/^738$/is)
		else
		{
			$size = stat($Storefile)->size;
			print "FileSizein_bytes  :: $size\n";
			$return_size_kb=sprintf("%.2f", $size / 1024);
		}
		
		my $return_size_kb=sprintf("%.2f", $size / 1024);
		# my $return_size_kb;

		
		print "FileCount :: $overallcount\n";
		print "FileSizein_kb  :: $return_size_kb\n";
		
		
		if($fileType=~m/csv|txt/is)
		{
			open(FILE, "$Storefile");
			
			my @lines = <FILE>;
			close(FILE);

			$Total = @lines;
			$Total=$Total-1;
		}	
		# print "Total::$Total";<>;
		# print "Overall::$overallcount";<>;
		$overallcount=$Total if(($Total ne '')&&($Total>0));
		
		#Skip the Report History Entry for csv files in ZIP formation reports
		next if(($fileType=~m/csv/is)&&($ZipFileCheck eq 1));
		
		
		my $CurrentTime = DateTime->now(time_zone => 'Europe/London');
		$CurrentTime=~s/T/ /gsi;
		
		&SFDC_AUTOMATION_DB_V1::Insert_into_SFDC_ReportRunHistory($dbh,$SPParam_Id,$SFDC_ReportRunHistory,$todaystart,$CurrentTime,$overallcount);
		&SFDC_AUTOMATION_DB_V1::Insert_into_SFDC_ReportHistory($dbh,$SPParam_Id,$SFDC_ReportHistory,$todaystart,$CurrentTime,$overallcount,$return_size_kb);
			
	}
	
	
	
	#Macros For reports 55 and 155
	if($Report_Id =~ m/^(154|55|738)$/is)
	{		
		# print "HI Welcome Macros-->$Report_Id -->$count_54-->$count_155";<>;
		my $excel;
		eval {$excel = Win32::OLE->GetActiveObject('Excel.Application', 'quit')};
		die "Excel not installed" if $@;
		unless (defined $excel) 
		{
			$excel = Win32::OLE->new('Excel.Application', sub {$_[0]->Quit;})
			or die "Oops, cannot start Excel";
		} 
		
		# Switch off warnings and pop-ups, I hope.
		#
		$Win32::OLE::Warn = 0;
		$excel->{DisplayAlerts}=0;   

		# Let me see the spreadsheet.  
		#
		$excel->{Visible}=1;   
		
		$excel->Workbooks->open($macroDirectory.'/PERSONAL.XLSB');
		if(($Report_Id =~ m/^(154)$/is)&&($count_154==2))
		{
			$excel->run( 'PERSONAL.XLSB!QuoteCommaExport_v2' );
			
			my $error = Win32::OLE->LastError();
			print "Error = " . $error . ".\n";
  
			$count_154=0;
			foreach my $fp (glob("$FileServerpath/*.csv")) 
			{
				printf "%s\n", $fp;
	  
				dos_to_unix($fp);
			}
		}
		elsif(($Report_Id =~ m/^(55)$/is)&&($count_55==12))
		{
			$excel->run( 'PERSONAL.XLSB!BEA_Macro_v4' );
			$count_55=0;
		}
		elsif($Report_Id =~ m/^(738)$/is)
		{
			$excel->run( 'PERSONAL.XLSB!Virgin_Media' );
		}
	# $excel->quit;
	}
	&SFDC_AUTOMATION_DB_V1::Insert_into_StatusLog_AutoQA($dbh,$SPParam_Id,$SFDC_statuslog_table,$SFDC_status_AutoQARunning);
	my $enddatestring = time();
	
	my $previous_run = $enddatestring - $datestring;
	my $return_time = &convert_seconds_to_hhmmss($previous_run);
	
	&SFDC_AUTOMATION_DB_V1::Update_into_SFDC_Spparameter($dbh,$SPParam_Id,$SFDC_Spparameter_table,$return_time);
	
	# my $todayend = DateTime->now();
	my $todayend = DateTime->now( time_zone => 'Europe/London' );
	$todayend=~s/T/ /gsi;

	print "End Time   :: $todayend\n";
	print "Report_Name   :: $Report_Name\n";
	&SFDC_AUTOMATION_DB_V1::update_into_SFDC_Reports($dbh,$Report_Name,$SFDC_Reports,$todayend);
	print "\n\n\"$Report_Name\" executed successfully!!!\n";
		
	
	my $sourcefile=$FilePath;
	
	
}
# =cut
my $sourcefile=$FilePath.$CurrentDateFolder;

# $FileSharePath = "'//CH1021SF01/Glenigan_SFDC/SFDC_Exports'"; # dir with space

my $FileServerpaths=$FileSharePath."/$CurrentDateFolder";

unless ( -d $FileServerpaths ) 
{
	$FileServerpaths=~s/\//\\/igs;
	system("mkdir $FileServerpaths");
}

dircopy($sourcefile,$FileServerpaths);

#### Convert Sec into time(7) format ####
sub convert_seconds_to_hhmmss 
{
	my $hourz=int($_[0]/3600);

	my $leftover=$_[0] % 3600;

	my $minz=int($leftover/60);

	my $secz=int($leftover % 60);

	return sprintf ("%02d:%02d:%02d", $hourz,$minz,$secz)
}

### Change bytes to KB,MB,GB ###
sub scaleIt {
    my( $size, $n ) =( shift, 0 );
    ++$n and $size /= 1024 until $size < 1024;
    return sprintf "%.2f %s", 
          $size, ( qw[ bytes KB MB GB ] )[ $n ];
}
 
 
#### Excel_Formation ####
sub Excel_Formation()
{
	my $ExcelRef1			= shift;
	my $workbookRef			= shift;
	my $Multisheet_Flg1		= shift;
	my $write_header_check	= shift;
	my $Titless				= shift;
	my $Sheet_Name1			= shift;
	my $format_temp			= shift;
	my $date_format1		= shift;
	my $date_format2		= shift;
	my $date_format3		= shift;
	my $Report_Id			= shift;
	my $format1				= shift;
	print "Inside the Excel Formation\n";
	
	my @ExcelRef 	= @{$ExcelRef1};
	my @HeaderS  	= @{$Titless};
	my @sheetNames;
	
	if($Sheet_Name1=~m/\:/is)
	{
		@sheetNames	= split ('\:',$Sheet_Name1);
	}
	else
	{
		@sheetNames=$Sheet_Name1;
	}
	
	my $count=0;
	my $row=0;
	my $Sheet_reference;
	# my $worksheetFor122 = $workbookRef->add_worksheet();
	my $worksheet;
	if($Report_Id =~ m/^(122)$/is)
	{
		$worksheet = $workbookRef->add_worksheet($sheetNames[0]);
		$worksheet->write(1, 0, 'Large Only'); 
		$worksheet->write(2, 0, 'Plans Approved not yet on Site'); 
		$worksheet->write(15, 0, 'Plans Approved on Site'); 
		$worksheet->write(0, 1, 'Private & Social'); 
		$worksheet->write(0, 6, 'Private Only'); 
	}
	my $RowCount;
	for (my $i=0;$i<=$#ExcelRef;$i++) #### Fetch_All_Array Reference ####
	{
		my $Data_key = $ExcelRef[$i];
		my $header_key=$HeaderS[$i];
		print "ValuesRefernce :: $Data_key\n";
				
		if ($Multisheet_Flg1==1)
		{
			my $SheetNames = $sheetNames[$count];
			$SheetNames=~s/^TRANSPOSE_//igs;
			$SheetNames = substr($SheetNames, 0, 30);
			my $worksheet = $workbookRef->add_worksheet($SheetNames); ### Add a worksheet ####
			if($Report_Id =~ m/^(662)$/is)
			{
				# $worksheet->merge_range('A1:M2', 'Next 12 months - Moving Report', $format1); 
				$row=3;
			}
			elsif($Report_Id =~ m/^(705)$/is)
			{
				$row=1;
			}
			$RowCount+=&ExcelWrite($Data_key,$workbookRef,$worksheet,$row,$write_header_check,$header_key,$format_temp,$date_format1,$date_format2,$date_format3,$Report_Id);
		}
		else
		{
			
			unless($Report_Id =~ m/^(122)$/is)
			{
				$worksheet = $workbookRef->add_worksheet($sheetNames[0]);
			}
						
			if($Report_Id =~ m/^(662)$/is)
			{
				# $worksheet->merge_range('A1:M2', 'Next 12 months - Moving Report', $format1); 
				$row=3;
			}
			elsif($Report_Id =~ m/^(705)$/is)
			{
				$row=1;
			}
			
			if(($Report_Id =~ m/^(122)$/is)&&($i=~m/2|3/is))
			{
				$row=15;
			}
			elsif(($Report_Id =~ m/^(122)$/is)&&($i=~m/0|1/is))
			{
				$row=1;
			}
			
			
			$RowCount = &ExcelWrite($Data_key,$workbookRef,$worksheet,$row,$write_header_check,$header_key,$format_temp,$date_format1,$date_format2,$date_format3,$Report_Id,$i);
			
		}
		$count++;
	}
	return $RowCount;
	print "Excel Formation End\n";
}

#### Write into Excel ####
sub ExcelWrite()
{
	my $array_Reference 	= shift;
	my $WorkbookReference	= shift;
	my $sheet_Reference		= shift;
	my $Row					= shift;
	my $header_check		= shift;
	my $titless				= shift;
	my $format_temps		= shift;
	my $date_format1		= shift;
	my $date_format2		= shift;
	my $date_format3		= shift;
	my $Report_Id			= shift;
	my $i					= shift;
	
	print "Inside the Excel Write\n";
	my @ExcelRef1	= @{$array_Reference};
	my @HeaderS		= @{$titless};
	
	if ($Report_Id ~~ @AddressFormattingID)
	{
		&AddressFormattingID($array_Reference,$WorkbookReference,$sheet_Reference,$Row,$header_check);
	}
	else
	{
	
		if(($Report_Id=~ m/^(122)$/is)&&($i=~ m/1|3/is))
		{
			my $temp_col=6;
			for( my $h=0; $h<=$#HeaderS; $h++) #### Header values ####
			{
				$sheet_Reference->write($Row, $temp_col, $HeaderS[$h]);
				$temp_col++;
			}
			$Row++;
		}
		elsif(($Report_Id=~ m/^(122)$/is)&&($i=~ m/0|2/is))
		{
			my $temp_col=1;
			for( my $h=0; $h<=$#HeaderS; $h++) #### Header values ####
			{
				$sheet_Reference->write($Row, $temp_col, $HeaderS[$h]);
				$temp_col++;
			}
			$Row++;
		}
		elsif($Report_Id=~ m/^(662)$/is)
		{
			my $format3_head = $WorkbookReference->add_format();
			   $format3_head->set_bold();
			   $format3_head->set_bg_color('#ffad33');
			   $format3_head->set_align('left');
			my $kcr=1;my $kc=1;my $kct=1;my $kce=1;	my $bt=1; my $count;
			for( my $h=0; $h<=$#HeaderS; $h++) #### Header values ####
			{
				if($HeaderS[$h]=~m/^Key Contact Role$/is)
				{	
					$HeaderS[$h]=~s/^Key Contact Role$/Key Contact Role$kcr/is;
					$kcr++;
				}
				elsif($HeaderS[$h]=~m/^key contact$/is)
				{	
					$HeaderS[$h]=~s/^key contact$/key contact$kc/is;
					$kc++;
				}
				elsif($HeaderS[$h]=~m/^Key Contact Telephone$/is)
				{	
					$HeaderS[$h]=~s/^Key Contact Telephone$/Key Contact Telephone$kct/is;
					$kct++;
				}
				elsif($HeaderS[$h]=~m/^Key Contact Email$/is)
				{	
					$HeaderS[$h]=~s/^Key Contact Email$/Key Contact Email$kce/is;
					$kce++;
				}
				elsif($HeaderS[$h]=~m/^BusinessType$/is)
				{	
					$HeaderS[$h]=~s/^BusinessType$/BusinessType$bt/is;
					$bt++;
				}
				$count=$h;
				$sheet_Reference->write(3, $h, $HeaderS[$h], $format3_head);
			}
			
			my $format1 = $WorkbookReference->add_format(); # Add a format
		    $format1->set_bold();
		    $format1->set_bg_color('#b3b3b3');
		    $format1->set_align('center');
			
			$sheet_Reference->merge_range(0,0,1,$count, 'Next 12 months - Moving Report', $format1);
			
			
			$Row++;
		}		
		elsif($Report_Id=~m/^(705)$/is)
		{
			# Create a merged format
			my $format = $WorkbookReference->add_format();
			$format->set_bold();
		    $format->set_bg_color('#FFFF00');
		    # $format->set_align('center');
			my $format1 = $WorkbookReference->add_format();
			$format1->set_bold();
		    $format1->set_bg_color('#808080');
		    # $format1->set_align('center');
			my $format2 = $WorkbookReference->add_format();
			$format2->set_bold();
		    $format2->set_bg_color('#008000');
		    # $format2->set_align('center');
			my $format3 = $WorkbookReference->add_format();
			$format3->set_bold();
		    $format3->set_bg_color('#FFCC00');
		    # $format3->set_align('center');
			my $format4 = $WorkbookReference->add_format();
			$format4->set_bold();
		    $format4->set_bg_color('#0066CC');
		    # $format4->set_align('center');
			my $format5 = $WorkbookReference->add_format();
			$format5->set_bold();
		    $format5->set_bg_color('#800080');
		    # $format5->set_align('center');
			my $format6 = $WorkbookReference->add_format();
			$format6->set_bold();
		    $format6->set_bg_color('#00CCFF');
		    # $format6->set_align('center');
			

			$sheet_Reference->merge_range('A1:J1', 'Overview', $format); 
			$sheet_Reference->merge_range('K1:N1', 'Timings', $format1); 
			$sheet_Reference->merge_range('O1:X1', 'Address', $format2); 
			$sheet_Reference->merge_range('Y1:AC1', 'Planning details', $format3); 
			$sheet_Reference->merge_range('AD1:AP1', 'Additional site details', $format4); 
			$sheet_Reference->merge_range('AQ1:AT1', 'Contracting', $format5); 
			$sheet_Reference->merge_range('AU1:CC1', 'Contacts', $format6); 
			$sheet_Reference->merge_range('CD1:CG1', 'Geo-coordinates', $format2); 
			
			
			my $format_temp = $WorkbookReference->add_format(); # Add a format
			$format_temp->set_font('Calibri');
			$format_temp->set_bold();
			$format_temp->set_size('11');
			
			# print "Row<==>$Row\n"; <STDIN>;
			for( my $h=0; $h<=$#HeaderS; $h++) #### Header values ####
			{
				# print "$h<==>$HeaderS[$h]\n"; <STDIN>;
				$sheet_Reference->write(1, $h, $HeaderS[$h], $format_temp);
			}
			$Row++;

		}
		elsif ($header_check==1)
		{
			for( my $h=0; $h<=$#HeaderS; $h++) #### Header values ####
			{
				if($Report_Id=~m/^(744|1740|1748|1755|1761|1763|1768|1766|1773|1777|1802|1803|1809|1851)$/is)
				{
					my $format = $WorkbookReference->add_format(
					bold => 1,
					size => 11,
					font  => 'Calibri',
					); # Add a format
					
					if($HeaderS[$h]=~m/^(Story)$/is)
					{			
						$sheet_Reference->set_column($h, $h, 80);	
						$sheet_Reference->write(0, $h, $HeaderS[$h], $format);
					}
					elsif($HeaderS[$h]=~m/^(ROLE_NAME|Business\s*Type|Type|Contact_Surname|Contact_Email|Contact_Address|Contact_Company|Contact_Job_Title|Contact_Christian_Name|Contact_Telephone)$/is)
					{				
						$sheet_Reference->set_column($h, $h, 40);	
						$sheet_Reference->write(0, $h, $HeaderS[$h], $format);
					}
					elsif($HeaderS[$h]=~m/^(Company)$/is)
					{			
						$sheet_Reference->set_column($h, $h, 30);	
						$sheet_Reference->write(0, $h, $HeaderS[$h], $format);
					}
					elsif($HeaderS[$h]=~m/^(Floorspace\s*Sqm|Metropolis\s*ID|Contact_Title|Occupier|Lease\s*Expiry|City)$/is)
					{				
						$sheet_Reference->set_column($h, $h, 15);	
						$sheet_Reference->write(0, $h, $HeaderS[$h], $format);
					}
					else
					{					
						# my $len = length($HeaderS[$h]);		
						# $sheet_Reference->set_column($h, $h, $len);	
						$sheet_Reference->set_column($h, $h, 10);	
						$sheet_Reference->write(0, $h, $HeaderS[$h], $format);
					}
					
				}
				else
				{
					$sheet_Reference->write(0, $h, $HeaderS[$h], $format_temps);
				}
			}
			$Row++;
		}

		foreach my $key1 (@ExcelRef1) #### Row Reference
		{
			my @ExcelRef2 = @{$key1};
			# print $key1;
			# print "i<==>$i\n";<>;

			my $col = 0;
			if(($Report_Id=~ m/^(122)$/is)&&($i=~ m/1|3/is))
			{
				$col = 6;
			}
			elsif(($Report_Id=~ m/^(122)$/is)&&($i=~ m/0|2/is))
			{
				$col = 1;
			}
			foreach my $key2 (@ExcelRef2) #### Coloumn Reference
			{
				$key2 = &Clean($key2);
				# print $key2;<>;
				# print "Row<==>$Row\n";
				# print "col<==>$col\n";<>;
				
				my $length = length($key2);
							
				if(($length >= 255) && ($key2=~m/^http/is))
				{
					$sheet_Reference->write_string($Row, $col, $key2, $format_temps);
				}
				
				
				if(($Report_Id=~m/^(710)$/is) && ($key2=~m/HYPERLINK\(/is))
				{					
					my $link_format = $WorkbookReference->add_format(
						color      => 'blue',
						underline  => 1,
					);
					
					$key2=~s/HYPERLINK\(//igs;
					
					my $valueTemp = $1 if ($key2=~m/\)\,([^>]*?)\)/is);
					
					$key2=~s/\)\,([^>]*?)\)$/)/igs;
					
					my $CONCATENATE="https://www.my.glenigan.com/#/project/$valueTemp/summary";
					# $sheet_Reference->write_url($Row, $col, $CONCATENATE, $link_format, $valueTemp);
					$sheet_Reference->write_url($Row, $col, $CONCATENATE, $valueTemp, $link_format);
					
				}
				elsif ($key2=~m/HYPERLINK\(/is)
				{
					$key2=~s/HYPERLINK\(//igs;
					
					my $valueTemp = $1 if ($key2=~m/\)\,([^>]*?)\)/is);
					
					$key2=~s/\)\,([^>]*?)\)$/)/igs;
					
					my $CONCATENATE="https://www.my.glenigan.com/#/project/$valueTemp/summary";
					$sheet_Reference->write_url($Row, $col, $CONCATENATE, $valueTemp);
				}
				elsif($key2=~m/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)([\d]{2,4})\s*$/is) ######## 18 Feb 2017 ########
				{
					my $day = $1;
					my $month = uc($2);
					my $keyyear = $3;
					
					if($day ne "")
					{
						my $monthNnumber  = $MonthHash{$month};
						my $CurrentYear_TwoDigit = substr($CurrentYear, 0, 2);
						my $year;
						if (length($keyyear)==2)
						{
							$year = $CurrentYear_TwoDigit.$keyyear;
						}
						else
						{
							$year = $keyyear;
						}
						my $dt = $year.'-'.$monthNnumber.'-'.$day;
						# print "BeforeFormat :: $dt\n";
						my $formattedDate = DateTime::Format::DateParse->parse_datetime( $dt );
						# print "FormattedDate :: $formattedDate\n";
						
						if($Report_Id=~m/^(744|1740|1748|1755|1761|1763|1768|1766|1773|1777|1802|1803|1809|1851)$/is)
						{
							
							my $date_format1 = $WorkbookReference->add_format(num_format => 'dd-mmm-yy'); # Add a format for Date							
							$date_format1->set_font('Calibri');
							$date_format1->set_size('11');					
							$date_format1->set_text_wrap();
							$date_format1->set_align('left');	
							$date_format1->set_align('top');				
							
							$sheet_Reference->write_date_time($Row, $col, $formattedDate, $date_format1);
						}
						else
						{					
							$sheet_Reference->write_date_time($Row, $col, $formattedDate, $date_format1);
						}
					}
					else
					{
						my %MonthHashs = ('JAN'=> '01','FEB'=> '02','MAR'=>'03','APR'=>'04','MAY'=>'05','JUN'=>'06','JUL'=>'07','AUG'=>'08','SEP'=>'09','OCT'=>'10','NOV'=>'11','DEC'=>'12');
						
						my $months = $MonthHashs{$month};
						
						my $date = $keyyear.'-'.$months.'-01T00:00:00';
						
						if($Report_Id=~m/^(744|1740|1748|1755|1761|1763|1768|1766|1773|1777|1802|1803|1809|1851)$/is)
						{
							my $date_format3 =  $WorkbookReference->add_format(num_format => 'mmm-yy', align => 'left');
							$date_format3->set_font('Calibri');
							$date_format3->set_size('11');		
							$date_format3->set_text_wrap();
							$date_format3->set_align('left');	
							$date_format3->set_align('top');				
							
							$sheet_Reference->write_date_time($Row, $col, $date, $date_format3);
						}
						else
						{					
							$sheet_Reference->write_date_time($Row, $col, $date, $date_format3);
						}
						
						
					}
				}
				elsif($key2=~m/^\s*([\d]{0,2})\/([\d]{0,2})\/([\d]{0,4})\s*$/is) ######## 10/09/2015 ########
				{
					# print "$col==>$key2\n";
					# print "$Report_Id==>$col\n";
					my $dt = $3.'-'.$2.'-'.$1;
					# $dt=~s/^\s*0(\d{2})/$1/isg;
					my $Year = $3;
					my $Month = $2;
					my $Day = $1;
					if($Report_Id eq "628")
					{						
						if($col eq "28")
						{							
							print "$Report_Id==>$col==>$key2\n";
							$sheet_Reference->write($Row, $col, $key2, $format_temps);
						}
						else
						{
							if ($Month<=12)
							{
								print "HI Welcome\n";
								my $formattedDate = DateTime::Format::DateParse->parse_datetime( $dt );
								$sheet_Reference->write_date_time($Row, $col, $formattedDate, $date_format1);
							}
							else
							{
								$sheet_Reference->write($Row, $col, $key2, $format_temps);
							}
						}
					}					
					elsif(($Report_Id =~ m/^(1741)$/is) && ($col=~m/^35$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}						
					elsif(($Report_Id =~ m/^(604)$/is) && ($col=~m/^14$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}					
					elsif(($Report_Id =~ m/^(707)$/is) && ($col=~m/^(12|17)$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}					
					elsif(($Report_Id =~ m/^(79)$/is) && ($col=~m/^6$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}					
					elsif(($Report_Id =~ m/^(738)$/is) && ($col=~m/^33$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}					
					elsif(($Report_Id =~ m/^(76)$/is) && ($col=~m/^8$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}						
					elsif(($Report_Id =~ m/^(717)$/is) && ($col=~m/^13$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}						
					elsif(($Report_Id =~ m/^(745)$/is) && ($col=~m/^13$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}
					elsif(($Report_Id =~ m/^(1772)$/is) && ($col=~m/^34$/is))
					{
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}
					else
					{
						# if ($Month<=12)
						# if (($Month<=12) && ($Day<=31) && ($Year>=1900))
						if (($Month<=12) && ($Day<=31))
						{
							my $formattedDate = DateTime::Format::DateParse->parse_datetime( $dt );
							if ($Report_Id==20)
							{
								$sheet_Reference->write_date_time($Row, $col, $formattedDate, $date_format2);
							}
							elsif ($Report_Id=~m/^(744|1740|1748|1755|1761|1763|1768|1766|1773|1777|1802|1803|1809|1851)$/is)
							{						
								my $date_format1 = $WorkbookReference->add_format(num_format => 'dd-mmm-yy'); # Add a format for Date							
								$date_format1->set_font('Calibri');
								$date_format1->set_size('11');					
								$date_format1->set_text_wrap();
								$date_format1->set_align('left');	
								$date_format1->set_align('top');		
								
								$sheet_Reference->write_date_time($Row, $col, $formattedDate, $date_format1);
							}							
							else
							{
								# print "$Report_Id==>$col==>$key2\n";
								$sheet_Reference->write_date_time($Row, $col, $formattedDate, $date_format1);
							}
						}
						else
						{	
							if ($Report_Id=~m/^(744|1740|1748|1755|1761|1763|1768|1766|1773|1777|1802|1803|1809|1851)$/is)
							{						
								my $format = $WorkbookReference->add_format(); # Add a format
								$format->set_font('Calibri');
								$format->set_size('11');						
								$format->set_text_wrap();
								$format->set_align('left');	
								$format->set_align('top');		
								
								$sheet_Reference->write($Row, $col, $key2, $format);
							}	
							else
							{
								$sheet_Reference->write($Row, $col, $key2, $format_temps);
							}
						}
					}
				}
				elsif(($Report_Id=~m/^1769$/is) && ($col=~m/^109$/is)) ######## 10/09/2015 ########
				{
					$sheet_Reference->write_string($Row, $col, $key2, $format_temps);
				}
				elsif($key2=~m/^\s*\=\s*$/is) ######## Pari ########
				{
					$sheet_Reference->write_string($Row, $col, $key2, $format_temps);
				}
				elsif($key2=~m/^\s*\=\s*[^\n]*?$/is) ######## Pari ########
				{
					$sheet_Reference->write_string($Row, $col, $key2, $format_temps);
				}
				else
				{		
					if($Report_Id=~m/^(744|1740|1748|1755|1761|1763|1768|1766|1773|1777|1802|1803|1809|1851)$/is)
					{
						my $format = $WorkbookReference->add_format(); # Add a format
						$format->set_font('Calibri');
						$format->set_size('11');						
						$format->set_text_wrap();
						$format->set_align('left');	
						$format->set_align('top');	
						
						$sheet_Reference->write($Row, $col, $key2, $format);
					}
					else
					{					
						$sheet_Reference->write($Row, $col, $key2, $format_temps);
					}
				}
				$col++;
			}
			$Row++;
		}
	}
	$Row=$Row-1;
	return $Row;
}

##### Customizing the address format #####
sub AddressFormattingID()
{
	my $array_Reference1	= shift;
	my $WorkbookReference1	= shift;
	my $sheet_Reference1	= shift;
	my $Row1				= shift;
	my $header_check1		= shift;
	
	print "Inside the AddressFormatting\n";
	
	my @ExcelRef2	= @{$array_Reference1};
	
	my @Headerss = ('CUSTOMER PROJECT ID','Heading','Site Address','APPLICATION_NO','APPLICATION TYPE','APPLICATION DATE','DECISION DATE','DECISION TYPE','PROJECT SIZE','Salutation','First Name','Last Name','Applicant Address','Applicant Tel','Agent','Agent Address','Agent Tel','Description');
	

	my $format = $WorkbookReference1->add_format();
	$format->set_bold();
	$format->set_color('Black');
	$format->set_align('Left');
		
	if ($header_check1==1)
	{
		for( my $h=0; $h<=$#Headerss; $h++) #### Header values ####
		{
			$sheet_Reference1->write(0, $h, $Headerss[$h], $format);
		}
		$Row1++;
	}
	
	my $dataformat = $WorkbookReference1->add_format();
	$dataformat->set_color('Black');
	$dataformat->set_align('Left');
	
	my $format_wrap = $WorkbookReference1->add_format();
	$format_wrap->set_text_wrap();
	$format_wrap->set_color('Black');
	$format_wrap->set_align('Left');
	$format_wrap->set_align('top');
	
	foreach my $key1 (@ExcelRef2) #### Row Reference
	{
		my @fieldsarray = @{$key1};
		
		if($fieldsarray[10]=~m/^\s*[\d]{1,2}\s+[A-Za-z]{3}\s+[\d]{0,2}[\d]{0,2}\s*$/is)
		{
			$fieldsarray[10]=~s/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)[\d]{0,2}([\d]{0,2})\s*$/$1-$2-$3/igs;
		}
		elsif($fieldsarray[10]=~m/^\s*[\d]{1,2}\-[A-Za-z]{3}\-[\d]{0,2}[\d]{0,2}\s*$/is)
		{
			$fieldsarray[10]=~s/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)[\d]{0,2}([\d]{0,2})\s*$/$1-$2-$3/igs;
		}
		elsif($fieldsarray[10]=~m/^\s*[\d]{1,2}\s*[A-Za-z]{3}\s*[\d]{0,2}[\d]{0,2}\s*$/is)
		{
			$fieldsarray[10]=~s/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)[\d]{0,2}([\d]{0,2})\s*$/$1-$2-$3/igs;
		}
		
		if($fieldsarray[11]=~m/^\s*[\d]{1,2}\s+[A-Za-z]{3}\s+[\d]{0,2}[\d]{0,2}\s*$/is)
		{
			$fieldsarray[11]=~s/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)[\d]{0,2}([\d]{0,2})\s*$/$1-$2-$3/igs;
		}
		elsif($fieldsarray[11]=~m/^\s*[\d]{1,2}\-[A-Za-z]{3}\-[\d]{0,2}[\d]{0,2}\s*$/is)
		{
			$fieldsarray[11]=~s/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)[\d]{0,2}([\d]{0,2})\s*$/$1-$2-$3/igs;
		}
		elsif($fieldsarray[11]=~m/^\s*[\d]{1,2}\s*[A-Za-z]{3}\s*[\d]{0,2}[\d]{0,2}\s*$/is)
		{
			$fieldsarray[11]=~s/^\s*([\d]{0,2})(?:\s*|\-)([A-Za-z]{3})(?:\s*|\-)[\d]{0,2}([\d]{0,2})\s*$/$1-$2-$3/igs;
		}
		
		if (($fieldsarray[31]!~m/^\s*0/is)&&($fieldsarray[31]!~m/^\s*[A-za-z]/is))
		{
			$fieldsarray[31]='0'.$fieldsarray[31];
		}
		$sheet_Reference1->set_row($Row1, 16);
		$sheet_Reference1->write($Row1, 0, $fieldsarray[0] ,$dataformat);
		$sheet_Reference1->write($Row1, 1, $fieldsarray[1] ,$dataformat);
		$sheet_Reference1->write($Row1, 2, $fieldsarray[2] ,$dataformat);
		$sheet_Reference1->write($Row1, 3, $fieldsarray[8] ,$dataformat);
		$sheet_Reference1->write($Row1, 4, $fieldsarray[9] ,$dataformat);
		$sheet_Reference1->write_date_time($Row1, 5, $fieldsarray[10] ,$dataformat);
		$sheet_Reference1->write_date_time($Row1, 6, $fieldsarray[11] ,$dataformat);
		$sheet_Reference1->write($Row1, 7, $fieldsarray[12] ,$dataformat);
		$sheet_Reference1->write($Row1, 8, $fieldsarray[13] ,$dataformat);
		$sheet_Reference1->write($Row1, 9, $fieldsarray[14] ,$dataformat);
		$sheet_Reference1->write($Row1, 10, $fieldsarray[15] ,$dataformat);
		$sheet_Reference1->write($Row1, 11, $fieldsarray[16] ,$dataformat);
		$sheet_Reference1->write($Row1, 12, $fieldsarray[17] ,$dataformat);
		$sheet_Reference1->write($Row1, 13, $fieldsarray[23] ,$dataformat);
		$sheet_Reference1->write($Row1, 14, $fieldsarray[24] ,$dataformat);
		$sheet_Reference1->write($Row1, 15, $fieldsarray[25] ,$dataformat);
		$sheet_Reference1->write_string($Row1, 16, $fieldsarray[31] ,$dataformat); # Pari
		$sheet_Reference1->write($Row1, 17, $fieldsarray[32] ,$format_wrap);
		
		$Row1++;
		$sheet_Reference1->write($Row1, 2, $fieldsarray[3] ,$dataformat);
		$sheet_Reference1->write($Row1, 12, $fieldsarray[18] ,$dataformat);
		$sheet_Reference1->write($Row1, 15, $fieldsarray[26] ,$dataformat);
		$Row1++;
		$sheet_Reference1->write($Row1, 2, $fieldsarray[4] ,$dataformat);
		$sheet_Reference1->write($Row1, 12, $fieldsarray[19] ,$dataformat);
		$sheet_Reference1->write($Row1, 15, $fieldsarray[27] ,$dataformat);
		$Row1++;
		$sheet_Reference1->write($Row1, 2, $fieldsarray[5] ,$dataformat);
		$sheet_Reference1->write($Row1, 12, $fieldsarray[20] ,$dataformat);
		$sheet_Reference1->write($Row1, 15, $fieldsarray[28] ,$dataformat);
		$Row1++;
		$sheet_Reference1->write($Row1, 2, $fieldsarray[6] ,$dataformat);
		$sheet_Reference1->write($Row1, 12, $fieldsarray[21] ,$dataformat);
		$sheet_Reference1->write($Row1, 15, $fieldsarray[29] ,$dataformat);
		$Row1++;
		$sheet_Reference1->write($Row1, 2, $fieldsarray[7] ,$dataformat);
		$sheet_Reference1->write($Row1, 12, $fieldsarray[22] ,$dataformat);
		$sheet_Reference1->write($Row1, 15, $fieldsarray[30] ,$dataformat);
		$Row1++;
		$Row1++;
	}	
}

##### Clean Process #####
sub Clean()
{
	my $input  = shift;
	
	$input =~s/^\s*|\s*$//igs;
	$input =~s/\s\s+/ /igs;
	$input =~s/\s+/ /igs;
	eval{$input = decode('cp1256',$input);};
	
	return ($input);
}

##### Xml_Formation #####
sub Xml_Formation()
{
	my $row			= shift;
	my $filepath	= shift;

	my @RowReference = @{$row};

	foreach my $rowRef (@RowReference)
	{
		my @RowReference1 = @{$rowRef};
		foreach my $rowRef1 (@RowReference1)
		{
			open (RE,">>$filepath");
			print RE "$rowRef1";
			close RE;
		}
	}
}

##### New_Xml_Formation #####
sub New_Xml_Formation()
{
	my $row			= shift;
	my $filepath	= shift;
	my $outfile_Name	= shift;

	my @RowReference = @{$row};

	$outfile_Name=~s/\-/ /isg;
	foreach my $rowRef (@RowReference)
	{
		my @RowReference1 = @{$rowRef};
		foreach my $rowRef1 (@RowReference1)
		{
			my $formatrows = $rowRef1;
			$formatrows=~s/<Projects>/<\?xml version=\"1\.0\" encoding=\"UTF\-8\"\?><Projects xmlns:xsi=\"http\:\/\/www\.w3\.org\/2001\/XMLSchema\-instance\" xsi:noNamespaceSchemaLocation=\"$outfile_Name\.xsd\">/si;
			
			$formatrows=~s/Æ/AE/gs;
			$formatrows=~s/æ/ae/gs;
			$formatrows=~s/Œ/OE/gs;
			$formatrows=~s/œ/oe/gs;
			$formatrows=~s/Ç/C/gs;
			$formatrows=~s/ç/c/gs;
			$formatrows=~s/Ñ/N/gs;
			$formatrows=~s/ñ/n/gs;
			$formatrows=~s/ð/eth/gs;
			$formatrows=~s/Š/S/gs;
			$formatrows=~s/š/s/gs;
			$formatrows=~s/Ð/ETH/gs;
			$formatrows=~s/Þ/THORN/gs;
			$formatrows=~s/þ/thorn/gs;
			$formatrows=~s/ß/sharp s - ess-zed/gs;
			$formatrows=~s/(À|Á|Â|Ã|Ä|Å)/A/gs;
			$formatrows=~s/(à|á|â|ã|ä|å)/a/gs;
			$formatrows=~s/(Ò|Ó|Ô|Õ|Ö|Ø)/O/gs;
			$formatrows=~s/(ò|ó|ô|õ|ö|ø)/o/gs;
			$formatrows=~s/(ù|ú|û|ü)/u/gs;
			$formatrows=~s/(Ù|Ú|Û|Ü)/U/gs;
			$formatrows=~s/(È|É|Ê|Ë)/E/gs;
			$formatrows=~s/(è|é|ê|ë)/e/gs;
			$formatrows=~s/(Ì|Í|Î|Ï)/I/gs;
			$formatrows=~s/(ì|í|î|ï)/i/gs;
			$formatrows=~s/(Ý|Ÿ)/Y/gs;
			$formatrows=~s/(ý|ÿ)/y/gs;
			
			open(my $RE, '>>:encoding(UTF-8)', $filepath) or die $!;
			print $RE "$formatrows";
			close $RE;
		}
	}
	
	open(INFILE, "+<$filepath");	
	open(OUTFILE, "$filepath");
	
	undef $/;
	while(<OUTFILE>)
	{
		findxmlall();
		print INFILE $_;
	}
	$/ = "\n";
	
	close OUTFILE;
	close INFILE;
	
}

sub findxmlall
{	
	$_ =~ s/></>\n</gsi;
	$_=~s/<Project>/  <Project>/gsi;
	$_=~s/<\/Project>/  <\/Project>/gsi;
	$_=~s/<(DataProvider|ProjectId|ProjectHeading|SiteAddress1|SiteAddress2|SiteAddress3|SiteTown|SiteCounty|SitePostcode|Region|EstimatedValue|Contractstage|Planningstage|Startdate|Enddate|Units|LastResearchedDate|Latestproduction|ProjectDescription)>/\t<$1>/gsi;
			
	$_=~s{<Category>\s*((?:(?!<\/Category>).)*?)\s*<\/Category>}
	{
		my $Category = $&;
		$Category=~s/\t//gsi;
		$Category=~s/</\t  </gsi;
		$Category=~s/\t  <\//<\//gsi;
		$Category;
	}gsie;
	
	$_=~s{<Role>((?:(?!<\/Role>).)*?)<\/Role>}
	{
		my $Role = $&;
		$Role=~s/\t//gsi;
		$Role=~s/</\t  </gsi;
		$Role=~s/\t  <\//<\//gsi;
		$Role;
	}gsie;
	
	$_=~s/(<(.+?)>)\n+(<\/\2>)/$1$3/gsi;
	$_=~s/\t\s+<(Category|Role)>/<$1>/gsi;
	$_=~s/<(Category|Role)>/\t<$1>/gsi;
	$_=~s/<\/(Category|Role)>/\t<\/$1>/gsi;	
}

##### Txt_Formation #####
sub Txt_Formation()
{
	my $row			= shift;
	my $filepath	= shift;

	my @RowReference = @{$row};

	foreach my $rowRef (@RowReference)
	{
		my @RowReference1 = @{$rowRef};
		
		
		for(my $i = 0; $i<=$#RowReference1;$i++)
		{
			my ($Promoter,$address,$Postcode,$Phone,$Heading);
			if($i==0)
			{
				$Promoter = "Promoter: ".$RowReference1[$i];
			}
			elsif($i==1)
			{
				$address = "Address : ".$RowReference1[$i];
			}
			elsif($i==2)
			{
				$Postcode = "Postcode: ".$RowReference1[$i];
			}
			elsif($i==3)
			{
				$Phone = "Phone   : ".$RowReference1[$i];
			}
			elsif($i==4)
			{
				$Heading = "Heading : ".$RowReference1[$i]."\n";
			}
			
			open (RE,">>$filepath");
			print RE "$Promoter$address$Postcode$Phone$Heading\n";
			close RE;
		}
	}
}

##### New_Txt_Formation #####
sub New_Txt_Formation()
{
	my $row			= shift;
	my $filepath	= shift;
	my $overallcount= shift;

	if($overallcount == '0')
	{
		open (RE,">>$filepath");
		print RE;
		close RE;
	}
	else
	{
		my @RowReference = @{$row};

		foreach my $rowRef (@RowReference)
		{
			my @RowReference1 = @{$rowRef};
			for(my $i = 0; $i<=$#RowReference1;$i++)
			{
				my $formatnewline="";
				if(($filepath=~ m/\/mc_[^<]+\.txt$/is) && ($i==9))
				{
					$formatnewline = "\"".$RowReference1[$i]."\"\n";
				}
				elsif(($filepath=~ m/\/pa_[^<]+\.txt$/is) && ($i==8))
				{
					$formatnewline = "\"".$RowReference1[$i]."\"\n";
				}
				elsif(($filepath=~ m/\/pl_[^<]+\.txt$/is) && ($i==8))
				{
					$formatnewline = "\"".$RowReference1[$i]."\"\n";
				}
				elsif(($filepath=~ m/\/tn_[^<]+\.txt$/is) && ($i==9))
				{
					$formatnewline = "\"".$RowReference1[$i]."\"\n";
				}
				else
				{
					$formatnewline = "\"".$RowReference1[$i]."\"\^";
				}
			
				open (RE,">>$filepath");
				print RE $formatnewline;
				close RE;
			}
		}
	}
}

##### Csv_Formation #####
sub Csv_Formation()
{
	my $row			= shift;
	my $filepath	= shift;
	my $with_header1= shift;
	my $titles1		= shift;
	my $Report_Id	= shift;
	
	print "Inside the CSV Formation\n";
	
	my $csv = Text::CSV->new({ binary => 1, eol => $/ }) or die "Cannot use CSV: " . Text::CSV->error_diag();
	open my $fh, ">:encoding(cp1256)", "$filepath" or die "new.csv: $!";
	
	my @RowReference = @{$row};
	
	if ($with_header1==1)
	{
		$csv->print($fh, $titles1);
	}
	
	foreach my $rowRef (@RowReference)
	{
		my @ColumnArray;
		foreach my $columRef(@{$rowRef})
		{
			$columRef = &Clean($columRef);
			# if($Report_Id =~ m/^154$/is)
			# {
				# $columRef=~s/^(.*?)$/\#\#\#$1\#\#\#/gsi;
			# }
			push (@ColumnArray, $columRef);
		}
		$csv->print($fh, \@ColumnArray);
	}
	close $fh or die "$filepath: $!";

}


##### New_Csv_Formation #####
sub New_Csv_Formation
{
	my $row			= shift;
	my $secrow		= shift;
	my $filepath	= shift;
	my $with_header1= shift;
	my $titles1		= shift;
	my $outfile_Name= shift;
	my $File_path	= shift;
	
	

	my @outfile_Name_Values	= @{$outfile_Name};
	
	foreach my $outnames (@outfile_Name_Values)
	{
		my @arraynames = split ('\|\|',$outnames);
		
		
		my $filepath = "$File_path/$arraynames[0].csv";	

		print "First::$filepath\n";
	
		my $csv = Text::CSV->new({ binary => 1, eol => $/ }) or die "Cannot use CSV: " . Text::CSV->error_diag();
		
		open my $fh, ">:encoding(cp1256)", "$filepath" or die "new.csv: $!";
		
		my @RowReference = @{$row};
		
		if ($with_header1==1)
		{
			$csv->print($fh, $titles1);
		}
		
		foreach my $rowRef (@RowReference)
		{
			my @ColumnArray;
			foreach my $columRef(@{$rowRef})
			{
				$columRef = &Clean($columRef);
				$columRef=~s/�/ /gsi;
				$columRef=~s/  / /gsi;
				push (@ColumnArray, $columRef);
			}
			 
			$csv->print($fh, \@ColumnArray);
		}
		close $fh or die "$filepath: $!";
		
		
		
		
		my $filepaths = "$File_path/$arraynames[1].csv";		
		print "Second::$filepaths\n";
	
		my $csv = Text::CSV->new({ binary => 1, eol => $/ }) or die "Cannot use CSV: " . Text::CSV->error_diag();
		
		open my $fh, ">:encoding(cp1256)", "$filepaths" or die "new.csv: $!";
		
		my @secRowReference = @{$secrow};
		if ($with_header1==1)
		{
			$csv->print($fh, $titles1);
		}
		
		foreach my $secrowRef (@secRowReference)
		{
			my @secColumnArray;
			foreach my $seccolumRef(@{$secrowRef})
			{
				$seccolumRef = &Clean($seccolumRef);
				$seccolumRef=~s/�/ /gsi;
				$seccolumRef=~s/  / /gsi;
				push (@secColumnArray, $seccolumRef);
			}
			
			$csv->print($fh, \@secColumnArray);
		}
		
		
		close $fh or die "$filepaths: $!";
	}
}

##### Zip_Formation #####
sub Zip_Formation()
{
	my $docname		= shift;
	my $storepath	= shift;
	
	my $zip = Archive::Zip->new();
    $zip->setChunkSize(65536);
	
	print "Inside the ZIP Formation\n";
	
	my @files = glob( $storepath .'/*' );
	my $tempName = $docname;
	$tempName=~s/\.zip$//is;
	foreach my $tempfilenames (@files)
	{
		if($tempfilenames=~m/$tempName/is)
		{
			# print "docname=>$docname\n";
			# print "tempfilenames=>$tempfilenames\n";
			my $tempfilenames1 =(split ('\/',$tempfilenames))[-1];
			# print "tempfilenames1=>$tempfilenames1\n"; <STDIN>;
			
			eval{
				my $file1 = File::Spec->catfile( $storepath, $tempfilenames1 );
				$zip->addFile( $file1, $tempfilenames1 )->desiredCompressionLevel(6);
				
				my $zipped_file = File::Spec->catfile( $storepath, $docname );
				if ( $zip->writeToFileNamed($zipped_file) != AZ_OK ) 
				{
					print STDERR "Zip Creation error\n";
				}
			};
			if ($@)
			{
				print "Zip Creation error\n";
			}
		}
	}
}

##### Transpose the Data #####
sub Transpose()
{
	my $dataaRef	= shift;
	my $headeref	= shift;
	my $sheetname	= shift;
	my $Report_Ids	= shift;
	
	my @dataaRef	= @{$dataaRef};
	my @headeref	= @{$headeref};
	my @sheetnames	= @{$sheetname};
	
	my (@TransposedData,@TransposedHeader);
	for( my $k=0;$k<=$#dataaRef;$k++)
	{	
		my @multipleKeyRow	= @{$dataaRef[$k]};
		my @HeaderKeyRow	= @{$headeref[$k]};
		
		my %WoW_Hash;
		my $sheetName = $sheetnames[$k];

		my ($RoleIndex);
		if($Report_Ids=~m/^662$/is)
		{
			($RoleIndex) = grep { $HeaderKeyRow[$_]=~m/Key Contact Role/igs } 0..$#HeaderKeyRow;
		}
		elsif($Report_Ids=~m/^1772|744$/is)
		{
			($RoleIndex) = grep { $HeaderKeyRow[$_]=~m/Contact_Job_Title/igs } 0..$#HeaderKeyRow;
		}
		# elsif($Report_Ids=~m/^1757$/is)
		# {
			# ($RoleIndex) = grep { $HeaderKeyRow[$_]=~m/Office Name/igs } 0..$#HeaderKeyRow;
		# }
		else
		{
			($RoleIndex) = grep { $HeaderKeyRow[$_]=~m/ROLE_NAME|ROLE_DESC|ROLE_EXTERNALID|OFFICENAME/igs } 0..$#HeaderKeyRow;
		}
		
		if ($RoleIndex eq '')
		{
			push (@TransposedData, \@multipleKeyRow);
			push (@TransposedHeader, \@HeaderKeyRow);
			next;
		}
		elsif ($sheetName!~m/TRANSPOSE/is)
		{
			push (@TransposedData, \@multipleKeyRow);
			push (@TransposedHeader, \@HeaderKeyRow);
			next;
		}
		
		my @RemainingHeaders;
		my $removable_column;
		if($Report_Ids=~m/^667$/is)
		{
			for (my $ii=0;$ii<=$#HeaderKeyRow;$ii++)
			{
				if($HeaderKeyRow[$ii]=~m/^\s*(ROLE_NAME)\s*$/si)
				{
					# print "$ii-->$HeaderKeyRow[$ii]\n";<>;
					$removable_column.=$ii.'|';
					next;
				}
				elsif(($HeaderKeyRow[$ii]=~m/^\s*(PROJECTID)\s*$/si)&&($ii<10))
				{
					# print "Hi-->$ii-->$HeaderKeyRow[$ii]\n";<>;
					$removable_column.=$ii.'|';
					next;
				}
				else
				{
					push (@RemainingHeaders, $HeaderKeyRow[$ii]);
				}
			}
				
		}
		elsif($Report_Ids=~m/^1772|744$/is)
		{

			for (my $ii=0;$ii<=$#HeaderKeyRow;$ii++)
			{
				if($HeaderKeyRow[$ii]!~m/^\s*(Contact_Job_Title|Contact_Company|Contact_Title|Contact_Christian_Name|Contact_Surname|Contact_Telephone|Contact_Email|Contact_Address)\s*$/si)
				{				
					$removable_column.=$ii.'|';
					next;
				}
				else
				{
					push (@RemainingHeaders, $HeaderKeyRow[$ii]);
				}
			}	
		}
		# elsif($Report_Ids=~m/^1757$/is)
		# {

			# for (my $ii=0;$ii<=$#HeaderKeyRow;$ii++)
			# {
				# if($HeaderKeyRow[$ii]!~m/^\s*(Office Name|Address Line 1|Address Line 2|Address Line 3|Town|County|Postcode|Phone Number|Email|Office ID|Contact ID|Contact Name|Contact Phone|Contact Email)\s*$/si)
				# {				
					# $removable_column.=$ii.'|';
					# next;
				# }
				# else
				# {
					# push (@RemainingHeaders, $HeaderKeyRow[$ii]);
				# }
			# }	
		# }
		elsif($Report_Ids=~m/^662$/is)
		{
			for (my $ii=0;$ii<=$#HeaderKeyRow;$ii++)
			{
				if($HeaderKeyRow[$ii]!~m/^\s*(Key Contact Role|key contact|Key Contact Telephone|Key Contact Email|BusinessType)\s*$/si)
				{				
					$removable_column.=$ii.'|';
					next;
				}
				else
				{
					push (@RemainingHeaders, $HeaderKeyRow[$ii]);
				}
			}	
		}
		else
		{
			for (my $ii=$RoleIndex;$ii<=$#HeaderKeyRow;$ii++)
			{
				if($Report_Ids=~m/^42$/is)
				{
					if($HeaderKeyRow[$ii]=~m/^\s*(ROLE_NAME|OfficeId|ContactId)\s*$/si)
					{
						# $removable_column.=$ii.'|';
						next;
					}
					else
					{
						push (@RemainingHeaders, $HeaderKeyRow[$ii]);
					}
				}
				else
				{
					push (@RemainingHeaders, $HeaderKeyRow[$ii]);
				}
			}
		}	
		$removable_column=~s/\|\s*$//isg;
		my (@TempProjectID,@LoopArray);
		foreach my $multipleKeyRow (@multipleKeyRow)
		{
			my @multipleKeyColumn = @{$multipleKeyRow};
			my $projectID = $multipleKeyColumn[0];
			$projectID = $projectID.$multipleKeyColumn[2] if($Report_Ids=~m/^667$/is);
			print "projectID :: $projectID\n";
			
			my @values;
			if ($projectID ~~ @TempProjectID)
			{
				for (my $Column=$RoleIndex; $Column<= $#multipleKeyColumn; $Column++)
				{
					if($Report_Ids=~m/^42$/is)
					{
						if($Column =~ m/^(37|51|52)$/is)
						{
							next;
						}
						else
						{
							my $value = $multipleKeyColumn[$Column];
							push (@values, $value);
						}
					}
					elsif($Report_Ids=~m/^(662|1772|744)$/is)
					{
						if($Column =~ m/^($removable_column)$/is)
						{
							next;
						}
						else
						{
							my $value = $multipleKeyColumn[$Column];
							push (@values, $value);
						}
					}
					elsif($Report_Ids=~m/^667$/is)
					{
						if($Column =~ m/^($removable_column)$/is)
						{
							next;
						}
						else
						{
							my $value = $multipleKeyColumn[$Column];
							push (@values, $value);
						}
					}
					else
					{
						my $value = $multipleKeyColumn[$Column];
						push (@values, $value);
					}
				}
				push @{$WoW_Hash{$projectID}},[@values];
			}
			else
			{
				for (my $Column=0; $Column<= $#multipleKeyColumn; $Column++)
				{
					if($Report_Ids=~m/^42$/is)
					{
						if($Column =~ m/^(37)$/is)
						{
							next;
						}
						else
						{
							my $value = $multipleKeyColumn[$Column];
							push (@values, $value);
						}
					}
					elsif($Report_Ids=~m/^667$/is)
					{
						if($Column =~ m/^($removable_column)$/is)
						{
							next;
						}
						else
						{
							my $value = $multipleKeyColumn[$Column];
							push (@values, $value);
						}
					}
					else
					{
						my $value = $multipleKeyColumn[$Column];
						push (@values, $value);						
					}
				}
				push @{$WoW_Hash{$projectID}},[@values];
			}
			push (@TempProjectID,$projectID);		
		}
		
		my $row=0;
		my $count = 0;
		my (@WoW_Array);
		foreach my $key (sort keys %WoW_Hash)
		{
			my @DataValues = @{$WoW_Hash{$key}};
			
			my $count_arrayRef = scalar @DataValues;
			$count = $count_arrayRef if ($count_arrayRef  > $count);
			
			my @concatenateArray;
			foreach my $data (@DataValues)
			{
				my @DataValues11 = @{$data};
				foreach my $data1 (@DataValues11)
				{
					push (@concatenateArray, $data1);
				}
			}
			push (@{$WoW_Array[$row]}, @concatenateArray);
			$row++;
		}
		
		my $i=1;
		while ($i<$count)
		{
			push (@HeaderKeyRow,@RemainingHeaders);
			$i++;
		}

		push (@TransposedData, \@WoW_Array);	
		if($Report_Ids=~m/^42$/is)
		{
			for ( my $index = $#HeaderKeyRow; $index >= 0; --$index )
			{
				splice @HeaderKeyRow, $index, 1
				if $HeaderKeyRow[$index] =~ /Role_name/is;  # remove certain elements
			}
		}
		elsif($Report_Ids=~m/^667$/is)
		{
			for ( my $index = $#HeaderKeyRow; $index >= 0; --$index )
			{
				splice @HeaderKeyRow, $index, 1
				if $HeaderKeyRow[$index] =~ /Role_name/is;  # remove certain elements
			}
			for ( my $index = 10; $index >= 0; --$index )
			{
				splice @HeaderKeyRow, $index, 1
				if $HeaderKeyRow[$index] =~ /PROJECTID/is;  # remove certain elements
			}
		}
		push (@TransposedHeader, [@HeaderKeyRow]);	
	}		
	return (\@TransposedData,\@TransposedHeader);
}

sub dos_to_unix {
    my($file) = @_;
	my $file1=$file;
	$file1=~s/\./1\./is;
	my($dst, $src);
    
    
    open($src, "<$file")
      or die "Can't open  for read due to: $!\n";
	open($dst, ">$file1")
      or die "Can't open  for write due to: $!\n";
    binmode($dst);
    while(<$src>) {
        chomp;
        print $dst "$_\n";
		
			
    }
    close($dst);
	
	
    close($src);
	unlink($file);
	rename $file1, $file;
}