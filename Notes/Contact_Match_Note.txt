----- Hopper Execution ------

Server  = 10.204.202.246

uid     = MeritUser
pwd     = Ez@bfyGAf*JDyq3RxQPj

database= Contacts_Hopper

****** ISTRUCURE Execution ***

.... 194 Ubunto server ....

cd hopper/root/ISTRUCTE

Just execute istructureControl.pl

****** ARB Execution ********

Step:1
______
use Contacts_Hopper
select count(*) from Contacts_ScrapeLog

=> Delete previous month data in "Contacts_ScrapeLog" Table.

delete Contacts_ScrapeLog

83 Server:: C:\Glenigan\hopper\root\ARB

Execute arbControlURL.pl -> trigger (arbListURL.pl) 

Step:2
______

select count(*) from Contacts_ScrapeLog
select * from Contacts_ScrapeLog order by id asc

select * from Contacts_ScrapeLog where convert (date,Created_Date)='2020-03-11' order by id asc

=> Collect ID's in ascending Order and Split each 500 "from" and "to" 

ex: perl arbDetailURL.pl 406184 406683

	(OR)

Consolidate the ID's in Input.txt file... as asecending Order(asc)
Execute the Hopper_Split.pl (It should be Split and Execute next Level)

Step:3
_____

After completed step2.,

Execute the below queries in sql server.

-- drop table contact_detail_unique_urls

with cte as
(
select ID, detail_urls, row_number() over (PARTITION by detail_urls order by detail_urls ) as row_numbers from ContactsScrape_Detail_URLLog
)
select * into contact_detail_unique_urls from cte where row_numbers = 1

select count(*) from ContactsScrape_Detail_URLLog
select count(*) from contact_detail_unique_urls

select * from contact_detail_unique_urls order by ID ASC

(select detail_urls from contact_detail_unique_urls where ID between '$from' and '$to' order by ID ASC****)

Split and Run Details 3000 each

perl arbDetailCollections.pl 367131	369605

	(OR)
	
Execute Hopper_Split2.pl (It should be Split and Execute next Level)

-----------------------------------

NOTES:

1. Hopper(Contact Match) script flow and DB table structure and Queries.
2. In Script, Control.pl file is main trigger script for both Istructe and ARB websites.
3. Here we need to collect all details URLs first and then get the applications details.
4. Istructe is simple and have two scripts(one is control and another once is list perl file)
5. For ARB, we have 4 Perl files and but the concept is to collect the details page URLs first and then collect details from details URLs.
	 * If any failed details page URLs in log file means we need rerun that one also using arbMissingDetails.pl file.
	 * Likewise, for details page failed URLs found means, we need to use ARB_Missed_App.pl file to coved missing details.

Note: We need to drop table(contact_detail_unique_urls) at the stage after collecting lists pages URLs(after arbDetailURL.pl this script completed) and then use the below query for file input table.