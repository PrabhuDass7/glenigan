#-- Create New Folder --
mkdir Foldername

#-- Move File --
mv folder1 folder2

mv dir2/combined.txt dir4/dir5/dir6
ls dir2
ls dir4/dir5/dir6

#-- Combained (COpy File) ---
cp combined.txt backup_combined.txt
ls

cp dir4/dir5/dir6/combined.txt .
ls dir4/dir5/dir6
ls

#-- Remove Files ----
rm -r test3
rm f* (Remove All File start from "f" Char)

#-- Remove Directory ---
rmdir foldername

#-- Read File ---

cat filename

#--- Write File ---
echo test1 >Filename.txt (Write)
echo test2 >>Filename.txt (Append)

#-- List --
ls dir1

#-- Word count in file ---
wc file.txt (Counct fo line and words)
wc -l file.txt (Count of line)

# --- print Whole file strucure --

ls ~

#-- Folder Structure Tree

tree
