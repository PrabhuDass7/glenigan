use strict;
use Time::HiRes qw(time);
use DateTime;
use POSIX qw(strftime);
use Time::Piece;
use Encode;
use File::Path 'rmtree';
use File::Find;
use File::stat;
use Cwd qw(abs_path);
use File::Basename;
use Cwd;
use DateTime::Format::Excel;
use Spreadsheet::XLSX;
use Excel::Writer::XLSX;
use DateTime::Format::Excel;
use Spreadsheet::WriteExcel;
use File::Copy;

my $date = localtime->strftime('%Y%m%d');
my $date2 = localtime->strftime('%d-%m-%Y');

my $dir = getcwd;

my $dt = DateTime->now->set_time_zone( 'Asia/Kolkata' );
my $y=$dt->year;
my $m=$dt->month_abbr;
my $d=$dt->day;
my $Date="$d-$m-$y";

print "dt:: $dt\n";
print "Date:: $Date\n";
# print "localtime:: ". localtime "\n";<>;

my $Year_Location="$dir/$y";
&createdirectory("$Year_Location");

my $Mon_Location="$Year_Location/$m";
&createdirectory("$Mon_Location");

my $CurrenDate_Location="$Mon_Location/$date2";
&createdirectory("$CurrenDate_Location");

my @files = glob("*.xls");
	
my ($workbook,$worksheet,$workbook2,$worksheet2);
$workbook = Excel::Writer::XLSX->new("Daily_Report.xls");
$worksheet = $workbook->add_worksheet("Report");

$workbook2 = Excel::Writer::XLSX->new("GSS_Not_Imported_Reports_$date.xls");

my $format  = $workbook->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
$format->set_bold();
$format->set_align('center');

$worksheet->write(0, 0, "Red In Detail", $format);
$worksheet->write(0, 1, "Planning", $format);
$worksheet->write(0, 2, "Decision", $format);

$worksheet->write(0, 5, "Council_Code", $format);
$worksheet->write(0, 6, "Council_Name", $format);
$worksheet->write(0, 7, "Planning", $format);
$worksheet->write(0, 8, "Decision", $format);
$worksheet->write(0, 9, "Comments", $format);

#---- Not Imoprted _Report Headers ----

$worksheet->write(5, 0, "Red In Detail", $format);
$worksheet->write(5, 1, "Planning", $format);
$worksheet->write(5, 2, "Decision", $format);

my $format = $workbook->add_format( border=>1, color=>'black' );
$format->set_bg_color( 'None' );
$format->set_align('center');

my $format2 = $workbook2->add_format( border=>1, color=>'black' );
$format2->set_bg_color( 'None' );
$format2->set_align('center');

$worksheet->write(6, 0, "Not imported (after dedupe) for last 7 days", $format);
$worksheet->write(7, 0, "Scraped count is equal for 4 consecutive days", $format);

my $rc=0;
my $cc=4;
my @council_Num_dump='7|Dass';
	
foreach my $file (@files)
{
	chomp($file);
	print "FILE_NAME:: $file\n";
	my $Sheet_Name=$1 if($file=~m/^([^>]*?)\.xls/is);
	# print "Sheet_Name:: $Sheet_Name\n";<>;
	if($Sheet_Name!~m/^[^>]*?(?:D|P)$/is)
	{
		print "GotoNXT_BOKK\n";
		goto nxtWrkBk;
	}
	
	my $excel = Spreadsheet::XLSX->new("$dir/$file");
	my $sheet = ${ $excel->{Worksheet} }[0];
	
	# print "SHEET:: $sheet\n";<>;
	
	my ($rw_cnt,$rc_data,$red_count,$not_scrap_count,$DC,$not_impo_cnt,$ScrConsecutive_cnt)=(0,0,0,0,0,0,0);
	my $alrt_cc=22;
	my $worksheet_Data;
	if($Sheet_Name=~m/D/is)
	{
		$DC=1;
		$alrt_cc=$alrt_cc-1;
		# $alrt_cc=$alrt_cc;
		$worksheet_Data = $workbook->add_worksheet("Decision");
		$worksheet2 = $workbook2->add_worksheet("Decision");
	}else{
		$worksheet_Data = $workbook->add_worksheet("Planning");
		$worksheet2 = $workbook2->add_worksheet("Planning");
	}
	
	my ($row_min, $row_max) = $sheet->row_range();
	my ($col_min, $col_max) = $sheet->col_range();
	
	# print "row_max:: $row_max\n";<>;
	
	foreach my $row ($sheet -> {MinRow} .. $sheet -> {MaxRow}+1) 
	{
		$rw_cnt++;
		my $c = 0;
		
		# print "row:: $row\n";
		my $alert=&Data_Cell($sheet,$row,$alrt_cc);
		
		print "alert:: $alert\n";
		
		if($alert eq "alert")
		{
			my $cc_data=0;
			
			my $format2  = $workbook2->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
			$format2->set_bold();
			$format2->set_align('center');
				
			foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol})
			{
				my $data=&Data_Cell($sheet,$row,$col);
				$worksheet2->write($not_impo_cnt, $cc_data, $data, $format2);
				$cc_data++;
			}
		}
		
		my($council_Num,$council_Name,$remark);
		
		if($alert eq "Red")
		{
			$rc_data++;
			
			#------- Red In Details Dump ------
			my $cc_data=0;
			foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol})
			{
				my $data=&Data_Cell($sheet,$row,$col);
				$worksheet_Data->write($rc_data, $cc_data, $data, $format);
				$cc_data++;
			}
		
			$red_count++;
			
			$remark=&Data_Cell($sheet,$row,$alrt_cc+1);
			if($remark eq "Not scraped for Last 7 days")
			{
				$not_scrap_count++;
				$council_Num=&Data_Cell($sheet,$row,0);
				$council_Name=&Data_Cell($sheet,$row,1);

				my ($Same_Council,$rc_temp);
				foreach my $council_Num_dump(@council_Num_dump)
				{
					my @Data = split('\|',$council_Num_dump);
					
					$rc_temp=$Data[0];
					my $council_Num_dumps=$Data[1];
					if($council_Num_dumps == $council_Num)
					{
						$Same_Council="True";
						goto nxt;
					}
				}	
				
				nxt:
				
				my $cc=7;
				$cc=$cc+1 if($Sheet_Name=~m/^[^>]*?D$/is);
								
				if($Same_Council eq "True")
				{
					$worksheet->write($rc_temp, $cc, "X", $format);
				}
				else{
					$rc++;
					push(@council_Num_dump,$rc."|".$council_Num);
					$worksheet->write($rc, 5, decode('utf8',$council_Num), $format);
					$worksheet->write($rc, 6, decode('utf8',$council_Name), $format);
					$worksheet->write($rc, $cc-1, "", $format) if($cc == 8);
					$worksheet->write($rc, $cc, decode('utf8',"X"), $format);
					$worksheet->write($rc, $cc+1, "", $format);
					$worksheet->write($rc, $cc+2, "", $format) if($cc+2 == 9);
				}
			}
						
			if($remark eq "Not imported (after dedupe) for last 7 days")
			{
				$not_impo_cnt++;
				my $cc_data=0;
				foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol})
				{
					my $data=&Data_Cell($sheet,$row,$col);
					$worksheet2->write($not_impo_cnt, $cc_data, $data, $format2);
					$cc_data++;
				}
			}
			$ScrConsecutive_cnt++ if($remark eq "Scraped count is equal for 4 consecutive days");
		}
	}	
	
	$worksheet->write(1, 0, "Not scraped for Last 7 days", $format);
	$worksheet->write(2, 0, "Others", $format);
	$worksheet->write(1, 1+$DC, $not_scrap_count, $format);
	$worksheet->write(2, 1+$DC, $red_count-$not_scrap_count, $format);
	
	#------- Not Imported Count -------
	$worksheet->write(6, 1+$DC, $not_impo_cnt, $format);
	$worksheet->write(7, 1+$DC, $ScrConsecutive_cnt, $format);
	
	nxtWrkBk:
}

$workbook->close;
$workbook2->close;

#------ File Move -------
my @files = glob("*.xls");
foreach my $file(@files)
{
	my $Copy_Status = move ("$dir/$file", "$CurrenDate_Location");
}

print "File Is Moved****\n";

sub Data_Cell()
{
	my $sheet=shift;
	my $row=shift;
	my $col=shift;
	
	my $cell = $sheet -> {Cells} [$row] [$col];
	my $va = $cell -> {Val};
	
	$va=~s/amp;//igs;

	return $va;
}

sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}