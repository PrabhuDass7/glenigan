use strict;
use Time::HiRes qw(time);
use DateTime;
use POSIX qw(strftime);
use Time::Piece;
use Encode;
use File::Path 'rmtree';
use File::Find;
use File::stat;
use Cwd qw(abs_path);
use File::Basename;
use Cwd;
use DateTime::Format::Excel;
use Spreadsheet::XLSX;
use Excel::Writer::XLSX;
use DateTime::Format::Excel;
use Spreadsheet::WriteExcel;
use File::Copy;

my $date = localtime->strftime('%Y%m%d');
my $date2 = localtime->strftime('%d-%m-%Y');

my $dir = getcwd;

my $dt = DateTime->now->set_time_zone( 'Asia/Kolkata' );
my $y=$dt->year;
my $m=$dt->month_abbr;
my $d=$dt->day;
my $Date="$d-$m-$y";

my $qury_date=localtime->strftime('%Y-%m-%d');

print "dt:: $dt\n";
print "Date:: $Date\n";
# print "qury_date:: $qury_date\n";<>;
# print "localtime:: ". localtime "\n";<>;

my $Year_Location="$dir/$y";
&createdirectory("$Year_Location");

my $Mon_Location="$Year_Location/$m";
&createdirectory("$Mon_Location");

my $CurrenDate_Location="$Mon_Location/$date2";
&createdirectory("$CurrenDate_Location");

my @files = glob("*.xls");
	
my ($workbook,$worksheet,$workbook2,$worksheet2,$workbook3,$worksheet3);
$workbook = Excel::Writer::XLSX->new("Daily_Report.xls");
$worksheet = $workbook->add_worksheet("Report");

$workbook2 = Excel::Writer::XLSX->new("OneApp_Field_Fill_Percentage_Report_$date.xls");

my $format  = $workbook->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
$format->set_bold();
$format->set_align('center');

# $worksheet->merge_range('A1:A2',"Remarks", $format);
# $worksheet->merge_range('B1:C1',"Count or Percentage", $format);
$worksheet->write(0,0,"Remarks", $format);
$worksheet->write(0,1,"OneApp", $format);

$worksheet->write(10, 0, "Remarks", $format);
$worksheet->write(10, 1, "OneApp", $format);

my $format = $workbook->add_format( border=>1, color=>'black' );
$format->set_bg_color( 'None' );
$format->set_align('center');

$worksheet->write(1,0,"Number of councils having 100% fill rate across all fields  in today's scrape", $format);
$worksheet->write(2,0,"Number of councils that did not have 100% fill rate across all fields in today's scrape", $format);

$worksheet->write(11, 0, "Number of councils with fields have difference in fill percent between yesterday's and last week percent", $format);
# $worksheet->write(12, 0, 'All fields are "0" (YesterDay)', $format);
$worksheet->write(12, 0, 'No of councils that did not have any deviation across all fields', $format);
# $worksheet->write(13, 0, 'All fields are "0" (Last Week)', $format);
$worksheet->write(13, 0, 'No of councils with negative deviation across all fields', $format);
$worksheet->write(14, 0, "No of councils with 1 to 10% deviation across all fields", $format);
$worksheet->write(15, 0, "No of councils with 11 to 50% deviation across all fields", $format);
$worksheet->write(16, 0, "No of councils with 51 to 60% deviation across all fields", $format);
$worksheet->write(17, 0, "No of councils with 61 to 70% deviation across all fields", $format);
$worksheet->write(18, 0, "No of councils with 71 to 80% deviation across all fields", $format);
$worksheet->write(19, 0, "No of councils with 81 to 90% deviation across all fields", $format);
$worksheet->write(20, 0, "No of councils with 91 to 100% deviation across all fields", $format);

$worksheet->write(21, 0, 'No Of Council with issues in (91 to 100) %', $format);
$worksheet->write(22, 0, 'Councils code with issues in (91 to 100) percentage', $format);
$worksheet->write(23, 0, 'Comments', $format);

# my $format2 = $workbook2->add_format( border=>1, color=>'black' );
my $format2 = $workbook->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
# $format2->set_bg_color( 'None' );
$format2->set_bold();
$format2->set_align('center');

my $less_Zero_format = $workbook->add_format(bg_color => "#FFEE58 ",pattern => 1,border => 1);
$less_Zero_format->set_bold();
$less_Zero_format->set_align('center');
my $one_to_10_format = $workbook->add_format(bg_color => "#EC407A",pattern => 1,border => 1);
$one_to_10_format->set_bold();
$one_to_10_format->set_align('center');
my $Eleven_to_50_format = $workbook->add_format(bg_color => "#CE93D8",pattern => 1,border => 1);
$Eleven_to_50_format->set_bold();
$Eleven_to_50_format->set_align('center');
my $Fifty_to_sixty_format = $workbook->add_format(bg_color => "#C5E1A5",pattern => 1,border => 1);
$Fifty_to_sixty_format->set_bold();
$Fifty_to_sixty_format->set_align('center');
my $sixty_to_seventy_format = $workbook->add_format(bg_color => "#82E0AA",pattern => 1,border => 1);
$sixty_to_seventy_format->set_bold();
$sixty_to_seventy_format->set_align('center');
my $seventy_to_80_format = $workbook->add_format(bg_color => "#E6B0AA",pattern => 1,border => 1);
$seventy_to_80_format->set_bold();
$seventy_to_80_format->set_align('center');
my $Eighty_to_90_Format = $workbook->add_format(bg_color => "#FFFF99",pattern => 1,border => 1);
$Eighty_to_90_Format->set_bold();
$Eighty_to_90_Format->set_align('center');
my $Nighty_to_100_Format = $workbook->add_format(bg_color => "#CCFF99",pattern => 1,border => 1);
$Nighty_to_100_Format->set_bold();
$Nighty_to_100_Format->set_align('center');

my $rc=0;
my $cc=4;
	
foreach my $file (@files)
{
	chomp($file);
	print "FILE_NAME:: $file\n";
	# print "dir:: $dir/$file\n";<>;
	my $Sheet_Name=$1 if($file=~m/^([^>]*?)\.xls/is);
	print "Sheet_Name:: $Sheet_Name\n";
	if($Sheet_Name!~m/(?:one|OneApp|oneapp|One_App|one_app)/is)
	{
		print "GotoNXT_BOKK\n";
		goto nxtWrkBk;
	}
	
	my $worksheet_Data = $workbook->add_worksheet("$Sheet_Name");
	
	my $excel = Spreadsheet::XLSX->new("$dir/$file");
	my $sheet = ${ $excel->{Worksheet} }[0];
	
	#------ Summary Report ------------------------
	my $summary_sheet = ${ $excel->{Worksheet} }[1];
	
	my ($rrc,$worksheet_summary2);
	my $s_row=1;
	if($Sheet_Name=~m/one/is)
	{
		$rrc=11;
		$worksheet2 = $workbook2->add_worksheet("OneApp");
		$worksheet_summary2 = $workbook2->add_worksheet("OneApp-Summary");
	}
	
	my $s_data1=&Data_Cell($summary_sheet,1,1);
	my $s_data2=&Data_Cell($summary_sheet,2,1);
	
	$worksheet->write(1, 1, $s_data1, $format);
	$worksheet->write(2, 1, $s_data2, $format);
	
	my $s_r=0;
	foreach my $row ($summary_sheet -> {MinRow} .. $summary_sheet -> {MaxRow}+1) 
	{
		my $s_col=0;
		foreach my $col ($summary_sheet -> {MinCol} ..  $summary_sheet -> {MaxCol})
		{
			my $data=&Data_Cell($summary_sheet,$row,$col);
			$worksheet_summary2->write_string($s_r, $s_col, $data, $format);
			$s_col++;
		}
		$s_r++;
	}
	
	#-------------------------------------------------

	my ($row_min, $row_max) = $sheet->row_range();
	my ($col_min, $col_max) = $sheet->col_range();
	
	# print "row_max:: $row_max\n";
	# print "col_max:: $col_max\n";
	
	my($fdc,$not_diviate_field_c,$lzc,$OtT,$EtF,$A_50,$A_60,$A_70,$A_80,$A_90,$yfec,$lwfec)=(0,0,0,0,0,0,0,0,0,0,0,0);
	my $rc_data=0;

	foreach my $row ($sheet -> {MinRow} .. $sheet -> {MaxRow}+1) 
	{
		my ($field_Diff_count, $less_zero_count, $one_to_10_count, $eleven_to_50_count, $fifty_count,$sixty_count,$seventy_count,$eighty_count,$nighty_count,$yest_fields_empty_count,$lastWeek_fields_empty_count)=(0,0,0,0,0,0,0,0,0,0,0); 
		my(@head1,@head2,@head3,@head4,@head5,@head6,@head7)="";
		my $cc_data=0;
		foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol})
		{
			my $data=&Data_Cell($sheet,$row,$col);
			# print "row:: $row\n";
			# print "col:: $col\n";<>;
			$data=~s/^\s*$//igs;
			
			# print "data:: $data\n";<>;
			
			goto nxtrow if(($data eq "") && ($col==0));
			
			
			# $worksheet_Data->write($rc_data, $cc_data, $data, $format);
			
			$worksheet2->write_string($rc_data, $cc_data, $data, $format);
			
			if($row > 0)
			{
				if(($col == 4)|| ($col == 7)|| ($col == 10)|| ($col == 13)|| ($col == 16)|| ($col == 19)|| ($col == 22)|| ($col == 25)|| ($col == 28)|| ($col == 31)|| ($col == 34)|| ($col == 37)|| ($col == 40)|| ($col == 43)|| ($col == 46)|| ($col == 49)|| ($col == 52)|| ($col == 55)|| ($col == 58)|| ($col == 61)|| ($col == 64)|| ($col == 67)|| ($col == 70)|| ($col == 73)|| ($col == 76)|| ($col == 79)|| ($col == 82)|| ($col == 85)|| ($col == 88)|| ($col == 91)|| ($col == 94)|| ($col == 97)|| ($col == 100)|| ($col == 103)|| ($col == 106)|| ($col == 109)|| ($col == 112)|| ($col == 115)|| ($col == 118)|| ($col == 121)|| ($col == 124)|| ($col == 127)|| ($col == 130)|| ($col == 133)|| ($col == 136)|| ($col == 139)|| ($col == 142)|| ($col == 145)|| ($col == 148)|| ($col == 151)|| ($col == 154)|| ($col == 157)|| ($col == 160)|| ($col == 163)|| ($col == 166)|| ($col == 169)|| ($col == 172)|| ($col == 175)|| ($col == 178)|| ($col == 181)|| ($col == 184)|| ($col == 187)|| ($col == 190)|| ($col == 193)|| ($col == 196)|| ($col == 199)|| ($col == 202)|| ($col == 205)|| ($col == 208)|| ($col == 211)|| ($col == 214))
				{
					my $temp=0;
					
					my($zero_to_ten_headers,$ten_to_fifty_headers,$fifty_headers,$sixty_headers,$sevety_headers,$eighty_headers,$above_nighty_headers)="";
					
					if($data != 0)
					{
						$worksheet_Data->write($rc_data, $cc_data,$data, $format);
						$field_Diff_count++;
						$temp=1;
					}
					if($data < 0)
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $less_Zero_format);
						$less_zero_count++;
						$temp=1;
					}
					if(($data > 0) && ($data <= 10))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $one_to_10_format);
						$one_to_10_count++;
						$temp=1;
						
						$zero_to_ten_headers=&headers($sheet,$col);
						push(@head1,$zero_to_ten_headers);	
					}
					if(($data > 10) && ($data <= 50))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $Eleven_to_50_format);
						$eleven_to_50_count++;
						$temp=1;
						$ten_to_fifty_headers=&headers($sheet,$col);
						push(@head2,$ten_to_fifty_headers);
					}
					
					if(($data > 50) && ($data <= 60))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $Fifty_to_sixty_format);
						$fifty_count++;
						$temp=1;
						$fifty_headers=&headers($sheet,$col);
						push(@head3,$fifty_headers);
					}
					
					if(($data > 60) && ($data <= 70))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $sixty_to_seventy_format);
						$sixty_count++;
						$temp=1;
						$sixty_headers=&headers($sheet,$col);
						push(@head4,$sixty_headers);
					}
					
					if(($data > 70) && ($data <= 80))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $seventy_to_80_format);
						$seventy_count++;
						$temp=1;
						$sevety_headers=&headers($sheet,$col);
						push(@head5,$sevety_headers);
					}
					if(($data > 80) && ($data <= 90))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $Eighty_to_90_Format);
						$eighty_count++;
						$temp=1;
						$eighty_headers=&headers($sheet,$col);
						push(@head6,$eighty_headers);
					}
					if(($data > 90) && ($data <= 100))
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $Nighty_to_100_Format);
						$nighty_count++;
						$temp=1;
						$above_nighty_headers=&headers($sheet,$col);
						push(@head7,$above_nighty_headers);
					}
					
					if($temp==0)
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $format);
					}
				}
				elsif(($col == 2)|| ($col == 5)|| ($col == 8)|| ($col == 11)|| ($col == 14)|| ($col == 17)|| ($col == 20)|| ($col == 23)|| ($col == 26)|| ($col == 29)|| ($col == 32)|| ($col == 35)|| ($col == 38)|| ($col == 41)|| ($col == 44)|| ($col == 47)|| ($col == 50)|| ($col == 53)|| ($col == 56)|| ($col == 59)|| ($col == 62)|| ($col == 65)|| ($col == 68)|| ($col == 71)|| ($col == 74)|| ($col == 77)|| ($col == 80)|| ($col == 83)|| ($col == 86)|| ($col == 89)|| ($col == 92)|| ($col == 95)|| ($col == 98)|| ($col == 101)|| ($col == 104)|| ($col == 107)|| ($col == 110)|| ($col == 113)|| ($col == 116)|| ($col == 119)|| ($col == 122)|| ($col == 125)|| ($col == 128)|| ($col == 131)|| ($col == 134)|| ($col == 137)|| ($col == 140)|| ($col == 143)|| ($col == 146)|| ($col == 149)|| ($col == 152)|| ($col == 155)|| ($col == 158)|| ($col == 161)|| ($col == 164)|| ($col == 167)|| ($col == 170)|| ($col == 173)|| ($col == 176)|| ($col == 179)|| ($col == 182)|| ($col == 185)|| ($col == 188)|| ($col == 191)|| ($col == 194)|| ($col == 197)|| ($col == 200)|| ($col == 203)|| ($col == 206)|| ($col == 209)|| ($col == 212))
				{
					if($data == 0)
					{
						$worksheet_Data->write($rc_data, $cc_data, $data, $format);
						$yest_fields_empty_count++;
					}else{
						$worksheet_Data->write($rc_data, $cc_data, $data, $format);
					}
					
				}
				elsif(($col == 3)|| ($col == 6)|| ($col == 9)|| ($col == 12)|| ($col == 15)|| ($col == 18)|| ($col == 21)|| ($col == 24)|| ($col == 27)|| ($col == 30)|| ($col == 33)|| ($col == 36)|| ($col == 39)|| ($col == 42)|| ($col == 45)|| ($col == 48)|| ($col == 51)|| ($col == 54)|| ($col == 57)|| ($col == 60)|| ($col == 63)|| ($col == 66)|| ($col == 69)|| ($col == 72)|| ($col == 75)|| ($col == 78)|| ($col == 81)|| ($col == 84)|| ($col == 87)|| ($col == 90)|| ($col == 93)|| ($col == 96)|| ($col == 99)|| ($col == 102)|| ($col == 105)|| ($col == 108)|| ($col == 111)|| ($col == 114)|| ($col == 117)|| ($col == 120)|| ($col == 123)|| ($col == 126)|| ($col == 129)|| ($col == 132)|| ($col == 135)|| ($col == 138)|| ($col == 141)|| ($col == 144)|| ($col == 147)|| ($col == 150)|| ($col == 153)|| ($col == 156)|| ($col == 159)|| ($col == 162)|| ($col == 165)|| ($col == 168)|| ($col == 171)|| ($col == 174)|| ($col == 177)|| ($col == 180)|| ($col == 183)|| ($col == 186)|| ($col == 189)|| ($col == 192)|| ($col == 195)|| ($col == 198)|| ($col == 201)|| ($col == 204)|| ($col == 207)|| ($col == 210)|| ($col == 213))
				{
					if($data == 0){
						$worksheet_Data->write($rc_data, $cc_data, $data, $format);
						$lastWeek_fields_empty_count++;
					}else{
						$worksheet_Data->write($rc_data, $cc_data, $data, $format);
					}
				}else{
					$worksheet_Data->write($rc_data, $cc_data, $data, $format);
				}
			}else{
				$worksheet_Data->write($rc_data, $cc_data, $data, $format);
			}
			
			$cc_data++;
		}
		
		if($row==0)
		{
			$worksheet_Data->write($rc_data, $cc_data, "Field_Diff", $format2);
			$worksheet_Data->write($rc_data, $cc_data+1, 'All fields are "0" (YesterDay)', $format2);
			$worksheet_Data->write($rc_data, $cc_data+2, 'All fields are "0" (Last Week)', $format2);
			$worksheet_Data->write($rc_data, $cc_data+3, 'less than "0"', $format2);
			$worksheet_Data->write($rc_data, $cc_data+4, "1 to 10", $format2);
			$worksheet_Data->write($rc_data, $cc_data+5, "11 to 50", $format2);
			$worksheet_Data->write($rc_data, $cc_data+6, "51 to 60", $format2);
			$worksheet_Data->write($rc_data, $cc_data+7, "61 to 70", $format2);
			$worksheet_Data->write($rc_data, $cc_data+8, "71 to 80", $format2);
			$worksheet_Data->write($rc_data, $cc_data+9, "81 to 90", $format2);
			$worksheet_Data->write($rc_data, $cc_data+10, "91 to 100", $format2);
			
			$worksheet_Data->write($rc_data, $cc_data+12, '1 to 10 Query', $format2);
			$worksheet_Data->write($rc_data, $cc_data+13, '11 to 50 Query', $format2);
			$worksheet_Data->write($rc_data, $cc_data+14, '50 Query', $format2);
			$worksheet_Data->write($rc_data, $cc_data+15, '60 Query', $format2);
			$worksheet_Data->write($rc_data, $cc_data+16, '70 Query', $format2);
			$worksheet_Data->write($rc_data, $cc_data+17, '80 Query', $format2);
			$worksheet_Data->write($rc_data, $cc_data+18, '90 Query', $format2);
			
		}else{
			
			$worksheet_Data->write($rc_data, $cc_data, $field_Diff_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+1, $yest_fields_empty_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+2, $lastWeek_fields_empty_count, $format);
			
			$worksheet_Data->write($rc_data, $cc_data+3, $less_zero_count, $less_Zero_format);
			$worksheet_Data->write($rc_data, $cc_data+4, $one_to_10_count, $one_to_10_format);
			$worksheet_Data->write($rc_data, $cc_data+5, $eleven_to_50_count, $Eleven_to_50_format);
			$worksheet_Data->write($rc_data, $cc_data+6, $fifty_count, $Fifty_to_sixty_format);
			$worksheet_Data->write($rc_data, $cc_data+7, $sixty_count, $sixty_to_seventy_format);
			$worksheet_Data->write($rc_data, $cc_data+8, $seventy_count, $seventy_to_80_format);
			$worksheet_Data->write($rc_data, $cc_data+9, $eighty_count, $Eighty_to_90_Format);
			$worksheet_Data->write($rc_data, $cc_data+10, $nighty_count, $Nighty_to_100_Format);
			
			my $query1=&header_query(\@head1,$qury_date,$sheet,$row,$one_to_10_count);
			my $query2=&header_query(\@head2,$qury_date,$sheet,$row,$eleven_to_50_count);
			my $query3=&header_query(\@head3,$qury_date,$sheet,$row,$fifty_count);
			my $query4=&header_query(\@head4,$qury_date,$sheet,$row,$sixty_count);
			my $query5=&header_query(\@head5,$qury_date,$sheet,$row,$seventy_count);
			my $query6=&header_query(\@head6,$qury_date,$sheet,$row,$eighty_count);
			my $query7=&header_query(\@head7,$qury_date,$sheet,$row,$nighty_count);
			
			$worksheet_Data->write($rc_data, $cc_data+12, $query1, $one_to_10_format);
			$worksheet_Data->write($rc_data, $cc_data+13, $query2, $Eleven_to_50_format);
			$worksheet_Data->write($rc_data, $cc_data+14, $query3, $Fifty_to_sixty_format);
			$worksheet_Data->write($rc_data, $cc_data+15, $query4, $sixty_to_seventy_format);
			$worksheet_Data->write($rc_data, $cc_data+16, $query5, $seventy_to_80_format);
			$worksheet_Data->write($rc_data, $cc_data+17, $query6, $Eighty_to_90_Format);
			$worksheet_Data->write($rc_data, $cc_data+18, $query7, $Nighty_to_100_Format);
		}
		
		$fdc++ if($field_Diff_count!=0);
		$not_diviate_field_c++ if($field_Diff_count==0);
		$lzc++ if($less_zero_count!=0);
		$OtT++ if($one_to_10_count!=0);
		$EtF++ if($eleven_to_50_count!=0);
		$A_50++ if($fifty_count!=0);
		$A_60++ if($sixty_count!=0);
		$A_70++ if($seventy_count!=0);
		$A_80++ if($eighty_count!=0);
		$A_90++ if($nighty_count!=0);
		
		my $field_c;
		$field_c=71 if($Sheet_Name=~m/one/is);
		
		$yfec++ if($yest_fields_empty_count==$field_c);
		$lwfec++ if($lastWeek_fields_empty_count==$field_c);
		
		nxtrow:
		
		$rc_data++;
	}
	
	$worksheet->write(11, 1, $fdc, $format);
	$worksheet->write(12, 1, $not_diviate_field_c-1, $format);
	# $worksheet->write(12, 1, $yfec, $format);
	# $worksheet->write(13, 1, $lwfec, $format);
	$worksheet->write(13, 1, $lzc, $format);
	$worksheet->write(14, 1, $OtT, $format);
	$worksheet->write(15, 1, $EtF, $format);
	$worksheet->write(16, 1, $A_50, $format);
	$worksheet->write(17, 1, $A_60, $format);
	$worksheet->write(18, 1, $A_70, $format);
	$worksheet->write(19, 1, $A_80, $format);
	$worksheet->write(20, 1, $A_90, $format);

	$worksheet->write(21, 1, "", $format);
	$worksheet->write(22, 1, "", $format);
	$worksheet->write(23, 1, "", $format);
	
	nxtWrkBk:
}

$workbook->close;
$workbook2->close;

#------ File Move -------
my @files = glob("*.xls");
foreach my $file(@files)
{
	my $Copy_Status = move ("$dir/$file", "$CurrenDate_Location");
}

print "File Is Moved****\n";

sub Data_Cell()
{
	my $sheet=shift;
	my $row=shift;
	my $col=shift;
	
	my $cell = $sheet -> {Cells} [$row] [$col];
	my $va = $cell -> {Val};
	
	$va=~s/amp;//igs;

	return $va;
}


sub headers()
{
	my ($sheet,$col)=@_;
	
	my $headers=&Data_Cell($sheet,0,$col);
	$headers=~s/\s*\-\s*Difference\s*//igs;
	# print "headers:: $headers\n";
	# print "col:: $col\n";<>;
	
	return $headers;
}

sub header_query()
{
	my $Data = shift;
	my $qury_date = shift;
	my $sheet = shift;
	my $row = shift;
	my $dif_count = shift;
	# print "arr::". length($arr)."\n";<>;
	my @Data = @$Data;
	my $headers=join(", ",@Data);
	$headers=~s/^\,//igs;
	
	my $Council_code =&Data_Cell($sheet,$row,0);
	if($dif_count!=0)
	{	
		$headers='select top 50 Format_Id, Application_No, Council_code,'.$headers.',created_date from format_pdf where council_code= '.$Council_code. ' and convert(date,created_date)< '."'". $qury_date."'".' order by id desc';
	}else{
		$headers="";
	}
	# print "headers:: $headers\n";<>;
	
	return $headers;
}


sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}