use strict;
use Time::HiRes qw(time);
use DateTime;
use POSIX qw(strftime);
use Time::Piece;
use Encode;
use File::Path 'rmtree';
use File::Find;
use File::stat;
use Cwd qw(abs_path);
use File::Basename;
use Cwd;
use DateTime::Format::Excel;
use Spreadsheet::XLSX;
use Excel::Writer::XLSX;
use DateTime::Format::Excel;
use Spreadsheet::WriteExcel;
use File::Copy;

my $date = localtime->strftime('%Y%m%d');
my $date2 = localtime->strftime('%d-%m-%Y');

my $dir = getcwd;

my $dt = DateTime->now->set_time_zone( 'Asia/Kolkata' );
my $y=$dt->year;
my $m=$dt->month_abbr;
my $d=$dt->day;
my $Date="$d-$m-$y";

print "dt:: $dt\n";
print "Date:: $Date\n";
# print "localtime:: ". localtime "\n";<>;

my $Year_Location="$dir/$y";
&createdirectory("$Year_Location");

my $Mon_Location="$Year_Location/$m";
&createdirectory("$Mon_Location");

my $CurrenDate_Location="$Mon_Location/$date2";
&createdirectory("$CurrenDate_Location");

my @files = glob("*.xls");
	
my ($workbook,$worksheet,$workbook2,$worksheet2,$workbook3,$worksheet3);
$workbook = Excel::Writer::XLSX->new("Daily_Report.xls");
$worksheet = $workbook->add_worksheet("Report");

$workbook2 = Excel::Writer::XLSX->new("Field_Fill_Percentage_Report_$date.xls");



my $format  = $workbook->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
$format->set_bold();
$format->set_align('center');

$worksheet->merge_range('A1:A2',"Remarks", $format);
$worksheet->merge_range('B1:C1',"Count or Percentage", $format);
$worksheet->write(1,1,"Planning", $format);
$worksheet->write(1,2,"Decision", $format);

$worksheet->write(6, 0, "Catagories", $format);
$worksheet->write(6, 1, "Number of councils with fields have difference in fill percent between yesterday's and last week percent", $format);
$worksheet->write(6, 2, 'less than "0" (Difference between Lastweek & yesterday  percentage)', $format);
$worksheet->write(6, 3, "1 to 10", $format);
$worksheet->write(6, 4, "11 to 50", $format);
$worksheet->write(6, 5, "51 to 100", $format);
$worksheet->write(6, 6, 'All fields are "0" (YesterDay)', $format);
$worksheet->write(6, 7, 'All fields are "0" (Last Week)', $format);
$worksheet->write(6, 8, 'No Of Council with issues in (51 to 100) %', $format);
$worksheet->write(6, 9, 'Councils code with issues in (51 to 100) percentage', $format);
$worksheet->write(6, 10, 'Comments', $format);


# if( grep (/Field_Fill_Percentage_Monthly_Dump_Report/, @files))
# {
	# print "File Already available\n";
	# my $workbook3 = Spreadsheet::XLSX->new("$dir/Field_Fill_Percentage_Monthly_Dump_Report_"."$m"."_$y.xls");
	# $worksheet3 = ${ $workbook3->{Worksheet} }[0];
# }else{
	# $workbook3 = Excel::Writer::XLSX->new("Field_Fill_Percentage_Monthly_Dump_Report_"."$m"."_$y.xls");
	# $worksheet3 = $workbook3->add_worksheet("Dump");
	# $worksheet3->write(0, 0, "Catagories", $format);
	# $worksheet3->write(0, 1, "Number of councils with fields have difference in fill percent between yesterday's and last week percent", $format);
	# $worksheet3->write(0, 2, 'less than "0" (Difference between Lastweek & yesterday  percentage)', $format);
	# $worksheet3->write(0, 3, "1 to 10", $format);
	# $worksheet3->write(0, 4, "11 to 50", $format);
	# $worksheet3->write(0, 5, "51 to 100", $format);
	# $worksheet3->write(0, 6, 'All fields are "0" (YesterDay)', $format);
	# $worksheet3->write(0, 7, 'All fields are "0" (Last Week)', $format);
	# $worksheet3->write(0, 8, 'No Of Council with issues in (51 to 100) %', $format);
	# $worksheet3->write(0, 9, 'Councils code with issues in (51 to 100) percentage', $format);
	# $worksheet3->write(0, 10, 'Comments', $format);
# }

my $format = $workbook->add_format( border=>1, color=>'black' );
$format->set_bg_color( 'None' );
$format->set_align('center');

$worksheet->write(2,0,"Number of councils having 100% fill rate across all fields  in today's scrape", $format);
$worksheet->write(3,0,"Number of councils that did not have 100% fill rate across all fields in today's scrape", $format);

$worksheet->write(7, 0, "Planning", $format);
$worksheet->write(8, 0, "Decision", $format);

# my $format2 = $workbook2->add_format( border=>1, color=>'black' );
my $format2 = $workbook->add_format(bg_color => "#99CCFF",pattern => 1,border => 1);
# $format2->set_bg_color( 'None' );
$format2->set_bold();
$format2->set_align('center');

my $rc=0;
my $cc=4;
my @council_Num_dump='7|Dass';
	
foreach my $file (@files)
{
	chomp($file);
	print "FILE_NAME:: $file\n";
	# print "dir:: $dir/$file\n";<>;
	my $Sheet_Name=$1 if($file=~m/^([^>]*?)\.xls/is);
	
	if($Sheet_Name!~m/^(Planning|Decision)$/is)
	{
		print "GotoNXT_BOKK\n";
		goto nxtWrkBk;
	}
	
	my $worksheet_Data = $workbook->add_worksheet("$Sheet_Name");
	
	my $excel = Spreadsheet::XLSX->new("$dir/$file");
	my $sheet = ${ $excel->{Worksheet} }[0];
	
	#------ Summary Report ------------------------
	my $summary_sheet = ${ $excel->{Worksheet} }[1];
	
	my ($rrc,$worksheet_summary2);
	my $s_row=1;
	if($Sheet_Name=~m/Planning/is)
	{
		$rrc=7;
		$worksheet2 = $workbook2->add_worksheet("Planning");
		$worksheet_summary2 = $workbook2->add_worksheet("Planning-Summary");
	}else{
		$rrc=8;
		$s_row=2;
		$worksheet2 = $workbook2->add_worksheet("Decision");
		$worksheet_summary2 = $workbook2->add_worksheet("Decision-Summary");
	}

	my $s_data1=&Data_Cell($summary_sheet,1,1);
	my $s_data2=&Data_Cell($summary_sheet,2,1);
	
	$worksheet->write(2, $s_row, $s_data1, $format);
	$worksheet->write(3, $s_row, $s_data2, $format);
	
	my $s_r=0;
	foreach my $row ($summary_sheet -> {MinRow} .. $summary_sheet -> {MaxRow}+1) 
	{
		my $s_col=0;
		foreach my $col ($summary_sheet -> {MinCol} ..  $summary_sheet -> {MaxCol})
		{
			my $data=&Data_Cell($summary_sheet,$row,$col);
			$worksheet_summary2->write_string($s_r, $s_col, $data, $format);
			$s_col++;
		}
		
		$s_r++;
	}
	#-------------------------------------------------

	my ($row_min, $row_max) = $sheet->row_range();
	my ($col_min, $col_max) = $sheet->col_range();
	
	# print "row_max:: $row_max\n";
	# print "col_max:: $col_max\n";
	
	my($fdc,$lzc,$OtT,$EtF,$A_50,$yfec,$lwfec)=(0,0,0,0,0,0,0);
	my $rc_data=0;

	foreach my $row ($sheet -> {MinRow} .. $sheet -> {MaxRow}+1) 
	{
		my ($field_Diff_count, $less_zero_count, $one_to_10_count, $eleven_to_50_count, $above_50, $yest_fields_empty_count,$lastWeek_fields_empty_count)=(0,0,0,0,0,0,0); 
		
		my $cc_data=0;
		foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol})
		{
			my $data=&Data_Cell($sheet,$row,$col);
			# print "row:: $row\n";
			# print "col:: $col\n";<>;
			$data=~s/^\s*$//igs;
			
			# print "data:: $data\n";<>;
			
			goto nxtrow if(($data eq "") && ($col==0));
			
			
			$worksheet_Data->write($rc_data, $cc_data, $data, $format);
			
			$worksheet2->write_string($rc_data, $cc_data, $data, $format);
			
			$cc_data++;
			
			if($row > 0)
			{
				if(($col == 4) || ($col == 7) || ($col == 10) || ($col == 13)|| ($col == 16)|| ($col == 19)|| ($col == 22)|| ($col == 25)|| ($col == 28)|| ($col == 31))
				{
					# print "data:: $data\n";<>;
					$field_Diff_count++ if($data != 0);
					$less_zero_count++ if($data < 0);
					$one_to_10_count++ if(($data > 0) && ($data <= 10));
					$eleven_to_50_count++ if(($data > 10) && ($data <= 50));
					$above_50++ if(($data > 50) && ($data <= 100));
				}elsif(($col == 2) || ($col == 5) || ($col == 8) || ($col == 11)||($col == 14)|| ($col == 17)|| ($col == 20)|| ($col == 23)|| ($col == 26)|| ($col == 29))
				{
					$yest_fields_empty_count++ if($data == 0);
				}elsif(($col == 3) || ($col == 6) || ($col == 9) || ($col == 12)|| ($col == 15)|| ($col == 18)|| ($col == 21)|| ($col == 24)|| ($col == 27)|| ($col == 30))
				{
					$lastWeek_fields_empty_count++ if($data == 0);
				}
			}
		}
		
		if($row==0)
		{
			$worksheet_Data->write($rc_data, $cc_data, "Field_Diff", $format2);
			$worksheet_Data->write($rc_data, $cc_data+1, 'less than "0" (Difference between Lastweek & yesterday  percentage)', $format2);
			$worksheet_Data->write($rc_data, $cc_data+2, "1 to 10", $format2);
			$worksheet_Data->write($rc_data, $cc_data+3, "11 to 50", $format2);
			$worksheet_Data->write($rc_data, $cc_data+4, "51 to 100", $format2);
			$worksheet_Data->write($rc_data, $cc_data+5, 'All fields are "0" (YesterDay)', $format2);
			$worksheet_Data->write($rc_data, $cc_data+6, 'All fields are "0" (Last Week)', $format2);
		}else{
			$worksheet_Data->write($rc_data, $cc_data, $field_Diff_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+1, $less_zero_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+2, $one_to_10_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+3, $eleven_to_50_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+4, $above_50, $format);
			$worksheet_Data->write($rc_data, $cc_data+5, $yest_fields_empty_count, $format);
			$worksheet_Data->write($rc_data, $cc_data+6, $lastWeek_fields_empty_count, $format);
		}
		
		$fdc++ if($field_Diff_count!=0);
		$lzc++ if($less_zero_count!=0);
		$OtT++ if($one_to_10_count!=0);
		$EtF++ if($eleven_to_50_count!=0);
		$A_50++ if($above_50!=0);
		
		my $field_c;
		$field_c=10 if($Sheet_Name=~m/Planning/is);
		$field_c=4 if($Sheet_Name=~m/Decision/is);
		
		$yfec++ if($yest_fields_empty_count==$field_c);
		$lwfec++ if($lastWeek_fields_empty_count==$field_c);
		
		nxtrow:
		
		$rc_data++;
	}
	
	$worksheet->write($rrc, 1, $fdc, $format);
	$worksheet->write($rrc, 2, $lzc, $format);
	$worksheet->write($rrc, 3, $OtT, $format);
	$worksheet->write($rrc, 4, $EtF, $format);
	$worksheet->write($rrc, 5, $A_50, $format);
	$worksheet->write($rrc, 6, $yfec, $format);
	$worksheet->write($rrc, 7, $lwfec, $format);
	$worksheet->write($rrc, 8, "", $format);
	$worksheet->write($rrc, 9, "", $format);
	$worksheet->write($rrc, 10, "", $format);
	
	# my $row_max=0;
	# my ($row_min, $row_max) = $worksheet3->row_range();
	# $row_max++;
	
	# $worksheet3->write($row_max, 1, $fdc, $format);
	# $worksheet3->write($row_max, 2, $lzc, $format);
	# $worksheet3->write($row_max, 3, $OtT, $format);
	# $worksheet3->write($row_max, 4, $EtF, $format);
	# $worksheet3->write($row_max, 5, $A_50, $format);
	# $worksheet3->write($row_max, 6, $yfec, $format);
	# $worksheet3->write($row_max, 7, $lwfec, $format);
	# $worksheet3->write($row_max, 8, "", $format);
	# $worksheet3->write($row_max, 9, "", $format);
	# $worksheet3->write($row_max, 10, "", $format);
	
	nxtWrkBk:
}

$workbook->close;
$workbook2->close;

#------ File Move -------
my @files = glob("*.xls");
foreach my $file(@files)
{
	my $Copy_Status = move ("$dir/$file", "$CurrenDate_Location");
}

print "File Is Moved****\n";

sub Data_Cell()
{
	my $sheet=shift;
	my $row=shift;
	my $col=shift;
	
	my $cell = $sheet -> {Cells} [$row] [$col];
	my $va = $cell -> {Val};
	
	$va=~s/amp;//igs;

	return $va;
}

sub createdirectory()
{
	my $dirname=shift;
	if (-e $dirname and -d $dirname) 
	{
		print "Directory exists\n";
	}
	else 
	{
		print "Directory doesnot exists\n";
		mkdir( $dirname ) or die "Couldn't create $dirname directory, $!\n";
		print "Directory created successfully\n";
	}
}