# -*- coding: utf-8 -*-
import requests
import pdfkit
import os,sys,re
from bs4 import BeautifulSoup

with open('Amazon_Output.txt', 'w') as fd:
        fd.write("Product_Name"+"\t"+"Price"+"\t"+"Link"+"\n")

def dataFetch(blk,regx):
    dataPoint=""
    try:
        dataPoint=re.findall(regx,str(blk),re.IGNORECASE)
        dataPoint=dataPoint[0]
    except:
        pass
    
    clean(dataPoint)
    return dataPoint
        
def clean(cleanValue):
	try:
		clean=''
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
		
		return clean	

	except Exception as ex:
		print(ex,sys.exc_traceback.tb_lineno)
		
url='https://www.amazon.de/gp/search/ref=sr_nr_n_0?fst=as%3Aoff&rh=n%3A3167641%2Cn%3A%213169011%2Cn%3A3597086031%2Cp_36%3A2000-99999999%2Cn%3A13528286031&bbn=3597086031&ie=UTF8&qid=1507905316&rnid=3597086031'
# url='https://www.amazon.de/s?bbn=3597086031&rh=n%3A3167641%2Cn%3A%213169011%2Cn%3A3597086031%2Cn%3A13528286031%2Cp_36%3A2000-99999999&dc&fst=as%3Aoff&qid=1507905316&rnid=3597086031&ref=sr_nr_n_0'

s=requests.session()
headers = {
		"authority": "www.amazon.de",
		"method": "GET",
		"accept-language": "en-US,en;q=0.9",
		"path": "/s?bbn=3597086031&rh=n%3A3167641%2Cn%3A%213169011%2Cn%3A3597086031%2Cn%3A13528286031%2Cp_36%3A2000-99999999&dc&fst=as%3Aoff&qid=1507905316&rnid=3597086031&ref=sr_nr_n_0",
		"scheme": "https",
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"content-type":'text/html;charset=UTF-8',
		"Upgrade-Insecure-Requests": "1",
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
	}
respond=s.get(url,headers=headers)
print ("respond Status : ",respond.status_code)

page_content = BeautifulSoup(respond.content, "html.parser")
    
blocks=re.findall('\"s-expand-height s-include-content-margin s-latency-cf-section[^>]*?>([\w\W]*?)<\/div>\s*<\/div>\s*<\/div>\s*<\/div>',str(page_content),re.IGNORECASE)

op=[]
for blk in blocks:
    blk=str(blk)
    price=dataFetch(blk,'a-price-whole\">([^>]*?)<')
    name=dataFetch(blk,'a-size-base-plus[^>]*?\">([^>]*?)<\/span>')
    link=dataFetch(blk,'<a\s*class\=\"a\-link\-normal\s*a\-text\-normal\"\s*href\=\"([^>]*?)\">')
    
    name=clean(name)
    price=clean(price)
    link=clean(link)
    
    link="https://www.amazon.de"+str(link)
    
    print("name : ",name)
    print("price : ",price)
    print("link : ",link)
    
    op.append(price)
    op.append(name)
    op.append(link)
    
    with open('Amazon_Output.txt', 'a') as fd:
        fd.write(str(name)+"\t"+str(price)+"\t"+str(link)+"\n")

print ("Output in List :",op)    
    
    
    
        

