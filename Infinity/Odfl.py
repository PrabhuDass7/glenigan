# -*- coding: utf-8 -*-
import requests
import pdfkit
import os,sys,re
from bs4 import BeautifulSoup

with open('Odfl_Ouptut.txt', 'w') as fd:
	fd.write("Origin"+"\t"+"Service_Cente_1"+"\t"+"Destination"+"\t"+"Service_Cente_2"+"\t"+"Deliver"+"\t"+"Standard_Service_Days"+"\t"+"Service"+"\t"+"\n")
	
def clean(cleanValue):
	try:
		clean=''
		clean = re.sub(r'\n', "", str(cleanValue)) 
		clean = re.sub(r'\'', "''", str(clean)) 
		clean = re.sub(r'\t', "", str(clean))
		clean = re.sub(r'\\', "", str(clean))
		clean = re.sub(r'\&nbsp\;', " ", str(clean))
		clean = re.sub(r'\&amp\;', "&", str(clean))
		clean = re.sub(r'\s*<[^>]*?>\s*', " ", str(clean))
		clean = re.sub(r'^\s+|\s+$', "", str(clean))
		clean = re.sub(r'\s\s+', " ", str(clean))
		clean = re.sub(r'^\W+$', "", str(clean))
		clean = re.sub(r'\&\#39\;', "'", str(clean))
		clean = re.sub(r'(?:[\,\s])+$', "", str(clean))
		clean = re.sub(r'\'', "''", str(clean))
		clean = re.sub(r'^\s*', "", str(clean))
		clean = re.sub(r'\s*$', "", str(clean))
		
		return clean	

	except Exception as ex:
		print(ex,sys.exc_traceback.tb_lineno)
		
url='https://www.odfl.com/TransitTime/'

s=requests.session()

respond=s.get(url)

print ("respond Status : ",respond.status_code)

page_content = BeautifulSoup(respond.content, "html.parser")
page_content = page_content.encode("ascii", "ignore")
page_content = page_content.decode()
	
with open('Content.html', 'w') as fd:
	fd.write(page_content)


as_sfid=re.findall('<input\s*name\=\"as\_sfid\"[^>]*?value\=\"([^>]*?)\"',str(page_content),re.IGNORECASE)
as_sfid=clean(as_sfid[0])

as_fid=re.findall('<input\s*name\=\"as_fid\"[^>]*?value\=\"([^>]*?)\"',str(page_content),re.IGNORECASE)
as_fid=clean(as_fid[0])

ViewState=re.findall('id\=\"j\_id1[^>]*?\"[^>]*?value\=\"([^>]*?)\"',str(page_content),re.IGNORECASE)
ViewState=clean(ViewState[0])


print ("as_sfid:",as_sfid)
print ("as_fid:",as_fid)
print ("ViewState:",ViewState)

payload="skeletonForm=skeletonForm&skeletonForm%3AorgCity=&skeletonForm%3AorgState=&skeletonForm%3AdstCity=&skeletonForm%3AdstState=&skeletonForm%3AorgZip=10901&skeletonForm%3AorgZip2=&skeletonForm%3AorgZip3=&skeletonForm%3AorgZip4=&skeletonForm%3AdstZip=10001&skeletonForm%3AdstZip2=&skeletonForm%3AdstZip3=&skeletonForm%3AdstZip4=&skeletonForm%3AsubmitBtn=Submit&javax.faces.ViewState="+str(ViewState)+"&as_sfid="+str(as_sfid)+"&as_fid="+str(as_fid)+""

print ("LEN:",len(payload))

headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.9",
		"Connection": "keep-alive",
		"Content-Type": "application/x-www-form-urlencoded",
		"Content-Length":'659',
		'Origin': 'https://www.odfl.com',
		"Upgrade-Insecure-Requests": "1",
		"Referer" : 'https://www.odfl.com/TransitTime/',
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
		"Host": "www.odfl.com",
	}
	
s.headers.update(headers)

link=re.findall('<form\s*action\=\"([^>]*?)\"',str(page_content),re.IGNORECASE)
link=clean(link[0])

url2="https://www.odfl.com" + str(link)

print ("URL2::",url2)

response = s.post(url2,data=payload,headers=headers)
searchcontent = response.content
print("SearchPost Response::",response.status_code)

searchcontent = BeautifulSoup(searchcontent, "html.parser")
searchcontent = searchcontent.encode("ascii", "ignore")
searchcontent = searchcontent.decode()

with open('Content2.html', 'w') as fd:
	fd.write(str(searchcontent))
		
Origin=re.findall('<label>\s*Origin\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Origin=clean(Origin[0])

Service_Cente_1=re.findall('<label>\s*Service\s*Center\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Service_Cente_1=clean(Service_Cente_1[0])

Service_Cente_2=re.findall('<label>\s*Service\s*Center\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Service_Cente_2=clean(Service_Cente_2[1]) 

Destination=re.findall('<label>\s*Destination\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Destination=clean(Destination[0]) 

Deliver=re.findall('<label>\s*Deliver\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Deliver=clean(Deliver[0]) 

Standard_Service_Days=re.findall('<label>\s*Standard\s*Service\s*Days\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Standard_Service_Days=clean(Standard_Service_Days[0])

Service=re.findall('<label>\s*Service\:\s*<\/label>[\w\W]*?id\=\"skeletonForm\:[^>]*?>([\w\W]*?)<\/div>',str(searchcontent),re.IGNORECASE)
Service=clean(Service[0])

print ("Origin : ",Origin)
print ("Service_Cente_1 : ",Service_Cente_1)
print ("Destination : ",Destination)
print ("Service_Cente_2 : ",Service_Cente_2)
print ("Deliver : ",Deliver)
print ("Standard_Service_Days : ",Standard_Service_Days)
print ("Service : ",Service)

with open('Odfl_Ouptut.txt', 'a') as fd:
	fd.write(str(Origin)+"\t"+str(Service_Cente_1)+"\t"+str(Destination)+"\t"+str(Service_Cente_2)+"\t"+str(Deliver)+"\t"+str(Standard_Service_Days)+"\t"+str(Service)+"\t"+"\n")

print ("Completed")
