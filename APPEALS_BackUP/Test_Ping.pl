use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use FileHandle;
use JSON::Parse 'parse_json';
use URI::Escape;
use DateTime;

my $ua=LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);
# $ua->proxy(['http','https'], 'http://172.27.137.192:3128');

# my $Config = Config::Tiny->new();
# $Config = Config::Tiny->read("$Current_Location/Scraping_Details.ini");
# print "result: $resu\n";

my $Home_Url="https://acp.planninginspectorate.gov.uk/CaseSearch.aspx";
print "$Home_Url\n";
my ($Content, $Cookie)=&Getcontent($Home_Url);
print "Cookie: $Cookie\n";
my $fh=FileHandle->new("planninginspectorate.html",'w');
binmode($fh);
$fh->print($Content);
$fh->close();
exit;
# my $Home_Url="http://planning.broxtowe.gov.uk/Scripts/ApplicationDetail.jsx";
# print "$Home_Url\n";
# my ($Content,$Cookie)=&Getcontent($Home_Url);
# print "Cookie: $Cookie\n";
# my $fh=FileHandle->new("Arun2.html",'w');
# binmode($fh);
# $fh->print($Content);
# $fh->close();

# my $Home_Url="http://www1.arun.gov.uk/PublicViewer/Authenticated/Results.aspx?tab=ALL%20RECORDS";
# print "$Home_Url\n";
# my ($Content,$Cookie)=&Getcontent($Home_Url);
# print "Cookie: $Cookie\n";
# my $fh=FileHandle->new("Arun3.html",'w');
# binmode($fh);
# $fh->print($Content);
# $fh->close();

# my $Home_Url="http://www1.arun.gov.uk/PublicViewer/Authenticated/FrameImage.aspx?pg_id=2106629";
# print "$Home_Url\n";
# my ($Content,$Cookie)=&Getcontent($Home_Url);
# print "Cookie: $Cookie\n";
# my $fh=FileHandle->new("Arun.pdf",'w');
# binmode($fh);
# $fh->print($Content);
# $fh->close();

# exit;
# while($Content=~m/<script \s*[^>]*?\s*src\s*\=(?:\'|\")?([^<]*?)(?:\'|\")\s*[^>]*?>/igs)
# {
	# $Home_Url='http://www1.arun.gov.uk/'.$1;
	# print "$Home_Url\n";
	# my ($Content,$Cookie)=&Getcontent($Home_Url);
	# my $fh=FileHandle->new("Arun.html",'w');
	# binmode($fh);
	# $fh->print($Content);
	# $fh->close();
	# print "OK\n";
	# <stdin>;
# }

# my $Home_Url="http://www1.arun.gov.uk/PublicViewer/Authenticated/Main.aspx?user_key_1=AL/115/17/OUT";
# print "$Home_Url\n";
# my ($Content,$Cookie)=&Getcontent($Home_Url);
# print "Cookie: $Cookie\n";
# my $fh=FileHandle->new("Arun1.html",'w');
# binmode($fh);
# $fh->print($Content);
# $fh->close();


# if($Content=~m/<a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*View\s*associated\s*documents/is)
# {
	# $Home_Url=$1;
	# print "$Home_Url\n";
	# ($Content,$Cookie)=&Getcontent($Home_Url);
	# my $fh=FileHandle->new("waveney.html",'w');
	# binmode($fh);
	# $fh->print($Content);
	# $fh->close();
# }
# exit;

my ($ViewState,$ViewstateGenerator,$EventValidation,$CSRFToken,$ActivityId);
# if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATE\"\s*value="([^>]*?)"\s*\/>/is)
if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATE\"\s*id\s*=\s*\"__VIEWSTATE\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
{
	$ViewState=uri_escape($1);
}
if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewstateGenerator=uri_escape($1);
}

if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$EventValidation=uri_escape($2);
}

if($Content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"Civica\.CSRFToken[^>]*?value="([^>]*?)"\s*\/>/is)
{
	$CSRFToken=uri_escape($1);
}

if($Content=~m/<input\s*type\s*=\s*"hidden[^>]*?ActivityId"\s*value=\"([^>]*?)\"\s*\/>/is)
{
	$ActivityId=$1;
	# $ActivityId='20180321132645299b4df60860f7f5';
}
# print "\n$replace_info1\n";

print "$ViewstateGenerator\n";

my $Search_po_Content='__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE='.$ViewState.'&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION='.$EventValidation.'&ctl00%24ContentPlaceHolder%24ddlPageSize=99999&ctl00%24ContentPlaceHolder%24gvApplicationDocuments%24ctl02%24ctl00=061d294a81aa32e7&ctl00%24ContentPlaceHolder%24gvApplicationDocuments%24ctl02%24btnViewDocument=View+document&ctl00%24ContentPlaceHolder%24gvApplicationDocuments%24ctl02%24hdnID=061d294a81aa32e7&ctl00%24hdnWindowWidth=1616';
my $post_url='https://dctmviewer.salford.gov.uk/PlanningApplications/ExternalEntryPoint.aspx?SEARCH_TYPE=1&DOC_CLASS_CODE=PLAN&Application_number=18%2f71546%2fFUL';
my $Referer='https://dctmviewer.salford.gov.uk/PlanningApplications/ExternalEntryPoint.aspx?SEARCH_TYPE=1&DOC_CLASS_CODE=PLAN&Application_number=18%2f71546%2fFUL';
my $Host = 'dctmviewer.salford.gov.uk';
my ($Result_content,$Cookie)=&Post_Method($post_url, $Host, $Referer, $Search_po_Content);

my $fh=FileHandle->new("salford_Post.html",'w');
binmode($fh);
$fh->print($Result_content);
# $fh->print($Search_po_Content);
$fh->close();
exit;
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*\"__VIEWSTATE\"\s*id\s*=\s*\"__VIEWSTATE\s*"\s*value=\"([^>]*?)\"\s*\/>/is)
{
	$ViewState=uri_escape($1);
}
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__VIEWSTATEGENERATOR\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$ViewstateGenerator=uri_escape($2);
}
if($Result_content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"([^>]*?)"\s*id\s*=\s*"__EVENTVALIDATION\s*"\s*value="([^>]*?)"\s*\/>/is)
{
	$EventValidation=uri_escape($2);
}

if($Result_content=~m/<input\s*type\s*=\s*"hidden[^>]*?ActivityId"\s*value=\"([^>]*?)\"\s*\/>/is)
{
	$ActivityId=$1;
	# $ActivityId='20180321132645299b4df60860f7f5';
}
my $target;
if($Result_content=~m/>\s*\[\s*<i>\s*([\d]+)\s*<\/i>\s*\]\s*<\/a>/is) 
{
	$target = uri_escape($1);
	$target = sprintf("%02d", $target);
}
print "Target: $target\n";
				
my $Search_po_Content='__LASTFOCUS=&__EVENTTARGET=ctl00%24MainContent%24dlPager%24ctl'.$target.'%24lnkPageNo&__EVENTARGUMENT=&__VIEWSTATE='.$ViewState.'&__VIEWSTATEGENERATOR='.$ViewstateGenerator.'&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=228&__EVENTVALIDATION='.$EventValidation.'&ctl00%24txtsearch=Search&ctl00%24MainContent%24hdnActivityId='.$ActivityId.'&ctl00%24MainContent%24hdnAdvancedSearchOptionsOpen=&ctl00%24MainContent%24hdnAdvancedSearchOptionsEntered=&ctl00%24MainContent%24hdnSearchType=&ctl00%24MainContent%24hfScrollPosition=0';
# my $Search_po_Content='__LASTFOCUS=&__EVENTTARGET=ctl00%24MainContent%24dlPager%24ctl'.$target.'%24lnkPageNo&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwULLTEzODc2Nzk2NDcPZBYCZg9kFgICAw9kFgwCAQ8PFgIeB1Zpc2libGVoZBYCAgEPDxYCHgRUZXh0ZWRkAgUPDxYCHwEFqwE8bGk%2BPGEgdGl0bGU9IlNlYXJjaCBob21lIiBocmVmPSJTZWFyY2guYXNweD9jZD1BcHBsaWNhdGlvblNlYXJjaCZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij5TZWFyY2ggaG9tZTwvYT48L2xpPjxsaSBjbGFzcz0ibGFzdCI%2BUGxhbm5pbmcgYXBwbGljYXRpb24gc2VhcmNoPC9saT5kZAILD2QWAgIBDw8WAh8BBV08bGk%2BPGEgdGl0bGU9ImNvbnRhY3QgdXMiIGhyZWY9Im1haWx0bzpkZXZjb25AcHJlc3Rvbi5nb3YudWsiID5kZXZjb25AcHJlc3Rvbi5nb3YudWs8L2E%2BPC9saT5kZAIPDw8WAh8BBRtQbGFubmluZyBhcHBsaWNhdGlvbiBzZWFyY2hkZAIRD2QWBgILDw8WAh8AaGQWDgIBDw8WAh8BZWRkAhEPEA8WBh4ORGF0YVZhbHVlRmllbGQFCVdhcmRfQ29kZR4NRGF0YVRleHRGaWVsZAUJV2FyZF9OYW1lHgtfIURhdGFCb3VuZGdkEBUXAFBBc2h0b24gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBCcm9va2ZpZWxkICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBDYWRsZXkgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBDb2xsZWdlICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBEZWVwZGFsZSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBGaXNod2ljayAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBHYXJyaXNvbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBHcmV5ZnJpYXJzICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBJbmdvbCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBMYXJjaGVzICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBMZWEgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBNb29yIFBhcmsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBQcmVzdG9uIFJ1cmFsIEVhc3QgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBQcmVzdG9uIFJ1cmFsIE5vcnRoICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBSaWJibGV0b24gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBSaXZlcnN3YXkgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBTaGFyb2UgR3JlZW4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBTdCBHZW9yZ2VzICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBTdCBNYXR0aGV3cyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBUb3duIENlbnRyZSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBUdWxrZXRoICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFBVbml2ZXJzaXR5ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIBUXAAIzMgIzNgIzNQIzOAIzOQIzMwIzNAIzNwI0NgI1MAI0MgI0NwI0NAI0MQI0OAI0MAI1MQI0NQI0MwIzMQI0OQIzMBQrAxdnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCFQ8QZGQWAWZkAhkPDxYCHwEFCjAxLzAzLzIwMThkZAIdDw8WAh8BBQowNy8wMy8yMDE4ZGQCHw9kFgICGw8QDxYGHwIFC1BhcmlzaF9Db2RlHwMFC1BhcmlzaF9OYW1lHwRnZBAVCgBQQmFydG9uICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQQnJvdWdodG9uLWluLUFtb3VuZGVybmVzcyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQR29vc25hcmdoICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQR3JpbXNhcmdoICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQSGFpZ2h0b24gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQSW5nb2wgYW5kIFRhbnRlcnRvbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQTGVhIGFuZCBDb3R0YW0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQV2hpdHRpbmdoYW0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBQV29vZHBsdW1wdG9uICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAVCgACMTICMTEBOQIxNQIxMwIyOAIxMAIxNgIxNBQrAwpnZ2dnZ2dnZ2dnFgFmZAIhDw8WAh4NT25DbGllbnRDbGljawVvZGlzYWJsZUJ1dHRvbk9uQ2xpY2sodGhpcywgJ1BsZWFzZSB3YWl0Li4uJywgJ2Rpc2FibGVkX2J1dHRvbicpOyBfX2RvUG9zdEJhY2soJ2N0bDAwJE1haW5Db250ZW50JGJ0blNlYXJjaCcsJycpZGQCDQ8PFgIfAQVPNDMgcmVjb3JkcyBmb3VuZC48YnIvPkNsaWNrIG9uIHRoZSBhcHBsaWNhdGlvbiBudW1iZXIgdG8gdmlldyB0aGUgZnVsbCBkZXRhaWxzLmRkAhEPDxYCHwBnZBYGAgMPPCsACQEADxYGHghEYXRhS2V5cxYAHgtfIUl0ZW1Db3VudAIFHwBnZBYKZg9kFgICAQ8PFgYfAQUKWzxpPjE8L2k%2BXR4PQ29tbWFuZEFyZ3VtZW50BQExHgdFbmFibGVkaGRkAgEPZBYCAgEPDxYEHwEFATIfCAUBMmRkAgIPZBYCAgEPDxYEHwEFATMfCAUBM2RkAgMPZBYCAgEPDxYEHwEFATQfCAUBNGRkAgQPZBYCAgEPDxYEHwEFBExhc3QfCAUBNWRkAgcPDxYCHwBnZBYCAgEPDxYCHwEFtzE8ZGl2IHN0eWxlPSJ3aWR0aDoxMDAlO2Zsb2F0OmxlZnQ7Ij48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI3NSZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNzU8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj4yNCwgVGhlIE1hbGwsIFByZXN0b24sIFBSMiA2RFU8YnIvPjxiPkRlc2NyaXB0aW9uOiA8L2I%2BU2luZ2xlIHN0b3JleSByZWFyIGV4dGVuc2lvbiAocGFydCByZXRyb3NwZWN0aXZlKTxici8%2BPGI%2BUmVnaXN0cmF0aW9uIERhdGU6IDwvYj4wNy8wMy8yMDE4PGJyLz48Yj5EZWNpc2lvbjogPC9iPjxici8%2BPC9kaXY%2BPC9kaXY%2BPCEtLSAjY29udGFpbmVyIC0tPjxoci8%2BPGJyLz48ZGl2PjxkaXYgaWQ9ImFwcGxpY2F0aW9uY29udGFpbmVyIj48ZGl2IGlkPSJhcHBsaWNhdGlvbmRldGFpbHMiPjxiPkFwcGxpY2F0aW9uIE51bWJlcjogPC9iPjxhIHN0eWxlPSJ0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lICFpbXBvcnRhbnQ7Y29sb3I6Ymx1ZTsiIHRpdGxlPSJWaWV3IGZ1cnRoZXIgZGV0YWlscyIgaHJlZj0iQXBwbGljYXRpb25WaWV3LmFzcHg%2FQXBwTm89MDYvMjAxOC8wMjc0JklkMT0yMDE4MDMyMTEzMjY0NTI5OWI0ZGY2MDg2MGY3ZjUiPjA2LzIwMTgvMDI3NDwvYT48L2I%2BPGJyLz48Yj5Mb2NhdGlvbjogPC9iPjE1LCBHYXJyaXNvbiBSb2FkLCBGdWx3b29kLCBQcmVzdG9uLCBMYW5jYXNoaXJlLCBQUjIgOEFMPGJyLz48Yj5EZXNjcmlwdGlvbjogPC9iPlJlZHVjZSBoZWlnaHQvZmVsbCwgY3Jvd24gcmVkdWNlL3RoaW5uaW5nIHRvIDhuby4gdHJlZXM8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDcvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI3MyZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNzM8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj5Gb3JtZXIsIFdoaXR0aW5naGFtIEhvc3BpdGFsLCBXaGl0dGluZ2hhbSBIb3NwaXRhbCBTaXRlLCBXaGl0dGluZ2hhbSBMYW5lLCBCcm91Z2h0b24sIFByZXN0b24sIExhbmNhc2hpcmUsIFBSMyAySkU8YnIvPjxiPkRlc2NyaXB0aW9uOiA8L2I%2BSW5zdGFsbGF0aW9uIG9mIGFuIHVuZGVyZ3JvdW5kIGZvdWwgZ3JvdW5kIHdhdGVyIHBpcGUgaW5jbHVkaW5nIGFpciB2YWx2ZSBjaGFtYmVyIGFuZCB3YXNob3V0IGNoYW1iZXI8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDYvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI3MSZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNzE8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj5MYW5kIGFkamFjZW50LCA3NywgQ2h1cmNoIEF2ZW51ZSwgUHJlc3RvbiwgTGFuY2FzaGlyZSwgUFIxIDRVRDxici8%2BPGI%2BRGVzY3JpcHRpb246IDwvYj5EaXNjaGFyZ2Ugb2YgY29uZGl0aW9uIG5vLjUgKFNpdGUgSW52ZXN0aWdhdGlvbikgYXR0YWNoZWQgdG8gcGxhbm5pbmcgcGVybWlzc2lvbiAwNi8yMDE3LzAwNzY8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDYvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI2OSZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNjk8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj5MYW5kIGFkamFjZW50LCAyNCAsIFR1bGtldGggUm9hZCwgUFJFU1RPTiwgUFIyIDFBUTxici8%2BPGI%2BRGVzY3JpcHRpb246IDwvYj5EaXNjaGFyZ2Ugb2YgY29uZGl0aW9uIG5vLjUgKFNpdGUgSW52ZXN0aWdhdGlvbikgYXR0YWNoZWQgdG8gcGxhbm5pbmcgcGVybWlzc2lvbiAwNi8yMDE3LzAwNzc8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDYvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI2MSZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNjE8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj40NiwgSGVuZGVyc29uIFN0cmVldCwgUHJlc3RvbiwgUFIxIDdYUDxici8%2BPGI%2BRGVzY3JpcHRpb246IDwvYj5DaGFuZ2Ugb2YgdXNlIGZyb20gcmVzaWRlbnRpYWwgdG8gZWR1Y2F0aW9uIGZvciB0aGUgdGVhY2hpbmcgb2YgSUZFIHNraWxscywgZXJlY3Rpb24gb2YgYSBjb25zZXJ2YXRvcnkgdG8gcmVhciBhbmQgY3JlYXRpb24gb2YgYW4gYWNjZXNzIGdhdGUgZnJvbSB0aGUgYmFjayB5YXJkIHRvIHRoZSBwdWJsaWMgZm9vdHBhdGg8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDUvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI2NSZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNjU8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj5UaGUgT2xkIEJsYWNrIEJ1bGwsIDM1LTM2IEZyaWFyZ2F0ZSwgUHJlc3RvbiwgTGFuY2FzaGlyZSwgUFIxIDJBVDxici8%2BPGI%2BRGVzY3JpcHRpb246IDwvYj5MaXN0ZWQgQnVpbGRpbmcgY29uc2VudCBmb3IgaW50ZXJuYWwgYW5kIGV4dGVybmFsIGFsdGVyYXRpb25zIGFzc29jaWF0ZWQgd2l0aCAxLjhtIGhpZ2ggZmVuY2UgYW5kIGdhdGUsIGZpeGVkIHNlYXRpbmcgZm9ybWVkIGZyb20gcGFsbGV0cywgdGltYmVyIFRWIHVuaXQgYW5kIGNhbm9weSBhbmQgbGlnaHRpbmc8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDYvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI2NCZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNjQ8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj5UaGUgT2xkIEJsYWNrIEJ1bGwsIDM1LTM2IEZyaWFyZ2F0ZSwgUHJlc3RvbiwgTGFuY2FzaGlyZSwgUFIxIDJBVDxici8%2BPGI%2BRGVzY3JpcHRpb246IDwvYj4xLjhtIGhpZ2ggZmVuY2UgYW5kIGdhdGUsIGZpeGVkIHNlYXRpbmcgZm9ybWVkIGZyb20gcGFsbGV0cywgdGltYmVyIFRWIHVuaXQgYW5kIGNhbm9weSBhbmQgbGlnaHRpbmc8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDYvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj48ZGl2IGlkPSJhcHBsaWNhdGlvbmNvbnRhaW5lciI%2BPGRpdiBpZD0iYXBwbGljYXRpb25kZXRhaWxzIj48Yj5BcHBsaWNhdGlvbiBOdW1iZXI6IDwvYj48YSBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZSAhaW1wb3J0YW50O2NvbG9yOmJsdWU7IiB0aXRsZT0iVmlldyBmdXJ0aGVyIGRldGFpbHMiIGhyZWY9IkFwcGxpY2F0aW9uVmlldy5hc3B4P0FwcE5vPTA2LzIwMTgvMDI2MyZJZDE9MjAxODAzMjExMzI2NDUyOTliNGRmNjA4NjBmN2Y1Ij4wNi8yMDE4LzAyNjM8L2E%2BPC9iPjxici8%2BPGI%2BTG9jYXRpb246IDwvYj5Sb3lhbCBQcmVzdG9uIEhvc3BpdGFsLCBTaGFyb2UgR3JlZW4gTGFuZSwgUHJlc3RvbiwgTGFuY2FzaGlyZSwgUFIyIDlIQjxici8%2BPGI%2BRGVzY3JpcHRpb246IDwvYj5FcmVjdGlvbiBvZiBmb3VyIHN0b3JleSBtdWx0aS1zdG9yZXkgY2FyIHBhcmsgKHB1cnN1YW50IHRvIDA2LzIwMTQvMDg0OSB0byBzZWVrIHZhcmlhdGlvbiBvZiBjb25kaXRpb24gbm8ncyAzICJUcmF2ZWwgUGxhbiIsIDQgIkV4dGVybmFsIE1hdGVyaWFscyIsIDUgIk9mZi1TaXRlIFdvcmtzIiwgNiAiVHJhZmZpYyBNYW5hZ2VtZW50IiwgNyAiQ2FyIFBhcmtpbmciLCAxMCAiTGFuZHNjYXBpbmciIGFuZCAxMiAiVHJlZXMiKTxici8%2BPGI%2BUmVnaXN0cmF0aW9uIERhdGU6IDwvYj4wNS8wMy8yMDE4PGJyLz48Yj5EZWNpc2lvbjogPC9iPjxici8%2BPC9kaXY%2BPC9kaXY%2BPCEtLSAjY29udGFpbmVyIC0tPjxoci8%2BPGJyLz48ZGl2PjxkaXYgaWQ9ImFwcGxpY2F0aW9uY29udGFpbmVyIj48ZGl2IGlkPSJhcHBsaWNhdGlvbmRldGFpbHMiPjxiPkFwcGxpY2F0aW9uIE51bWJlcjogPC9iPjxhIHN0eWxlPSJ0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lICFpbXBvcnRhbnQ7Y29sb3I6Ymx1ZTsiIHRpdGxlPSJWaWV3IGZ1cnRoZXIgZGV0YWlscyIgaHJlZj0iQXBwbGljYXRpb25WaWV3LmFzcHg%2FQXBwTm89MDYvMjAxOC8wMjYwJklkMT0yMDE4MDMyMTEzMjY0NTI5OWI0ZGY2MDg2MGY3ZjUiPjA2LzIwMTgvMDI2MDwvYT48L2I%2BPGJyLz48Yj5Mb2NhdGlvbjogPC9iPjU3IEJsYWNrcG9vbCBSb2FkLCBSaWJibGV0b24sIFByZXN0b24sIFBSMiA2Qlg8YnIvPjxiPkRlc2NyaXB0aW9uOiA8L2I%2BU2luZ2xlIHN0b3JleSBleHRlbnNpb24gdG8gcmVhciwgdGhlIHByb3Bvc2VkIGV4dGVuc2lvbiB3b3VsZCBleHRlbmQgYmV5b25kIHRoZSByZWFyIHdhbGwgb2YgdGhlIG9yaWdpbmFsIGR3ZWxsaW5nIGJ5IDYuMG0sIHRoZSBtYXhpbXVtIGhlaWdodCBvZiB0aGUgcHJvcG9zZWQgZXh0ZW5zaW9uIHdvdWxkIGJlIDIuOG0gYW5kIDIuNTVtIGhlaWdodCBhdCB0aGUgZWF2ZXM8YnIvPjxiPlJlZ2lzdHJhdGlvbiBEYXRlOiA8L2I%2BMDIvMDMvMjAxODxici8%2BPGI%2BRGVjaXNpb246IDwvYj48YnIvPjwvZGl2PjwvZGl2PjwhLS0gI2NvbnRhaW5lciAtLT48aHIvPjxici8%2BPGRpdj5kZAIJDzwrAAkBAA8WBh8GFgAfBwIFHwBnZBYKZg9kFgICAQ8PFgYfAQUKWzxpPjE8L2k%2BXR8IBQExHwloZGQCAQ9kFgICAQ8PFgQfAQUBMh8IBQEyZGQCAg9kFgICAQ8PFgQfAQUBMx8IBQEzZGQCAw9kFgICAQ8PFgQfAQUBNB8IBQE0ZGQCBA9kFgICAQ8PFgQfAQUETGFzdB8IBQE1ZGQCEw8PFgIfAGhkFgQCAQ9kFgICAQ8PFgIfAQVVUGxhbm5pbmcgRGVwYXJ0bWVudCA8YnIvPlRvd24gSGFsbDxici8%2BTGFuY2FzdGVyIFJvYWQgPGJyLz5QcmVzdG9uIDxici8%2BUFIxIDJSTCA8YnIvPmRkAgMPDxYCHwEFxQE8bGkgY2xhc3M9ImVtYWlsIj5FbWFpbDogPGEgdGl0bGU9ImNvbnRhY3QgdXMiIGhyZWY9Im1haWx0bzpkZXZjb25AcHJlc3Rvbi5nb3YudWsiPmRldmNvbkBwcmVzdG9uLmdvdi51azwvYT48L2xpPjxsaSBjbGFzcz0icGhvbmUiPlRlbGVwaG9uZTogMDE3NzIgOTA2OTEyPC9saT48bGkgY2xhc3M9ImZheCI%2BRmF4OiAwMTc3MiA5MDY3NjI8L2xpPmRkZAFVpWKs9cSyrpdxgOWl%2BXbBsYaP&__VIEWSTATEGENERATOR='.$ViewstateGenerator.'&__SCROLLPOSITIONX=0&__SCROLLPOSITIONY=228&__EVENTVALIDATION=%2FwEWDwKUsJKGDwLroOTgBAKC2fOnBgKgs%2FCcDQLU4OLyDgL80OrQAwL9xLzGBQLmiqyLDwKL4rXRDgLs3rShDwKUo6iSAQL3kY3eBAL3kfHPBAL3kYXVCwL3kansCYGMXiFtrqlqW78OxPMnU1dUm2H7&ctl00%24txtsearch=Search&ctl00%24MainContent%24hdnActivityId='.$ActivityId.'&ctl00%24MainContent%24hdnAdvancedSearchOptionsOpen=&ctl00%24MainContent%24hdnAdvancedSearchOptionsEntered=&ctl00%24MainContent%24hdnSearchType=&ctl00%24MainContent%24hfScrollPosition=0';
my $post_url='https://selfservice.preston.gov.uk/service/planning/ApplicationSearch.aspx';
my $Referer='https://selfservice.preston.gov.uk/service/planning/ApplicationSearch.aspx';
my $Host = 'selfservice.preston.gov.uk';
my ($Result_content,$Cookie)=&Post_Method($post_url, $Host, $Referer, $Search_po_Content);

my $fh=FileHandle->new("Selfservice_preston_p1.html",'w');
binmode($fh);
$fh->print($Result_content);
# $fh->print($Search_po_Content);
$fh->close();



sub Getcontent
{
	my $url = shift;
	my $Cookie = shift;

	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	# $req->header("Host"=> "dctmviewer.salford.gov.uk");
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=> "en-US,en;q=0.5");
	# $req->header("Accept-Encoding"=> "gzip, deflate, br");
	# $req->header("Content-Type"=> "application/json; charset=utf-8");
	# $req->header("Referer"=> "http://www1.arun.gov.uk/PublicViewer/Authenticated/Main.aspx?user_key_1=AL/115/17/OUT");
	# $req->header("Cookie"=> "JSESSIONID=B6E584616E4C9754FAB0DF5190089C2F; CFID=888645; CFTOKEN=ce0f49f185a0868f-B209C158-B758-3FA0-C71F46DA75AE661D; CFGLOBALS=urltoken%3DCFID%23%3D888645%26CFTOKEN%23%3Dce0f49f185a0868f%2DB209C158%2DB758%2D3FA0%2DC71F46DA75AE661D%23lastvisit%3D%7Bts%20%272018%2D01%2D10%2006%3A05%3A28%27%7D%23timecreated%3D%7Bts%20%272018%2D01%2D10%2005%3A45%3A56%27%7D%23hitcount%3D9%23cftoken%3Dce0f49f185a0868f%2DB209C158%2DB758%2D3FA0%2DC71F46DA75AE661D%23cfid%3D888645%23; _ga=GA1.3.302983350.1515563431; _gid=GA1.3.2073129035.1515563431");
	
	# $req->header("Host"=> "publicaccessdocuments.eastsuffolk.gov.uk");
	# $req->header("Content-Type"=> "text/html; charset=utf-8");
	# $req->header("Host"=> "npe.conwy.gov.uk");	
	# $req->header("Referer" => "http://planning.broxtowe.gov.uk/ApplicationDetail?RefVal=18/00094/FUL");
	# $req->header("Accept-Language"=> "en-US,en;q=0.5");
	# $req->header("Accept-Encoding"=> "gzip, deflate");
	# $req->header("Referer"=> "http://planningpublicaccess.waveney.gov.uk/online-applications/applicationDetails.do?activeTab=externalDocuments&keyVal=ORYHMZQXG5J00");
	# $req->header("User-Agent"=>"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:48.0) Gecko/20100101 Firefox/48.0");
	# $req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	# $req->header("Referer"=>"http://planningpublicaccess.waveney.gov.uk/online-applications/applicationDetails.do?activeTab=externalDocuments&keyVal=ORU6NDQX06O00");
	
	# $req->header("Connection"=> "keep-alive");
	# $req->header("If-Modified-Since"=> "Mon, 30 Mar 2015 14:52:02 GMT");
	# $req->header("If-None-Match"=> "6d4d3f15f96ad01:0");
	if($Cookie ne '')
	{
		print "Cookie: $Cookie\n";
		$req->header("Cookie"=> "$Cookie");
	}	
	my $res = $ua->request($req);
	my %res=%$res;
	my $headers=$res{'_headers'};
	my %headers=%$headers;

	# for my $k (keys %headers)
	# {
		# print "Key:: $k\n";
	# }	
	eval{
	$Cookie=$headers{'set-cookie'};
	my @Cookie=@$Cookie;
	$Cookie=join(" ",@Cookie);
	};
	# print "set-cookie:: $Cookie\n";

	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# return ($content,$co);
	return ($content,$Cookie);
}

sub Getcontent1
{
	my $url = shift;
	my $Cookie = shift;

	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "application/x-www-form-urlencoded");
	# $req->header("Host"=> "npe.conwy.gov.uk");	
	$req->header("Referer"=> "$Referer");
	if($Cookie ne '')
	{
		$req->header("Cookie"=> "$Cookie");
	}	
	# $req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Encoding"=> "gzip, deflate");
	# $req->header("Accept-Language"=> "en-US,en;q=0.5");
	my $res = $ua->request($req);
	my %res=%$res;
	my $headers=$res{'_headers'};
	my %headers=%$headers;

	# for my $k (keys %headers)
	# {
		# print "Key:: $k\n";
	# }	
	my $Cookie=$headers{'set-cookie'};
	# my @Cookie=@$Cookie;
	print "set-cookie:: $Cookie";
# <stdin>;	
	
	return ($Cookie);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# return ($content,$co);
}

sub Post_Method()
{
	my $post_url=shift;
	my $Host=shift;
	my $Referer=shift;
	my $PostContent=shift;
	my $Cookie=shift;
	print "Cookie: $Cookie\n";
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=>"$Host");
	# $req->header("Host"=>"selfservice.preston.gov.uk");
	# $req->header("User-Agent"=>"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:48.0) Gecko/20100101 Firefox/48.0");
	# $req->header("Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	# $req->header("Accept-Language"=>"en-US,en;q=0.5");
	# $req->header("Accept-Encoding"=>"gzip, deflate, br");
	$req->header("Referer"=>"$Referer");
	# $req->header("Cookie"=>"ASP.NET_SessionId=fw1202iwsqliug55gf0qwfve");
	# $req->header("Connection"=>"keep-alive");
	# $req->header("Upgrade-Insecure-Requests"=>"1");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded");

	# if($Cookie ne '')
	# {
		# $req->header("Cookie"=>"$Cookie");
	# }	
	$req->content($PostContent);
	my $res = $ua->request($req);
	my %res=%$res;
	my $headers=$res{'_headers'};
	my %headers=%$headers;

	# for my $k (keys %headers)
	# {
		# print "Key:: $k\n";
	# }	
	$Cookie=$headers{'set-cookie'};
	# if($rerun_count==0)
	# {
		# $rerun_count=1;
		# goto Home;
	# }
	# my @Cookie=@$Cookie;
	# print "\nCookie:::::====> $Cookie\n";
	# <stdin>;
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 1 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$Cookie);
}
