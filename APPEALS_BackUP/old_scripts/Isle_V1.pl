use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use URI::Escape;
use HTTP::Cookies;
use JSON::Parse 'parse_json';
use JSON qw( decode_json ); # from CPAN
use Text::CSV;
use XML::CSV;

my $ua=LWP::UserAgent->new(show_progress=>1);
# $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 1;
# my $ua=LWP::UserAgent->new(ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_PEER',verify_hostname => 1 }, show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:43.0) Gecko/20100101 Firefox/43.0");
$ua->timeout(50); 
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my $month=$ARGV[0];
my $year=$ARGV[0];
$month='0'.$month if (length($month)==1);
# my $End_Date=$ARGV[1];

my $csv_filename="Isle_Output_V1.csv";
my $xml_filename="Isle_Output_V1.xml";

unlink($csv_filename);
unlink($xml_filename);

open (RE,">result.txt");
print RE "ID\tPage_URL\tApp_code\n";
close RE;


# $ua->proxy('http', 'http://172.27.137.192:3128'); 


my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();

open my $fh, ">>", "$csv_filename" or die "Failed to open file: $!";
$csv->print($fh,["ID","Application","Address","Proposal","Application_Type","Date_Application_Received","Date_Application_Validated","Agent_Address_1","Grid_Reference","Document_Url","Applicant_Flag","Council_Code","Page_URL"]);
close $fh;

my $url;
if ($month ne '')
{
	$url='https://www.iwight.com/planning/planAppSearchHistory.aspx';
}
else
{
	$url='https://www.iwight.com/planning/planAppSearch.aspx';
}	
my ($Home_content,$Referer)=&Getcontent($url);
# open(out,">Home_content.html");
# print out $Home_content;
# close out;	

my ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$View_State_Value)=&post_parameters($Home_content);

my $Page_Number=1;
my $line_feed=chr(10);
my $Carriage_return=chr(13);

my ($Search_URL,$content);
if ($month ne '')
{
	$Search_URL = 'https://www.iwight.com/planning/planAppSearchHistory.aspx';
	$content = '__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='.$View_State_Value.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR_Value.'&__EVENTVALIDATION='.$EVENTVALIDATION_Value.'&q=Search+the+site...&txtTCP=&txtPIN=p&txtAddress=&btnSearch=Search';
}
else
{
	$Search_URL = 'https://www.iwight.com/planning/planAppSearch.aspx';
	$content = '__EVENTTARGET=lnkShowAll&__EVENTARGUMENT=&__VIEWSTATE='.$View_State_Value.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR_Value.'&__EVENTVALIDATION='.$EVENTVALIDATION_Value.'&q=Search+the+site...&txtTCP=&txtPIN=&txtAddress=';
}
my ($Search_content,$Referer)=&Post_Method($Search_URL,$content);
my $ID = 1;	
nextt:
$Search_content=decode_entities($Search_content);
open(out,">Search_content.html");
print out $Search_content;
close out;

$ID = &info_collect($Search_content,$month,$ID);

if ($Search_content=~m/<tr[^>]*?EAEAEA\;\">([\w\W]*?)<\/table>/is)
{
	my $block = $1;
	while ($block=~m/javascript:__doPostBack\(\'([^>]*?)\'\,[^>]*?>\s*([^>]*?)\s*</igs)
	{
		my $page_navigationkey = uri_escape($1);
		my $page_no			   = $2;
		print "PageNumber	:: $page_no\n";	
		my ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$View_State_Value)=&post_parameters($Search_content);
		my $Search_URL1 = 'https://www.iwight.com/planning/planAppSearchHistory.aspx';
		my $content1 = '__EVENTTARGET='.$page_navigationkey.'&__EVENTARGUMENT=&__VIEWSTATE='.$View_State_Value.'&__VIEWSTATEGENERATOR='.$VIEWSTATEGENERATOR_Value.'&__EVENTVALIDATION='.$EVENTVALIDATION_Value.'&q=Search+the+site...&txtTCP=&txtPIN=p&txtAddress=';
		my ($Search_content1,$Referer)=&Post_Method($Search_URL1,$content1);
		open(out,">Search_content_while.html");
		print out $Search_content1;
		close out;	
		if ($page_no=~m/\.\.\./is)
		{
			$Search_content=$Search_content1;
			goto nextt;
		}
		$ID = &info_collect($Search_content1,$month,$ID);
	}
}

my $csv_obj = XML::CSV->new();
$csv_obj->parse_doc("$csv_filename", {headings => 1});
$csv_obj->declare_xml({version => '1.0', encoding => 'Windows-1252'}); 
$csv_obj->print_xml("$xml_filename", {file_tag   => 'Items', parent_tag  => 'Item'});

sub info_collect()
{
	my $contenttt = shift;
	my $month	  = shift;	
	my $ID		  = shift;
	
	my $Applicant_Flag = 'N';
	my $Council_Code = 376;
	while($contenttt=~m/<td>\s*<a\s*href\=\"([^>]*?)\"[^>]*?>\s*([\w\W]*?)\s*<\/td>[\w\W]*?<td>Comment[^>]*?[\d]+\/([\d]+)\/([\d]+)\s*/igs)
	{
		my $Page_URL= 'https://www.iwight.com/planning/'.$1;
		my $App_code= $2;
		my $page_month = $3;
		my $page_Year = $4;
		$page_month='0'.$page_month if (length($page_month)==1);
		if ($month ne '')
		{
			next if (($page_month!=$month)&&($page_Year!=$year));
		}	
		# print "$ID :: $App_code\n";
		# open (RE,">>result.txt");
		# print RE "$ID\t$Page_URL\t$App_code\n";
		# close RE;
		decode_entities($Page_URL);
		my ($Application_content,$Referer)=&Getcontent($Page_URL);
		$Application_content=decode_entities($Application_content);
		open(out,">Application_content.html");
		print out $Application_content;
		close out;	
		my $Application = &clean($1) if($Application_content=~m/lblAppNo\">\s*([^>]*?)\s*</is);
		my $Location = &clean($1) if($Application_content=~m/lblLocation\">\s*([\w\W]*?)\s*<\/td>/is);
		my $ProPosal = &clean($1) if($Application_content=~m/lblProposal\">\s*([\w\W]*?)\s*<\/td>/is);
		my $TrackreceivedDate = &clean($1) if($Application_content=~m/lblTrackreceivedDate\">\s*([\w\W]*?)\s*<\/td>/is);
		my $RegDate = &clean($1) if($Application_content=~m/lblTrackRegDate\">\s*([\w\W]*?)\s*<\/td>/is);
		my $Agent_or_Applicant = &clean($1) if($Application_content=~m/lblAgent\">\s*([\w\W]*?)\s*<\/td>/is);
		my $Grid_Reference = &clean($1) if($Application_content=~m/lblEN\">\s*([\w\W]*?)\s*<\/td>/is);
		my $Document_Url = $Page_URL;
		$ProPosal=~s/–/-/igs;
		$Application=~s/^(T|L)[^>]*?\,\s*//igs;
		print "Application $ID: $Application\n";
		my ($Day1,$Month1,$Year1);
		# if($RegDate=~m/^([\d]{2})(?:\/|\-)([\d]{2})(?:\/|\-)([\d]{4})$/is)
		# {
			# $Day1=$1;
			# $Month1=$2;
			# $Year1=$3;
		# }
		# else
		# {
			# print "\nDATE FORMAT ERROR\n";
			# # exit;
		# }
		
		
		
		open my $fh, ">>", "$csv_filename" or die "Failed to open file: $!";
		$csv->print($fh,["$ID","$Application","$Location","$ProPosal","","$TrackreceivedDate","$RegDate","$Agent_or_Applicant","$Grid_Reference","$Document_Url","$Applicant_Flag","$Council_Code","$Page_URL"]);
		close $fh;	
		$ID++;
	}
	return ($ID);
}
sub post_parameters()
{
	my $contentsdfs = shift;
	my ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$View_State_Value);
	if($contentsdfs=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"(__VIEWSTATE)"\s[^>]*?value="([^>]*?)"\s*\/>/is)
	{
		$View_State_Value=uri_escape($2);
	}
	if($contentsdfs=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"(__VIEWSTATEGENERATOR)"\s[^>]*?value="([^>]*?)"\s*\/>/is)
	{
		$VIEWSTATEGENERATOR_Value=uri_escape($2);
	}
	if($contentsdfs=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"(__EVENTVALIDATION)"\s[^>]*?value="([^>]*?)"\s*\/>/is)
	{
		$EVENTVALIDATION_Value=uri_escape($2);
	}
	return ($EVENTVALIDATION_Value,$VIEWSTATEGENERATOR_Value,$View_State_Value);
}
sub DateFormat()
{
	my $Start_Date=shift;
	my $End_Date  =shift;
	my ($Start_Day,$Start_Month,$Start_Year,$End_Day,$End_Month,$End_Year);
	if($Start_Date=~m/^([\d]{2})(?:\/|\-)([\d]{2})(?:\/|\-)([\d]{4})$/is)
	{
		$Start_Day=$1;
		$Start_Month=$2;
		$Start_Year=$3;
	}
	else
	{
		print "\nDATE FORMAT ERROR\n";
		exit;
	}
	if($End_Date=~m/^([\d]{2})(?:\/|\-)([\d]{2})(?:\/|\-)([\d]{4})$/is)
	{
		$End_Day=$1;
		$End_Month=$2;
		$End_Year=$3;
	}
	else
	{
		print "\nDATE FORMAT ERROR\n";
		exit;
	}
	return ($Start_Day,$Start_Month,$Start_Year,$End_Day,$End_Month,$End_Year);
}
sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s*,\s*,\s*/, /igs;
	$values=~s/,\s*,/, /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s*,|,\s*$//igs;
	$values=~s/^\s+|\s+$//igs;
	# decode_entities($values);
	return($values);
}

sub Post_Method()
{
	my $post_url=shift;
	my $Content=shift;
	my $Page_Number=shift;
	my $Start_Date=shift;
	my $End_Date=shift;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.iwight.com");
	$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 

	# $req->header("Cookie"=> "ASP.NET_SessionId=xvflt5esqo0cwwlmnfsaekuq; nmstat=1451561859051; szcookiechoice=p");
	$req->content($Content);
	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	# print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Host"=> "www.iwight.com");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	# print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# return ($content,$co);
	return ($content);
}
