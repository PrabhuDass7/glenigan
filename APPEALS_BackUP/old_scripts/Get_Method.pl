use strict;
use LWP::UserAgent;
use Time::Local qw(timelocal_nocheck);
use DateTime;
use LWP::Simple;
use URI::Escape;

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:42.0) Gecko/20100101 Firefox/42.0");
$ua->timeout(100); 
$ua->max_redirect(0); 
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~ s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my $url='https://www.portsmouth.gov.uk/ext/the-council/transparency/payments-to-suppliers.aspx';
# $url=uri_escape($url);
my ($content)=&Getcontent($url);
open(FI,">portsmouth.html");
print FI $content;
close FI;

sub Getcontent
{
	my $url = shift;
	my $main_url = shift;
	my $rerun_count=0;
	my $redir_url;
	if($url=~m/^\s*$/is)
	{
		return ("","");		
	}
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	$req->header("Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	$req->header("Accept-Language"=> "en-US,en;q=0.5");
	# $req->header("Host"=> "www.portsmouth.gov.uk");
	# $req->header("Cookie"=> "ASP.NET_SessionId=1apuhe5rp0hytiyfze5qfm5i; BNES_ASP.NET_SessionId=CaJ8wSDu3hFrST/FS+t2XOfPo1s3K7S14kVku+ge811ynGFS8xoPeBuapsLr3vVrs56MWdC4opyYN+YLsMlz7kjIYB7dxRyFd/QtCDfKLEo=; __utma=63970606.632953166.1458120501.1458120501.1458120501.1; __utmc=63970606; __utmz=63970606.1458120501.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); pcc-cookie-notification=true");
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
		$content=~s/\&nbsp;/ /igs;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}