use strict;
use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use JSON::Parse 'parse_json';
use JSON qw( decode_json ); # from CPAN
use Text::CSV;
use XML::CSV;

my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0");
$ua->timeout(50); 
$ua->max_redirect(0);
$ua->cookie_jar({});
my $cookiefile = $0;
$cookiefile =~s/.pl/_cookie.txt/g;
my $cookie = HTTP::Cookies->new(file=>$cookiefile,autosave=>1); 
$ua->cookie_jar($cookie);

my $Start_Date=$ARGV[0];
my $End_Date=$ARGV[1];

my $csv_filename="Jersey_Output.csv";
my $xml_filename="Jersey_Output.xml";
# open(out,">>$csv_filename");
# print out "Name\tAddress1\tAddress2\tAddress3\tPhone\tCity\tState\tCountry\tPostalCode\tRegion\tCountry_Url\tRegion_Url\n";
# close out;
my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();

open my $fh, ">>", "$csv_filename" or die "Failed to open file: $!";
$csv->print($fh,["ID","Application","Address","Proposal","Application_Type","Applicant_Address_1","Agent_Address_1","Document_Url","Applicant_Flag","Council_Code","Page_URL"]);
close $fh;

my $url='https://www.gov.je/PlanningBuilding/CurrentPlanningApplications/Pages/index.aspx';
my ($Home_content,$Referer)=&Getcontent($url);
my $Planning_Applications_Url;
if($Home_content=~m/<a\s*[^>]*?\s*href\=(?:\'|\")([^<]*?)(?:\'|\")\s*[^>]*?>\s*Planning\s*applications\s*register\s*</is)
{
	$Planning_Applications_Url=$1;
	print "$Planning_Applications_Url\n";
}
my $Page_Number=1;
my $MaxPageNumber=2;
my $ID=1;
my $Applicant_Flag = 'N';
my $Council_Code = 467;
my $Search_URL='https://www.mygov.je/_layouts/15/PlanningAjaxServices/PlanningSearch.svc/Search';

Next_Page:
my ($Search_content,$Referer)=&Post_Method($Search_URL,$Planning_Applications_Url,$Page_Number,$MaxPageNumber,$Start_Date,$End_Date);
my $Search_content=parse_json($Search_content);
my %Search_content=%$Search_content;

$MaxPageNumber=$Search_content{"MaxPageNumber"};
my $Search_content=$Search_content{"ResultHTML"};
print "MaxPageNumber: $MaxPageNumber\n";
open(out,">Search_content.html");
print out $Search_content;
close out;
while($Search_content=~m/<a\s*[^>]*?\s*href\=(?:\'|\")([^<]*?)(?:\'|\")\s*[^>]*?>\s*See\s*details/igs)
{
	my $Page_URL=$1;
	decode_entities($Page_URL);
	my ($Application_content,$Referer)=&Getcontent($Page_URL);
	open(out,">Application_content.html");
	print out $Application_content;
	close out;	
	my $Application = $1 if($Application_content=~m/Application\s*Reference\s*\:\s*([^>]*?)\s*</is);
	my $Address1 = $1 if($Application_content=~m/<span\s*id=[^>]*?ApplicationAddress[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Address2 = $1 if($Application_content=~m/<span\s*id=[^>]*?RoadName[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Address3 = $1 if($Application_content=~m/<span\s*id=[^>]*?Parish[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Address4 = $1 if($Application_content=~m/<span\s*id=[^>]*?Postcode[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Address = $Address1.', '.$Address2.', '.$Address3.', '.$Address4;
	$Address=~s/&nbsp;/ /igs;
	$Address=~s/,\s+,\s+/, /igs;
	$Address=~s/\s\s+/ /igs;
	$Address=~s/^\s+|\s+$//igs;
	$Address=~s/^,\s*|,\s+$//igs;
	my $Proposal = $1 if($Application_content=~m/<span\s*id=[^>]*?Description[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Application_Type = $1 if($Application_content=~m/<span\s*id=[^>]*?Category[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Applicant_Address_1 = $1 if($Application_content=~m/<span\s*id=[^>]*?Applicant[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Agent_Address_1 = $1 if($Application_content=~m/<span\s*id=[^>]*?Agent[^>]*?>\s*([^>]*?)\s*<\/span>/is);
	my $Document_Url = 'https://www.mygov.je'.$1 if($Application_content=~m/<a\s*[^>]*?\s*href\=(?:\'|\")([^<]*?)(?:\'|\")\s*[^>]*?>\s*Application\s*documentation\s*<\/a>/is);
	
	print "Application: $Application\n";
	print "Address: $Address\n";
	print "Proposal: $Proposal\n";
	print "Application_Type: $Application_Type\n";
	print "Applicant_Address_1: $Applicant_Address_1\n";
	print "Agent_Address_1: $Agent_Address_1\n";
	print "Document_Url: $Document_Url\n";
	print "Page_URL: $Page_URL\n\n";

	open my $fh, ">>", "$csv_filename" or die "Failed to open file: $!";
	$csv->print($fh,["$ID","$Application","$Address","$Proposal","$Application_Type","$Applicant_Address_1","$Agent_Address_1","$Document_Url","$Applicant_Flag","$Council_Code","$Page_URL"]);
	close $fh;	
	$ID++;
}
if($MaxPageNumber > $Page_Number)
{
	$Page_Number++;
	goto Next_Page;
}	

my $csv_obj = XML::CSV->new();
$csv_obj->parse_doc("$csv_filename", {headings => 1});
$csv_obj->declare_xml({version => '1.0', encoding => 'Windows-1252'}); 
$csv_obj->print_xml("$xml_filename",
	{file_tag    => 'GCS-Planning',
		parent_tag  => 'GCS-Planning'});
		
sub clean()
{
	my $values=shift;
	$values=~s/\\n//igs;
	$values=~s/\\t//igs;
	$values=~s/\\//igs;
	$values=~s/&nbsp;/ /igs;
	$values=~s/\s*<[^>]*?>\s*/ /igs;
	$values=~s/\s\s+/ /igs;
	$values=~s/^\s+|\s+$//igs;
	return($values);
}

sub Post_Method()
{
	my $post_url=shift;
	my $Referer=shift;
	my $Page_Number=shift;
	my $MaxPageNumber=shift;
	my $Start_Date=shift;
	my $End_Date=shift;
	print "MAX: $MaxPageNumber\n";
	# exit;
	my $rerun_count=0;
	$post_url =~ s/^\s+|\s+$//g;
	$post_url =~ s/amp;//igs;
	my ($Start_Day,$Start_Month,$Start_Year,$End_Day,$End_Month,$End_Year);
	if($Start_Date=~m/^([\d]{2})(?:\/|\-)([\d]{2})(?:\/|\-)([\d]{4})$/is)
	{
		$Start_Day=$1;
		$Start_Month=$2;
		$Start_Year=$3;
	}
	else
	{
		print "\nDATE FORMAT ERROR\n";
		exit;
	}
	if($End_Date=~m/^([\d]{2})(?:\/|\-)([\d]{2})(?:\/|\-)([\d]{4})$/is)
	{
		$End_Day=$1;
		$End_Month=$2;
		$End_Year=$3;
	}
	else
	{
		print "\nDATE FORMAT ERROR\n";
		exit;
	}
	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "www.mygov.je");
	$req->header("Content-Type"=>"application/json; charset=utf-8"); 
	$req->header("Referer"=> "$Planning_Applications_Url");
	$req->content('{"URL":"https://www.mygov.je//Planning/Pages/Planning.aspx","CommonParameters":"|05|'.$Page_Number.'|'.$MaxPageNumber.'|49.21042016382462|49.21042016382462|12","SearchParameters":"|1301||||0|All|All|'.$Start_Day.'|'.$Start_Month.'|'.$Start_Year.'|'.$End_Day.'|'.$End_Month.'|'.$End_Year.'"}');
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return ($content,$redir_url);
}

sub Getcontent
{
	my $url = shift;
	my $rerun_count=0;
	my $redir_url;
	$url=~s/^\s+|\s+$//g;
	$url=~s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=> "text/html; charset=utf-8");
	if($url=~m/www\.mygov\.je/is)
	{
		$req->header("Host"=> "www.mygov.je");
	}
	else
	{	
		$req->header("Host"=> "www.gov.je");
	}	
	my $res = $ua->request($req);
	$cookie->extract_cookies($res);
	$cookie->save;
	$cookie->add_cookie_header($req);
	my $code=$res->code;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count<=3)
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
				$redir_url=$u2;
			}
			else
			{
				$url=$loc;
				$redir_url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# return ($content,$co);
	return ($content);
}
