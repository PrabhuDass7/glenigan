use LWP::UserAgent;
use HTML::Entities;
use URI::URL;
use HTTP::Cookies;
use filehandle;
use POSIX qw(strftime);
use URI::Escape;
use Date::Calc ("Add_Delta_Days","Today");
use Text::CSV;
use XML::CSV;
use CAM::PDF;
use CAM::PDF::PageText;

# my $char=',';
# print ord($char)."\n";
# exit;
open(ti,">>Time_Log.txt");
print ti "Start :".localtime()."\n";
close ti;
my $ua=LWP::UserAgent->new(show_progress=>1);
$ua->agent("User-Agent=> Mozilla/5.0 (Windows NT 6.1; rv:29.0) Gecko/20100101 Firefox/29.0");
$ua->max_redirect(0);
# $ua->proxy('http', 'http://172.27.137.192:3128'); 
# my $cookie = HTTP::Cookies->new(file=>$0."_cookie.txt",autosave=>1);
# my $Date = strftime "%d/%m/%Y", localtime;

my $csv = Text::CSV->new({ binary => 1, auto_diag => 1, eol => "\n"})
        or die "Cannot use CSV: " . Text::CSV->error_diag();

my($year,$month,$day) =Add_Delta_Days( Today(), -1 ),"\n";
$month = sprintf("%02d", $month);	
$day = sprintf("%02d", $day);	
my $Date = "$day/$month/$year";
# my $Date = '24/03/2017';
my $Date = $ARGV[0];
my $Month = $ARGV[1];
print "Date: $Date\n";
my $Date_Tmp=$Date;
$Date_Tmp=~s/\//_/igs;
my $Pdf_store_path="\\\\172.27.137.182\\commonglenigan\\Automation\\Appeals\\$Month\\$Date_Tmp";
# my $Pdf_store_path="C:\\Appeals\\Appeals\\$Month\\$Date_Tmp";
# my $Pdf_store_path="C:\\Appeals\\$Date_Tmp";
unless ( -d $Pdf_store_path )
{
	$Pdf_store_path=~s/\//\\/igs;
	system("mkdir $Pdf_store_path");
}
my $CSV_Output_File_Name = "$Pdf_store_path\\Appeals_CaseWork_$Date_Tmp.csv";
my $Output_File_Name="$Pdf_store_path\\Appeals_CaseWork_$Date_Tmp.xls";
my $Xml_Output_File_Name="$Pdf_store_path\\Appeals_CaseWork_$Date_Tmp.xml";

open my $fh, ">", "$CSV_Output_File_Name" or die "Failed to open file: $!";
$csv->print($fh,["ID","Date","Search_Date_Type","Search_Status_Type","Application","Applicant_Name","Agent_Name","Address","Application_Type","Local_Planning_Authority","Proposal","Appeal_Status","Appeal_Decision_Status","Appeal_Lodged_Date","Appeal_Decision_Date","Document_Url","Council_Code","PDF_Appeal_application","PDF_Application_Refno","PDF_Refused_date","PDF_Appeal_Decision_status"]);
close $fh;
	
# open(XML,">>$Xml_Output_File_Name");
# print XML '<?xml version="1.0" encoding="us-ascii"?>'."\n".'<Items>'."\n";
# close XML;

open(OUT,">$Output_File_Name");
print OUT "ID\tDate\tSearch_Date_Type\tSearch_Status_Type\tApplication\tApplicant_Name\tAgent_Name\tAddress\tApplication_Type\tLocal_Planning_Authority\tProposal\tAppeal_Status\tAppeal_Decision_Status\tAppeal_Lodged_Date\tAppeal_Decision_Date\tDocument_Url\tCouncil_Code\tPDF_Appeal_application\tPDF_Application_Refno\tPDF_Refused_date\tPDF_Appeal_Decision_status\n";
close OUT;

my %Search_Type;
my $Base_Url='https://acp.planningportal.gov.uk/CaseSearch.aspx';
my ($Search_Form_content,$redir)=&Getcontent($Base_Url);
open(poc,">Search_Form_content.html");
print poc $Search_Form_content;
close poc;
my ($EVENTVALIDATION,$VIEWSTATE,$VIEWSTATEGENERATOR);
if($Search_Form_content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"(__EVENTVALIDATION)"\s[^>]*?value="([^>]*?)"\s*\/>/is)
{
	$EVENTVALIDATION=$2;
}
if($Search_Form_content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"(__VIEWSTATE)"\s[^>]*?value="([^>]*?)"\s*\/>/is)
{
	$VIEWSTATE=$2;
}
if($Search_Form_content=~m/<input\s*type\s*=\s*"hidden"\s*name\s*=\s*"(__VIEWSTATEGENERATOR)"\s[^>]*?value="([^>]*?)"\s*\/>/is)
{
	$VIEWSTATEGENERATOR=$2;
}

my @Search_Status=('IN_PROGRESS','COMPLETE');
for my $Type(@Search_Status)
{
my $Post_Content1='-----------------------------14913562229646
Content-Disposition: form-data; name="ScriptManager1_HiddenField"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTTARGET"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTARGUMENT"


-----------------------------14913562229646
Content-Disposition: form-data; name="__LASTFOCUS"


-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATE"

'.$VIEWSTATE.'
-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATEGENERATOR"

'.$VIEWSTATEGENERATOR.'
-----------------------------14913562229646
Content-Disposition: form-data; name="__SCROLLPOSITIONX"

0
-----------------------------14913562229646
Content-Disposition: form-data; name="__SCROLLPOSITIONY"

0
-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATEENCRYPTED"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTVALIDATION"

'.$EVENTVALIDATION.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$hidIsListed"

No
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$pcsLanguageSelect$dlstLanguageSelect"

1
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtCaseReference"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtStreet"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtTownCity"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtCounty"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtPostCode"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtSearchLPA"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txt_lparefnumber"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboAppealType"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$ppsAppellant$txtPerson"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$ppsOtherParty$txtPerson"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsHearing$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsSiteVisit$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsCallIn$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsReceived$txtDateSearch"

'.$Date.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsStart$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsDecision$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboProcedureType"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboStatus"

'.$Type.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cmdSearch"

Search
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboSearchLPA"

-1
-----------------------------14913562229646
';
$Search_Type{"Date Received|$Type"}=$Post_Content1;
}

for my $Type(@Search_Status)
{
my $Post_Content1='-----------------------------14913562229646
Content-Disposition: form-data; name="ScriptManager1_HiddenField"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTTARGET"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTARGUMENT"


-----------------------------14913562229646
Content-Disposition: form-data; name="__LASTFOCUS"


-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATE"

'.$VIEWSTATE.'
-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATEGENERATOR"

'.$VIEWSTATEGENERATOR.'
-----------------------------14913562229646
Content-Disposition: form-data; name="__SCROLLPOSITIONX"

0
-----------------------------14913562229646
Content-Disposition: form-data; name="__SCROLLPOSITIONY"

0
-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATEENCRYPTED"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTVALIDATION"

'.$EVENTVALIDATION.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$hidIsListed"

No
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$pcsLanguageSelect$dlstLanguageSelect"

1
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtCaseReference"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtStreet"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtTownCity"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtCounty"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtPostCode"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtSearchLPA"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txt_lparefnumber"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboAppealType"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$ppsAppellant$txtPerson"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$ppsOtherParty$txtPerson"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsHearing$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsSiteVisit$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsCallIn$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsReceived$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsStart$txtDateSearch"

'.$Date.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsDecision$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboProcedureType"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboStatus"

'.$Type.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cmdSearch"

Search
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboSearchLPA"

-1
-----------------------------14913562229646--
';
$Search_Type{"Start Date|$Type"}=$Post_Content1;
}

for my $Type(@Search_Status)
{
my $Post_Content2='-----------------------------14913562229646
Content-Disposition: form-data; name="ScriptManager1_HiddenField"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTTARGET"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTARGUMENT"


-----------------------------14913562229646
Content-Disposition: form-data; name="__LASTFOCUS"


-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATE"

'.$VIEWSTATE.'
-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATEGENERATOR"

'.$VIEWSTATEGENERATOR.'
-----------------------------14913562229646
Content-Disposition: form-data; name="__SCROLLPOSITIONX"

0
-----------------------------14913562229646
Content-Disposition: form-data; name="__SCROLLPOSITIONY"

0
-----------------------------14913562229646
Content-Disposition: form-data; name="__VIEWSTATEENCRYPTED"


-----------------------------14913562229646
Content-Disposition: form-data; name="__EVENTVALIDATION"

'.$EVENTVALIDATION.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$hidIsListed"

No
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$pcsLanguageSelect$dlstLanguageSelect"

1
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtCaseReference"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtStreet"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtTownCity"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtCounty"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtPostCode"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txtSearchLPA"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$txt_lparefnumber"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboAppealType"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$ppsAppellant$txtPerson"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$ppsOtherParty$txtPerson"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsHearing$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsSiteVisit$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsCallIn$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsReceived$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsStart$txtDateSearch"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$pdsDecision$txtDateSearch"

'.$Date.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboProcedureType"


-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboStatus"

'.$Type.'
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cmdSearch"

Search
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$cphMainContent$cboSearchLPA"

-1
-----------------------------14913562229646
Content-Disposition: form-data; name="ctl00$hidDebug"

Hide Debug
-----------------------------14913562229646--
';
$Search_Type{"Decision Date|$Type"}=$Post_Content2;
}

my $ID;
for my $Method(keys %Search_Type)
{
	print "Method: $Method\n";
	my $Post_Content=$Search_Type{$Method};
	my ($Search_Date_Type,$Search_Status_Type)=split('\|',$Method);
	my $Council_Code;
	if(($Search_Date_Type eq 'Date Received') or ($Search_Date_Type eq 'Start Date'))
	{
		$Council_Code=1;
	}
	elsif($Search_Date_Type eq 'Decision Date')
	{
		$Council_Code=2;
	}
	# print "$Post_Content\n";
	my $url='https://acp.planningportal.gov.uk/CaseSearch.aspx';

	my ($Search_content,$redir)=&Post_Method($url,$Date,$Post_Content);

	my $file_name='Appeals_Casework_Portal.html';
	my $fh = FileHandle->new("$file_name",'w') or die "Cannot open $file_name for write :$!";
	binmode($fh);
	$fh->print($Search_content);
	$fh->close();
	my $num=1;
	while($Search_content=~m/<a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*(APP\/[^>]*?)\s*<\/a>/igs)
	{
		my $Case_Reference_Link='https://acp.planningportal.gov.uk/'.$1;
		my $Case_Reference=$2;
		print "Appeal Running for $Date\n";
		my ($Case_Reference_content,$redir)=&Getcontent($Case_Reference_Link,$Date);
		open(poc,">Case_Reference_content.html");
		print poc $Case_Reference_content;
		close poc;
		# my $Ping_Status=system("Ping.pl",$Case_Reference_Link);
		my $da=$num%5;
		if($da==0)
		{
			# print "Sleep for 5 min\n";
			# sleep(300);
			my ($h,$r)=&Getcontent("https://acp.planningportal.gov.uk/CaseSearch.aspx");			
		}
		$num++;
		$cnt=1;
		Re_Ping:
		# my $Ping_Status=`Ping.pl $Case_Reference_Link`;
		# if($Ping_Status=~m/^\s*$/is)
		# {
			# print "Re_Ping Count: $cnt\n";
			# if($cnt<2)
			# {
				# $cnt++;
				# goto Re_Ping;
			# }	
		# }
		# print "Ping_Status: $Ping_Status\n";
		# open(poc,"<Page_Content_Test.html");
		# my @Case_Reference_content=<poc>;
		# close poc;
		# my $Case_Reference_content=join("",@Case_Reference_content);
		
		my ($Applicant,$Agent,$Site_Address,$Case_Type,$Local_Planning_Authority,$Case_Summary,$Status,$Decision_and_Outcome,$Start_Date,$Decision_Date,$Decision_Doc_Link);
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labName[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Applicant=&Trim($1);
			if($Applicant=~m/^\s*$/is)
			{
				if($cnt<3)
				{
					$cnt++;
					goto Re_Ping;
				}	
			}
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labAgentName[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Agent=&Trim($1);
			if($Agent=~m/^\s*$/is)
			{
				if($cnt<3)
				{
					$cnt++;
					goto Re_Ping;
				}	
			}
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labSiteAddress[^>]*?>\s*([\w\W]*?)\s*<\/span>/is)
		{
			$Site_Address=$1;
			$Site_Address=~s/\s*<br\s*\/\s*>\s*/, /igs;
			$Site_Address=&Trim($Site_Address);
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labCaseTypeName[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Case_Type=&Trim($1);
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labLPAName\"[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Local_Planning_Authority=&Trim($1);
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labCaseSummary\s*\"[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Case_Summary=&Trim($1);
			if($Case_Summary=~m/^\s*$/is)
			{
				print "Re_Ping:Count: $cnt\nCase_Summary is empty\n";
				open(REF,">Case_Reference_content$cnt.html");
				print REF $Case_Reference_content;
				close REF;
				if($cnt<3)
				{
					$cnt++;
					goto Re_Ping;
				}	
			}
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labStatus\s*\"[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Status=&Trim($1);
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labOutcome\s*\"[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Decision_and_Outcome=&Trim($1);
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labStartDate\s*\"[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Start_Date=&Trim($1);
		}
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labDecisionDate\s*\"[^>]*?>\s*([^>]*?)\s*<\/span>/is)
		{
			$Decision_Date=&Trim($1);
		}
		
		if($Case_Reference_content=~m/<span[^>]*?cphMainContent_labDecisionLink\s*\"[^>]*?><a\s*[^>]*?\s*href\=(?:\"|\')([^<]*?)(?:\"|\')\s*[^>]*?>\s*([^>]*?)\s*</is)
		{
			$Decision_Doc_Link='https://acp.planningportal.gov.uk/'.$1;
			$PDF_Name=&Trim($2);
		}
		if($Decision_Doc_Link!~m/^\s*$/is)
		{
			my $Case_Reference_Tmp=$Case_Reference;
			$Case_Reference_Tmp=~s/\//_/igs;
			my $Pdf_store_path="$Pdf_store_path\\$Case_Reference_Tmp";
			unless ( -d $Pdf_store_path )
			{
				$Pdf_store_path=~s/\//\\/igs;
				system("mkdir $Pdf_store_path");
			}
			$PDF_Name =~ s/\s+/_/igs;
			my $file_name="$Pdf_store_path\\$PDF_Name";
			print "File_name: $file_name\n";
			my ($Decision_PDF,$redir)=&Getcontent($Decision_Doc_Link);
			my $fh = FileHandle->new("$file_name",'w') or die "Cannot open $file_name for write :$!";
			binmode($fh);
			$fh->print($Decision_PDF);
			$fh->close();
		}
		$ID++;
		### Getting PDF Content ###
		my $Case_Reference_Tmp1=$Case_Reference;
		$Case_Reference_Tmp1=~s/\//_/igs;
		$pdf_text_file_name="$Pdf_store_path\\$Case_Reference_Tmp1\\$PDF_Name";
		$text_file_name=$pdf_text_file_name;
		$text_file_name=~s/\.([a-z]{3,4})$/\.txt/isg;
		print "PDF : $text_file_name\n";
		eval{
			system("C:\\One_App\\deillustrate.pl $pdf_text_file_name $pdf_text_file_name");
		};
		eval{
			system("C:\\One_App\\pdftotext1.exe $pdf_text_file_name $text_file_name");
		};
		
		open(out,"<$text_file_name");
		my @PDF_ContentA=<out>;
		close out;
		
		my $i=0;
		my $PDF_contentB=join("",@PDF_ContentA);
		#$PDF_contentB=~s/(Appeal\s*(?:[A-z0-9]+\s*)?Ref(.*?))(Appeal\s*(?:[A-z0-9]+\s*)?Ref(.*?)Decision)/$1Appeal190$3/is;
		#$PDF_contentB=~s/(Appeal\s*(?:[A-z0-9]+\s*)?\d+\.\s*The\s*appeal\s*is\s*(.*?))(Appeal\s*(?:[A-z0-9]+\s*)?\d+\.\s*The\s*appeal\s*is)/$1Appeal$3/is;
		my (@PDF_Appeal_application,@PDF_application_refno,@PDF_Refused_date,@PDF_Appeal_Decision_status);
		if($PDF_contentB=~m/Appeal\s*(?:[A-z0-9]+\s*)?Ref[\w\W]*?Appeal\s*(?:[A-z0-9]+\s*)?Ref/is){			
			my($content_a,$content_b);
			
			if($PDF_contentB=~m/(Appeal\s*(?:[A-z0-9]+\s*)?Ref([\w\W]*?))(Appeal\s*(?:[A-z0-9]+\s*)?Ref([\w\W]*?))Decisions?\s*Appeal/is){
				$content_a=$1;
				$content_b=$3;
				
			}
			if($PDF_contentB=~m/(Appeal\s*(?:[A-z0-9]+\s*)?\d+\.\s*The\s*appeal\s*is\s*([\w\W]*?))(Appeal\s*(?:[A-z0-9]+\s*)?\d+\.\s*The\s*appeal\s*is\s*([A-z]+))/is){
				$content_a.=$1;
				$content_b.=$3;
				#print $content_b;
			}
			$PDF_Appeal_application[0]=$1 if($content_a=~m/Appeal\s*(?:[A-z0-9]+\s*)?Ref\s*\:?\.?\-?\s*([A-z\/\d]+)\s/is);
			$PDF_application_refno[0]=$1 if($content_a=~m/application\s*Ref\s*\.?\:?\s*([^\,\s]+)\,?\s/is);
			$PDF_Refused_date[0]=$1 if($content_a=~m/refused[A-z\s]+dated\s*([^\.]+)\./is);
			$PDF_Appeal_Decision_status[0]=ucfirst($1) if($content_a=~m/s*\d+\.\s*The\s*appeal\s*is\s*([A-z]+)/is);
			$PDF_Appeal_application[1]=$1 if($content_b=~m/Appeal\s*(?:[A-z0-9]+\s*)?Ref\s*\:?\.?\-?\s*([A-z\/\d]+)\s/is);
			$PDF_application_refno[1]=$1 if($content_b=~m/application\s*Ref\s*\.?\:?\s*([^\,\s]+)\,?\s/is);
			$PDF_Refused_date[1]=$1 if($content_b=~m/refused[A-z\s]+dated\s*([^\.]+)\./is);
			$PDF_Appeal_Decision_status[1]=ucfirst($1) if($content_b=~m/\d+\.\s*The\s*appeal\s*is\s*([A-z]+)/is);
		}
		else
		{
			$PDF_Appeal_application[0]=$1 if($PDF_contentB=~m/Appeal\s*(?:[A-z0-9]+\s*)?Ref\s*\:?\.?\-?\s*([A-z\/\d]+)\s/isg);
			$PDF_application_refno[0]=$1 if($PDF_contentB=~m/application\s*Ref\s*\.?\:?\s*([^\,\s]+)\,?\s/isg);
			$PDF_Refused_date[0]=$1 if($PDF_contentB=~m/refused[A-z\s]+dated\s*([^\.]+)\./isg);
			$PDF_Appeal_Decision_status[0]=ucfirst($1) if($PDF_contentB=~m/Decision\s*\d+\.\s*The\s*appeal\s*is\s*([A-z]+)/isg);
		}
		# my $PDF_application_refno=$1 if($PDF_contentB=~m/application\s*Ref\s*\.?\:?\s*([^\,\s]+)\,?\s/is);
		# my $PDF_Refused_date=$1 if($PDF_contentB=~m/refused[A-z\s]+dated\s*([^\.]+)\./is);
		# my $PDF_Appeal_Decision_status=ucfirst($1) if($PDF_contentB=~m/Decision\s*\d+\.\s*The\s*appeal\s*is\s*([A-z]+)/is);
		unlink($text_file_name);
		
		print "Applicant		: $Applicant\n";
		print "Agent			: $Agent\n";
		print "Site_Address		: $Site_Address\n";
		print "Case_Type		: $Case_Type\n";
		print "Planning_Authority	: $Local_Planning_Authority\n";
		print "Case_Summary		: $Case_Summary\n";
		print "Status			: $Status\n";
		print "Decision & Outcome	: $Decision_and_Outcome\n";
		print "Start_Date		: $Start_Date\n";
		print "Decision_Date		: $Decision_Date\n";
		print "Decision_Doc		: $Decision_Doc_Link\n\n";

		#For unmatched regex in PDF converted source.
		if(@PDF_Appeal_application==0){
		@PDF_Appeal_application=('');
		}
		open my $fh, ">>", "$CSV_Output_File_Name" or die "Failed to open file: $!";
		open(OUT,">>$Output_File_Name");
		for($i=0;$i<@PDF_Appeal_application;$i++)
		{
			$csv->print($fh,["$ID","$Date","$Search_Date_Type","$Search_Status_Type","$Case_Reference","$Applicant","$Agent","$Site_Address","$Case_Type","$Local_Planning_Authority","$Case_Summary","$Status","$Decision_and_Outcome","$Start_Date","$Decision_Date","$Decision_Doc_Link","$Council_Code","$PDF_Appeal_application[$i]","$PDF_application_refno[$i]","$PDF_Refused_date[$i]","$PDF_Appeal_Decision_status[$i]"]);
			
			print OUT "$ID\t$Date\t$Search_Date_Type\t$Search_Status_Type\t$Case_Reference\t$Applicant\t$Agent\t$Site_Address\t$Case_Type\t$Local_Planning_Authority\t$Case_Summary\t$Status\t$Decision_and_Outcome\t$Start_Date\t$Decision_Date\t$Decision_Doc_Link\t$Council_Code\t$PDF_Appeal_application[$i]\t$PDF_application_refno[$i]\t$PDF_Refused_date[$i]\t$PDF_Appeal_Decision_status[$i]\n";
		}	
		close $fh;
		close OUT;
		print "While End\n";
		# &Write_XML($ID,$Date,$Search_Date_Type,$Search_Status_Type,$Case_Reference,$Applicant,$Agent,$Site_Address,$Case_Type,$Local_Planning_Authority,$Case_Summary,$Status,$Decision_and_Outcome,$Start_Date,$Decision_Date,$Decision_Doc_Link,$Council_Code);
	}
}
my $csv_obj = XML::CSV->new();
$csv_obj->parse_doc("$CSV_Output_File_Name", {headings => 1});
$csv_obj->declare_xml({version => '1.0', encoding => 'Windows-1252'}); 
$csv_obj->print_xml("$Xml_Output_File_Name",
	{file_tag    => 'Appeals',
		parent_tag  => 'Appeal'});
		
# open(XML,">>$Xml_Output_File_Name");
# print XML '</Items>';
# close XML;
open(ti,">>Time_Log.txt");
print ti "End   :".localtime()."\n";
close ti;

sub Write_XML()
{
	my ($ID,$Date,$Search_Date_Type,$Search_Status_Type,$Case_Reference,$Applicant,$Agent,$Site_Address,$Case_Type,$Local_Planning_Authority,$Case_Summary,$Status,$Decision_and_Outcome,$Start_Date,$Decision_Date,$Decision_Doc_Link,$Council_Code)=@_;
	
	open(XML,">>$Xml_Output_File_Name");
	print XML '<Item>'."\n";
	print XML '<ID>'.$ID.'</ID>'."\n";
	print XML '<Date>'.$Date.'</Date>'."\n";
	print XML '<Search_Date_Type>'.$Search_Date_Type.'</Search_Date_Type>'."\n";
	print XML '<Search_Status_Type>'.$Search_Status_Type.'</Search_Status_Type>'."\n";
	print XML '<Application>'.$Case_Reference.'</Application>'."\n";
	print XML '<Applicant_Name>'.$Applicant.'</Applicant_Name>'."\n";
	print XML '<Agent_Name>'.$Agent.'</Agent_Name>'."\n";
	print XML '<Address>'.$Site_Address.'</Address>'."\n";
	print XML '<Application_Type>'.$Case_Type.'</Application_Type>'."\n";
	print XML '<Local_Planning_Authority>'.$Local_Planning_Authority.'</Local_Planning_Authority>'."\n";
	print XML '<Proposal>'.$Case_Summary.'</Proposal>'."\n";
	print XML '<Appeal_Status>'.$Status.'</Appeal_Status>'."\n";
	print XML '<Appeal_Decision_Status>'.$Decision_and_Outcome.'</Appeal_Decision_Status>'."\n";
	print XML '<Appeal_Lodged_Date>'.$Start_Date.'</Appeal_Lodged_Date>'."\n";
	print XML '<Appeal_Decision_Date>'.$Decision_Date.'</Appeal_Decision_Date>'."\n";
	print XML '<Document_Url>'.$Decision_Doc_Link.'</Document_Url>'."\n";
	print XML '<Council_Code>'.$Council_Code.'</Council_Code>'."\n";
	print XML '</Item>'."\n";
	close XML;
}


sub Getcontent
{
	my $url = shift;
	my $ref = shift;
	my $rerun_count=0;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $req = HTTP::Request->new(GET=>$url);
	$req->header("Content-Type"=>"text/html; charset=utf-8"); 
	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my $content;
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			if($loc!~m/http/is)
			{
				my $u1=URI::URL->new($loc,$url);
				my $u2=$u1->abs;
				$url=$u2;
			}
			else
			{
				$url=$loc;
			}
			goto Home;
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	return($content,$url);
}

sub Getcontent_Alarm
{
	my $url = shift;
	my $ref = shift;
	my $rerun_count=0;
	$url =~ s/^\s+|\s+$//g;
	$url =~ s/amp;//igs;
	Home:
	my $cot;
	my $content;
	eval {
		alarm (10);
			my $req = HTTP::Request->new(GET=>$url);
			$req->header("Content-Type"=>"application/x-www-form-urlencoded"); 
			my $res = $ua->request($req);
			# $cookie->extract_cookies($res);
			# $cookie->save;
			# $cookie->add_cookie_header($req);
			my $code=$res->code;
			my $status_line=$res->status_line;
			print "CODE :: $code\n";
			if($code =~m/20/is)
			{
				$content = $res->content;
			}
			elsif($code =~m/30/is)
			{
				my $loc=$res->header("location");
				if ( $rerun_count <= 3 )
				{
					$rerun_count++;
					if($loc!~m/http/is)
					{
						my $u1=URI::URL->new($loc,$url);
						my $u2=$u1->abs;
						$url=$u2;
					}
					else
					{
						$url=$loc;
					}
					goto Home;
				}
			}
			else
			{
				if ( $rerun_count <= 3 )
				{
					$rerun_count++;
					sleep 1;
					goto Home;
				}
			}
		alarm(0);
	};
	if ($@) {
		if ($@ =~ /timeout/) 
		{
			open (RER,">>failed.txt");
			print RER "TimeOuttt\n";
			close RER;
			print "asfddfadsTimeOuttt:TimeOuttt\n";
		}
		else 
		{
			alarm(0);           # clear the still-pending alarm
			die;                # propagate unexpected exception
		} 
	}
	return($content,$url);
}

sub Post_Method()
{
	my $post_url=shift;
	my $Date=shift;
	my $Post_Content=shift;
	my $rerun_count=0;

	Home:
	my $req = HTTP::Request->new(POST=>$post_url);
	$req->header("Host"=> "acp.planningportal.gov.uk");
	$req->header("Content-Type"=>"multipart/form-data; boundary=---------------------------14913562229646"); 
	# $req->header("Content-Type"=>"multipart/form-data;"); 
	$req->header("Referer"=> "https://acp.planningportal.gov.uk/CaseSearch.aspx");
	$req->content("$Post_Content");

	my $res = $ua->request($req);
	# $cookie->extract_cookies($res);
	# $cookie->save;
	# $cookie->add_cookie_header($req);
	my $code=$res->code;
	my $status_line=$res->status_line;
	print "CODE :: $code\n";
	my ($content,$redir_url);
	if($code =~m/20/is)
	{
		$content = $res->content;
	}
	elsif($code =~m/30/is)
	{
		my $loc=$res->header("location");
		$redir_url=$loc;
		# print "Loc: $loc\n";
		if($rerun_count <= 3)
		{
			my $u1=URI::URL->new($loc,$post_url);
			my $u2=$u1->abs;
			my $Redir_url=$u2;
			($content,$Redir_url)=&Getcontent($u2);
		}
	}
	else
	{
		if ( $rerun_count <= 3 )
		{
			$rerun_count++;
			sleep 1;
			goto Home;
		}
	}
	# open(poc,">POST_PcccC.html");
	# print poc $content;
	# close poc;
	return ($content,$redir_url);
}

sub Trim()
{
	my $value=shift;
	my $Replace_Char;
	print "Value: $value\n";
	if($value=~m/([^[:print:]])/is)
	{
		my $char=$1;
		my $code=ord($char);
		if($code=~m/\b(?:146|226)\b/is)
		{
			$value=~s/[^[:print:]]+/\'/igs;
		}
		elsif($code=~m/\b13\b/is)
		{
			$value=~s/[^[:print:]]+/\-/igs;
		}
		else
		{
			$value=~s/[^[:print:]]+//igs;
		}
	}
	$value=~s/\s*<[^>]*?>\s*/ /igs;
	$value=~s/\s\s+/ /igs;
	return($value);
}